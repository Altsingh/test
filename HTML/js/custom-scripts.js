$(document).ready(function() {
	// upload file
	$(".file").change(function(){
	$(".fileValue").text($(this).val());
	});

	$(window).scroll(function(){
	
	  if ($(window).scrollTop() >= 81) {
		$('.notification-bar').addClass('notification-fixed');
	   }
	   else {
		$('.notification-bar').removeClass('notification-fixed');		
	   }
	});
	$(window).scroll(function(){
	
	  if ($(window).scrollTop() >= 200) {
		$('.top-btn').addClass('display-show');
	   }
	   else {

		$('.top-btn').removeClass('display-show');
	   }
	});
 	// scroll Top 
	$('.top-btn').click(function(){
		$('html,body').animate({ scrollTop: 0 }, "slow");
	 });
	// zoom card 
	$('.zoom-out-icon').click(function(){
		$('.module-true').show();
		$(this).parent().parent().parent().parent().addClass('zoom-card');
	 });
	 $('.zoom-in-icon').click(function(){
		$('.module-true').hide();
		$(this).parent().parent().parent().parent().removeClass('zoom-card');
	 });
	 
	//Dashboard Bottom
	$('.dashboard-bottom ul li').click(function(){
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
	});
 	// Dropdown	active	
 	$('.dropdown-block li.dropdown').click(function(){
		$(this).addClass('active');		
	});	
	$('ul.dropdown-menu > li').click(function () {
		$('ul.dropdown-menu > li').removeClass('active');
		$(this).addClass('active');    
	});
		
 	$('.navigation li.dropdown ').click(function(){		
		$('.navigation li.dropdown').removeClass('active');
		$(this).addClass('active');
	});
	
	//  Filter Dropdown
	$('.dropdown-style li').click(function () {
		$('.dropdown-style li').removeClass('active');
		$(this).addClass('active');    
	});
	
	$('.dropdown-filter-block .dropdown-style ul li').click(function () {
		$('.dropdown-filter-block .dropdown-style ul li').removeClass('active');
		$(this).addClass('active');    
	});
	
   
	// Dailog module	
	$('.social-block-profile .social-block-footer-lg .li-icon').click(function(){
		$(this).parent().parent().addClass('input-arrow-top1');
		$(this).parent().parent().removeClass('input-arrow-top4');
		$(this).parent().parent().removeClass('input-arrow-top2');
		$(this).parent().parent().removeClass('input-arrow-top3');
	});
	$('.social-block-profile .social-block-footer-lg .fb-icon').click(function(){
		$(this).parent().parent().addClass('input-arrow-top2');
		$(this).parent().parent().removeClass('input-arrow-top1');
		$(this).parent().parent().removeClass('input-arrow-top3');
		$(this).parent().parent().removeClass('input-arrow-top4');
	});
	$('.social-block-profile .social-block-footer-lg .tw-icon').click(function(){
		$(this).parent().parent().addClass('input-arrow-top3');
		$(this).parent().parent().removeClass('input-arrow-top1');
		$(this).parent().parent().removeClass('input-arrow-top2');
		$(this).parent().parent().removeClass('input-arrow-top4');
	});
		$('.social-block-profile .social-block-footer-lg .gp-icon').click(function(){
		$(this).parent().parent().addClass('input-arrow-top4');
		$(this).parent().parent().removeClass('input-arrow-top1');
		$(this).parent().parent().removeClass('input-arrow-top2');
		$(this).parent().parent().removeClass('input-arrow-top3');
	});
	
	// Dropdown	7
	$('.dropdown-7 ul.sub-dropdown-7 li').click(function (){
		$(this).addClass('actives');		
	});
	
	
	// Dropdown	filter height fixed  
	$('.dropdown-common li').click(function (){
		var LiHeight = $(this).children('ul').children('li').innerHeight();
		var LiLength = $(this).children('ul').children('li').length;
		var ulHeight = LiLength * LiHeight;
		var parentHeight = $(this).parent('ul').height();
		if(parentHeight > ulHeight){
			$('.dropdown-common').css({
				'height': 'auto'
				})
		}
		else { $(this).parent('ul').height(ulHeight); }
	});
	
	// sub-dropdown
	$('.sub-dropdown li').click(function (){
		$(this).addClass('actives');	
		$(this).siblings().removeClass('actives');		
	});
	
 
	// Button Start	
	$('.btn-dashboard').hover(function(){
		$("body").toggleClass('btn-dashboard-show');	
		$(this).addClass('btn-dashboard');
	});
  
  // Menu Style
  $('.menu-click').click(function (){
  		$('.module-true').show();
		$(this).next('.menu-style').show();	
  });
  $('.close').click(function (){
	  	$(this).parent('.menu-style').hide();
  		$('.module-true').hide();
  });
  
    // Forgot
  $('.forgot-click ').click(function (){
  		$("body").find('.login-block').hide();	
		$("body").find('.forgot-password').show();
  });
    $('.new-pwd').click(function (){
  		$("body").find('.change-password').show();	
		$("body").find('.forgot-password').hide();
  });

  
		
	// Popup Box 
	$('.popup-small-click').click(function (){
		$('.module-true').show();
		$(this).next('.popup-small').show();	
	});
	$('.close, .popup-small .btn-white').click(function (){
		$(this).parent().parent('.popup-small').hide();
		$('.module-true').hide();
	});
	
	// Popup job Details small 
	$('.add-card-click').click(function (){
		$('.module-true').show();
		$(this).next('.popup-small').show();	
	});
	$('.close, .popup-small .btn-white').click(function (){
		$(this).parent().parent('.popup-small').hide();
		$('.module-true').hide();
	});
	
	// Popup Small
	$('.popup-small-click').click(function (){
		$('.module-true').show();
		$(this).next('.popup-section').show();	
	});
	$('.close, .popup-section .btn-white').click(function (){
		$('body').next('.popup-section').hide();
		$('.module-true').hide();
	});
	
	 // Popup Box 
	$('.popup-click').click(function (){
		$('.module-true').show();
		$('body').find('.popup-team').show();	
	});
	$('.close, .popup-section .btn-white').click(function (){
		$('body').find('.popup-team').hide();
		$('.module-true').hide();
	});
	
	 // Popup Job Published 
	$('.published-click').click(function (){
		$('.module-true').show();
		$('body').find('.popup-assign-jobs').show();	
	});
	$('.close, .popup-assign-jobs .btn-white').click(function (){
		$('body').find('.popup-assign-jobs').hide();
		$('.module-true').hide();
	});
	
	// Popup New Fillter
	$('.popup-new-fillter').click(function (){
		$('.module-true').show();
		$('body').find('.popup-add-filter').show();	
	});
	$('.close, .popup-section .btn-white').click(function (){
		$('body').find('.popup-add-filter').hide();
		$('.module-true').hide();
	});
	

	
	

 
   // Mobile Menu 
	var removeClassMobile = true;
	$(".button-mobile-menu").click(function () {
		$("body").toggleClass("menu-open");
		removeClassMobile = false;
	});
	$(".navigation li").click(function() {
		removeClassMobile = false;
	});
	$(".creat-job-block").click(function() {
		removeClassMobile = false;
	});
	$("html").click(function () {
		if (removeClassMobile) {
			$("body").removeClass("menu-open");
		}
		removeClassMobile = true;
	});	
	
	// Calendar	
	$('.dropdown-calendar').click(function(){
		$(this).toggleClass('calendar-active');
		$("body").toggleClass('module-calendar');	
	});
	
	
	// Genie Chat  
	$('.genie-chat-btn').click(function() {
		$("body").toggleClass('module-genie');		
	});
	
	
// Show less 		
	
 	
	$('.toggle-jobs').click(function(){
		$(this).prev('.section-filter-inner').slideToggle('fast').siblings('.section-filter-inner:visible').slideDown('fast');
		$(this).parent().parent().toggleClass('filter-show-arrow');
	});	
	
	

	
	
  
	


	   

	
	// Job Slider		
	$(".filtered-dropdown").click(function () {
		$(this).parent().parent().parent().addClass('filtered-open');
		$('.dropdown-filtered-jobs').show();
		$('.module-true').show();
 
	});
	$(".module-true").click(function () {
		$('.module-true').hide();
		$('.dropdown-filtered-jobs').hide();
		$('body').find('.filtered-open').removeClass('filtered-open');
	});
 	

// Tabs Script		   
	var tabContainers = $('.tab-details-com');
	tabContainers.hide().filter(':first').show();
	$('.tab-bar-com ul li a').click(function () {
	tabContainers.hide();
	tabContainers.filter(this.hash).show();
	$('.tab-bar-com ul li a').parent("li").removeClass('active');
	$(this).parent("li").addClass('active');
	return false;
	}).filter(':first').click();
	

		
	$('.tab-section .tab-bar-com ul li a').click(function () {
		$('body').find('.checkbox-group').css('display','none');
	});
	$('.tab-section .tab-bar-com ul li:first-child a').click(function () {
		$('body').find('.checkbox-group').css('display','block');
	});
	


$('.tab-bar-com ul li a').click(function () {
	var xyz = this.innerHTML.toLowerCase();
	if(xyz == 'day'){
		$('.tasks-header-mid span')[0].innerHTML='WEDNESDAY, 18 JANUARY 2016';
	}else if(xyz == 'week'){
		$('.tasks-header-mid span')[0].innerHTML='WEEK 3, 2016';
	}
	else{
		$('.tasks-header-mid span')[0].innerHTML='JANUARY 2016';
	}
	 
	
});
 // Flip Card 	 
  $(".card-flip").flip({
		trigger: 'manual'
   });
	$(".flip-btn").click(function () {
		$(this).closest(".card-flip").flip(true);
	});
	$(".unflip-btn").click(function () {
		$(this).closest(".card-flip").flip(false);
	});
	

});	


	// Profile Options 
	$(".tasks-header-left .cal-icon").click(function () {
		$(this).parent().addClass('open');
	});
	$(".tasks-calendar .table tr td").click(function () {
		$(this).parent().parent().parent().parent().parent().removeClass('open');
	});
		

	// Profile Options 
 
	var removeClass = true;
	if ($(window).width() <= 1080){		
	// My Profile
	$(".header-right .profile-block").click(function () {
		$("body").toggleClass("show-open");
		removeClass = false;
	});
	}
  	else if ($(window).width() >= 1079){	
		$(".more-click").click(function () {
		$("body").toggleClass("show-open");
		removeClass = false;
	});
	}	
	$(".more-options li").click(function() {
		removeClass = false;
	});
	$("html").click(function () {
		if (removeClass) {
			$("body").removeClass("show-open");
		}
		removeClass = true;
	});
	
	$(".more-options li").click(function() {
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
	});
	
	// Tooltip
 	 $('[data-toggle="tooltip"]').tooltip();   
 	 
	 // CheckBox	 
 	$('.dropdown-form-destop').on('click', function(e){
        if($(this).hasClass('dropdown-style')){
            e.stopPropagation();
        }
	});	
	
 



// Vertical Scrollbox 
	$('.vertical-scrollbox').enscroll({
		verticalTrackClass: 'track',
		verticalHandleClass: 'handle',
		minScrollbarLength: 28
	});
	
// Horizontal Scrollbox 	
	$('.horizontal-scrollbox').enscroll({
		horizontalScrolling: true, 
		horizontalTrackClass: 'horizontal-track2',
		horizontalHandleClass: 'horizontal-handle2',
		minScrollbarLength: 28		 
	});	
	
	
 // JS for Mobile

	
	if ($(window).width() <= 1023){		
	
	
	// Dropdown	filter height fixed  
	$('.dropdown-filter-block li').click(function (){		
		var LiHeightM = $(this).children('div').innerHeight();		
		var parentHeightM = $(this).parent('ul').height();		
		if(parentHeightM > LiHeightM){
			$('.dropdown-common').css({
				'height': 'auto'
				})
		}
		else { $(this).parent('ul').height(LiHeightM); }
	});
	
	
	// sub-dropdown
	$('.sub-dropdown li').click(function (){
		$(this).addClass('actives');	
		$(this).siblings().removeClass('actives');		
	});
		
	// Dropdown	active	
 	$('.dropdown-block li.dropdown').click(function(){
		$(this).siblings().removeClass('active');
		$(this).addClass('active');		
	});
	
	// Filter Tab 1
 
	$(".section-filter-inner ul li .filter-cell").click(function () {
		$(this).parent().addClass("filter-open"); 
		$(this).parent().removeClass('filter-cel-active');
		$(this).parent().siblings().removeClass("filter-open");
		$(this).parent().siblings().addClass('filter-cel-active');
	});
	

 
 

	
	 // CheckBox	 
 	$('ul.dropdown-style').on('click', function(e){
        if($(this).hasClass('dropdown-style')){
            e.stopPropagation();
        }
	});
	 }
	 
	 
 // alt_admin_framework table content popup
  $('.content-click').click(function (){
		$(this).next('.content-style').show();	
  });
  $('.close').click(function (){
	  	$(this).parent('.content-style').hide();
  });


// alt_admin_framework table action popup
  $('.action-more-click').click(function (){
		$(this).next('.action-more-style').show();	
  });
  $('.close').click(function (){
	  	$(this).parent('.action-more-style').hide();
  });



 // alt_one add field popup
  $('.add-click').click(function (){
		$(this).next('.add-style').show();	
  });
  $('.close').click(function (){
	  	$(this).parent('.add-style').hide();
  });

// alt_one add filter text box
$('.filter_textbox').hide();
  $('.filter_btn').click(function (){
      $('.filter_textbox').toggle();
      $('.filter_btn').toggleClass('active');
  });
  

// alt_one filter arrow show
$('.filter_icon').hide();
$('.filter_th').mouseover(function(){
    $(this).children('.filter_icon').show(); 
});

var clickCount = 0;
  $(".clickFilter").click(function(){
      clickCount += 1;
      if(clickCount == 1){
        $(this).addClass('filter_down');
       $(this).removeClass('filter_icon');
         }
      if(clickCount == 2){
        $(this).addClass('filter_up');
       $(this).removeClass('filter_down');
         }
      if(clickCount == 3){
        $(this).addClass('filter_icon');
        $(this).removeClass('filter_up');
          clickCount = 0;
         }
  });  

$('.filter_th').mouseout(function(){
    $(this).children('.filter_icon').hide();
});


// Module actions
  $('.module-action-click').click(function (){
  		$('.module-true').show();
		$(this).next('.module-action-style').show();	
  });
  $('.module-true').click(function (){
	  	$('.module-action-style').hide();
  });


// Module page add menu group & add menu
$('.click_add_menu_group').click(function(){
    $('.add_menu_group').hide();
    $('.add_menu').hide();
    $(this).closest('table').find('.add_menu_group').toggle();
    $(this).closest('table').find('.add_menu').toggle();
});
$('.click_add_menu').click(function(){
    $('.add_menu_group').hide();
    $('.add_menu').hide();
    $(this).closest('table').find('.add_menu').toggle();
});



// form page right sidebar controls & widgets tab
$('.controls_click').click(function(){
    $('.tab_widgets').hide();
    $('.tab_controls').show();
    $('.widgets_click').removeClass('active');
    $('.controls_click').addClass('active');
});
$('.widgets_click').click(function(){
    $('.tab_controls').hide();
    $('.tab_widgets').show();
    $('.controls_click').removeClass('active');
    $('.widgets_click').addClass('active');
});


// form page right sidebar show hide
$('.sidebar_arrow').click(function(){
    $('.right-sidebar-inner').toggle();
    $('.hide_controls').toggle();
    $('.show_controls').toggle();
    $('.fixed-header-right').toggleClass('pos_relative');
    $(this).toggleClass('without-sidebar-arrow');
    $('.with-fixed-sidebar').toggleClass('without-fixed-sidebar');
});
$('.sidebar_arrow').mouseover(function(){
    $('.hide_sidebar_text').css({'opacity' : '1'});
});
$('.sidebar_arrow').mouseout(function(){
    $('.hide_sidebar_text').css({'opacity' : '0'});
});


// form page controls 
$('.input_box, .full_box').click(function(){
    $('.left_panel').find('.selected').removeClass('selected');
    $(this).addClass('selected');
        
    $('.tab_controls').hide();
    $('.tab_widgets').show();
    $('.controls_click').removeClass('active');
    $('.widgets_click').addClass('active');
});

$('.attach_form_click').click(function(){
    $('.alt-module-true').show(200);
});
$('.alt-modal-close').click(function(){
    $('.alt-module-true').hide(200);
});


// form page controls sub tabs
    var subTabContent = $('.sub-tab-content');
    //subTabContent.hide().filter(':first').show();
    
    $('.sub-tabs-head .sub-tab a').click(function(){
        $('.sub-tab-content').siblings().removeClass('open_tab');
        $('.sub-tab-content').siblings().filter(this.hash).addClass('open_tab');
        
        $('.sub-tabs-head .sub-tab a').parent('li').removeClass('active');
        $(this).parent('li').addClass('active');
        return false;
    });
    
    /*$('.sub-tabs-head .sub-tab-all a').click(function(){
        subTabContent.show();
        return false;
    });
*/



// form page fix sidebar
/*
$(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if(scroll >= 120){
        $('.forms_page .right-sidebar').addClass('fixed-sidebar');
    }
    else{
        $('.forms_page .right-sidebar').removeClass('fixed-sidebar', 2000);
    }
});
*/
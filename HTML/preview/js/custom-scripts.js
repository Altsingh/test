$(document).ready(function() {
	//footer fixed or relative
    $(window).scroll(function(){
        if($(this).scrollTop() > 0){
           $(".scroll-top").fadeIn();
            $(".scroll-top").css({'display':'block'})
           }
        else{ $(".scroll-top").fadeOut(); }
    });
     var opentask = false;
    $(".task").click(function(){
        $(".header-right").toggleClass("open-task");
        opentask = false;
    });
   
    $("html").click(function () {
		if (opentask) {
			$(".header-right").removeClass("open-task");
		}
		opentask = true;
	});
    
    
    var quicklinks = false;
    $(".quick-links").click(function(){
        $(".header-right").toggleClass("show-quick-links");
        quicklinks = false;
    });
   
    $("html").click(function () {
		if (quicklinks) {
			$(".header-right").removeClass("show-quick-links");
		}
		quicklinks = true;
	});
    
    
	if($(".layout-main").innerHeight() < $(window).height()){
		$(".alt-footer").addClass("fixed");
	}
	$(window).scroll(function(){
		if($(".layout-main").innerHeight() < $(window).height()){
		$(".alt-footer").addClass("fixed");
	}
	else { $(".alt-footer").removeClass("fixed"); }
	});

    
    

    $(".alt-dropdown").hover(function(){
        $("body").addClass("site-header-fixed");
        $(window).scroll(function(){
        $("body").addClass("site-header-fixed"); 
    });
        var windowHeight = $(window).height();
        var subInnerHeight = $(this).children(".alt-dropdown-menu").innerHeight();
        var siteHeader = $(".site-header-inner").innerHeight();
        var totalHeightNeed = windowHeight - 100;
        if(subInnerHeight >= totalHeightNeed){
            $(this).children(".alt-dropdown-menu").addClass("alt-dropdown-menu-fixed");
            $(this).children('.alt-dropdown-menu').css({'height': totalHeightNeed,});
           
           }
        else{
            $(this).children(".alt-dropdown-menu").removeClass("alt-dropdown-menu-fixed");
            
            
        }
    });
     
    
    
    $(".menu-scroll-up").click(function(){
        var scrollerHeight = $('.alt-menu-scroller').innerHeight() + $('.alt-menu-scroller').offset().top;
        var dropdowBottom = $('.dropdown-scroller-bottom').innerHeight() + $('.dropdown-scroller-bottom').offset().top;
       
        $(".alt-menu-scroller").animate({marginTop: '-=20px'}, 0);
        if(scrollerHeight  < dropdowBottom){
            $(".alt-dropdown-menu-fixed .dropdown-scroller-bottom").fadeOut();
        }
         $(".dropdown-scroller-top").show();
    });
    $(".menu-scroll-down").click(function(){
        if($('.alt-menu-scroller').css("margin-top")!='0px'){
        $(".alt-menu-scroller").animate({marginTop: '+=20px'}, 0);
    }
        else {
            $(".alt-dropdown-menu-fixed .dropdown-scroller-top").fadeOut();
            $(".alt-dropdown-menu-fixed .dropdown-scroller-bottom").show();
        }
    });
    $(".alt-dropdown").mouseleave(function(){
         
    $(window).scroll(function(){
        $("body").removeClass("site-header-fixed"); 
    });
          $(".alt-menu-scroller").animate({marginTop: '0px'}, 0);
         $(".alt-dropdown-menu").removeClass("alt-dropdown-menu-fixed");
     });
	var currentIndex = 0;
	var currentLength = $(".scroller_content li").length;
	$(".previous").attr('disabled','disabled');   
	for(i=1; i<=currentLength; i++){
		$(".scroller_navi ul").append("<li></li>");
	}
	$(".scroller_navi ul li").first().addClass('active');
	$(".next").click(function(event){
		event.preventDefault();
		$(".previous").removeAttr('disabled');
		$(".scroller_content li").eq(currentIndex).hide();
		currentIndex ++;
		$(".scroller_content li").eq(currentIndex).show();
		
		if(currentIndex == currentLength - 1){
			
			$(".scroller_content li").eq(currentIndex).show();
			$(this).attr('disabled','disabled');   
			
		}
		else { 
			$(".scroller_content li").eq(currentIndex).show();
			$(this).removeAttr('disabled');
		}
		$(".scroller_navi ul li").siblings('li').removeClass('active');
		$(".scroller_navi ul li").eq(currentIndex).addClass('active');
		
		
	});
	$(".previous").click(function(event){
		event.preventDefault();
		$(".next").removeAttr('disabled');
		$(".scroller_content li").eq(currentIndex).hide();
		currentIndex --;
		$(".scroller_content li").eq(currentIndex).show();
		if(currentIndex == 0){
			
			$(".scroller_content li").eq(currentIndex).show();
			$(this).attr('disabled','disabled');   
			
		}
		else { 
			$(".scroller_content li").eq(currentIndex).show();
			$(this).removeAttr('disabled');
		}
		$(".scroller_navi ul li").siblings('li').removeClass('active');
		$(".scroller_navi ul li").eq(currentIndex).addClass('active');
		
	});

	// upload file
	$(".file").change(function(){
	$(".fileValue").text($(this).val());
	});
	
	$(".module-true").click(function(){
		$(this).hide();
	});
 	// scroll Top 
	$('.top-btn').click(function(){
		$('html,body').animate({ scrollTop: 0 }, "slow");
	 });


 	// Dropdown	active	
 	$('.dropdown-block li.dropdown').click(function(){
		$(this).addClass('active');
		
	});	
	$('ul.dropdown-menu > li').click(function () {
		$('ul.dropdown-menu > li').removeClass('active');
		$(this).addClass('active');    
	});
		
 	$('.navigation li.dropdown ').click(function(){		
		$('.navigation li.dropdown').removeClass('active');
		$(this).addClass('active');
	});
	
	//  Filter Dropdown
	$('.dropdown-style li').click(function () {
		$('.dropdown-style li').removeClass('active');
		$(this).addClass('active');    
	});
	
	$('.dropdown-filter-block .dropdown-style ul li').click(function () {
		$('.dropdown-filter-block .dropdown-style ul li').removeClass('active');
		$(this).addClass('active');    
	});
	
	// Dailog module	
	var moduleClass = true;
	$(".dropdown").click(function () {
		//$("body").toggleClass("show-module");
		moduleClass = false;
	});
	$(".dropdown li").click(function() {
		moduleClass = true;
	});
	$("html").click(function () {
		if (moduleClass) {
			$("body").removeClass("show-module");
		}
		moduleClass = true;
	});
	
	// Dropdown	7
	$('.dropdown-7 ul.sub-dropdown-7 li').click(function (){
		$(this).addClass('actives');
		
	});


	// Button Start	
	$('.btn-dashboard').hover(function(){
		$("body").toggleClass('btn-dashboard-show');	
		$(this).addClass('btn-dashboard');
	});
  
  // Menu Style
  $('.zoom_card').click(function (){
  		$('.module-true').toggle();
		$(this).parent('.chart_icons').parent('.chart_title').parent('.card-block-inner').toggleClass("card_zoomed");
  });
  $('.menu-click').click(function (){
  		//$('.module-true').show();
		$(this).next('.menu-style').show();	
  });
  $('.close').click(function (){
	  	$(this).parent('.menu-style').hide();
  		$('.module-true').hide();
  });
  
    // Forgot
  $('.forgot-click ').click(function (){
  		$("body").find('.login-block').hide();	
		$("body").find('.forgot-password').show();
  });
    $('.new-pwd').click(function (){
  		$("body").find('.change-password').show();	
		$("body").find('.forgot-password').hide();
  });

  
		
	// Popup Box 
	$('.popup-small-click').click(function (){
		$('.module-true').show();
		$(this).next('.popup-small').show();	
	});
	$('.close, .popup-small .btn-white').click(function (){
		$(this).parent().parent('.popup-small').hide();
		$('.module-true').hide();
	});
	
	// Popup job Details small 
	$('.add-card-click').click(function (){
		$('.module-true').show();
		$(this).next('.popup-small').show();	
	});
	$('.close, .popup-small .btn-white').click(function (){
		$(this).parent().parent('.popup-small').hide();
		$('.module-true').hide();
	});
	
	// Popup Small
	$('.popup-small-click').click(function (){
		$('.module-true').show();
		$(this).next('.popup-section').show();	
	});
	$('.close, .popup-section .btn-white').click(function (){
		$('body').next('.popup-section').hide();
		$('.module-true').hide();
	});
	
	 // Popup Box 
	$('.popup-click').click(function (){
		$('.module-true').show();
		$('body').find('.popup-team').show();	
	});
	$('.close, .popup-section .btn-white').click(function (){
		$('body').find('.popup-team').hide();
		$('.module-true').hide();
	});
	
	 // Popup Job Published 
	$('.published-click').click(function (){
		$('.module-true').show();
		$('body').find('.popup-assign-jobs').show();	
	});
	$('.close, .popup-assign-jobs .btn-white').click(function (){
		$('body').find('.popup-assign-jobs').hide();
		$('.module-true').hide();
	});
	
	// Popup New Fillter
	$('.popup-new-fillter').click(function (){
		$('.module-true').show();
		$('body').find('.popup-add-filter').show();	
	});
	$('.close, .popup-section .btn-white').click(function (){
		$('body').find('.popup-add-filter').hide();
		$('.module-true').hide();
	});
	

	
	

 
   // Mobile Menu 
	var removeClassMobile = true;
	$(".button-mobile-menu").click(function () {
		$("body").toggleClass("menu-open");
		removeClassMobile = false;
	});
	$(".navigation li").click(function() {
		removeClassMobile = false;
	});
	$(".creat-job-block").click(function() {
		removeClassMobile = false;
	});
	$("html").click(function () {
        
		if (removeClassMobile) {
			$("body").removeClass("menu-open");
		}
		removeClassMobile = true;
	});	
	
	// Calendar	
	$('.dropdown-calendar').click(function(){
		$(this).toggleClass('calendar-active');
		$("body").toggleClass('module-calendar');	
	});
	
	
	// Genie Chat  
	$('.genie-chat-btn').click(function() {
		$("body").toggleClass('module-genie');		
	});
	

	
	// Show less 	
 	$('.toggle-jobs').click(function(){
	$(this).next('.toggle-jobs-container').slideToggle('fast').siblings('.toggle-jobs-container:visible').slideDown('fast');
		$(this).toggleClass('active');
		$(this).siblings('.toggle-jobs').removeClass('active');	
	});	   
	
	// Slide Toggle	

	   

	
	// Job Slider	
	
	$(".filtered-dropdown").click(function () {
		$(this).parent().parent().parent().addClass('filtered-open');
		$('.dropdown-filtered-jobs').show();
		$('.module-true').show();
 
	});
	$(".module-true").click(function () {
		$('.module-true').hide();
		$('.dropdown-filtered-jobs').hide();
		$('body').find('.filtered-open').removeClass('filtered-open');
	});
 
	
  
	
	// Profile Options 
 
	var removeClass = true;
	if ($(window).width() <= 1080){		
	// My Profile
	$(".header-right .profile-block").click(function () {
		$("body").toggleClass("show-open");
		removeClass = false;
	});
	}
  	else if ($(window).width() >= 1079){	
		$(".more-click").click(function () {
		$("body").toggleClass("show-open");
		removeClass = false;
	});
	}
	
	$(".more-options li").click(function() {
		removeClass = false;
	});
	$("html").click(function () {
		if (removeClass) {
			$("body").removeClass("show-open");
		}
		removeClass = true;
	});
	
	$(".more-options li").click(function() {
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
	});
	
	// Tooltip
 	 $('[data-toggle="tooltip"]').tooltip();   
 	 
	 // CheckBox	 
 	$('.dropdown-form-destop').on('click', function(e){
        if($(this).hasClass('dropdown-style')){
            e.stopPropagation();
        }
	});
	
	
// Tabs Script		   
	var tabContainers = $('.tab-section > div.tab-details');
	tabContainers.hide().filter(':first').show();
	$('div.tab-section .tab-bar ul li a').click(function () {
	tabContainers.hide();
	tabContainers.filter(this.hash).show();
	$('div.tab-section .tab-bar ul li a').parent("li").removeClass('active');
	$(this).parent("li").addClass('active');
	return false;
	}).filter(':first').click();

 // Flip Card 	 
  $(".card-flip").flip({
		trigger: 'manual'
   });
	$(".flip-btn").click(function () {
		$(this).closest(".card-flip").flip(true);
	});
	$(".unflip-btn").click(function () {
		$(this).closest(".card-flip").flip(false);
	});
	

});	
	
// Vertical Scrollbox 
	$('.vertical-scrollbox').enscroll({
		verticalTrackClass: 'track',
		verticalHandleClass: 'handle',
		minScrollbarLength: 28
	});
	

	
	
 // JS for Mobile

	
	if ($(window).width() <= 1023){		
		
	// Dropdown	active	
 	$('.dropdown-block li.dropdown').click(function(){
		$(this).siblings().removeClass('active');
		$(this).addClass('active');		
	});
	
	// Filter Tab 1
  	var FilterTab1 = true;
	$(".filter-by-block .filter-cell").click(function () {
		$(".filter-by-block").addClass("filter-open");
		FilterTab1 = false;
	});
	$(".filter-by-block").click(function() {
		FilterTab1 = false;
	});
 	$("html").click(function () {
		if (FilterTab1) {
			$(".filter-by-block").removeClass("filter-open");
		}
		FilterTab1 = true;
	});
  // Filter Tab 2
	var FilterTab2 = true;
	$(".sort-by-block .filter-cell").click(function () {
		$(".sort-by-block").addClass("filter-open");
		FilterTab2 = false;
	});
	$(".sort-by-block").click(function() {
		FilterTab2 = false;
	});
 	$("html").click(function () {
		if (FilterTab2) {
			$(".sort-by-block").removeClass("filter-open");
		}
		FilterTab2 = true;
	});
	
	 // CheckBox	 
 	$('ul.dropdown-style').on('click', function(e){
        if($(this).hasClass('dropdown-style')){
            e.stopPropagation();
        }
	});
	 }

function showModal(){
    $(".alt-module-true").fadeIn('slow');
    
}
function hideModal(){
    $(".alt-module-true").fadeOut('slow');
    
}
 
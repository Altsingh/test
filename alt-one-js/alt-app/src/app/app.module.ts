import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ApplicationComponent } from './application/application.component';

import { Routes, RouterModule } from "@angular/router";
import { DataService } from "./services/data.service";
import { ApplicationDataService } from "./services/application/application-data.service";
import { HttpClientModule } from '@angular/common/http';
import { FormComponent } from './application/form/form.component';
import { FormDataService } from "./services/form/form-data.service";
import { Form1DataService } from "./services/form/form1-data.service";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { DynamicComponentsComponent } from './application/form/dynamic-components/dynamic-components.component';
import { ErrorPageComponent } from './application/error-page/error-page.component';
import { routings } from './app.routing';
import { FormPopupComponent } from './application/form-popup/form-popup.component';
import { TasksComponent } from './application/tasks/tasks.component';
import { HomeComponent } from './home/home.component';
import { LogoutComponent } from './logout/logout.component';
import { MeshAppService } from './services/meshapps/meshapp.service';
import { MatSnackBarModule } from '@angular/material';
import { MatAutocompleteModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const appRoutes: Routes = [
  { path: 'application/:appId', component: ApplicationComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    ApplicationComponent,
    FormComponent,
    DynamicComponentsComponent,
    ErrorPageComponent,
    FormPopupComponent,
    TasksComponent,
    HomeComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    routings,
    HttpClientModule,
    FormsModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      appRoutes,
      { useHash: true }
    ),
    ReactiveFormsModule
  ],
  providers: [MeshAppService, DataService, ApplicationDataService, FormDataService, Form1DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LogoutComponent } from "./logout/logout.component";

const routes: Routes = [
    {path: 'home',component: HomeComponent},
    { path: 'logout', component: LogoutComponent},
    {path: 'not-found',loadChildren: './not-found/not-found.module#NotFoundModule'},
    { path: 'securelogin/:appname', loadChildren: './securelogin/securelogin.module#SecureloginModule'}
]

export const routings: ModuleWithProviders = RouterModule.forRoot(routes,{ useHash: true });

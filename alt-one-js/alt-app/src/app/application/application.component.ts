import { Component, ViewChild, OnInit } from '@angular/core';
import { UtilService } from "../utility/util.service";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApplicationDataService } from "../services/application/application-data.service";
import { Application } from "../models/application/application.model";
import { FormComponent } from "./form/form.component";
import { MeshModel } from "../models/application/mesh.model";
import { TaskTO } from '../models/workflow/task.model';
import { FormDataRetrieveRequest } from './form-popup/formdataretrieve.model';
import { DomSanitizer } from '@angular/platform-browser';
import { UserWorkflowInfo } from '../models/workflow/user-workflow-info.model';
import { MeshAppService } from '../services/meshapps/meshapp.service';
import { DataService } from '../services/data.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'alt-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {
  @ViewChild(FormComponent) formComponent: FormComponent;
  application: Application;
  appId: string;
  selectedMenu: string;
  tasksVisible: boolean = false;
  switchApps: boolean = false;
  overlay: boolean = false;
  appLogoUrl: string;
  home: boolean;
  meshModels: MeshModel[];
  userId: string;
  organizationId: string;
  tenantId: string;
  meshFlag: boolean;
  userWorkflowInfo: UserWorkflowInfo = new UserWorkflowInfo();
  responsiveClass: string = 'header-container';
  mobileResponsiveFlag = false;
  activeMenuForm: any;
  private cardUrl: string = environment.cardUrl;

  constructor(private activatedRoute: ActivatedRoute, private applicationDataService: ApplicationDataService,
    private _DomSanitizationService: DomSanitizer, private router: Router, private meshAppService: MeshAppService,
    private dataService: DataService) {
  }

  ngOnInit() {

    this.home = true;
    this.appLogoUrl = localStorage.getItem("productLogo")

    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['appId']) {

        this.appId = UtilService.replaceHashByPercent(params['appId']);
        this.applicationDataService.get(this.appId)
          .subscribe((res: Application) => {
            this.application = res;
            localStorage.setItem("appName", this.application.appName);
            this.application.modules.forEach(module => {
              module.menus.every(menu => {
                if (menu.landingPage) {
                  this.home = false;
                  this.openForm(menu.form);
                  return false;
                }
                return true;
              })
            });
            this.dataService.post(this.cardUrl, this.application.classIds)
              .subscribe((res) => {
                this.userWorkflowInfo = res;
              });
          });
      } else {
        this.appId = "";
      }
    });
    this.getMeshListing();


  }

  getMeshListing() {

    this.userId = atob(localStorage.getItem("entityId"));
    this.organizationId = atob(localStorage.getItem("realm"));
    this.tenantId = localStorage.getItem("tenantId");

    this.applicationDataService.getMeshDetails(this.userId, this.organizationId, this.tenantId)
      .subscribe((res: MeshModel[]) => {
        this.meshModels = res;
        if (this.meshModels.length != 0) {
          this.meshFlag = true;
        }
      });
  }

  /**
   * this method is being implemented remove "unsafe"
   * unsafe:url from the app logo url, bypassing the security
   * trust. Without this app logo won't be displayed
   * 
  */
  photoUrl() {
    if (this.appLogoUrl == null || this.appLogoUrl == 'null')
      return '../assets/images/default.png';
    else
      return this._DomSanitizationService.bypassSecurityTrustUrl(this.appLogoUrl.toString().replace(/['"]+/g, ''));
  }

  openForm(form: any, navigation?: any) {
    this.activeMenuForm = form;
    localStorage.setItem("editFormId", form.formId);
    localStorage.setItem("refrmName", form.formName);

    var formRequest = new FormDataRetrieveRequest();
    formRequest.formId = form.formId;
    formRequest.assignedRoleIds = this.application.employeeInfo.assignedRoleIds;
    formRequest.formRoles = form.assignedRoles;
    formRequest.navigation = navigation;
    this.formComponent.getFormByRequest(formRequest);
    this.formComponent.sysUserWorkflowHistoryId = undefined;
    this.formComponent.instanceId = undefined;
    this.formComponent.employeeId = undefined;
    this.tasksVisible = false;
  }

  openTasks() {
    this.tasksVisible = true;
    this.home = false;
  }

  openTask(task: TaskTO) {
    var formRequest = new FormDataRetrieveRequest();
    formRequest.classid = task.entityId.toString();
    formRequest.instanceId = task.instanceID.toString();
    formRequest.stageId = task.stageId;
    formRequest.assignedRoleIds = this.application.employeeInfo.assignedRoleIds;
    this.formComponent.getFormByRequest(formRequest);
    this.formComponent.sysUserWorkflowHistoryId = task.sysUserWorkflowHistoryId;
    this.formComponent.instanceId = task.instanceID.toString();
    this.formComponent.employeeId = task.employeeId;
    this.tasksVisible = false;
  }

  refreshHome(userWorkflowInfo: UserWorkflowInfo) {
    this.userWorkflowInfo = userWorkflowInfo;
  }

  callApp(url: string, identifier: string, appType: string) {
    localStorage.setItem("appUrl", url);
    localStorage.setItem("customerIdentifier", identifier);
    localStorage.setItem("appType", appType);
    this.meshAppService.loadAppUrl();
  }

  logoutUser() {
    localStorage.clear();
    this.router.navigateByUrl('logout');
  }

  getProfilePic() {
    return localStorage.getItem("profile_pic")
  }

  meshDropHide() {
    this.switchApps = false;
    this.overlay = false;
  }

  showSwicher() {
    this.overlay = !this.overlay;
    this.switchApps = !this.switchApps;
  }

  checkIfLogoutPage() {
    var loggedOut = window.location.href
    if (loggedOut.includes("logout")) {
      return false;
    } else {
      return true;
    }
  }

  changeResponsiveClass() {
    if (this.mobileResponsiveFlag) {
      this.responsiveClass = 'header-container menu-open';
      this.mobileResponsiveFlag = false;
    } else {
      this.responsiveClass = 'header-container';
      this.mobileResponsiveFlag = true;
    }
  }
  getResponsiveClass(): string {
    return this.responsiveClass;
  }

  navigate(navigation: any) {
    var form = undefined;
    for (let module of this.application.modules) {
      for (let menu of module.menus) {
        if (menu.form && menu.form.formId == navigation.formId) {
          form = menu.form;
        }
      }
    }
    this.openForm(form, navigation);
  }
}

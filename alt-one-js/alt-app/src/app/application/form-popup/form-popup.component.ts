import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { FormDataService } from "../../services/form/form-data.service";
import { FormUtilService } from "../../utility/form-util.service";
import { Form1DataService } from '../../services/form/form1-data.service';
import { notifications } from "../../services/static.notifications";
import { FormTO } from '../form/formdata.model';
import { FormCreateResponse } from './form-submit-response.model';
import { FormDataRetrieveRequest } from './formdataretrieve.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-form-popup',
  templateUrl: './form-popup.component.html',
  styleUrls: ['./form-popup.component.css']
})
export class FormPopupComponent implements OnInit {
  @Input() formPopupId: string;
  @Input() instanceId: string;
  @Input() deleteCall: boolean;
  @Input() sysUserWorkflowHistoryId: number;
  @Output() popUpMessageEvent = new EventEmitter();
  @Output() clearPopup = new EventEmitter();
  showPopup: boolean;
  formId: string;
  formIdInEditing: string;
  message: string;
  interval: any;
  form: FormGroup;
  htmlComponents: any[];
  errorPage: boolean;
  formName: string;
  isLogoUploaded: boolean = false;
  @Input() assignedRoleIds: number[];

  constructor(private formDataService: FormDataService,
    private form1DataService: Form1DataService) { }

  ngOnInit() {

  }

  clearPopUp() {
    this.showPopup = undefined;
    this.clearPopup.emit();
    this.form = undefined;
  }

  ngOnChanges() {
    var inEditing: boolean = false;
    if (this.instanceId) {
      if (!this.deleteCall) {
        this.getFormDetailsByInstanceId();
      }
      inEditing = true;
    }
    if (!inEditing && this.formPopupId) {
      var formRequest = new FormDataRetrieveRequest();
      formRequest.formId = this.formPopupId;
      formRequest.assignedRoleIds = this.assignedRoleIds;
      this.getForm(formRequest);
    }

    /**
     * Show & hide pop-up.
     * Delete doesn't require pop-up (requires conform box)
    */
    if (this.formPopupId) {
      this.showPopup = true;
      if (this.deleteCall) {
        this.showPopup = false;
      }
    } else {
      this.showPopup = false;
    }

  }

  getFormDetailsByInstanceId() {
    if (this.sysUserWorkflowHistoryId)
      return;
    var formRequest = new FormDataRetrieveRequest();
    formRequest.instanceId = this.instanceId;
    formRequest.formId = this.formPopupId;
    formRequest.assignedRoleIds = this.assignedRoleIds;
    formRequest.appId = localStorage.getItem("productId");
    formRequest.tenantId = localStorage.getItem("tenantId");
    this.formDataService.getForm(formRequest)
      .subscribe((res: any) => {
        this.htmlComponents = res.componentList;
        this.form = FormUtilService.toFormGroup(this.htmlComponents);
        this.formName = res.uiClassName;
      });
  }

  getForm(formRequest: FormDataRetrieveRequest) {
    this.formId = formRequest.formId;
    formRequest.appId = localStorage.getItem("productId");
    formRequest.tenantId = localStorage.getItem("tenantId");
    if (formRequest.formId) {
      this.errorPage = false;
      this.formDataService.getForm(formRequest)
        .subscribe((res: any) => {
          this.htmlComponents = res.componentList;
          this.formName = res.uiClassName;
          this.form = FormUtilService.toFormGroup(this.htmlComponents);
        });
    } else {
      this.errorPage = true;
    }

  }

  submitForms(value) {
    var components = value.components;
    var form = this.form;
    this.updateValuesRecursively(components, form);
    var formTO = new FormTO;
    formTO.components = components;
    formTO.formId = this.formPopupId;
    formTO.appId = localStorage.getItem("productId");
    formTO.formName = localStorage.getItem("refrmName");
    formTO.sysUserWorkflowHistoryId = this.sysUserWorkflowHistoryId;
    formTO.instanceId = this.instanceId;
    //formTO.employeeId = this.employeeId;
    formTO.appName = localStorage.getItem("appName");
    this.form1DataService.create(formTO)
      .subscribe(res => {
        this.extractFormSubmitResponse(res);
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
            //that.taskCompleted.emit();
          }, 3000);
        })(this);
      })
  }

  extractFormSubmitResponse(res: any): FormCreateResponse {
    let body = res;
    let responseForm = new FormCreateResponse();
    if (body != null) {
      if (body.responseMessage != null) {
        this.message = body.responseMessage;
        this.popUpMessageEvent.emit(body.responseMessage);
      } else {
        this.message = notifications.unexpected_result;
        this.popUpMessageEvent.emit(notifications.unexpected_result);
      }
    }

    return responseForm;
  }

  updateValuesRecursively(components, form) {
    debugger
    components.forEach(component => {
      if (component.controlType == 'PANEL') {
        if (component.componentList.length > 0) {
          this.updateValuesRecursively(component.componentList, form.controls[component.id]);
        }
      } else if (component.controlType == 'TAB_PANEL') {
        component.panels.forEach(pannel => {
          if (pannel.componentList.length > 0) {
            this.updateValuesRecursively(pannel.componentList, form.controls[component.id].controls[pannel.id]);
          }
        });
      } else if (component.controlType == 'IMAGE') {
        if (component.value == "") {
          component.value = environment.defaultusruploadimg;
        }
      }
      if (component.controlType != 'IMAGE' && component.controlType != 'BUTTON' && component.controlType != 'DATATABLE' && form) {
        component.value = form.value[component.id];
      }

    });
  }

  uploadInstanceLogoForm(eventdata: any) {
    if (eventdata.files.length > 0) {
      let file: File = eventdata.files[0];
      let formData: FormData = new FormData();
      formData.append("uploadingFiles", file, file.name);
      if (this.validate(file.size, file.type) == "") {
        this.formDataService.uploadImage(formData).subscribe((res: any) => {
          var applogourl = res;
          eventdata.component.value = applogourl.applogourl;
          this.isLogoUploaded = true;
        });
      } else {
        console.log(this.message);
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
          }, 1500);
        })(this);
      }
    }
  }

  validate(size, fileType): String {
    if (size > 1048576) {
      this.message = "File size is more than 1MB,Can't Upload";
      return this.message;
    }
    if (fileType.indexOf('image') == -1) {
      this.message = "File is not of specifc format";
      return this.message;
    }
    return "";
  }



}

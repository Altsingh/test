/**
 * @author vaibhbav.kashyap
 * 
*/
export class FormCreateResponse {
    success: boolean;
    responseMessage: string;
    cas: String;
    formId: string;
    constructor() {
    }
}
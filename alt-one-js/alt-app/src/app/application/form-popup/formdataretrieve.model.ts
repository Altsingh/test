/**
 * @author vaibhbav.kashyap
 * 
*/
export class FormDataRetrieveRequest {
	appId: string = "";
	formId: string = "";
	formName: string = "";
	userName: string = "";
	classid: string = "";
	instanceId: string = null;
	stageId: number;
	assignedRoleIds: number[];
	formRoles: any[];
	navigation: any;
	tenantId:string;
	constructor() {
	}
}
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { AltBase } from "../../../models/Form/components/alt-base";
import { FormGroup } from "@angular/forms";
import { DataTable } from '../../../models/Form/components/alt-datatable';
import { AltDropdown } from '../../../models/Form/components/alt-dropdown';
import { AltComponent } from '../../../models/Form/components/alt-component';
import { saveAs } from 'file-saver';
import { FilteredDataModel } from './alt-tabledata.model';
import { FilterBase } from "../../../models/Form/filter/filter-base-pojo";
import { ReloadGlobalTextfields } from 'src/app/models/Form/reload-global-textfields.model';
import { FormDataService } from 'src/app/services/form/form-data.service';
import { Case, Rule } from '../../../models/rule/rule.model';
import { ReloadLocalDataTable } from 'src/app/models/Form/reload-local-datatable.model';
import * as ts from "typescript";
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { DownloadDataTable } from 'src/app/models/Form/download-data-table.model';
import { UploadDataTable } from 'src/app/models/Form/upload-data-table.model';
import { AltDate } from '../../../models/Form/components/alt-date';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';
import { environment } from '../../../../environments/environment';
import { FormDataRetrieveRequest } from '../../form-popup/formdataretrieve.model';

@Component({
  selector: 'dynamic-component-loader',
  templateUrl: './dynamic-components.component.html',
  styleUrls: ['./dynamic-components.component.css'],
  host: {
    '(document:click)': 'handleClick($event)'
  }
})
export class DynamicComponentsComponent implements OnInit {

  @Input() component: AltBase<any>;
  @Input() htmlComponents: AltBase<any>[];
  @Input() form: FormGroup;
  @Output() submitFormsEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() photoUploadEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() addDatatableRow = new EventEmitter();
  @Output() editDataTableRow = new EventEmitter();
  @Output() deleteDataTableRow = new EventEmitter();
  filterText: string;
  sortingIndex: number;
  historyData: boolean = false;
  selectedValue: string;
  cardViewPhotoDataMap: Map<string, string>;
  private dataMap: Map<string, boolean>;
  private masterComponentValueMapForRule;
  filteredData: FilteredDataModel[];
  filteredOptions: any[];
  flatHtmlComponents: any;
  @Output() dataTableUploaded = new EventEmitter();
  @Output() navigation = new EventEmitter();
  navigationFlag: boolean;
  activeInstanceForNavigationMenu: string;

  // toast variables
  message: string;
  actionButtonLabel: string;
  action: boolean;
  setAutoHide: boolean;
  autoHide: number;
  horizontalPosition: MatSnackBarHorizontalPosition;
  verticalPosition: MatSnackBarVerticalPosition;
  addExtraClass: boolean;
  config: MatSnackBarConfig;
  filteredSelectOptions: Observable<string[]>;
  @Output() excelUploadEvent: EventEmitter<any> = new EventEmitter<any>();
  displayedExcelColumns: any[];
  @Output() attachmentUploadEvent: EventEmitter<any> = new EventEmitter<any>();

  constructor(private formDataService: FormDataService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    //iniializae snack-bar for toast messages
    this.initializeToast();

    this.historyData = false;
    if (this.component.controlType == "DATATABLE") {
      this.dataMap = new Map<string, boolean>();
      this.dataMap.clear();
      var table = this.component as any;
      this.prepareDataTable(table.data)
      table.filteredData = this.filteredData;
      if (table.cardView/*true*/) {
        this.cardViewPhotoDataMap = new Map<string, string>();
        this.prepareCardViewData(table);
      }
      /***
       * this.history flag is being used for hide/unhide
       * add, edit, delete buttons from datatable when
       * it is a hisstory table
       *
      */
      if (table.historyEnabled != null && !(table.historyEnabled == 'undefined')) {
        this.historyData = !(table.historyEnabled);
      } else {
        this.historyData = false;
      }
      this.component = table;

      var formRequest = new FormDataRetrieveRequest();
      formRequest.formId = table.formPopup;
      formRequest.appId = localStorage.getItem("productId");
      formRequest.tenantId = localStorage.getItem("tenantId");
      this.formDataService.getForm(formRequest)
        .subscribe((res: any) => {
          var jsonResponse = res;
          var referencedFormComponents = this.flattenHtmlComponents(jsonResponse.componentList);
          this.displayedExcelColumns = new Array;
          for (var i = 0; i < table.displayedColumns.length; i++) {
            for (let referencedComponent of referencedFormComponents) {
              if (table.displayedColumns[i] == referencedComponent.name && referencedComponent.controlType == 'EXCEL') {
                this.displayedExcelColumns.push(i);
              }
            }
          }
        });

    }
    let isFilter: boolean;
    isFilter = false;
    if (this.component.controlType == "DROPDOWN") {
      if (this.component.filters && this.component.filters.length > 0) {
        let filterBase: FilterBase = this.component.filters[0];
        if (filterBase.filterField && filterBase.filterField != 'null' && filterBase.filterSourceComponentId && filterBase.filterSourceComponentId != 'null') {
          isFilter = true;

        }
      }
      this.filteredSelectOptions = this.form.get(this.component.id).valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(value))
        );
    }
    if (!isFilter) {
      console.log('setting filtered options for component ' + this.component.name);
      (<AltDropdown>this.component).filteredOptions = (<AltDropdown>this.component).options;
    } else {
      //(<AltDropdown>this.component).filteredOptions = new Array();
    }
    if (this.component.value) {
      this.processFilterOnValue(this.component.value);
    }

    // prepare master data for rule engine - script execution
    this.prepareMasterDataSetForRuleEngine();
  }

  preparePanelMasterDataSetForRuleEngine(panel) {
    var componentList = panel.componentList;
    for (var i = 0; i < componentList.length; i++) {
      var controlType = componentList[i].controlType;
      switch (controlType) {
        case 'PANEL':
          this.preparePanelMasterDataSetForRuleEngine(componentList[i]);
          break;
        case 'DROPDOWN':
          if (componentList[i].filteredOptions) {
            var compDetail = { controlType: componentList[i].controlType, data: componentList[i].filteredOptions };
            this.masterComponentValueMapForRule.push({ label: componentList[i].label, details: compDetail });
          }
          break;
        case 'BUTTON':
          break;
        case 'DATATABLE':
          if (componentList[i].filteredData) {
            var compDetail = { controlType: componentList[i].controlType, data: componentList[i].filteredData };
            this.masterComponentValueMapForRule.push({ label: componentList[i].label, details: compDetail });
          }
          break;
        case 'DATE':
          if (componentList[i].value) {
            var compDetail = { controlType: componentList[i].controlType, data: componentList[i].value };
            this.masterComponentValueMapForRule.push({ label: componentList[i].label, details: compDetail });
          }
          break;
      }
    }
  }

  prepareMasterDataSetForRuleEngine() {
    if (this.htmlComponents) {
      this.masterComponentValueMapForRule = new Array();
      for (var i = 0; i < this.htmlComponents.length; i++) {
        var controlType = this.htmlComponents[i].controlType;
        switch (controlType) {
          case 'PANEL':
            this.preparePanelMasterDataSetForRuleEngine(this.htmlComponents[i]);
            break;
          case 'DROPDOWN':
            var dropDown: AltDropdown = <AltDropdown>this.htmlComponents[i];
            if (dropDown.filteredOptions) {
              var compDetail = { controlType: dropDown.controlType, data: dropDown.filteredOptions };
              this.masterComponentValueMapForRule.push({ label: dropDown.label, details: compDetail });
            }
            break;
          case 'BUTTON':
            break;
          case 'DATATABLE':
            var dataTable: DataTable = <DataTable><AltComponent>this.htmlComponents[i];
            if (dataTable.filteringData) {
              //var compDetail = {controlType:dataTable.controlType,data:dataTable.filteredData};
              //this.masterComponentValueMapForRule.push({label:dataTable.label,details:compDetail});
            }
            break;
          case 'DATE':
            var date: AltDate = <AltDate><AltComponent>this.htmlComponents[i];
            if (date.value) {
              var compDetailDate = { controlType: date.controlType, data: date.value };
              this.masterComponentValueMapForRule.push({ label: date.label, details: compDetailDate });
            }
            break;
        }
      }
      sessionStorage.setItem("MasterMap", JSON.stringify(this.masterComponentValueMapForRule));
    }
  }

  /**
   * This function adds lists which are populated on
   * filter selection. Hence their values needs to be
   * updated in the master map stored in session storage
   * which is utilized for rule script execution.
   * @argument vaibhav.kashyap
   * @function updateMasterMapForFilterKeys
   *
  */
  updateMasterMapForFilterKeys(component, filteredData) {
    var masterMap = sessionStorage.getItem("MasterMap");
    if (masterMap) {
      var map = JSON.parse(sessionStorage.getItem("MasterMap"));
      if (!map.find(e => e.label === component.label)) {
        var compDetail = { controlType: component.controlType, data: filteredData };
        map.push({ label: component.label, details: compDetail });
      } else {
        var obj = map.find(e => e.label === component.label);
        var index = map.indexOf(obj);
        map.splice(index, 1);
        var compDetail = { controlType: component.controlType, data: filteredData };
        map.push({ label: component.label, details: compDetail });
      }
      sessionStorage.setItem("MasterMap", JSON.stringify(map));
    }
  }

  getCompFilterValue() {
    return this.component.value;
		/*if('TEXTFIELD'==this.component.controlType){
			filterValue=((AltTextbox)comp).value;
		}
		if('DROPDOWN'==this.component.controlType){
		}
		if('DATE'==this.component.controlType){
		}*/
  }

  processFilterOnComponent(comp: AltComponent, filterValue, sourceInstanceId: any, form?: FormGroup) {
    let filterComp: FilterBase = comp.filters[0];
    if (comp.controlType == 'DATATABLE') {
      this.filterDataTable(<DataTable>comp, filterComp.filterField, filterValue);
    }
    if (comp.controlType == 'DROPDOWN') {
      this.filterDropdown(<AltDropdown>comp, filterComp.filterField, filterValue, form);
    }


    // AltBase htmlComponent= getHtmlComponent(id);
  }

  processFilterOnGlobalComponent(employeeId: number, globalTextFields: AltBase<string>[]) {
    var attributeNames: string[] = [];
    var componentMap: Map<string, AltBase<string>> = new Map();
    globalTextFields.forEach(textField => {
      attributeNames.push(textField.dbAttrRead);
      componentMap.set(textField.dbAttrRead, textField);
    });
    var reloadRequest = new ReloadGlobalTextfields();
    reloadRequest.objectAttrList = attributeNames;
    reloadRequest.employeeId = employeeId;
    this.formDataService.reloadGlobalTextfields(reloadRequest).subscribe((res: ReloadGlobalTextfields) => {
      for (var i = 0; i < res.objectAttrList.length; i++) {
        var comp = componentMap.get(attributeNames[i]);
        var control = this.form.get(comp.id);
        if (control == null) {
          this.toArray(this.form.controls).every(element => {
            control = element.get(comp.id);
            if (control != null) {
              return false;
            }
            return true;
          });
        }
        control.setValue(res.objectAttrValues[i]);
      }
    });
  }

  toArray(obj) { return Object.keys(obj).map(key => obj[key]) }

  isValidFilterTarget(comp: AltComponent) {
    if (comp.controlType == 'DATATABLE' || comp.controlType == 'DROPDOWN' || comp.controlType == 'TEXTFIELD') {
      return true;
    }
    return false;
  }

  processFilterOnValue(value: any) {
    var sourceInstanceId = value;
    console.log('sourceInstanceId ' + sourceInstanceId);
    let comp1: AltBase<any>;
    for (let comp of this.flattenHtmlComponents(this.htmlComponents)) {
      if (this.isValidFilterTarget(comp) == true) {
        if (comp.filters && comp.filters.length > 0) {
          let filterComp: FilterBase = comp.filters[0];

          if (filterComp.filterSourceComponentId == this.component.id) {
            this.processFilterOnComponent(<AltComponent>comp, this.fetchFilterValueForDropdown(filterComp.filterSourceField, sourceInstanceId), sourceInstanceId);
          }
        }
      }


			/*if(comp.id==id1){
				comp1=comp;
				break;
			}*/
    }
  }


  flattenHtmlComponents(htmlComponents: any) {
    var flatHtmlComponents = [];
    for (let comp of htmlComponents) {
      if (comp.controlType == 'PANEL') {
        var panel = comp as any;
        panel.componentList.forEach(element => {
          flatHtmlComponents.push(element);
        });

      } else {
        flatHtmlComponents.push(comp);
      }
    }
    return flatHtmlComponents;
  }

  processFilter(event: any, form?: FormGroup) {
    var sourceInstanceId = event.currentTarget.value;
    console.log('sourceInstanceId ' + sourceInstanceId);
    var globalTextFields: AltBase<string>[] = [];
    for (let comp of this.flattenHtmlComponents(this.htmlComponents)) {
      if (this.isValidFilterTarget(comp)) {
        if (comp.filters && comp.filters.length > 0) {
          let filterComp: FilterBase = comp.filters[0];

          if (filterComp.filterSourceComponentId == this.component.id) {
            if (this.component.dbClassType == 'Global')
              globalTextFields.push(comp);
            else
              this.processFilterOnComponent(<AltComponent>comp, this.fetchFilterValueForDropdown(filterComp.filterSourceField, sourceInstanceId), sourceInstanceId, form);

          }
        }
      }
    }
    if (globalTextFields.length > 0) {
      this.processFilterOnGlobalComponent(sourceInstanceId, globalTextFields);
    }
  }

  fetchFilterValueForDropdown(filterSourceField: string, sourceInstanceId: string) {
    let dpDown: AltDropdown = (<AltDropdown>this.component);
    let allCols = dpDown.allColumns;
    let i: number;
    i = 0;
    console.log('filterSourceField ' + filterSourceField);
    console.log('sourceInstanceId ' + sourceInstanceId);
    console.log('dpDown.filteringData ' + dpDown.filteringData);
    if (filterSourceField == 'null' || !filterSourceField || filterSourceField == 'select') {
      i = -1;
    } else {
      for (let col of allCols) {
        console.log('col ' + col);
        if (col == filterSourceField) {
          break;
        }
        i++;
      }
    }

    var instanceIDS = Object.keys(dpDown.filteringData);
    //this.filteredData = new Array;
    //var data: FilteredDataModel;
    // for (var instanceId = 0; instanceId < instanceIDS.length; instanceId++) {
    //data = new FilteredDataModel();
    //data.instanceId = parseInt(instanceIDS[instanceId]);
    if (i == -1) {
      console.log('returning instance id for source ' + sourceInstanceId);
      return sourceInstanceId;
    } else {
      let data = dpDown.filteringData[sourceInstanceId];
      return data[i];
    }
    //this.filteredData.push(data);
    //}

    //	dpDown.filteringData.forEach((value: Object, key: string) => {
  }

  prepareCardViewData(table: any) {
    for (var i = 0; i < this.filteredData.length; i++) {
      var valuesArr = this.filteredData[i].rowData;
      for (var j = 0; j < valuesArr.length; j++) {
        if (valuesArr[j]) {
          if (valuesArr[j] != null && valuesArr[j].toString() != '' && (valuesArr[j].toString().includes('png')) || valuesArr[j].toString().includes('jpg') || valuesArr[j].toString().includes('BMP') || valuesArr[j].toString().includes('jpeg') || valuesArr[j].toString().includes('bmp')) {
            this.cardViewPhotoDataMap.set(this.removeDoubleQuotes(this.filteredData[i].instanceId.toString()), this.removeDoubleQuotes(valuesArr[j].toString()));
            break;
          }
        }
      }
    }
  }

  pickCardViewPhoto(instanceIdKey: string) {
    return this.cardViewPhotoDataMap.get(instanceIdKey.toString());
  }

  prepareDataTable(dataReceived) {
    var instanceIDS = Object.keys(dataReceived)
    this.filteredData = new Array;
    var data: FilteredDataModel;
    for (var instanceId = 0; instanceId < instanceIDS.length; instanceId++) {
      data = new FilteredDataModel();
      data.instanceId = parseInt(instanceIDS[instanceId]);
      data.rowData = dataReceived[instanceIDS[instanceId]];
      this.filteredData.push(data);
    }
    this.sortTableDataForRecentOnTop();
  }

  sortTableDataForRecentOnTop() {
    for (var i = 0; i < this.filteredData.length; i++) {
      for (var j = 0; j < this.filteredData.length - i - 1; j++) {
        if (this.filteredData[j].instanceId < this.filteredData[j + 1].instanceId) {
          var temp = this.filteredData[j];
          this.filteredData[j] = this.filteredData[j + 1];
          this.filteredData[j + 1] = temp;
        }
      }
    }
  }

  filter(event) {
    console.log(this.filterText);
    this.filterTable(this.filterText);
  }

  addRow(formid) {
    if (!this.component.security.editable)
      return;
    this.addDatatableRow.emit(formid);
  }

  dynamicSort(sortingIndex: number, sortOrderFlag: boolean) {
    var sortOrder = 1; //ascending order
    if (sortOrderFlag) {
      sortOrder = 1;
    } else {
      sortOrder = -1; //descending order
    }
    return function (a, b) {
      return (('' + a[sortingIndex]).localeCompare(b[sortingIndex])) * sortOrder;
    }
  }

  sortColumn(columndata: any, columnName: string) {
    var dataFetched = columndata.filteredData;

    var instanceIDList = dataFetched.map(function (row) {
      return row.instanceId;
    })

    var rowDataCollective = dataFetched.map(function (row) {
      return row.rowData;
    })

    // adding instance id as last element of every row of table
    for (var i = 0; i < instanceIDList.length; i++) {
      rowDataCollective[i].push(instanceIDList[i]);
    }

    //find which column to sort
    for (var j = 0; j < columndata.headers.length; j++) {
      if (columnName == columndata.headers[j]) {
        this.sortingIndex = j;
        break;
      }
    }
    /**
     * decide the ordering of sorting & also
     * find which column needs to be sorted
     * */
    var sortOrder = false;
    if (this.dataMap.has(columnName)) {
      sortOrder = this.dataMap.get(columnName);
      sortOrder = !sortOrder;
      this.dataMap.set(columnName, sortOrder);
    } else {
      this.dataMap.set(columnName, true);
      sortOrder = true;
    }

    // perform sorting
    rowDataCollective.sort(this.dynamicSort(this.sortingIndex, sortOrder));

    // clear current unsorted data available in filteredData array
    columndata.filteredData = [];

    // reassign new sorted data
    for (var k = 0; k < instanceIDList.length; k++) {
      var arry = rowDataCollective[k];
      var index = arry[arry.length - 1]
      /***
       * remove instanceID which was assigned on the last index of the
       * row data
      */
      arry.splice(arry.length - 1, 1);
      var obj = new FilteredDataModel();
      obj.instanceId = index;
      obj.rowData = arry;
      columndata.filteredData.push(obj);
    }
  }

  downloadFile(data: any) {
    var headers = data.headers;
    if (data.headers == null || data.headers == 'undefined')
      headers = data.displayedColumns;
    //fetch table data
    var csv = data.data
    if (csv == null || csv == 'undefined')
      return;

    //Convert object to array
    var result = Object.keys(csv).map(function (key) {
      return [csv[key]];
    });
    result.reverse();
    // add header column
    result.unshift(headers.join(','))
    var str = result.join('\r\n');
    var blob = new Blob([str], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
    saveAs(blob, "report.csv");
  }

  editRow(instanceid, formid) {
    this.editDataTableRow.emit(instanceid + "#" + formid);
  }

  deleteRow(instanceid, formid) {
    this.deleteDataTableRow.emit(instanceid + "#" + formid + "#" + true);
  }

  filterTable(filterValue) {
    console.log(this.component);
    var table = this.component as any;
    var filteredData: { [k: string]: any } = {};
    //var data = table.data as Object;

    for (var key in table.data) {
      for (var i = 0; i < table.data[key].length; i++)
        if (table.data[key][i] && table.data[key][i].toLowerCase().includes(filterValue.toLowerCase())) {
          filteredData[key] = table.data[key];
          break;
        }
    }
    table.filteredData = filteredData;
    // this is done due to new structuring on data table
    this.prepareDataTable(table.filteredData)
    table.filteredData = this.filteredData;
    this.component = table;
  }

  getInstanceIDs(map) {
    return Object.keys(map);
  }

  getValues(valuesArray) {
    return Array.from(valuesArray.rowData);
  }

  submitForms(components, form) {
    var c = this.component as any;
    if (c.validationsOnly) {
      return;
    }
    if (this.navigationFlag)
      return;
      setTimeout(() => {if (form) {
              this.submitFormsEvent.emit({ components, form });
            } else {
              this.submitFormsEvent.emit(components);
            }
    }, 1000);
    
  }

  openFormInEditingMode($event) {
    this.editDataTableRow.emit($event);
  }

  openFormInDeleteMode($event) {
    this.deleteDataTableRow.emit($event);
  }

  openFormDialog($event) {
    this.addDatatableRow.emit($event);
  }

  uploadInstanceLogoForm($event) {
    this.photoUploadEvent.emit($event);
  }

  navigate(navigation: any, instanceid?: any) {
    if (instanceid) {
      var table = <DataTable>(this.component as any);
      for (var i = 0; i < table.displayedColumns.length; i++) {
        if (navigation.sourceComponentName == table.displayedColumns[i]) {
          navigation.sourceComponentValue = instanceid.rowData[i];
          break;
        }
      }
    } else {
      for (let comp of this.flattenHtmlComponents(this.htmlComponents)) {
        if (navigation.sourceComponentName == comp.name) {
          var control = this.form.get(comp.id)
          navigation.sourceComponentValue = control.value;
          break;
        }
      }
    }
    this.navigation.emit(navigation);
  }

  buttonClicked(component: any) {
    if (component.navigation && component.navigation.formId) {
      this.navigationFlag = true;
      this.navigate(component.navigation);
    } else {
      component.clicked = true;
    }
  }

  uploadInstanceLogo(event, component) {
    this.photoUploadEvent.emit({ files: event.target.files, component: component });
  }

  getUploadedPhoto(comp: any) {
    return comp.value
  }

  removeDoubleQuotes(stringData) {
    if (stringData == null || stringData == 'undefined' || stringData == '') {
      return undefined
    }
    return stringData.replace(/['"]+/g, '')
  }

  determineSelection(component, option) {
    return component.values == null ? false : component.values.includes(option.value);
  }
  filterDataTable(table1: DataTable, filterKey, filterValue) {
    console.log(table1);
    console.log('filterKey ' + filterKey);
    console.log('filterValue ' + filterValue);
    var table = table1 as any;
    var filteredData: { [k: string]: any } = {};
    //var data = table.data as Object;

    var instanceIDS = Object.keys(table1.filteringData)
    this.filteredData = new Array;
    var data: FilteredDataModel;
    let i: number = this.fetchDatatableIndex(table1.allColumns, filterKey);
    console.log('i ' + i);
    table.filteredData = {};
    for (var instanceId = 0; instanceId < instanceIDS.length; instanceId++) {
      let rowData1: any[] = table1.filteringData[instanceIDS[instanceId]];
      console.log('rowdata i ' + rowData1[i]);
      if (rowData1[i] == filterValue || filterValue == instanceIDS[instanceId]) {
        data = new FilteredDataModel();
        data.instanceId = parseInt(instanceIDS[instanceId]);

        data.rowData = table1.data[instanceIDS[instanceId]];

        this.filteredData.push(data);
      }

    }

    /*for(let comp of this.htmlComponents){
      if(comp.id==table1.id){

      }
    }*/
    table.filteredData = this.filteredData;
    //this.component = table;

    /*
        //for (var key in table.data) {
          for (var i = 0; i < table.filteringData[filterKey].length; i++)
            if (table.data[filterKey][i].toLowerCase().includes(filterValue.toLowerCase())) {
              filteredData[filterKey] = table.data[filterKey];
             // break;
           }
        //}
        table.filteredData = this.filteredData;
        // this is done due to new structuring on data table
        this.prepareDataTable(table.filteredData)
        table.filteredData = this.filteredData;
        this.component = table;*/
  }

  filterDropdown(dpdown1: AltDropdown, filterKey, filterValue, form?: FormGroup) {
    // console.log(table1);
    console.log('filterKey ' + filterKey);
    console.log('filterValue ' + filterValue);

    var instanceIDS = Object.keys(dpdown1.filteringData);


    let i: number = this.fetchDatatableIndex(dpdown1.allColumns, filterKey);

    let instances: number[] = new Array;

    for (var instanceId = 0; instanceId < instanceIDS.length; instanceId++) {
      let rowData1: any[] = dpdown1.filteringData[instanceIDS[instanceId]];

      if (rowData1[i] == filterValue) {
        instances.push(parseInt(instanceIDS[instanceId]));
        console.log('pushed instance ' + instanceIDS[instanceId]);
      }
    }
    //dpdown1.filteredOptions={};
    this.filteredOptions = new Array;
    for (i = 0; i < dpdown1.options.length; i++) {
      let option1: any = dpdown1.options[i];
      //}
      // for(let option1 in dpdown1.options){
      console.log('option1.value ' + option1.value);
      if (this.isPresentInArray(option1.value, instances) == true) {
        console.log('pushed in filtered options');
        this.filteredOptions.push(option1);
      }
    }
    dpdown1.filteredOptions = this.filteredOptions;
    //form.get(dpdown1.id).setValue('');
    //form.get(dpdown1.id).setValue(null);

    /**
     *  This will be called for preparing metadata required for
     *  rule editor's script execution.
    */
    this.updateMasterMapForFilterKeys(dpdown1, dpdown1.filteredOptions);

    /*data = new FilteredDataModel();
  data.instanceId = parseInt(instanceIDS[instanceId]);

  data.rowData = options[instanceId]];

  this.filteredData.push(data);*/


  }

  isPresentInArray(value2, values: any[]) {
    console.log('value2 ' + value2)
    for (let value1 in values) {
      console.log('values[value1] ' + values[value1]);
      if (values[value1] == value2) {
        return true;
      }
    }
    return false;
  }


  fetchDatatableIndex(allColumns: string[], column: string) {
    let i: number;
    for (i = 0; i < allColumns.length; i++) {
      let col1: string = allColumns[i];
      if (column == col1) {
        break;
      }
    }
    return i;
  }

  processSelection(event: any, component: any) {
    var options = event.currentTarget.options;
    component.values = [];
    for (var i = 0; i < options.length; i++) {
      if (options[i].selected && options[i].value != 'Select') {
        component.values.push(options[i].value);
      }
    }
  }

  /**
   * Custom event registering, will be called
   * if rule script will be triggered on the same.
   * @author vaibhav.kashyap
  */

  buttonClickEvent(component, event) {
    console.log("custom buttom click event called");
    this.ruleEditorScriptExecutor(component, event);
  }

  dropDownChangedEvent(component, event) {
    console.log("drop down changed")
    this.ruleEditorScriptExecutor(component, event);
  }

  dateChangedEvent(component, evnt) {
    this.ruleEditorScriptExecutor(component, event);
  }

  ruleEditorScriptExecutor(component, event) {
    var eventFound: boolean = false
    var outMap = [];
    if (component.ruleObjects) {
      for (var i = 0; i < component.ruleObjects.length; i++) {
        var r: Rule = component.ruleObjects[i];
        if (r && r.cases) {
          for (var j = 0; j < r.cases.length; j++) {
            var c: Case = r.cases[j];
            if ((c.event == 'change' || c.event == 'click') && c.scriptData) {
              //eventFound = true;
              let code: string = `(
                {
                Run:
                runMe();
                }
                function runMe() {
                  `+ c.scriptData + `
                }
                )`;
              let result = ts.transpile(code);
              eval(result);
              setTimeout(() => {
                var scriptTime  = sessionStorage.getItem("scriptTime");
                if(!scriptTime){
                  scriptTime='1000';
                }
                setTimeout(() => this.reassignValuesAfterRuleScriptExecution(), +scriptTime);
              }, 500);
              

              //clear taoast messages and actions
              this.message = "";
              this.actionButtonLabel = "";
              break;
            }
          }
        }
      }
    }
  }

  reassignValuesAfterRuleScriptExecution() {
    // call to alert messages for user to take actions if required
    this.callToastIfRequiredWithMessages();

    var outMap = sessionStorage.getItem("outMap");
    if (outMap) {
      var outMapParsed = JSON.parse(outMap);
      for (var j = 0; j < this.htmlComponents.length; j++) {
        if (this.htmlComponents[j].controlType == 'PANEL') {
          var innerComp: any = this.htmlComponents[j];
          var compList = innerComp.componentList;
          for (var k = 0; k < outMapParsed.length; k++) {
            for (var l = 0; l < compList.length; l++) {
              if (outMapParsed[k].label == compList[l].label) {
                //use switch case for different type of components
                switch (compList[l].controlType) {
                  case 'DROPDOWN':
                    compList[l].filteredOptions = outMapParsed[k].details.data;
                    break;
                  case 'BUTTON':
                    break;
                  case 'DATATABLE':
                    compList[l] = JSON.parse(outMapParsed[0].details.data).table;
                    break;
                  case 'DATE':
                    compList[l].value = outMapParsed[k].details.data;
                    break;
                  case 'TEXTFIELD':
                    compList[l].value = outMapParsed[k].details.data;
                    break;

                }
              }
            }
          }

        } else {
          //use switch case for different type of components
        }
      }
    }
  }


  processOwner(event: any) {
    var sourceInstanceId = event.currentTarget.value;
    var datatableFields: any[] = [];
    if (this.component.dbClassType == 'Global') {
      for (let comp of this.flattenHtmlComponents(this.htmlComponents)) {
        if (comp.controlType == 'DATATABLE') {
          if (comp.matchOwnerId == this.component.id && comp.dbClassType == 'Local') {
            datatableFields.push(comp);
          }
        }
      }
    }
    if (datatableFields.length == 0)
      return;
    var reloadLocalDataTable = new ReloadLocalDataTable();
    reloadLocalDataTable.tables = datatableFields;
    reloadLocalDataTable.ownerEmployeeId = sourceInstanceId;
    this.formDataService.reloadLocalDataTable(reloadLocalDataTable).subscribe((res: ReloadLocalDataTable) => {
      for (var i = 0; i < res.tables.length; i++) {
        var dataReceived = res.tables[i].data;
        var instanceIDS = Object.keys(dataReceived)
        var filteredData = new Array;
        var data: FilteredDataModel;
        for (var instanceId = 0; instanceId < instanceIDS.length; instanceId++) {
          data = new FilteredDataModel();
          data.instanceId = parseInt(instanceIDS[instanceId]);
          data.rowData = dataReceived[instanceIDS[instanceId]];
          filteredData.push(data);
        }
        datatableFields[i].filteredData = filteredData;
      }
    });
  }

  downloadDataTable(component: DataTable) {
    var downloadRequest = new DownloadDataTable();
    downloadRequest.tenantId = localStorage.getItem("tenantId");
    downloadRequest.formRoles = null;
    downloadRequest.table = component;
    this.formDataService.downloadDataTable(downloadRequest).subscribe((download: DownloadDataTable) => {
      var byteString = atob(download.fileData);
      var byteCharacters = new Array(byteString.length);
      for (let i = 0; i < byteString.length; i++) {
        byteCharacters[i] = byteString.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteCharacters);
      var blob = new Blob([byteArray]);
      saveAs(blob, download.fileName);
    });
  }

  selectFileForUploadDataTable(component: DataTable) {
    var fileInput = document.getElementById('template-upload_' + component.id);
    fileInput.click();
  }

  uploadDataTable(event: any, component: DataTable) {
    var file: File = event.target.files[0];
    var reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = () => {
      const byteArray = new Uint8Array(reader.result as any);
      var byteString: string = "";
      for (var i = 0; i < byteArray.length; i++) {
        byteString += String.fromCharCode(byteArray[i]);
      }
      var fileDataBase64 = btoa(byteString);

      var uploadRequest = new UploadDataTable();
      uploadRequest.table = component;
      uploadRequest.fileDataBase64 = fileDataBase64;
      this.formDataService.uploadDataTable(uploadRequest).subscribe((upload: UploadDataTable) => {
        this.dataTableUploaded.emit();
      });
    }
  }

  handleClick(event) {
    if (event.target.innerHTML != "more_horiz") {
      this.activeInstanceForNavigationMenu = undefined;
    }
  }

  callToastIfRequiredWithMessages() {
    var toastMapRecieved = sessionStorage.getItem("toastMap");
    if (toastMapRecieved) {
      var toastMapRecievedParsed = JSON.parse(toastMapRecieved);
      for (var i = 0; i < toastMapRecievedParsed.length; i++) {
        if (toastMapRecievedParsed[i].message) {
          this.message = toastMapRecievedParsed[i].message;
        }
        if (toastMapRecievedParsed[i].actionButtonLabel) {
          this.actionButtonLabel = toastMapRecievedParsed[i].actionButtonLabel;
        }
      }

      if (this.message && this.actionButtonLabel) {
        this.snackBar.open(this.message, this.action ? this.actionButtonLabel : undefined, this.config);
        sessionStorage.removeItem("toastMap");
      }
    }

  }

  initializeToast() {
    this.action = true;
    this.setAutoHide = true;
    this.autoHide = 2000;
    this.horizontalPosition = 'center';;
    this.verticalPosition = 'top';
    this.addExtraClass = false;

    this.config = new MatSnackBarConfig();
    this.config.verticalPosition = this.verticalPosition;
    this.config.horizontalPosition = this.horizontalPosition;
    this.config.duration = this.setAutoHide ? this.autoHide : 0;

    sessionStorage.setItem("ApiUrlPrefix", environment.url);
  }

  private _filter(value: any): string[] {
    var filterValue = null;
    if (value.label)
      filterValue = value.label;
    else
      filterValue = value.toLowerCase();
    var castedComponent = this.component as any;
    return castedComponent.filteredOptions.filter(option => option.label.toLowerCase().includes(filterValue));
  }

  displayOptionsFn(value: any): string | undefined {
    if (value == '')
      return;
    var castedComponent = this.component as any;
    for (let option of castedComponent.filteredOptions) {
      if (option.value == value) {
        return option.label;
      }
    }
    return undefined;
  }

  uploadExcel($event, component) {
    this.excelUploadEvent.emit({ event: $event, component: component });
  }

  resetValue(event: any) {
    event.target.value = '';
  }

  saveFileAs(filePath: string, fileName: string) {
    var url = environment.formUrl + "/forms/downloadFile?filePath=" + encodeURIComponent(filePath);
    saveAs(url, fileName);
  }

  getFileName(filePath: string) {
    return filePath.substring(filePath.lastIndexOf('/') + 1, filePath.length);
  }

  uploadAttachment($event, component) {
    this.attachmentUploadEvent.emit({ event: $event, component: component });
  }

  getFileSize(fileSize: number) {
    if (fileSize < 1024)
      return fileSize + 'B'
    else if (fileSize < 1014 * 1024)
      return (fileSize - fileSize % 1024) / 1024 + 'K';
    else
      return (fileSize / (1024 * 1024)) + 'M';
  }

  removeAttachment(files, index) {
    files.splice(index, 1);
  }

  parseJSON(json: any) {
    return JSON.parse(json);
  }

}

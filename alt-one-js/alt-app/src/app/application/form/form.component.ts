import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { FormDataService } from "../../services/form/form-data.service";
import { FormUtilService } from "../../utility/form-util.service";
import { Form1DataService } from '../../services/form/form1-data.service';
import { notifications } from "../../services/static.notifications";
import { FormTO } from './formdata.model';
import { FormCreateResponse } from '../form-popup/form-submit-response.model';
import { FormDataRetrieveRequest } from '../form-popup/formdataretrieve.model';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'my-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  formPopupId: string;
  instanceId: string;
  deleteCall: boolean;
  editCall: boolean;
  formId: string;
  message: string;
  interval: any;
  form: FormGroup;
  htmlComponents: any[];
  errorPage: boolean;
  sysUserWorkflowHistoryId: number;
  @Output() taskCompleted = new EventEmitter();
  employeeId: number;
  @Input() assignedRoleIds: number[];
  uploadedURL1: string;
  isLogoUploaded: boolean = false;
  @Output() dataTableUploaded = new EventEmitter();
  @Output() navigation = new EventEmitter();

  constructor(private formDataService: FormDataService,
    private form1DataService: Form1DataService) { }

  ngOnInit() {
    this.message = undefined;
  }

  setPopupMessage(message) {
    this.message = message;
    this.getDetailsAfterFormUpdate();
    setTimeout(() => {
      this.message = "";
    }, 3000);
    this.formPopupId = undefined;
  }

  getDetailsAfterFormUpdate() {
    var formRequest = new FormDataRetrieveRequest();
    formRequest.formId = localStorage.getItem("editFormId");
    formRequest.assignedRoleIds = this.assignedRoleIds;
    this.getFormByRequest(formRequest);
  }

  openFormDialog(id) {
    this.formPopupId = id;
  }

  openFormInEditingMode(instanceIdAndFormId) {
    this.enableDisableEditCall(true);
    this.enableDisbaleDeleteCall(false);
    this.setOperatingVariables(instanceIdAndFormId);
  }

  enableDisbaleDeleteCall(flag) {
    this.deleteCall = flag;
  }

  enableDisableEditCall(flag) {
    this.editCall = flag;
  }

  /**
   * Operating varibales contains
   * instanceId=Array[0];
   * formPopupId=Array[1];
   * deleteCall=Array[2];
   * 
  */
  setOperatingVariables(operatingVaribales) {
    let valuesRetrieved = operatingVaribales.split("#");
    var lengthArry = valuesRetrieved.length;
    switch (lengthArry) {
      case (2):
        this.instanceId = valuesRetrieved[0];
        this.formPopupId = valuesRetrieved[1];
        break;
      case (3):
        this.instanceId = valuesRetrieved[0];
        //this.formPopupId = valuesRetrieved[1];
        this.deleteCall = valuesRetrieved[2];
        break;
    }

  }

  openFormInDeleteMode(instanceIdAndFormIdAndDeleteCall) {
    this.enableDisableEditCall(false);
    this.setOperatingVariables(instanceIdAndFormIdAndDeleteCall);
    this.deleteSelectedInstance();
  }

  deleteSelectedInstance() {
    this.formDataService.deleteInstance(this.instanceId)
      .subscribe(res => {
        var formRequest = new FormDataRetrieveRequest();
        formRequest.formId = localStorage.getItem("editFormId");
        formRequest.assignedRoleIds = this.assignedRoleIds;
        this.getFormByRequest(formRequest);
        this.clearPopup(null);
      });
  }

  clearPopup(event) {
    this.formPopupId = undefined;
    this.instanceId = undefined;
    this.deleteCall = undefined;
  }

  getFormByRequest(request: FormDataRetrieveRequest) {
    if (request.formId || request.classid) {
      if (request.formRoles) {
        sessionStorage.setItem("formRoles", JSON.stringify(request.formRoles));
      }
      this.errorPage = false;
      request.appId = localStorage.getItem("productId");
      request.tenantId = localStorage.getItem("tenantId");
      this.formDataService.getForm(request)
        .subscribe((res: any) => {
          var jsonResponse = res;
          this.htmlComponents = jsonResponse.componentList;
          this.form = FormUtilService.toFormGroup(this.htmlComponents);
          this.formId = jsonResponse.uiClassCode;
        });
    } else {
      this.errorPage = true;
    }
  }

  submitForms(value) {
    debugger
    var components = value.components;
    var form = this.form;
    this.updateValuesRecursively(components, form);
    var formTO = new FormTO;
    formTO.components = components;
    formTO.formId = this.formId;
    formTO.appId = localStorage.getItem("productId");
    formTO.formName = localStorage.getItem("refrmName");
    formTO.sysUserWorkflowHistoryId = this.sysUserWorkflowHistoryId;
    formTO.instanceId = this.instanceId;
    formTO.employeeId = this.employeeId;
    formTO.appName = localStorage.getItem("appName");
    this.form1DataService.create(formTO)
      .subscribe(res => {
        //remove outMap data created from script
        sessionStorage.removeItem("outMap");

        this.extractFormSubmitResponse(res);
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
            that.taskCompleted.emit();
          }, 3000);
        })(this);
      })
  }

  extractFormSubmitResponse(res: any): FormCreateResponse {
    let body = res;
    this.instanceId = undefined;
    let responseForm = new FormCreateResponse();
    if (body != null) {
      if (body.responseMessage != null) {
        this.message = body.responseMessage;
      } else {
        this.message = notifications.unexpected_result;
      }
    }

    return responseForm;
  }

  updateValuesRecursively(components, form) {
    components.forEach(component => {
      if (component.controlType == 'PANEL') {
        if (component.componentList.length > 0) {
          this.updateValuesRecursively(component.componentList, form.controls[component.id]);
        }
      } else if (component.controlType == 'TAB_PANEL') {
        component.panels.forEach(pannel => {
          if (pannel.componentList.length > 0) {
            this.updateValuesRecursively(pannel.componentList, form.controls[component.id].controls[pannel.id]);
          }
        });
      } else if (component.controlType == 'IMAGE') {
        if (component.value == "") {
          component.value = environment.defaultusruploadimg;
        }
      }
      if (component.controlType != 'IMAGE' && component.controlType != 'BUTTON' && component.controlType != 'DATATABLE' && form && (form.value[component.id])) {
        component.value = form.value[component.id];
      }

    });
  }

  uploadInstanceLogoForm(eventdata) {
    if (eventdata.files.length > 0) {
      let file: File = eventdata.files[0];
      let formData: FormData = new FormData();
      formData.append("uploadingFiles", file, file.name);
      if (this.validate(file.size, file.type) == "") {
        this.formDataService.uploadImage(formData).subscribe((res: any) => {
          var applogourl = res;
          eventdata.component.value = applogourl.applogourl;
          this.isLogoUploaded = true;
        });
      } else {
        console.log(this.message);
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
          }, 1500);
        })(this);
      }
    }
  }

  validate(size: any, fileType: any): String {
    if (size > 1048576) {
      this.message = "File size is more than 1MB,Can't Upload";
      return this.message;
    }
    if (fileType.indexOf('image') == -1) {
      this.message = "File is not of specifc format";
      return this.message;
    }
    return "";
  }

  reloadMenu() {
    this.dataTableUploaded.emit();
  }

  navigate(event: any) {
    this.navigation.emit(event);
  }

  uploadExcelForm(eventdata) {
    if (eventdata.event.target.files.length > 0) {
      let file: File = eventdata.event.target.files[0];
      let formData: FormData = new FormData();
      formData.append("uploadingFiles", file, file.name);
      if (this.validateExcel(file.size, file.type) == "") {
        this.formDataService.uploadFile(formData).subscribe((res: any) => {
          eventdata.component.dataFileName = res.dataFileName;
          eventdata.component.dataFilePath = res.dataFilePath;
        });
      } else {
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
          }, 1500);
        })(this);
      }
    }
  }

  validateExcel(size: any, fileType: any): String {
    if (size > 1048576) {
      this.message = "File size is more than 1MB,Can't Upload";
      return this.message;
    }
    if (fileType != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      this.message = "File is not of specifc format";
      return this.message;
    }
    return "";
  }

  uploadAttachmentForm(eventdata) {
    var event = eventdata.event;
    var component = eventdata.component;
    if (event.target.files.length > 0) {
      let file: File = event.target.files[0];
      let formData: FormData = new FormData();
      formData.append("uploadingFiles", file, file.name);
      if (this.validateAttachment(file, component) == "") {
        this.formDataService.uploadFile(formData).subscribe((res: any) => {
          var fileDetail = {} as any;
          fileDetail.fileName = res.dataFileName;
          fileDetail.filePath = res.dataFilePath;
          fileDetail.fileSizeBytes = file.size;
          if (!component.files) {
            component.files = [];
          }
          component.files.push(fileDetail);
        });
      } else {
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
          }, 1500);
        })(this);
      }
    }
  }

  validateAttachment(file: File, component: any): String {
    var extension = file.name.substring(file.name.lastIndexOf(".") + 1, file.name.length);
    var blockedFileTypes = ["ADE", "ADP", "APK", "BAT", "CHM", "CMD", "COM", "CPL", "DLL", "DMG", "EXE",
      "HTA", "INS", "ISP", "ISO", "JAR", "JS", "JSE", "LIB", "LNK", "MDE", "MSC", "MSI", "MSP", "MST",
      "NSH", "PIF", "SCR", "SCT", "SHB", "SYS", "VB", "VBE", "VBS", "VXD", "WSC", "WSF", "WSH", "CAB"];
    if (blockedFileTypes.includes(extension.toUpperCase())) {
      this.message = "File is not of allowed format";
      return this.message;
    }
    var totalSize = 0;
    if (component.files) {
      for (let file of component.files) {
        totalSize += file.fileSizeBytes;
      }
    }
    totalSize += file.size;
    if (totalSize > (component.totalFileSizeLimitMB * 1024 * 1024)) {
      this.message = "Total fize size limit is " + component.totalFileSizeLimitMB + "MB";
      return this.message;
    }
    return "";
  }
}

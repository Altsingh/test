/**
 * @author vaibhbav.kashyap
 * 
*/
export class FormTO {

    components: any;
    formId: string;
    appId: string;
    formName: string;
    instanceId: string;
    sysUserWorkflowHistoryId: number;
    employeeId: number;
    appName: string;

    constructor() {
    }
}
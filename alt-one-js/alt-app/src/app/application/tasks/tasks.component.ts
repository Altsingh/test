import { Component, Input, Output, EventEmitter } from "@angular/core";
import { TaskTO } from "../../models/workflow/task.model";

@Component({
  selector: 'my-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})

export class TasksComponent {
  @Input() tasks: TaskTO[];
  @Output() taskOpenedEvent = new EventEmitter<TaskTO>();

  openTask(task: TaskTO) {
    this.taskOpenedEvent.emit(task);
  }
}

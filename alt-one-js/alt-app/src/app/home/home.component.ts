import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { DataService } from '../services/data.service';
import { environment } from '../../environments/environment';
import { Application } from '../models/application/application.model';
import { UserWorkflowInfo } from '../models/workflow/user-workflow-info.model';
import { WorkflowCardTO } from '../models/workflow/workflow-card.model';
import { DatePipe } from '@angular/common';
import { HomeCardModel } from './homecard.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit, OnChanges {
  private cardUrl: string = environment.cardUrl;
  @Input() application: Application;
  workFlowInfo: UserWorkflowInfo;
  cards: WorkflowCardTO[];
  appliedCardFront: boolean = true;
  approvalCardFront: boolean = true;
  myDate: string;
  @Output() tasksFetchedEvent = new EventEmitter<UserWorkflowInfo>();
  @Output() openTasks = new EventEmitter();
  homeCardsPrefilled: HomeCardModel[];
  public cardFlipDisplay: Map<string, boolean>;

  constructor(private dataService: DataService,
    private datePipe: DatePipe) {
    this.myDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  }

  ngOnInit() {
    this.cardFlipDisplay = null;
  }

  ngOnChanges() {
    if (this.application) {
      this.dataService.post(this.cardUrl, this.application.classIds)
        .subscribe((res) => {
          this.workFlowInfo = res;
          this.cards = this.workFlowInfo.cards;
          if(this.cards){
            this.cardFlipDisplay = new Map<string, boolean>()
            this.generateIdForCards()
          }
          this.tasksFetchedEvent.emit(this.workFlowInfo);
          localStorage.setItem("profile_pic", this.application.employeeInfo.photoPath)
        });

      if (this.application.homeCards != null && this.application.homeCards.length > 0) {
        this.prepareHomeCardsPrefilled();
      }
    }
  }

  generateIdForCards(){
    for(var j=0; j<this.cards.length; j++){
      this.cardFlipDisplay.set(this.cards[j].formName,true);
    }
  }

  changeFlipStatusForFlip(card: any){
    if(this.cardFlipDisplay.get(card.formName)){
      this.cardFlipDisplay.set(card.formName,false);
    }else{
      this.cardFlipDisplay.set(card.formName,true);
    }

  }

  getCardFlipStatus(card: any){
    return this.cardFlipDisplay.get(card.formName)
  }

  prepareHomeCardsPrefilled() {
    this.homeCardsPrefilled = new Array();
    var model: HomeCardModel;
    for (var i = 0; i < this.application.homeCards.length; i++) {
      model = new HomeCardModel();
      model.title = this.application.homeCards[i].title;
      model.description = this.application.homeCards[i].description;
      if (this.application.homeCards[i].image != null
        && this.application.homeCards[i].image != 'undefined'
        && this.application.homeCards[i].image != '') {
          model.image = this.application.homeCards[i].image;
      }else{
        model.showImage = false;
      }

      this.homeCardsPrefilled.push(model);
    }
  }

}

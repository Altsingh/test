export class HomeCardModel {
    title: string;
    description: string;
    image: string;
    showImage: boolean = true;

    constructor() {
        this.title = '';
        this.description = '';
        this.image = '';
    }
}
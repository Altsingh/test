import { AltComponent } from "./alt-component";
export class AltBase<T> extends AltComponent{
    dbClass: string;
    dbAttr: string;
    dbClassWrite:string;
    dbAttrWrite:string;
    hint: string;
    value: T;
    required:boolean;
    dbClassType:string;
    dbAttrRead:string
    matchOwnerId:string;

    constructor(options: {
        namespace?: string,
        id?: string,
        label?: string,
        name?: string,
        controlType?: string,
        dbClass?: string;
        dbAttr?: string;
        dbClassWrite?:string;
        dbAttrWrite?:string;
        hint?: string;
        value?: T
    } = {}) {
        super(options);
        this.dbClass = options.dbClass;
        this.dbAttr = options.dbAttr;
        this.dbClassWrite=options.dbClassWrite;
        this.dbAttrWrite=options.dbAttrWrite;
        this.hint = options.hint;
        this.value = options.value;
        this.required=options['required']||'';
    }
}
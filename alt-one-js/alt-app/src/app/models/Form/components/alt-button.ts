import { AltComponent } from "./alt-component";
export class AltButton extends AltComponent {
    componentIds: string[];

    constructor(options: {} = {}) {
        super(options);
        this.componentIds = options['componentIds'] || '';
    }
}
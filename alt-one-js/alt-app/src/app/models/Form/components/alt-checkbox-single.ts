import { AltBase } from "./alt-base";

export class AltCheckboxSingle extends AltBase<boolean> {

    constructor(options:{}={}){
        super(options);
    }
}
export class AltComponentSecurity {
    fieldLabel: string;

    title: string;

    required: boolean;

    viewable: boolean;

    editable: boolean;
}
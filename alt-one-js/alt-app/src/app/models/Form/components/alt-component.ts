import { AltComponentSecurity } from "./alt-component-security";
import { FilterBase } from "../filter/filter-base-pojo";
import { Rule } from '../../rule/rule.model';
export class AltComponent {
    namespace: string;
    id: string;
    label: string;
    instruction:string;
    helpText:string;
    name: string;
    cssClass:string;
    controlType: string;
    relation: AltRelation[];
    securityType: EnumSecurityType;
    security: AltComponentSecurity;
    filters:FilterBase[];
    ruleObjects:Rule[];
    rules:number[];
    events: string[];
    
    constructor(options: {
        namespace?: string,
        id?: string,
        label?: string,
        instruction?:string,
        helpText?:string,
        name?: string,
        cssClass?:string,
        controlType?: string,
        relation?: AltRelation[],
        securityType?: EnumSecurityType,
        ruleObjects?:Rule[],
        rules?:number[],
        events?:string[]
    } = {}) {
        this.namespace = options.namespace;
        this.id = options.id || '';
        this.label = options.label || '';
        this.instruction=options.instruction||'';
        this.helpText=options.helpText||'';
        this.name = options.name || '';
        this.cssClass = options.cssClass || '';
        this.controlType = options.controlType || '';
        this.relation = options.relation || [];
        this.securityType = options.securityType || EnumSecurityType.DENY;
        this.ruleObjects = options.ruleObjects || [];
        this.rules = options.rules || [];
        this.events = options.events || [];
    }
}

export enum EnumSecurityType {
    DENY, MANDATORY, VIEWABLE
}
export class AltRelation {
    trigger: string;
    targetComponentList: TargetComponent[]
}
export class TargetComponent {
    targetComponent: string;
    events: AltEvent[]
}
export class AltEvent {
    action: string;
    webService: WebService;
}
export class WebService {
    type: WebServiceType;
    url: string;
    webServiceParam: WebServiceParamMetamap
}

export class WebServiceParamMetamap {
    [index: string]: string;
}

export class WebServiceType {

}

import { AltComponent } from "./alt-component";

export class DataTable extends AltComponent{
    displayedColumns: string[];
    data: Map<string,object>;
    formPopup:number;
    allColumns:string[];
    filteringData:Map<string,object>;
}
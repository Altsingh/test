import { AltBase } from "./alt-base";

export class AltDropdown extends AltBase<string> {
    options: any [];
     allColumns:string [];
    filteringData:Map<string,object>;
    attributeName:string;
    filteredOptions:any[];
    constructor(options){
        super(options);
        this.options=options['options']||[];
    }
}
import { AltBase } from "./alt-base";

export class AltMultiSelect extends AltBase<string> {
    options: any[];

    constructor(options) {
        super(options);
        this.options = options['options'] || [];
    }
}
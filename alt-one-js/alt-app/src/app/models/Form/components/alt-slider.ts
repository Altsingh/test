import { AltBase } from "./alt-base";

export class AltSlider extends AltBase<boolean> {
    max: number;
    min: number;

    constructor(options:{}={}){
        super(options);
        this.min = options['min'] || '';
        this.max = options['max'] || '';
    }
}
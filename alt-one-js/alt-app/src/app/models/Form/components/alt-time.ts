import { AltBase } from "./alt-base";

export class AltTime extends AltBase<string> {
    placeholder: string;
    
    constructor(options:{}={}){
        super(options);
        this.placeholder = options['placeholder'] || '';
    }
}
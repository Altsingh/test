import { DataTable } from './components/alt-datatable';

export class DownloadDataTable {
    tenantId: string;
    formRoles: any[];
    table: DataTable;
    fileName: string;
    fileData: string;
}
import { DataTable } from './components/alt-datatable';

export class ReloadLocalDataTable {
    tables: DataTable[];
    ownerEmployeeId: number;
}
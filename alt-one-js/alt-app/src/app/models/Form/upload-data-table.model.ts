import { DataTable } from './components/alt-datatable';

export class UploadDataTable {
    tenantId: string;
    formRoles: any[];
    appId: string;
    table: DataTable;
    fileDataBase64: string;
}
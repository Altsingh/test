import { Module } from "./module.model";
import { TaskTO } from "../workflow/task.model";
import { WorkflowCardTO } from "../workflow/workflow-card.model";
import { EmployeeResponseTO } from "../employee/employee-response.model";
import { HomeCardModel } from "../../home/homecard.model";

export class Application {

    appName: string;
    appCode: string;
    appId: string;
    modules: Module[];
    homeCards: HomeCardModel[];
    cas: string;
    employeeInfo: EmployeeResponseTO;
    classIds: number[];

    constructor() {
        this.appName = "";
        this.appCode = "";
        this.appId = "";
        this.modules = [];
        this.cas = "";
    }
}
import { Menu } from "./menu.model";

export class MenuGroup {
    name:string;
    menus:Menu[];

    constructor(){
        this.name="";
        this.menus=[];
    }
}
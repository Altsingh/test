import { AttachedForm } from "./attached-form.model";

export class Menu {

    name:string;
	uiClass:string;
    androidIcon:string;
	iosIcon:string;
    webIcon:string;
    webSequence:string;
    mobileSequence:string;
    menus:Menu[]; 
    form:AttachedForm;  
    landingPage:boolean;
     
    constructor(){
          this.name="";
          this.uiClass="";
          this.androidIcon="";
          this.iosIcon="";
          this.webIcon="";
          this.webSequence="";
          this.mobileSequence="";
          this.form=null;
    }
}
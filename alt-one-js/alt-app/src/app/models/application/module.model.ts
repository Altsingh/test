import { Menu } from "./menu.model";
import { MenuGroup } from "./menu-group.model";
import { AttachedForm } from "./attached-form.model";

export class Module {
    name:string;
    oldname:string;
    iconPath:string;
    sequence:number;
    menus:Menu[];
    menuGroups:MenuGroup[];
    form:AttachedForm;
    active:boolean;

    constructor(){
        this.name="";
        this.oldname="";
        this.iconPath="";
        this.sequence=-1;
        this.menus=[];
        this.menuGroups=[];
        this.form=null;
        this.active=false;
    }
}
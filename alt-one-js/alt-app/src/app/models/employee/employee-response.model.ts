export class EmployeeResponseTO {
    employeeName: string;
    photoPath: string;
    assignedRoleIds: number[];
    mobile: string;
    email: string;
}
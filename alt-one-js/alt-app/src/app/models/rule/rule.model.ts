export class Rule{
    ruleId:number;
    ruleName:string;
    ruleType:string;
    cases:Case[];
    successMessage:string;
}


export class Case{
    caseName:string;
    firstInput:RuleInput;
    secondInput:RuleInput;
    validationType:string;
    failureMessage:string;
    assignToAttribute:RuleInput;
    event:string;
    componentType:string;
    componentName:string;
    ruleExecutionEnd: string;
    scriptData: string;
    caseType: string;
}

export class RuleInput{
    inputType:string;
    dbClass:string;
    dbClassAttr:string;
    value:string;
}

export class RulePopup{
    ruleId:number;
    ruleName:string;
    ruleType:string;
    cases:CasePopup[];
    successMessage:string;
}

export class CasePopup{
    caseName:string;
    firstInput:string;
    secondInput:string;
    secondAttribute:string;
    validationType:string;
    failureMessage:string;
    assignToAttribute:string;
    event:string;
}
export class RuleDeleteRequest{
    rules:Array<Number>;
}
/*
export class ValidationCase{
    firstInput:RuleInput;
    secondInput:RuleInput;
    validationType:ValidationCondition;
    failureMessage:string;
}

export enum ValidationCondition{
    EQUALS,GREATER_THAN,LESS_THAN
}

export class TransactionCase extends Case{
    firstInput:RuleInput;
    secondInput:RuleInput;
    validationType:TransactionCondition;
    failureMessage:string;
}



export enum TransactionCondition{
    ADD, SUBTRACT, CONCAT
}

export enum InputType{
    REFERENCE,VALUE
}

export enum RuleType{
    VALIDATION,TRANSACTION
}*/
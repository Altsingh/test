import { TaskTO } from "./task.model";
import { TaskGroupTO } from "./task-group.model";

export class AppliedWorkflowCardTO {

    appliedTaskCount: number;

    appliedTasks: TaskTO[];

    appliedTaskGroups: TaskGroupTO[];

}
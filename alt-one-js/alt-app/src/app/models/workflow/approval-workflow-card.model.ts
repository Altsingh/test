import { TaskTO } from "./task.model";

export class ApprovalWorkflowCardTO {

    approvalTaskCount: number;

    approvalTasks: TaskTO[];

}
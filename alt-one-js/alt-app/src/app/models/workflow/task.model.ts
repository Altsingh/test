export class TaskTO {

    actorID: number;

    actionDate: string;

    actionDateType: Date;

    actorName: string;

    defaultClassCall: string;

    delegateUserID: number;

    entityId: number;

    employeeCode: string;

    employeeName: string;

    employeeId: number;

    formId: number;

    formName: string;

    groupId: number;

    groupList: TaskTO[];

    instanceID: number;

    instanceUserId: number;

    moduleId: number;

    moduleName: string;

    photoPath: string;

    resignationCode: string;

    resignationStatus: string;

    roleId: number;

    roleName: string;

    serialNumber: string;

    statusMessage: string;

    sysUserWorkflowHistoryId: number;

    startDate: string;

    startDateType: Date;

    stageId: number;

    sysworkflowstageTypeID: number;

    taskNumber: number;

    stageType: string;

    stageName: string;

    url: string;

    workflowType: string;

    workflowstageName: string;

}
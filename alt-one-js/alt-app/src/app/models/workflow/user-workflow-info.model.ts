import { TaskTO } from "./task.model";
import { WorkflowCardTO } from "./workflow-card.model";

export class UserWorkflowInfo {

    tasks: TaskTO[];

    cards: WorkflowCardTO[];

}
import { AppliedWorkflowCardTO } from "./applied-workflow-card.model";
import { ApprovalWorkflowCardTO } from "./approval-workflow-card.model";

export class WorkflowCardTO {

    formName: string;

    appliedWorkflowCard: AppliedWorkflowCardTO;

    approvalWorkflowCard: ApprovalWorkflowCardTO;

}
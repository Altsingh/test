import { Component, OnInit } from '@angular/core';
import { SecureloginService } from '../securelogin.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { SecureLoginModel } from '../securelogin.model';




@Component({
  selector: 'app-securelogin-receiver',
  templateUrl: './securelogin-receiver.component.html',
  providers: [SecureloginService]
})
export class SecureloginReceiverComponent implements OnInit {
  private accessToken: string;
  private refreshToken: string;
  private url: string;
  private custom: boolean;
  private tenantId: string;
  private dataMap: Map<string, string>;
  private appCode:string;

  constructor(private route: ActivatedRoute,
    private secureloginService: SecureloginService, private router: Router) {
      this.route.params.subscribe((params: Params) => {
                this.appCode = params['appname'];
                console.log("************** appname : "+this.appCode)
              });
  }

  ngOnInit() {

    this.accessToken = this.route.snapshot
      .queryParams['accessToken'];
    this.refreshToken = this.route.snapshot
      .queryParams['refreshToken'];
    this.custom = this.route.snapshot
      .queryParams['isCustom'];
    this.tenantId = this.route.snapshot.queryParams['tenantId'];
    localStorage.setItem("tenantId",this.tenantId);
    // check for SSO Login
    this.secureLoginPreFetch();
  }

  /**
 * @description Responsible for sending custom app URL
 * received from the mesh & will send it microservice for fetching
 * app details
 * @param string custom app url
 * @author vaihav.kashyap
 */

  prepareUrlAndFetchAppDetails(appUrl: string): string {
    if (appUrl != null && appUrl.length > 0) {
      let strArry = appUrl.split("/");
      this.url = strArry[2];
      return this.url;
    }

  }

  /**
   * @description Prepares a map of data recievied from microservices
   * sample data: usr=vaibhav$comp=peoplestrong
   * @author vaihav.kashyap
   * @param data 
   */
  prepareFetchedData(data: string) {
    if (data.includes("$")) {
      let strArry = data.split("$");
      for (var i = 0; i < strArry.length; i++) {
        let keyValue = strArry[i].split("=");
        this.dataMap.set(keyValue[0], keyValue[1]);
      }
    } else {
      let keyValue = data.split("=");
      this.dataMap.set(keyValue[0], keyValue[1]);
    }

  }

  /**
   * @description Responsible for sending access and refresh token
   * received from the mesh & will send it microservice for fetching the userName
   * @author vaihav.kashyap
   */
  secureLoginPreFetch() {
    let secureInstance = new SecureLoginModel();
    secureInstance.accesstoken = this.accessToken;
    secureInstance.refreshtoken = this.refreshToken;
    secureInstance.username = null;
    secureInstance.received = false;
    secureInstance.appCode = this.appCode;
    secureInstance.custom = this.custom;

    this.secureloginService.secureLoginUser(secureInstance);
    /*let received = localStorage.getItem("received");
    if (received) {
      if (this.custom) {
        // get the custom app URL from request URL
        this.url = this.prepareUrlAndFetchAppDetails(location.href)
        this.getCustomAppData(this.appCode)
      }
    }*/


  }

}

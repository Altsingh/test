import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecureloginReceiverComponent} from './securelogin-receiver/securelogin-receiver.component';

const routes: Routes = [
  {path: '', component: SecureloginReceiverComponent,pathMatch: 'full'},
  {path: 'securelogin/:appname', component: SecureloginReceiverComponent,pathMatch: 'full' }
];

export const SecureloginRoutingModule = RouterModule.forChild(routes);

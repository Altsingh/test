import { AppData } from "./customapp.model";

/**
 * @author vaibhbav.kashyap
 * 
*/
export class SecureLoginModel {
    accesstoken: string;
    refreshtoken: string;
    username: string;
    received: boolean;
    authtoken: string;
    appCode: string;
    custom: boolean;
    appdata: AppData;
    userId:string;
    orgId:string;
    photoPath: string;
    constructor() {
    }
}
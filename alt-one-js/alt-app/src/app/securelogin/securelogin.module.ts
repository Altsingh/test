import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecureloginRoutingModule } from './securelogin-routing.module';
import { SecureloginReceiverComponent } from './securelogin-receiver/securelogin-receiver.component';

@NgModule({
  imports: [
    CommonModule,
    SecureloginRoutingModule
  ],
  declarations: [SecureloginReceiverComponent]
})
export class SecureloginModule { }

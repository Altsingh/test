import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { SecureLoginModel } from './securelogin.model';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable()
export class SecureloginService {
  private secureurl: string = environment.secureurl;
  private receivedSecureModelInstance: SecureLoginModel;

  constructor(private http: HttpClient, private router: Router) { }

  /**
   * @description sends the post request to the microservice along with tokens
   * received for fetching related deails
   * @author vaihav.kashyap
   * @param SecureLoginModel
   */
  secureLoginUser(seucreLoginObj: SecureLoginModel): SecureLoginModel {
    this.http.post(this.secureurl, seucreLoginObj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response'
    })
      .subscribe((res: any) => {
        if (res.status == 200) {
          this.extractLoginData(res, seucreLoginObj.appCode);
        } else {
          throw new Error('Bad response status: ' + res.status);
        }
      },
        (err) => {
          this.handleError(err);
        });

    return this.receivedSecureModelInstance;

  }


  /**
   * @description prepares data received from backend boot services along with user details
   * fetched
   * @author vaihav.kashyap
   * @param Response
   */
  extractLoginData(res: any, appCode: any): SecureLoginModel {

    let body = res.body;

    /**
     * Prepare Security instance object on the basis of the data
     * received from backend after successful login.
     * */
    if (body != null) {
      if (body.authtoken != null && !(body.authtoken == "")) {
        localStorage.setItem("authToken", body.authtoken);

        this.receivedSecureModelInstance = new SecureLoginModel();
        this.receivedSecureModelInstance.accesstoken = body.accesstoken;
        this.receivedSecureModelInstance.refreshtoken = body.refreshtoken;
        this.receivedSecureModelInstance.username = body.username;
        this.receivedSecureModelInstance.received = body.received;
        this.receivedSecureModelInstance.appCode = body.appCode;
        this.receivedSecureModelInstance.custom = body.isCustom;
        this.receivedSecureModelInstance.authtoken = body.authtoken;
        this.receivedSecureModelInstance.userId = body.userId;
        this.receivedSecureModelInstance.orgId = body.orgId;
        localStorage.setItem("adminPhoto",body.photoPath);
        localStorage.setItem("realm", btoa(body.orgId));
        localStorage.setItem("entityId", btoa(body.userId));
        localStorage.setItem("refresh_token", btoa(body.refreshtoken));
        let x = (body.refreshtoken).split(".");
        let decoded_refresh_token = atob(x[1]);
        var mapValue = JSON.parse(decoded_refresh_token).iss;
        localStorage.setItem("keycloakUrl", btoa(mapValue));
        //var issuer = mapValue.get("iss").toString();
        //localStorage.setItem("keycloakUrl",issuer);
        //angular.toJson(decoded_refresh_token);
        // this will be true if user has successfuly logged in
        localStorage.setItem("received", body.received);
        this.getCustomAppData(this.receivedSecureModelInstance, appCode);

      } else {
        // error in login
        this.router.navigateByUrl('not-found');
      }
    } else {
      // error in login
      this.router.navigateByUrl('not-found');
    }
    return this.receivedSecureModelInstance;
  }


  /**
   * @description sends the post request to the microservice along with custom
   * app url for fetching app's detail
   * @author vaihav.kashyap
   * @param SecureLoginModel
   */
  getCustomAppData(seucreLoginObj: SecureLoginModel, appCode) {
    this.http.post(environment.getappdata, seucreLoginObj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response'
    })
      .subscribe((res: any) => {
        if (res.status == 200) {

          /* if SSO login was successful then extract custom app details
          * & redirect user to app's home page
          * */
          let isLoggedIn = localStorage.getItem("received")
          if (isLoggedIn) {
            this.extractAppData(res);
          }
        } else {
          throw new Error('Bad response status: ' + res.status);
        }

      },
        (err) => {
          this.handleError(err);
        });
  }


  /**
   * @description prepares App data received from backend boot services
   *
   * @author vaihav.kashyap
   * @param Response
   */
  extractAppData(res: any) {
    let body = res.body;

    /**
     * Prepare Security instance object on the basis of the data
     * received from backend after successful Custom App Data
     * fetch.
     * */
    if (body != null) {
      var secureloginResponseInstance = body.appdata;
      localStorage.setItem("productId", secureloginResponseInstance.appId);
      localStorage.setItem("productLogo", secureloginResponseInstance.appLogoUrl)
      console.log("after login : " + secureloginResponseInstance.appId)
      this.router.navigateByUrl('application/' + secureloginResponseInstance.appId);
    }
  }

  /**
   * @description Handles error message
   * @author vaihav.kashyap
   * @param any
   */
  private handleError(error: any) {
    let errMsg = error.message || 'Server error';
    this.router.navigateByUrl('not-found');
  }

}

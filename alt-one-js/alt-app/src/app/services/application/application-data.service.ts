import { Injectable } from '@angular/core';
import { DataService } from "../data.service";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable()
export class ApplicationDataService extends DataService {

  constructor(http: HttpClient) {
    super(http);
    super.setUrl(environment.url + '/' + 'apps');
  }

}

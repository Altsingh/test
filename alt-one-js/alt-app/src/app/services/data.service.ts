import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { FormDataRetrieveRequest } from '../application/form-popup/formdataretrieve.model';

@Injectable()
export class DataService {

  public options: any;
  private url: string;

  constructor(private http: HttpClient) {

    this.options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      })
    }

  }

  public getUrl() {
    return this.url;
  }

  public setUrl(url: string) {
    this.url = url;
  }

  getAll() {
    return this.http.get(this.url, this.options);
  }

  get(id: string): any {
    return this.http.get(this.url + '/' + id, this.options);
  }

  public getMeshDetails(userId: string, organizationId: string, tenantId: string, ) {
    let body = `orgId=${organizationId}&userId=${userId}&tenantId=${tenantId}`;

    var meshOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    }

    return this.http.post(environment.meshUrl, body, meshOptions);
  }

  getForm(formDataRequest: FormDataRetrieveRequest) {
    return this.http.post(environment.getformpostrequest, formDataRequest, this.options);
  }

  deleteInstance(id: string) {
    return this.http.get(environment.deleteInstance + '/' + id, this.options);
  }

  create(formTO: any) {
    return this.http.post(this.url, formTO, this.options);
  }


  update(resource) {
    return this.http.put(this.url, resource);
  }

  delete(id: string) {
    return this.http.delete(this.url + '/' + id);
  }

  post(url: string, body: any): any {
    return this.http.post(url, body, this.options);
  }

  uploadImage(requestData) {
    return this.http
      .post(environment.appLogoUploadUrl, requestData);
  }

  postWithoutOptions(url: string, body: any): any {
    return this.http.post(url, body);
  }
}

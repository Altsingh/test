import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { DataService } from "../data.service";
import { ReloadGlobalTextfields } from 'src/app/models/Form/reload-global-textfields.model';
import { ReloadLocalDataTable } from 'src/app/models/Form/reload-local-datatable.model';
import { Observable } from 'rxjs';
import { DownloadDataTable } from 'src/app/models/Form/download-data-table.model';
import { UploadDataTable } from 'src/app/models/Form/upload-data-table.model';

@Injectable()
export class FormDataService extends DataService {

  constructor(http: HttpClient) {
    super(http);
    super.setUrl(environment.formUrl + '/' + 'forms');
  }

  reloadGlobalTextfields(reloadGlobalTextfields: ReloadGlobalTextfields): any {
    return super.post(environment.formUrl + "/forms/reloadGlobalTextfields", reloadGlobalTextfields);
  }

  reloadLocalDataTable(reloadLocalDataTable: ReloadLocalDataTable): any {
    return super.post(environment.formUrl + "/forms/reloadLocalDataTable", reloadLocalDataTable);
  }

  downloadDataTable(download: DownloadDataTable): any {
    return super.post(environment.formUrl + "/forms/downloadDataTable", download);
  }

  uploadDataTable(upload: UploadDataTable): any {
    return super.post(environment.formUrl + "/forms/uploadDataTable", upload);
  }

  uploadFile(request: any) {
    return super.postWithoutOptions(environment.formUrl + "/forms/uploadFile", request)
  }
}

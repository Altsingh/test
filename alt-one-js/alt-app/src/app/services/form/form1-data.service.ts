import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { DataService } from "../data.service";

@Injectable()
export class Form1DataService extends DataService {

  constructor(http: HttpClient) {
    super(http);
    super.setUrl(environment.formUrl + '/' + 'forms1');
  }

}

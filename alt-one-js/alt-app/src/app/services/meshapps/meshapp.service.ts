import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";


@Injectable()
export class MeshAppService {
	keycloakBaseUrl: string;
	realm: string;
	client: string;
	refreshToken: string;
	accessToken: string;
	callbackUrl: string;
	ssoUrl: string;
	reportList: any;

	constructor(private http: HttpClient) {
	}

	loadAppUrl() {

		console.log('inside meshapp module');
		this.keycloakBaseUrl = atob(localStorage.getItem("keycloakUrl"));
		this.realm = atob(localStorage.getItem("realm"));
		this.client = this.realm;
		this.refreshToken = atob(localStorage.getItem("refresh_token"));
		this.ssoUrl = this.keycloakBaseUrl + 'auth/realms/' + this.realm + '/protocol/openid-connect/token';
		this.callbackUrl = this.keycloakBaseUrl + 'auth/realms/' + this.realm + '/protocol/openid-connect/userinfo';
		this.keycloakSSO1();
	}
	private getKeycloakHeaders() {
		return {
			headers: new HttpHeaders({
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Access-Control-Allow-Headers': 'Content-Type',
				'Access-Control-Allow-Methods': 'POST'
			})
		}
	}

	keycloakSSO1() {

		let tokenUrl = this.ssoUrl;
		let req = new XMLHttpRequest();
		let params = 'grant_type=refresh_token&client_id=' + this.client + '&refresh_token=' + this.refreshToken;
		req.open('POST', tokenUrl, true);
		req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

		req.send(params);
		(<HTMLInputElement>document.getElementById("CallbackURL")).value = this.callbackUrl;
		req.onreadystatechange = function () {
			if (req.readyState == 4 && req.status == 200) {
				var data = JSON.parse(req.responseText);
				localStorage.setItem("refresh_token", data['refresh_token']);

				var appType = localStorage.getItem("appType");
				var url = localStorage.getItem("appUrl");

				if (url.includes("creator") || appType == 'Custom App') {
					(<HTMLFormElement>document.getElementById("altAppFormCreator")).action = "/AltOneCreaterCaller";
					(<HTMLInputElement>document.getElementById("accessTokenn")).value = data['access_token'];;
					(<HTMLInputElement>document.getElementById("refreshTokenn")).value = data['refresh_token'];
					(<HTMLInputElement>document.getElementById("url")).value = url;
					(<HTMLInputElement>document.getElementById("orgId")).value = localStorage.getItem("orgId");
					(<HTMLInputElement>document.getElementById("tenantId")).value = localStorage.getItem("tenantId");
					(<HTMLFormElement>document.getElementById("altAppFormCreator")).submit();
				}
				else if (appType == 'Alt App') {
					(<HTMLFormElement>document.getElementById("altAppForm")).action = localStorage.getItem("appUrl");
					(<HTMLInputElement>document.getElementById("accessToken")).value = data['access_token'];
					console.log('ret access token ' + data['access_token']);
					(<HTMLInputElement>document.getElementById("refreshToken")).value = data['refresh_token'];
					(<HTMLFormElement>document.getElementById("altAppForm")).submit();
				} else {
					(<HTMLFormElement>document.getElementById("partnerAppForm")).action = localStorage.getItem("appUrl");
					(<HTMLInputElement>document.getElementById("CustomerIdentifier")).value = localStorage.getItem("customerIdentifier");
					(<HTMLInputElement>document.getElementById("AccessToken")).value = data['access_token'];


					let d = new Date();
					var d1 = [
						'0' + d.getDate(),
						'0' + (d.getMonth() + 1),
						'' + d.getFullYear(),
						'0' + d.getHours(),
						'0' + d.getMinutes(),
						'0' + d.getSeconds()
					].map(component => component.slice(-2)); // take last 2 digits of every component
					var fDate = d1.slice(0, 3).join('.') + ' ' + d1.slice(3).join(':');
					(<HTMLInputElement>document.getElementById("Timestamp")).value = fDate.toString();

					(<HTMLFormElement>document.getElementById("partnerAppForm")).submit();
				}
			}

		}

	}


	keycloakSSO() {

		var data = {

			grant_type: 'refresh_token',

			client_id: this.client,

			refresh_token: this.refreshToken,
		};
		console.log('client_id ' + this.client);
		console.log('refresh_token ' + this.refreshToken);
		console.log('ssoUrl ' + this.ssoUrl);

		//Call the services
		this.http.post(this.ssoUrl, JSON.stringify(data), this.getKeycloakHeaders()).subscribe((res) => {
			console.log('res json ' + res);
			var appType = localStorage.getItem("appType");
			var url = localStorage.getItem("appUrl");

			if (url.includes("creator") || appType == 'Custom App') {
				(<HTMLFormElement>document.getElementById("altAppFormCreator")).action = "/AltOneCreaterCaller";
				(<HTMLInputElement>document.getElementById("accessTokenn")).value = this.refreshToken;
				(<HTMLInputElement>document.getElementById("refreshTokenn")).value = data.refresh_token;
				(<HTMLInputElement>document.getElementById("url")).value = url;
				(<HTMLInputElement>document.getElementById("orgId")).value = localStorage.getItem("orgId");
				(<HTMLInputElement>document.getElementById("tenantId")).value = localStorage.getItem("tenantId");
				(<HTMLFormElement>document.getElementById("altAppFormCreator")).submit();
			}
			else if (appType == 'Alt App') {
				(<HTMLFormElement>document.getElementById("altAppForm")).action = localStorage.getItem("appUrl");
				(<HTMLInputElement>document.getElementById("accessToken")).value = data['access_token'];
				(<HTMLInputElement>document.getElementById("refreshToken")).value = data['refresh_token'];
				(<HTMLFormElement>document.getElementById("altAppForm")).submit();
			} else {
				(<HTMLFormElement>document.getElementById("partnerAppForm")).action = localStorage.getItem("appUrl");
				(<HTMLInputElement>document.getElementById("CustomerIdentifier")).value = localStorage.getItem("customerIdentifier");
				(<HTMLInputElement>document.getElementById("AccessToken")).value = data['access_token'];
				(<HTMLInputElement>document.getElementById("CallbackURL")).value = this.callbackUrl;

				let d = new Date();
				var d1 = [
					'0' + d.getDate(),
					'0' + (d.getMonth() + 1),
					'' + d.getFullYear(),
					'0' + d.getHours(),
					'0' + d.getMinutes(),
					'0' + d.getSeconds()
				].map(component => component.slice(-2)); // take last 2 digits of every component
				var fDate = d1.slice(0, 3).join('.') + ' ' + d1.slice(3).join(':');
				(<HTMLInputElement>document.getElementById("Timestamp")).value = fDate.toString();

				(<HTMLFormElement>document.getElementById("partnerAppForm")).submit();
			}


		});
	}


}

export const notifications={
    "save_successful": "Saved successfully",
    "created_successful": "Created successfully",
    "updated_successful": "Updated successfully",
    "unexpected_result": "Unexpected Result"
}
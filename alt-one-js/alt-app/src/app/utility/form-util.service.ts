import { Injectable } from '@angular/core';
import { AltTextbox } from "../models/Form/components/alt-textbox";
import { AltBase } from "../models/Form/components/alt-base";
import { FormGroup, FormArray, FormControl, Validators } from "@angular/forms";
import { AltButton } from '../models/Form/components/alt-button';
import { AltDropdown } from '../models/Form/components/alt-dropdown';
import { AltMultiSelect } from "../models/Form/components/alt-multi-select";
import { AltPanel } from '../models/Form/components/alt-panel';

@Injectable()
export class FormUtilService {

    constructor() { }

    public static toFormGroup(htmlComponents: any[]) {
        let group: any = {};
        htmlComponents.forEach(component => {
            if (component.controlType == 'PANEL') {
                group[component.id] = this.toFormGroup(component.componentList);
            } else if (component.controlType == 'TAB_PANEL') {
                var panelGroup: any = {};
                component.panels.forEach(panel => {
                    panelGroup[panel.id] = this.toFormGroup(panel.componentList);
                });
                group[component.id] = new FormGroup(panelGroup);
            } else {
                if (component.required) {
                    group[component.id] = new FormControl(component.value || '', Validators.required);
                } else {
                    group[component.id] = new FormControl(component.value);
                }
            }
        });
        return new FormGroup(group);
    }
}

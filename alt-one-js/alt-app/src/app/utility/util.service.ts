import { Injectable } from "@angular/core";

@Injectable()
export class UtilService {

    constructor() {

    }

    public static replaceHashStringByUnderScore(str: string) {
        return str.replace(new RegExp('#', 'g'), '_');
    }

    public static replaceUnderScoreByHash(str: string) {
        return str.replace(new RegExp('_', 'g'), '#');
    }

    public static replaceUnderScoreByPercent(str: string) {
        return str.replace(new RegExp('_', 'g'), '%23');
    }

    public static replaceHashByPercent(str:string){
          return str.replace(new RegExp('#', 'g'), '%23');
    }
}
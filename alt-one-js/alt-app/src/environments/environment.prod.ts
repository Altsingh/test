export const environment = {

  //environment
  production: true,

  // backend urls
  url: "https://gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/api/v1",
  secureurl: "https://gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/loginReceiverController/login",
  getappdata: "https://gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/loginReceiverController/getCustomAppData",
  getformpostrequest: "https://gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/api/v1/forms/getForm",
  deleteInstance: "https://gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/api/v1/forms/deleteinstance",
  formUrl: "https://gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/api/v1",
  appLogoUploadUrl: "https://gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/api/v1/apps/logo/applogoupload",
  cardUrl: "https://gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/api/v1/apps/workflowInfo",
  defaultusruploadimg: "../assets/images/noimage.jpg",
  meshUrl: "https://s2demo-admin.peoplestrong.com/services/apps",
  //authtoken
  authToken: "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTA2NTEwNDUsInN1YiI6ImxvZ2luIiwidXNlckNvZGUiOiJ1c2VyMDAxIiwibG9nZ2VkSW5UaW1lc3RhbXAiOjE1MTA2NTEwNDUwNzgsInVzZXJOYW1lIjoidXNlciIsIm9yZ0NvZGUiOiJvcmcyIiwidGVuYW50Q29kZSI6IiAiLCJzeXNGb3JtQ29kZSI6IiAiLCJwb3J0YWxUeXBlIjoiU1VQRVJBRE1JTiIsImxvZ2dlZEluVXNlcklkZW50aWZpZXIiOiIgIiwiaXNzIjoiQWx0LUxvZ2luIiwiZXhwIjoxNTQyMTg3MDQ1fQ.sBLgWygjlciZv7YOgHRc2RAeoQzPZn-P8PY6z6V8oX8"
};

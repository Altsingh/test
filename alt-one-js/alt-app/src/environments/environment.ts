// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {

  //environment
  production: false,

  //backend urls
  url: "https://localhost:8095/formframework/api/v1",
  secureurl: "https://localhost:8095/formframework/loginReceiverController/login",
  getappdata: "https://localhost:8095/formframework/loginReceiverController/getCustomAppData",
  getformpostrequest: "https://localhost:8095/formframework/api/v1/forms/getForm",
  deleteInstance: "https://localhost:8095/formframework/api/v1/forms/deleteinstance",
  formUrl: "https://localhost:8095/formframework/api/v1",
  appLogoUploadUrl: "https://localhost:8095/formframework/api/v1/apps/logo/applogoupload",
  cardUrl: "https://localhost:8095/formframework/api/v1/apps/workflowInfo",
  defaultusruploadimg: "../assets/images/noimage.jpg",
  meshUrl: "https://hrms-admin.sohum.com/services/apps"
  //authtoken
  //authToken: "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTA2NTEwNDUsInN1YiI6ImxvZ2luIiwidXNlckNvZGUiOiJ1c2VyMDAxIiwibG9nZ2VkSW5UaW1lc3RhbXAiOjE1MTA2NTEwNDUwNzgsInVzZXJOYW1lIjoidXNlciIsIm9yZ0NvZGUiOiJvcmcyIiwidGVuYW50Q29kZSI6IiAiLCJzeXNGb3JtQ29kZSI6IiAiLCJwb3J0YWxUeXBlIjoiU1VQRVJBRE1JTiIsImxvZ2dlZEluVXNlcklkZW50aWZpZXIiOiIgIiwiaXNzIjoiQWx0LUxvZ2luIiwiZXhwIjoxNTQyMTg3MDQ1fQ.sBLgWygjlciZv7YOgHRc2RAeoQzPZn-P8PY6z6V8oX8"
};

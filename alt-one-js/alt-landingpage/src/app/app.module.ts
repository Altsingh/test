import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModuleWithProviders } from "@angular/core";

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from "@angular/router";
import { LandingPageComponent } from './landing-page/landing-page.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FormsModule } from '@angular/forms';
import { LoginHttpService } from './services/login-http.service';
import { AppsListService } from './services/apps-list.service';
import { EmpInfoService } from './empInfo/emp-info.service';
import { UserInfoService } from './userInfo/userinfo.service';
import { HttpModule } from '@angular/http';
import { MeshAppService } from './meshapps/meshapp.service';
import { UrlUtility } from './urls/url-utility';
/*const routes: Routes = [
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: '', component: LandingPageComponent, pathMatch: 'full' }
]

export const routings: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });*/

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LandingPageComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    /*routings,*/
    FormsModule,
    HttpModule
  ],
  providers: [LoginHttpService,AppsListService, MeshAppService, UrlUtility, EmpInfoService, UserInfoService],
  bootstrap: [AppComponent]
})
export class AppModule { }

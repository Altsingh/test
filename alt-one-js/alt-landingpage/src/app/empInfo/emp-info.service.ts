import { Injectable } from '@angular/core';
import { DataService } from '../services/data-service';
import { environment } from '../../environments/environment';
import { Http, Response, Headers, RequestOptions, ResponseOptions } from '@angular/http';
import { EmpInfoInputTO } from "./empInfoInput";
import { Observable } from "rxjs/Observable";
import { EmpInfoModel } from './empInfo.model';
//import { CategoryApps } from '../models/category-apps.model';
//import { Application } from "../models/application.model";
//import { Category } from "../models/category.model";
//import { UrlUtility } from '../urls/url-utility';
//import { UrlObject } from '../urls/UrlObject';
//declare var angular: any;

@Injectable()
export class EmpInfoService extends DataService {
 empInfoValue:EmpInfoModel;
     appsModelObservable:Observable<any>;
 
  constructor(http: Http) {
    super(environment.employeeInfo, http);
   }
   
   getEmpInfo(empInfoInputTO: EmpInfoInputTO):Observable<any> {
       let body = `organizationId:${empInfoInputTO.orgId}&username=${empInfoInputTO.userName}`;
    console.log('inside getEmpInfo body ' + body);
	let orgId1=empInfoInputTO.orgId;
	let userName1=empInfoInputTO.userName;
    this.appsModelObservable=this.http.post(this.url, {organizationId:orgId1,username:userName1} , this.callAppOptions())
      .map(res => res.json());
      this.appsModelObservable.subscribe(res1 => {  
      this.empInfoValue=res1;
        });
     // return this.empInfoValue;
      return this.appsModelObservable;
   }
    
    
  callAppOptions(): RequestOptions {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return options;
  }
  
  static handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

	
  
  /*getAppsValue(): CategoryApps[]{
  	return this.applications;
  }*/
  
}

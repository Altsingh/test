import { Component, OnInit } from '@angular/core';
import { CategoryApps } from '../models/category-apps.model';
import { AppsListService } from '../services/apps-list.service';
import { MeshAppService } from '../meshapps/meshapp.service';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, Response, Headers, RequestOptions, ResponseOptions } from '@angular/http';
import { AppsInputTO } from "../home/appsInput";
import { Observable } from "rxjs/Observable";
import { AppsModel } from '../home/apps.model';
import { Application } from "../models/application.model";
import { Category } from "../models/category.model";
import { UrlUtility } from '../urls/url-utility';
import { UrlObject } from '../urls/UrlObject';
declare var angular: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  host: {
    '(document:click)': 'handleClick($event)',
  },
})

export class HeaderComponent implements OnInit {
  showMesh: boolean;
  showProfileLogout:boolean;
  showLogout: boolean;
  applicationsList:AppsModel[];
  
  applications: CategoryApps[];
  timer: any;

  constructor(private appsListService: AppsListService,private meshAppService : MeshAppService,  private urlUtility: UrlUtility) { }

  showMeshPopUp(event) {
    if (this.showMesh == false) {
      this.showMesh = true;
     this.setApps();
      let ele1=document.getElementsByClassName("switch-app-holder");
      ele1[0].classList.add("open");
      
       let ele2=document.getElementsByClassName("module-true");
      ele2[0].classList.add("open");      
      
    } else {
      this.showMesh = false;
      let ele1=document.getElementsByClassName("switch-app-holder");
      ele1[0].classList.remove("open");
      
       let ele2=document.getElementsByClassName("module-true");
      ele2[0].classList.remove("open");
        
    }
    event.stopPropagation();
  }
  
  
  
  hideMesh(){
  	this.showMesh = false;
      let ele1=document.getElementsByClassName("switch-app-holder");
      ele1[0].classList.remove("open");
      
       let ele2=document.getElementsByClassName("module-true");
      ele2[0].classList.remove("open");
        
  }

	showProfileLogoutPopup(event){
		if (this.showProfileLogout == false) {
      this.showProfileLogout = true;
    } else {
      this.showProfileLogout = false;
    }
    event.stopPropagation();
	}

  logout() {
   console.log('inside keycloak logout removing localstorage');
   
   var j=localStorage.length;
    for (var i = 0; i < j; i++)  {
                    var key = localStorage.key(0);
                    //if(!(key=='hostname')){
                    var value = localStorage.getItem(key);
                   //alert('key '+key+' value '+value);
                    //if (value) {
                    	///alert('key '+key+' value '+value);
                    	
                        try {
                            //var expires = JSON.parse(value).expires;
                           // if (!expires || expires < time) {
                                localStorage.removeItem(key);
                            //}
                        } catch (err) {
                            localStorage.removeItem(key);
                        }
                    //}
                    //}
                    
                }
    (<any>window)._keycloak.logout();
    
   
  }

  toggleLogout() {
    if (this.showLogout == false) {
      this.showLogout = true;
    } else {
      this.showLogout = false;
    }
  }

  handleClick(event) {
    var clickedComponent = event.target;
    if (!clickedComponent.classList.contains("switch-app-holder")) {
      this.showMesh = false;
     }
     if (!clickedComponent.classList.contains("dp22")) {
      this.showLogout = false;
     }
  }
  ngOnInit() {
    this.showMesh = false;
    this.showLogout = false;
    this.timer = setInterval(() => {    //<<<---    using ()=> syntax
		if(window.navigator.userAgent.indexOf("Edge") > -1){
			if (localStorage.getItem('userAppsData')!='null') {
				//this.username = localStorage.getItem('username');
				console.log('userAppsData '+localStorage.getItem('userAppsData'));
				var data = JSON.parse(localStorage.getItem('userAppsData'));
				this.applicationsList=data;
				console.log('data '+data);
				this.convertToCategoryWiseAppsModel();
				//localStorage.setItem('userAppsData',null);
				clearInterval(this.timer);
			}
			else {
				
				//console.log('running timer for userAppsData');
		
			}
		}else{
			console.log('stopping timer for userAppsData as Browser is not Edge');
			clearInterval(this.timer);
		}
	
	}, 10);
   //(click)="hideMesh($event)"
   /*document.getElementsByClassName('module-true')[0].onSelect(function(){
    this.hideMesh();
});*/
  }
  
  convertToCategoryWiseAppsModel(){
		console.log('inside convertToCategoryWiseAppsModel');
	  	let myhash : Map<string, Application[]> = new Map<string, Application[]>();
	  	if (this.applicationsList != null) {
			for (let app of this.applicationsList) {
		    
		    	let applic=new Application();
		    
		    	applic.appName = app.appName;
				applic.appLogo = app.appIconImage;
				applic.appUrl = app.webURL;
				applic.identifier = app.identifier;
				applic.appType = app.appType;
				applic.meshAppLogo = app.appIconImage;
				applic.appCode = app.appCode;
				applic.webEnabled = app.webEnabled;
				applic.androidEnabled = app.androidEnabled;
				applic.iosEnabled = app.iosEnabled;
				applic.andriodUrl = app.androidURL;
				applic.iosUrl = app.iosURL;
		    
	    	
	    	applic.meshAppLogo=app.appIconImage;
	    	console.log(' app name '+applic.appName+' logo '+applic.appLogo+' app code '+app.appCode);
	    	console.log(' app name '+applic.appName+' logo '+applic.appLogo+' app code '+app.appCode);
	    	if(angular.equals(app.appCode,'AORG')){
	    		console.log('setting custom_apps_home');
	    		
	    		let url: UrlObject = this.urlUtility.parse(app.webURL);
	    		localStorage.setItem("custom_apps_home", url.host);
	    	}
	    	
	    	if(angular.equals(applic.appCode,'ARCT')){
	    		applic.meshAppLogo = environment.altRecruitImagePath;
	    		
	    	}
	    	
	    	if (angular.equals(applic.appCode, 'AHOME')) {
				applic.meshAppLogo = '../../../assets/images/alt_home_icon.png';
			}
	    	
	    	let appTyp=app.appType;
		    if(myhash.get(appTyp)==null){
		    	let apps=new Array();
		    	myhash.set(appTyp,apps);
		    }
		    myhash.get(appTyp).push(applic);
		}
		this.applications=new Array();
		myhash.forEach((value: Application[], key: string) => {
    
		   
		   let cat:Category=new Category();
		   cat.categoryName=key;		   
		   let catApps:CategoryApps=new CategoryApps();
		   catApps.category=cat;
		   catApps.apps=value;
		   this.applications.push(catApps);
		   
		});
	  }
	  

		
  
  }
  
    callApp(url: string, identifier: string, appType: string, appCode:string) {
      localStorage.setItem("appUrl", url);
      localStorage.setItem("customerIdentifier", identifier);
      localStorage.setItem("appType", appType);
      this.meshAppService.loadAppUrl(appCode, false);
    }

  getApps() {
    this.appsListService.getAll().subscribe(
      res => {
        this.applications = res;
      });
  }
  
  setApps(){
  	this.applications=this.appsListService.getAppsValue();
  }
}

export class AppsModel {
  
    appName : string;
    appIconImage : string;
    appCode : string;
    webEnabled : boolean;
    webURL : string;
    androidEnabled : boolean;
    androidURL : string;
    androidPackage : string;
    iosEnabled : boolean;
    iosURL : string;
    iosPackage : string;
    appType : string;
    altHomeImage : string;
    identifier : string; 
    
   /* static mesh_urls={"sohum":"https://hrms-admin.sohum.com/services/apps", "staging":"https://staging-hrms-admin.peoplestrong.com/services/apps",
	"uat":"https://uat-hrms-admin.peoplestrong.com/services/apps", "prod":"https://hrms-admin.peoplestrong.com/services/apps"};
    */
 
    constructor(){
        this.appName = "";
        this.appIconImage = "";
        this.appCode = "";
        this.webEnabled = false;
        this.webURL = "";
        this.androidEnabled = false;
        this.androidURL = "";
        this.androidPackage = "";
        this.iosEnabled = false;
        this.iosURL = "";
        this.iosPackage = "";
        this.appType = "";
        this.identifier="";
    }

}
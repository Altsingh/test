import { Application } from "../models/application.model";

export interface CategoryAppsHash {
    [details: string] : Application[];
} 
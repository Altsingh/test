import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CategoryApps } from '../models/category-apps.model';
import { AppsListService } from '../services/apps-list.service';
import { Router } from "@angular/router";
import { AppsInputTO } from "./appsInput";
import { AppsModel } from './apps.model';
import { Application } from "../models/application.model";
import { Category } from "../models/category.model";
import { Http, RequestOptions, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import { MeshAppService } from '../meshapps/meshapp.service';
import { UrlUtility } from '../urls/url-utility';
import { UrlObject } from '../urls/UrlObject';
import { EmpInfoModel } from '../empInfo/empInfo.model';
import { EmpInfoInputTO } from '../empInfo/empInfoInput';
import { EmpInfoService } from '../empInfo/emp-info.service';
import { UserInfoService } from '../userInfo/userinfo.service';
declare const Keycloak: any;
declare var angular: any;
declare var $: any;

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
	inputs: ['keycloakRefreshToken']

})

export class HomeComponent implements OnInit/*, OnChanges*/ {
	webAppsMap: CategoryApps[];
	mobileAppsMap: CategoryApps[];
	applicationsList: AppsModel[];
	applicationsList1: AppsModel[];
	errorMessage: string;
	orgInfoUrl: string;
	userId: number;
	organizationId: number;
	tenantId: number;
	keycloakEnabled: boolean;
	keycloakUrl: string;
	keycloakRealm: string;
	keycloakClient: string;
	keycloakRefreshToken: string;
	timer: any;
	timer1: any;
	username: string;
	defaultAppCode: string;
	webApps: Application[];
	mobileApps: Application[];
	empInfoValue: EmpInfoModel;
	empInfoUrl:string;
	userDefaultAppCode:string;

	options: RequestOptions;
	constructor(private appsListService: AppsListService, private meshAppService: MeshAppService,
		protected http: Http, private urlUtility: UrlUtility, private empInfoService: EmpInfoService, private userInfoService:UserInfoService) { }



	ngOnInit() {



		console.log("inside home component ngOnInit");
		this.orgInfoUrl = environment.orgInfoUrl;
		this.options = new RequestOptions({
			headers: new Headers({
				'Content-Type': 'application/json'

			})
		});
		this.getOrganizationInfo();
		this.timer = setInterval(() => {    //<<<---    using ()=> syntax
			if(window.navigator.userAgent.indexOf("Edge") > -1){
				if (localStorage.getItem('userAppsData')!='null') {
					//this.username = localStorage.getItem('username');
					console.log('userAppsData '+localStorage.getItem('userAppsData'));
					var data = JSON.parse(localStorage.getItem('userAppsData'));
					this.applicationsList=data;
					console.log('home data '+data);
					this.convertToCategoryWiseAppsModel();
					//localStorage.setItem('userAppsData',null);
					clearInterval(this.timer);
				}
				else {
					
					//console.log('running timer for home userAppsData');
			
				}
			}else{
			console.log('stopping timer for userAppsData as Browser is not Edge');
			clearInterval(this.timer);
		}

		}, 10);
	}
	
	updateDefaultAppForUser(username:string, appCode:string){
		
		let abc:any=this.userInfoService.postUserInfo(username, appCode);
		if(abc.responseText && angular.equals(abc.responseText,'Success')){
			return true;
		}else{
			return false;
		}	
	}
	
	  callAppOptions(): RequestOptions {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return options;
  }
  
  getAppByAppCode(appCode:string){
  	if(this.webAppsMap[0].apps){
	  	for(let app of this.webAppsMap[0].apps){
	  		if(app.appCode==appCode){
	  			return app;
	  		}
	  	}
	  }
	  
	  if(this.webAppsMap[1].apps){
	  	for(let app of this.webAppsMap[1].apps){
	  		if(app.appCode==appCode){
	  			return app;
	  		}
	  	}
	  }
	  if(this.webAppsMap[2].apps){
	  	for(let app of this.webAppsMap[2].apps){
	  		if(app.appCode==appCode){
	  			return app;
	  		}
	  	}
	  }
	  
  	
  }
	
	toggleDefaultApp(event, app:any){
		let appCode=app.appCode;
		let userInfoFullUrl: string = environment.userInfoUrl;
		console.log('userInfoFullUrl '+userInfoFullUrl);
		localStorage.setItem('currentDefaultAppId',event.currentTarget.id);
		if(event.currentTarget.classList.contains('active')){
			appCode=null;
		}
		let orgId1=localStorage.getItem("orgId");
		
		this.http.post(userInfoFullUrl, {userName:this.username,orgId:orgId1,defaultAppCode:appCode} , this.callAppOptions())
      .subscribe( 
      	res => {
      		
      		console.log('after post');let abc:any= res.json();
      		let abc1:string="default_app_icon_";
      	if(abc.responseText && angular.equals(abc.responseText,'Success')){
      		let eles=document.getElementsByClassName("default_app_icon");
			console.log('event.currenttarget '+event.currentTarget);
			let currentIconEl=document.getElementById(localStorage.getItem('currentDefaultAppId'));
	      if(currentIconEl && eles){
		      for(let ele of eles as any){
		      	//console.log('ele.classList '+ele.classList);
		      	 if(currentIconEl!=ele && ele.classList.contains('active')){  
		      	 	
		      	 	//ele.classList.remove("active");
		      	 	
		      	 	let app1=this.getAppByAppCode(ele.id.substring(abc1.length,ele.id.length));
		      	 	app1.isDefault=false;
		      	 }
		      }
		  }
		  console.log(currentIconEl.classList);
	      if(currentIconEl.classList.contains('active')){
	      	 console.log('its not equals');
	      	// currentIconEl.classList.remove('active');
	      	 app.isDefault=false;
	      	 localStorage.setItem('currentDefaultAppId',null);
	      	 
	      }else{
	      	console.log('its equals equals');
	      	//currentIconEl.classList.add('active');
	      	app.isDefault=true;
	      }
	    }else{
	    	alert('default app could not be updated');
	    }
	
      
      }, error => 
        {alert('default app could not be updated due to error');});
		//if(this.updateDefaultAppForUser(this.username, appCode)){
			
	}

	getOrganizationInfo() {
		console.log((<any>window).location);
		let hostn: string = (<any>window).location.hostname;
		if (hostn.includes('localhost')) {
			hostn = 'test-altone.peoplestrong.com';
		}
		//localStorage.setItem('hostname',hostn);
		let orgInfoFullUrl: string = this.orgInfoUrl + '?url=' + hostn + '&portalType=AltHomePortal';
		this.http.get(orgInfoFullUrl, this.options)
			.map(
			response => response.json()
			).subscribe(
			res => {
				this.organizationId = res.orgId;

				console.log('this.organizationId ' + this.organizationId);
				this.tenantId = res.tenantId;
				console.log('this.tenantId ' + this.tenantId);

				this.keycloakEnabled = res.keycloakEnabled;
				console.log(' this.keycloakEnabled ' + this.keycloakEnabled);
				
	       		if(this.keycloakEnabled && angular.equals(this.keycloakEnabled,true)){
	       		
	       		
					this.keycloakUrl = res.keycloakUrl;
					if (this.keycloakUrl) {
						//alert('set
						localStorage.setItem("keycloakUrl", this.keycloakUrl);
					}
					if (this.organizationId) {
						this.keycloakRealm = this.organizationId.toString();
						this.keycloakClient = this.organizationId.toString();
	
						localStorage.setItem("realm", this.organizationId.toString());
						localStorage.setItem("client", this.organizationId.toString());
						localStorage.setItem("orgId", this.organizationId.toString());
					}
					if (this.tenantId) {
						localStorage.setItem("tenantId", this.tenantId.toString());
					}
					console.log('res.defaultAppCode ' + res.defaultAppCode);
					this.defaultAppCode = res.defaultAppCode;
					if (res.defaultAppCode) {
						localStorage.setItem("defaultAppCode", this.defaultAppCode.toString());
					}else{
						localStorage.setItem("defaultAppCode",null);
					}
					console.log('this.keycloakUrl ' + this.keycloakUrl);
	
					(<any>window)._keycloak = Keycloak({
						"realm": this.keycloakRealm,
						"url": this.keycloakUrl + "auth",
						"clientId": this.keycloakClient
					});
	
					
					(<any>window)._keycloak.init({ flow: 'standard', responseMode: 'fragment','checkLoginIframe': false }).success(function (authenticated) {
						console.log('authenticated ' + authenticated);
						console.log('keycloak.accessToken' + (<any>window)._keycloak.accessToken);
						console.log('keycloak.refreshToken' + (<any>window)._keycloak.refreshToken);
						if ((<any>window)._keycloak.refreshToken) {
							(<any>window)._keycloak.updateToken(-1);
							localStorage.setItem("refresh_token", (<any>window)._keycloak.refreshToken);
							this.keycloakRefreshToken = (<any>window)._keycloak.refreshToken;
							let x = (this.keycloakRefreshToken).split(".");
					        let decoded_refresh_token = atob(x[1]);
					        console.log('decoded refresh token '+JSON.stringify(decoded_refresh_token));
					        var mapValue = JSON.parse(decoded_refresh_token).userId;
					        localStorage.setItem("loggedInUserId",mapValue);
						}
						if ((<any>window)._keycloak.refreshToken == null) {
							(<any>window)._keycloak.login();
						}
	
					});
					
	
					(<any>window)._keycloak.onAuthRefreshSuccess = function () {
						console.log('after onAuthRefreshSuccess');
						//alert('access token after refresh '+(<any>window)._keycloak.token);
						localStorage.setItem("access_token", (<any>window)._keycloak.token);
						let x = ((<any>window)._keycloak.token).split(".");
					    let decoded_access_token = atob(x[1]);
						console.log('decoded access token '+JSON.stringify(decoded_access_token));
						(<any>window)._keycloak.loadUserInfo();
	
	
	
					}
					this.timer1 = setInterval(() => {    //<<<---    using ()=> syntax
	
						if (!this.username) {
							let userInfo1 = (<any>window)._keycloak.userInfo;
							if (userInfo1) {
								this.username = (<any>window)._keycloak.userInfo.preferred_username;
								localStorage.setItem("username", this.username);
								console.log('uername '+this.username);
								//console.log('userinfo '+JSON.stringify((<any>window)._keycloak.userInfo));
								//localStorage.setItem("loggedInUserId",(<any>window)._keycloak.userInfo.userid);
								this.getUserInfo();
							
								this.getEmployeeInfo();
							}
						}
						else {
							clearInterval(this.timer1);
						}
	
					}, 10);
	
	
					/*(<any>window)._keycloak.onAuthLogout=function(){
						(<any>window)._keycloak.LocalStorage.clearExpired();
					}*/
	
					
				}else{
					alert('Keycloak is not enabled for Organiation ID '+ this.organizationId);
				}

			});

	}

	getEmployeeInfo() {
		//alert('fetching user info');
		this.empInfoUrl = environment.employeeInfo;
		let appsInputTo: EmpInfoInputTO = new EmpInfoInputTO(this.username,
			this.organizationId);
		this.empInfoService.getEmpInfo(appsInputTo)
			.subscribe(res => {
				this.empInfoValue = res;
				console.log('this.empInfoValue '+this.empInfoValue.photoPath);
				if(this.empInfoValue.photoPath){
					document.getElementsByClassName('header_profile_pic')[0].setAttribute("src",this.empInfoValue.photoPath);
				}else{
					document.getElementsByClassName('header_profile_pic')[0].setAttribute("src","../../../assets/images/default_img.png");
				}
				/*if (!(this.redirectToDefaultApp() == true)) {
					this.applicationsList = res;
					this.convertToCategoryWiseAppsModel();
					console.log('applications' + this.applications);
				}*/

			},
			(error) => this.errorMessage = error);
	}

	getUserInfo(){
	
	//this.user = environment.employeeInfo;
		/*let appsInputTo: EmpInfoInputTO = new EmpInfoInputTO(this.username,
			this.organizationId);*/
		this.userInfoService.getUserInfo().subscribe(res => {
				this.userDefaultAppCode = res.appCode;
				this.getApps();
				//console.log('this.empInfoValue '+this.empInfoValue.photoPath);
				/*if(this.empInfoValue.photoPath){
					document.getElementsByClassName('header_profile_pic')[0].setAttribute("src",this.empInfoValue.photoPath);
				}else{
					document.getElementsByClassName('header_profile_pic')[0].setAttribute("src","../../../assets/images/default_img.png");
				}*/
				/*if (!(this.redirectToDefaultApp() == true)) {
					this.applicationsList = res;
					this.convertToCategoryWiseAppsModel();
					console.log('applications' + this.applications);
				}*/

			},
			(error) => this.errorMessage = error);
	}



	callApp(url: string, identifier: string, appType: string, appCode:string, isDefaultAppCall:boolean) {
		localStorage.setItem("appUrl", url);
		localStorage.setItem("customerIdentifier", identifier);
		localStorage.setItem("appType", appType);
		this.meshAppService.loadAppUrl(appCode,isDefaultAppCall);
	}

	fetchUserIdFromKeycloak(){
	  		
		console.log('fetchUserIdFromKeycloak refresh_token ' + localStorage.getItem('refresh_token'));
	
	}

	redirectToDefaultApp(){
		let url: UrlObject = this.urlUtility.parse((<any>window).location.href);
		if(url.query){
			let appRedirectValue=url.query['appRedirected'];
			if(appRedirectValue && angular.equals(appRedirectValue,"1")){
				console.log('appredirected found 1, not redirecting to default app');
				return false;
			}
			let redirectDefaultValue=url.query['redirectDefault'];
			if(redirectDefaultValue && angular.equals(redirectDefaultValue,"false")){
				console.log('redirectDefault found false, not redirecting to default app');
				return false;
			}
		}
		  	
		let redirected: boolean = false;
		let redirectUrl: string;
		let redirectIdent: string;
		let redirectAppType: string;
		let defaultApp:string;
		if(this.userDefaultAppCode){
			defaultApp=this.userDefaultAppCode;
		}else {
			if(localStorage.getItem("defaultAppCode")){
				defaultApp=localStorage.getItem("defaultAppCode");	
			}
		}
		if (defaultApp) {
			if(angular.equals(defaultApp,'AHOME')){
				return false;
			}else{
				if (this.applicationsList1 != null) {
					for (let app of this.applicationsList1) {
					
						if (angular.equals(app.appCode, defaultApp)) {
							//foundDefault=true;
							redirectUrl = app.webURL;
							redirectIdent = app.identifier;
							redirectAppType = app.appType;
							this.callApp(redirectUrl, redirectIdent, redirectAppType, app.appCode, true);
							return true;
						}
					}
				}
			}
		}
		return redirected;
	}
	
	convertToCategoryWiseAppsModel(){
		let defaultApp = localStorage.getItem("defaultAppCode");
		let foundDefault = false;
		let redirectUrl: string;
		let redirectIdent: string;
		let redirectAppType: string;
		let myhash: Map<string, Application[]> = new Map<string, Application[]>();
		let myhash1: Map<string, Application[]> = new Map<string, Application[]>();
		this.webAppsMap = new Array(3);
		this.mobileAppsMap = new Array(3);
		if (this.applicationsList != null) {
			for (let app of this.applicationsList) {
	
				let applic = new Application();
				applic.appName = app.appName;
				applic.appLogo = app.appIconImage;
				applic.appUrl = app.webURL;
				applic.identifier = app.identifier;
				applic.appType = app.appType;
				applic.meshAppLogo = app.appIconImage;
				applic.appCode = app.appCode;
				applic.webEnabled = app.webEnabled;
				applic.androidEnabled = app.androidEnabled;
				applic.iosEnabled = app.iosEnabled;
				applic.andriodUrl = app.androidURL;
				applic.iosUrl = app.iosURL;
				console.log(' app name ' + applic.appName + ' logo ' + applic.appLogo + ' app code ' + app.appCode);
				if (angular.equals(applic.appCode, 'AORG')) {
					console.log('setting custom_apps_home');
	
					let url: UrlObject = this.urlUtility.parse(app.webURL);
					localStorage.setItem("custom_apps_home", url.host);
				}
				
				if(angular.equals(applic.appCode,this.userDefaultAppCode)){
					applic.isDefault=true;
				}
				
				if (angular.equals(applic.appCode, 'ARCT')) {
					applic.appLogo = environment.altRecruitImagePath;
				}
				if (angular.equals(applic.appCode, 'AHOME')) {
					applic.appLogo = '../../../assets/images/alt_home_icon.png';
				}
				
	
				if (defaultApp) {
					if (angular.equals(applic.appCode, defaultApp)) {
						foundDefault = true;
						redirectUrl = applic.appUrl;
						redirectIdent = applic.identifier;
						redirectAppType = applic.appType;
					}
				}
				console.log(' app name ' + applic.appName + ' logo ' + applic.appLogo);
				let appTyp = app.appType;
				
				
				if (applic.webEnabled == true) {
					if (myhash.get(appTyp) == null) {
						let apps = new Array();
						myhash.set(appTyp, apps);
					}
					myhash.get(appTyp).push(applic);
					//this.webApps.push(applic);
				}
				if (applic.iosEnabled == true || applic.androidEnabled==true) {
					
					if (myhash1.get(appTyp) == null) {
						let apps = new Array();
						myhash1.set(appTyp, apps);
					}
					myhash1.get(appTyp).push(applic);
					//this.mobileApps.push(applic);
					
				}
	
			}
			//this.applications = new Array();
			myhash.forEach((value: Application[], key: string) => {
	
				//console.log('key '+key);
				let cat: Category = new Category();
				
	
	
				let catApps: CategoryApps = new CategoryApps();
				catApps.category = cat;
				catApps.apps = value;
				if(key.indexOf("Alt")>=0){
					//cat.categoryName = "Alt";
					this.webAppsMap[0]=catApps;
					this.webAppsMap[0].category.categoryName="Alt";
					console.log('set alt apps');
				}else if(key.indexOf("Partner")>=0){
					//cat.categoryName = "Partner";
					this.webAppsMap[1]=catApps;
					this.webAppsMap[1].category.categoryName="Partner";
					
					console.log('set partner apps');
					
				}else if(key.indexOf("Custom")>=0){
					//cat.categoryName = "Custom";
					
					this.webAppsMap[2]=catApps;
					this.webAppsMap[2].category.categoryName="Custom";
					
					console.log('set custom apps');
					
				}
	
			});
			myhash1.forEach((value: Application[], key: string) => {
	
				//console.log('key1 '+key);
				let cat: Category = new Category();
				
	
	
				let catApps: CategoryApps = new CategoryApps();
				catApps.category = cat;
				catApps.apps = value;
				if(key.indexOf("Alt")>=0){
					cat.categoryName = "Alt";
					this.mobileAppsMap[0]=catApps;
					console.log('set alt mobile apps');
				}else if(key.indexOf("Partner")>=0){
					cat.categoryName = "Partner";
					this.mobileAppsMap[1]=catApps;
					console.log('set partner mobile apps');
					
				}else if(key.indexOf("Custom")>=0){
					cat.categoryName = "Custom";
					this.mobileAppsMap[2]=catApps;
					console.log('set custom mobile apps');
					
				}
				//this.mobileAppsMap.push(catApps);
	
			});
			for(let i =0; i<3;i++){
				if(!this.webAppsMap[i]){
					this.webAppsMap[i]=new CategoryApps();
				}
			}
			
			for(let i=0; i<3;i++){
				if(!this.mobileAppsMap[i]){
					this.mobileAppsMap[i]=new CategoryApps();
				}
			}
		}
		console.log('foundDefault ' + foundDefault);
		/*if(angular.equals(foundDefault,true)){
			console.log('default redirecting');
			this.callApp(redirectUrl, redirectIdent, redirectAppType, true);
			
		}*/
		//console.log('end convert applications ' + this.applications);
	
	}
	
	fetchUserId(username:string){
		this.userId = 2398117;
	}
	
	getApps(){
	  
		let appsInputTo: AppsInputTO = new AppsInputTO(this.username,
			this.organizationId, this.tenantId);
		let abc=this.appsListService.getAllApps(appsInputTo);
		if(abc){
		
			abc.subscribe(res => {
				this.applicationsList1 = res;
				if (!(this.redirectToDefaultApp() == true)) {
					this.applicationsList = res;
					this.convertToCategoryWiseAppsModel();
					
					//console.log('applications' + this.applications);
				}
	
			},
			(error) => this.errorMessage = error);
		}
	}
   
}

import { Component, OnInit } from '@angular/core';
import { LoginHttpService } from '../services/login-http.service';
import { LoginModel } from '../models/login.model';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginModal: LoginModel;
  loginFailed = false;
  
  constructor(private loginHttpService: LoginHttpService,
              private router:Router) { }

  ngOnInit() {
    this.loginModal = new LoginModel();
    this.loginModal.companyURL = environment.companyURL;
  }

  submit() {
    this.loginHttpService.login(this.loginModal)
      .subscribe(
      res => {
        if (res.status == 200) {
          localStorage.setItem("user",res.json().jwt);
          //this.router.navigate(['']);
        }
        else {
          this.loginFailed = true;
        }
      },
    error=>{
      this.loginFailed = true;
    })
  }

}

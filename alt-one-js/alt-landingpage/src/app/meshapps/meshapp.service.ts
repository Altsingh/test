import { Component, OnInit } from '@angular/core';
//import { NotificationService, NotificationSeverity } from 'app/common/notification-bar/notification.service';
//import { CommonService } from 'app/common/common.service';
import { Http, Response, Headers, RequestOptions, ResponseOptions } from '@angular/http';
//import { DialogService } from '../../shared/dialog.service';
import { Injectable } from '@angular/core';
//import { SessionService } from '../../session.service';
declare var angular: any;

@Injectable()
export class MeshAppService {
keycloakBaseUrl :string;
			realm : string;
			client :string;
			refreshToken : string;
			accessToken : string;
			callbackUrl : string; 
			ssoUrl: string;
  reportList: any;

  constructor(
    /*private notificationService: NotificationService,
    private commonService: CommonService,
    private dialogService: DialogService,
    private sessionService: SessionService*/
    private http: Http
  ) {
   // this.commonService.hideRHS();
  }

  loadAppUrl(appCode:string,isDefaultAppCall:boolean) {
	if(appCode && appCode=='AHOME'){
		if(angular.equals(isDefaultAppCall,true)){
			return;
		}      			
	}
    console.log('inside meshapp module');
    this.keycloakBaseUrl = localStorage.getItem("keycloakUrl");
	this.realm = localStorage.getItem("realm");
	this.client = localStorage.getItem("client");
	this.refreshToken = localStorage.getItem("refresh_token");
	this.ssoUrl = this.keycloakBaseUrl+ 'auth/realms/' + this.realm + '/protocol/openid-connect/token';
	this.callbackUrl = this.keycloakBaseUrl + 'auth/realms/' + this.realm + '/protocol/openid-connect/userinfo';
    this.keycloakSSO1(appCode,isDefaultAppCall);
  }
 private getKeycloakHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'POST');
    headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Authorization', 'Basic wipro_user:ieSV55Qc+eQOaYDRSha/AjzNTJE=');
    return headers;
  }
  
  keycloakSSO1(appCode:string,isDefaultAppCall:boolean) {
    //let baseUrl = this.keycloakUrl;

   // let realm = this.realm;
    //let client = this.clientId;

   /* if (realm == '290' || realm == '325') {
      realm = 'RoyalEnfield';
      client = 'OktaClient';
    }*/

    //let refresh_token = this.keycloakRefreshToken;
    let tokenUrl = this.ssoUrl;//baseUrl + 'auth/realms/' + realm + '/protocol/openid-connect/logout';
    //this.keycloakLogout1(client,refresh_token,tokenUrl);
    let req = new XMLHttpRequest();
    let params = 'grant_type=refresh_token&client_id=' + this.client + '&refresh_token=' + this.refreshToken;
    req.open('POST', tokenUrl, true);
    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    req.send(params);
    (<HTMLInputElement>document.getElementById("CallbackURL")).value = this.callbackUrl;
    req.onreadystatechange = function() {
		if(req.readyState == 4 && req.status == 200) {
			var data = JSON.parse(req.responseText);
			localStorage.setItem("refresh_token",data['refresh_token']);
			
	      	var appType = localStorage.getItem("appType");
			var url = localStorage.getItem("appUrl");
			
			if(url.includes("creator")|| url.includes("builder") || appType=='Custom App'){
				
	       		(<HTMLFormElement>document.getElementById("altAppFormCreator")).action = "https://"+localStorage.getItem("custom_apps_home")+"/AltOneCreaterCaller";
	       		(<HTMLInputElement>document.getElementById("accessTokenn")).value =data['access_token'];;
	       		(<HTMLInputElement>document.getElementById("refreshTokenn")).value = data['refresh_token'];
	       		(<HTMLInputElement>document.getElementById("url")).value = url;
	       		(<HTMLInputElement>document.getElementById("orgId")).value = localStorage.getItem("orgId");
	       		(<HTMLInputElement>document.getElementById("tenantId")).value = localStorage.getItem("tenantId");
	       		if(!angular.equals(isDefaultAppCall,true)){
	       			(<HTMLFormElement>document.getElementById("altAppFormCreator")).setAttribute("target","_blank");
	       		}
	       		(<HTMLFormElement>document.getElementById("altAppFormCreator")).submit();
	       	}
	       	else if(appType=='Alt App') {

	       		console.log('appcode here '+appCode);
	       		if(appCode && angular.equals(appCode,'AHOME')){

	       			window.open(localStorage.getItem("appUrl")+"?appRedirected=1", '_blank');
	       			return;
	       		}else{
	       			console.log('appcode  is not althome');
	       		}
	       		(<HTMLFormElement>document.getElementById("altAppForm")).action = localStorage.getItem("appUrl");
	       		(<HTMLInputElement>document.getElementById("accessToken")).value = data['access_token'];
	       		console.log('ret access token '+data['access_token']);
	       		(<HTMLInputElement>document.getElementById("refreshToken")).value = data['refresh_token'];
	       		if(!angular.equals(isDefaultAppCall,true)){
	       			(<HTMLFormElement>document.getElementById("altAppForm")).setAttribute("target","_blank");
	       		}
	       		(<HTMLFormElement>document.getElementById("altAppForm")).submit();
	       	} else {
		       	(<HTMLFormElement>document.getElementById("partnerAppForm")).action = localStorage.getItem("appUrl");
		       	(<HTMLInputElement>document.getElementById("CustomerIdentifier")).value = localStorage.getItem("customerIdentifier");			       		
				(<HTMLInputElement>document.getElementById("AccessToken")).value = data['access_token'];
				
				
				let d = new Date();
				var d1 = [
				'0' + d.getDate(),
				'0' + (d.getMonth() + 1),
				'' + d.getFullYear(),
				'0' + d.getHours(),
				'0' + d.getMinutes(),
				'0' + d.getSeconds()
				].map(component => component.slice(-2)); // take last 2 digits of every component
				var fDate = d1.slice(0, 3).join('.') + ' ' + d1.slice(3).join(':');
				(<HTMLInputElement>document.getElementById("Timestamp")).value = fDate.toString();
				if(!angular.equals(isDefaultAppCall,true)){
	       			(<HTMLFormElement>document.getElementById("partnerAppForm")).setAttribute("target","_blank");
	       		}
				(<HTMLFormElement>document.getElementById("partnerAppForm")).submit();
	       	}
	   	}
       	 
	}

  }
  
  
  keycloakSSO() {

    var data = {
    
    grant_type:'refresh_token',

      client_id: this.client,

      refresh_token: this.refreshToken,
    };
	console.log('client_id '+this.client);
	console.log('refresh_token '+this.refreshToken);
	console.log('ssoUrl '+this.ssoUrl);

    //Call the services
    let options = new RequestOptions({ headers: this.getKeycloakHeaders() });
    this.http.post(this.ssoUrl, JSON.stringify(data), options).map((res) => {
      //alert(res.json());
      console.log('res json '+res.json());
      var appType = localStorage.getItem("appType");
		var url = localStorage.getItem("appUrl");
		
		if(url.includes("creator") || appType=='Custom App'){
       		(<HTMLFormElement>document.getElementById("altAppFormCreator")).action = "/AltOneCreaterCaller";
       		(<HTMLInputElement>document.getElementById("accessTokenn")).value = this.refreshToken;
       		(<HTMLInputElement>document.getElementById("refreshTokenn")).value = data.refresh_token;
       		(<HTMLInputElement>document.getElementById("url")).value = url;
       		(<HTMLInputElement>document.getElementById("orgId")).value = localStorage.getItem("orgId");
       		(<HTMLInputElement>document.getElementById("tenantId")).value = localStorage.getItem("tenantId");
       		(<HTMLFormElement>document.getElementById("altAppFormCreator")).submit();
       	}
       	else if(appType=='Alt App') {
       		(<HTMLFormElement>document.getElementById("altAppForm")).action = localStorage.getItem("appUrl");
       		(<HTMLInputElement>document.getElementById("accessToken")).value = data['access_token'];
       		(<HTMLInputElement>document.getElementById("refreshToken")).value = data['refresh_token'];
       		(<HTMLFormElement>document.getElementById("altAppForm")).submit();
       	} else {
	       	(<HTMLFormElement>document.getElementById("partnerAppForm")).action = localStorage.getItem("appUrl");
	       	(<HTMLInputElement>document.getElementById("CustomerIdentifier")).value = localStorage.getItem("customerIdentifier");			       		
			(<HTMLInputElement>document.getElementById("AccessToken")).value = data['access_token'];
			(<HTMLInputElement>document.getElementById("CallbackURL")).value = this.callbackUrl;
			
			let d = new Date();
			var d1 = [
			'0' + d.getDate(),
			'0' + (d.getMonth() + 1),
			'' + d.getFullYear(),
			'0' + d.getHours(),
			'0' + d.getMinutes(),
			'0' + d.getSeconds()
			].map(component => component.slice(-2)); // take last 2 digits of every component
			var fDate = d1.slice(0, 3).join('.') + ' ' + d1.slice(3).join(':');
			(<HTMLInputElement>document.getElementById("Timestamp")).value = fDate.toString();
			
			(<HTMLFormElement>document.getElementById("partnerAppForm")).submit();
       	}
       	 
			       	
    });
  }
  

}

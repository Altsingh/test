export class Application{
  appId:string;
  appName:string;
  appLogo:string;
  appUrl:string;
  meshAppLogo:string; 
   identifier:string;
   appType:string;
   appCode:string;
   webEnabled : boolean;
    androidEnabled : boolean;
    iosEnabled : boolean;
    andriodUrl:string;
    iosUrl:string;
    isDefault:boolean;
  
  constructor(){
    this.appId="";
    this.appName="";
    this.appLogo="";
    this.appUrl="";   
    this.meshAppLogo="";
    this.identifier="";
    this.appType="";
    this.appCode="";
  }

}

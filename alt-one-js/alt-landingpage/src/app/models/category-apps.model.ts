import { Category } from "./category.model";
import { Application } from "./application.model";


export class CategoryApps{
  category:Category;
  apps:Application[];

  constructor(){
    this.apps=[];
    this.category=null;
  }

}
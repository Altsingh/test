export class LoginModel{
    userName:string
    password:string
    companyURL:string
    portalType:string
    deviceName:string
    osName:string
    version:string
    appVersion:string
    userNameHash:string
    passwordHash:string

    constructor(){
        this.userName=""
        this.password=""
        this.companyURL="hrms.peoplestrong.com"
        this.portalType="Employee Portal"
        this.deviceName=""
        this.osName="Android"
        this.version=""
        this.appVersion="worklife-2.2"
        this.userNameHash=""
        this.passwordHash=""
    }
}
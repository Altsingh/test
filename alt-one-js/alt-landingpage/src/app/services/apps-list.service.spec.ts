import { TestBed, inject } from '@angular/core/testing';

import { AppsListService } from './apps-list.service';

describe('AppsListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppsListService]
    });
  });

  it('should be created', inject([AppsListService], (service: AppsListService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { DataService } from './data-service';
import { environment } from '../../environments/environment';
import { Http, Response, Headers, RequestOptions, ResponseOptions } from '@angular/http';
import { AppsInputTO } from "../home/appsInput";
import { Observable } from "rxjs/Observable";
import { AppsModel } from '../home/apps.model';
import { CategoryApps } from '../models/category-apps.model';
import { Application } from "../models/application.model";
import { Category } from "../models/category.model";
import { UrlUtility } from '../urls/url-utility';
import { UrlObject } from '../urls/UrlObject';
declare var angular: any;
declare const XDomainRequest: any;
@Injectable()
export class AppsListService extends DataService {
	applicationsList:AppsModel[];
    applications: CategoryApps[];
    appsModelObservable:Observable<any>;
  	constructor(http: Http, private urlUtility: UrlUtility) {
    	super(environment.appsListUrl, http);
    	localStorage.setItem('userAppsData',null);
   	}
   
	getAllApps(appsInputTO: AppsInputTO): Observable<any>  {
		let body = `orgId=${appsInputTO.orgId}&tenantId=${appsInputTO.tenantId}&userName=${appsInputTO.userName}`;
    	console.log('inside getAllApps url ' + this.url + ' body ' + body);


		if(window.navigator.userAgent.indexOf("Edge") > -1){
			this.makeRequestForMicrosoftEdge(body);
		}else{
	
    		this.appsModelObservable=this.http.post(this.url, body, this.callAppOptions())
      		.map(res => res.json());
      		this.appsModelObservable.subscribe(res1 => {  
      		this.applicationsList=res1;
        	this.convertToCategoryWiseAppsModel();});
      		return this.appsModelObservable;
     	}
     
     
   	}
   
   	makeRequestForMicrosoftEdge(body){
   		console.log('sending makeRequestForMicrosoftEdge');
	   	var req = this.createCORSRequest('POST', this.url);
		if (!req) {
	  		throw new Error('CORS not supported');
		}
		req.open('POST', this.url, true);
		//let params = 'grant_type=refresh_token&client_id=' + this.client + '&refresh_token=' + this.refreshToken;
	   	// req.open('POST', tokenUrl, true);
	    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		//req.withCredentials = true;
	    //
	   	// (<HTMLInputElement>document.getElementById("CallbackURL")).value = this.callbackUrl;
	    req.onreadystatechange = function() {
	       	console.log('req.readyState '+req.readyState+' req.status '+req.status);
			if(req.readyState == 4 && req.status == 200) {
				//this.url+'_'+appsInputTO.orgId+'_'+appsInputTO.tenantId+'_'+appsInputTO.userName
				localStorage.setItem('userAppsData',req.responseText);
				console.log(req.responseText);
				/*var data = JSON.parse(req.responseText);
				this.applicationsList=data;
				console.log('data '+data);
				this.convertToCategoryWiseAppsModel();*/
			}
   		}
   		req.send(body);
   	}
   
	createCORSRequest(method, url) {
  		var xhr = new XMLHttpRequest();
  		if ("withCredentials" in xhr) {
			console.log('withCredentials in xhr');
	    	// Check if the XMLHttpRequest object has a "withCredentials" property.
	    	// "withCredentials" only exists on XMLHTTPRequest2 objects.
	   	 	

  		} else if (typeof XDomainRequest != "undefined") {
			console.log('typeof XDomainRequest != undefined');

		    // Otherwise, check if XDomainRequest.
		    // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
		    xhr = new XDomainRequest();
		    xhr.open(method, url);

  		} else {

		    // Otherwise, CORS is not supported by the browser.
		    xhr = null;

		}
		return xhr;
	}


    
    
  	callAppOptions(): RequestOptions {
    	const headers = new Headers();
    	headers.append('Content-Type', 'application/x-www-form-urlencoded');
    	let options = new RequestOptions({ headers: headers });
    	return options;
  	}
  
  	static handleError(error: Response | any) {
    	let errMsg: string;
    	if (error instanceof Response) {
      		const body = error.json() || '';
      		const err = body.error || JSON.stringify(body);
      		errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    	} else {
      		errMsg = error.message ? error.message : error.toString();
    	}
    	console.error(errMsg);
    	return Observable.throw(errMsg);
  	}

	convertToCategoryWiseAppsModel(){
		console.log('inside convertToCategoryWiseAppsModel');
	  	let myhash : Map<string, Application[]> = new Map<string, Application[]>();
	  	if (this.applicationsList != null) {
			for (let app of this.applicationsList) {
		    
		    	let applic=new Application();
		    
		    	applic.appName = app.appName;
				applic.appLogo = app.appIconImage;
				applic.appUrl = app.webURL;
				applic.identifier = app.identifier;
				applic.appType = app.appType;
				applic.meshAppLogo = app.appIconImage;
				applic.appCode = app.appCode;
				applic.webEnabled = app.webEnabled;
				applic.androidEnabled = app.androidEnabled;
				applic.iosEnabled = app.iosEnabled;
				applic.andriodUrl = app.androidURL;
				applic.iosUrl = app.iosURL;
		    
	    	
	    	applic.meshAppLogo=app.appIconImage;
	    	console.log(' app name '+applic.appName+' logo '+applic.appLogo+' app code '+app.appCode);
	    	console.log(' app name '+applic.appName+' logo '+applic.appLogo+' app code '+app.appCode);
	    	if(angular.equals(app.appCode,'AORG')){
	    		console.log('setting custom_apps_home');
	    		
	    		let url: UrlObject = this.urlUtility.parse(app.webURL);
	    		localStorage.setItem("custom_apps_home", url.host);
	    	}
	    	
	    	if(angular.equals(applic.appCode,'ARCT')){
	    		applic.meshAppLogo = environment.altRecruitImagePath;
	    		
	    	}
	    	
	    	if (angular.equals(applic.appCode, 'AHOME')) {
				applic.meshAppLogo = '../../../assets/images/alt_home_icon.png';
			}
	    	
	    	let appTyp=app.appType;
		    if(myhash.get(appTyp)==null){
		    	let apps=new Array();
		    	myhash.set(appTyp,apps);
		    }
		    myhash.get(appTyp).push(applic);
		}
		this.applications=new Array();
		myhash.forEach((value: Application[], key: string) => {
    
		   
		   let cat:Category=new Category();
		   cat.categoryName=key;		   
		   let catApps:CategoryApps=new CategoryApps();
		   catApps.category=cat;
		   catApps.apps=value;
		   this.applications.push(catApps);
		   
		});
	  }
	  

		
  
  }
  
  getAppsValue(): CategoryApps[]{
  	return this.applications;
  }
  
}

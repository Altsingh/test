import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  public options: RequestOptions;

  constructor(protected url: string, protected http: Http) {

      this.options = new RequestOptions({
        headers: new Headers({
          'Content-Type': 'application/json',
        })
      });
  }

  getAll() {
      return this.http.get(this.url, this.options)
        .map(
        response => response.json()
        );
    }

}

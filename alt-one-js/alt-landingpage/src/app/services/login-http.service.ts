import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import { environment } from "../../environments/environment";
import { Observable } from 'rxjs/Rx';
import { error } from 'util';
import 'rxjs/add/operator/map'

@Injectable()
export class LoginHttpService {
  private url: string = environment.loginUrl;
  constructor(private http: Http) { }

  login(loginModal) {
    return this.http.post(this.url+"/login", loginModal, null)
      .map(res => {
        return res;
      }, error => {
        return error;
      })
  }
    
}
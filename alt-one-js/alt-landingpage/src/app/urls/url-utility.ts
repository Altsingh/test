//url-utility.ts
import { UrlObject } from './UrlObject';
import { Injectable } from '@angular/core';
import * as urlParse from 'url-parse';

@Injectable()
export class UrlUtility {
  public parse(url: string, parseQuery= true): UrlObject {
      return urlParse(url, parseQuery);
  }
}
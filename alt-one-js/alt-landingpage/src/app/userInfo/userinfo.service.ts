import { Injectable } from '@angular/core';
import { DataService } from '../services/data-service';
import { environment } from '../../environments/environment';
import { Http, Response, Headers, RequestOptions, ResponseOptions } from '@angular/http';
import { Observable } from "rxjs/Observable";
//import { CategoryApps } from '../models/category-apps.model';
//import { Application } from "../models/application.model";
//import { Category } from "../models/category.model";
//import { UrlUtility } from '../urls/url-utility';
//import { UrlObject } from '../urls/UrlObject';
//declare var angular: any;

@Injectable()
export class UserInfoService extends DataService {

     appsModelObservable:Observable<any>;
  userInfoUrl:string;
  userInfoObservable:Observable<any>;
  constructor(http: Http) {
    super(environment.userInfoUrl, http);
    this.userInfoUrl=environment.userInfoUrl;
   }
   
  getUserInfo() {
		
		//localStorage.setItem('hostname',hostn);
		let userInfoFullUrl: string = this.userInfoUrl + '?username='+localStorage.getItem("username")+'&orgId='+localStorage.getItem("orgId");
		this.userInfoObservable=this.http.get(userInfoFullUrl, this.options)
			.map(
			response => response.json()
			);
		 return this.userInfoObservable;
	}
	
	
	postUserInfo(username:string, appCode:String){
		let userInfoFullUrl: string = this.userInfoUrl;
		let orgId1=localStorage.getItem("orgId");
		
		this.http.post(userInfoFullUrl, {userName:username,orgId:orgId1,defaultAppCode:appCode} , this.callAppOptions())
      .map(res => {return res.json()}, error => 
        {return error});
	
	}
    
    
    
  callAppOptions(): RequestOptions {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return options;
  }
  
  static handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

	
  
  /*getAppsValue(): CategoryApps[]{
  	return this.applications;
  }*/
  
}

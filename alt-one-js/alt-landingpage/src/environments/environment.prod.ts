export const environment = {

  //environment
  production: true,

  //backend urls
  loginUrl:"https://gateway.peoplestrong.com/alt-login-0.0.1-SNAPSHOT/api/v1",

  //mocks
  //appsListUrl:"https://gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/mocks/userapps",
	appsListUrl:"https://s2demo-admin.peoplestrong.com/services/apps",
  companyURL:"hrms.peoplestrong.com",
  
  meshUrl: 'https://s2demo-admin.peoplestrong.com/services/apps',
  
  orgInfoUrl:"https://gateway.peoplestrong.com/api/v1/organization/info",
    employeeInfo: "https://s2demo.peoplestrong.com/service/api/employee/employeeInfo",
        altRecruitImagePath:"../../../assets/images/alt-recruit.png",
     userInfoUrl:"https://gateway.peoplestrong.com/api/v1/user/info"
  
  
};

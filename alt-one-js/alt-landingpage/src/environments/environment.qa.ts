// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {

  //environment
  production: false,

  //backend urls
  loginUrl:"http://10.226.0.219:8097/login/api/v1",

  //mocks
  appsListUrl:"http://10.226.0.219:8096/marketplace/api/v1/mocks/userapps",

  companyURL:"hrms.sohum.com",
  
  meshUrl: 'https://hrms-admin.sohum.com/services/apps',
    employeeInfo: "https://s2demo.sohum.com/service/api/employee/employeeInfo"
  
};

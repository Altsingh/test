export const environment = {

  //environment
  production: true,

  //backend urls
  loginUrl:"https://gatewaydc2.peoplestrong.com/alt-login-0.0.1-SNAPSHOT/api/v1",

  //mocks
  //appsListUrl:"https://gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/mocks/userapps",
	appsListUrl:"https://truecorp-admin.peoplestrong.com/services/apps",
  companyURL:"hrms.peoplestrong.com",
  
  meshUrl: 'https://truecorp-admin.peoplestrong.com/services/apps',
  
  orgInfoUrl:"https://gatewaydc2.peoplestrong.com/api/v1/organization/info",
    employeeInfo: "https://truecorp.peoplestrong.com/service/api/employee/employeeInfo",
        altRecruitImagePath:"../../../assets/images/alt-recruit.png",
       userInfoUrl:"https://gatewaydc2.peoplestrong.com/api/v1/user/info"
  
  
};

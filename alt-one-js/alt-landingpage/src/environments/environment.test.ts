// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {

  //environment
  production: false,

  //backend urls
  loginUrl:"https://test-gateway.peoplestrong.com/api/v1/organization",

  //mocks
 // appsListUrl:"https://test-gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT//api/v1/mocks/userapps",
  appsListUrl:"https://hrms-admin.sohum.com/services/apps",
  companyURL:"hrms.sohum.com",
  
  
  //orgInfoUrl:"http://localhost:8081/api/v1/organization/info",
  
  orgInfoUrl:"https://test-gateway.peoplestrong.com/api/v1/organization/info",
  
    employeeInfo: "https://s2demo.sohum.com/service/api/employee/employeeInfo",
      altRecruitImagePath:"../../../assets/images/alt-recruit.png",
      userInfoUrl:"https://test-gateway.peoplestrongalt.com/api/v1/user/info"
      
  
};

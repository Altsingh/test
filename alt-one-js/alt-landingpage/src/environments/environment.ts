// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {

  // environment
  production: false,

  // backend url
  loginUrl:"http://localhost:8081/api/v1/organization",

  // mocks
  //appsListUrl:"http://localhost:8096/marketplace/api/v1/marketplaceapps",
  //appsListUrl:"http://localhost:8096/marketplace/api/v1/mocks/userapps",
  appsListUrl:"http://hrms-admin.sohum.com/services/apps",

  companyURL:"hrms.sohum.com",
  
  orgInfoUrl:"http://localhost:8081/api/v1/organization/info",
 //orgInfoUrl:"https://test-gateway.peoplestrongalt.com/api/v1/organization/info",
  
  meshUrl: 'https://hrms-admin.sohum.com/services/apps',
    employeeInfo: "https://s2demo.sohum.com/service/api/employee/employeeInfo",
    
    //altRecruitImagePath:"https://sohum-static.peoplestrongalt.com/talentpact/server/appdata/system//appImage/alt_recruit.png"
  altRecruitImagePath:"../../../assets/images/alt-recruit.png",
  
   userInfoUrl:"http://localhost:8081/api/v1/user/info"
  

};

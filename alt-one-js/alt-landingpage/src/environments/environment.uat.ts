// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  // environment
  production: false,
  // backend url
  loginUrl:"http://localhost:8097/login/api/v1",
  // mocks
  //appsListUrl:"http://localhost:8096/marketplace/api/v1/marketplaceapps",
  //appsListUrl:"http://localhost:8096/marketplace/api/v1/mocks/userapps",
  appsListUrl:"https://uat-hrms-admin.peoplestrong.com/services/apps",
  companyURL:"uat-hrms.peoplestrong.com",
  orgInfoUrl:"https://uat-gateway.peoplestrong.com/api/v1/organization/info",
  userInfoUrl:"https://uat-gateway.peoplestrong.com/api/v1/user/info",
  employeeInfo: "https://uat-s2demo.peoplestrong.com/service/api/employee/employeeInfo",
  altRecruitImagePath:"../../../assets/images/alt-recruit.png"

};

import { Component, OnInit } from '@angular/core';
import { AdminLoginHttpService } from '../services/http-service/adminLogin-http.service';
import { LoginModel } from '../models/application/login.model';
import { Router } from '@angular/router';
import { environment } from "../../environments/environment";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginModal: LoginModel;
  loginFailed: boolean = false;

  constructor(private loginHttpService: AdminLoginHttpService,
    private router: Router) { }

  ngOnInit() {
    if(localStorage.getItem("adminJwt")){
      this.router.navigate(["/admin"])
    }
    this.loginModal = new LoginModel();
    this.loginModal.companyURL = environment.companyURL;
  }

  submit() {
    this.loginHttpService.login(this.loginModal)
      .subscribe(
      res => {
        if (res.status == 200 && res.json().admin == true) {
          localStorage.setItem("adminJwt", res.json().jwt);
          localStorage.setItem("adminId", res.json().userId);
          this.router.navigate(['adminUser']);
        }
        else {
          this.loginFailed = true;
        }
      },
      error => {
        this.loginFailed = true;
      })
  }

}

import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import 'rxjs/add/operator/distinctUntilChanged';

declare var ga:Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';
  showHeaderFooter: boolean = false;
  
  constructor(private _router:Router){
    _router.events.distinctUntilChanged((previous: any, current: any) => {
            // Subscribe to any `NavigationEnd` events where the url has changed
            if(current instanceof NavigationEnd) {
                return previous.url === current.url;
            }
            return true;
        }).subscribe((x: any) => {
            ga('send', 'pageview', x.url.split('?', 1)[0].split('/', 3).join('/') )
        });

  }

  ngOnInit(){
    localStorage.removeItem("adminJwt");
    this._router.events.subscribe(event => {
      if (event['url'] === '/adminUser/login') {
        this.showHeaderFooter = false;
      } else {
        this.showHeaderFooter = true;
      }
    });
  }
  
}

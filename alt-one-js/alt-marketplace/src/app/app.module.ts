import { BrowserModule } from '@angular/platform-browser';
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { RatingModule } from "ngx-rating";
import { PartnerModule } from "./partner/partner.module";
import { routings } from "./app.routing";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { DndModule } from 'ng2-dnd';

import { AppComponent } from './app.component';
import { LoginComponent } from './admin-login/login.component';
import { HomeComponent } from './home/home.component';
import { ApplicationsComponent } from './applications/applications.component';
import { ApplicationDetailsComponent } from "./applications/application-details/application-details.component";

import { PartnerComponent } from './partner/partner.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ApplicationsListService } from './services/applications-service/applications-list.service';
import { HttpModule } from '@angular/http';
import { CategoryAllAppsComponent } from './applications/category-all-apps/category-all-apps.component';
import { SubCategoryAllAppsComponent } from './applications/subcategory-all-apps/subcategory-all-apps.component';
import { FormsModule } from "@angular/forms";
import { UserEnquiryDetailsService } from './services/applications-service/userEnquiryDetails.service';
import { LoginHttpService } from "./header/login-http.service";
import { AdminLoginHttpService} from "./services/http-service/adminLogin-http.service";
import { AppInfoDataService } from './services/data-service/app-info-data.service';
import { Ng2CarouselamosModule } from "ng2-carouselamos";
import { StarRatingComponent } from './common-components/star-rating/star-rating.component';
import { SafePipe } from "./pipes/safe.pipe";
import { StarRatingModule } from './common-components/star-rating/star-rating.modue';
import { CategoryListService } from './services/applications-service/category-list.service';
import { BannersListService } from './services/applications-service/banners-list.service';
import { ContactComponent } from './contact/contact.component';
import { PartnerLoginGuard } from "../app/services/authentication-services/partner-login-guard.service";
import { ContactDetailsService } from './services/applications-service/contactDetails.service';
import { CreateBannerService } from './services/admin-services/create-banner.service';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ApplicationsComponent,
    FooterComponent,
    HeaderComponent,
    ApplicationDetailsComponent,
    CategoryAllAppsComponent,
    SubCategoryAllAppsComponent,
    SafePipe,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    Ng2CarouselamosModule,
    RatingModule,
    FormsModule,
    PartnerModule,
    NgbModule.forRoot(),
    routings,
    StarRatingModule,
    DndModule.forRoot()
  ],
  providers: [CreateBannerService,PartnerLoginGuard,BannersListService,ApplicationsListService, UserEnquiryDetailsService, LoginHttpService, AppInfoDataService, CategoryListService, AdminLoginHttpService,ContactDetailsService],
  bootstrap: [AppComponent ]

})
export class AppModule { }

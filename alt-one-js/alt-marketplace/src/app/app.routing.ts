import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from './admin-login/login.component';
import { HomeComponent } from "./home/home.component";
import { ApplicationsComponent } from "./applications/applications.component";
import { CategoryAllAppsComponent } from "./applications/category-all-apps/category-all-apps.component";
import { SubCategoryAllAppsComponent } from "./applications/subcategory-all-apps/subcategory-all-apps.component";
import { ApplicationDetailsComponent } from "./applications/application-details/application-details.component";
import { ContactComponent } from "./contact/contact.component";
import { CreateAppComponent } from "./partner/create-app/create-app.component";
import { AdminAppListComponent } from "./partner/admin-app-list/admin-app-list.component";

const routes: Routes = [
    { path: 'partner', loadChildren: 'app/partner/partner.module#PartnerModule' },
    { path: 'admin', loadChildren: 'app/partner/partner.module#PartnerModule' },
    { path: 'home', component: HomeComponent, pathMatch: 'full' },
    { path: 'apps/:appId', component: ApplicationDetailsComponent, pathMatch: 'full' },
    { path: 'apps', component: ApplicationsComponent, pathMatch: 'full' },
    { path: 'contact', component: ContactComponent, pathMatch: 'full' },
    { path: 'category/:categoryId/subcategory/:subCategoryId', component: SubCategoryAllAppsComponent, pathMatch: 'full', data: { draggable: false } },
    { path: 'category/:categoryId', component: CategoryAllAppsComponent, pathMatch: 'full' },
    { path: 'admin/login', component: LoginComponent, pathMatch: 'full' },
    { path: '**', redirectTo: 'home', pathMatch: 'full' }
]

export const routings: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });

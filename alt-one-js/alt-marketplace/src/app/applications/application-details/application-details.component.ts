import { Component, OnInit, Input } from '@angular/core';
import { UserEnquiryDetails } from '../../models/application/userEnquiryDetails';
import { UserEnquiryDetailsService } from "../../services/applications-service/userEnquiryDetails.service";
import { Application } from '../../models/application/application.model';
import { AppInfoDataService } from '../../services/data-service/app-info-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryApplicationsResponse } from '../../models/responses/category-applications-response.model';
import { SubCategoryResponse } from '../../models/responses/subCategoryList.model';
import { CategoryListService } from '../../services/applications-service/category-list.service';

@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.css']
})

export class ApplicationDetailsComponent implements OnInit {
  showLess: boolean=true;
  showListPopUp: boolean;
  userEnquiryDetails: UserEnquiryDetails;
  message: string;
  interval: any;
  successMessage: boolean = false;
  errorMessage: boolean;
  appInfo: Application;
  appsAndCategory: CategoryApplicationsResponse;
  apps: Application[];
  displayFrame: boolean = false;
  videoUrlFlag: boolean = false;
  viewedImage: string;
  viewedImageIndex;

  ngOnInit() {
    this.getApp(this.route.snapshot.params['appId']);
    this.userEnquiryDetails = new UserEnquiryDetails();
  }

  changeApp(appId) {
    this.getApp(appId);
  }

  constructor(private userEnquiryDetailsService: UserEnquiryDetailsService,
    private appInfoDataService: AppInfoDataService,
    private route: ActivatedRoute,
    private categoryListService: CategoryListService,
    private router: Router) {
  }

  getAppsFromCategory(categoryId: string) {
    this.appInfoDataService.getCategory(categoryId).subscribe(
      res => {
        this.appsAndCategory = res;
        this.apps = [];
        for (let i = 0; i < this.appsAndCategory.subCategories.length; i++) {
          for (let j = 0; j < this.appsAndCategory.subCategories[i].apps.length; j++) {
            this.apps.push(this.appsAndCategory.subCategories[i].apps[j]);
          }
        }
      });
  }

  getApp(appId: string) {
    this.appInfoDataService.getAppDetails(appId).subscribe(
      res => {
        this.appInfo = res;
        if (this.appInfo.videoUrl && this.appInfo.videoUrl.indexOf("https://www.youtube.com/embed") > -1) {
          this.appInfo.screenShots.splice(0, 0, this.appInfo.videoUrl);
          this.videoUrlFlag = true;
        }
        this.getAppsFromCategory(this.appInfo.categoryId);
      });
  }

  openViewer(ind) {
    this.viewedImageIndex = ind;
    this.viewedImage = this.appInfo.screenShots[this.viewedImageIndex];
    this.displayFrame = true;
  }

  closeViewer() {
    this.displayFrame = false;
  }

  showModal_getStarted(name) {
    if(name == 'Great Place To Work'){
    window.open("https://survey.greatplacetoworkindia.co.in/2019study", "_blank");} 
  
    else{
    this.userEnquiryDetails = new UserEnquiryDetails();
    this.showListPopUp = true;
   }
  }

  closeModal_getStarted() {
    this.showListPopUp = false;
    this.userEnquiryDetails = new UserEnquiryDetails();

  }

  showPrev() {
    if (this.viewedImageIndex != 0) {
      this.viewedImageIndex = this.viewedImageIndex - 1;
      this.viewedImage = this.appInfo.screenShots[this.viewedImageIndex];
    }
  }

  showNext() {
    if (this.viewedImageIndex != this.appInfo.screenShots.length - 1) {
      this.viewedImageIndex = this.viewedImageIndex + 1;
      this.viewedImage = this.appInfo.screenShots[this.viewedImageIndex];
    }
  }

  sendUserEnquiryDetails() {
    this.userEnquiryDetails.appName = this.appInfo.appName;
    this.userEnquiryDetailsService.create(this.userEnquiryDetails).subscribe(
      res => {
        this.showListPopUp = false;
        if (res.status == 200)
          this.successMessage = true;
        else
          this.errorMessage = true;
      });
  }

}

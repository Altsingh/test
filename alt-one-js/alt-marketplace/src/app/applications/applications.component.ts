import { Component, OnInit } from '@angular/core';
import { ApplicationsListService } from '../services/applications-service/applications-list.service';
import { CategoryApplicationsResponse } from '../models/responses/category-applications-response.model';
import { Application } from '../models/application/application.model';
import { AppInfoDataService } from '../services/data-service/app-info-data.service';
import { BannersListService } from '../services/applications-service/banners-list.service';
import { Banner } from '../models/application/banner.model';
@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  applications: CategoryApplicationsResponse[];
  appInfo:Application;
  banners:Banner[];
  futurePartnerImages:Array<any>=[];

  constructor(private applicationListsService: ApplicationsListService,
              private appInfoDataService: AppInfoDataService,
              private bannerListService:BannersListService) {
              this.futurePartnerImages = [
                { name: '../../assets/images/upcoming_partner_1.png' },
                { name: '../../assets/images/upcoming_partner_2.png' },
                { name: '../../assets/images/upcoming_partner_3.png' },
                { name: '../../assets/images/upcoming_partner_4.png' },
                { name: '../../assets/images/upcoming_partner_5.png' },
                { name: '../../assets/images/upcoming_partner_6.png' },
                { name: '../../assets/images/upcoming_partner_7.png' },
                { name: '../../assets/images/upcoming_partner_8.png' }
              ];

               }

  ngOnInit() {
      this.getBanners();
      this.getApplications();
   }

  getApplications() {
    this.applicationListsService.getAllApps().subscribe(
      res => {
        this.applications = res;
      });
  }

  getBanners(){
    this.bannerListService.getAll().subscribe(
      res => {
        this.banners = res;
      });
  }

  selectedApp(app:Application,applications:Application[]){
      this.appInfoDataService.appInfo=app;
  }

}

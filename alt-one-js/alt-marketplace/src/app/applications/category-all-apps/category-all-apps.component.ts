import { Component, OnInit } from '@angular/core';
import { Application } from '../../models/application/application.model';
import {  Router, ActivatedRoute, Params } from '@angular/router';
import { CategoryListService } from '../../services/applications-service/category-list.service';
import { CategoryApplicationsResponse } from '../../models/responses/category-applications-response.model';
import { ApplicationsListService } from '../../services/applications-service/applications-list.service';

@Component({
  selector: 'app-category-all-apps',
  templateUrl: './category-all-apps.component.html'
})
export class CategoryAllAppsComponent implements OnInit {

  applications:Application[];
  response:CategoryApplicationsResponse;

  constructor(private activatedRoute: ActivatedRoute,
              private categoryListService:CategoryListService,
            private appListService: ApplicationsListService) { }

  ngOnInit() {
      this.getAllApps();
  }

  getAllApps(){
    this.activatedRoute.params.subscribe((params: Params) => {
        this.appListService.getAppsForCategory(params['categoryId'])
          .subscribe(res => {
            this.response = res;
            this.applications = [];
            for(let i=0;i<this.response.subCategories.length;i++){
              for(let j=0;j<this.response.subCategories[i].apps.length;j++){
                this.applications.push(this.response.subCategories[i].apps[j]);
              }
            }
          });
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { Application } from '../../models/application/application.model';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SubCategoryResponse } from '../../models/responses/subCategoryList.model';
import { ApplicationsListService } from '../../services/applications-service/applications-list.service';

@Component({
  selector: 'app-subcategory-all-apps',
  templateUrl: './subcategory-all-apps.component.html'
})
export class SubCategoryAllAppsComponent implements OnInit {

  applications: Application[];
  response: SubCategoryResponse;
  startingIndex: number;
  routeSubscriber: any;
  draggable: boolean;
  constructor(private activatedRoute: ActivatedRoute,
    private appListService: ApplicationsListService) { }

  ngOnInit() {
    this.getAllApps();
    this.routeSubscriber = this.activatedRoute
      .data
      .subscribe(v => {
        this.draggable = v.draggable;
      });
  }

  getAllApps() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.appListService.getAppsForSubCategory(params['categoryId'], params['subCategoryId'])
        .subscribe(res => {
          this.response = res;
        });
    });
  }
  initializeStartingIndex($index) {
    this.startingIndex = $index;
  }


  bannerSortSuccess(endingIndex: any) {
    let i, j, n;

    if (this.startingIndex < endingIndex) {
      i = this.startingIndex;
      n = endingIndex;
    } else {
      n = this.startingIndex;
      i = endingIndex;
    }
    j = i;
    for (; i <= n; i++) {
      this.response.apps[i].sequence = i;
    }
    this.appListService.saveAll(this.response.apps.slice(j, n + 1)).subscribe(res => {
      console.log("abcd");
    });
  }
}

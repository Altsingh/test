import { Component, OnInit, Output, OnDestroy, EventEmitter } from "@angular/core";
import { Input } from "@angular/core";
import { Banner } from "../../models/application/banner.model";
import { EnumBannerType } from "../../enums/enum-banner-type";
import {
  trigger,
  state,
  style,
  transition,
  animate
} from "@angular/animations";

@Component({
  selector: "alt-carousel",
  templateUrl: "./alt-carousel.component.html",
  animations: [
    trigger("carousel", [
      state(
        "show",
        style({
          opacity: 1
        })
      ),
      state(
        "hide",
        style({
          opacity: 0.2
        })
      ),
      transition("hide=>show", animate("1000ms ease-in-out")),
      transition("show=>hide", animate("1000ms ease-in-out"))
    ])
  ]
})
export class AltCarouselComponent implements OnInit, OnDestroy {
  @Input("images") images: Banner[];
  @Input("editable") editable: boolean;
  @Output() showBannerListEvent: EventEmitter<any> = new EventEmitter<any>();
  mainImage: Banner;
  state: string = "show";
  mainImageIndex: number;
  topImageIndex: number;
  downImageIndex: number;
  interval: any;
  enumBannerType: EnumBannerType;

  constructor() {}

  ngOnInit() {
    this.mainImageIndex = 0;
    this.topImageIndex = 1;
    this.downImageIndex = 2;
    this.mainImage = this.images[this.mainImageIndex];
    setInterval(() => {
      this.clear();
    }, 5000);
    setTimeout(() => {
      setInterval(() => {
        this.change(this.mainImageIndex + 1);
      }, 5000);
    }, 1000);
  }

  ngOnDestroy() {
    clearTimeout(this.interval);
  }

  showBannerList() {
    this.showBannerListEvent.emit("hello");
  }

  clear() {
    // this.mainImage = null;
    this.state = "hide";
  }

  change(mainIndex) {
    this.mainImageIndex = mainIndex;
    this.topImageIndex = this.mainImageIndex + 1;
    this.downImageIndex = this.topImageIndex + 1;
    if (this.mainImageIndex == this.images.length) {
      this.mainImageIndex = 0;
      this.topImageIndex = 1;
      this.downImageIndex = 2;
    } else if (this.topImageIndex == this.images.length) {
      this.topImageIndex = 0;
      this.downImageIndex = 1;
    } else if (this.downImageIndex == this.images.length) {
      this.downImageIndex = 0;
    }
    this.mainImage = this.images[this.mainImageIndex];
    this.state = "show";
  }
}

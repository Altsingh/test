import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {

  @Input('rating')  inputWidth:number;

  calculatedWidth: number ;
  width:string;
  constructor() { }

  ngOnInit() {
    this.calculatedWidth=this.inputWidth*20;
    this.width=this.calculatedWidth+'%';
 }

}

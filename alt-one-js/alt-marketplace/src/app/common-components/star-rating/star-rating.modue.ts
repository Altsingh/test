import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { StarRatingComponent } from "./star-rating.component";
import { AltCarouselComponent } from "../alt-carousel/alt-carousel.component";

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [StarRatingComponent, AltCarouselComponent],
  declarations: [StarRatingComponent, AltCarouselComponent]
})
export class StarRatingModule { }

import { Component, OnInit, Input } from '@angular/core';
import { ContactDetails } from '../models/application/contactDetails';
import { ContactDetailsService } from '../services/applications-service/contactDetails.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contactDetails: ContactDetails;
  showMessage: boolean = false;
  interval: any;
  message: string;
  constructor(
    private contactDetailsService: ContactDetailsService) {
  }


  ngOnInit() {
    this.contactDetails = new ContactDetails();
  }
  saveContactUsDetails(form) {   
    this.contactDetailsService.create(this.contactDetails).subscribe(
      res => {
        if (res.status == 200) {         
          form.reset();
          this.message = "Details sent successfully.";
         
        }
        else {
          this.message = "Some error occured.";
        }
        this.showMessage = true;
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
            that.showMessage=false;
          }, 3000);
        })(this);
      });
  }
  clearAll() {
    this.contactDetails = new ContactDetails();

    
  }
}

import { Component, OnInit, EventEmitter } from "@angular/core";
import { Http, RequestOptions, Headers } from "@angular/http";
import { LoginModel } from "./login.model";
import { LoginHttpService } from "./login-http.service";
import { from } from "rxjs/observable/from";
import { error } from "util";
import { Router } from "@angular/router";
import { environment } from "../../environments/environment";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html"
})
export class HeaderComponent implements OnInit {
  showHeader: boolean = false;
  loginModal: LoginModel;
  showLogin: boolean = false;
  loggedInStatus: boolean = false;
  isPartnerPage: boolean = false;
  authToken: string = "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTMzMTE3NzIsInN1YiI6ImxvZ2luIiwidXNlckNvZGUiOiJ1c2VyMDAxIiwibG9nZ2VkSW5UaW1lc3RhbXAiOjE1MTMzMTE3NzI5ODQsInVzZXJOYW1lIjoidXNlciIsIm9yZ0NvZGUiOiJvcmcyIiwidGVuYW50Q29kZSI6IiAiLCJzeXNGb3JtQ29kZSI6IiAiLCJwb3J0YWxUeXBlIjoiU1VQRVJBRE1JTiIsImxvZ2dlZEluVXNlcklkZW50aWZpZXIiOiIgIiwiaXNzIjoiQWx0LUxvZ2luIiwiZXhwIjoxNTQ0ODQ3NzcyfQ.vb-R5za2uXkAope_cjaabYqUj0DklfXQXAP9dKLJjog";
  showPartnerProfile: boolean = false;
  loginFailed:boolean = false;
  constructor(
    private loginHttpService: LoginHttpService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginModal = new LoginModel();
    this.loginModal.companyURL = environment.companyURL;

    if (localStorage.getItem("userId")) {
      this.loggedInStatus = true;
    } else {
      this.loggedInStatus = false;
    }

    if (window.location.href.indexOf('partner') > -1) {
      this.isPartnerPage = true;
    } else {
      this.isPartnerPage = false;
    }

    this.router.events.subscribe(val => {
      if (val["url"]) {
        if (val["url"].indexOf("partner") > -1) {
          this.loggedInStatus = false;
          this.isPartnerPage = true;
          if (localStorage.getItem("userId")) {
            this.loggedInStatus = true;
          } else {
            this.loggedInStatus = false;
          }
        } else {
          this.isPartnerPage = false;
          this.loggedInStatus = false;
        }
      }
    });
  }

  showLoginPopUp() {
    this.showLogin = true;
    this.showHeader = !this.showHeader;
  }

  hideLoginPopUp() {
    this.showLogin = false;
  }

  setloggedInStatus(status: boolean) {
    this.loggedInStatus = status;
  }

  logout() {
    localStorage.clear();
    this.setloggedInStatus(false);
    this.showHeader = !this.showHeader;
    this.router.navigate(["/partner"]);
  }

  submit() {
    this.loginHttpService.login(this.loginModal).subscribe(
      res => {
        if (res.status == 200) {
          this.hideLoginPopUp();
          this.loginModal = new LoginModel();

          //jwt gets append if not done
          if (localStorage.getItem("partnerUser")) {
            localStorage.removeItem("partnerUser");
          }
          if (localStorage.getItem("userId")) {
            localStorage.removeItem("userId");
          }

          localStorage.setItem("partnerUser", res.json().jwt);
          localStorage.setItem("userId", res.json().userId);
          this.setloggedInStatus(true);
          this.router.navigate(["/partner/apps"]);
        } else {
          this.loginFailed = true;
        }
      },
      error => {
        this.loginFailed = true;
      }
    );
  }
}

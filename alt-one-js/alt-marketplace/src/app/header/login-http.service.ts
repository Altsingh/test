import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import { environment } from "../../environments/environment";
import { Observable } from 'rxjs/Rx';
import { error } from 'util';

@Injectable()
export class LoginHttpService {
  private url: string = environment.loginUrl;
  loginFailed = false;
  constructor(private http: Http) { }

  login(loginModal) {
    return this.http.post(this.url, loginModal, null)
      .map(res => {
        return res;
      }, error => {
        return error;
      })
  }
}
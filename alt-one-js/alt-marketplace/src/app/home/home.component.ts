import { Component, OnInit } from '@angular/core';
import { CategoryListService } from '../services/applications-service/category-list.service';
import { Category } from '../models/application/category.model';
import { ApplicationsListService } from '../services/applications-service/applications-list.service';
import { Application } from '../models/application/application.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  categories: Category;
  selectedCategory: Category;
  apps: Application[] = [];

  constructor(private categoryListService: CategoryListService,
    private applicationsListService: ApplicationsListService) { }

  ngOnInit() {
    this.categoryListService.getAll()
      .subscribe(res => {
        this.categories = res;
        this.selectedCategory = this.categories[0];
        this.getAppsForcategory(this.selectedCategory.categoryId);
      })
  }

  selectCategory(category: Category) {
    this.selectedCategory = category;
    this.getAppsForcategory(this.selectedCategory.categoryId);
  }


  getAppsForcategory(categoryId) {
    this.apps = [];
    this.applicationsListService.getAppsForCategory(categoryId)
      .subscribe(res => {

        res.subCategories.forEach(subCategory => {
          subCategory.apps.forEach(app => {
            this.apps.push(app);
          });

        });

      })
  }

}

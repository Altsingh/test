export class Application{
  appId:string;
  appName:string;
  company:string;
  rating:string;
  categoryName:string;
  launchDate:string;
  appLogo:string;
  description:string;
  screenShots:string[];
  subCategory:string; 
  websiteUrl:string; 
  videoUrl:string; 
  appUrl:string 
  shortDescription:string;
  headerCoverImage:string;
  previewBanner:string;
  partnerId: number;
  categoryId: string;
  sequence:string;

  constructor(){
    this.appId="";
    this.appName="";
    this.company="";
    this.rating="";
    this.categoryName="";
    this.launchDate="";
    this.appLogo="";
    this.description="";
    this.screenShots=[];
    this.subCategory="";
    this.websiteUrl="";
    this.videoUrl="";    
    this.appUrl="";   
    this.shortDescription="";    
    this.headerCoverImage="" 
    this.previewBanner="";
    this.partnerId = -1;
    this.categoryId = "";
    this.sequence="";
  }
}

import { EnumBannerType } from "../../enums/enum-banner-type";
import { EnumBannerStatus } from "../../enums/enum-banner-status";

export class Banner{
   imageBanner:string;
   imageThumbnail:string;
   type:EnumBannerType;
   status:EnumBannerStatus;
   link:string;
   bannerId:number;
   sequence:number;

   constructor(){
     this.imageBanner="";
     this.imageThumbnail="";
     this.link="";
     this.sequence=-1;
     this.bannerId=-1;
     this.status=EnumBannerStatus.INACTIVE;
     this.type=EnumBannerType.INTERNAL;
   }
}

export class ContactDetails {
name: string;
companyName: string;
companyAddress: string;
email: string;
companyWebsite: string;
mobileNumber: string;
bestTimeToCall: string;
comment: string;

constructor() {
    this.name = "";
    this.companyName = "";
    this.companyAddress = "";
    this.email = "";
    this.companyWebsite = "";
    this.mobileNumber = "";
    this.bestTimeToCall = "";
    this.comment = "";
}
}
export class UserEnquiryDetails {

    organizationName: string;
    firstName: string;
    lastName: string;
    email: string;
    howDidYouHearText: string;
    mobileNumber: string;
    referredBy: string;
    employeeCount: number;
    appName : string;
    constructor() {
        this.organizationName = "";
        this.firstName = "";
        this.lastName = "";
        this.email = "";
        this.howDidYouHearText = "";
        this.mobileNumber = "";
        this.referredBy = "";
        this.appName = "";
       
    }

}
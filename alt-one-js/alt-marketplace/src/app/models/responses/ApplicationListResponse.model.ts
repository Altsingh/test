import { CategoryApplicationsResponse } from "./category-applications-response.model";

export class ApplicationListResponse{
  applicationsResponse:CategoryApplicationsResponse[];
  
  constructor(){
    this.applicationsResponse=[];
  }

}

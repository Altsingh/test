import { Category } from "../application/category.model";
import { Application } from "../application/application.model";
import { SubCategoryResponse } from "./subCategoryList.model";

export class CategoryApplicationsResponse{
  category:Category;  
  subCategories : SubCategoryResponse[];
  
  constructor(){
    this.subCategories=[];
    this.category=null;
  }

  }


import { Application } from "../application/application.model";
import { SubCategory } from "../application/subCategory.model";

export class SubCategoryResponse{
    subCategory: SubCategory;
    apps:Application[];
  
    constructor(){
      this.subCategory=null;
      this.apps=[];
    }
  
  }
import { Component, OnInit } from "@angular/core";
import { Application } from "../../models/application/application.model";
import { ApplicationListResponse } from "../../models/responses/ApplicationListResponse.model";
import { Router } from "@angular/router";
import { CreateAppService } from "../../services/partner-module-services/create-app.service";
import { CategoryListService } from "../../services/partner-module-services/category-list.service";
import { SubcategoryListService } from "../../services/partner-module-services/subcategory-list.service";
import { ApplicationsListService } from "../../services/applications-service/applications-list.service";
import { CategoryApplicationsResponse } from "../../models/responses/category-applications-response.model";
import { BannersListService } from "../../services/applications-service/banners-list.service";
import { Banner } from "../../models/application/banner.model";
import { CreateBannerService } from "../../services/admin-services/create-banner.service";
import { EnumBannerType } from "../../enums/enum-banner-type";
import { FileValidationService } from "../../services/partner-module-services/file-validation.service";
import { EnumBannerStatus } from "../../enums/enum-banner-status";
import { Message } from "../../models/notification/message.model";
import { SubCategoryResponse } from "../../models/responses/subCategoryList.model";

@Component({
  selector: "app-admin-app-list",
  templateUrl: "./admin-app-list.component.html",
  styleUrls: ["./admin-app-list.component.css"],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})
export class AdminAppListComponent implements OnInit {
  subCatIdForPopUp: number = -1;
  appIdForPopUp: number = -1;
  catIdForPopUp: number = -1;
  app: Application;
  categories;
  subCategories;
  categoryListResponse: CategoryApplicationsResponse[];
  isSubCatPopUp: boolean = false;
  isAppPopUp: boolean = false;
  isCategoryPopUp: boolean = false;
  showCreatePopUp: boolean = false;
  subCategoryName: boolean = false;
  banners: Banner[];
  showBannerList: boolean;
  bannerCreatePopup: boolean = false;
  bannerPopUpEditMode:boolean=false;
  banner: Banner;
  interval: any;
  startingIndex: number;
  startingIndexSubCategory: number;
  message: Message;
  types:EnumBannerType;
  typeOption: EnumBannerType[]=[EnumBannerType.INTERNAL, EnumBannerType.EXTERNAL];

  create: boolean = false;

  constructor(
    private router: Router,
    private fileValidationService: FileValidationService,
    private applicationsListService: ApplicationsListService,
    private createAppService: CreateAppService,
    private categoryListService: CategoryListService,
    private subcategoryListService: SubcategoryListService,
    private bannerListService: BannersListService,
    private bannerCreateService: CreateBannerService
  ) {
    this.message=new Message();
  }

  ngOnInit() {
    this.validateLogin();
    this.app = new Application();
  }

  initializeStartingIndex($index) {
    this.startingIndex = $index;
  }

  initializeStartingIndexSubCategory($index) {
    this.startingIndexSubCategory = $index;
  }

  showBannerListFunction() {
    this.showBannerList = true;
  }

  subcategorySortSuccess(subCategoryRes: CategoryApplicationsResponse, endingIndex: any, categoryId){
    let i, j, n;

    if (this.startingIndexSubCategory < endingIndex) {
      i = this.startingIndexSubCategory;
      n = endingIndex;
    } else {
      n = this.startingIndexSubCategory;
      i = endingIndex;
    }
    j = i;
    for (; i <= n; i++) {
      subCategoryRes.subCategories[i].subCategory.sequence = i;
    }
    this.categoryListService.saveAllSubCategories(categoryId, subCategoryRes.subCategories).subscribe(res => {
      console.log("abcd");
    });
  }

  bannerSortSuccess(endingIndex: any) {
    let i, j, n;

    if (this.startingIndex < endingIndex) {
      i = this.startingIndex;
      n = endingIndex;
    } else {
      n = this.startingIndex;
      i = endingIndex;
    }
    j = i;
    for (; i <= n; i++) {
      this.banners[i].sequence = i;
    }
    this.bannerListService.saveAll(this.banners.slice(j, n+1)).subscribe(res => {
      console.log("abcd");
    });
  }

  openCreateBannerPopup() {
    this.bannerCreatePopup = true;
    this.bannerPopUpEditMode=false;
    this.banner = new Banner();
  }

  closeBannerPopup() {
    this.bannerCreatePopup = false;
    this.banner = null;
    this.bannerPopUpEditMode=false;
  }

  openUpdateBannerPopup(banner:Banner) {
    var bannerLocal=new Banner();
    bannerLocal.bannerId=banner.bannerId;
    bannerLocal.link=banner.link;
    bannerLocal.sequence=banner.sequence;
    bannerLocal.status=banner.status;
    bannerLocal.type=banner.type;
    bannerLocal.imageBanner=banner.imageBanner;
    bannerLocal.imageThumbnail=banner.imageThumbnail;
    this.banner = bannerLocal;
    this.bannerCreatePopup = true;
    this.bannerPopUpEditMode=true;
  }

  fileChange(type, event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append("uploadingFiles", file, file.name);
      if (this.fileValidationService.validate(type, file.size,file.type,this.message)) {
        this.bannerCreateService.uploadImage(formData).subscribe(res => {
          this.banner[type] = res["_body"];
        });
      } else {
        console.log(this.message);
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message.message = "";
          }, 1500);
        })(this);
      }
    }
  }

  validateLogin() {
    this.getBanners();
    this.getApps();

    // if (localStorage.getItem("adminId")) {
    //   this.getApps();
    // }
    // else {
    //   this.router.navigate(["/login"])
    // }
  }

  createBanner() {
    if(this.validateBannerRequest()){
      if(!this.bannerPopUpEditMode){
          this.bannerCreateService.create(this.banner).subscribe(res => {
                if (res.status == 200) {
                  var createdBanner=res.json();
                  this.banners.push(createdBanner);
                  this.bannerCreatePopup = false;
                  this.banner = null;
                }
          });
        }
      else {
        this.bannerCreateService.update(this.banner).subscribe(res=>{
            if (res.status == 200) {
              // this.banners.push(this.banner);
              this.showUpdatedItem(this.banner);
              this.bannerCreatePopup = false;
              this.banner = null;
              this.bannerPopUpEditMode=false;
            }
        });
      }
  }
  }

  showUpdatedItem(newItem:Banner){
    let updateItem = this.banners.find(this.findIndexToUpdate, newItem.bannerId);

    let index = this.banners.indexOf(updateItem);


    this.banners[index] = newItem;

  }

  findIndexToUpdate(newItem) {
        return newItem.bannerId === this;
  }

  closeCreateBannerPopup():void{
    this.bannerCreatePopup = false;
    this.banner = null;
  }
  validateBannerRequest(): boolean {
    if (this.banner.type == null)
      return false;
    else{
      return true;
    }
  }

  getBanners() {
    this.bannerListService.getAll().subscribe(res => {
      this.banners = res;
      this.showBannerList = false;
    });
  }

  getApps() {
    this.applicationsListService.getAll().subscribe(res => {
      this.categoryListResponse = res;
    });
  }


  showCategoryPopUp(event, catIndex) {
    this.catIdForPopUp = catIndex;
    this.isCategoryPopUp = true;
    this.isAppPopUp = false;
    this.isSubCatPopUp = false;
    event.stopPropagation();
  }
  showSubCatPopUp(event, catIndex, subCatIndex) {
    this.catIdForPopUp = catIndex;
    this.subCatIdForPopUp = subCatIndex;
    this.isSubCatPopUp = true;
    this.isAppPopUp = false;
    this.isCategoryPopUp = false;
    event.stopPropagation();
  }

  showAppPopUp(event, catIndex, subCatIndex, appIndex) {
    this.catIdForPopUp = catIndex;
    this.subCatIdForPopUp = subCatIndex;
    this.appIdForPopUp = appIndex;
    this.isAppPopUp = true;
    this.isSubCatPopUp = false;
    this.isCategoryPopUp = false;
    event.stopPropagation();
  }
  closeEditPopUp(popUp) {
    this.catIdForPopUp = -1;
    if (popUp == "Category") {
      this.isCategoryPopUp = false;
    } else if (popUp == "App") {
      this.isAppPopUp = false;
      this.appIdForPopUp = -1;
      this.subCatIdForPopUp = -1;
    } else if (popUp == "SubCategory") {
      this.isSubCatPopUp = false;
      this.subCatIdForPopUp = -1;
    }
  }

  initializeCreate() {
    this.app = new Application();
    if (localStorage.getItem("adminId")) {
      this.app.partnerId = Number(localStorage.getItem("adminId"));
      this.getCategories();
      this.create = true;
    }
  }

  getCategories() {
    this.categoryListService.getAll().subscribe(res => {
      this.categories = res;
    });
  }

  categoryChanged(id: number) {
    this.subcategoryListService.getSubcategories(id).subscribe(res => {
      this.subCategories = res;
    });
  }

  submit() {
    this.createAppService.create(this.app)
      .subscribe(res => {
        let appId = res.json().appId;
        if (res.status == 200) {
          this.router.navigate(["/adminUser/apps/" + appId]);
        }
      })
  }

  checkCategoryPopUp(catIndex) {
    return this.isCategoryPopUp && catIndex == this.catIdForPopUp;
  }
  checkSubCategoryPopUp(catIndex, subCatIndex) {
    return (
      this.isSubCatPopUp &&
      catIndex == this.catIdForPopUp &&
      subCatIndex == this.subCatIdForPopUp
    );
  }
  checkAppPopUp(catIndex, subCatIndex, appIndex) {
    return (
      this.isAppPopUp &&
      catIndex == this.catIdForPopUp &&
      subCatIndex == this.subCatIdForPopUp &&
      appIndex == this.appIdForPopUp
    );
  }
  createAppPopUp(subCatName, subCatId, categoryName, categoryId) {
    this.app = new Application();
    this.showCreatePopUp = true;
    this.app.categoryId = categoryId;
    this.app.categoryName = categoryName;
    this.app.subCategory = subCatId;
    this.subCategoryName = subCatName;
  }

  handleClick(event) {
    var clickedComponent = event.target;
    if (!clickedComponent.classList.contains("menu-style")) {
      this.isAppPopUp = false;
      this.isSubCatPopUp = false;
      this.isCategoryPopUp = false;
      this.catIdForPopUp = -1;
      this.subCatIdForPopUp = -1;
      this.appIdForPopUp = -1;
    }
    // if (clickedComponent.classList.contains("menu-click")) {
    //   this.showEditPopUp = true;

    // }
  }
}

import { Component, OnInit } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { PartnerModel } from '../model/partner.model';
import { Category } from '../../models/application/category.model';
import { CreateAppService } from '../../services/partner-module-services/create-app.service';
import { CategoryListService } from '../../services/partner-module-services/category-list.service';
import { SubcategoryListService } from '../../services/partner-module-services/subcategory-list.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppInfoDataService } from '../../services/data-service/app-info-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FileValidationService } from '../../services/partner-module-services/file-validation.service';
import { Message } from '../../models/notification/message.model';

@Component({
  selector: 'admin-app-create-app',
  templateUrl: './admin-create-app.component.html'
})
export class AdminCreateAppComponent implements OnInit {
  form: FormGroup;
  logo: any;
  app: PartnerModel = new PartnerModel();
  categories: Category[];
  subCategories: Category[];
  interval: any;
  message: Message;

  constructor(private fb: FormBuilder,
    private fileValidationService: FileValidationService,
    private appInfoDataService: AppInfoDataService,
    private createAppService: CreateAppService,
    private route: ActivatedRoute,
    private router: Router,
    private categoryListService: CategoryListService,
    private subcategoryListService: SubcategoryListService) { }

  ngOnInit() {
  //  if (localStorage.getItem("adminId")) {
      this.getApp(this.route.snapshot.params['id']);
    //}
    // else {
    //   this.router.navigate(["/login"])
    // }
  }

  getApp(appId: string) {
    this.appInfoDataService.getAppDetails(appId).subscribe(
      app => {
        this.app = app;
        this.getCategories();
        this.categoryChanged(this.app.categoryId);
      });
  }

  createForm(): any {
    this.getCategories();
  }

  getCategories() {
    this.categoryListService.getAll().subscribe(
      res => {
        this.categories = res;
      }
    );
  }

  categoryChanged(id: number) {
    this.subcategoryListService.getSubcategories(id).subscribe(
      res => {
        this.subCategories = res;
      }
    )
  }


  updateApp() {
    this.createAppService.updateApp(this.app).subscribe(
      res => {
        if (res.status == 200) {
          console.log("success");
          this.message.message = "App updated successfully";
          this.interval = (function (that) {
            return setTimeout(function () {
              that.message.message = "";
            }, 3000);
          })(this);
        }
      });
  }

  removeScreenshot(ind) {
    this.app.screenShots.splice(ind, 1);
  }

  fileChange(type, event) {
    let fileList: FileList = event.target.files;
    this.logo = event.target.files[0];
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('uploadingFiles', file, file.name);
      if (this.fileValidationService.validate(type, file.size,file.type,this.message)) {

        this.createAppService.uploadImage(type, formData, this.app.appId)
          .subscribe(res => {
            if (type == 'screenShots') {
              this.app.screenShots.push(res["_body"]);
            } else {
              this.app[type] = res["_body"];
            }
          });
      } else {
        alert("file size is more than 1mb");
      }
    }
  }
}

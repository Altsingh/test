export class FileModel {
    "fileName": string;
    "fileType": string;
    "value": string;

    constructor(options) {
        this.fileName = options.fileName || "";
        this.fileType = options.fileType || "";
        this.value = options.value || "";
    }
}
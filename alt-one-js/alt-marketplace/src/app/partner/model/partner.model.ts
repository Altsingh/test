export class PartnerModel {
  appId: string;
  appName: string;
  company: string;
  partnerId: string;
  rating: string;
  launchDate: string;
  description: string;
  screenShots: string[];
  appLogo: string;
  categoryId: number;
  subCategory: string;
  appUrl: string;
  shortDescription: string;
  websiteUrl: string;
  videoUrl: string;
  headerCoverImage: string;
  previewBanner: string;


  constructor() {
    this.appId = "";
    this.appName = "";
    this.company = "";
    this.partnerId = "1";
    this.rating = "";
    this.launchDate = "";
    this.description = "";
    this.screenShots = [];
    this.appLogo = "";
    this.categoryId = 0;
    this.subCategory = "";
    this.appUrl = "";
    this.shortDescription = "";
    this.websiteUrl = "";
    this.videoUrl = "";
    this.headerCoverImage = "";
    this.previewBanner = "";
  }
}

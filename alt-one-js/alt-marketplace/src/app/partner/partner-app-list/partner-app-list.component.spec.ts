import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerAppListComponent } from './partner-app-list.component';

describe('PartnerAppListComponent', () => {
  let component: PartnerAppListComponent;
  let fixture: ComponentFixture<PartnerAppListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerAppListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerAppListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

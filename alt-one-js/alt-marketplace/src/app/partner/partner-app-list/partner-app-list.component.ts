import { Component, OnInit } from '@angular/core';
import { Application } from '../../models/application/application.model';
import { CategoryListService } from '../../services/partner-module-services/category-list.service';
import { CreateAppService } from '../../services/partner-module-services/create-app.service';
import { SubcategoryListService } from '../../services/partner-module-services/subcategory-list.service';
import { Router } from '@angular/router';
import { ApplicationListResponse } from '../../models/responses/ApplicationListResponse.model';


@Component({
  selector: 'app-partner-app-list',
  templateUrl: './partner-app-list.component.html',
  styleUrls: ['./partner-app-list.component.css'],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})
export class PartnerAppListComponent implements OnInit {
  app: Application;
  categories;
  subCategories;
  apps: ApplicationListResponse[];
  showEditPopUp: boolean = false;
  indexPopUp: number = -1;

  create: boolean = false;
  constructor(
    private router: Router,
    private createAppService: CreateAppService,
    private categoryListService: CategoryListService,
    private subcategoryListService: SubcategoryListService) { }

  ngOnInit() {
    this.validateLogin();
    this.getApps();

  }

  validateLogin(){
    if(localStorage.getItem("userId")){
      this.getApps();
    }
    else{
      this.router.navigate(["/partner"])
    }
  }

  getApps() {

    this.createAppService.getAllPartnerApps().subscribe(
      res => {
        this.apps = res;
      }
    )
  }
  handleClick(event) {
    var clickedComponent = event.target;
    if (!clickedComponent.classList.contains("menu-style")) {
      this.showEditPopUp = false;
      this.indexPopUp=-1;
    }
    // if (clickedComponent.classList.contains("menu-click")) {
    //   this.showEditPopUp = true;

    // }
  }

  showEditPop(event, index) {
    this.indexPopUp = index;
    this.showEditPopUp = true;
    event.stopPropagation();
  }

  closeEditPopUp() {
    this.indexPopUp = -1;
    this.showEditPopUp = false;
  }

  initializeCreate() {
    this.app = new Application();
    if (localStorage.getItem("userId")) {
      this.app.partnerId = Number(localStorage.getItem("userId"));
      this.getCategories();
      this.create = true;
    }
  }

  getCategories() {
    this.categoryListService.getAll().subscribe(
      res => {
        this.categories = res;
      }
    );
  }

  categoryChanged(id: number) {
    this.subcategoryListService.getSubcategories(id).subscribe(
      res => {
        this.subCategories = res;
      }
    )
  }

  submit() {
    this.createAppService.create(this.app)
      .subscribe(res => {
        let appId = res.json().appId;
        if (res.status == 200) {
          this.router.navigate(["/partner/apps/" + appId]);
        }
      })
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-partner-home',
  templateUrl: './partner-home.component.html',
  styleUrls: ['./partner-home.component.css']
})
export class PartnerHomeComponent implements OnInit {


  constructor(private router:Router) { }

  ngOnInit() {
    if(localStorage.getItem("userId")){
      this.router.navigate(["/partner/apps"])
    }
  }

}

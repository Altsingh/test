import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routings } from "./partner.routing";
import { ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DndModule } from 'ng2-dnd';

import { PartnerComponent } from "./partner.component";
import { CreateAppComponent } from "./create-app/create-app.component";
import { PartnerHomeComponent } from './partner-home/partner-home.component';
import { PartnerAppListComponent } from './partner-app-list/partner-app-list.component';
import { FormsModule } from '@angular/forms';
import { CreateAppService } from '../services/partner-module-services/create-app.service';
import { SubcategoryListService } from '../services/partner-module-services/subcategory-list.service';
import { StarRatingModule } from '../common-components/star-rating/star-rating.modue';
import { PartnerAppListService } from "../services/partner-module-services/partner-app-list.service";
import { CategoryListService } from '../services/partner-module-services/category-list.service';
import { FileValidationService } from '../services/partner-module-services/file-validation.service';
import { AdminAppListComponent } from "./admin-app-list/admin-app-list.component";
import { AdminCreateAppComponent } from './admin-create/admin-create-app.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    StarRatingModule,
    routings,
    DndModule.forRoot()
  ],
  declarations: [
    AdminCreateAppComponent,
     AdminAppListComponent,
     PartnerComponent,
     CreateAppComponent,
      PartnerHomeComponent,
       PartnerAppListComponent
      ],
  providers: [FileValidationService, CreateAppService, SubcategoryListService, CategoryListService, PartnerAppListService],
  exports: [CreateAppComponent]
})
export class PartnerModule { }

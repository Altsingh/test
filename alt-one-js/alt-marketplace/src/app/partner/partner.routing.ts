import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PartnerComponent } from "./partner.component";
import { PartnerHomeComponent } from "./partner-home/partner-home.component";
import { CreateAppComponent } from "./create-app/create-app.component";
import { PartnerAppListComponent } from "./partner-app-list/partner-app-list.component";
import { AdminAppListComponent } from "./admin-app-list/admin-app-list.component";
import { ApplicationDetailsComponent } from "../applications/application-details/application-details.component";
import { PartnerLoginGuard } from "../services/authentication-services/partner-login-guard.service";
import { AdminCreateAppComponent } from "./admin-create/admin-create-app.component";
import { SubCategoryAllAppsComponent } from "../applications/subcategory-all-apps/subcategory-all-apps.component";

const routes: Routes = [
  {
    path: "admin",
    component: PartnerComponent,
    children: [
      { path: "", redirectTo: "apps", pathMatch: "full" },
      { path: "apps", component: AdminAppListComponent, pathMatch: "full" },
      { path: "apps/:id", component: AdminCreateAppComponent, pathMatch: "full" },
      { path: 'category/:categoryId/subcategory/:subCategoryId', component: SubCategoryAllAppsComponent, pathMatch: 'full', data : {draggable : true}}
    ]
  },
  {
    path: "partner",
    component: PartnerComponent,

    children: [
      { path: "", component: PartnerHomeComponent, pathMatch: "full" },
      {
        path: "apps",
        component: PartnerAppListComponent,
        pathMatch: "full",
        canActivate: [PartnerLoginGuard]
      },
      {
        path: "apps/:id",
        component: CreateAppComponent,
        pathMatch: "full",
        canActivate: [PartnerLoginGuard]
      }
    ]
  }

];

export const routings: ModuleWithProviders = RouterModule.forChild(routes);

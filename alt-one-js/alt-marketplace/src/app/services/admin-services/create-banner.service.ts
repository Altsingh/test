import { Injectable } from "@angular/core";
import { DataService } from "../http-service/data.service";
import { Http, RequestOptions } from "@angular/http";
import { environment } from "../../../environments/environment";

@Injectable()
export class CreateBannerService extends DataService {
  constructor(http: Http) {
    super(environment.bannerListUrl, http);
  }

  uploadImage(requestData) {
    return this.http
      .post(
        environment.bannerListUrl + "/uploadImage",
        requestData,
        new RequestOptions()
      )
      .map(res => {
        return res;
      });
  }
}

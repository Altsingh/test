import { TestBed, inject } from '@angular/core/testing';

import { ApplicationsListService } from './applications-list.service';

describe('ApplicationsListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicationsListService]
    });
  });

  it('should be created', inject([ApplicationsListService], (service: ApplicationsListService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { DataService } from '../http-service/data.service';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class ApplicationsListService extends DataService {

  constructor(http: Http) {
    super(environment.applicationListUrl, http);
  }

  getAllApps() {
    if (!this.options.headers.get("authToken")) {
      this.options.headers.append("authToken", localStorage.getItem("partnerUser"));
    }
    return this.getAll();
  }

  getAppsForCategory(id) {
    return this.http.get(this.url + '/' + id, this.options)
      .map(response => {
        return response.json();
      });
  }

  getAppsForSubCategory(categoryid, subcategoryid) {
    return this.getSubCategory(categoryid, subcategoryid);
  }

  saveAll(apps) {
    return this.http.put(this.url+"/sequence", apps, this.options)
      .map(response => {
        return response;
      })
  }
}

import { Injectable } from '@angular/core';
import { DataService } from '../http-service/data.service';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class BannersListService extends DataService {

  constructor(http: Http) {
    super(environment.bannerListUrl, http);
  }

  saveAll(banners) {
    return this.http.put(this.url+'/updateSequence', banners, this.options)
      .map(response => {
        return response;
      })
  }

}

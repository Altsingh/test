import { Injectable } from '@angular/core';
import { DataService } from '../http-service/data.service';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';

@Injectable()
export class CategoryListService extends DataService {

  constructor(http: Http) {
    super(environment.categoryListUrl, http);
  }

  getAllCategories() {
    return this.getAll();
  }

}

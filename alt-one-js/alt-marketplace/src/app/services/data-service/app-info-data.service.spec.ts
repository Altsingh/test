import { TestBed, inject } from '@angular/core/testing';

import { AppInfoDataService } from './app-info-data.service';

describe('AppInfoDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppInfoDataService]
    });
  });

  it('should be created', inject([AppInfoDataService], (service: AppInfoDataService) => {
    expect(service).toBeTruthy();
  }));
});

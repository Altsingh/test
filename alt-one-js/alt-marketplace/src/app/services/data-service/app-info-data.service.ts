import { Injectable } from '@angular/core';
import { Application } from '../../models/application/application.model';
import { DataService } from '../http-service/data.service';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';

@Injectable()
export class AppInfoDataService extends DataService {

  public appInfo: Application;
  public categoryApps: Application;

  constructor(http: Http) {
    super(environment.applicationListUrl + '/', http);
  }

  getAppDetails(appId){
    return this.get(appId);
  }

  getCategory(id: string) {
    return this.http.get(this.url + '/' + id, this.options)
      .map(response => {
        return response.json();
      });
  }

}

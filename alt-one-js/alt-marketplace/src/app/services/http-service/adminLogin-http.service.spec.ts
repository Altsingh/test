import { TestBed, inject } from '@angular/core/testing';

import { AdminLoginHttpService } from './adminLogin-http.service';

describe('LoginHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminLoginHttpService]
    });
  });

  it('should be created', inject([AdminLoginHttpService], (service: AdminLoginHttpService) => {
    expect(service).toBeTruthy();
  }));
});

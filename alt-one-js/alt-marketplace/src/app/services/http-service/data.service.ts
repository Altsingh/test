import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  public options: RequestOptions;
  public multiPartOptions: RequestOptions;

  constructor(protected url: string, protected http: Http) {
    this.options = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json',
      })
    });

    this.multiPartOptions = new RequestOptions({
      headers: new Headers({
        'Content-Type': 'multipart/form-data',
      })
    });
  }

  getAll() {
    return this.http.get(this.url, this.options)
      .map(
      response => {
        return response.json()
      });
  }

  get(id: string) {
    return this.http.get(this.url + '/app/' + id, this.options)
      .map(response => {
        return response.json();
      });
  }
  
  getSubCategory(categoryid: string, subcategoryid: string) {
    return this.http.get(this.url + '/subcategory/' + categoryid + "/" + subcategoryid, this.options)
      .map(response => {
        return response.json();
      });
  }

  create(resource) {
    return this.http.post(this.url, resource, this.options)
      .map(response => {
        return response;
      });
  }

  update(resource) {
    return this.http.put(this.url, resource, this.options)
      .map(response => {
        return response;
      })
  }

  delete(id: string) {
    return this.http.delete(this.url + '/' + id);
  }

}

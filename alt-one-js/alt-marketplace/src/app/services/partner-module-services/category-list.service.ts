import { Injectable } from '@angular/core';
import { DataService } from '../http-service/data.service';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';
import { SubCategoryResponse } from '../../models/responses/subCategoryList.model';
import { SubCategory } from '../../models/application/subCategory.model';

@Injectable()
export class CategoryListService extends DataService {

  constructor(http: Http) {
    super(environment.categoryListUrl, http);
  }

  saveAllSubCategories(categoryId, subCategoryResponse: SubCategoryResponse[]){
    return this.http.put(this.url+'/'+categoryId+'/subcategories/sequence', this.retrieveSubCategoryFromData(subCategoryResponse), this.options)
      .map(response => {
        return response;
      })
  }

  retrieveSubCategoryFromData(subCategoryList){
    let subCategories=[];
    for(var i=0;i<subCategoryList.length;i++){
      subCategories[i]=new SubCategory;
      subCategories[i].sequence = subCategoryList[i].subCategory.sequence;
      subCategories[i].subCategoryId = subCategoryList[i].subCategory.subCategoryId;
      subCategories[i].subCategoryName = subCategoryList[i].subCategory.subCategoryName;
    }
    return subCategories;
  }

}

import { Injectable } from '@angular/core';
import { DataService } from '../http-service/data.service';
import { Http, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class CreateAppService extends DataService {

  constructor(http: Http) {
    super(environment.applicationListUrl, http);
  }

  uploadImage(type, requestData, appId) {
    return this.http.post(environment.partnerAppList + "/uploadImage/" + appId + "/" + type, requestData, new RequestOptions())
      .map(res => {
        return res;
      })
  }

  getAllPartnerApps() {
    if(!this.options.headers.get("authToken")){
      this.options.headers.append("authToken",localStorage.getItem("partnerUser"));
    }
    this.url=environment.partnerAppList;
    return this.getAll();
  }

  updateApp(resource){
    this.url=environment.partnerAppList;
    return this.update(resource);
  }
}

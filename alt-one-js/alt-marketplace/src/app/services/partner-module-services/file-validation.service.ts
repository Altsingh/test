import { Injectable } from "@angular/core";
import { Message } from "../../models/notification/message.model";

@Injectable()
export class FileValidationService{
    constructor(){}

    validate(type, size,fileType,message:Message){
        if( size>1048576){
          message.message="File size is more than 1MB,Can't Upload";
            return false;
        }
        if(fileType.indexOf('image')==-1){
          message.message="File is not of specifc format";
          return false;
        }
        return true;
    }

}

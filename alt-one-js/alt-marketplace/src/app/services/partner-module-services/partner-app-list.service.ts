import { Injectable } from "@angular/core";
import { DataService } from "../http-service/data.service";
import { environment } from "../../../environments/environment";
import { Http, RequestOptions, Headers } from "@angular/http";
import { Input } from "@angular/core/src/metadata/directives";

@Injectable()
export class PartnerAppListService extends DataService {


  constructor(http: Http) {
    super(environment.partnerAppList, http);

  }

  getAllPartnerApps() {
    if (!this.options.headers.get("authToken")) {
      this.options.headers.append("authToken", localStorage.getItem("partnerUser"));
    }
    return this.getAll();
  }
}

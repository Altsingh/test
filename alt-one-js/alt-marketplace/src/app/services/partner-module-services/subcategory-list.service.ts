import { Injectable } from '@angular/core';
import { DataService } from '../http-service/data.service';
import { Http } from '@angular/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class SubcategoryListService extends DataService  {

  constructor(http:Http) {
    super(environment.categoryListUrl,http)
   }
   
   getSubcategories(id:number){
     this.url=environment.categoryListUrl+'/'+id+'/'+'subcategories';
     return this.getAll();
   }

}

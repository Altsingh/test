function showModal(){
    $(".popUp_Sign_in").fadeIn('slow');
}
function hideModal(){
    $(".popUp_Sign_in").fadeOut('slow');    
}

function showModal_getStarted(){
    $(".popUp_getStarted").fadeIn('slow');
}
function hideModal_getStarted(){
    $(".popUp_getStarted").fadeOut('slow');    
}
function showModal_popUp_Sign_up(){
    $(".popUp_Sign_up").fadeIn('slow');
}
function hideModal_popUp_Sign_up(){
    $(".popUp_Sign_up").fadeOut('slow');
}
function hideModal_popUp_partner_profile_completion(){
    $(".partner_profile_completion_overlay").fadeOut('slow');
}

function open_media_popup(){
    $(".open_media_popup").fadeIn('slow');
}


function close_media_popup(){
    $(".open_media_popup").fadeOut('slow');
}

function openOption(x){
    if(x.parentNode.className == 'appstore-slider-item')
    {
    x.parentNode.className += ' menu-active'
    }
  };


  function closeOption(x){
    x.parentNode.parentNode.className = 'appstore-slider-item';

      }
 $(function(){
 $('#midpageSlider1').carousel({
      interval: 4000
    });
});

 $(function(){
    $('.open_media_popup .alt-modal-close').click(function(){      
        $('iframe').attr('src', $('iframe').attr('src'));
    });
});
/********** Owl Carousel scripts *****/

$(document).ready(function(){
    $('.owl-appstore').owlCarousel({
        loop:false,
        dots:!1,
       navText:["<img src='assets/images/chevron_active_left.png'>","<img src='assets/images/chevron_active_right.png'>"],
        responsive:{
            0:{
                items:1,
                nav:false
            },
            600:{
                items:2,
                  nav:true
            },
            1000:{
                items:4,
                  nav:true
            }
        }
    });
    $('.owl-app-info').owlCarousel({
        loop:false,
        dots:!1,
        margin:10,
       navText:["<img src='assets/images/chevron_active_left.png'>","<img src='assets/images/chevron_active_right.png'>"],
        responsive:{
            0:{
                items:1,
                nav:false,
                stagePadding:30

            },
            600:{
                items:2,
                nav:false
            },
            1000:{
                items:2,
                nav:true
             
            },
            1200:{
                items:2,
                nav:true
             
            }
        }
    });
    $('.owl-app-info-popup').owlCarousel({
        loop:false,
        dots:!1,
        stagePadding:0,
       navText:["<img src='assets/images/chevron_active_left.png'>","<img src='assets/images/chevron_active_right.png'>"],
        responsive:{
            0:{
                items:1,
                nav:false,
                margin:10,
                stagePadding:30

            },
            600:{
                items:1,
                nav:false
             
            },
            1000:{
                items:1,
                nav:false
             
            },
            1200:{
                items:1,
                nav:true
             
            }
        }
    });
});



       

// Home page categories tabs
$('.cat_name').click(function(){
    $('.categories_items_home').removeClass('open_cat');
    $('.categories_items_home').filter(this.hash).addClass('open_cat');
    $(this).parent('li').siblings().removeClass('active');
    $(this).parent('li').addClass('active');
    return false;
});


// admin home add new banner show 
$('.appstore_edit_slider a').click(function(){
    $('.banner_add_new').toggle(400);
    $('.admin_banner').hide();
});

// admin home upload banner & link popup show
function showModal_upload_banner(){
    $(".admin_home_banner_upload").fadeIn('slow');
}
function hideModal_upload_banner(){
    $(".admin_home_banner_upload").fadeOut('slow');
}
// admin home edit banner & link popup show
function showModal_edit_banner(){
    $(".admin_home_banner_edit").fadeIn('slow');
}
function hideModal_edit_banner(){
    $(".admin_home_banner_edit").fadeOut('slow');
}


//input label transition effect functions

        function addClassfocused(x){
            
                if(x.parentNode.className == 'inputContainer'){
                    x.parentNode.className += ' focused'
                }
            };

            function removeClassfocused(x){
                x.parentNode.className = 'inputContainer';
                if(x.value !== ""){
                    addClassfocused(x);
                }
            }
export const environment = {
  production: true,
  applicationListUrl:"https://gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/marketplaceapps",
  loginUrl:"https://gateway.peoplestrong.com/alt-login-0.0.1-SNAPSHOT/api/v1/login",
  partnerAppList:"https://gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/marketplace/partner/apps",
  categoryListUrl:"https://gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/marketplace/categories",
  bannerListUrl:"https://gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/marketplace/banners",
  companyURL:"hrms.peoplestrong.com"

}

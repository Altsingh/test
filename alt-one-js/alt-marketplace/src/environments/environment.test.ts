export const environment = {
  production: false,
  applicationListUrl:"https://test-gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/marketplaceapps",
  loginUrl:"https://test-gateway.peoplestrong.com/alt-login-0.0.1-SNAPSHOT/api/v1/login",
  partnerAppList:"https://test-gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/marketplace/partner/apps",
  categoryListUrl:"https://test-gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/marketplace/categories",
  bannerListUrl:"https://gateway.peoplestrong.com/alt-marketplace-0.0.1-SNAPSHOT/api/v1/marketplace/banners",
  companyURL:"hrms.sohum.com"

};

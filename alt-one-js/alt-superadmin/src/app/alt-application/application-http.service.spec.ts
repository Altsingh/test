import { TestBed, inject } from '@angular/core/testing';

import { HttpModule } from "@angular/http";
import { Http } from "@angular/http";
import { ApplicationHttpService } from './application-http.service';

describe('ApplicationHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApplicationHttpService],
      imports: [HttpModule]
    });
  });

  it('should exist', inject([ApplicationHttpService], (service: ApplicationHttpService) => {
    expect(service).toBeTruthy();
  }));

  it('should return list of module on calling getModules method',
    inject([ApplicationHttpService, Http], (service: ApplicationHttpService, http: Http) => {
      spyOn(http, "get").and.returnValue("1");
      service.getModules("101")
        .subscribe(res => {
          expect(res).toBeDefined();
        })
    }))
});

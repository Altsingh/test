import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { FormRolePermissionTO } from './pojo/form-role-permission.model';

@Injectable()
export class ApplicationHttpService {


  private url: string = environment.url + "/apps";

  constructor(private http: HttpClient) { }

  getFormForMenu(appId) {
    return this.http.get(this.url + "/" + appId, this.getOptions());
  }

  getApp(appId: String) {
    return this.http.get(this.url + "/" + appId, this.getOptions());
  }

  uploadImage(requestData) {
    return this.http.post(environment.appLogoUploadUrl, requestData);
  }

  addUpdateModules(appId: String) {

  }

  getApps() {
    return this.http.get(this.url + "?offset=-1&limit=-1", this.getOptions());
  }

  createApp(app) {
    return this.http.post(this.url, this.getBody(app), this.getOptions());
  }

  editApp(app) {
    return this.http.post(this.url, this.getBody(app), this.getOptions());
  }

  saveApp(app) {
    return this.http.put(this.url, this.getBodyForModuleAddition(app), this.getOptions());
  }

  getPreviewAccessToken(url, realm, refreshToken) {
    var params = 'grant_type=refresh_token&client_id=' + realm + '&refresh_token=' + refreshToken;
    return this.http.post(url, params, this.getKeyCloakOptions());
  }

  private getOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      })
    }
  }

  getProtocol() {
    return {
      "userCode": "",
      "userName": "",
      "orgCode": "org2",
      "tenantCode": "",
      "sysFormCode": "",
      "portalType": "SUPERADMIN",
      "loggedInUserIdentifier": "2",
      "loggedInTimestamp": 346834638434,
      "jwt": "a"
    }
  }

  getBody(app) {
    return {
      "protocol": this.getProtocol(),
      "action": "CREATE",
      "app": app
    }
  }

  getBodyForModuleAddition(app) {

    return {
      "protocol": this.getProtocol(),
      "app": app

    }
  }

  getKeyCloakOptions() {
    return {
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-type': 'application/x-www-form-urlencoded'
      })
    }
  }

  changePermission(formRolePermission: FormRolePermissionTO) {
    return this.http.post(this.url + '/changeFormPermission', formRolePermission, this.getOptions());
  }

}

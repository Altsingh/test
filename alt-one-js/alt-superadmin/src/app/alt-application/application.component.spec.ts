import { ApplicationComponent } from './application.component';


import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { AltCommonModule } from "../alt-common/alt-common.module";
import { HttpModule } from "@angular/http";
import { RequestDispatcher } from "../alt-common/request.dispatcher";


describe('AppComponent', function () {
  let de: DebugElement;
  let addButton: DebugElement;
  let comp: ApplicationComponent;
  let requestDispatcher: RequestDispatcher
  let fixture: ComponentFixture<ApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApplicationComponent],
      imports: [ RouterTestingModule, FormsModule, HttpModule, AltCommonModule ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationComponent);
    comp = fixture.componentInstance;
    requestDispatcher = fixture.debugElement.injector.get(RequestDispatcher);
    de = fixture.debugElement.query(By.css('alt-data-table'));
    addButton  = fixture.debugElement.query(By.css('.addPackage'));
  });

  xit('should create app-list component', () => expect(comp).toBeDefined());

  xit('should have alt-data-table component inside app-list component', () => expect(de).toBeDefined());

  xit("should have edit, create and delete popup hidden at initialization", () => {
    expect(comp.editMode).toBe(false);
    expect(comp.createMode).toBe(false);
    expect(comp.deleteMode).toBe(false);
  });

  xit("should fetch metadata when I click on add button", () => {
    spyOn(requestDispatcher, 'getPaginationMeta').and.returnValue({
      "respData":[]
    });
    addButton.triggerEventHandler('click', null);
    expect(requestDispatcher.getPaginationMeta).toHaveBeenCalled();
  });
});

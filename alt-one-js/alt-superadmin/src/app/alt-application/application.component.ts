import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationModel } from "./pojo/application.model";
import { ApplicationHttpService } from "./application-http.service";
import { AltCommonService } from "../alt-common/alt-common.service";
import { DefaultDataService } from "../form/default-data.service";

@Component({
    templateUrl: 'application.component.html'
})
export class ApplicationComponent implements OnInit {
    private dataMap: Map<string, boolean>;
    sortingIndex: number;
    apps: ApplicationModel[];
    allApps: ApplicationModel[];
    app: ApplicationModel;
    createMode: boolean = false;
    editMode: boolean = false;
    deleteMode: boolean = false;
    interval: any;
    message: String;
    uploadedURL: string;
    isLogoUploaded: boolean = false;
    //@ViewChild('fileUpload')
    //applogoFileUploadElement:ElementRef;

    constructor(
        private applicationHttpService: ApplicationHttpService,
        private activatedRoute: ActivatedRoute,
        private commonService: AltCommonService,
        private router: Router,
        private defaultDataService: DefaultDataService) { }

    ngOnInit() {
        this.dataMap = new Map<string, boolean>();
        this.dataMap.clear();
        this.getAppList();
    }

    getAppList() {
        this.applicationHttpService.getApps()
            .subscribe((res: ApplicationModel[]) => {
                this.apps = res;
                this.allApps = res;
            })
    }

    openCreateAppWindow() {
        this.app = new ApplicationModel();
        this.createMode = true;
    }

    openEditAppWindow(app) {
        this.app = new ApplicationModel();
        this.app.appName = this.defaultDataService.removeDoubleQuotes(app.appName);
        this.app.appCode = this.defaultDataService.removeDoubleQuotes(app.appCode);
        this.app.createdBy = app.createdBy;
        this.app.createdDate = app.createdDate;
        this.app.status = app.status;
        this.app.appId = this.defaultDataService.removeDoubleQuotes(app.appId);
        this.app.modules = app.modules;
        this.app.homeCards = app.homeCards;
        this.app.appLogoUrl = app.appLogoUrl;
        this.editMode = true;
    }

    openDeleteAppWindow(app) {
        this.deleteMode = true;
    }

    routeToModules(app) {
        this.router.navigateByUrl("/application/app/" + this.commonService.replaceHashStringByUnderScore(app.appId) + "/module");
    }

    cancel() {
        this.disableCRUDMode();
        this.app = null;
    }

    submit() {
        if (this.createMode || this.editMode) {
            debugger
            this.app.isApplicationForEditing = false
            if (this.editMode) {
                this.app.isApplicationForEditing = true;
                if (this.isLogoUploaded) {
                    this.app.appLogoUrl = this.uploadedURL;
                }
            }

            this.app.tenantId = atob(localStorage.getItem("tenantId"));

            if (!this.editMode) {
                if (this.isLogoUploaded) {
                    this.app.appLogoUrl = this.uploadedURL;
                } else {
                    this.app.appLogoUrl = undefined;
                }
            }

            this.disableCRUDMode();
            this.applicationHttpService.createApp(this.app)
                .subscribe((res: any) => {
                    let body = res;
                    this.getAppList();
                    this.app.appId = body.appId;
                    this.routeToModules(this.app)
                })
            this.isLogoUploaded = false;
            this.uploadedURL = undefined;
        } else {
            this.applicationHttpService.editApp(this.app)
                .subscribe(res => {
                    res = res;
                    this.getAppList();
                })
        }
    }

    ifEdit() {
        if (this.editMode) {
            return true;
        }
        return false;
    }
    delete() {

    }
    disableCRUDMode() {
        this.createMode = false;
        this.editMode = false;
        this.deleteMode = false;
    }

    uploadAppLogo(type, event) {
        this.uploadedURL = undefined;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            let formData: FormData = new FormData();
            formData.append("uploadingFiles", file, file.name);
            if (this.validate(type, file.size, file.type) == "") {
                this.applicationHttpService.uploadImage(formData).subscribe((res: any) => {
                    var applogourl = res;
                    this.uploadedURL = applogourl.applogourl;
                    this.isLogoUploaded = true;
                    //this.applogoFileUploadElement.nativeElement.value="";
                });
            } else {
                console.log(this.message);
                this.interval = (function (that) {
                    return setTimeout(function () {
                        that.message = "";
                    }, 1500);
                })(this);
            }
        }
    }

    validate(type, size, fileType): String {
        if (size > 1048576) {
            this.message = "File size is more than 1MB,Can't Upload";
            return this.message;
        }
        if (fileType.indexOf('image') == -1) {
            this.message = "File is not of specifc format";
            return this.message;
        }
        return "";
    }

    sorting(columnHeader: string) {
        var appsTemp: ApplicationModel[] = [];
        columnHeader = this.removeDoubleQuotes(columnHeader);
        var columnHeaderArr = [];
        //find which column to sort
        for (var j = 0; j < this.apps.length; j++) {
            var obj: ApplicationModel = this.apps[j];
            switch (columnHeader) {
                case 'appName':
                    columnHeaderArr.push(this.removeDoubleQuotes(obj.appName));
                    break;
                case 'appId':
                    columnHeaderArr.push(obj.appId);
                    break;
                case 'createdDate':
                    columnHeaderArr.push(obj.createdDate);
                    break;
                case 'createdBy':
                    columnHeaderArr.push(obj.createdBy);
                    break;
                case 'status':
                    columnHeaderArr.push(obj.status);
                    break;

            }
        }
        /**
         * decide the ordering of sorting & also
         * find which column needs to be sorted
         * */
        var sortOrder = false;
        if (this.dataMap.has(columnHeader)) {
            sortOrder = this.dataMap.get(columnHeader);
            sortOrder = !sortOrder;
            this.dataMap.set(columnHeader, sortOrder);
        } else {
            this.dataMap.set(columnHeader, true);
            sortOrder = true;
        }

        columnHeaderArr.sort(this.dynamicSort(sortOrder));

        var columnMap = new Map<number, boolean>();
        var appMap = new Map<number, boolean>();

        for (var l = 0; l < columnHeaderArr.length; l++) {
            for (var m = 0; m < this.apps.length; m++) {
                if (!columnMap.has(l) && !appMap.has(m)) {
                    var objj: ApplicationModel = this.apps[m];
                    switch (columnHeader) {
                        case 'appName':
                            if (columnHeaderArr[l] == (this.removeDoubleQuotes(objj.appName))) {
                                appsTemp.push(objj);
                                columnMap.set(l, true);
                                appMap.set(m, true);
                            }
                            break;
                        case 'appId':
                            if (columnHeaderArr[l] == (this.removeDoubleQuotes(objj.appId))) {
                                appsTemp.push(objj);
                                columnMap.set(l, true);
                                appMap.set(m, true);
                            }
                            break;
                        case 'createdDate':
                            if (columnHeaderArr[l] == (this.removeDoubleQuotes(objj.createdDate))) {
                                appsTemp.push(objj);
                                columnMap.set(l, true);
                                appMap.set(m, true);
                            }
                            break;
                        case 'createdBy':
                            if (columnHeaderArr[l] == (this.removeDoubleQuotes(objj.createdBy))) {
                                appsTemp.push(objj);
                                columnMap.set(l, true);
                                appMap.set(m, true);
                            }
                            break;
                        case 'status':
                            if (columnHeaderArr[l] == (this.removeDoubleQuotes(objj.status))) {
                                appsTemp.push(objj);
                                columnMap.set(l, true);
                                appMap.set(m, true);
                            }
                            break;
                    }
                }
            }
        }

        this.apps = [];
        this.apps = appsTemp;

    }

    dynamicSort(sortOrderFlag: boolean) {
        var sortOrder = 1; //ascending order
        if (sortOrderFlag) {
            sortOrder = 1;
        } else {
            sortOrder = -1; //descending order
        }
        return function (a, b) {
            return (('' + a).localeCompare(b)) * sortOrder;
        }
    }

    removeDoubleQuotes(stringData) {
        if (stringData == null || stringData == 'undefined' || stringData == '') {
            return undefined
        }
        return stringData.replace(/['"]+/g, '')
    }

    filterApps(event: any) {
        var searchTerm = event.currentTarget.value;
        if (searchTerm == "") {
            this.apps = this.allApps;
        } else {
            this.apps = this.allApps.filter(app => {
                return app.appName.toLowerCase().includes(searchTerm.toLowerCase());
            });
        }
    }
}
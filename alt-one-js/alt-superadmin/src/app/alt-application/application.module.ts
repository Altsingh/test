import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { routings } from "./application.routing";
import { AltCommonModule } from "../alt-common/alt-common.module";

import { ApplicationComponent } from "./application.component";
import { ModuleComponent } from './module/module.component';
import { ApplicationHttpService } from "./application-http.service";
import { HomeBuilderComponent } from "./home-builder/home-builder.component";

@NgModule({
    imports: [
        AltCommonModule,
        CommonModule,
        FormsModule,
        HttpClientModule,
        routings
    ],
    declarations: [
        ApplicationComponent,
        ModuleComponent,
        HomeBuilderComponent
    ],
    providers: [
        ApplicationHttpService
    ],
    exports: []
})
export class AltAppModule { }

import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ApplicationComponent } from "./application.component";
import { ModuleComponent } from "./module/module.component";
import { HomeBuilderComponent } from "./home-builder/home-builder.component";

const routes: Routes = [
    { path: 'app', component: ApplicationComponent },
    { path: 'app/:id/module', component: ModuleComponent /*, data: { selectedTab: "menus" }*/ },
    { path: 'app/:id/homebuilder', component: HomeBuilderComponent /*, data: { selectedTab: "homebuilder" }*/ }
]

export const routings: ModuleWithProviders = RouterModule.forChild(routes);
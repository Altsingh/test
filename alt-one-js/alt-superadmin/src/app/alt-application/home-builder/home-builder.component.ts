import { Component, OnInit, Input } from '@angular/core';
import { AltCommonService } from '../../alt-common/alt-common.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApplicationHttpService } from '../application-http.service';
import { HomeCardModel } from '../pojo/homecard.model';

@Component({
  selector: 'app-home-builder',
  templateUrl: './home-builder.component.html',
  styleUrls: ['./home-builder.component.css']
})
export class HomeBuilderComponent implements OnInit {

  @Input() selectedTab;
  appId: String;
  app: any;
  title: string;
  description: string;
  image: string;
  uploadedURL: string;
  isLogoUploaded: boolean = false;
  message: String;
  interval: any;
  hideForm: boolean = true;
  editModeForm: boolean = false;
  createModeForm: boolean = false;
  showlisting: boolean = false;
  editCardId: number;
  homeCardsPrefilled: HomeCardModel[];

  constructor(private commonService: AltCommonService,
    private httpService: ApplicationHttpService,
    private applicationHttpService: ApplicationHttpService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.clear();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.appId = this.commonService.replaceUnderScoreByPercent(params['id']);
      this.getApp();
    });
  }

  getApp() {
    this.httpService.getApp(this.appId)
      .subscribe(res => {
        this.app = res;
        if (this.app.homeCards == null || this.app.homeCards.length <= 0) {
          this.showlisting = false;
          this.app.homeCards = new Array();
        } else if (this.app.homeCards.length > 0) {
          this.showlisting = true;
          this.prepareHomeCardsPrefilled();
        }
      })
  }

  prepareHomeCardsPrefilled() {
    this.homeCardsPrefilled = new Array();
    var model: HomeCardModel;
    for (var i = 0; i < this.app.homeCards.length; i++) {
      model = new HomeCardModel();
      model.title = this.app.homeCards[i].title;
      model.description = this.app.homeCards[i].description;
      model.image = this.app.homeCards[i].image;
      model.id = i;
      this.homeCardsPrefilled.push(model);
    }
  }

  insertCard() {
    this.clear();
    this.createModeForm = true;
    this.hideForm = false;
  }

  clear() {
    this.title = '';
    this.description = '';
    this.image = '';
    this.hideForm = true;
    this.editModeForm = false;
    this.editCardId = -1;
    this.createModeForm = false;
  }
  cancel() {
    this.clear();
  }

  uploadAppLogo(type, event) {
    this.uploadedURL = undefined;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append("uploadingFiles", file, file.name);
      if (this.validate(type, file.size, file.type) == "") {
        this.applicationHttpService.uploadImage(formData).subscribe((res: any) => {
          var applogourl = res;
          this.image = applogourl.applogourl;
          this.isLogoUploaded = true;
        });
      } else {
        console.log(this.message);
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
          }, 1500);
        })(this);
      }
    }
  }

  validate(type, size, fileType): String {
    if (size > 1048576) {
      this.message = "File size is more than 1MB,Can't Upload";
      return this.message;
    }
    if (fileType.indexOf('image') == -1) {
      this.message = "File is not of specifc format";
      return this.message;
    }
    return "";
  }

  setCardData() {
    if (this.createModeForm) {
      var cardData = new HomeCardModel();
      cardData.title = this.title;
      cardData.description = this.description;
      cardData.image = this.image;
      this.app.homeCards.push(cardData);
    } else if (this.editModeForm) {
      for (var i = 0; i < this.app.homeCards.length; i++) {
        if (this.editCardId == i) {
          this.app.homeCards[i].title = this.title;
          this.app.homeCards[i].description = this.description;
          this.app.homeCards[i].image = this.image;
          break;
        }
      }
    }

  }

  submit() {
    this.setCardData();
    this.httpService.saveApp(this.app)
      .subscribe(data => {
        //this.hideForm = true;
        this.clear();
        this.getApp();
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
          }, 3000);
        })(this);
      }, err => {
        this.getApp();
      });
  }

  routeToModules() {
    if (this.app != null && this.app != 'undefined' && this.app.appId != null && this.app.appId != 'undefined') {
      this.router.navigateByUrl("/application/app/" + this.commonService.replaceHashStringByUnderScore(this.app.appId) + "/module");
    }
  }

  routeToFormList() {
    this.router.navigateByUrl("/form/list/" + this.app.appId);
  }

  editHomeCard(editCardDetail: HomeCardModel) {
    this.editModeForm = true
    this.hideForm = false;
    this.editCardId = editCardDetail.id;
    this.title = editCardDetail.title;
    this.description = editCardDetail.description;
    this.image = editCardDetail.image;
  }

  deleteHomeCard(editCardDetail: HomeCardModel) {
    this.app.homeCards.splice(editCardDetail.id, 1);
    this.submit();
  }
}

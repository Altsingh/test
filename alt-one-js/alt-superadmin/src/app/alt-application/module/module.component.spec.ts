import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule } from '@angular/forms';
import { ModuleComponent } from './module.component';
import { ApplicationHttpService } from "../application-http.service";

describe('ModuleComponent', () => {
  let component: ModuleComponent;
  let httpService: ApplicationHttpService;
  let fixture: ComponentFixture<ModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModuleComponent]
    })
      .compileComponents();
    spyOn(httpService, 'getModules').and.returnValue({
      "app": {
        "appName": "App001",
        "appCode": "",
        "module": [
          {
            "name": "module001",
            "desc": "",
            "iconPath": "",
            "status": "",
            "sequence": "",
            "menus": [],
            "menuGroups": [
              {
                "name": "yfsd",
                "desc": "",
                "status": "",
                "sequence": "",
                "menus": []
              }
            ]
          },
          {
            "name": "module002",
            "desc": "",
            "iconPath": "",
            "status": "",
            "sequence": "",
            "menus": [],
            "menuGroups": [
              {
                "name": "yfsd",
                "desc": "",
                "status": "",
                "sequence": "",
                "menus": []
              }
            ]
          },
          {
            "name": "module003",
            "desc": "",
            "iconPath": "",
            "status": "",
            "sequence": "",
            "menus": [],
            "menuGroups": [
              {
                "name": "yfsd",
                "desc": "",
                "status": "",
                "sequence": "",
                "menus": []
              }
            ]
          }
        ]
      }
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });

  xit('should fetch modules of an app on initialize', () => {
    expect(component.modules.length).toBe(3);
  })
});

import { Component, Input, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AltCommonService } from "../../alt-common/alt-common.service";
import { ApplicationHttpService } from "../application-http.service";
import { ModuleModel } from "../pojo/module.model";
import { MenuModel } from "../pojo/menu.model";
import { AttachedForm } from "../pojo/menu.model";
import { FormHttpService } from "../../form/form-http.service";
import { FormModel } from "../../form/pojo/form.model";
import { RequestDispatcher } from "../../alt-common/request.dispatcher";
import { notifications } from "../../alt-common/static.notifications";
import { environment } from "src/environments/environment";
import { FormRolePermissionTO } from '../pojo/form-role-permission.model';
import { ExternalFormData } from '../../form/models/form-external/external-form-data.model';
import { Organisation } from '../../form/models/form-external/organisation.model';
import { EnumFormType } from '../../form/enum/external-form-enum/enum-form-type';




@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css'],
  host: {
    '(document:click)': 'handleClick($event)',
  },
})
export class ModuleComponent implements OnInit {
  appPage: string = environment.appPage;
  message: string;
  interval: any;
  selectedModule: ModuleModel;
  app: any;
  accessData: any;
  appId: String;
  editableModuleIndex: number;
  attachableModuleIndex: number;
  editableMenuIndex: number;
  editableMenuModuleIndex: number;
  popupMenuIndex: number;
  popupMenuModuleIndex: number;
  deletMenuIndex: number;
  elementRef;
  showListPopUp: boolean;
  forms: FormModel[];
  filteredForms: FormModel[];
  selectedMenu: MenuModel;
  showRolesPopup: boolean;
  selectedForm: any
  selectedOrganisation: Organisation = null;

  //PAGINATION
  @Input() offset: number = 0;
  @Input() limit: number = 10;
  @Input() order: string = "ASC";
  @Input() orderBy: string = "id";
  @Input() type: string;
  @Input() metaType: string;
  @Input() docId: string = null;

  indexingData: any;
  totalPages: number;
  currentPage: number;
  totalRecords: number;
  presentRecords: number;

  errorMessage: string;
  loading: boolean = true;
  data: any[];
  filteredRoles: any[];
  selectedTab: string;
  form: FormModel;
  externalFormData: ExternalFormData;


  constructor(
    private httpService: ApplicationHttpService,
    private activatedRoute: ActivatedRoute,
    private commonService: AltCommonService,
    private router: Router,
    myElement: ElementRef,
    private formHttpService: FormHttpService) {
    this.elementRef = myElement;

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.appId = this.commonService.replaceUnderScoreByPercent(params['id']);
      this.getApp();
    });

    this.selectedOrganisation = this.formHttpService.getLoggedInOrganizationDetail();
  }

  getApp() {
    this.httpService.getApp(this.appId)
      .subscribe(res => {
        this.app = res;
      })
  }

  getPreview() {
    var keycloakUrl = environment.keycloak_url;
    var realm = atob(localStorage.getItem("realm"));
    var refreshToken = localStorage.getItem("refreshToken");
    var tokenUrl = keycloakUrl + 'auth/realms/' + realm + '/protocol/openid-connect/token';
    this.httpService.getPreviewAccessToken(tokenUrl, realm, refreshToken)
      .subscribe(res => {
        this.accessData = res;
        /**
         * add getApp() for home page listing, to make this method
         * to work for that page as well 
         * 
        */
        var openUrl = environment.appPage + "securelogin/" + this.app.appCode + "?accessToken=" + this.accessData.access_token +
          "&refreshToken=" + this.accessData.refresh_token + "&isCustom=false&orgId=" + realm + "&tenantId=" + atob(localStorage.getItem("tenantId"));
        window.open(openUrl);
        //this.getApp();        
        console.log(res);
      })
  }

  insertModule() {
    this.app.modules.push(new ModuleModel());
    this.editableModuleIndex = this.app.modules.length - 1;
    this.editableMenuIndex = -1;
    this.editableMenuModuleIndex = -1
  }

  insertMenu(module, moduleIndex) {
    module.menus.push(new MenuModel());
    this.editableModuleIndex = -1;
    this.editableMenuIndex = module.menus.length - 1;
    this.editableMenuModuleIndex = moduleIndex;
  }

  deleteMenu(module, moduleIndex, deletMenuIndex) {
    module.menus.splice(deletMenuIndex, 1)
    this.editableModuleIndex = -1;
    this.editableMenuIndex = module.menus.length - 1;
    this.editableMenuModuleIndex = moduleIndex;
    this.submit();
  }

  cancel() {
    this.getApp();
  }

  uncheckOtherHome(homeMenu:any){
    this.app.modules.forEach(module=>{
      module.menus.forEach(menu=>{
        if(!(menu.name==homeMenu.name)){
          menu.landingPage=false;
        }
      })
    })
  }

  submit() {
    this.httpService.saveApp(this.app)
      .subscribe(data => {
        this.getApp();
        this.editableModuleIndex = -1;
        this.editableMenuIndex = -1;
        this.editableMenuModuleIndex = -1;
        this.message = notifications.updated_successful;
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
          }, 3000);
        })(this);
      }, err => {
        this.getApp();
        this.editableModuleIndex = -1;
        this.editableMenuIndex = -1;
        this.editableMenuModuleIndex = -1;
      });
  }

  edithisfield(editableModuleIndex, editableMenuIndex, editableMenuModuleIndex) {
    this.editableModuleIndex = editableModuleIndex;
    this.editableMenuIndex = editableMenuIndex;
    this.editableMenuModuleIndex = editableMenuModuleIndex;
  }

  enableModuleClickPopup(popupMenuIndex, popupMenuModuleIndex) {
    this.popupMenuIndex = popupMenuIndex;
    this.popupMenuModuleIndex = popupMenuModuleIndex;
    this.editableModuleIndex = null;
    this.editableMenuIndex = null;
    this.editableMenuModuleIndex = null;
  }

  onClickOutside(event: Object) {
    if (event && event['value'] === true) {
    } else {
    }
  }

  handleClick(event) {
    var clickedComponent = event.target;
    if (!clickedComponent.classList.contains("menu-style-icon")) {
      this.popupMenuIndex = null;
      this.popupMenuModuleIndex = null;
    }
  }

  newformPreInstantiation(formName: string) {
    this.form = new FormModel();
    this.form.formName = formName;
    this.externalFormData = new ExternalFormData();
    this.externalFormData.orgId = this.selectedOrganisation.orgId;
    this.externalFormData.tenantId = this.selectedOrganisation.tenantId;
    /** this should be the default as no form as 
     * significance without org id
     **/
    this.externalFormData.customFormType = EnumFormType.CUSTOMFORM;
  }

  prepareNewlyCreatedmenu(editableMenuModuleIndex, editableMenuIndex, menu) {
    this.selectedModule = null;
    this.selectedMenu = new MenuModel();
    var keys = Object.keys(menu);
    keys.forEach(key => {
      this.selectedMenu[key] = menu[key];
    });
    this.editableMenuModuleIndex = editableMenuModuleIndex;
    this.editableMenuIndex = editableMenuIndex;
  }

  attachNewFormToNewlyCreatedmenu(formName, formId) {
    this.selectedMenu.form = new AttachedForm();
    this.selectedMenu.form.formId = formId;
    this.selectedMenu.form.formName = formName;
  }

  /**
   * @author vaibhav kashyap
   * this method is called when an form is click attached with a menu in edit mode
   * or a new menu is created then a new form is created
  */
  createNewForm(editableMenuModuleIndex, editableMenuIndex, menu) {
    if (menu.form != null) {
      this.editForm(menu.form.formId);
    } else {
      this.newformPreInstantiation(menu.name);
      this.formHttpService.createForm(this.form, this.externalFormData, this.app.appId)
        .subscribe((res: any) => {
          if (res.status == 200) {
            var body = res.body;

            this.prepareNewlyCreatedmenu(editableMenuModuleIndex, editableMenuIndex, menu);
            this.attachNewFormToNewlyCreatedmenu(this.form.formName, body.formId);
            this.attachForm();

            this.message = notifications.save_successful;
            this.interval = (function (that) {
              return setTimeout(function () {
                that.message = "";
              }, 3000);
            })(this);
            /**
             * Auto save the newly created form
            */
            this.submit();
            this.getForms();
            /**
             * open form in edit mode
            */
            this.editForm(body.formId);
          } else {
            this.message = notifications.form_save_unsuccessful;
          }
        },
          error => {
            this.message = notifications.form_save_unsuccessful;;
          });
    }

  }

  editForm(id) {
    this.router.navigateByUrl("form/edit/" + this.commonService.replaceHashStringByUnderScore(id));
  }

  openModuleAttachPopUp(attachableModuleIndex, module) {
    this.getForms();
    this.selectedMenu = null;
    this.selectedModule = new ModuleModel();
    var keys = Object.keys(module);
    keys.forEach(key => {
      this.selectedModule[key] = module[key];
    });
    this.attachableModuleIndex = attachableModuleIndex;
    this.showListPopUp = true;
  }

  openMenuAttachPopUp(editableMenuModuleIndex, editableMenuIndex, menu) {
    this.getForms();
    this.prepareNewlyCreatedmenu(editableMenuModuleIndex, editableMenuIndex, menu)
    this.showListPopUp = true;
  }

  closeFormListPopUp() {
    this.showListPopUp = false;
    this.editableMenuIndex = -1;
    this.editableMenuModuleIndex = -1;
    this.attachableModuleIndex = -1;
  }

  selectForm(form: FormModel) {
    if (this.selectedMenu) {
      this.attachNewFormToNewlyCreatedmenu(form.formName, form.uiClassCode);
    } else {
      this.selectedModule.form = new AttachedForm();
      this.selectedModule.form.formId = form.uiClassCode;
      this.selectedModule.form.formName = form.formName;
    }
  }

  attachForm() {
    if (this.selectedMenu) {
      this.app.modules[this.editableMenuModuleIndex].menus[this.editableMenuIndex] = this.selectedMenu;
    } else {
      this.app.modules[this.attachableModuleIndex] = this.selectedModule;
    }
    this.closeFormListPopUp();
  }

  removeSelectedModule() {
    this.selectedModule.form = null;
  }

  removeSelectedMenu() {
    this.selectedMenu.form = null;
  }

  //PAGINATION

  //set pagination params to be sent to request
  setPaginationParams() {
    this.indexingData = {
      offset: this.offset,
      limit: this.limit,
      order: this.order,
      orderBy: this.orderBy
    }
  }

  //set pagination data for controls
  setPaginationDataForControls(total, presentRecords) {
    if (total == 0) {
      this.totalRecords = 0;
      this.presentRecords = 0;
      this.totalPages = 0;
      this.currentPage = 0;
    } else {
      this.totalRecords = total;
      this.presentRecords = presentRecords;
      this.totalPages = Math.floor(this.totalRecords / this.limit) + (this.totalRecords % this.limit > 0 ? 1 : 0);
      this.currentPage = (this.offset / this.limit) + (this.offset % this.limit > 0 ? 1 : 0);
    }
  }


  //fetch data for pagination
  getForms() {
    this.loading = true;
    this.setPaginationParams();

    this.formHttpService.getPublishedForms(-1, -1, this.appId)
      .subscribe((res: any) => {
        this.loading = false;
        var totalCount = res.headers.get("totalCount");
        this.forms = res.body;
        this.filteredForms = res.body;
        this.setPaginationDataForControls(totalCount, this.forms.length);

      });

  }

  refresh() {
    this.data = [];
    if (!this.type) {
      this.errorMessage = "type of list is not specified";
    } else {
      this.errorMessage = "";
      this.getForms();
    }
  }
  //PAGINATION ENDS

  openRolesPopup(form: any) {
    this.showRolesPopup = true;
    this.selectedForm = form;
    this.filteredRoles = this.app.roles;
  }

  closeRolesPopup() {
    this.showRolesPopup = false;
  }

  processRoleCheckbox(role: any, event: any) {
    var formRolePermission = new FormRolePermissionTO();
    formRolePermission.hrFormId = Number(this.selectedForm.formSecurityId);
    formRolePermission.roleId = role.roleId;
    if (event.currentTarget.checked) {
      this.selectedForm.assignedRoles.push(role);
      formRolePermission.webAccessible = true;
    } else {
      this.selectedForm.assignedRoles = this.selectedForm.assignedRoles.filter(e => e.roleId != role.roleId);
      formRolePermission.webAccessible = false;
    }
    this.httpService.changePermission(formRolePermission).subscribe(res => {
      console.log(res);
    });
  }

  determineSelection(assignedRoles: any, role: any) {
    return assignedRoles.some(e => e.roleId == role.roleId);
  }

  filterRoles(event: any) {
    var searchTerm = event.currentTarget.value;
    this.filteredRoles = this.app.roles.filter(role => {
      return role.roleName.toLowerCase().includes(searchTerm.toLowerCase());
    });
  }

  filterForms(event: any) {
    var searchTerm = event.currentTarget.value;
    this.filteredForms = this.forms.filter(form => {
      return form.formName.toLowerCase().includes(searchTerm.toLowerCase());
    });
  }

  routeToHomeBuilder() {
    this.router.navigateByUrl("/application/app/" + this.commonService.replaceHashStringByUnderScore(this.app.appId) + "/homebuilder");
  }

  routeToFormList() {
    this.router.navigateByUrl("/form/list/" + this.app.appId);
  }
}


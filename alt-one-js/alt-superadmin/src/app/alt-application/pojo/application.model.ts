export class ApplicationModel {
    appName: string;
    appCode: string;
    createdBy: string;
    createdDate: string;
    status: string;
    appId: string;
    tenantId: string;
    appLogoUrl: string;
    modules: any[];
    homeCards: any[];
    isApplicationForDeletion: boolean;
    isApplicationForEditing: boolean

    constructor() {
        this.appName = "";
        this.appCode = "";
        this.createdBy = null;
        this.createdDate = null;
        this.status = null;
        this.appId = null;
        this.modules = [];
        this.homeCards = [];
    }
}
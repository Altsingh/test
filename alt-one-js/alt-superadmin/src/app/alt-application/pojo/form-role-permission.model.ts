export class FormRolePermissionTO {
    hrFormRolePermissionId: number;

    hrFormId: number;

    roleId: number;

    webAccessible: boolean;
}
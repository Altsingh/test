import { AttachedForm } from "./menu.model";

export class HomeCardModel {
    title: string;
    description: string;
    image: string;
    id: number;

    constructor() {
        this.title = '';
        this.description = '';
        this.image = '';
    }
}
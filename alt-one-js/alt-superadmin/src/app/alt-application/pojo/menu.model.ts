export class MenuModel {
    name: String;
    menus: MenuModel[];
    form: AttachedForm

    constructor() {
        this.name = null;
        this.menus = null;
        this.form = null;
    }
}
export class AttachedForm {
    formName: String;
    formId: String;
    constructor(){
        this.formName="";
        this.formId="";
    }
}
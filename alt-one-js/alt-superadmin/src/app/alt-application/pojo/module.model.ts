import { AttachedForm } from "./menu.model";

export class ModuleModel {
    name: string;
    oldName: string;
    sequence: number;
    iconPath: string;
    menuGroups: any[];
    menus: any[];
    form: AttachedForm;

    constructor() {
        this.name = null;
        this.oldName = null;
        this.sequence = 0;
        this.iconPath = null;
        this.menuGroups = [];
        this.menus = [];
        this.form = null;
    }
}
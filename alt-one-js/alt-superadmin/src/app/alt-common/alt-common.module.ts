import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { DataTableComponent } from "./alt-datatable.component";
import { RequestDispatcher } from "./request.dispatcher";
import { CommaSeparatedStringPipe, RemoveQuotesFilter } from "./common.filters";
import { MilliSecondsToDateFilter } from "./common.filters";
import { AltCommonService } from "./alt-common.service";
import { ReadWriteMapper } from "./read-write.mapper.service";
import { AltUtilService } from "./alt-util.servive";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule
    ],
    declarations: [
        CommaSeparatedStringPipe,
        DataTableComponent,
        MilliSecondsToDateFilter,
        RemoveQuotesFilter
    ],
    providers: [
        RequestDispatcher,
        AltCommonService,
        ReadWriteMapper,
        AltUtilService
    ],
    exports: [
        DataTableComponent,
        CommaSeparatedStringPipe,
        MilliSecondsToDateFilter,
        RemoveQuotesFilter
    ]
})
export class AltCommonModule { }
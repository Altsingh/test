import { Injectable } from "@angular/core";

@Injectable()
export class AltCommonService {

    constructor() {

    }

    replaceHashStringByUnderScore(str: string) {
        return str.replace(new RegExp('#', 'g'), '_');
    }

    replaceUnderScoreByHash(str: string) {
        return str.replace(new RegExp('_', 'g'), '#');
    }

    replaceUnderScoreByPercent(str: string) {
        return str.replace(new RegExp('_', 'g'), '%23');
    }
}
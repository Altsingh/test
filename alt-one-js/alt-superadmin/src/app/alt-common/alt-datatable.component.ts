import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import { RequestDispatcher } from "./request.dispatcher";
import { ClassMeta } from "../alt-pojo/class-meta.model";

@Component({
    selector: 'alt-data-table',
    templateUrl: 'alt-datatable.component.html'
})
export class DataTableComponent implements OnInit {
    @Input() offset: number = 1;
    @Input() limit: number = 10;
    @Input() order: string = "ASC";
    @Input() orderBy: string = "id";
    @Input() type: string;
    @Input() metaType: string;
    @Input() docId: string = null;

    indexingData: any;
    totalPages: number;
    currentPage: number;
    totalRecords: number;
    presentRecords: number;

    errorMessage: string;
    loading: boolean = true;

    meta: ClassMeta;
    data: Map<string,object>;

    @Output()
    editEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    deleteEvent: EventEmitter<any> = new EventEmitter<any>();
    @Output()
    childListEvent: EventEmitter<any> = new EventEmitter<any>();

    constructor(private requestDispatcher: RequestDispatcher) { }

    ngOnInit() {
        this.refresh();
    }

    //set pagination params to be sent to request
    setPaginationParams() {
        this.indexingData = {
            offset: this.offset,
            limit: this.limit,
            order: this.order,
            orderBy: this.orderBy
        }
    }

    //set pagination data for controls
    setPaginationDataForControls(total, presentRecords) {
        if(total==0){
            this.totalRecords = 0;
            this.presentRecords = 0;
            this.totalPages = 0;
            this.currentPage = 0;
        } else {
            this.totalRecords = total;
            this.presentRecords = presentRecords;
            this.totalPages = Math.floor(this.totalRecords / this.limit) + (this.totalRecords % this.limit > 0 ? 1 : 0);
            this.currentPage = (this.offset / this.limit) + (this.offset % this.limit > 0 ? 1 : 0);
        }
    }

    fetchPaginationMeta() {
        return this.requestDispatcher.getPaginationMeta(this.metaType, this.docId)
            .subscribe(res => {
                this.meta = res['respData'];
                this.fetchPaginationData();
            });
    }

    //fetch data for pagination
    fetchPaginationData() {
        this.loading = true;
        this.setPaginationParams();
        this.requestDispatcher.getPaginationData(this.type, this.docId, this.indexingData)
            .subscribe(res => {
                this.loading = false;
                this.data = res['respData']['response'];
                this.setPaginationDataForControls(res['respData']['doccumentCount'], this.data.size);
            });
    }

    getFirstPage() {
        this.offset = 0;
        this.fetchPaginationData();
        console.log(this.offset);
    }

    getPreviousPage() {
        this.offset = this.offset - this.limit;
        this.fetchPaginationData();
        console.log(this.offset);
    }

    getNextPage() {
        this.offset = this.offset + this.limit;
        this.fetchPaginationData();
        console.log(this.offset);
    }

    getLastPage() {
        this.offset = this.limit * (this.totalPages - 1);
        this.fetchPaginationData();
        console.log(this.offset);
    }

    refresh() {
        this.data = null;
        if (!this.type) {
            this.errorMessage = "type of list is not specified";
        } else {
            this.errorMessage = "";
            if (this.metaType) {
                this.fetchPaginationMeta();
            } else {
                this.fetchPaginationData();
            }
        }
    }

    editItem(item) {
        this.editEvent.emit(item);
    }

    deleteItem(item) {
        this.deleteEvent.emit(item);
    }

    openChildListEvent(id){
        this.childListEvent.emit({
            "docId":this.docId,
            "id":id
        });
    }
}
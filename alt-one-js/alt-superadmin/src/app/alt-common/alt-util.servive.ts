import { Injectable } from "@angular/core";

@Injectable()
export class AltUtilService {
    
    constructor() {

    }

    hasDuplicates(array, prop) {
        var valuesSoFar = Object.create(null);
        for (var i = 0; i < array.length; ++i) {
            var value = array[i][prop];
            if (value in valuesSoFar) {
                return true;
            }
            valuesSoFar[value] = true;
        }
        return false;
    }
}
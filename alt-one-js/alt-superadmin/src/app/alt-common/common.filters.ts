import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'commaseparated',
})
export class CommaSeparatedStringPipe implements PipeTransform {
    transform(value: any): string {
        if (value.length <= 0) {
            return "None";
        } else {
            let commaString = "";
            let i;
            for (i = 0; i < value.length - 1; i++) {
                commaString += value[i].name + ", ";
            }
            commaString += value[i];
            return commaString;
        }
    }
}

@Pipe({
    name: 'dateFilter',
})
export class MilliSecondsToDateFilter implements PipeTransform {
    transform(value: any): string {
        var dateinmillisec = parseInt(value);
        var date = new Date(dateinmillisec);
        var month = date.getMonth()+1;
        var dateStr = date.getDate() + "/" + month + "/" + date.getFullYear();
        return dateStr;
    }
}

@Pipe({
    name: 'removeQuotes',
})
export class RemoveQuotesFilter implements PipeTransform {
    transform(value: any): string {
        var result = value.replace(/['"]+/g, '');
        return result;
    }
}
import { Injectable } from "@angular/core";
import { ClassMeta } from "../alt-pojo/class-meta.model";
import { KernelClassProcessData } from "../alt-pojo/kernel-class-process-data.model";

@Injectable()
export class ReadWriteMapper {

    convertClassMetaToKernelClassMeta(classMeta: ClassMeta) {
        var kernelClassMeta = new KernelClassProcessData;
        kernelClassMeta.name = classMeta.name;
        kernelClassMeta.classCode = classMeta.classCode;
        kernelClassMeta.status = classMeta.status;
        kernelClassMeta.locationSpecific = classMeta.locationSpecific;
        kernelClassMeta.customAttributeAllowed = classMeta.customAttributeAllowed;
        kernelClassMeta.packageCodes = classMeta.packageCodes;
        kernelClassMeta.instanceLimiter = classMeta.instanceLimiter;
        kernelClassMeta.instancePrefix = classMeta.instancePrefix;
        kernelClassMeta.parameterType = classMeta.parameterType;
        kernelClassMeta.cas = classMeta.cas;
        kernelClassMeta.readLock = classMeta.readLock;
        kernelClassMeta.attributes = { 
            "UPDATE": [],
            "CREATE": []
        };
        classMeta.attributes.forEach(attribute => {
            kernelClassMeta.attributes["UPDATE"].push(attribute);
        });
        return kernelClassMeta;
    }

}
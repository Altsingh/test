import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { protocol } from "./static.protocol";
import { map } from 'rxjs/operators';

@Injectable()
export class RequestDispatcher {

    //  serverUrl: string = "http://10.226.0.219:8094/business/fetchall";
    //  createUpdateDelete: string = "http://10.226.0.219:8094/business/createupdatedelete";

    //for local
    serverUrl: string = "http://localhost:8094/business/fetchall";
    createUpdateDelete: string = "http://localhost:8094/business/createupdatedelete";

    constructor(private http: HttpClient) { }

    //request body
    getBody(type, docId, indexingData, instanceData) {
        let body = {
            "protocol": protocol,
            "type": type,
            "docId": docId
        }
        if (indexingData) {
            body['indexingData'] = indexingData;
        }
        if (instanceData) {
            body['instanceData'] = instanceData;
        }
        return body;
    }

    //request body for instance creation and deletion
    getBodyForInstanceCreationEdition(action, classCode, instance, type: string) {
        let body = {
            "protocol": protocol,
            "type": type,
            "instanceData": {
                "action": action,
                "instanceData": {
                    "type": "INSTANCE",
                    "classCode": classCode,
                    "attributes": instance.attributes,
                    "instanceCode": instance.id,
                    "cas": instance.cas,
                    "status": instance.status
                }
            }
        }
        return body;
    }

    getBodyForClassCreationEdition(action, packageCode, classInstance) {
        let body = {
            "protocol": protocol,
            "type": "CLASS",
            "kernelClassProcessRequest": {
                "action": action,
                "classData": classInstance
            }
        }
        return body;
    }

    //pagination meta request
    getPaginationMeta(metaType, docId) {
        return this.http.post(this.serverUrl, this.getBody(metaType, docId, null, null), null)
            .pipe(map(res => {
                res['respData'] = JSON.parse(res['respData']);
                return res;
            }))
    }

    //pagination data request
    getPaginationData(type, docId, indexingData) {
        return this.http.post(this.serverUrl, this.getBody(type, docId, indexingData, null), null)
            .pipe(map(res => {
                res['respData'] = JSON.parse(res['respData']);
                return res;
            }))
    }

    removeItem(type, docId, item) {
        var instanceData = {
            "action": "DELETE",
            "instanceData": {
                "type": type,
                "instanceCode": item
            }
        }
        return this.http.post(this.serverUrl, this.getBody(type, docId, null, instanceData), null)
            .pipe(map(res => {
                res['respData'] = JSON.parse(res['respData']);
                return res;
            }))
    }

    createEditInstance(action, classCode, instance, type) {
        return this.http.post(this.createUpdateDelete, this.getBodyForInstanceCreationEdition(action, classCode, instance, type), null)
            .pipe(map(res => {
                return res["respData"];
            }))
    }

    createEditClass(action, packageCode, classInstance) {
        return this.http.post(this.createUpdateDelete, this.getBodyForClassCreationEdition(action, packageCode, classInstance), null)
            .pipe(map(res => {
                return res;
            }))
    }

}
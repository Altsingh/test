export const errors={
    "save_error": "Not able to save",
    "created_error": "Not able to create",
    "updated_error": "Not able to update"
}
export const notifications={
    "save_successful": "Saved successfully",
    "created_successful": "Created successfully",
    "updated_successful": "Updated successfully",
    "empty_field_type":"Please select all fields",
    "form_unpublished":"Error:Form is unpublished",
    "security_sent":"Pushed to security successfully",
    "options_empty_alphanumeric":"Option values cannot be empty or non-alphanumeric",
    "form_save_unsuccessful":"Form Creation Failed"
}

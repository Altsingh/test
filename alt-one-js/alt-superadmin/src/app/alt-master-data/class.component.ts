import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';
import { DataTableComponent } from "../alt-common/alt-datatable.component";
import { RequestDispatcher } from "../alt-common/request.dispatcher";
import { AltCommonService } from "../alt-common/alt-common.service";
import { KernelClassProcessData } from "../alt-pojo/kernel-class-process-data.model";
import { ClassMeta } from "../alt-pojo/class-meta.model";
import { ToastrService } from 'ngx-toastr';
import { AttributeMeta } from "../alt-pojo/attribute-meta.model";
import { ReadWriteMapper } from "../alt-common/read-write.mapper.service";
import { ClassmasterdataService } from "../services/masterdata/classmasterdata.service";
import { CLassMasterData } from "../alt-pojo/MasterData/class-master-data.model";
import { CLassCEMasterData } from "../alt-pojo/MasterData/classCE-master-data.model";
import { AttributeData } from "../alt-pojo/MasterData/AttributeData.model";

@Component({
    templateUrl: 'class.component.html'
})
export class ClassComponent implements OnInit {

    @ViewChild(DataTableComponent) dataTable: DataTableComponent;
    docId;
    classInstance: KernelClassProcessData;
    classMeta: ClassMeta;
    attribute: AttributeMeta;
    editAttributeMode = null;
    createAttributeMode: boolean;
    createMode: boolean = false;
    editMode: boolean = false;
    deleteMode: boolean = false;
    classMasterdata: CLassMasterData[] = [];
    classCreateRequest: CLassCEMasterData;
    currentAttribute: AttributeData;
    editAttributeIndex: number = -1;
    editClassdata: CLassCEMasterData;



    constructor(
        private toastr: ToastrService,
        private classMasterDataService: ClassmasterdataService,
        private readWriteMapper: ReadWriteMapper,
        private activatedRoute: ActivatedRoute,
        private requestDispatcher: RequestDispatcher,
        private commonService: AltCommonService,
        private router: Router) {
    }

    ngOnInit() {
        this.getClassMasterData();
    }

    ngOnDestroy() {
    }


    childListRouter(name) {
        this.router.navigateByUrl("/master-data/instance/" + name);
    }


    cancel() {
        this.createMode = false;
        this.editMode = false;
        this.deleteMode = false;
    }


    addAttribute() {
        this.editAttributeMode = false;
        this.createAttributeMode = true;
        this.currentAttribute = new AttributeData();
    }



    editAttribute(attribute: AttributeData, index) {
        this.editAttributeMode = true;
        this.createAttributeMode = false;
        let copy = Object.assign({}, attribute);
        this.currentAttribute = copy;
        this.editAttributeIndex = index;
    }




    getClassMasterData() {
        this.classMasterDataService.getClasses()
            .subscribe((res: any) => {
                if (res.status == 200) {
                    this.classMasterdata = res.body;
                }
            })
    }

    createClassMasterData() {
        this.createMode = true;
        this.classCreateRequest = new CLassCEMasterData();
        this.classCreateRequest.attributes = [];
        console.log(this.currentAttribute);
    }
    SubmitCLass() {
        console.log(this.classCreateRequest);
        if (this.validateCreateRequest()) {
            this.classMasterDataService.createClass(this.classCreateRequest)
                .subscribe((res: any) => {
                    if (res.status == 200) {
                        this.getClassMasterData();
                        this.createMode = false;
                        this.toastr.success('CLass Created Successfully', 'Success!', { timeOut: 3000 });
                    } else {
                        this.toastr.error('Some Internel Error Occured', 'Failure!', { timeOut: 3000 });
                    }

                })
        }

    }

    editClass(id) {
        this.classMasterDataService.getClass(id)
            .subscribe((res: any) => {
                if (res.status == 200) {
                    this.editClassdata = new CLassCEMasterData();
                    this.editClassdata = res.body;
                    this.editMode = true;
                    this.classCreateRequest = new CLassCEMasterData();
                    this.classCreateRequest = this.editClassdata;
                }
            })

    }

    updateClass() {

        if (this.validateCreateRequest()) {
            this.classMasterDataService.updateClass(this.classCreateRequest)
                .subscribe((res: any) => {
                    if (res.status == 200) {
                        this.getClassMasterData();
                        this.editMode = false;
                        this.toastr.success('CLass updated Successfully', 'Success!', { timeOut: 3000 });
                    } else {
                        this.toastr.error('Some Internel Error Occured', 'Failure!', { timeOut: 3000 });
                    }

                })
        }

    }

    validateCreateRequest(): boolean {
        console.log(this.classCreateRequest);
        if (this.classCreateRequest.className == "" || this.classCreateRequest.className == null) {
            return false;
        } else if (this.classCreateRequest.instanceLimiter == "" || this.classCreateRequest.instanceLimiter == "NONE") {
            return false;
        } else if (this.classCreateRequest.status == 1) {
            return false;
        } else if (this.classCreateRequest.attributes.length <= 0 || this.classCreateRequest.attributes.length == null) {
            return false;
        }
        return true;
    }

    createAttribute() {
        console.log(this.currentAttribute);
        if (this.currentAttribute.attributeName == "" || this.currentAttribute.attributeName == null) {
        }
        else {
            this.editAttributeMode = false;
            this.createAttributeMode = false;
            this.classCreateRequest.attributes.push(this.currentAttribute);
            this.currentAttribute = new AttributeData();
            this.toastr.error("Attribute Name can't be Empty", 'Error!', { timeOut: 3000 });
        }
    }


    updateAttribute() {
        this.classCreateRequest.attributes.splice(this.editAttributeIndex, 1, this.currentAttribute);
        this.editAttributeIndex = -1;
        this.editAttributeMode = false;
        this.currentAttribute = new AttributeData();
    }

    cancelAttribute() {
        this.createAttributeMode = false;
        this.editAttributeMode = false;
        this.currentAttribute = new AttributeData();
    }

    dropDownChange(event, item, param) {
        item[param] = event.target.value;
    }
}
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DataTableComponent } from "../alt-common/alt-datatable.component";
import { RequestDispatcher } from "../alt-common/request.dispatcher";
import { KernelInstanceProcessData } from "../alt-pojo/kernel-instance-process-data.model";
import { KernelSingleAttributeValue } from "../alt-pojo/kernel-single-attribute-value.model";
import { AltCommonService } from "../alt-common/alt-common.service";
import { InstanceDataModel } from "../alt-pojo/MasterData/InstanceData.model";
import { InstancedataService } from "../services/masterdata/instancedata.service";
import { ClassmasterdataService } from "../services/masterdata/classmasterdata.service";
import { CLassMasterData } from "../alt-pojo/MasterData/class-master-data.model";
import { CLassCEMasterData } from "../alt-pojo/MasterData/classCE-master-data.model";
import { ToastrService } from 'ngx-toastr';
import { InstanceCEDataModel } from "../alt-pojo/MasterData/InstanceCEData.model";
import { AttributeInstance } from "../alt-pojo/MasterData/AttributeInstance.model";
import { Subscription } from "rxjs";

@Component({
    templateUrl: 'instance.component.html'
})
export class InstanceComponent implements OnInit {

    routeSubscriber: Subscription;
    @ViewChild(DataTableComponent) dataTable: DataTableComponent;
    docId: string;
    meta: any;
    selectedType: CLassMasterData = null;
    instance: KernelInstanceProcessData;
    createMode: boolean = false;
    editMode: boolean = false;
    deleteMode: boolean = false;
    instances: InstanceDataModel[] = [];
    classesList: CLassMasterData[] = [];
    attributesList: CLassCEMasterData;
    instanceCreateRequest: InstanceCEDataModel;


    constructor(
        private toastr: ToastrService,
        private instanceDataService: InstancedataService,
        private classService: ClassmasterdataService,
        private activatedRoute: ActivatedRoute,
        private requestDispatcher: RequestDispatcher,
        private commonService: AltCommonService,
        private router: Router) {
    }

    ngOnInit() {
        this.routeSubscriber = this.activatedRoute.params.subscribe((params: Params) => {
            this.docId = this.commonService.replaceUnderScoreByHash(params['id']);
            this.getInstances(this.docId);
        });
    }

    createInstance() {
        this.createMode = true;
        this.getClassesList();
        this.instanceCreateRequest = new InstanceCEDataModel();

    }

    getClassesList() {
        this.classService.getClasses()
            .subscribe((res: any) => {
                if (res.status == 200) {
                    this.classesList = res.body;
                    this.toastr.success('Class List Fetched Succesfully', 'Success!', { timeOut: 3000 });
                }
            })
    }

    getInstances(classId) {
        this.instanceDataService.getInstances(classId)
            .subscribe((res: any) => {
                if (res.status == 200) {
                    this.instances = res.body;
                    this.toastr.success('Class List Fetched Succesfully', 'Success!', { timeOut: 3000 });
                }
            })
    }


    getAttributesForClass(id) {
        this.classService.getAttributesForClass(id.classId)
            .subscribe((res: any) => {
                if (res.status == 200) {
                    this.attributesList = res.body;
                    this.instanceCreateRequest.classId = id.classId;
                    this.instanceCreateRequest.className = id.className;
                    this.attributesList.attributes.forEach(attribute => {
                        this.instanceCreateRequest["attributes"][attribute.attributeName] = new KernelSingleAttributeValue();
                    }
                    );
                }
            })
    }

    typeChanged(id: CLassMasterData) {
        if (id != null) {
            this.getAttributesForClass(id);
        }
    }


    editItem(item: InstanceDataModel) {
        this.editMode = true;
        this.attributesList = new CLassCEMasterData();
        this.instanceDataService.getInstance(this.docId, item.instanceId)
            .subscribe((res: any) => {
                if (res.status == 200) {
                    this.instanceCreateRequest = res.body;
                    var keys = Object.keys(this.instanceCreateRequest.attributes);
                    let instacnce: any;
                    for (let key of keys) {
                        instacnce = new AttributeInstance();
                        instacnce.attributeName = key;
                        this.attributesList.attributes.push(instacnce);
                    }

                }
            })
    }

    submit() {
        this.instanceDataService.createInstance(this.instanceCreateRequest, this.instanceCreateRequest.classId)
            .subscribe((res: any) => {
                if (res.status == 200) {
                    this.getInstances(this.docId);
                    this.createMode = false;
                    this.instanceCreateRequest = null;
                    this.attributesList = null;
                    this.toastr.error('Some Internel Error Occured', 'Failure!', { timeOut: 3000 });
                } else {
                }
            });
    }
    updateInstance() {
        this.instanceDataService.updateInstance(this.instanceCreateRequest, this.docId)
            .subscribe((res: any) => {
                if (res.status == 200) {
                    this.toastr.success('Instance Updated Successfully', 'Success!', { timeOut: 3000 });
                    this.getInstances(this.docId);
                    this.editMode = false;
                    this.instanceCreateRequest = null;
                    this.attributesList = null;

                } else {
                    this.toastr.error('Some Internel Error Occured', 'Failure!', { timeOut: 3000 });
                }
            });
    }


    cancel() {
        this.instanceCreateRequest = null;
        this.attributesList = null;
        this.editMode = false;
        this.createMode = false;
    }

}
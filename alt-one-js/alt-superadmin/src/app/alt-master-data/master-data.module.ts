import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { routings } from "./master-data.routing";
import { AltCommonModule } from "../alt-common/alt-common.module";

import { PackageComponent } from "./package.component";
import { ClassComponent } from "./class.component";
import { InstanceComponent } from "./instance.component";
import { ClassmasterdataService } from "../services/masterdata/classmasterdata.service";
import { InstancedataService } from "../services/masterdata/instancedata.service";

@NgModule({
    imports: [
        AltCommonModule,
        CommonModule,
        FormsModule,
        HttpClientModule,
        routings
    ],
    declarations: [
        PackageComponent,
        ClassComponent,
        InstanceComponent

    ],
    providers: [
        ClassmasterdataService,
        InstancedataService
    ],
    exports: []
})
export class AltMasterDataModule { }
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PackageComponent } from "./package.component";
import { ClassComponent } from "./class.component";
import { InstanceComponent } from "./instance.component";

const routes: Routes = [
    { path: 'package', component: PackageComponent },
    { path: 'class', component: ClassComponent,pathMatch:'full' },
    { path: 'instance', component: InstanceComponent,pathMatch:'full' },
    { path: 'class/:id', component: ClassComponent },
    { path: 'instance/:id', component: InstanceComponent }
]

export const routings: ModuleWithProviders = RouterModule.forChild(routes);
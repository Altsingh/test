import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { DataTableComponent } from "../alt-common/alt-datatable.component";
import { RequestDispatcher } from "../alt-common/request.dispatcher";
import { KernelInstanceProcessData } from "../alt-pojo/kernel-instance-process-data.model";
import { KernelSingleAttributeValue } from "../alt-pojo/kernel-single-attribute-value.model";
import { AltCommonService } from "../alt-common/alt-common.service";

@Component({
    templateUrl: 'package.component.html'
})
export class PackageComponent implements OnInit, OnDestroy {

    @ViewChild(DataTableComponent) dataTable: DataTableComponent;
    docId;
    meta: any;
    packageInstance: KernelInstanceProcessData;
    createMode: boolean = false;
    editMode: boolean = false;
    deleteMode: boolean = false;

    constructor(
        private commonService: AltCommonService,
        private requestDispatcher: RequestDispatcher,
        private router: Router) { }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    createItem() {
        this.requestDispatcher.getPaginationMeta('PACKAGEMETA', this.docId)
            .subscribe(res => {
                this.meta = res['respData'];
                // create empty package instance
                this.packageInstance = new KernelInstanceProcessData;
                this.packageInstance["attributes"] = {};
                this.meta["attributes"].forEach(attribute => {
                    this.packageInstance["attributes"][attribute.attributeName] = new KernelSingleAttributeValue;
                });
                this.createMode = true;
            })
    }

    deleteItem(item: KernelInstanceProcessData) {
        this.packageInstance = item;
        this.deleteMode = true;
    }

    editItem(item: KernelInstanceProcessData) {
        this.requestDispatcher.getPaginationMeta('PACKAGEMETA', this.docId)
            .subscribe(res => {
                this.meta = res['respData'];
                this.packageInstance = item;
                this.editMode = true;
            })
    }

    // childListRouter(params) {
    //     this.router.navigateByUrl("/master-data/class/" + this.commonService.replaceHashStringByUnderScore(params['id']))
    // }

    submit() {
        if (this.createMode) {
            this.requestDispatcher.createEditInstance("CREATE", "", this.packageInstance, "PACKAGE")
                .subscribe(res => {
                    console.log(res);
                    this.createMode = false;
                    this.editMode = false;
                    this.deleteMode = false;
                    this.dataTable.refresh();
                })
        } else if (this.editMode) {
            this.requestDispatcher.createEditInstance("UPDATE", "", this.packageInstance, "PACKAGE")
                .subscribe(res => {
                    console.log(res);
                    this.createMode = false;
                    this.editMode = false;
                    this.deleteMode = false;
                    this.dataTable.refresh();
                })
        } else if (this.deleteMode) {
            this.requestDispatcher.createEditInstance("SOFT_DELETE", "", this.packageInstance, "PACKAGE")
                .subscribe(res => {
                    console.log(res);
                    this.createMode = false;
                    this.editMode = false;
                    this.deleteMode = false;
                    this.dataTable.refresh();
                })
        }
    }

    cancel() {
        this.createMode = false;
        this.editMode = false;
        this.deleteMode = false;
    }
}
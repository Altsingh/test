import { AttributeValidator } from "../attribute-validator.model";
import { ClassMeta } from "../class-meta.model";
import { EnumDocType } from "../enums/enum-doc-type";
import { EnumDataType } from "../enums/enum-data-type";


export class AttributeData {
	attributeName: string;
	dataType: EnumDataType;
	referenceClassId: string;
	status: EnumDocType;
	placeholderType: boolean;
	defaultValue: string;
	validator: AttributeValidator;
	unique: boolean;
	mandatory: boolean;
	searchable: boolean;
	indexable: boolean;
	displayUnit: boolean;
	displaySequence: number;
	refClassMeta: ClassMeta;

	constructor() {
		this.attributeName = "";
		this.dataType = EnumDataType.STRING;
		this.referenceClassId = "";
		this.status = EnumDocType.INACTIVE;
		this.placeholderType = false;
		this.defaultValue = "";
		this.validator = null;
		this.unique = false;
		this.mandatory = false;
		this.searchable = false;
		this.indexable = false;
		this.displayUnit = false;
		this.displaySequence = 0;
		this.refClassMeta = null;
	}
}
import { EnumDocType } from "../enums/enum-doc-type";
import { KernelSingleAttributeValueMeta } from "../kernel-instance-process-data.model";

export class InstanceCEDataModel{
    instanceId:String;
	org:string;
    className:string;
    classId:string;
    status:EnumDocType;
    attributes:KernelSingleAttributeValueMeta;
    constructor(){
        this.instanceId=null;
        this.org=null;
        this.className=null;
        this.classId=null;
        this.status=EnumDocType.ACTIVE;
        this.attributes={};
    }
	
}
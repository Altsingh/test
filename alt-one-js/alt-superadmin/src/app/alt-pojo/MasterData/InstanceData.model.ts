import { EnumDocType } from "../enums/enum-doc-type";

export class InstanceDataModel{
    instanceId:number;
	org:string;
    className:string;
    status:EnumDocType;
    createdDate:string; 
    createdBy:string;
	constructor(){
        this.instanceId=null;
        this.org=null;
        this.className=null;
        this.status=null;
        this.createdDate=null;
        this.createdBy=null;
    }
	

}
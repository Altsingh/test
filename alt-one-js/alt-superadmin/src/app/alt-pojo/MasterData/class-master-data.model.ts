export class CLassMasterData{
     className:string;
     classId:string;
     createdBy:String;
     createdDate:String;
     packageCodes:String;
     status:String;

    constructor(){
        this.className="";
        this.classId="";
        this.createdBy="";
        this.createdDate="";
        this.packageCodes="";
        this.status="";
    }
}
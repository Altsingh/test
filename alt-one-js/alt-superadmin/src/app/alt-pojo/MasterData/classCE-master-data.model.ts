import { AttributeMeta } from "../attribute-meta.model";
import { EnumDocType } from "../enums/enum-doc-type";
import { AttributeData } from "./AttributeData.model";
import { AttributeInstance } from "./AttributeInstance.model";

export class CLassCEMasterData{
    className:String;
    attributes:AttributeInstance[];
    locationSpecific:boolean;
    customAttributeAllowed:boolean;
    parameterType:boolean;
    systemType:boolean;
    status:EnumDocType;
     instanceLimiter:String;
    packageCodes:String[];

   constructor(){
       this.className="";
       this.attributes=[];
       this.locationSpecific=false;
       this.customAttributeAllowed=false;
        this.parameterType=false;
        this.systemType=false;
        this.status=EnumDocType.INACTIVE;
        this.packageCodes=[];
        this,this.instanceLimiter="";
   }
}
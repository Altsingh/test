import { InstanceData } from "./instance-data.model";
import { ClassMeta } from "./class-meta.model";

export interface AltReadResponse{
    meta:ClassMetaMap;
    instance: InstanceData[];
}
export interface ClassMetaMap{
    [index:string]: ClassMeta,
}
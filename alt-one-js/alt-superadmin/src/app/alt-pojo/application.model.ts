export class ApplicationModel{
    appName: string;
    appCode: string;
    createdBy: string;
    createdDate: string;
    status: string;
    appId: string;
    modules : any[];

    constructor(){
        this.appName = null;
        this.appCode = null;
        this.createdBy = null;
        this.createdDate = null;
        this.status = null;
        this.appId = null;
        this.modules = [];
    }
}
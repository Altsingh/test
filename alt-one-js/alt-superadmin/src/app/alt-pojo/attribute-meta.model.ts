import { ClassMeta } from "./class-meta.model";
import { AttributeValidator } from "./attribute-validator.model";

export class AttributeMeta {
	attributeCode: string;
	attributeName: string;
	dataType: string;
	referenceClassCode: string;
	status: string;
	placeholderType: boolean;
	defaultValue: string;
	validator: AttributeValidator;
	unique: boolean;
	mandatory: boolean;
	searchable: boolean;
	indexable: boolean;
	displayUnit: boolean;
	displaySequence: number;
	refClassMeta: ClassMeta;

	constructor() {
		this.attributeCode = "";
		this.attributeName = "";
		this.dataType = "STRING";
		this.referenceClassCode = "";
		this.status = "ACTIVE";
		this.placeholderType = false;
		this.defaultValue = "";
		this.validator = null;
		this.unique = false;
		this.mandatory = false;
		this.searchable = false;
		this.indexable = false;
		this.displayUnit = false;
		this.displaySequence = 0;
		this.refClassMeta = null;
	}
}
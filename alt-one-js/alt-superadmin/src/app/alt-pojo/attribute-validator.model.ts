export interface AttributeValidator{
    regex:string;
    minvalue:number;
    maxvalue:number;
    precision:number;
}
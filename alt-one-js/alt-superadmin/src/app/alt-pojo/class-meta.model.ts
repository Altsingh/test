import { AttributeMeta } from "./attribute-meta.model";

export class ClassMeta {
    name: string;
    type: string;
    status: string;
    locationSpecific: boolean;
    systemType: boolean;
    parameterType: boolean;
    classCode: string;
    packageCodes: string[];
    customAttributeAllowed: boolean;
    attributes: AttributeMeta[];
    instanceLimiter: string;
    instancePrefix: string;
    referredBy: referredByMap;
    updationUuidList: string[];
    cas: string;
    id: string;
    createdDate: number;
    modifiedDate: number;
    createdBy: string;
    modifiedBy: string;
    readLock: boolean;

    constructor(){
        this.name="";
        this.type="";
        this.status="";
        this.locationSpecific=false;
        this.systemType=false;
        this.parameterType=false;
        this.classCode="";
        this.packageCodes=null;
        this.customAttributeAllowed=false;
        this.attributes=null;
        this.instanceLimiter="";
        this.instancePrefix="";
        this.referredBy=null;
        this.updationUuidList=null;
        this.cas="";
        this.id="";
        this.createdDate=null;
        this.modifiedDate=null;
        this.createdBy="";
        this.modifiedBy="";
        this.readLock=null;
    }
}

export class referredByMap {
    [index: string]: any[];
}
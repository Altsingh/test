import { SingleAttributeValue } from "./single-attribute-value.model";

export class InstanceData{
    docIdentifier: string;
    classCode: string;
    cas: string;
    type: string;
    attributes:SingleAttributeValueMeta;
    status: number;
    createdDate: number;
    modifiedDate: number;
    createdBy: number;
    modifiedBy: number;

    constructor(){
        this.docIdentifier = null;
        this.classCode = null;
        this.cas = null;
        this.type = null;
        this.status = null;
        this.createdDate = null;
        this.modifiedDate = null;
        this.modifiedBy = null;
        this.attributes = null;
    }
}

export class SingleAttributeValueMeta{
    [index: string]:SingleAttributeValue;
}
import { AttributeMeta } from "./attribute-meta.model";

export class KernelClassProcessData {
    name: string;
    classCode: string;
    status: string;
    locationSpecific: boolean;
    customAttributeAllowed: boolean;
    packageCodes: string[];
    instanceLimiter: string;
    instancePrefix: string;
    systemType: boolean;
    attributes: AttributeMetaMap;
    parameterType: boolean;
    cas: string;
    readLock: boolean;

    constructor() {
        this.name = "";
        this.classCode = "";
        this.status = "";
        this.locationSpecific = false;
        this.customAttributeAllowed = false;
        this.packageCodes = ["abcd"];
        this.instanceLimiter = "NONE";
        this.instancePrefix = "";
        this.systemType = false;
        this.attributes = null;
        this.parameterType = false;
        this.cas = "";
        this.readLock = false;
    }
}

export class AttributeMetaMap {
    [index: string]: AttributeMeta[];
}
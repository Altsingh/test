import { KernelSingleAttributeValue } from "./kernel-single-attribute-value.model";

export class KernelInstanceProcessData {
    docIdentifier: string;
    classCode: string;
    cas: string;
    type: string;
    attributes: KernelSingleAttributeValueMeta;
    status: number;
    createdDate: number;
    modifiedDate: number;
    createdBy: number;
    modifiedBy: number;

    constructor() {
        this.docIdentifier = null;
        this.classCode = null;
        this.cas = null;
        this.type = null;
        this.status = null;
        this.createdDate = null;
        this.modifiedDate = null;
        this.modifiedBy = null;
        this.attributes = null;
    }
}

export class KernelSingleAttributeValueMeta {
    [index: string]: KernelSingleAttributeValue;
}
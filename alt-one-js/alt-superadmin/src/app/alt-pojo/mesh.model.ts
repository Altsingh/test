export class MeshModel {
    appName : string;
    appIconImage : string;
    appCode : string;
    webEnabled : boolean;
    webURL : string;
    androidEnabled : boolean;
    androidURL : string;
    androidPackage : string;
    iosEnabled : boolean;
    iosURL : string;
    iosPackage : string;
    appType : string;
 
    constructor(){
        this.appName = "";
        this.appIconImage = "";
        this.appCode = "";
        this.webEnabled = false;
        this.webURL = "";
        this.androidEnabled = false;
        this.androidURL = "";
        this.androidPackage = "";
        this.iosEnabled = false;
        this.iosURL = "";
        this.iosPackage = "";
        this.appType = "";
    }

}
import { InstanceData } from "./instance-data.model";
export class SingleAttributeValue{
    reference: boolean;
    value: string;
    referenceValue: InstanceData[]

    constructor(){
        this.reference=null;
        this.value=null;
        this.referenceValue=null;
    }
}
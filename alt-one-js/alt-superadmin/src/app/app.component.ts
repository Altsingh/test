import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, ActivatedRoute } from '@angular/router';
import { distinctUntilChanged } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

declare var ga: Function;

@Component({
  selector: 'alt-application',
  templateUrl: 'app.component.html'
})
export class AppComponent implements OnInit {
  formsCreatePage: boolean = false;
  logOutMenu: boolean = false;
  switchApps: boolean = false;
  overlay: boolean = false;
  // meshModels: MeshModel[];
  userId: string;
  organizationId: string;
  tenantId: string;
  meshFlag: boolean;
  responsiveClass: string = 'header-container';
  mobileResponsiveFlag = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private _router: Router
    //private applicationDataService: DataService
  ) {

    _router.events.pipe(distinctUntilChanged((previous: any, current: any) => {
      // Subscribe to any `NavigationEnd` events where the url has changed
      if (current instanceof NavigationEnd) {
        return previous.url === current.url;
      }
      return true;
    })).subscribe((x: any) => {
      ga('send', 'pageview', x.url.split('?', 1)[0].split('/', 3).join('/'))
    });
  }

  callHome() {
    this._router.navigateByUrl('home');
  }

  loadLogo() {
    return environment.builderLogo;
  }

  ngOnInit() {
  }

  meshDropHide() {
    this.switchApps = false;
    this.overlay = false;
  }

  showSwicher() {
    this.overlay = !this.overlay;
    this.switchApps = !this.switchApps;
  }


  toggleMenu() {
    if (this.logOutMenu) {
      this.logOutMenu = false;
    } else {
      this.logOutMenu = true;
    }
  }

  logoutUser() {
    localStorage.clear();
    this._router.navigateByUrl('logout');
  }

  checkIfLogoutPage() {
    var loggedOut = window.location.href
    if (loggedOut.includes("logout")) {
      return false;
    } else {
      return true;
    }
  }

  callHomeBuilder() {
    this._router.navigateByUrl('homebuilder');
  }

  changeResponsiveClass() {
    if (this.mobileResponsiveFlag) {
      this.responsiveClass = 'header-container menu-open';
      this.mobileResponsiveFlag = false;
    } else {
      this.responsiveClass = 'header-container';
      this.mobileResponsiveFlag = true;
    }
  }
  getResponsiveClass(): string {
    return this.responsiveClass;
  }

  removeDoubleQuotes(stringData: any) {
    if (stringData == null || stringData == 'undefined' || stringData == '') {
      return undefined
    }
    return stringData.replace(/['"]+/g, '')
  }

  profilePhotoUrl(){
    if(localStorage.getItem("adminPhoto")==null || localStorage.getItem("adminPhoto")=='undefined')
      return '../assets/images/anu.jpg';
    else
      return this.removeDoubleQuotes(localStorage.getItem("adminPhoto"));
  }
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_component_1 = require("./app.component");
var dropdown_component_1 = require("./dropdown.component");
var dragable_directive_1 = require("./dragable.directive");
var dragtarget_directive_1 = require("./dragtarget.directive");
var drag_service_1 = require("./drag.service");
var button_component_1 = require("./button/button.component");
var date_component_1 = require("./date/date.component");
var file_component_1 = require("./file/file.component");
var header_component_1 = require("./header/header.component");
var para_component_1 = require("./para/para.component");
var select_component_1 = require("./select/select.component");
var textarea_component_1 = require("./textarea/textarea.component");
var textbox_component_1 = require("./textbox/textbox.component");
var checkbox_component_1 = require("./checkbox/checkbox.component");
var radio_component_1 = require("./radio/radio.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule],
        declarations: [
            button_component_1.ButtonComponent,
            date_component_1.DateComponent,
            file_component_1.FileComponent,
            header_component_1.HeaderComponent,
            para_component_1.ParaComponent,
            select_component_1.SelectComponent,
            textarea_component_1.TextareaComponent,
            textbox_component_1.TextboxComponent,
            checkbox_component_1.CheckboxComponent,
            radio_component_1.RadioComponent,
            dropdown_component_1.DropDownComponent,
            dragable_directive_1.DraggableDirective,
            dragtarget_directive_1.DragTargetDirective,
            app_component_1.AppComponent
        ],
        bootstrap: [app_component_1.AppComponent],
        providers: [drag_service_1.DragService]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map
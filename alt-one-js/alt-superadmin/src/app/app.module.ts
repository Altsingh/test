import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FormModule } from "./form/form.module";
import { routings } from "./app.routing";
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { SecureloginModule } from './securelogin/securelogin.module';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { ApplicationHttpService } from './alt-application/application-http.service';
import { LogoutComponent } from './logout/logout.component';
import { MatFormFieldModule, MatOptionModule, MatAutocompleteModule, MatInputModule } from '../../node_modules/@angular/material';
import { MaterialComponent } from './material/material.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routings,
    FormModule,
    HttpClientModule,
    SecureloginModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatOptionModule,
    MatFormFieldModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    AppComponent, HomeComponent, LogoutComponent, MaterialComponent
  ],
  bootstrap: [AppComponent],
  providers: [ApplicationHttpService]
})
export class AppModule { }

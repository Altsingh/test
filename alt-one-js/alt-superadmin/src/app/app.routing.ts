import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LogoutComponent } from "./logout/logout.component";
import { MaterialComponent } from './material/material.component';

const routes: Routes = [
    { path: 'material',component: MaterialComponent},
    { path: 'home',component: HomeComponent},
    { path: 'logout', component: LogoutComponent},
    { path: 'application', loadChildren: './alt-application/application.module#AltAppModule'},
    { path: 'master-data', loadChildren: './alt-master-data/master-data.module#AltMasterDataModule'},
    // { path: 'form', loadChildren: 'app/alt-form/alt-form.module#AltFormModule'},
    { path: 'form', loadChildren: './form/form.module#FormModule'},
    {path: 'not-found',loadChildren: './not-found/not-found.module#NotFoundModule'},
    //{path: '**',redirectTo: 'not-found'},
    { path: 'securelogin', loadChildren: './securelogin/securelogin.module#SecureloginModule'}
    // { path: '', redirectTo: '/application/app', pathMatch: 'full'}
]

export const routings: ModuleWithProviders = RouterModule.forRoot(routes,{ useHash: true });

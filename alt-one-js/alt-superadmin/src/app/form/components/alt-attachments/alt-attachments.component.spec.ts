import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltAttachmentsComponent } from './alt-attachments.component';

describe('AltAttachmentsComponent', () => {
  let component: AltAttachmentsComponent;
  let fixture: ComponentFixture<AltAttachmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltAttachmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltAttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

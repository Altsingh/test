import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltCheckboxComponent } from './alt-checkbox.component';

describe('AltCheckboxComponent', () => {
  let component: AltCheckboxComponent;
  let fixture: ComponentFixture<AltCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

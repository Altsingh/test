import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltExcelComponent } from './alt-excel.component';

describe('AltExcelComponent', () => {
  let component: AltExcelComponent;
  let fixture: ComponentFixture<AltExcelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltExcelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltExcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltLinkComponent } from './alt-link.component';

describe('AltLinkComponent', () => {
  let component: AltLinkComponent;
  let fixture: ComponentFixture<AltLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

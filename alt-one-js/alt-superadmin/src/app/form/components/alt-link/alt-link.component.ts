import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'alt-link',
  templateUrl: './alt-link.component.html',
  styleUrls: ['./alt-link.component.css']
})
export class AltLinkComponent implements OnInit {

  @Input() order: number;
  @Output() deleteItem = new EventEmitter<number>();
  @Output() copyItem = new EventEmitter<number>();
  @Input() element: any;
  editing: boolean = false;
  @Input() editMode: boolean;
  @Output() settingsOpened = new EventEmitter();

  ngOnInit() {
  }

  delete() {
    this.deleteItem.emit(this.order);
  }

  copy() {
    this.copyItem.emit(this.order);
  }

  addClass(type: string) {
    this.element.cssClass[type].push("");
  }

  removeClass(type, ind) {
    this.element.cssClass[type].splice(ind, 1);
  }

  openSettings(component) {
    this.settingsOpened.emit(component);
  }
}

import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { AltFormControlModelService } from "../../form-control.model.service";

@Component({
  selector: 'alt-blank',
  templateUrl: './blank.component.html'
})
export class BlankComponent implements OnInit {
  @Input() order: number;
  @Output() deleteItem=new EventEmitter<number>();
  @Output() copyItem=new EventEmitter<number>();
  @Input() element: any;
  editing:boolean=false;
  @Input() editMode: boolean;

  constructor(private formControlModelService: AltFormControlModelService) { }

  ngOnInit() {
  }

  delete(){
    this.deleteItem.emit(this.order);
  }

  copy(){
    this.copyItem.emit(this.order);
  }

  addClass(type:string){
    this.element.cssClass[type].push("");
  }

  removeClass(type,ind){
    this.element.cssClass[type].splice(ind,1);
  }

}

import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AltFormControlModelService } from "../../form-control.model.service";
import { Rule } from '../../rule/rule.model';

@Component({
  selector: 'alt-button',
  templateUrl: './button.component.html'
})
export class ButtonComponent implements OnInit {
  @Input() order: number;
  @Input() selected: boolean;
  @Output() deleteItem = new EventEmitter<number>();
  @Output() copyItem = new EventEmitter<number>();
  @Input() element: any;
  @Input() editMode: boolean;
  rules: number[];
  ruleObjects: Rule[];
  commIdList: number[];
  events: string[];
  /**
   * if this flag is true, form submission to backend
   * won't happen.
   * @author vaibhav.kashayp
  */
  validationsOnly: boolean;
  @Output() settingsOpened = new EventEmitter();

  ngOnInit() {
  }

  delete() {
    this.deleteItem.emit(this.order);
  }

  copy() {
    this.copyItem.emit(this.order);
  }

  addClass(type: string) {
    this.element.cssClass[type].push("");
  }

  removeClass(type, ind) {
    this.element.cssClass[type].splice(ind, 1);
  }

  openSettings(component) {
    this.settingsOpened.emit(component);
  }

}

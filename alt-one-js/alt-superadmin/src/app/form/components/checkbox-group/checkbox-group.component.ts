import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { AltFormControlModelService } from "../../form-control.model.service";
import { AltCheckboxSingle } from "../../models/alt-checkbox-single";
import { AltUtilService } from "../../../alt-common/alt-util.servive";

@Component({
  selector: 'alt-checkbox-group',
  templateUrl: './checkbox-group.component.html',
})
export class CheckboxGroupComponent {
  @Input() order: number;
  @Input() valid: boolean;
  @Output() deleteItem = new EventEmitter<number>();
  @Output() copyItem = new EventEmitter<number>();
  @Input() element: any;
  error: boolean = false;
  editing:boolean=false;
  constructor(private formControlModelService: AltFormControlModelService,
    private utilService: AltUtilService) { }

  ngOnInit() {
  }

  addOption() {
    var counter = this.element.options.length + 1;
    var checkBox = new AltCheckboxSingle({
      "namespace": null,
      "dbClass": null,
      "dbAttr": null,
      "id": this.element.id + counter,
      "label": "option" + counter,
      "name": this.element.id + counter,
      "controlType": "CHECKBOX_SINGLE",
      "value": false,
    });
    this.element['options'].push(checkBox);
  }

  checkDuplicacy() {
    this.error = this.utilService.hasDuplicates(this.element.options, "id")
      || this.utilService.hasDuplicates(this.element.options, "name")
      || this.utilService.hasDuplicates(this.element.options, "label")
      || this.utilService.hasDuplicates(this.element.options, "value");

    if (this.error) {
      this.valid = false;
    }
  }

  remove(ind) {
    this.element['options'].splice(ind, 1);
  }

  delete() {
    this.deleteItem.emit(this.order);
  }

  copy() {
    this.copyItem.emit(this.order);
  }

  addClass(type: string) {
    this.element.cssClass[type].push("");
  }

  removeClass(type, ind) {
    this.element.cssClass[type].splice(ind, 1);
  }

}

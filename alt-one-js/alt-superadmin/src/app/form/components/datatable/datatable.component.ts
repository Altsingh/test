import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'alt-datatable',
  templateUrl: './datatable.component.html'
})
export class DataTableComponent implements OnInit {
  @Input() order: number;
  @Output() deleteItem = new EventEmitter<number>();
  @Output() copyItem = new EventEmitter<number>();
  @Input() element: any;
  @Input() editMode: boolean;

  ngOnInit() {
  }

  delete() {
    this.deleteItem.emit(this.order);
  }

  copy() {
    this.copyItem.emit(this.order);
  }

  addClass(type: string) {
    this.element.cssClass[type].push("");
  }

  removeClass(type, ind) {
    this.element.cssClass[type].splice(ind, 1);
  }

}

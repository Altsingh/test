import { Component, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'alt-file',
  templateUrl: './file.component.html',
})
export class FileComponent  {
  @Input() order: number;
  @Output() deleteItem=new EventEmitter<number>();
  editing:boolean=false;
    constructor(){

    }

  delete(){
    this.deleteItem.emit(this.order);
  }
}

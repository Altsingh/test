import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { AltFormControlModelService } from "../../form-control.model.service";

@Component({
  selector: 'alt-multi-select',
  templateUrl: './multi-select.component.html',
})
export class MultiSelectComponent implements OnInit { 
  @Input() order: number;
  @Output() deleteItem=new EventEmitter<number>();
  @Output() copyItem=new EventEmitter<number>();
  @Input() element: any;
  selected: string[];
  
  constructor(private formControlModelService: AltFormControlModelService){ } 
  
  ngOnInit(){
    this.selected=[];
    this.element.options.forEach(element => {
      if(element.selected==true){
        this.selected.push(element.value);
      }
    });
  }

  addOption(){
    this.element['options'].push({
      "disabled": false,
      "label": "new label",
      "value": "new value",
      "selected": false
    })
  }

  updateDropdownValues(){
    this.element.options.forEach(element => {
      if(this.selected.indexOf(element.value)>-1){
        element.selected=true;
      } else {
        element.selected=false;
      }
    });
  }

  remove(ind){
    this.element['options'].splice(ind,1);
  }

  delete(){
    this.deleteItem.emit(this.order);
  }

  copy(){
    this.copyItem.emit(this.order);
  }

  addClass(type:string){
    this.element.cssClass[type].push("");
  }

  removeClass(type,ind){
    this.element.cssClass[type].splice(ind,1);
  }
  
}

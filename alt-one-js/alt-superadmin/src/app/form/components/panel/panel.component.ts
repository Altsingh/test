import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { DefaultDataService } from "../../default-data.service";

@Component({
    selector: 'alt-panel',
    templateUrl: './panel.component.html',
})
export class PanelComponent implements OnInit {
    @Input() selectedComponent;
    @Output() selectedComponentEvent: EventEmitter<any> = new EventEmitter<any>();
    @Input() order: number;
    @Input() element: any;
    @Output() deleteItem = new EventEmitter<number>();
    @Output() copyItem = new EventEmitter<number>();
    @Input() editMode: boolean
    @Output() settingsOpened = new EventEmitter();
    copyCount: number = 1;

    constructor(
        private dataService: DefaultDataService
    ) { }

    ngOnInit() {
    }

    deleteThisItem(index: number) {
        if (this.element.componentList[index] == this.selectedComponent) {
            this.selectedComponent = null;
        }
        this.element.componentList.splice(index, 1);
    }

    copyThisItem(index: number) {
        var altCopiedComponent = JSON.parse(JSON.stringify(this.element.componentList[index]));
        altCopiedComponent.id = altCopiedComponent.id + this.copyCount;
        altCopiedComponent.name = altCopiedComponent.name + this.copyCount;
        altCopiedComponent.label = altCopiedComponent.label + this.copyCount;
        this.element.componentList.splice(index + 1, 0, JSON.parse(JSON.stringify(altCopiedComponent)));
        //this.submit(false);
        this.copyCount = this.copyCount + 1;
        //this.element.componentList.splice(index, 0, JSON.parse(JSON.stringify(this.element.componentList[index])));
    }

    delete() {
        this.deleteItem.emit(this.order);
    }

    copy() {
        this.copyItem.emit(this.order);
    }

    selectComponent(component) {
        this.selectedComponentEvent.emit(component);
    }

    transferDataSuccess($event: any) {
        var randomId = this.randomise();
        switch ($event.dragData.component) {
            case "ATTACHMENTS":
                this.element.componentList.push(this.dataService.getAttachmentsJSON("id" + randomId));
                break;
            case "BUTTON":
                this.element.componentList.push(this.dataService.getButtonJSON("id" + randomId));
                break;
            case "CHECKBOX":
                this.element.componentList.push(this.dataService.getCheckboxJSON("id" + randomId));
                break;
            case "EXCEL":
                this.element.componentList.push(this.dataService.getExcelJSON("id" + randomId));
                break;
            case "LINK":
                this.element.componentList.push(this.dataService.getLinkJSON("id" + randomId));
                break;
            case "TEXTFIELD":
                this.element.componentList.push(this.dataService.getTextfieldJSON("id" + randomId));
                break;
            case "DROPDOWN":
                this.element.componentList.push(this.dataService.getDropdownJSON("id" + randomId));
                break;
            case "MULTI_SELECT":
                this.element.componentList.push(this.dataService.getMultiSelectJSON("id" + randomId));
                break;
            case "PANEL":
                this.element.componentList.push(this.dataService.getPanelJSON("id" + randomId));
                break;
            case "TAB_PANEL":
                var tabbedPanel = this.dataService.getTabbedPanelJSON("id" + randomId);
                for (var i = 0; i < 3; i++) {
                    this.dataService.addPanelToTabbedPanelJSON(tabbedPanel, this.dataService.getPanelJSON("id" + this.randomise()));
                }
                this.element.componentList.push(tabbedPanel);
                break;
            case "DATE":
                this.element.componentList.push(this.dataService.getDateJSON("id" + randomId));
                break;
            case "LABEL":
                this.element.componentList.push(this.dataService.getLabelJSON("id" + randomId));
                break;
            case "BLANK":
                this.element.componentList.push(this.dataService.getBlankJSON("id" + randomId));
                break;
            case "IMAGE":
                this.element.componentList.push(this.dataService.getImageJSON("id" + randomId));
                break;
            case "TEXTAREA":
                this.element.componentList.push(this.dataService.getTextareaJSON("id" + randomId));
                break;
            case "TABLE":
                this.element.componentList.push(this.dataService.getTableJSON("id" + randomId));
                break;
        }
    }

    randomise() {
        return Math.floor(Math.random() * (1000000 - 1)) + 1;
    }

    openSettings(component) {
        this.settingsOpened.emit(component);
    }
}

import { Component, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'alt-para',
  templateUrl: './para.component.html',
})
export class ParaComponent  {
  @Input() order: number;
  @Output() deleteItem=new EventEmitter<number>();
  editing:boolean=false;
    constructor(){

    }

  delete(){
    this.deleteItem.emit(this.order);
  }
}

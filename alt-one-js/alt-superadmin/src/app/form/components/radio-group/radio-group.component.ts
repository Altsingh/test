import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { AltFormControlModelService } from "../../form-control.model.service";
import { AltCheckboxSingle } from "../../models/alt-checkbox-single";

@Component({
    selector: 'alt-radio-group',
    templateUrl: './radio-group.component.html',
})
export class RadioGroupComponent {
    @Input() order: number;
    @Output() deleteItem = new EventEmitter<number>();
    @Output() copyItem = new EventEmitter<number>();
    @Input() element: any;
    editing:boolean=false;

    constructor(private formControlModelService: AltFormControlModelService) { }

    ngOnInit() {
    }

    addOption() {
        var counter = this.element.options.length+1;
        var checkBox = new AltCheckboxSingle({
            "namespace": null,
            "dbClass": null,
            "dbAttr": null,
            "id": this.element.id + counter,
            "label": "option" + counter,
            "name": this.element.id + counter,
            "controlType": "CHECKBOX_SINGLE",
            "value": false,
        });
        this.element['options'].push(checkBox);
    }

    remove(ind) {
        this.element['options'].splice(ind, 1);
    }

    delete() {
        this.deleteItem.emit(this.order);
    }

    copy() {
        this.copyItem.emit(this.order);
    }

    addClass(type: string) {
        this.element.cssClass[type].push("");
    }

    removeClass(type, ind) {
        this.element.cssClass[type].splice(ind, 1);
    }

    updateRadioValues(child) {
        this.element.options.forEach(option => {
            if (option.id == child.id) {
                option.value = true;
            } else {
                option.value = false;
            }
        });
    }
}

import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { AltFormControlModelService } from "../../form-control.model.service";
import { Rule } from '../../rule/rule.model';

@Component({
  selector: 'alt-select',
  templateUrl: './select.component.html',
})
export class SelectComponent implements OnInit {
  @Input() order: number;
  @Output() deleteItem = new EventEmitter<number>();
  @Output() copyItem = new EventEmitter<number>();
  @Input() element: any;
  selected: string[];
  @Input() editMode: boolean;
  
  events: string[];
  ruleObjects:Rule[];
  rules:number[];

  constructor(private formControlModelService: AltFormControlModelService) { }

  ngOnInit() {
    this.selected = [];
    if(this.element.options){
      this.element.options.forEach(element => {
        if (element.selected == true) {
          this.selected.push(element.value);
        }
      });
    }
  }

  addOption() {
    this.element['options'].push({
      "disabled": false,
      "label": "new label",
      "value": "new value",
      "selected": false
    })
  }

  updateDropdownValues() {
    this.element.options.forEach(element => {
      if (this.selected.indexOf(element.value) > -1) {
        element.selected = true;
      } else {
        element.selected = false;
      }
    });
  }

  remove(ind) {
    this.element['options'].splice(ind, 1);
  }

  delete() {
    this.deleteItem.emit(this.order);
  }

  copy() {
    this.copyItem.emit(this.order);
  }

  addClass(type: string) {
    this.element.cssClass[type].push("");
  }

  removeClass(type, ind) {
    this.element.cssClass[type].splice(ind, 1);
  }
  
}

import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { FormHttpService } from "../../form-http.service";
import { DragService } from "../../drag.service";
import { DefaultDataService } from "../../default-data.service";
import { AltCommonService } from "../../../alt-common/alt-common.service";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { ClassDataService } from "../../services/class-data/class-data.service";
import { ClassAttributesService } from "../../services/class-data/class-attributes.service";
import { AltTabbedPanel } from '../../models/alt-tabbed-panel';

@Component({
    selector: 'alt-tabbed-panel',
    templateUrl: './tabbed-panel.component.html',
})
export class TabbedPanelComponent implements OnInit {
    @Input() selectedComponent;
    @Output() selectedComponentEvent: EventEmitter<any> = new EventEmitter<any>();
    @Input() order: number;
    @Input() element: AltTabbedPanel;
    @Output() deleteItem = new EventEmitter<number>();
    @Output() copyItem = new EventEmitter<number>();

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private formHttpService: FormHttpService,
        private dragService: DragService,
        private dataService: DefaultDataService,
        private commonService: AltCommonService,
        private classDataService: ClassDataService,
        private classAttributesService: ClassAttributesService) { }

    ngOnInit() {
        console.log(this.element.activePanelIndex);
    }

    addTab(){
        this.dataService.addPanelToTabbedPanelJSON(this.element, this.dataService.getPanelJSON("id" + this.randomise()))
    }

    deleteThisItem(index: number) {
        if (this.element.panels[this.element.activePanelIndex]["componentList"][index] == this.selectedComponent) {
            this.selectedComponent = null;
        }
        this.element.panels[this.element.activePanelIndex]["componentList"].splice(index, 1);
    }

    copyThisItem(index: number) {
        this.element.panels[this.element.activePanelIndex]["componentList"].splice(index, 0, JSON.parse(JSON.stringify(this.element.panels[this.element.activePanelIndex]["componentList"][index])));
    }

    delete() {
        this.deleteItem.emit(this.order);
    }

    copy() {
        this.copyItem.emit(this.order);
    }

    selectComponent(component) {
        this.selectedComponentEvent.emit(component);
    }

    selectPanel(ind) {
        this.element.activePanelIndex = ind;
    }

    transferDataSuccess($event: any) {
        var randomId = this.randomise();
        switch ($event.dragData.component) {
            case "TEXTFIELD":
                this.element.panels[this.element.activePanelIndex]["componentList"].push(this.dataService.getTextfieldJSON("id" + randomId));
                break;
            case "BUTTON":
                this.element.panels[this.element.activePanelIndex]["componentList"].push(this.dataService.getButtonJSON("id" + randomId));
                break;
            case "DROPDOWN":
                this.element.panels[this.element.activePanelIndex]["componentList"].push(this.dataService.getDropdownJSON("id" + randomId));
                break;
            case "MULTI_SELECT":
                this.element.panels[this.element.activePanelIndex]["componentList"].push(this.dataService.getMultiSelectJSON("id" + randomId));
                break;
            case "PANEL":
                this.element.panels[this.element.activePanelIndex]["componentList"].push(this.dataService.getPanelJSON("id" + randomId));
                break;
            case "TAB_PANEL":
                var tabbedPanel = this.dataService.getTabbedPanelJSON("id" + randomId);
                for (var i = 0; i < 3; i++) {
                    this.dataService.addPanelToTabbedPanelJSON(tabbedPanel, this.dataService.getPanelJSON("id" + this.randomise()));
                }
                this.element.panels[this.element.activePanelIndex]["componentList"].push(tabbedPanel);
                break;
        }
    }

    randomise() {
        return Math.floor(Math.random() * (1000000 - 1)) + 1;
    }
}

import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { AltFormControlModelService } from "../../form-control.model.service";
import { Rule } from '../../rule/rule.model';

@Component({
  selector: 'alt-textbox',
  templateUrl: './textbox.component.html',
})
export class TextboxComponent implements OnInit { 
  @Input() order: number;
  @Output() deleteItem = new EventEmitter<number>();
  @Output() copyItem = new EventEmitter<number>();
  @Input() element: any;
  @Input() editMode: boolean
  events: string[];
  ruleObjects:Rule[];
  rules:number[];
  constructor(private formControlModelService: AltFormControlModelService){ } 

  ngOnInit(){
  }
  callAlert(){
    alert("hello");
  }

  delete(){
    this.deleteItem.emit(this.order);
  }

  copy(){
    this.copyItem.emit(this.order);
  }

  addClass(type:string){
    this.element.cssClass[type].push("");
  }

  removeClass(type,ind){
    this.element.cssClass[type].splice(ind,1);
  }
  
}

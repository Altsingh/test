import { Injectable } from "@angular/core";
import { AltButton } from "./models/alt-button";
import { AltTextbox } from "./models/alt-textbox";
import { AltDropdown } from "./models/alt-dropdown";
import { AltMultiSelect } from "./models/alt-multi-select";
import { AltPanel } from "./models/alt-panel";
import { AltTabbedPanel } from "./models/alt-tabbed-panel";
import { AltDataTable } from "./models/alt-datatable";
import { AltDate } from "./models/alt-date";
import { AltBlank } from "./models/alt-blank";
import { AltImage } from "./models/alt-image";
import { AltTextarea } from "./models/alt-textarea";
import { AltLabel } from './models/alt-label';
import { AltBase } from './models/alt-base';
import { AltComponent } from './models/alt-component';
import { AltAttachments } from './models/alt-attachments';

@Injectable()
export class DefaultDataService {


    public getCheckboxGroupJSON() {
        return {
            "cssClass": {
                "control": [
                    "control_class"
                ],
                "errors": [
                    "error_class"
                ],
                "hint": [
                    "hint_class"
                ],
                "label": [
                    "label_class"
                ]
            },
            "options": [
                {
                    "disabled": false,
                    "label": "class X",
                    "value": "10",
                    "selected": false
                },
                {
                    "disabled": false,
                    "label": "class XII",
                    "value": "12",
                    "selected": false
                },
                {
                    "disabled": false,
                    "label": "graduate",
                    "value": "graduate",
                    "selected": false
                },
                {
                    "disabled": false,
                    "label": "post graduate",
                    "value": "graduate2",
                    "selected": false
                }
            ],
            "namespace": "namespace",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "CHECKBOX_GROUP",
            "relation": [],
            "dbClass": "class",
            "dbAttr": "attr",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "readOnly": false,
            "accept": false
        }
    }

    public getRadioGroupJSON() {
        return {
            "cssClass": {
                "control": [
                    "control_class"
                ],
                "errors": [
                    "error_class"
                ],
                "hint": [
                    "hint_class"
                ],
                "label": [
                    "label_class"
                ]
            },
            "options": [
                {
                    "disabled": false,
                    "label": "Compute Science",
                    "value": "CS",
                    "selected": false
                },
                {
                    "disabled": false,
                    "label": "Information Technology",
                    "value": "IT",
                    "selected": false
                },
                {
                    "disabled": false,
                    "label": "Mechanical Engineering",
                    "value": "ME",
                    "selected": false
                },
                {
                    "disabled": false,
                    "label": "Civil Engineering",
                    "value": "CE",
                    "selected": false
                }
            ],
            "namespace": "namespace",
            "id": "id",
            "label": "label ",
            "name": "name",
            "controlType": "RADIO_GROUP",
            "relation": [],
            "dbClass": "class",
            "dbAttr": "attr",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "readOnly": false,
            "accept": false
        }
    }

    public getTextareaJSON(customId: string) {
        return new AltTextarea({
            "namespace": "namespace",
            "id": customId,
            "label": "Label ",
            "name": "name" + customId,
            "controlType": "TEXTAREA",
            "instruction": "",
            "dataType": "TEXTAREA",
            "relation": [],
            "dbClass": "class",
            "dbAttr": "attr",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "readOnly": false,
            "accept": false,
            "value": "",
            "rows": "4"
        })
    }

    public getDateJSON(customId: string) {
        return new AltDate({
            "namespace": "namespace",
            "id": customId,
            "label": "Label ",
            "name": "name" + customId,
            "controlType": "DATE",
            "instruction": "",
            "dataType": "DATE",
            "relation": [],
            "dbClass": "class",
            "dbAttr": "attr",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "readOnly": false,
            "accept": false,
            "value": "",
            "events": ["change"]
        })
    }

    public getBlankJSON(customId: string) {
        return new AltBlank({
            "namespace": "namespace",
            "id": customId,
            "label": "label ",
            "name": "name" + customId,
            "controlType": "BLANK",
            "instruction": "",
            "dataType": "BLANK",
            "relation": [],
            "dbClass": "class",
            "dbAttr": "attr",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "readOnly": false,
            "accept": false,
            "value": ""
        })
    }

    public getExcelJSON(customId: string) {
        return new AltBase({
            "namespace": "namespace",
            "id": customId,
            "label": "excel ",
            "name": "name" + customId,
            "controlType": "EXCEL",
            "dbClass": "class",
            "dbAttr": "attr"
        })
    }

    public getImageJSON(customId: string) {
        return new AltImage({
            "namespace": "namespace",
            "id": customId,
            "label": "label ",
            "name": "name" + customId,
            "controlType": "IMAGE",
            "instruction": "",
            "dataType": "IMAGE",
            "relation": [],
            "dbClass": "class",
            "dbAttr": "attr",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "readOnly": false,
            "accept": false,
            "value": ""
        })
    }

    public getLabelJSON(customId: string) {
        return new AltLabel({
            "namespace": "namespace",
            "id": customId,
            "label": "label ",
            "name": "name" + customId,
            "controlType": "LABEL",
            "instruction": "",
            "dataType": "STRING",
            "relation": [],
            "dbClass": "class",
            "dbAttr": "attr",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "readOnly": false,
            "accept": false,
            "value": ""
        })
    }


    public getButtonJSON(customId: string) {
        return new AltButton({
            "namespace": "namespace",
            "id": customId,
            "label": "label ",
            "name": "name" + customId,
            "cssClass": "layout_1by8_col",
            "controlType": "BUTTON",
            "componentIds": [],
            "events": ["click"]
        });
    }

    public getTextfieldJSON(customId: string) {
        return new AltTextbox({
            "namespace": "namespace",
            "id": customId,
            "label": "label",
            "name": "name" + customId,
            "cssClass": "layout_1_col",
            "controlType": "TEXTFIELD",
            "instruction": "",
            "helpText": "",
            "dbClassRead": "",
            "dbAttrRead": "",
            "dbClassWrite": "",
            "dbAttrWrite": "",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "placeholder": "",
            "readOnly": false,
            "accept": false,
            "inputType": "TEXT",
            "value": "",
            "required": false,
            "events": ["focusLost", "focusGain"]
        });
    }


    public getDropdownJSON(customId: string) {
        return new AltDropdown({
            "namespace": "namespace",
            "id": customId,
            "label": "label ",
            "name": "name" + customId,
            "cssClass": "layout_1_col",
            "controlType": "DROPDOWN",
            "instruction": "",
            "helpText": "",
            "dbClassRead": "",
            "dbAttrRead": "",
            "dbClassWrite": "",
            "dbAttrWrite": "",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "placeholder": "",
            "readOnly": false,
            "accept": false,
            "inputType": "TEXT",
            "value": "",
            "required": false,
            "events": ["change"]
        });

    }

    public getMultiSelectJSON(customId: string) {
        return new AltMultiSelect({
            "namespace": "namespace",
            "id": customId,
            "label": "label ",
            "name": "name" + customId,
            "cssClass": "layout_1_col",
            "controlType": "MULTI_SELECT",
            "instruction": "",
            "helpText": "",
            "dbClassRead": "",
            "dbAttrRead": "",
            "dbClassWrite": "",
            "dbAttrWrite": "",
            "autoComplete": false,
            "webService": null,
            "autofocus": false,
            "placeholder": "",
            "readOnly": false,
            "accept": false,
            "inputType": "TEXT",
            "value": [],
            "required": false
        });

    }

    public getPanelJSON(customId: string) {
        var saveButton = new AltButton({
            "namespace": "namespace",
            "id": customId,
            "label": "Save",
            "name": "Save" + customId,
            "cssClass": "layout_1_col",
            "controlType": "BUTTON",
            "componentIds": []
        });
        var cancelButton = new AltButton({
            "namespace": "namespace",
            "id": customId,
            "label": "Cancel",
            "name": "Cancel" + customId,
            "cssClass": "layout_1_col",
            "controlType": "BUTTON",
            "componentIds": []
        });
        return new AltPanel({
            "namespace": "namespace",
            "id": customId,
            "label": "label ",
            "name": "name" + customId,
            "cssClass": "layout_4_col",
            "instruction": "",
            "helpText": "",
            "controlType": "PANEL",
            "componentList": [],
            "buttonList": [cancelButton, saveButton]
        })
    }

    public getTabbedPanelJSON(customId: string) {
        return new AltTabbedPanel({
            "namespace": "namespace",
            "id": customId,
            "label": "label ",
            "name": "name" + customId,
            "cssClass": "layout_4_col",
            "instruction": "",
            "helpText": "",
            "controlType": "TAB_PANEL",
            "panels": [],
            "activePanelIndex": 1
        })
    }

    public addPanelToTabbedPanelJSON(tabbedPanel: AltTabbedPanel, panel: AltPanel) {
        tabbedPanel.panels.push(panel);
    }

    public getTableJSON(customId: string) {
        return new AltDataTable({
            "controlType": "DATATABLE",
            "namespace": "namespace",
            "id": customId,
            "cssClass": "layout_4_col",
            "label": "label ",
            "name": "name" + customId,
            "data": null,
            "displayedColumns": ["Column1", "Column2", "Column3"],
            "headers": ["Column1", "Column2", "Column3"],
            "dbClassRead": ""
        })
    }

    removeDoubleQuotes(stringData) {
        if (stringData == null || stringData == 'undefined' || stringData == '') {
            return undefined
        }
        return stringData.replace(/['"]+/g, '')
    }

    public getAttachmentsJSON(customId: string) {
        return new AltAttachments({
            "namespace": "namespace",
            "id": customId,
            "label": "attachments ",
            "name": "name" + customId,
            "controlType": "ATTACHMENTS",
            "totalFileSizeLimitMB":"1"
        })
    }

    public getCheckboxJSON(customId: string) {
        return new AltBase({
            "namespace": "namespace",
            "id": customId,
            "label": "checkbox ",
            "name": "name" + customId,
            "controlType": "CHECKBOX",
            "dbClass": "class",
            "dbAttr": "attr"
        })
    }

    public getLinkJSON(customId: string) {
        return new AltBase({
            "namespace": "namespace",
            "id": customId,
            "label": "link ",
            "name": "name" + customId,
            "controlType": "LINK",
            "dbClass": "class",
            "dbAttr": "attr"
        })
    }
}
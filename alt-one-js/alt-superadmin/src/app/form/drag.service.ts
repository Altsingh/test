import { Injectable } from "@angular/core";

@Injectable()
export class DragService{
    item:string;

    constructor(){}

    setItem(item:string){
        this.item=item;
    }

    getItem(){
        return this.item;
    }

}
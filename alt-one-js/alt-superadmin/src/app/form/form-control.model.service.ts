import { Injectable } from "@angular/core";
import { AltTextbox } from "./models/alt-textbox";
import { AltTextarea } from "./models/alt-textarea";
import { AltDropdown } from "./models/alt-dropdown";
import { AltCheckboxSingle } from "./models/alt-checkbox-single";
import { AltSlider } from "./models/alt-slider";
import { AltDate } from "./models/alt-date";
import { AltTime } from "./models/alt-time";
import { AltCheckboxGroup } from "./models/alt-checkbox-group";
import { AltPanel } from "./models/alt-panel";
import { AltRadioGroup } from "./models/alt-radio-group";

@Injectable()
export class AltFormControlModelService {

    public getTextfieldJSON() {
        var textField = new AltTextbox({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "instruction":"my instruction",
            "controlType": "TEXTFIELD",
            "value": "",
            "placeholder": "placeholder",
            "inputType": "TEXT"
        });
        return textField;
    }

    public getTextareaJSON() {
        var textArea = new AltTextarea({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "TEXTAREA",
            "value": "",
            "placeholder": "placeholder",
            "rows": 4
        });
        return textArea;
    }

    public getDropdownJSON() {
        var dropDown = new AltDropdown({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "DROPDOWN",
            "value": "",
            "options": [{
                "disabled": false,
                "label": "option 1",
                "value": "1",
                "selected": false
            }, {
                "disabled": false,
                "label": "option 2",
                "value": "2",
                "selected": false
            }, {
                "disabled": false,
                "label": "option 2",
                "value": "3",
                "selected": false
            }],
            "multiple": false
        });
        return dropDown;
    }

    public getCheckboxSingleJSON() {
        var checkBox = new AltCheckboxSingle({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "CHECKBOX_SINGLE",
            "value": false,
        });
        return checkBox;
    }

    public getSliderJSON() {
        var slider = new AltSlider({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "SLIDER",
            "value": 0,
            "max": 100,
            "min": 0
        });
        return slider;
    }

    public getDateJSON() {
        var date = new AltDate({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "DATE",
            "value": "",
            "placeholder": "placeholder"
        });
        return date;
    }

    public getTimeJSON() {
        var time = new AltTextbox({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "TIME",
            "value": "",
            "placeholder": "placeholder"
        });
        return time;
    }

    public getCheckboxGroupJSON() {
        var checkBoxGroup = new AltCheckboxGroup({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "CHECKBOX_GROUP",
            "value": "",
            "placeholder": "placeholder",
            "options": []
        });
        var counter = 1;
        while (counter < 4) {
            var checkBox = new AltCheckboxSingle({
                "namespace": "namespace",
                "dbClass": "dbClass",
                "dbAttr": "dbAttr",
                "id": checkBoxGroup.id + counter,
                "label": "option" + counter,
                "name": checkBoxGroup.id + counter,
                "controlType": "CHECKBOX_SINGLE",
                "value": false,
            });
            checkBoxGroup.options.push(checkBox);
            counter++;
        }
        return checkBoxGroup;
    }

    public getRadioGroupJSON() {
        var radioGroup = new AltRadioGroup({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "RADIO_GROUP",
            "value": "",
            "placeholder": "placeholder",
            "options": []
        });
        var counter = 1;
        while (counter < 4) {
            var checkBox = new AltCheckboxSingle({
                "namespace": "namespace",
                "dbClass": "dbClass",
                "dbAttr": "dbAttr",
                "id": radioGroup.id + counter,
                "label": "option" + counter,
                "name": radioGroup.id + counter,
                "controlType": "CHECKBOX_SINGLE",
                "value": false,
            });
            radioGroup.options.push(checkBox);
            counter++;
        }
        return radioGroup;
    }

    public getPanelJSON() {
        var panel = new AltPanel({
            "namespace": "namespace",
            "dbClass": "dbClass",
            "dbAttr": "dbAttr",
            "id": "id",
            "label": "label",
            "name": "name",
            "controlType": "PANEL",
            "value": "",
            "placeholder": "placeholder",
            "children": []
        });
        return panel;
    }
}
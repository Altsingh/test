import { Component, ElementRef, OnInit } from '@angular/core';
import { FormHttpService } from "../form-http.service";
import { DefaultDataService } from "../default-data.service";
import { FormEntityModel } from "../pojo/form-entity.model";
import { AltCommonService } from "../../alt-common/alt-common.service";
import { ActivatedRoute, Params } from "@angular/router";
import { notifications } from "../../alt-common/static.notifications";
import { ClassIdNameModel } from "../models/class-data-models/class-id-name.model";
import { ClassAttributesModel } from "../models/class-data-models/class-attributes.model";
import { AltComponent } from "../models/alt-component";
import { FormUpdateResponseModel } from "../pojo/form-update-response.model"
import { Rule, Case, RuleInput, RulePopup, CasePopup, RuleDeleteRequest } from '../rule/rule.model';;
import { RuleDataService } from '../services/rule/rule-data.service';
import { NestedAttributeService } from '../services/reference-attribute/nested-attribute.service';
import { FilterBase } from "../models/filter/filter-base-pojo";
import { CommEnvlp, CommunicationType } from '../mail/mail.model';
import { FormControl } from '../../../../node_modules/@angular/forms';
import { Observable } from '../../../../node_modules/rxjs';
import { startWith, map } from '../../../../node_modules/rxjs/operators';
import { ApplicationHttpService } from '../../alt-application/application-http.service';
import { Location } from '@angular/common';
import { FormModel } from '../pojo/form.model';
import { debug } from 'util';

@Component({
  selector: 'app-form-creator',
  templateUrl: './form-creator.component.html',
  styleUrls: ['./form-creator.component.css']
})
export class FormCreatorComponent implements OnInit {

  myControl = new FormControl();
  myControlCc = new FormControl();
  myControlBcc = new FormControl();
  filteredOptions: Observable<any[]>;
  filteredOptionsCc: Observable<any[]>;
  filteredOptionsBcc: Observable<any[]>;

  rulePopup: any;
  envelope: CommEnvlp;
  commListOp: CommEnvlp[];
  cc: any;
  bcc: any;
  to: any;
  attributeNames: string[];
  showRuleDialog: boolean;
  showMailDialog: boolean;
  uiClassCode: string;
  formEntityModel: FormEntityModel;
  targetFormEntityModel: FormEntityModel;
  formUpdateResponseModel: FormUpdateResponseModel
  selectedComponent: any;
  message: string;
  errMessage: string;
  interval: any;
  isFormPublished: boolean = false;
  isFormDraft: boolean = false;
  editMode: boolean = false;
  editCommObj: CommEnvlp;
  taskCodeEnabled: boolean;

  app: any;

  showPanel: boolean = true;
  showInputControls: boolean = true;
  showQuickPresets: boolean = true;
  showControls: boolean = true;
  randomId: number;
  altCopiedComponent: AltComponent;
  component: any;
  component1: any;
  component2: any;
  ruleEvent: string;
  scriptData: string;

  classes: ClassIdNameModel[];
  classAttributesRead: ClassAttributesModel;
  classAttributesWrite: ClassAttributesModel;

  filteredRoles: any[];

  panel: Object = { component: 'PANEL' };
  tabbedPanel: Object = { component: 'TABBED_PANEL' };
  table: Object = { component: 'TABLE' };
  card: Object = { component: 'CARD' };
  accordian: Object = { component: 'ACCORDIAN' };
  chart: Object = { component: 'CHART' };
  textField: Object = { component: 'TEXTFIELD' };
  dropDown: Object = { component: 'DROPDOWN' };
  multiSelect: Object = { component: 'MULTI_SELECT' };
  checkboxGroup: Object = { component: 'CHECKBOX_GROUP' };
  date: Object = { component: 'DATE' };
  blank: Object = { component: 'BLANK' };
  image: Object = { component: 'IMAGE' };
  label: Object = { component: 'LABEL' };
  textarea: Object = { component: 'TEXTAREA' };
  radioGroup: Object = { component: 'RADIO_GROUP' };
  file: Object = { component: 'FILE' };
  button: Object = { component: 'BUTTON' };
  note: Object = { component: 'NOTE' };
  showSettingsPanel: boolean = true;
  copyCount: number = 1;
  showSettings: boolean;
  forms: FormModel[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private elementRef: ElementRef,
    private formHttpService: FormHttpService,
    private httpService: ApplicationHttpService,
    private dataService: DefaultDataService,
    private commonService: AltCommonService,
    private ruleDataService: RuleDataService,
    private nestedAttributeService: NestedAttributeService,
    private location: Location) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['id']) {
        this.uiClassCode = this.commonService.replaceUnderScoreByHash(params['id']);
        this.getFormDetails();
      } else {
        this.uiClassCode = "";
      }
    });
  }

  /*ngAfterContentChecked() {
    if(this.filteredOptions && this.filteredRoles){
      this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith<string | any>(''),
            map(value => typeof value === 'string' ? value : value.roleName),
            map(roleName => roleName ? this._filter(roleName) : this.filteredRoles.slice())
          );

          if(this.editCommObj && this.filteredOptions){
            this.cc = this.editCommObj.cc 
            this.to = this.editCommObj.to
            this.bcc = this.editCommObj.bcc
          }
    }
    
    this.cdref.detectChanges();
    
  }*/

  selectComponent(component) {
    this.selectedComponent = component;
    if (component.controlType == "BUTTON") {
      this.getCommObjectsDetailsByCommIds();
    }

    if (!this.selectedComponent.filters) {
      this.selectedComponent.filters = new Array();
      this.selectedComponent.filters.push(new FilterBase());
    }
  }

  getFormDetails() {
    this.formHttpService.getFormDetails(this.commonService.replaceHashStringByUnderScore(this.uiClassCode))
      .subscribe((res: FormEntityModel) => {
        this.formEntityModel = res;
        if (this.formEntityModel.formState == "PUBLISH") {
          this.isFormPublished = true;
          this.isFormDraft = false;
        }
        else if (this.formEntityModel.formState == "DRAFT") {
          this.isFormDraft = true;
          this.isFormPublished = false;
          this.editMode = true;
        } else {
          this.isFormDraft = false;
          this.isFormPublished = false;
        }
        this.formHttpService.getPublishedForms(-1, -1, this.formEntityModel.appid).subscribe((res: any) => {
          this.forms = res.body;
        });
      }
      )
  }

  onResizeEnd($event: any, index) {
    console.log(this.elementRef.nativeElement.getElementsByClassName("input_box")[index]);
    console.log($event);
  }

  sortSuccess($event: any) {
    this.submit(false);
  }

  randomise() {
    return Math.floor(Math.random() * (1000000 - 1)) + 1;
  }

  transferDataSuccess($event: any) {
    var randomId = this.randomise();
    switch ($event.dragData.component) {
      case "ATTACHMENTS":
        this.formEntityModel.componentList.push(this.dataService.getAttachmentsJSON("id" + randomId));
        break;
      case "BLANK":
        this.formEntityModel.componentList.push(this.dataService.getBlankJSON("id" + randomId));
        break;
      case "BUTTON":
        this.formEntityModel.componentList.push(this.dataService.getButtonJSON("id" + randomId));
        break;
      case "CHECKBOX":
        this.formEntityModel.componentList.push(this.dataService.getCheckboxJSON("id" + randomId));
        break;
      case "DATE":
        this.formEntityModel.componentList.push(this.dataService.getDateJSON("id" + randomId));
        break;
      case "DROPDOWN":
        this.formEntityModel.componentList.push(this.dataService.getDropdownJSON("id" + randomId));
        break;
      case "EXCEL":
        this.formEntityModel.componentList.push(this.dataService.getExcelJSON("id" + randomId));
        break;
      case "LINK":
        this.formEntityModel.componentList.push(this.dataService.getLinkJSON("id" + randomId));
        break;
      case "MULTI_SELECT":
        this.formEntityModel.componentList.push(this.dataService.getMultiSelectJSON("id" + randomId));
        break;
      case "PANEL":
        this.formEntityModel.componentList.push(this.dataService.getPanelJSON("id" + randomId));
        break;
      case "TAB_PANEL":
        var tabbedPanel = this.dataService.getTabbedPanelJSON("id" + randomId);
        for (var i = 0; i < 3; i++) {
          this.dataService.addPanelToTabbedPanelJSON(tabbedPanel, this.dataService.getPanelJSON("id" + this.randomise()));
        }
        this.formEntityModel.componentList.push(tabbedPanel);
        break;
      case "TEXTFIELD":
        this.formEntityModel.componentList.push(this.dataService.getTextfieldJSON("id" + randomId));
        break;
      case "IMAGE":
        this.formEntityModel.componentList.push(this.dataService.getImageJSON("id" + randomId));
        break;
      case "LABEL":
        this.formEntityModel.componentList.push(this.dataService.getLabelJSON("id" + randomId));
        break;
      case "TEXTAREA":
        this.formEntityModel.componentList.push(this.dataService.getTextareaJSON("id" + randomId));
        break;
      case "TABLE":
        this.formEntityModel.componentList.push(this.dataService.getTableJSON("id" + randomId));
        break;
    }
    this.submit(false);
  }

  deleteThisItem(index: number) {
    if (this.formEntityModel.componentList[index] == this.selectedComponent) {
      this.selectedComponent = null;
    }
    this.formEntityModel.componentList.splice(index, 1);
    this.submit(false);
  }

  copyThisItem(index: number) {
    this.altCopiedComponent = JSON.parse(JSON.stringify(this.formEntityModel.componentList[index]));
    this.altCopiedComponent.id = this.altCopiedComponent.id + this.copyCount;
    this.altCopiedComponent.name = this.altCopiedComponent.name + this.copyCount;
    this.altCopiedComponent.label = this.altCopiedComponent.label + this.copyCount;
    this.formEntityModel.componentList.splice(index + 1, 0, JSON.parse(JSON.stringify(this.altCopiedComponent)));
    this.submit(false);
    this.copyCount = this.copyCount + 1;
  }

  isAlphanumeric(val) {
    return /^[a-z0-9"_ ]+$/i.test(val);
  }
  optionValidation() {
    if (this.component.options) {
      for (let j = 0; j < this.component.options.length; j++) {
        if (this.component.options[j] == "" || !this.isAlphanumeric(this.component.options[j]) &&
          !this.component.dbClassRead && !this.component.dbAttrRead) {
          if (!this.component.dbClassRead && this.component.options.length > 0) {
            /**
             * this is the case when user enters values manually from UI.
             * i.e. no master data selection. hence dbclassRead will be 
             * null/undefined
            */
            return true;
          } else {
            this.errMessage = notifications.options_empty_alphanumeric;
            setTimeout(() => {
              this.errMessage = "";
            }, 3000);
            return false;
          }
        }
      }
    }

    if ((this.component.dbClassRead && this.component.dbAttrRead) &&
      (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
      this.component.dbClassRead = null;
      this.component.dbAttrRead = null;
    }

    if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
      (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
      this.component.dbClassWrite = null;
      this.component.dbAttrWrite = null;
    }
    return true;
  }

  submit(shouldPublish?) {
    if (shouldPublish == 'undefined') {
      this.editMode = true;
    }

    for (let i = 0; i < this.formEntityModel.componentList.length; i++) {
      if (this.formEntityModel.componentList[i].controlType == "DROPDOWN" ||
        this.formEntityModel.componentList[i].controlType == "MULTI_SELECT") {
        this.component = this.formEntityModel.componentList[i];
        if (!this.optionValidation()) {
          return;
        }
      } else if (this.formEntityModel.componentList[i].controlType == "DATE") {
        this.component = this.formEntityModel.componentList[i];
        if ((this.component.dbClassRead && this.component.dbAttrRead) &&
          (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
          this.component.dbClassRead = null;
          this.component.dbAttrRead = null;
        }

        if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
          (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
          this.component.dbClassWrite = null;
          this.component.dbAttrWrite = null;
        }
      } else if (this.formEntityModel.componentList[i].controlType == "BLANK") {
        this.component = this.formEntityModel.componentList[i];
        if ((this.component.dbClassRead && this.component.dbAttrRead) &&
          (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
          this.component.dbClassRead = null;
          this.component.dbAttrRead = null;
        }

        if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
          (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
          this.component.dbClassWrite = null;
          this.component.dbAttrWrite = null;
        }
      } else if (this.formEntityModel.componentList[i].controlType == "IMAGE") {
        this.component = this.formEntityModel.componentList[i];
        if ((this.component.dbClassRead && this.component.dbAttrRead) &&
          (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
          this.component.dbClassRead = null;
          this.component.dbAttrRead = null;
        }

        if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
          (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
          this.component.dbClassWrite = null;
          this.component.dbAttrWrite = null;
        }
      } else if (this.formEntityModel.componentList[i].controlType == "LABEL") {
        this.component = this.formEntityModel.componentList[i];
        if ((this.component.dbClassRead && this.component.dbAttrRead) &&
          (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
          this.component.dbClassRead = null;
          this.component.dbAttrRead = null;
        }

        if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
          (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
          this.component.dbClassWrite = null;
          this.component.dbAttrWrite = null;
        }
      } else if (this.formEntityModel.componentList[i].controlType == "TEXTAREA") {
        this.component = this.formEntityModel.componentList[i];
        if ((this.component.dbClassRead && this.component.dbAttrRead) &&
          (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
          this.component.dbClassRead = null;
          this.component.dbAttrRead = null;
        }

        if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
          (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
          this.component.dbClassWrite = null;
          this.component.dbAttrWrite = null;
        }
      } else if (this.formEntityModel.componentList[i].controlType == "TEXTFIELD") {
        this.component = this.formEntityModel.componentList[i];
        if ((this.component.dbClassRead && this.component.dbAttrRead) &&
          (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
          this.component.dbClassRead = null;
          this.component.dbAttrRead = null;
        }

        if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
          (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
          this.component.dbClassWrite = null;
          this.component.dbAttrWrite = null;
        }
      } else if (this.formEntityModel.componentList[i].controlType == "PANEL") {
        this.component1 = this.formEntityModel.componentList[i];
        for (let j = 0; j < this.component1.componentList.length; j++) {
          this.component = this.component1.componentList[j];
          if (this.component.controlType == "DROPDOWN" || this.component.controlType == "MULTI_SELECT") {
            if (!this.optionValidation()) {
              return;
            }
          } else if (this.component.controlType == "DATE") {
            if ((this.component.dbClassRead && this.component.dbAttrRead) &&
              (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
              this.component.dbClassRead = null;
              this.component.dbAttrRead = null;
            }
            if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
              (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
              this.component.dbClassWrite = null;
              this.component.dbAttrWrite = null;
            }
          } else if (this.component.controlType == "BLANK") {
            if ((this.component.dbClassRead && this.component.dbAttrRead) &&
              (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
              this.component.dbClassRead = null;
              this.component.dbAttrRead = null;
            }
            if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
              (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
              this.component.dbClassWrite = null;
              this.component.dbAttrWrite = null;
            }
          } else if (this.component.controlType == "IMAGE") {
            if ((this.component.dbClassRead && this.component.dbAttrRead) &&
              (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
              this.component.dbClassRead = null;
              this.component.dbAttrRead = null;
            }
            if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
              (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
              this.component.dbClassWrite = null;
              this.component.dbAttrWrite = null;
            }
          } else if (this.component.controlType == "LABEL") {
            if ((this.component.dbClassRead && this.component.dbAttrRead) &&
              (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
              this.component.dbClassRead = null;
              this.component.dbAttrRead = null;
            }
            if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
              (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
              this.component.dbClassWrite = null;
              this.component.dbAttrWrite = null;
            }
          } else if (this.component.controlType == "TEXTAREA") {
            if ((this.component.dbClassRead && this.component.dbAttrRead) &&
              (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
              this.component.dbClassRead = null;
              this.component.dbAttrRead = null;
            }
            if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
              (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
              this.component.dbClassWrite = null;
              this.component.dbAttrWrite = null;
            }
          } else if (this.component.controlType == "TEXTFIELD") {
            if ((this.component.dbClassRead && this.component.dbAttrRead) &&
              (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
              this.component.dbClassRead = null;
              this.component.dbAttrRead = null;
            }
            if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
              (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
              this.component.dbClassWrite = null;
              this.component.dbAttrWrite = null;
            }
          } else if (this.component.controlType == "PANEL") {
            this.component2 = this.component;
            for (let k = 0; k < this.component2.componentList.length; k++) {
              this.component = this.component2.componentList[k];
              if (this.component.controlType == "DROPDOWN" || this.component.controlType == "MULTI_SELECT") {
                if (!this.optionValidation()) {
                  return;
                }
              } else if (this.component.controlType == "DATE") {
                if ((this.component.dbClassRead && this.component.dbAttrRead) &&
                  (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
                  this.component.dbClassRead = null;
                  this.component.dbAttrRead = null;
                }
                if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
                  (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
                  this.component.dbClassWrite = null;
                  this.component.dbAttrWrite = null;
                }
              } else if (this.component.controlType == "BLANK") {
                if ((this.component.dbClassRead && this.component.dbAttrRead) &&
                  (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
                  this.component.dbClassRead = null;
                  this.component.dbAttrRead = null;
                }
                if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
                  (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
                  this.component.dbClassWrite = null;
                  this.component.dbAttrWrite = null;
                }
              } else if (this.component.controlType == "IMAGE") {
                if ((this.component.dbClassRead && this.component.dbAttrRead) &&
                  (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
                  this.component.dbClassRead = null;
                  this.component.dbAttrRead = null;
                }
                if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
                  (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
                  this.component.dbClassWrite = null;
                  this.component.dbAttrWrite = null;
                }
              } else if (this.component.controlType == "LABEL") {
                if ((this.component.dbClassRead && this.component.dbAttrRead) &&
                  (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
                  this.component.dbClassRead = null;
                  this.component.dbAttrRead = null;
                }
                if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
                  (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
                  this.component.dbClassWrite = null;
                  this.component.dbAttrWrite = null;
                }
              } else if (this.component.controlType == "TEXTAREA") {
                if ((this.component.dbClassRead && this.component.dbAttrRead) &&
                  (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
                  this.component.dbClassRead = null;
                  this.component.dbAttrRead = null;
                }
                if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
                  (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
                  this.component.dbClassWrite = null;
                  this.component.dbAttrWrite = null;
                }
              } else if (this.component.controlType == "TEXTFIELD") {
                if ((this.component.dbClassRead && this.component.dbAttrRead) &&
                  (this.component.dbClassRead == 'select' || this.component.dbAttrRead == 'select')) {
                  this.component.dbClassRead = null;
                  this.component.dbAttrRead = null;
                }
                if ((this.component.dbClassWrite && this.component.dbAttrWrite) &&
                  (this.component.dbClassWrite == 'select' || this.component.dbAttrWrite == 'select')) {
                  this.component.dbClassWrite = null;
                  this.component.dbAttrWrite = null;
                }
              }
            }
          }
        }
      }
    }

    if (this.interval) {
      clearTimeout(this.interval);
      this.message = "";
    }
    this.formHttpService.updateForm(this.formEntityModel)
      .subscribe((response: FormUpdateResponseModel) => {
        this.formUpdateResponseModel = response;
        this.uiClassCode = this.formUpdateResponseModel.formId;
        console.log('creator uiClassCode ' + this.uiClassCode);
        this.getFormDetails();
        this.formEntityModel.cas = response.cas;
        this.message = notifications.updated_successful;
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
          }, 3000);
        })(this);
        if (shouldPublish) {
          this.isFormPublished = true;
        }
        else {
          this.isFormPublished = false;
        }
      })
    this.selectedComponent = null;
  }

  submitMeta() {
    if (this.interval) {
      clearTimeout(this.interval);
      this.message = "";
    }
    if (this.formEntityModel.formState != "PUBLISH") {
      this.errMessage = notifications.form_unpublished;
      this.interval = (function (that) {
        return setTimeout(function () {
          that.message = "";
        }, 3000);
      })(this);
    } else {
      this.formHttpService.submitMeta(this.formEntityModel)
        .subscribe(response => {
          this.getFormDetails();
          this.message = notifications.security_sent;
          this.interval = (function (that) {
            return setTimeout(function () {
              that.message = "";
            }, 3000);
          })(this);
        }, error => {
          console.log(error);
        })
    }
  }

  publish() {
    if (this.taskCodeEnabled) {
      this.formEntityModel.taskCodeEnabled = this.taskCodeEnabled;
    }
    this.formHttpService.publishForm(this.formEntityModel)
      .subscribe((response: FormUpdateResponseModel) => {
        this.editMode = false;
        this.formUpdateResponseModel = response;
        this.uiClassCode = this.formUpdateResponseModel.formId;
        this.getFormDetails();
        this.message = notifications.updated_successful;
        this.interval = (function (that) {
          return setTimeout(function () {
            that.message = "";
          }, 3000);
        })(this);
      }, error => {
        console.log(error);
      })
  }

  clear() {
    this.formEntityModel.componentList = [];
  }

  addCase(rule) {
    this.formAttributes();
    var c = new Case;
    /*if (rule.ruleType==RuleType.VALIDATION){
      c = new ValidationCase;
    }else if(rule.ruleType==RuleType.TRANSACTION){
      c = new TransactionCase;
    }*/
    if (!rule.cases) {
      rule.cases = [];
    }
    rule.cases.push(c);

  }

  addRule() {
    this.rulePopup = [];
    var rule = new Rule;
    rule.ruleType = "VALIDATION";
    this.addCase(rule);
    this.rulePopup.push(rule);
    this.showRuleDialog = true;
  }

  addNewMail(editMail?) {
    this.getApp(this.formEntityModel.appid);

    if (editMail) {
      this.envelope = this.editCommObj;
    } else {
      this.envelope = new CommEnvlp;
    }

    this.envelope.commType = CommunicationType.MAIL;
    this.showMailDialog = true;
  }

  getApp(appId) {
    this.httpService.getApp(appId)
      .subscribe(res => {
        this.app = res;
        // populating list for cc, bcc, and to
        this.filteredRoles = this.app.roles;

        if (this.filteredRoles) {
          this.filteredOptions = this.myControl.valueChanges
            .pipe(
              startWith<string | any>(''),
              map(value => (typeof value === 'string' && value != null) ? value : value.roleName),
              map(roleName => roleName ? this._filter(roleName) : this.filteredRoles.slice())
            );

          this.filteredOptionsCc = this.myControlCc.valueChanges
            .pipe(
              startWith<string | any>(''),
              map(value => (typeof value === 'string' && value != null) ? value : value.roleName),
              map(roleName => roleName ? this._filter(roleName) : this.filteredRoles.slice())
            );

          this.filteredOptionsBcc = this.myControlBcc.valueChanges
            .pipe(
              startWith<string | any>(''),
              map(value => (typeof value === 'string' && value != null) ? value : value.roleName),
              map(roleName => roleName ? this._filter(roleName) : this.filteredRoles.slice())
            );

          if (this.editCommObj) {

            if (this.editCommObj.from) {
              this.editCommObj.from = this.prepareFromFieldInEditing(this.editCommObj.from);
            }

            if (this.editCommObj.cc) {
              var ccValue = this.editCommObj.cc.replace(/[@]/g, '');

              if (ccValue) {
                for (var i = 0; i < this.filteredRoles.length; i++) {
                  if (this.filteredRoles[i].roleName.toLowerCase() == ccValue.toLowerCase()) {
                    this.cc = this.filteredRoles[i];
                    break;
                  }
                }
              }
            } else {
              this.cc = null;
            }

            if (this.editCommObj.to) {
              var toValue = this.editCommObj.to.replace(/[@]/g, '');

              if (toValue) {
                for (var i = 0; i < this.filteredRoles.length; i++) {
                  if (this.filteredRoles[i].roleName.toLowerCase() == toValue.toLowerCase()) {
                    this.to = this.filteredRoles[i];
                    break;
                  }
                }
              }

            } else {
              this.to = null;
            }

            if (this.editCommObj.bcc) {
              var bccValue = this.editCommObj.bcc.replace(/[@]/g, '');

              if (bccValue) {
                for (var i = 0; i < this.filteredRoles.length; i++) {
                  if (this.filteredRoles[i].roleName.toLowerCase() == bccValue.toLowerCase()) {
                    this.bcc = this.filteredRoles[i];
                    break;
                  }
                }
              }
            } else {
              this.bcc = null;
            }

          }

        }
      })
  }

  displayFn(role?: any): string | undefined {
    return role ? role.roleName : undefined;
  }

  private _filter(roleName: string): any[] {
    const filterValue = roleName.toLowerCase();
    return this.filteredRoles.filter(option => option.roleName.toLowerCase().indexOf(filterValue) === 0);
  }

  formAttributes() {
    var map = new Map<string, string>();
    this.attributeNames = this.flattenHtmlComponents(this.formEntityModel.componentList)
      .filter(c => !(c.dbClassType == "Local" && c.dbClassRead && c.dbAttrRead))
      .map(c => c.name);
    var referenceAttr = this.flattenHtmlComponents(this.formEntityModel.componentList)
      .filter(c => (c.dbClassType == "Local" && c.dbClassRead && c.dbAttrRead))
      .forEach(c => {
        map.set(c.name, c.dbClassRead)
      })
    var request = Array.from(map).reduce((o, [key, value]) => {
      o[key] = value;

      return o;
    }, {});

    this.nestedAttributeService.getNestedAttributes(request).subscribe((res: any) => {
      if (res.status == 200) {
        var response: string[] = res.body;
        response.forEach(attr => this.attributeNames.push(attr));
      }
    });

  }

  prepareFromFieldInEditing(from): string {
    var fromString = from;

    while (fromString.charAt(0) === '@') {
      fromString = fromString.slice(1);
    }
    fromString = fromString.split("").reverse().join("");

    while (fromString.charAt(0) === '@') {
      fromString = fromString.slice(1);
    }

    fromString = fromString.split("").reverse().join("");
    return fromString;
  }
  editMail(mail) {
    this.editCommObj = mail;


    this.addNewMail(true);
  }

  editRule(rule) {
    this.formAttributes();
    this.rulePopup = [];

    var rulePopupObj = new RulePopup;
    rulePopupObj.ruleId = rule.ruleId
    rulePopupObj.ruleName = rule.ruleName;
    rulePopupObj.ruleType = rule.ruleType;
    var casesPopup = new Array;
    rulePopupObj.cases = casesPopup;
    rule.cases.forEach(caseElement => {
      var c = new CasePopup;
      c.caseName = caseElement.caseName;
      c.firstInput = caseElement.firstInput.dbClassAttr;
      c.validationType = caseElement.validationType;
      if (caseElement.secondInput.inputType == 'REFERENCE') {
        c.secondAttribute = caseElement.secondInput.dbClassAttr;
      } else {
        c.secondInput = caseElement.secondInput.value;
      }
      if (caseElement.assignToAttribute) {
        c.assignToAttribute = caseElement.assignToAttribute.value;
      }
      c.failureMessage = caseElement.failureMessage;
      if (caseElement.scriptData) {
        this.scriptData = caseElement.scriptData;
      }
      this.ruleEvent = caseElement.event;
      casesPopup.push(c);
    });

    this.rulePopup.push(rulePopupObj);
    this.showRuleDialog = true;
  }

  onFocus(event) {
  }

  createMailUIValidation(): boolean {
    if (this.envelope.from) {
      if (!this.envelope.from.includes("@")) {
        return false;
      }
    } else {
      return false;
    }

    if (this.to) {
      if (!this.to.roleName) {
        return false;
      }
    } else {
      return false;
    }

    if (!(this.envelope.subject) || !(this.envelope.body)) {
      return false;
    }

    return true;
  }

  saveMail(deleteComm?) {
    if (!deleteComm || deleteComm == 'undefined') {
      if (!this.createMailUIValidation()) {
        return;
      }
      if (this.cc) {
        this.envelope.cc = "@@" + this.cc.roleName + "@@";
      }
      if (this.bcc) {
        this.envelope.bcc = "@@" + this.bcc.roleName + "@@";
      }
      this.envelope.to = "@@" + this.to.roleName + "@@";
      this.envelope.from = "@@" + this.envelope.from + "@@";
      this.envelope.templateName = this.app.appName + ":" + this.formEntityModel.uiClassName + ":" + this.envelope.subject;
      // set action as "CREATE" as we're saving the mail template
      if (this.editCommObj) {
        this.envelope.action = "UPDATE"
      } else {
        this.envelope.action = "CREATE"
      }
    } else {
      this.envelope.action = "DELETE"
    }

    this.formHttpService.communicationOperations(this.envelope).subscribe((res: CommEnvlp) => {
      this.envelope = res;
      if (!deleteComm) {
        if (!this.editCommObj) {
          if (this.selectedComponent.commIdList) {
            this.selectedComponent.commIdList.push(this.envelope.commId);
          } else {
            this.selectedComponent.commIdList = [];
            var commIds = new Array;
            commIds.push(this.envelope.commId)
            this.selectedComponent.commIdList = commIds;
          }
        }
      } else {
        for (var i = 0; i < this.selectedComponent.commIdList.length; i++) {
          if (this.selectedComponent.commIdList[i] === this.envelope.commId) {
            this.selectedComponent.commIdList.splice(i, 1);
            break;
          }
        }
      }


      // auto save commid generated from comm API to form table in postgres
      if (this.envelope) {
        this.submit(false);
      }
      // reset mail fields once mail is saved
      this.resetMailFields();
    })
    console.log(this.envelope);
  }

  getCommObjectsDetailsByCommIds() {
    if (this.selectedComponent.commIdList != null && this.selectedComponent.commIdList != 'undefined'
      && this.selectedComponent.commIdList.length > 0) {
      this.envelope = new CommEnvlp;
      this.envelope.action = "SELECT";
      this.envelope.selectMailIds = this.selectedComponent.commIdList
      this.formHttpService.communicationOperations(this.envelope).subscribe((res: CommEnvlp) => {
        this.envelope = res;
        this.commListOp = this.envelope.commEnvlpList
      })
    }

  }

  saveRules() {
    var ruleList = new Array;
    var rule = new Rule;
    rule.ruleName = this.rulePopup[0].ruleName;
    rule.ruleType = this.rulePopup[0].ruleType;
    rule.ruleId = this.rulePopup[0].ruleId;

    var cases = new Array;
    rule.cases = cases;
    this.rulePopup[0].cases.forEach(caseElement => {
      var c = new Case;
      c.caseName = caseElement.caseName;
      c.firstInput = new RuleInput;
      c.firstInput.inputType = "REFERENCE";
      c.firstInput.dbClassAttr = caseElement.firstInput;
      c.validationType = caseElement.validationType;
      c.secondInput = new RuleInput;
      if (caseElement.secondAttribute) {
        c.secondInput.inputType = "REFERENCE";
        c.secondInput.dbClassAttr = caseElement.secondAttribute;
      } else {
        c.secondInput.inputType = "VALUE";
        c.secondInput.value = caseElement.secondValue;
      }
      c.assignToAttribute = new RuleInput;
      c.assignToAttribute.inputType = "REFERENCE";
      c.assignToAttribute.dbClassAttr = caseElement.assignToAttribute;

      c.failureMessage = caseElement.failureMessage;

      c.componentName = this.selectedComponent.name;
      c.componentType = this.selectedComponent.controlType;
      c.event = this.ruleEvent;
      if (this.selectedComponent.controlType != "BUTTON") {
        c.ruleExecutionEnd = "FRONTEND";
      } else {
        c.ruleExecutionEnd = "BACKEND";
      }

      if (this.scriptData) {
        c.scriptData = this.scriptData;
        c.caseType = "ADVANCE";
      } else {
        c.caseType = "BASIC";
      }

      cases.push(c);
    });
    ruleList.push(rule);
    this.ruleDataService.create(ruleList).subscribe((res: any) => {
      if (res.status == 200) {
        this.ruleEvent = "";
        res.body.rules.forEach(element => {
          if (!this.selectedComponent.ruleObjects) {
            this.selectedComponent.ruleObjects = [];
          }
          this.selectedComponent.ruleObjects = this.selectedComponent.ruleObjects.filter(e => e.ruleId != element.ruleId)
          this.selectedComponent.ruleObjects.push(element);
        });;
      }
    }
    );
    this.showRuleDialog = false;

  }

  deleteMail(mail) {
    this.envelope = mail;
    this.saveMail(true);
  }

  deleteRule(rule) {
    var ruleList = new Array;
    ruleList.push(rule.ruleId);
    var ruleDeleteRequest = new RuleDeleteRequest;
    ruleDeleteRequest.rules = ruleList;
    this.ruleDataService.deleteRule(ruleList).subscribe(
      res => {
        if (res.status == 200) {
          this.selectedComponent.ruleObjects = this.selectedComponent.ruleObjects.filter(r => r.ruleId != rule.ruleId)
        }
      }
    );
  }

  resetMailFields() {
    this.showMailDialog = false;
    this.envelope = null;
    this.cc = null;
    this.to = null;
    this.bcc = null;
    this.editCommObj = null;

  }

  taskCodeSwitched(flag) {
    this.taskCodeEnabled = flag;
    console.log("form creator taskCode switch : " + flag);
  }
  onToStart(event) {
    eval("alert(\"test\")");
    console.log("testese")
  }

  goBack() {
    this.location.back();
  }

  flattenHtmlComponents(htmlComponents: AltComponent[]) {
    var flatHtmlComponents = [];
    if (!htmlComponents)
      return flatHtmlComponents;
    for (let comp of htmlComponents) {
      if (comp.controlType == 'PANEL') {
        var panel = comp as any;
        panel.componentList.forEach(element => {
          flatHtmlComponents.push(element);
        });

      } else {
        flatHtmlComponents.push(comp);
      }
    }
    return flatHtmlComponents;
  }

  openSettings(component: any) {
    this.selectComponent(component);

    if (this.selectedComponent.controlType == "BUTTON") {
      if (!this.selectedComponent.navigation) {
        this.selectedComponent.navigation = {};
      } else if (this.selectedComponent.navigation.formId) {
        this.changeTargetForm(this.selectedComponent.navigation.formId);
      }
    }

    this.showSettings = true;
  }

  saveSettings() {
    this.submit(false);
    this.showSettings = false;
  }

  changeTargetForm(formId: any) {
    this.formHttpService.getFormDetails(formId).subscribe((res: FormEntityModel) => {
      this.targetFormEntityModel = res;
    })
  }

}
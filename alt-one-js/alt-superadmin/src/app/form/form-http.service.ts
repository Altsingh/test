import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { FormModel } from "./pojo/form.model";
import { ExternalFormData } from './models/form-external/external-form-data.model';
import { FormCreateModel } from './pojo/form-create.model';
import { Organisation } from './models/form-external/organisation.model';


@Injectable()
export class FormHttpService {

  private formUrl: string = environment.formUrl + "/forms";
  selectedOrganisation: Organisation = null;
  constructor(private http: HttpClient) { }

  getForms(limit, offset, appId) {
    if (appId) {
      return this.http.get(this.formUrl + "?limit=" + limit + "&offset=" + offset + "&appId=" + appId, this.getOptions());
    } else {
      return this.http.get(this.formUrl + "?limit=" + limit + "&offset=" + offset, this.getOptions());
    }
  }

  getLoggedInOrganizationDetail() {
    this.selectedOrganisation = new Organisation();
    this.selectedOrganisation.orgId = atob(localStorage.getItem("realm"));
    this.selectedOrganisation.tenantId = atob(localStorage.getItem("tenantId"));
    return this.selectedOrganisation;
  }

  getPublishedForms(limit, offset, appId): any {
    return this.http.get(this.formUrl + "?limit=" + limit + "&offset=" + offset + "&published=true&appId=" + appId, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  getFormDetails(formId) {
    return this.http.get(this.formUrl + "/" + formId, this.getOptions());
  }

  createForm(form: FormModel, externalFormData: ExternalFormData, appId: Number) {
    return this.http.post(this.formUrl, this.getFormBody(form.formName, externalFormData, appId, form.parentFormId), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  updateForm(form) {
    return this.http.put(this.formUrl, form, this.getOptions());
  }

  publishForm(form) {
    return this.http.post(this.formUrl + "/publish", form, this.getOptions());
  }

  submitMeta(form) {
    return this.http.post(this.formUrl + "/submitFormMeta", form, this.getOptions());
  }

  communicationOperations(comm){
    return this.http.post(this.formUrl + "/mailOperations", comm, this.getOptions());
  }

  private getOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      })
    }
  }

  private getFormBody(name, externalFormData, appId, parentFormId: number) {
    return new FormCreateModel(name, externalFormData, appId, parentFormId);
  }
}

import { Component, OnInit } from '@angular/core';
import { FormHttpService } from "../form-http.service";
import { FormModel } from "../pojo/form.model";
import { AltCommonService } from "../../alt-common/alt-common.service";
import { notifications } from "../../alt-common/static.notifications";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { FormTypeService } from '../services/form-external/form-type.service';
import { FormOrganisationListService } from '../services/form-external/form-organisation-list.service';
import { Organisation } from '../models/form-external/organisation.model';
import { ExternalFormDetail } from '../models/form-external/external-form-detail.model';
import { OrganisationFormListService } from '../services/form-external/organisation-form-list.service';
import { FormFieldGroup } from '../models/form-external/form-field-group.model';
import { FormFieldGroupsService } from '../services/form-external/form-field-groups.service';
import { ExternalFormData } from '../models/form-external/external-form-data.model';
import { ExternelFormType } from '../models/form-external/external-form-type.model';
import { EnumFormType } from '../enum/external-form-enum/enum-form-type';
import { ThrowStmt } from '../../../../node_modules/@angular/compiler';

@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.css']
})
export class FormListComponent implements OnInit {
  private dataMap: Map<string, boolean>;
  forms: FormModel[];
  allForms: FormModel[];
  form: FormModel;
  createMode: boolean = false;
  editMode: boolean = false;
  deleteMode: boolean = false;
  interval: any;
  externalFormData: ExternalFormData;
  message: string;
  formTypes: ExternelFormType[];
  selectedType: ExternelFormType = null;
  selectedOrganisation: Organisation = null;
  selectedExternalForm: string = null;
  selectedFieldGroup: FormFieldGroup = null;
  selectedCheckBox: boolean = true;
  organisations: Organisation[];
  externalForms: ExternalFormDetail[];
  formFieldGroups: FormFieldGroup[];
  appId: Number;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private commonService: AltCommonService,
    private formHttpService: FormHttpService,
    private formTypeService: FormTypeService,
    private formOrganisationListService: FormOrganisationListService,
    private organisationFormListService: OrganisationFormListService,
    private formFieldGroupsService: FormFieldGroupsService
  ) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['appId']) {
        this.appId = params['appId'];
        this.getForms(this.appId);
      }
    });

    this.dataMap = new Map<string, boolean>();
    this.dataMap.clear();
    //this.getForms();
    this.selectedOrganisation = this.formHttpService.getLoggedInOrganizationDetail();
    //this.selectedOrganisation.orgId = atob(localStorage.getItem("realm"));
    //this.selectedOrganisation.tenantId = atob(localStorage.getItem("tenantId"));
    this.organisationChanged(this.selectedOrganisation);
  }

  routeToModules() {
    if (this.appId) {
      this.router.navigateByUrl("/application/app/" + this.appId + "/module");
    }
  }

  routeToHomeBuilder() {
    this.router.navigateByUrl("/application/app/" + this.appId + "/homebuilder");
  }

  getForms(appId: Number) {
    this.formHttpService.getForms(-1, -1, appId)
      .subscribe((res: FormModel[]) => {
        this.forms = res;
        this.allForms = res;
        /**
         * this is against coding standard practice & is a temporary fix
         * & should be removed
         * */
        this.formStateStatusRenaming(this.forms)
      })
  }
  // cometic changes
  formStateStatusRenaming(forms) {
    if (forms != null && !(forms == 'undefined') && forms.length > 0) {
      for (var i = 0; i < forms.length; i++) {
        if (forms[i].formState == 'OBSOLETE') {
          forms[i].formState = "INACTIVE"
        } else if (forms[i].formState == 'PUBLISH') {
          forms[i].formState = "PUBLISHED"
        }
      }
    }
  }

  typeChanged(externalFormType: ExternelFormType) {

    this.formFieldGroups = [];
    //this.externalForms = []; //EF-838
    this.organisations = [];
    //this.selectedOrganisation=null; //EF-838
    this.selectedCheckBox = true;
    if ((externalFormType != null) && (externalFormType.type == "Org Custom Form" || externalFormType.type == "Org Custom Field")) {
      //this.getOrganisationList(); //EF-838
    }
  }

  organisationChanged(orgLocal: Organisation) {
    //if(orgLocal && this.selectedType.enumValue==EnumFormType.CUSTOMFORMFIELD){
    this.getOrganisationForms(this.selectedOrganisation.orgId, this.selectedOrganisation.tenantId);
    //}
    this.selectedExternalForm = null;
    this.selectedFieldGroup = null;
    this.formFieldGroups = [];
  }

  externelFormChanged(id: string) {
    if (id != null) {
      this.getFormFieldGroups(id, this.selectedOrganisation.orgId, this.selectedOrganisation.tenantId);
    } else {
      this.formFieldGroups = [];
    }
    this.selectedFieldGroup = null;

  }
  checkBoxChanged() {
    this.selectedFieldGroup = null;
    if (!this.selectedCheckBox) {
      this.externalFormData.customFormType = EnumFormType.CUSTOMFORMFIELD;
    }
    else {
      this.externalFormData.customFormType = EnumFormType.CUSTOMFORMFIELDNEW;
    }
  }

  getOrganisationList() {
    this.formOrganisationListService.getAll()
      .subscribe(res => {
        this.organisations = res;
      })
  }
  getOrganisationForms(orgId: String, tenantId: String) {
    this.organisationFormListService.getAllForms(orgId, tenantId)
      .subscribe(res => {
        this.externalForms = res;
      })
  }

  getFormFieldGroups(id: String, orgId: String, tenantId: String) {
    this.formFieldGroupsService.getAllFormGroup(id, orgId, tenantId)
      .subscribe(res => {
        this.formFieldGroups = res;
      })
  }

  openCreateWindow() {
    this.form = new FormModel();
    this.externalFormData = new ExternalFormData();
    this.createMode = true;
    this.formTypes = this.formTypeService.types;
  }

  disableCRUDMode() {
    this.createMode = false;
    this.editMode = false;
    this.deleteMode = false;
  }

  resetSelectedValue() {
    this.selectedExternalForm = null;
    //this.selectedOrganisation=null;
    this.selectedType = null;
    this.selectedFieldGroup = null;
    this.selectedCheckBox = true;
  }

  editForm(id) {
    this.router.navigateByUrl("form/edit/" + this.commonService.replaceHashStringByUnderScore(id));
  }

  cancel() {
    this.disableCRUDMode();
    this.resetSelectedValue();
    this.form = null;
  }

  setExternalFormMappingValue() {
    if (this.selectedType.enumValue == EnumFormType.CUSTOMFORMFIELD) {
      if (!this.selectedCheckBox) {
        this.externalFormData.customFormType = EnumFormType.CUSTOMFORMFIELD;
      }
      else {
        this.externalFormData.customFormType = EnumFormType.CUSTOMFORMFIELDNEW;
      }
    } else {
      this.externalFormData.customFormType = this.selectedType.enumValue;
    }
    this.externalFormData.customFormId = this.selectedExternalForm;
    if (this.selectedType.enumValue == EnumFormType.CUSTOMFORM ||
      this.selectedType.enumValue == EnumFormType.CUSTOMFORMFIELD) {
      this.externalFormData.organization =/*this.selectedOrganisation.orgName*/null;
      this.externalFormData.orgId = this.selectedOrganisation.orgId;
      this.externalFormData.tenantId = this.selectedOrganisation.tenantId;
    }
    if (this.selectedType.enumValue == EnumFormType.CUSTOMFORMFIELD) {
      this.externalFormData.customFieldGroupId = this.selectedFieldGroup.fieldGroupId;
      this.externalFormData.customFieldGroupName = this.selectedFieldGroup.fieldGroupName;
    }
  }

  ValidateSubmit() {
    if (this.selectedType) {
      if (this.selectedType.enumValue == EnumFormType.GENERAL)
        return true;
      if (this.selectedType.enumValue == EnumFormType.CUSTOMFORM &&
        this.selectedOrganisation) {
        return true;
      }
      if (this.selectedType.enumValue == EnumFormType.CUSTOMFORMFIELD &&
        this.selectedOrganisation && this.selectedFieldGroup) {
        return true;
      }
    }

    this.resetSelectedValue();
    this.message = notifications.empty_field_type;
    this.interval = (function (that) {
      return setTimeout(function () {
        that.message = "";
      }, 3000);
    })(this);
    return false;
  }

  submit() {
    if (this.interval) {
      clearTimeout(this.interval);
      this.message = "";
    }
    if (this.createMode) {

      this.disableCRUDMode();
      if (this.ValidateSubmit()) {
        this.setExternalFormMappingValue();
        this.formHttpService.createForm(this.form, this.externalFormData, this.appId)
          .subscribe(
            res => {
              if (res.status == 200) {
                this.form = new FormModel();
                this.resetSelectedValue();
                this.getForms(this.appId);
                this.message = notifications.save_successful;
                this.interval = (function (that) {
                  return setTimeout(function () {
                    that.message = "";
                  }, 3000);
                })(this);
              } else {
                this.message = notifications.form_save_unsuccessful;
              }
            },
            error => {
              this.message = notifications.form_save_unsuccessful;;
            });
      }

    }
  }

  sorting(columnHeader: string) {
    var appsTemp: FormModel[] = [];
    columnHeader = this.removeDoubleQuotes(columnHeader);
    var columnHeaderArr = [];
    //find which column to sort
    for (var j = 0; j < this.forms.length; j++) {
      var obj: FormModel = this.forms[j];
      switch (columnHeader) {
        case 'formName':
          columnHeaderArr.push(this.removeDoubleQuotes(obj.formName));
          break;
        case 'createdBy':
          columnHeaderArr.push(obj.createdBy);
          break;
        case 'formState':
          columnHeaderArr.push(obj.formState);
          break;
      }
    }
    /**
     * decide the ordering of sorting & also
     * find which column needs to be sorted
     * */
    var sortOrder = false;
    if (this.dataMap.has(columnHeader)) {
      sortOrder = this.dataMap.get(columnHeader);
      sortOrder = !sortOrder;
      this.dataMap.set(columnHeader, sortOrder);
    } else {
      this.dataMap.set(columnHeader, true);
      sortOrder = true;
    }

    columnHeaderArr.sort(this.dynamicSort(sortOrder));

    var columnMap = new Map<number, boolean>();
    var appMap = new Map<number, boolean>();

    for (var l = 0; l < columnHeaderArr.length; l++) {
      for (var m = 0; m < this.forms.length; m++) {
        if (!columnMap.has(l) && !appMap.has(m)) {
          var objj: FormModel = this.forms[m];
          switch (columnHeader) {
            case 'formName':
              if (columnHeaderArr[l] == (this.removeDoubleQuotes(objj.formName))) {
                appsTemp.push(objj);
                columnMap.set(l, true);
                appMap.set(m, true);
              }
              break;
            case 'createdBy':
              if (columnHeaderArr[l] == (this.removeDoubleQuotes(objj.createdBy))) {
                appsTemp.push(objj);
                columnMap.set(l, true);
                appMap.set(m, true);
              }
              break;
            case 'formState':
              if (columnHeaderArr[l] == (this.removeDoubleQuotes(objj.formState))) {
                appsTemp.push(objj);
                columnMap.set(l, true);
                appMap.set(m, true);
              }
              break;
          }
        }
      }
    }

    this.forms = [];
    this.forms = appsTemp;

  }

  dynamicSort(sortOrderFlag: boolean) {
    var sortOrder = 1; //ascending order
    if (sortOrderFlag) {
      sortOrder = 1;
    } else {
      sortOrder = -1; //descending order
    }
    return function (a, b) {
      return (('' + a).localeCompare(b)) * sortOrder;
    }
  }
  removeDoubleQuotes(stringData) {
    if (stringData == null || stringData == 'undefined' || stringData == '') {
      return undefined
    }
    return stringData.replace(/['"]+/g, '')
  }

  filterForms(event: any) {
    var searchTerm = event.currentTarget.value;
    if (searchTerm == "") {
      this.forms = this.allForms;
    } else {
      this.forms = this.allForms.filter(form => {
        return form.formName.toLowerCase().includes(searchTerm.toLowerCase());
      });
    }
  }
}

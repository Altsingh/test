import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule, FormControlName, FormGroup } from '@angular/forms';
import { DndModule } from 'ng2-dnd';

import { FormComponent } from "./form.component";
import { routings } from "./form.routing";
import { AltCommonModule } from "../alt-common/alt-common.module";
import { SettingsPanelComponent } from "./settings-panel/settings-panel.component";
import { FormCreatorComponent } from './form-creator/form-creator.component';
import { FormListComponent } from './form-list/form-list.component';

import { FormHttpService } from "./form-http.service";
import { DragService } from "./drag.service";
import { DefaultDataService } from "./default-data.service";
import { AltFormControlModelService } from "./form-control.model.service";

import { TextboxComponent } from "./components/textbox/textbox.component";
import { TextareaComponent } from "./components/textarea/textarea.component";
import { PanelComponent } from "./components/panel/panel.component";
import { TabbedPanelComponent } from "./components/tabbed-panel/tabbed-panel.component";
import { SelectComponent } from "./components/select/select.component";
import { MultiSelectComponent } from "./components/multi-select/multi-select.component";
import { CheckboxGroupComponent } from "./components/checkbox-group/checkbox-group.component";
import { RadioGroupComponent } from "./components/radio-group/radio-group.component";
import { DateComponent } from "./components/date/date.component";
import { ResizableModule } from 'angular-resizable-element';
import { ButtonComponent } from './components/button/button.component';
import { ClassDataService } from "./services/class-data/class-data.service";
import { ClassAttributesService } from "./services/class-data/class-attributes.service";
import { FormTypeService } from "./services/form-external/form-type.service";
import { FormOrganisationListService } from "./services/form-external/form-organisation-list.service";
import { OrganisationFormListService } from "./services/form-external/organisation-form-list.service";
import { FormFieldGroupsService } from "./services/form-external/form-field-groups.service";
import { FileComponent } from "./components/file/file.component";
import { CheckboxSingleComponent } from "./components/checkbox-single/checkbox-single.component";
import { TimeComponent } from "./components/time/time.component";
import { SliderComponent } from "./components/slider/slider.component";
import { ParaComponent } from "./components/para/para.component";
import { GlobelObjectDataService } from "./services/global-object-data/global-object-data.service";
import { DataTableComponent } from "./components/datatable/datatable.component";
import { RuleDataService } from "./services/rule/rule-data.service";
import { NestedAttributeService } from "./services/reference-attribute/nested-attribute.service";
import { BlankComponent } from './components/blank/blank.component';
import { ImageComponent } from './components/image/image.component';
import { LabelComponent } from './components/label/label.component';
import { NewFormComponent } from './new-form/new-form.component';
import { MatInputModule, MatAutocompleteModule, MatOptionModule, MatFormFieldModule, MatButtonModule, MatToolbarModule, MatTabsModule, MatSelectModule } from '../../../node_modules/@angular/material';
import { AltExcelComponent } from './components/alt-excel/alt-excel.component';
import { AltAttachmentsComponent } from './components/alt-attachments/alt-attachments.component';
import { AltCheckboxComponent } from './components/alt-checkbox/alt-checkbox.component';
import { AltLinkComponent } from './components/alt-link/alt-link.component';


@NgModule({
    imports: [
        CommonModule,
        AltCommonModule,
        FormsModule,
        ResizableModule,
        ReactiveFormsModule,
        MatInputModule,
        MatAutocompleteModule,
        MatOptionModule,
        MatFormFieldModule,
        MatButtonModule,
        MatToolbarModule,
        MatTabsModule,
        MatSelectModule,
        DndModule.forRoot(),
        routings
    ],
    declarations: [
        FormComponent,
        FormCreatorComponent,
        FormListComponent,
        SettingsPanelComponent,
        PanelComponent,
        TabbedPanelComponent,
        TextboxComponent,
        TextareaComponent,
        SelectComponent,
        MultiSelectComponent,
        CheckboxGroupComponent,
        RadioGroupComponent,
        DateComponent,
        ButtonComponent,
        ParaComponent,
        SliderComponent,
        TimeComponent,
        CheckboxSingleComponent,
        FileComponent,
        DataTableComponent,
        BlankComponent,
        ImageComponent,
        LabelComponent,
        NewFormComponent,
        AltExcelComponent,
        AltAttachmentsComponent,
        AltCheckboxComponent,
        AltLinkComponent
    ],
    providers: [
        FormHttpService,
        DragService,
        DefaultDataService,
        AltFormControlModelService,
        ClassDataService,
        ClassAttributesService,
        FormTypeService,
        FormOrganisationListService,
        OrganisationFormListService,
        FormFieldGroupsService,
        GlobelObjectDataService,
        RuleDataService,
        NestedAttributeService
    ],
    exports: [
    ]
})
export class FormModule { }

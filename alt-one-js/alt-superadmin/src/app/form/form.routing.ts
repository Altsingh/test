import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { FormComponent } from "./form.component";
import { FormCreatorComponent } from './form-creator/form-creator.component';
import { FormListComponent } from "./form-list/form-list.component";
import { NewFormComponent } from './new-form/new-form.component';

const routes: Routes = [
    {
        path: 'form', component: FormComponent,
        children: [
            { path: 'list', component: FormListComponent },
            { path: 'list/:appId', component: FormListComponent },
            { path: 'new/:appId', component: NewFormComponent },
            { path: 'edit/:id', component: FormCreatorComponent },
            { path: '', redirectTo: 'list', pathMatch: 'full' }
        ]
    }
]

export const routings: ModuleWithProviders = RouterModule.forChild(routes);
export class CommEnvlp{
    templateName: string
    commId:number;
    from:string;
    to:string;
    cc:string;
    bcc:string;
    subject:string;
    body:string;
    commType:string;
    action:string;
    successMessage:string;
    commEnvlpList:CommEnvlp[];
    selectMailIds:number[];
}


export enum CommunicationType{
    MAIL = "MAIL",
    JINNIE = "JINNIE"
}
import { AltComponent } from './alt-component';

export class AltAttachments extends AltComponent {
    totalFileSizeLimitMB: number;

    constructor(options) {
        super(options);
        this.totalFileSizeLimitMB = options.totalFileSizeLimitMB;
    }
}
import { AltComponent } from "./alt-component";
import { AltValidation } from "./alt-validation";
export class AltBase<T> extends AltComponent {
    dbClass: string;
    dbAttr: string;
    dbClassWrite:string;
    dbAttrWrite:string;
    hint: string;
    value: T;
    validation: AltValidation<T>[];
    required:boolean;

    constructor(options: {
        namespace?: string,
        id?: string,
        label?: string,
        name?: string,
        controlType?: string,
        dbClass?: string;
        dbAttr?: string;
        dbClassWrite?:string;
        dbAttrWrite?:string;
        hint?: string;
        value?: T;
        validation?:AltValidation<T>[];
    } = {}) {
        super(options);
        this.dbClass = options.dbClass;
        this.dbAttr = options.dbAttr;
        this.dbClassWrite=options.dbClassWrite;
        this.dbAttrWrite=options.dbAttrWrite;
        this.hint = options.hint;
        this.value = options.value;
        this.validation=options.validation;
        this.required=options['required'] || '';
    }
}
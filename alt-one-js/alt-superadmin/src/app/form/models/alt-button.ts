import { AltComponent } from "./alt-component";
import { Rule } from "../rule/rule.model";
export class AltButton extends AltComponent {
    componentIds: string[];
    ruleObjects:Rule[];
    commIdList:number[];
    rules:number[];
    events: string[];
    validationsOnly: boolean;

    constructor(options: {} = {}) {
        super(options);
        this.componentIds = options['componentIds'] || '';
        this.ruleObjects = options['ruleObjects'] || [];
        this.commIdList = options['commIdList'] || [];
        this.rules = options['rules'] || [];
        this.events = options['events'] || [];
        this.validationsOnly = options['validationsOnly'] || false;
    }
}

import { AltBase } from "./alt-base";
import { AltCheckboxSingle } from "./alt-checkbox-single";

export class AltCheckboxGroup extends AltBase<boolean> {
    options: AltCheckboxSingle[] = [];

    constructor(options: {} = {}) {
        super(options);
        this.options = options['options'] || [];
    }
}
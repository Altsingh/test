import { AltBase } from "./alt-base";

export class AltCheckboxSingle extends AltBase<boolean> {
    checked:boolean;

    constructor(options:{}={}){
        super(options);
    }
}
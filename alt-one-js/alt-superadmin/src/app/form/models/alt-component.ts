import { FilterBase } from "./filter/filter-base-pojo";
export class AltComponent {
    namespace: string;
    id: string;
    label: string;
    instruction: string;
    helpText: string;
    name: string;
    cssClass: string;
    controlType: string;
    relation: AltRelation[];
    securityType: EnumSecurityType;
    dbClassType: string;
    dataType: string;
    historyEnabled: boolean;
    listView: boolean;
    cardView: boolean;
    dbClassRead: string;
    dbAttrRead: string;
    dbClassName: string;
    /*filterField:string;
    filterSourceField:string
    
    filterSourceComponentId:string;
    filterSourceClassName:string;*/
    filters: FilterBase[];
    instanceOwner: boolean;
    matchOwnerId: string;

    constructor(options: {
        namespace?: string,
        id?: string,
        label?: string,
        instruction?: string,
        helpText?: string,
        name?: string,
        cssClass?: string,
        controlType?: string,
        relation?: AltRelation[],
        securityType?: EnumSecurityType,
        dbClassType?: string,
        historyEnabled?: boolean,
        cardView?: boolean,
        listView?: boolean,
        dataType?: string,

    } = {}) {
        this.namespace = options.namespace;
        this.id = options.id || '';
        this.label = options.label || '';
        this.instruction = options.instruction || '';
        this.helpText = options.helpText || '';
        this.name = options.name || '';
        this.cssClass = options.cssClass || '';
        this.controlType = options.controlType || '';
        this.relation = options.relation || [];
        this.securityType = options.securityType || EnumSecurityType.DENY;
        this.dbClassType = options.dbClassType || 'Select'
        this.dataType = options.dataType || 'STRING';
        this.historyEnabled = options.historyEnabled || false;
        this.listView = options.listView || true;
        this.cardView = options.cardView || false;

    }
}

export enum EnumSecurityType {
    DENY, MANDATORY, VIEWABLE
}

export enum EnumFieldDataType {
    INTEGER, DECIMAL, FLOAT, STRING
}
export class AltRelation {
    trigger: string;
    targetComponentList: TargetComponent[]
}
export class TargetComponent {
    targetComponent: string;
    events: AltEvent[]
}
export class AltEvent {
    action: string;
    webService: WebService;
}
export class WebService {
    type: WebServiceType;
    url: string;
    webServiceParam: WebServiceParamMetamap
}

export class WebServiceParamMetamap {
    [index: string]: string;
}

export class WebServiceType {

}

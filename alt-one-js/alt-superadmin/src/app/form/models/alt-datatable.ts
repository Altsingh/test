import { AltBase } from "./alt-base";

export class AltDataTable extends AltBase<string> {
    data:Map<string,object>;
    displayedColumns:string[];
    headers:string[];
    
    constructor(options) {
        super(options);
        this.data = options.data || null;
        this.displayedColumns = options.displayedColumns || [];
        this.headers = options.headers || [];
    }
}
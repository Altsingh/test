import { AltBase } from "./alt-base";
import { Rule } from '../rule/rule.model';

export class AltDate extends AltBase<string> {
    placeholder: string;
    inputType: string;
    ruleObjects:Rule[];
    rules:number[];
    events: string[];

    constructor(options:{}={}){
        super(options);
        this.inputType = options['inputType'] || '';
        this.placeholder = options['placeholder'] || '';
        this.ruleObjects = options['ruleObjects'] || [];
        this.rules = options['rules'] || [];
        this.events = options['events'] || [];
    }
}
import { AltBase } from "./alt-base";
import { Rule } from '../rule/rule.model';

export class AltDropdown extends AltBase<string> {
    options: any[];
    ruleObjects:Rule[];
    rules:number[];
    events: string[];

    constructor(options) {
        super(options);
        this.options = options['options'] || [];
        this.ruleObjects = options['ruleObjects'] || [];
        this.rules = options['rules'] || [];
        this.events = options['events'] || [];
    }
}
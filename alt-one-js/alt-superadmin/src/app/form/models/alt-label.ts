import { AltBase } from "./alt-base";

export class AltLabel extends AltBase<string> {
    placeholder: string;
    inputType: string;
    
    constructor(options:{}={}){
        super(options);
        this.inputType = options['inputType'] || '';
        this.placeholder = options['placeholder'] || '';
    }
}
import { AltBase } from "./alt-base";
import { AltComponent } from "./alt-component";
import { AltButton } from "./alt-button";

export class AltPanel extends AltBase<string> {
    componentList: AltComponent[];
    buttonList: AltButton;

    constructor(options) {
        super(options);
        this.componentList = options.componentList || [];
        this.buttonList = options.buttonList || [];
    }
}
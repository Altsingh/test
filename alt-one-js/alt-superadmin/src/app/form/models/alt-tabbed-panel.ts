import { AltBase } from "./alt-base";
import { AltComponent } from "./alt-component";
import { AltButton } from "./alt-button";

export class AltTabbedPanel extends AltBase<string> {
    panels: AltComponent[];
    activePanelIndex: number;

    constructor(options) {
        super(options);
        this.panels = options.panels || [];
        this.activePanelIndex = options.activePanelIndex || 0;
    }
}
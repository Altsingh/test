import { AltBase } from "./alt-base";
import { Rule } from '../rule/rule.model';

export class AltTextbox extends AltBase<string> {
    inputType: string;
    placeholder: string;
    events:string[];
    ruleObjects: Rule[];
    rules:number[];
    
    constructor(options:{}={}){
        super(options);
        this.inputType = options['inputType'] || '';
        this.events = options['events'] || [];
        this.placeholder = options['placeholder'] || '';
        this.ruleObjects = options['ruleObjects'] || [];
        this.rules = options['rules'] || [];
    }
}
export class AltValidation<T>{
   value: T;
   errorMessage:string;

   constructor(options:{
       value?: T,
       errorMessage?:string
   }){
       this.value=options.value;
        this.errorMessage=options.errorMessage;
   } 
}
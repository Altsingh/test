export class SimpleCheckboxGroup{
    options: SimpleCheckbox[] = [];
}
export class SimpleCheckbox{
    value:string;
    checked:boolean;
    label:string;

    constructor(value:string,checked:boolean) {
        this.value=value;
        this.checked=checked;
        this.label=value;
    }
}
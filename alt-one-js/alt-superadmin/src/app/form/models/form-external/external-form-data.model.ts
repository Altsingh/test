import { EnumFormType } from "../../enum/external-form-enum/enum-form-type";

export class ExternalFormData {
  customFormType: EnumFormType;
  organization: string;
  orgId: string;
  customFormId: string;
  customFieldGroupId: string;
  customFieldGroupName: string;
  tenantId:string;

  constructor() {
    this.customFieldGroupId = "";
    this.customFieldGroupName = "";
    this.customFormId = "";
    this.customFormType = null;
    this.organization = "";
    this.tenantId="";
    this.orgId="";
  }

}

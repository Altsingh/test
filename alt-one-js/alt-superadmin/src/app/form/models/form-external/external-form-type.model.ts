import { EnumFormType } from "../../enum/external-form-enum/enum-form-type";

export class ExternelFormType{
  type: String;
  enumValue: EnumFormType;
  constructor(){
    this.type="";
    this.enumValue=null;
  }
}

export class GlobalObjectModel{
    public sysObjectID: string;
    public objectName: string;
    public objectCode:string;
    public objAttr:GlobalAttributeModel[];

    constructor(){
        this.sysObjectID="";
        this.objectName="";
        this.objectCode="";
    }
}

export class GlobalAttributeModel{
    public sysObjectAttrID:string;
    public objectAttrName:string;
    public objectAttrCode:string;
    constructor(){
        this.sysObjectAttrID="";
        this.objectAttrName="";
        this.objectAttrCode="";
    }

}
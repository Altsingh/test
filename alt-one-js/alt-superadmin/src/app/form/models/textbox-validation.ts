import { AltValidation } from "./alt-validation";
import { EnumTextBoxValidation } from "./enum-textbox-validation";

export class TextBoxValidation extends AltValidation<string> {
    validation: EnumTextBoxValidation; 
    constructor(options:{} = {}){
        super(options);
        this.validation = options['validation'] || '';
    }
}
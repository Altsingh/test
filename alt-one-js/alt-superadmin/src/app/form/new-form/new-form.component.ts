import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormHttpService } from '../form-http.service';
import { FormModel } from '../pojo/form.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ExternalFormData } from '../models/form-external/external-form-data.model';
import { EnumFormType } from '../enum/external-form-enum/enum-form-type';
import { Organisation } from '../models/form-external/organisation.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-new-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.css']
})
export class NewFormComponent implements OnInit {

  form: FormModel;
  externalFormData: ExternalFormData;
  formType: number;
  forms: FormModel[];
  appId: number;
  selectedOrganisation: Organisation;
  message: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private formHttpService: FormHttpService,
    private location: Location,
    private router: Router,
  ) {
    this.selectedOrganisation = this.formHttpService.getLoggedInOrganizationDetail();
    this.form = new FormModel();
    this.externalFormData = new ExternalFormData();
    this.externalFormData.orgId = this.selectedOrganisation.orgId;
    this.externalFormData.tenantId = this.selectedOrganisation.tenantId;
    this.externalFormData.customFormType = EnumFormType.CUSTOMFORM;
    this.formType = 1;
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params['appId']) {
        this.appId = params['appId'];
      }
    });
    this.formHttpService.getPublishedForms(-1, -1, this.appId).subscribe((res: any) => {
      this.forms = res.body;
    });
  }

  goBack() {
    this.location.back();
  }

  enterFormName(event: any) {
    this.form.formName = event.currentTarget.value.trim();
  }

  selectParentForm(event: any) {
    this.form.parentFormId = event.currentTarget.value;
  }

  changeType(event: any) {
    if (this.formType != event.currentTarget.value)
      this.message = undefined;
    this.formType = event.currentTarget.value;
    if (this.formType == 1) {
      this.form.parentFormId = undefined;
    }
  }

  checkSubmit(): boolean {
    if (!this.form.formName)
      return true;
    return this.form.formName.length == 0;
  }

  submit() {

    var validated: boolean = this.validate();
    if (!validated)
      return;

    this.formHttpService.createForm(this.form, this.externalFormData, this.appId).subscribe(
      (res: any) => {
        this.router.navigateByUrl("form/edit/" + res.body.formId);
      },
      (error: HttpErrorResponse) => {
        this.message = 'form creation failed';
      });
  }

  validate(): boolean {
    if (this.formType == 2 && this.form.parentFormId == undefined) {
      this.message = "Please select parent form";
      return false;
    }

    return true;
  }
}

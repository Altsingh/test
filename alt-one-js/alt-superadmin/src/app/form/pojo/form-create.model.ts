import { ExternalFormData } from "../models/form-external/external-form-data.model";

export class FormCreateModel {
    status: string;
    type: string;
    orgCode: string;
    uiClassName: string;
    customComponentAllowed: boolean;
    moduleCode: string;
    description: string;
    componentList: any[];
    bundleId: string;
    componentCount: number;
    formStatus: string;
    formMapping: ExternalFormData;
    appId: Number;
    parentFormId: number;

    constructor(name, externalFormData, appId, parentFormId) {
        this.status = "ACTIVE";
        this.type = "SYSTEM";
        this.orgCode = "12132";
        this.uiClassName = name;
        this.customComponentAllowed = false;
        this.moduleCode = "Y";
        this.description = "FORM A";
        this.componentList = [];
        this.bundleId = "131";
        this.componentCount = 0;
        this.formStatus = "DRAFT";
        this.formMapping = externalFormData;
        this.appId = appId;
        this.parentFormId = parentFormId;
    }
}

import { AltComponent } from "../models/alt-component";
export class FormEntityModel {
    status: string;
    type: string;
    orgCode: string;
    uiClassName: string;
    uiClassCode: string;
    customComponentAllowed: boolean;
    moduleCode: string;
    description: string;
    formUpdateDetails: FormUpdateDetails[];
    componentList: AltComponent[];
    cas: string;
    readLock: boolean;
    versionId: string;
    componentCounter: number;
    formState: string;
    appid: number;
    metaclassid: number;
    hrFormId: number;
    parentFormId: number;
    taskCodeEnabled: boolean;
}
export class FormUpdateDetails {
    componentOperation: string;
    componentUpdateDetailsList: string;
}
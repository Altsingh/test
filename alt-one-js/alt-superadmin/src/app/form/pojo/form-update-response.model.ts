export class FormUpdateResponseModel {
    success: boolean;
    responseMessage: string;
    cas: string;
    formId: string;

    constructor() {
        this.success = false;
        this.responseMessage = null;
        this.cas = null;
        this.formId = null;
    }
}
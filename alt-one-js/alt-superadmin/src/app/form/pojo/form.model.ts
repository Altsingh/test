export class FormModel {
    formName: string;
    status: string;
    createdBy: string;
    uiClassCode: string;
    formState: string;
    parentFormId: number;

    constructor() {
        this.formName = "";
        this.status = null;
        this.createdBy = null;
        this.uiClassCode = null;
        this.formState = "DRAFT";
    }
}
import { Injectable } from '@angular/core';
import { DataService } from "../data.service";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable()
export class ClassAttributesService extends DataService {

  constructor(http: HttpClient) {
    super(environment.formUrl + '/' + 'classes', http)
  }

  nestedAttributes(id): any {
    return this.getFromUrl(environment.formUrl + '/masterdata/classes/' + id + '/nested-attributes')
  }

}

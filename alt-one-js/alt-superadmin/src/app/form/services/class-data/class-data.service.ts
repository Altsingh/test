import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { DataService } from "../data.service";


@Injectable()
export class ClassDataService extends DataService {

  constructor(http: HttpClient) {
    super(environment.formUrl + '/' + 'classes?id=true&&name=true', http)
  }

  public getAppClasses(appId: number) {
    return this.http.get(this.url + '&appId=' + appId, this.options);
  }
}

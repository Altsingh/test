import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable()
export class DataService {

  public options: any;
  public byPassOptions: any;

  constructor(public url: string, public http: HttpClient) {
    this.options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      })
    }

    this.byPassOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    }

  }

  getAll(): any {
    return this.http.get(this.url, this.options);
  }

  get(id: string, parameters: string): any {
    return this.http.get(this.url + '/' + id + parameters, this.options);
  }

  getFromUrl(url) {
    return this.http.get(url, this.options);
  }

  getWithPathParam(parameters: string): any {
    return this.http.get(this.url + parameters, this.options);
  }

  create(resource) {
    return this.http.post(this.url, resource, { observe: 'response' })
  }

  update(resource) {
    return this.http.put(this.url, resource);
  }

  delete(id: string) {
    return this.http.delete(this.url + '/' + id);
  }
}

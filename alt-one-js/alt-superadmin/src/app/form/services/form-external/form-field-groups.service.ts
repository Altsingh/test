import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class FormFieldGroupsService extends DataService {

  constructor(http: HttpClient) {
    super(environment.formFieldGroupList, http);
  }

  getAllFormGroup(formId: String, orgId: String, tenantId: String): any {
    return this.http.get(environment.formFieldGroupList + "?formId=" + formId + "&orgId=" + orgId + "&tenantId=" + tenantId, this.options);
  }

}
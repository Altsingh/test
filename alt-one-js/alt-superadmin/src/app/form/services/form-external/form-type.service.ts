import { Injectable } from '@angular/core';
import { ExternelFormType } from '../../models/form-external/external-form-type.model';
import { EnumFormType } from '../../enum/external-form-enum/enum-form-type';

@Injectable()
export class FormTypeService {

  type1 :ExternelFormType=new ExternelFormType();
  type2 :ExternelFormType= new ExternelFormType();
  type3 :ExternelFormType=new ExternelFormType();
  constructor() {
    this.type1.enumValue=EnumFormType.GENERAL;
    this.type1.type="General";
    this.type2.enumValue=EnumFormType.CUSTOMFORM;
    this.type2.type="Org Custom Form";
    this.type3.enumValue=EnumFormType.CUSTOMFORMFIELD;
    this.type3.type="Org Custom Field";
   }


  types : ExternelFormType[]=[this.type1,this.type2,this.type3];
}

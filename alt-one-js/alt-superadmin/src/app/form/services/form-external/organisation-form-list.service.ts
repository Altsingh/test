import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable()
export class OrganisationFormListService extends DataService {

  constructor(http: HttpClient) {
    super(environment.organisationFormList, http);
  }

  getAllForms(orgId: String, tenantId: String): any {
    return this.http.get(environment.organisationFormList + "?orgId=" + orgId + "&tenantId=" + tenantId, this.byPassOptions);
  }
}

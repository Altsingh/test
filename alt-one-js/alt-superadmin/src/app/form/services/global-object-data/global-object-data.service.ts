import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { DataService } from "../data.service";


@Injectable()
export class GlobelObjectDataService extends DataService {

  constructor(http: HttpClient) {
    super(environment.globalObjectUrl, http);
  }

}
import { TestBed, inject } from '@angular/core/testing';

import { NestedAttributeService } from './nested-attribute.service';

describe('NestedAttributeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NestedAttributeService]
    });
  });

  it('should be created', inject([NestedAttributeService], (service: NestedAttributeService) => {
    expect(service).toBeTruthy();
  }));
});

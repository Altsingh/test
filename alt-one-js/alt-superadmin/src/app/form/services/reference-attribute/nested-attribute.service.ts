import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { DataService } from '../data.service';

@Injectable()
export class NestedAttributeService extends DataService {

  constructor(http: HttpClient) {
    super(environment.formUrl + '/masterdata/classes/getRefereceAttributes', http)
  }

  getNestedAttributes(body) {
    return this.http.post(this.url, body, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

}

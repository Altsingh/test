import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { DataService } from '../data.service';

@Injectable()
export class RuleDataService extends DataService {

  constructor(http: HttpClient) {
    super(environment.ruleUrl + '/' + 'rule', http)
  }

  deleteRule(request: any) {
    return this.http.post(this.url + '/delete', request, { observe: 'response' });
  }

}

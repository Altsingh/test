import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from "@angular/core";
import { AltCommonService } from "../../alt-common/alt-common.service";
import { ClassDataService } from "../services/class-data/class-data.service";
import { ClassIdNameModel } from "../models/class-data-models/class-id-name.model";
import { ClassAttributesService } from "../services/class-data/class-attributes.service";
import { ClassAttributesModel } from "../models/class-data-models/class-attributes.model";
import { GlobelObjectDataService } from "../services/global-object-data/global-object-data.service";
import { GlobalObjectModel } from "../models/global-object-model/global-object.model";
import { FormHttpService } from "../form-http.service";
import { FormModel } from "../pojo/form.model";
import { SimpleCheckbox } from "../models/controls/simple-checkbox.model";
import { AltComponent } from "../models/alt-component";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { CommEnvlp } from '../mail/mail.model';

@Component({
    selector: 'settings-panel',
    templateUrl: './settings-panel.component.html'
})
export class SettingsPanelComponent implements OnInit, OnChanges {
    @Output() addRuleEvent = new EventEmitter();
    @Output() editRuleEvent = new EventEmitter();
    @Output() deleteRuleEvent = new EventEmitter();
    @Output() addMailEvent = new EventEmitter();
    @Output() editMailEvent = new EventEmitter();
    @Output() deleteMailEvent = new EventEmitter();
    @Output() enableTaskode = new EventEmitter();
    @Input() selectedComponent;
    @Input() uiClassCode;
    @Input() formEntityModel;
    @Input() editMode;
    @Input() appId: number;
    @Input() commListOp: CommEnvlp[]

    //@Input() formEntityModel;
    globalObjects: GlobalObjectModel[];
    classes: ClassIdNameModel[];
    localClasses: ClassIdNameModel[];
    nestedAttributesRead: Array<string>;
    attributesCheckBox: SimpleCheckbox[];
    classAttributesRead: ClassAttributesModel;
    filterSourceAttributes: ClassAttributesModel;
    classAttributesWrite: ClassAttributesModel;
    showInputControls: boolean = true;
    showQuickPresets: boolean = true;
    showControls: boolean = true;
    showSettings: boolean = false;
    frontendTab: boolean = true;
    ruleTab: boolean = false;
    filterTab: boolean = false;
    mailTab: boolean = false;
    optionsCount: number = 0;
    showOption: boolean = false;
    showAddValue: boolean = true;
    options: string[] = new Array(0);
    showValues: boolean = true;
    forms: FormModel[];
    taskCodeSwitch: boolean = false;
    disableTaskCodeSwitch: boolean = false;
    //formEntityModel: FormEntityModel;
    formComponents: AltComponent[];
    flatHtmlComponents: AltComponent[];
    filterSourceComponentSelected: AltComponent;
    selectedFilterSourceComponent: boolean;

    panel: Object = { component: 'PANEL' };
    tabPanel: Object = { component: 'TAB_PANEL' };
    table: Object = { component: 'TABLE' };
    card: Object = { component: 'CARD' };
    accordian: Object = { component: 'ACCORDIAN' };
    chart: Object = { component: 'CHART' };
    textField: Object = { component: 'TEXTFIELD' };
    dropDown: Object = { component: 'DROPDOWN' };
    multiSelect: Object = { component: 'MULTI_SELECT' };
    checkboxGroup: Object = { component: 'CHECKBOX_GROUP' };
    date: Object = { component: 'DATE' };
    textarea: Object = { component: 'TEXTAREA' };
    radioGroup: Object = { component: 'RADIO_GROUP' };
    file: Object = { component: 'FILE' };
    button: Object = { component: 'BUTTON' };
    note: Object = { component: 'NOTE' };
    blank: Object = { component: 'BLANK' };
    image: Object = { component: 'IMAGE' };
    label: Object = { component: 'LABEL' };
    labelMap: Map<string, string> = new Map<string, string>();

    constructor(
        private commonService: AltCommonService,
        private classDataService: ClassDataService,
        private classAttributesService: ClassAttributesService,
        private globelObjectDataService: GlobelObjectDataService,
        private formHttpService: FormHttpService,
        private activatedRoute: ActivatedRoute,
        private router: Router, ) { }

    ngOnInit() {
        this.getFormListForDatatableAddEdit();
        this.getClasses();
        this.getGlobalClasses();

        if (this.formEntityModel && this.formEntityModel.taskCodeRequired) {
            this.taskCodeSwitch = this.formEntityModel.taskCodeRequired;
        }

        if (this.formEntityModel.metaclassid) {
            this.disableTaskCodeSwitch = true;
        } else {
            this.disableTaskCodeSwitch = false;
        }
        if (this.formEntityModel.componentList) {
            this.flattenHtmlComponents(this.formEntityModel.componentList);
        }
    }


    getTaskCodeSwitch() {
        return this.disableTaskCodeSwitch;
    }

    getDbClassName(classId: string) {
        for (let class1 of this.localClasses) {
            if (class1.id == classId) {
                return class1.name;
            }
        }
    }
    updateFormComponentsForFilter() {
        this.mailTab = false;
        console.log('uiClassCode ' + this.uiClassCode)
        //this.getFormDetails(true);
        this.fetchSourceComponentsForFilter(true);

    }

    activateMailTabData() {
        this.mailTab = true;
    }

    aa(id: string) {
    }

    updateFormComponentLabel() {
        for (let comp of this.formComponents) {
            if (this.selectedComponent.id == comp.id) {
                comp.label = this.selectedComponent.label;
                this.labelMap.set(this.selectedComponent.id, comp.label);
                console.log('updated form comp label for filter');
            }
        }

    }

    fetchSourceComponentsForFilter(isUpdateForFilter: boolean) {
        let comps = new Array();
        if (this.formEntityModel.componentList) {
            console.log('this.formEntityModel.componentList.length ' + this.formEntityModel.componentList.length);
            for (let comp of this.formEntityModel.componentList) {
                if ('TEXTFIELD' == comp.controlType || 'DROPDOWN' == comp.controlType || 'DATE' == comp.controlType) {
                    if (isUpdateForFilter && this.labelMap.get(comp.id)) {
                        comp.label = this.labelMap.get(comp.id);
                        console.log('updated comp label');
                    }
                    comp.dbClassName = this.getDbClassName(comp.dbClassRead);
                    comps.push(comp);
                }
            }
        }
        this.formComponents = comps;
        if (isUpdateForFilter) {
            console.log('inside updatae for filter');
            this.frontendTab = false; this.ruleTab = false; this.filterTab = true; this.mailTab = false;
            this.selectedComponent.filters[0].joiner = "=";
            if (this.selectedComponent.filters && this.selectedComponent.filters[0].filterSourceComponentId) {
                console.log('fetchin source components');

                this.onSelectedFilterSourceComp(this.selectedComponent.filters[0].filterSourceComponentId);
            }
        }
    }

    getFormDetails(isUpdateForFilter: boolean) {
        console.log('this.uiClassCode ' + this.uiClassCode);
        console.log('this.formEntityModel ' + this.formEntityModel);

        this.formHttpService.getFormDetails(this.commonService.replaceHashStringByUnderScore(this.uiClassCode))
            .subscribe(res => {
                console.log('this.formEntityModel ' + this.formEntityModel);
                this.formEntityModel = res;
                this.fetchSourceComponentsForFilter(isUpdateForFilter);
            }
            )
    }

    ngOnChanges() {
        console.log("*************************** editmode : " + this.editMode)
        if (this.selectedComponent) {
            if (this.selectedComponent.controlType == 'DATATABLE' && this.attributesCheckBox) {
                this.getAttributesForRead(this.selectedComponent.dbClassRead);
                this.getFormListForDatatableAddEdit();
            } else if (this.selectedComponent.controlType == 'DATATABLE') {
                /**
                 * dbClassRead is equivalent to form back binded
                 * classes. 
                 * "this.selectedComponent.dbClassType" -> "Local"
                 * In settings panel under backend class dropdown was coming blank
                */
                this.selectedComponent.dbClassType = 'Local'
                this.getClassesForRead(this.selectedComponent.dbClassType);
            } else if (this.selectedComponent.controlType == 'TEXTFIELD') {
                this.selectedComponent.dbClassType = 'Global'
                this.getClassesForRead(this.selectedComponent.dbClassType);
            }
            this.showSettings = true;
            this.showControls = false;
            if (this.selectedComponent.controlType == 'PANEL' || this.selectedComponent.controlType == 'TAB_PANEL') {
                this.frontendTab = true;
            }
            this.showValues = true;
            this.options = this.selectedComponent.options;
            if (this.options && this.options.length == 0) {
                this.showAddValue = true;
                this.showOption = false;
            } else {
                this.showOption = true;
                this.showAddValue = false;
            }



        }
        if (this.formEntityModel.componentList) {
            this.flattenHtmlComponents(this.formEntityModel.componentList);
        }
    }

    updateFormComponentsForBackend() {
        console.log('inside updateFormComponentsForBackend');
        this.getClassesForRead(this.selectedComponent.dbClassType);
        this.frontendTab = false; this.ruleTab = false; this.filterTab = false; this.mailTab = false;

    }

    tableListingControl(view: string) {
        if (this.selectedComponent.controlType == 'DATATABLE') {
            if (view == '1') {
                this.selectedComponent.cardView = true;
                this.selectedComponent.listView = false;
            } else {
                this.selectedComponent.cardView = false;
                this.selectedComponent.listView = true;
            }
        }
    }
    addValueToArr(event: any, option: any, i: number) {
        this.options.splice(i, 1);
        this.options.push(event.currentTarget.value)
    }

    taskCodeRequired() {
        if (this.taskCodeSwitch) {
            this.taskCodeSwitch = false;
        } else {
            this.taskCodeSwitch = true;
        }
        console.log("taskCodeRequired : " + this.taskCodeSwitch);
        this.enableTaskode.emit(this.taskCodeSwitch);
    }

    createNewMail(button) {
        this.addMailEvent.emit(button);
    }

    editMail(mail) {
        this.editMailEvent.emit(mail);
    }

    deleteMail(mail) {
        this.deleteMailEvent.emit(mail);
    }


    createRule(button) {
        this.addRuleEvent.emit(button);
    }

    editRule(rule) {
        this.editRuleEvent.emit(rule);
    }

    deleteRule(rule) {
        this.deleteRuleEvent.emit(rule);
    }

    getClasses() {
        this.classDataService.getAppClasses(this.appId)
            .subscribe(
                (res: any) => {
                    this.localClasses = res;
                    this.activatedRoute.params.subscribe((params: Params) => {
                        if (params['id']) {
                            this.uiClassCode = this.commonService.replaceUnderScoreByHash(params['id']);
                            //this.getFormDetails(false);
                        }
                    });
                }
            )
        if (this.selectedComponent && this.selectedComponent.dbClassRead) {
            this.getAttributesForRead(this.selectedComponent.dbClassRead);
            this.showValues = false;
        }
        if (this.selectedComponent && this.selectedComponent.dbClassWrite) {
            this.getAttributesForWrite(this.selectedComponent.dbClassWrite);
        }
    }

    getClassesForRead(classType: string) {
        if (classType == 'Global') {
            this.classes = [];
            this.globalObjects.forEach(element => {
                let classIdNameModel: ClassIdNameModel = new ClassIdNameModel();
                classIdNameModel.id = element.objectCode;
                classIdNameModel.name = element.objectName;
                this.classes.push(classIdNameModel);
            });
        } else if (classType == 'Local') {
            this.classes = this.localClasses;
        } else {
            this.classes = [];
            if (this.classAttributesRead) {
                this.classAttributesRead.attributes = [];
            }
        }
        if (this.selectedComponent && this.selectedComponent.dbClassRead) {
            this.getAttributesForRead(this.selectedComponent.dbClassRead);
        }

    }

    getGlobalClasses() {
        var orgId = atob(localStorage.getItem("realm"));
        this.globelObjectDataService.getWithPathParam(orgId)
            .subscribe(res => {
                this.globalObjects = res;
            })
        if (this.selectedComponent && this.selectedComponent.dbClassRead) {
            this.getAttributesForRead(this.selectedComponent.dbClassRead);
        }
    }

    flattenHtmlComponents(htmlComponents: AltComponent[]) {
        var flatHtmlComponents = [];
        for (let comp of htmlComponents) {
            if (comp.controlType == 'PANEL') {
                var panel = comp as any;
                panel.componentList.forEach(element => {
                    flatHtmlComponents.push(element);
                });

            } else {
                flatHtmlComponents.push(comp);
            }
        }
        this.flatHtmlComponents = flatHtmlComponents;
    }

    gettestdatatype() {
        console.log(this.selectedComponent.dataType);
    }
    onSelectedFilterSourceComp(componentId: string) {
        if (this.selectedComponent.filters[0].filterSourceComponentId == 'null') {
            this.filterSourceComponentSelected = null;
            this.filterSourceAttributes = null;
            this.selectedComponent.filters[0].filterSourceField = null;
        } else {
            console.log('componentId here ' + this.selectedComponent.filters[0].filterSourceComponentId);
            for (let comp of this.flatHtmlComponents) {
                if (comp.id == componentId) {
                    this.filterSourceComponentSelected = comp;
                    this.getAttributesForSourceFilterComponent();
                    this.selectedFilterSourceComponent = true;
                }
            }
        }
    }
    getAttributesForRead(id: string) {
        if (this.selectedComponent.dbClassType == 'Local') {
            this.classAttributesService.get(this.commonService.replaceHashStringByUnderScore(id), '?attributes=true')
                .subscribe(res => {
                    this.classAttributesRead = res;
                }
                )
        } else if (this.selectedComponent.dbClassType == 'Global') {
            this.classAttributesRead = new ClassAttributesModel();
            this.globalObjects
                .find(x => x.objectCode == this.selectedComponent.dbClassRead)
                .objAttr.forEach(element => {
                    this.classAttributesRead.attributes.push(element.objectAttrName)
                });

        }
        if (this.selectedComponent.controlType == 'DATATABLE') {
            this.getNestedAttributes(id);
        }
    }


    getAttributesForSourceFilterComponent() {
        console.log('this.filterSourceComponentSelected.dbClassType' + this.filterSourceComponentSelected.dbClassType);
        if (this.filterSourceComponentSelected.dbClassType == 'Local') {
            let id: string = this.filterSourceComponentSelected.dbClassRead;
            this.classAttributesService.get(this.commonService.replaceHashStringByUnderScore(id), '?attributes=true')
                .subscribe(res => {
                    this.filterSourceAttributes = res;
                    if (this.filterSourceAttributes && this.filterSourceAttributes.attributes) {
                        console.log('setting value for filter source field');
                        //this.selectedComponent.filters[0].filterSourceField=this.filterSourceAttributes.attributes[0];
                    }
                }
                )
        } else if (this.filterSourceComponentSelected.dbClassType == 'Global') {
            this.filterSourceAttributes = new ClassAttributesModel();
            let attr = this.globalObjects
                .find(x => x.objectCode == this.filterSourceComponentSelected.dbClassRead)
            if (attr) {
                attr.objAttr.forEach(element => {
                    this.filterSourceAttributes.attributes.push(element.objectAttrName)
                });
            }

        }
    }

    setDisplayedColumn(event) {
        this.selectedComponent.displayedColumns = this.attributesCheckBox.filter(attribute => attribute.checked)
            .map(attribute => attribute.value);
        this.selectedComponent.headers = this.attributesCheckBox.filter(attribute => attribute.checked)
            .map(attribute => attribute.label);
    }

    moveUp(index) {
        if (index > 0) {
            var temp = this.attributesCheckBox[index];
            this.attributesCheckBox[index] = this.attributesCheckBox[index - 1];
            this.attributesCheckBox[index - 1] = temp;
        }
        this.setDisplayedColumn(event);
    }

    moveDown(index) {
        if (this.attributesCheckBox[index + 1]) {
            var temp = this.attributesCheckBox[index];
            this.attributesCheckBox[index] = this.attributesCheckBox[index + 1];
            this.attributesCheckBox[index + 1] = temp;
        }
        this.setDisplayedColumn(event);

    }

    getFormListForDatatableAddEdit() {
        this.formHttpService.getPublishedForms(-1, -1, this.appId)
            .subscribe(res => {
                this.forms = res.body;
            })
    }

    getAttributesForWrite(id: string) {
        this.classAttributesService.get(this.commonService.replaceHashStringByUnderScore(id), '?attributes=true')
            .subscribe(res => {
                this.classAttributesWrite = res;
            }
            )

    }

    addValue() {
        if (this.options == null) {
            this.options = new Array(0);
        }
        this.optionsCount++;
        this.options.push("");
        this.showOption = true;
        this.showAddValue = false;
        this.selectedComponent.options = this.options;
    }

    removeValue(index) {
        this.optionsCount--;
        this.options.splice(index, 1);
        if (this.optionsCount == 0) {
            this.showAddValue = true;
            this.showOption = false;
        }
        this.selectedComponent.options = this.options;
    }

    trackByFn(index: any, item: any) {
        return index;
    }

    selectInstanceOwner(event: any) {
        var selectedInstanceOwner = event.currentTarget.value;
        for (let comp of this.formEntityModel.componentList) {
            if (comp.id == selectedInstanceOwner) {
                comp.instanceOwnerFlag = true;
            } else {
                comp.instanceOwnerFlag = false;
            }
        }
    }

    checkOwnerSelection(component: any): boolean {
        return component.instanceOwnerFlag;
    }

    selectMatchOwner(component: any, event: any) {
        component.matchOwnerId = event.currentTarget.value;
    }

    checkMatchSelection(selectedComponent: any, optionComponent: any): boolean {
        return selectedComponent.matchOwnerId == optionComponent.id;
    }

    selectAddEditForm(component: any, event: any) {
        component.formPopup = event.currentTarget.value;
    }

    getNestedAttributes(id: string) {
        if (!id) {
            this.attributesCheckBox = undefined;
            return;
        }
        this.classAttributesService.nestedAttributes(this.commonService.replaceHashStringByUnderScore(id))
            .subscribe(
                res => {
                    this.nestedAttributesRead = res;
                    this.attributesCheckBox = this.nestedAttributesRead.map(attribute => new SimpleCheckbox(attribute, true));

                    if (this.selectedComponent.displayedColumns.some(column => this.nestedAttributesRead.includes(column))) {
                        var attributes = new Array<SimpleCheckbox>();
                        this.attributesCheckBox.forEach(element => {
                            var index = this.selectedComponent.displayedColumns.indexOf(element.value);
                            var attributeCheck = null;
                            if (index >= 0) {
                                attributeCheck = element;
                                attributeCheck.label = this.selectedComponent.headers[index];
                            } else {
                                attributeCheck = new SimpleCheckbox(element.value, false);
                            }
                            attributes.push(attributeCheck);
                        });
                        this.attributesCheckBox = attributes;
                    } else {
                        this.selectedComponent.displayedColumns = this.nestedAttributesRead;
                        this.selectedComponent.headers = this.nestedAttributesRead;
                    }
                }
            )
    }

    enableInstanceSpecificHistory() {
        this.selectedComponent.instanceSpecificHistory = this.selectedComponent.historyEnabled;
    }
}

import { Component, OnInit } from '@angular/core';
import { ApplicationHttpService } from '../alt-application/application-http.service';
import { ApplicationModel } from '../alt-application/pojo/application.model';
import { environment } from "src/environments/environment";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  apps: ApplicationModel[];
  accessData: any;
  app: any;
  constructor(private applicationHttpService: ApplicationHttpService,
    private httpService: ApplicationHttpService) { }

  ngOnInit() {
    this.applicationHttpService.getApps().subscribe((res: ApplicationModel[]) => {
      this.apps = res;
    });
  }

  photoUrl(app: any) {
    if(app.appLogoUrl==null)
      return '../assets/images/default.png';
    else
      return this.removeDoubleQuotes(app.appLogoUrl);
  }

  getPreview(app: any) {
    var keycloakUrl = environment.keycloak_url;
    var realm = atob(localStorage.getItem("realm"));
    var refreshToken = localStorage.getItem("refreshToken");
    var tokenUrl = keycloakUrl + 'auth/realms/' + realm + '/protocol/openid-connect/token';
    this.httpService.getPreviewAccessToken(tokenUrl, realm, refreshToken)
      .subscribe(res => {
        this.accessData = res;
        var openUrl = environment.appPage + "securelogin/" + this.removeDoubleQuotes(app.appCode) + "?accessToken=" + this.accessData.access_token +
          "&refreshToken=" + this.accessData.refresh_token + "&isCustom=false&orgId=" + realm + "&tenantId=" + atob(localStorage.getItem("tenantId"));
        window.open(openUrl);
      })
  }

  removeDoubleQuotes(stringData: any) {
    if (stringData == null || stringData == 'undefined' || stringData == '') {
      return undefined
    }
    return stringData.replace(/['"]+/g, '')
  }

}

/**
 * @author vaibhbav.kashyap
 * 
*/
export class AppData {
    appName: string;
    appCode: string;
    createdBy: string;
    createdDate: boolean;
    appId: string;
    constructor() {
    }
}
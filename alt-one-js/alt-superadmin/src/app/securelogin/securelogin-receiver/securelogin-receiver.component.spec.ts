import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecureloginReceiverComponent } from './securelogin-receiver.component';

describe('SecureloginReceiverComponent', () => {
  let component: SecureloginReceiverComponent;
  let fixture: ComponentFixture<SecureloginReceiverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecureloginReceiverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecureloginReceiverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

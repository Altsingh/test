import { Component, OnInit } from '@angular/core';
import { SecureloginService } from '../securelogin.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { SecureLoginModel } from '../securelogin.model';



@Component({
  selector: 'app-securelogin-receiver',
  templateUrl: './securelogin-receiver.component.html',
  styleUrls: ['./securelogin-receiver.component.css'],
  providers: [SecureloginService]
})
export class SecureloginReceiverComponent implements OnInit {
  private accessToken: string;
  private refreshToken: string;
  private url: string;
  private custom: boolean;
  private orgId: string;
  private tenantId: string;
  private dataMap: Map<string, string>;

  constructor(private route: ActivatedRoute,
    private secureloginService: SecureloginService, private router: Router) {
  }

  ngOnInit() {
    this.accessToken = this.route.snapshot
      .queryParams['accessToken'];
    this.refreshToken = this.route.snapshot
      .queryParams['refreshToken'];
    this.custom = this.route.snapshot
      .queryParams['isCustom'];
    this.orgId = this.route.snapshot.queryParams['orgId'];
    this.tenantId = this.route.snapshot.queryParams['tenantId'];
    
    localStorage.setItem('tenantId',btoa(this.tenantId));
    localStorage.setItem('refreshToken',this.refreshToken);

    // check for SSO Login
    this.secureLoginPreFetch();
  }


  /**
   * @description Prepares a map of data recievied from microservices
   * sample data: usr=vaibhav$comp=peoplestrong
   * @author vaihav.kashyap
   * @param data 
   */
  prepareFetchedData(data: string) {
    if (data.includes("$")) {
      let strArry = data.split("$");
      for (var i = 0; i < strArry.length; i++) {
        let keyValue = strArry[i].split("=");
        this.dataMap.set(keyValue[0], keyValue[1]);
      }
    } else {
      let keyValue = data.split("=");
      this.dataMap.set(keyValue[0], keyValue[1]);
    }

  }

  /**
   * @description Responsible for sending access and refresh token
   * received from the mesh & will send it microservice for fetching the userName
   * @author vaihav.kashyap
   */
  secureLoginPreFetch() {
    let secureInstance = new SecureLoginModel();
    secureInstance.accesstoken = this.accessToken;
    secureInstance.refreshtoken = this.refreshToken;
    secureInstance.username = null;
    secureInstance.received = false;
    secureInstance.url = this.url;
    secureInstance.custom = this.custom;

    this.secureloginService.secureLoginUser(secureInstance);
    let received = localStorage.getItem("received");
  }

}

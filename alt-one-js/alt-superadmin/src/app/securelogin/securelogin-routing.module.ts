import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SecureloginReceiverComponent} from './securelogin-receiver/securelogin-receiver.component';

const routes: Routes = [
  {path: '', component: SecureloginReceiverComponent},
  {path: 'securelogin', component: SecureloginReceiverComponent,pathMatch: 'full' }
];

export const SecureloginRoutingModule = RouterModule.forChild(routes);

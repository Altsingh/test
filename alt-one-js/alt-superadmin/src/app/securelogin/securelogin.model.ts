import { AppData } from "./customapp.model";

/**
 * @author vaibhbav.kashyap
 * 
*/
export class SecureLoginModel {
    accesstoken: string;
    refreshtoken: string;
    username: string;
    received: boolean;
    authtoken: string;
    url: string;
    custom: boolean;
    appdata: AppData;
    orgId: string;
    userId: string
    photoPath: string
    constructor() {
    }
}
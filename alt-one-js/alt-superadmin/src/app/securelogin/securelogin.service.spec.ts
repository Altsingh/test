import { TestBed, inject } from '@angular/core/testing';

import { SecureloginService } from './securelogin.service';

describe('SecureloginService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SecureloginService]
    });
  });

  it('should be created', inject([SecureloginService], (service: SecureloginService) => {
    expect(service).toBeTruthy();
  }));
});

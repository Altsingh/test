import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { SecureLoginModel } from './securelogin.model';
import { Router } from '../../../node_modules/@angular/router';

@Injectable()
export class SecureloginService {
  private secureurl: string = environment.secureurl;
  private receivedSecureModelInstance: SecureLoginModel;

  constructor(private http: HttpClient, private router: Router) { }

  /**
   * @description sends the post request to the microservice along with tokens
   * received for fetching related deails
   * @author vaihav.kashyap
   * @param SecureLoginModel
   */
  secureLoginUser(secureLoginObj: SecureLoginModel): SecureLoginModel {
    this.http.post(this.secureurl, secureLoginObj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response'
    }).subscribe((res: any) => {
      if (res.status == 200) {
        this.extractLoginData(res);
      } else {
        throw new Error('Bad response status: ' + res.status);
      }
    },
      (err) => {
        this.handleError(err);
      });

    return this.receivedSecureModelInstance;

  }


  /**
   * @description prepares data received from backend boot services along with user details
   * fetched
   * @author vaihav.kashyap
   * @param Response
   */
  extractLoginData(res: any): SecureLoginModel {

    let body = res.body;

    /**
     * Prepare Security instance object on the basis of the data
     * received from backend after successful login.
     * */

    if (body != null) {

      if (body.authtoken != null && !(body.authtoken == "")) {
        localStorage.setItem("authToken", body.authtoken);

        this.receivedSecureModelInstance = new SecureLoginModel();
        this.receivedSecureModelInstance.accesstoken = body.accesstoken;
        this.receivedSecureModelInstance.refreshtoken = body.refreshtoken;
        this.receivedSecureModelInstance.username = body.username;
        this.receivedSecureModelInstance.received = body.received;
        this.receivedSecureModelInstance.url = body.url;
        this.receivedSecureModelInstance.custom = body.isCustom;
        this.receivedSecureModelInstance.authtoken = body.authtoken;
        this.receivedSecureModelInstance.orgId = body.orgId;
        localStorage.setItem("adminPhoto",body.photoPath);
        localStorage.setItem("realm", btoa(body.orgId));
        localStorage.setItem("entityId", btoa(body.userId));
        // this will be true if user has successfuly logged in
        localStorage.setItem("received", body.received);
        this.router.navigateByUrl('home');
      } else {
        // error in login
        this.router.navigateByUrl('not-found');
      }
    } else {
      // error in login
      this.router.navigateByUrl('not-found');
    }
    return this.receivedSecureModelInstance;
  }

  /**
   * @description Handles error message
   * @author vaihav.kashyap
   * @param any
   */
  private handleError(error: any) {
    let errMsg = error.message || 'Server error';
    this.router.navigateByUrl('not-found');
  }

}

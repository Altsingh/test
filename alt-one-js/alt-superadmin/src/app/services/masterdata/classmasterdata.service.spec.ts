import { TestBed, inject } from '@angular/core/testing';

import { ClassmasterdataService } from './classmasterdata.service';

describe('ClassmasterdataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClassmasterdataService]
    });
  });

  it('should be created', inject([ClassmasterdataService], (service: ClassmasterdataService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable()
export class ClassmasterdataService {

  private url: string = environment.url + "/masterdata/classes";


  constructor(private http: HttpClient) { }

  getClasses() {
    return this.http.get(this.url, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  getClass(id) {
    return this.http.get(this.url + "/" + id, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  getAttributesForClass(id) {
    return this.http.get(this.url + "/" + id + "/attributes", {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  createClass(classData) {
    return this.http.post(this.url, classData, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  updateClass(classData) {
    return this.http.put(this.url, classData, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  getProtocol() {
    return {
      "userCode": "",
      "userName": "",
      "orgCode": "org2",
      "tenantCode": "",
      "sysFormCode": "",
      "portalType": "SUPERADMIN",
      "loggedInUserIdentifier": "2",
      "loggedInTimestamp": 346834638434,
      "jwt": "a"
    }
  }

  getBody(app) {
    return {
      "protocol": this.getProtocol(),
      "action": "CREATE",
      "app": app
    }
  }

  getBodyForModuleAddition(app) {

    return {
      "protocol": this.getProtocol(),
      "app": app

    }
  }

}

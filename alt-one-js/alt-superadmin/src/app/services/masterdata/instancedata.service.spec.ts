import { TestBed, inject } from '@angular/core/testing';

import { InstancedataService } from './instancedata.service';

describe('InstancedataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InstancedataService]
    });
  });

  it('should be created', inject([InstancedataService], (service: InstancedataService) => {
    expect(service).toBeTruthy();
  }));
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable()
export class InstancedataService {

  private url: string = environment.url + "/masterdata/classes";

  constructor(private http: HttpClient) {
  }

  getInstances(classId) {
    return this.http.get(this.url + "/" + classId + "/instances", {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  getInstance(classId, id) {
    return this.http.get(this.url + "/" + classId + "/instances/" + id, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  createInstance(instanceData, classId) {
    return this.http.post(this.url + "/" + classId + "/instances", instanceData, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  updateInstance(instanceData, classId) {
    return this.http.put(this.url + "/" + classId + "/instances", instanceData, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'authToken': localStorage.getItem("authToken")
      }),
      observe: 'response'
    });
  }

  getProtocol() {
    return {
      "userCode": "",
      "userName": "",
      "orgCode": "org2",
      "tenantCode": "",
      "sysFormCode": "",
      "portalType": "SUPERADMIN",
      "loggedInUserIdentifier": "2",
      "loggedInTimestamp": 346834638434,
      "jwt": "a"
    }
  }

  getBody(app) {
    return {
      "protocol": this.getProtocol(),
      "action": "CREATE",
      "app": app
    }
  }

  getBodyForModuleAddition(app) {

    return {
      "protocol": this.getProtocol(),
      "app": app

    }
  }

}

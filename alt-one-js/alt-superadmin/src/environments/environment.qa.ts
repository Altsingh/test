// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {

    //environment
    production: false,

    //authtoken
    authToken: "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTA2NTEwNDUsInN1YiI6ImxvZ2luIiwidXNlckNvZGUiOiJ1c2VyMDAxIiwibG9nZ2VkSW5UaW1lc3RhbXAiOjE1MTA2NTEwNDUwNzgsInVzZXJOYW1lIjoidXNlciIsIm9yZ0NvZGUiOiJvcmcyIiwidGVuYW50Q29kZSI6IiAiLCJzeXNGb3JtQ29kZSI6IiAiLCJwb3J0YWxUeXBlIjoiU1VQRVJBRE1JTiIsImxvZ2dlZEluVXNlcklkZW50aWZpZXIiOiIgIiwiaXNzIjoiQWx0LUxvZ2luIiwiZXhwIjoxNTQyMTg3MDQ1fQ.sBLgWygjlciZv7YOgHRc2RAeoQzPZn-P8PY6z6V8oX8",

    //backend urls
    url: "https://10.226.0.219:8095/formframework/api/v1",
    ruleUrl: "https://10.226.0.219:8094/business/api/v1",
    formUrl: "https://10.226.0.219:8095/formframework/api/v1",
    secureurl: "https://10.226.0.219:8095/formframework/loginReceiverController/login",
    getappdata: "https://10.226.0.219:8095/formframework/loginReceiverController/getCustomAppData",
    appLogoUploadUrl: "https://10.226.0.219:8095/formframework/api/v1/apps/logo/applogoupload",
    keycloak_url: "https://sohum-auth.peoplestrong.com/",
    builderLogo: "https://test-image.peoplestrongalt.com/altbuilder/default01.png",

    


    //frontend urls
    appPage: "http://10.226.0.219:4300/#",

    //  mocks
    temp: "https://www.mocky.io/v2/5a1d501c2e0000b72548b786",
    organisationFormList: "https://hrms-admin.sohum.com/services/forms",
    formFieldGroupList: "https://hrms-admin.sohum.com/services/fieldGroups",
    organisationList: "https://hrms-admin.sohum.com/services/organizations",
    globalObjectUrl: "https://hrms-admin.sohum.com/services/objects?orgId=",
    meshUrl: "https://hrms-admin.sohum.com/services/apps"
};

export const environment = {

    //environment
    production: true,
  
    //authtoken
    authToken: "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTA2NTEwNDUsInN1YiI6ImxvZ2luIiwidXNlckNvZGUiOiJ1c2VyMDAxIiwibG9nZ2VkSW5UaW1lc3RhbXAiOjE1MTA2NTEwNDUwNzgsInVzZXJOYW1lIjoidXNlciIsIm9yZ0NvZGUiOiJvcmcyIiwidGVuYW50Q29kZSI6IiAiLCJzeXNGb3JtQ29kZSI6IiAiLCJwb3J0YWxUeXBlIjoiU1VQRVJBRE1JTiIsImxvZ2dlZEluVXNlcklkZW50aWZpZXIiOiIgIiwiaXNzIjoiQWx0LUxvZ2luIiwiZXhwIjoxNjQyMTg3MDQ1fQ.01qVkOB9J4boMIzH5MrAiX-O-J9ZlrXHcUpenDN74Zg",
  
    //backend urls
    url: "https://staging-gateway.peoplestrong.com/alt-formframework-0.0.1-SNAPSHOT/api/v1",
    ruleUrl: "https://staging-gateway.peoplestrong.com/alt-businesslayer-0.0.1-SNAPSHOT/api/v1",
    formUrl: "https://staging-gateway.peoplestrong.com/alt-formframework-0.0.1-SNAPSHOT/api/v1",
    secureurl: "https://staging-gateway.peoplestrong.com/alt-formframework-0.0.1-SNAPSHOT/loginReceiverController/login",
    getappdata: "https://staging-gateway.peoplestrong.com/alt-formframework-0.0.1-SNAPSHOT/loginReceiverController/getCustomAppData",
    appLogoUploadUrl: "https://staging-gateway.peoplestrong.com/alt-formframework-0.0.1-SNAPSHOT/api/v1/apps/logo/applogoupload",
    keycloak_url: "https://staging-auth.peoplestrong.com/",
    builderLogo: "https://staging-image.peoplestrong.com/altbuilder/default01.png",
  
    //frontend urls
    appPage: "https://staging-app.peoplestrong.com/#",
  
    //mocks
    temp: "https://www.mocky.io/v2/5a1d501c2e0000b72548b786",
    organisationFormList: "https://staging-hrms-admin.peoplestrongalt.com/services/forms",
    formFieldGroupList: "https://staging-hrms-admin.peoplestrongalt.com/services/fieldGroups",
    organisationList: "https://staging-hrms-admin.peoplestrongalt.com/services/organizations",
    globalObjectUrl: "https://staging-hrms-admin.peoplestrongalt.com/services/objects?orgId=",
    meshUrl: "https://staging-hrms-admin.peoplestrongalt.com/services/apps"
  };
  
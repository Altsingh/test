export const environment = {

    //environment
    production: true,

    //authtoken
    authToken: "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTA2NTEwNDUsInN1YiI6ImxvZ2luIiwidXNlckNvZGUiOiJ1c2VyMDAxIiwibG9nZ2VkSW5UaW1lc3RhbXAiOjE1MTA2NTEwNDUwNzgsInVzZXJOYW1lIjoidXNlciIsIm9yZ0NvZGUiOiJvcmcyIiwidGVuYW50Q29kZSI6IiAiLCJzeXNGb3JtQ29kZSI6IiAiLCJwb3J0YWxUeXBlIjoiU1VQRVJBRE1JTiIsImxvZ2dlZEluVXNlcklkZW50aWZpZXIiOiIgIiwiaXNzIjoiQWx0LUxvZ2luIiwiZXhwIjoxNTQyMTg3MDQ1fQ.sBLgWygjlciZv7YOgHRc2RAeoQzPZn-P8PY6z6V8oX8",

    //backend urls
    url: "https://test-gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/api/v1",
    ruleUrl: "https://test-gateway.peoplestrongalt.com/alt-businesslayer-0.0.1-SNAPSHOT/api/v1",
    formUrl: "https://test-gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/api/v1",
    secureurl: "https://test-gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/loginReceiverController/login",
    getappdata: "https://test-gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/loginReceiverController/getCustomAppData",
    appLogoUploadUrl: "https://test-gateway.peoplestrongalt.com/alt-formframework-0.0.1-SNAPSHOT/api/v1/apps/logo/applogoupload",
    keycloak_url: "https://sohum-auth.peoplestrong.com/",
    builderLogo: "https://test-image.peoplestrongalt.com/altbuilder/default01.png",

    //frontend urls
    appPage: "https://test-preview.peoplestrong.com/#",

    //mocks
    temp: "https://www.mocky.io/v2/5a1d501c2e0000b72548b786",
    organisationFormList: "https://hrms-admin.sohum.com/services/forms",
    formFieldGroupList: "https://hrms-admin.sohum.com/services/fieldGroups",
    organisationList: "https://hrms-admin.sohum.com/services/organizations",
    globalObjectUrl: "https://hrms-admin.sohum.com/services/objects?orgId=",
    meshUrl: "https://hrms-admin.sohum.com/services/apps"
};


package com.alt.rule;

import com.alt.rule.component.RuleComponent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication(scanBasePackages = { "com.alt.rule.*" })
@EntityScan(basePackageClasses = { com.alt.rule.entity.Rule.class })
@EnableTransactionManagement
public class RuleEngineRun  extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RuleEngineRun.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(RuleEngineRun.class, args);
    }

    @Bean
    public RuleComponent ruleComponent() {
        return new RuleComponent();
    }

    @Bean
    @Profile("local")
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");

            }
        };
    }

}

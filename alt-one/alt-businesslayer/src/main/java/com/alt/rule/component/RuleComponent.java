package com.alt.rule.component;

import com.alt.datacarrier.formsecurity.core.MessageTO;
import com.alt.rule.model.*;
import com.alt.rule.transport.response.MessageResponseTO;
import com.alt.rule.transport.response.RuleCreationResponse;
import com.alt.rule.dao.RuleRepository;
import com.alt.rule.util.RuleConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RuleComponent {

    @Autowired
    RuleRepository ruleRepository;

    @Transactional
    public RuleCreationResponse createRule(List<? extends Rule> rules) {
        RuleCreationResponse response=new RuleCreationResponse();
        List<com.alt.rule.entity.Rule> rulesData = new ArrayList<>();
        List<com.alt.rule.entity.Rule> validationRules = rules.stream()
                .filter(rule -> rule instanceof ValidationRule)
                .map(rule -> (ValidationRule) rule)
                .map(rule -> {
                    return com.alt.rule.entity.Rule.RuleBuilder.aRule()
                            .withCases(RuleConvertor.convertToEntityCase(rule.getCases(),"VALIDATION"))
                            .withRulename(rule.getRuleName())
                            .withRuleid(rule.getRuleId())
                            .withRuleexpression(rule.getRuleExpresssion())
                            .withRulemessage(rule.getSuccessMessage())
                            .withRuletype(RuleType.VALIDATION.toString())
                            .withCreateddate(new Date().getTime())
                            .withModifieddate(new  Date().getTime())
                            .withCreatedby("")
                            .withModifiedby("")
                            .withStatus(true)
                            .build();
                })
                .collect(Collectors.toList());

        List<com.alt.rule.entity.Rule> transactionRules = rules.stream()
                .filter(rule -> rule instanceof TransactionRule)
                .map(rule -> (TransactionRule) rule)
                .map(rule -> {
                    return com.alt.rule.entity.Rule.RuleBuilder.aRule()
                            .withCases(RuleConvertor.convertToEntityCase(rule.getCases(),"TRANSACTION"))
                            .withRulename(rule.getRuleName())
                            .withRuleid(rule.getRuleId())
                            .withRulemessage(rule.getSuccessMessage())
                            .withRuletype(RuleType.TRANSACTION.toString())
                            .withCreateddate(new Date().getTime())
                            .withModifieddate(new  Date().getTime())
                            .withCreatedby("")
                            .withModifiedby("")
                            .build();
                })
                .collect(Collectors.toList());

        List<com.alt.rule.entity.Rule> validationRuleData = ruleRepository.save(validationRules);
        List<com.alt.rule.entity.Rule> transactionRuleData = ruleRepository.save(transactionRules);
        rulesData.addAll(validationRuleData);
        rulesData.addAll(transactionRuleData);
        response.setSuccess(true);
        response.setRules(RuleConvertor.convertRuleEntityToModel(rulesData));
        return response;
    }

	public RuleCreationResponse getRuleById(List<Long> ruleIdList) {
		RuleCreationResponse response = new RuleCreationResponse();
		 List<com.alt.rule.entity.Rule> fetchedrule= ruleRepository.findAll(ruleIdList);
		if(fetchedrule!=null) {
			response.setSuccess(true);
			response.setRules(RuleConvertor.convertRuleEntityToModel(fetchedrule));
		}else {
			response.setSuccess(false);
		}
		
		return response;
	}

	public MessageResponseTO deleteRule(List<Long> rules){
        rules.forEach(rule->{
            ruleRepository.delete(rule);
        });
        return new MessageResponseTO("Deleted rule successfully");
    }
}

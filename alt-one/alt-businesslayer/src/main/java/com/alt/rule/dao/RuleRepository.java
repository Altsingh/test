package com.alt.rule.dao;

import com.alt.rule.entity.Rule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleRepository  extends JpaRepository<Rule,Long> {
}

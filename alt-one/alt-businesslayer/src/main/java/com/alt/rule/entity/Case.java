package com.alt.rule.entity;

import java.io.Serializable;

import com.alt.rule.model.RuleInput;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Case implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String failureMessage;
	private RuleInput firstInput;
	private RuleInput secondInput;
	private String validationType;
	private RuleInput assignToAttribute;
	private String event;
	private String componentName;
	private String componentType;
	private String ruleExecutionEnd;
	private String scriptData;
	private String caseType;


	public Case(String failureMessage, RuleInput firstInput, RuleInput secondInput, String validationType,RuleInput assignToAttribute, String event, String componentName, String componentType, String ruleExecutionEnd, String scriptData, String caseType) {
		super();
		this.failureMessage = failureMessage;
		this.firstInput = firstInput;
		this.secondInput = secondInput;
		this.validationType = validationType;
		this.assignToAttribute = assignToAttribute;
		this.event = event;
		this.componentName = componentName;
		this.componentType = componentType;
		this.ruleExecutionEnd = ruleExecutionEnd;
		this.scriptData = scriptData;
		this.caseType = caseType;
	}

	public Case() {
		
	}

	public RuleInput getAssignToAttribute() {
		return assignToAttribute;
	}

	public void setAssignToAttribute(RuleInput assignToAttribute) {
		this.assignToAttribute = assignToAttribute;
	}

	public String getFailureMessage() {
		return failureMessage;
	}

	
	public RuleInput getFirstInput() {
		return firstInput;
	}

	public RuleInput getSecondInput() {
		return secondInput;
	}

	public String getValidationType() {
		return validationType;
	}

	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}

	public void setFirstInput(RuleInput firstInput) {
		this.firstInput = firstInput;
	}

	public void setSecondInput(RuleInput secondInput) {
		this.secondInput = secondInput;
	}

	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	public String getRuleExecutionEnd() {
		return ruleExecutionEnd;
	}

	public void setRuleExecutionEnd(String ruleExecutionEnd) {
		this.ruleExecutionEnd = ruleExecutionEnd;
	}

	public String getScriptData() {
		return scriptData;
	}

	public void setScriptData(String scriptData) {
		this.scriptData = scriptData;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	@Override
    public String toString() {
        return "Case{" +
        		"firstInput=" + firstInput +","+
        		"secondInput=" + secondInput +","+
        		"validationType=" + validationType +","+
                "failureMessage=" + failureMessage +
                '}';
    }
}

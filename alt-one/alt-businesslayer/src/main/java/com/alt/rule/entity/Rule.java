package com.alt.rule.entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.Date;

@Entity
@Table(name = "rule")
public class Rule {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(columnDefinition = "ruleid")
    private Long ruleid;

    @Column
    private String rulename;

    @Column(columnDefinition = "ruletype")
    private String ruletype;

    @Column(columnDefinition = "ruleexpression")
    private String ruleexpression;

    @Column(columnDefinition = "rulemessage")
    private String rulemessage;

    private boolean status;
    private String orgid;
    private String createdby;

    @Column(columnDefinition = "createddate")
    private Long createddate;

    private String modifiedby;

    @Column(columnDefinition = "modifieddate")
    private Long modifieddate;



    @Column
    @Type(type = "CaseType")
    private List<Case> cases;

    public String getRulename() {
        return rulename;
    }

    public void setRulename(String rulename) {
        this.rulename = rulename;
    }

    public Long getRuleid() {
        return ruleid;
    }

    public void setRuleid(Long ruleid) {
        this.ruleid = ruleid;
    }

    public String getRuletype() {
        return ruletype;
    }

    public void setRuletype(String ruletype) {
        this.ruletype = ruletype;
    }

    public String getRuleexpression() {
        return ruleexpression;
    }

    public void setRuleexpression(String ruleexpression) {
        this.ruleexpression = ruleexpression;
    }

    public String getRulemessage() {
        return rulemessage;
    }

    public void setRulemessage(String rulemessage) {
        this.rulemessage = rulemessage;
    }

    public List<Case> getCases() {
        return cases;
    }

    public void setCases(List<Case> cases) {
        this.cases = cases;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }



    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Long getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Long createddate) {
        this.createddate = createddate;
    }

    public Long getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Long modifieddate) {
        this.modifieddate = modifieddate;
    }


    public static final class RuleBuilder {
        private Long ruleid;
        private String rulename;
        private String ruletype;
        private String ruleexpression;
        private String rulemessage;
        private boolean status;
        private String orgid;
        private String createdby;
        private Long createddate;
        private String modifiedby;
        private Long modifieddate;
        private List<Case> cases;

        private RuleBuilder() {
        }

        public static RuleBuilder aRule() {
            return new RuleBuilder();
        }

        public RuleBuilder withRuleid(Long ruleid) {
            this.ruleid = ruleid;
            return this;
        }

        public RuleBuilder withRulename(String rulename) {
            this.rulename = rulename;
            return this;
        }

        public RuleBuilder withRuletype(String ruletype) {
            this.ruletype = ruletype;
            return this;
        }

        public RuleBuilder withRuleexpression(String ruleexpression) {
            this.ruleexpression = ruleexpression;
            return this;
        }

        public RuleBuilder withRulemessage(String rulemessage) {
            this.rulemessage = rulemessage;
            return this;
        }

        public RuleBuilder withStatus(boolean status) {
            this.status = status;
            return this;
        }

        public RuleBuilder withOrgid(String orgid) {
            this.orgid = orgid;
            return this;
        }

        public RuleBuilder withCreatedby(String createdby) {
            this.createdby = createdby;
            return this;
        }

        public RuleBuilder withCreateddate(Long createddate) {
            this.createddate = createddate;
            return this;
        }

        public RuleBuilder withModifiedby(String modifiedby) {
            this.modifiedby = modifiedby;
            return this;
        }

        public RuleBuilder withModifieddate(Long modifieddate) {
            this.modifieddate = modifieddate;
            return this;
        }

        public RuleBuilder withCases(List<Case> cases) {
            this.cases = cases;
            return this;
        }

        public Rule build() {
            Rule rule = new Rule();
            rule.setRuleid(ruleid);
            rule.setRulename(rulename);
            rule.setRuletype(ruletype);
            rule.setRuleexpression(ruleexpression);
            rule.setRulemessage(rulemessage);
            rule.setStatus(status);
            rule.setOrgid(orgid);
            rule.setCreatedby(createdby);
            rule.setCreateddate(createddate);
            rule.setModifiedby(modifiedby);
            rule.setModifieddate(modifieddate);
            rule.setCases(cases);
            return rule;
        }
    }
}

package com.alt.rule.resource;

import com.alt.datakernel.exception.RuleRelatedException;
import com.alt.rule.component.RuleComponent;
import com.alt.rule.model.Rule;
import com.alt.rule.transport.request.RuleCreationRequest;
import com.alt.rule.transport.response.MessageResponseTO;
import com.alt.rule.transport.response.RuleCreationResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/rule")
public class RuleResource {

    @Autowired(required = true)
    RuleComponent ruleComponent;

    private static final Logger logger = Logger.getLogger(RuleResource.class);

    @GetMapping("/test")
    public ResponseEntity<String> getTest() {
        return new ResponseEntity<>("Hello World", HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<RuleCreationResponse> createRule(@RequestBody List<? extends Rule> rules){
        RuleCreationResponse response  = ruleComponent.createRule(rules);
        return ResponseEntity.ok(response);
    }
    
	@RequestMapping(value = "/getRuleById", method = RequestMethod.POST)
    public ResponseEntity<RuleCreationResponse> getRuleById(@RequestBody RuleCreationRequest ruleCreationRequest) throws RuleRelatedException{
    	RuleCreationResponse response = null;
    	if(ruleCreationRequest!=null && ruleCreationRequest.getRuleIdList()!=null)
    	response  = ruleComponent.getRuleById(ruleCreationRequest.getRuleIdList());
    	return ResponseEntity.ok(response);
    }

    @PostMapping("/delete")
    public ResponseEntity<MessageResponseTO> deleteRule(@RequestBody List<Long> rules){
        MessageResponseTO response  = ruleComponent.deleteRule(rules);
        return ResponseEntity.ok(response);
    }

}

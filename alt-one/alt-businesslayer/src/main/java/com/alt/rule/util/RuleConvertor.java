package com.alt.rule.util;


import com.alt.rule.model.*;

import com.alt.rule.entity.Case;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class RuleConvertor {

    public static List<Rule> convertRuleEntityToModel(List<com.alt.rule.entity.Rule> ruleEntity){
        List<Rule> rules = new ArrayList<>();
        List<Rule> validationRules = ruleEntity.stream()
                .filter(rule -> rule.getRuletype().equalsIgnoreCase("VALIDATION"))
                .map(rule -> {
                    return ValidationRule.ValidationRuleBuilder.aValidationRule()
                            .withCases(convertToValidationCase(rule.getCases()))
                            .withRuleName(rule.getRulename())
                            .withRuleId(rule.getRuleid())
                            .withRuleExpresssion(rule.getRuleexpression())
                            .withSuccessMessage(rule.getRulemessage())
                            .withRuleType(RuleType.VALIDATION)
                            .build();
                })
                .collect(Collectors.toList());

        List<Rule> transactionRules = ruleEntity.stream()
                .filter(rule -> rule.getRuletype().equalsIgnoreCase("TRANSACTION"))
                .map(rule -> {
                    return TransactionRule.TransactionRuleBuilder.aTransactionRule()
                            .withCases(convertToTransactionCase(rule.getCases()))
                            .withRuleName(rule.getRulename())
                            .withRuleId(rule.getRuleid())
                            .withSuccessMessage(rule.getRulemessage())
                            .withRuleType(RuleType.TRANSACTION)
                            .build();
                }) 
                .collect(Collectors.toList());
        rules.addAll(validationRules);
        rules.addAll(transactionRules);
        return rules;
        }

    private static List<TransactionCase> convertToTransactionCase(List<Case> cases) {
        return cases.stream().map(c->new TransactionCase(c.getFirstInput(), c.getSecondInput(), c.getValidationType(), c.getFailureMessage(),c.getAssignToAttribute(),c.getEvent(),c.getComponentName(),c.getComponentType(),c.getRuleExecutionEnd(),c.getScriptData(),c.getCaseType())).collect(Collectors.toList());
    }

    private static List<ValidationCase> convertToValidationCase(List<Case> cases) {
    	return cases.stream().map(c->new ValidationCase(c.getFirstInput(), c.getSecondInput(), c.getValidationType(), c.getFailureMessage(),c.getEvent(),c.getComponentName(),c.getComponentType(),c.getRuleExecutionEnd(),c.getScriptData(),c.getCaseType())).collect(Collectors.toList());
    }
    
    public static List<Case> convertToEntityCase(List<? extends com.alt.rule.model.Case> cases, String caseType){
    	if(caseType.equals("VALIDATION")) {
    		return cases.stream()
    			.map(c->(ValidationCase)c)
    			.map(c->new Case(c.getFailureMessage(), c.getFirstInput(), c.getSecondInput(), c.getValidationType().toString(),null,c.getEvent(),c.getComponentName(),c.getComponentType(),c.getRuleExecutionEnd(),c.getScriptData(),c.getCaseType())).collect(Collectors.toList());
    	}else if(caseType.equals("TRANSACTION")) {
    		return cases.stream()
        			.map(c->(TransactionCase)c)
        			.map(c->new Case(c.getFailureMessage(), c.getFirstInput(), c.getSecondInput(), c.getValidationType().toString(),c.getAssignToAttribute(), c.getEvent(), c.getComponentName(), c.getComponentType(),c.getRuleExecutionEnd(),c.getScriptData(),c.getCaseType())).collect(Collectors.toList());
    	}
    	return null;
    }
}

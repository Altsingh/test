package com.alt.core.util;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.client.methods.HttpAsyncMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import rx.Observable;
import rx.apache.http.ObservableHttp;

public class CommonUtil {

	static Logger LOGGER = LoggerFactory.getLogger(CommonUtil.class);

	public static String getUUID() {
		// To be changed as per decided template(later)
		return UUID.randomUUID().toString();
	}

	public static rx.Observable<String> getDataFromUrl(String url, String jsonRequest) throws NullPointerException {
		Observable<String> rawResponse = null;
		try {
			final RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(9000).setConnectTimeout(500)
					.build();
			final CloseableHttpAsyncClient httpClient = HttpAsyncClients.custom().setDefaultRequestConfig(requestConfig)
					.setMaxConnPerRoute(20).setMaxConnTotal(50).build();
			httpClient.start();
			rawResponse = (ObservableHttp.createRequest(
					HttpAsyncMethods.createPost(url, jsonRequest, ContentType.APPLICATION_JSON), httpClient))
							.toObservable().flatMap(resp -> resp.getContent().map(String::new));
		} catch (Exception e) {
			LOGGER.error("Error occured:", e);
			rawResponse.singleOrDefault(e.getMessage());
		}
		return rawResponse;
	}

	public static Map<String, Observable<String>> getDataFromUrlList(Map<String, String> requestMap) {
		Map<String, Observable<String>> response = new HashMap<>();
		try {
			final RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30000).setConnectTimeout(5000)
					.build();
			final CloseableHttpAsyncClient httpClient = HttpAsyncClients.custom().setDefaultRequestConfig(requestConfig)
					.setMaxConnPerRoute(20).setMaxConnTotal(50).build();
			httpClient.start();
			requestMap.entrySet().parallelStream().forEach(entry -> {
				try {
					response.put(entry.getKey(),
							(ObservableHttp.createRequest(HttpAsyncMethods.createPost(entry.getKey(), entry.getValue(),
									ContentType.APPLICATION_JSON), httpClient)).toObservable()
											.flatMap(resp -> resp.getContent().map(String::new)));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			LOGGER.error("Error occured:", e);
		}
		return response;
	}

}

package com.alt.core.util;

import com.alt.datacarrier.common.enumeration.EnumPortalTypes;
import com.alt.datacarrier.core.Protocol;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;

public class JwtUtil {

	@Value("${jwt.secret.key}")
	static String secretKey = "alt_secret_key";

	public static Claims validateJwt(String jwt) throws JsonProcessingException {
		// We will sign our JWT with our ApiKey secret
		ObjectMapper mapper = new ObjectMapper();
		Claims claims = null;
		String res1 = Base64.getEncoder().encodeToString(secretKey.getBytes(StandardCharsets.UTF_8));
		try {
			claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(res1)).parseClaimsJws(jwt)
					.getBody();
		} catch (ExpiredJwtException e) {
			throw new RuntimeException("Auth Token Expired");
		}
		// String userName = claims.get("userCode", String.class);
		return claims;
	}

	public static String generateJwt(Protocol protocol) {
		long nowMillis = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		Date today = cal.getTime();
		cal.add(Calendar.YEAR, 1); // to get previous year add -1
		Date nextYear = cal.getTime();
		JwtBuilder builder = Jwts.builder().setIssuedAt(today).setSubject("login").claim("loggedInTimestamp", nowMillis)
				.claim("userName", protocol.getUserName()).claim("orgCode", protocol.getOrgCode())
				.claim("orgId", protocol.getOrgId()).claim("tenantCode", " ").claim("sysFormCode", " ")
				.claim("portalType", EnumPortalTypes.SUPERADMIN).claim("loggedInUserIdentifier", " ")
				.claim("userId", protocol.getUserId()).claim("employeeId", protocol.getEmployeeId())
				.claim("tenantId", protocol.getTenantId()).claim("appCode",protocol.getAppCode()).setIssuer("Alt-Login")
				.signWith(SignatureAlgorithm.HS256, generateSecretKey());
		builder.setExpiration(nextYear);
		// Builds the JWT and serializes it to a compact, URL-safe string
		String jwt = builder.compact();
		return jwt;
	}

	private static SecretKeySpec generateSecretKey() {
		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		// We will sign our JWT with our ApiKey secret
		String res1 = Base64.getEncoder().encodeToString(secretKey.getBytes(StandardCharsets.UTF_8));
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(res1);
		SecretKeySpec signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
		return signingKey;
	}

}

package com.alt.datacarrier.business.common;


import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;

public class AltReadRequest implements IRequestData{

	private static final long serialVersionUID = -2987274116473716129L;

	private String docIdentifier;
	
	private EnumRequestType type;
	
	private int depth;
	
	private int offset;
	
	private int limit;

	public String getDocIdentifier() {
		return docIdentifier;
	}

	public void setDocIdentifier(String docIdentifier) {
		this.docIdentifier = docIdentifier;
	}

	public EnumRequestType getType() {
		return type;
	}

	public void setType(EnumRequestType type) {
		this.type = type;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

}

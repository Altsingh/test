package com.alt.datacarrier.business.common;

import java.util.List;
import java.util.Map;

import com.alt.datacarrier.core.ClassMeta;
import com.alt.datacarrier.core.IResponseData;
import com.alt.datacarrier.core.InstanceData;

public class AltReadResponse implements IResponseData
{

	private static final long serialVersionUID = -5084004595007707891L;

    private Map<String, ClassMeta> meta;

    private List<List<InstanceData>> instance;

    public List<List<InstanceData>> getInstance()
    {
        return instance;
    }

    public void setInstance(List<List<InstanceData>> instance)
    {
        this.instance = instance;
    }

	public Map<String, ClassMeta> getMeta() {
		return meta;
	}

	public void setMeta(Map<String, ClassMeta> meta) {
		this.meta = meta;
	}

	

	
}
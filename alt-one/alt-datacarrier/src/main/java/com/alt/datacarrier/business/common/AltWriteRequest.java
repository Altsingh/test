package com.alt.datacarrier.business.common;

import java.util.List;

import com.alt.datacarrier.core.AttributeMeta;
import com.alt.datacarrier.core.ClassMeta;
import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;

public class AltWriteRequest implements IRequestData {

	private static final long serialVersionUID = -6042672898035140554L;

	private EnumCRUDOperations action;

	private EnumRequestType type;

	private Boolean copyclass;

	private ClassMeta classMeta;

	private List<AttributeMeta> attributeMetaList;

	public EnumCRUDOperations getAction() {
		return action;
	}

	public void setAction(EnumCRUDOperations action) {
		this.action = action;
	}

	public EnumRequestType getType() {
		return type;
	}

	public void setType(EnumRequestType type) {
		this.type = type;
	}

	public Boolean getCopyclass() {
		return copyclass;
	}

	public void setCopyclass(Boolean copyclass) {
		this.copyclass = copyclass;
	}

	public ClassMeta getClassMeta() {
		return classMeta;
	}

	public void setClassMeta(ClassMeta classMeta) {
		this.classMeta = classMeta;
	}

	public List<AttributeMeta> getAttributeMetaList() {
		return attributeMetaList;
	}

	public void setAttributeMetaList(List<AttributeMeta> attributeMetaList) {
		this.attributeMetaList = attributeMetaList;
	}

}

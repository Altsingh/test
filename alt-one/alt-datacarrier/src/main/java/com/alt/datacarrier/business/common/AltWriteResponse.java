package com.alt.datacarrier.business.common;

import java.util.List;
import java.util.Map;

import com.alt.datacarrier.core.ClassMeta;
import com.alt.datacarrier.core.IResponseData;
import com.alt.datacarrier.core.InstanceData;

public class AltWriteResponse implements IResponseData {

	/**
	* 
	*/
	private static final long serialVersionUID = 4754875006981707374L;

	private Map<String, ClassMeta> meta;

	private List<List<InstanceData>> instance;

	public Map<String, ClassMeta> getMeta() {
		return meta;
	}

	public void setMeta(Map<String, ClassMeta> meta) {
		this.meta = meta;
	}

	public List<List<InstanceData>> getInstance() {
		return instance;
	}

	public void setInstance(List<List<InstanceData>> instance) {
		this.instance = instance;
	}

}

package com.alt.datacarrier.business.common;

import java.util.List;
import java.util.Map;

import com.alt.datacarrier.common.enumeration.EnumOperation;
import com.alt.datacarrier.core.Protocol;


public class CommonBusinessRequest{

	private Protocol protocolTO;
	
	private EnumOperation operation;
	
	private Map<String,List<String>> filter1;
	
	public Protocol getProtocolTO() {
		return protocolTO;
	}

	public void setProtocolTO(Protocol protocolTO) {
		this.protocolTO = protocolTO;
	}

	public EnumOperation getOperation() {
		return operation;
	}

	public void setOperation(EnumOperation operation) {
		this.operation = operation;
	}

	public Map<String, List<String>> getFilter1() {
		return filter1;
	}

	public void setFilter1(Map<String, List<String>> filter1) {
		this.filter1 = filter1;
	}

}

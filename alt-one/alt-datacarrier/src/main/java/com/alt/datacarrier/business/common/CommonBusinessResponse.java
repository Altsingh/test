package com.alt.datacarrier.business.common;

import java.util.List;
import java.util.Map;

import com.alt.datacarrier.core.OutputValueObject;

public class CommonBusinessResponse {

	private List<Map<String,OutputValueObject>> data;

	private String code;
	
	public List<Map<String, OutputValueObject>> getData() {
		return data;
	}

	public void setData(List<Map<String, OutputValueObject>> requisition) {
		this.data = requisition;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}

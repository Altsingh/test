package com.alt.datacarrier.business.common;


public class FirstLoginRequest extends AltReadRequest {

	private static final long serialVersionUID = 3440781540097176488L;

	private String orgUrl;

	public String getOrgUrl() {
		return orgUrl;
	}

	public void setOrgUrl(String orgUrl) {
		this.orgUrl = orgUrl;
	}
}

package com.alt.datacarrier.business.common;


public class UserLoginRequest extends AltReadRequest {

	private static final long serialVersionUID = -8580082179789752466L;

	private String userName;

	private String password;

	private String docIdentifier;

	public String getDocIdentifier() {
		return docIdentifier;
	}

	public void setDocIdentifier(String docIdentifier) {
		this.docIdentifier = docIdentifier;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

package com.alt.datacarrier.business.jobs;

import com.alt.datacarrier.business.user.User;

public class AllJobsRead {

	private User userTo;

	private JobFilter jobFilterTo;

	private JobSort jobSortTo;

	public User getUserTo() {
		return userTo;
	}

	public void setUserTo(User userTo) {
		this.userTo = userTo;
	}

	public JobFilter getJobFilterTo() {
		return jobFilterTo;
	}

	public void setJobFilterTo(JobFilter jobFilerTo) {
		this.jobFilterTo = jobFilerTo;
	}

	public JobSort getJobSortTo() {
		return jobSortTo;
	}

	public void setJobSortTo(JobSort jobSortTo) {
		this.jobSortTo = jobSortTo;
	}

}

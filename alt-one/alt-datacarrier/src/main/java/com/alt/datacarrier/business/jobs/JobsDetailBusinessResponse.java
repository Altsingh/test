package com.alt.datacarrier.business.jobs;

import java.util.List;
import java.util.Map;

import com.alt.datacarrier.core.OutputValueObject;

public class JobsDetailBusinessResponse {

	private List<Map<String, OutputValueObject>> job;
	
	public List<Map<String, OutputValueObject>> getJob() {
		return job;
	}

	public void setJob(List<Map<String, OutputValueObject>> job) {
		this.job = job;
	}

}

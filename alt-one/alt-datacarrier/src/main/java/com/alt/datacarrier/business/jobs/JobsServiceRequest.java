package com.alt.datacarrier.business.jobs;

import java.util.List;

public class JobsServiceRequest {

	// private UserTo userTo;
	private List<String> fields;

	private JobFilter jobFilterTo;

	private JobSort jobSortTo;

	public JobFilter getJobFilerTo() {
		return jobFilterTo;
	}

	public void setJobFilerTo(JobFilter jobFilerTo) {
		this.jobFilterTo = jobFilerTo;
	}

	public JobSort getJobSortTo() {
		return jobSortTo;
	}

	public void setJobSortTo(JobSort jobSortTo) {
		this.jobSortTo = jobSortTo;
	}

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

}

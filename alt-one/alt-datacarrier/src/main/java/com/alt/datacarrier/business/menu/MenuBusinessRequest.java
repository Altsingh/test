package com.alt.datacarrier.business.menu;

import java.util.Map;

import com.alt.datacarrier.common.enumeration.EnumOperation;
import com.alt.datacarrier.core.Protocol;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuBusinessRequest {

	private Protocol protocolTO;
	
	private EnumOperation operation;
	
	private Map<String,String> menuInput;

	public Protocol getProtocolTO() {
		return protocolTO;
	}

	public void setProtocolTO(Protocol protocolTO) {
		this.protocolTO = protocolTO;
	}

	public EnumOperation getOperation() {
		return operation;
	}

	public void setOperation(EnumOperation operation) {
		this.operation = operation;
	}

	public Map<String, String> getMenuInput() {
		return menuInput;
	}

	public void setMenuInput(Map<String, String> menuInput) {
		this.menuInput = menuInput;
	}

	@Override
	public String toString() {
		return "MenuInputUiTO [protocolTO=" + protocolTO + ", operation="
				+ operation + ", menuInput=" + menuInput + "]";
	}
	
	
	
}

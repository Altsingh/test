package com.alt.datacarrier.business.menu;

import java.util.Map;

import com.alt.datacarrier.core.OutputValueObject;

public class MenuBusinessResponse {
	
	private Map<String,OutputValueObject> menu;

	public Map<String, OutputValueObject> getMenu() {
		return menu;
	}

	public void setMenu(Map<String, OutputValueObject> menu) {
		this.menu = menu;
	}
	
}

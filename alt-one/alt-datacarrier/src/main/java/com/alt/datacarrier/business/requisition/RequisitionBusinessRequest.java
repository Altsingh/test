package com.alt.datacarrier.business.requisition;

import java.util.Map;

import com.alt.datacarrier.common.enumeration.EnumOperation;
import com.alt.datacarrier.core.InputValueObject;
import com.alt.datacarrier.core.Protocol;

public class RequisitionBusinessRequest{

	private Protocol protocolTO;
	
	private EnumOperation operation;
	
	private Map<String,InputValueObject> requisition;
	
	public Protocol getProtocolTO() {
		return protocolTO;
	}

	public void setProtocolTO(Protocol protocolTO) {
		this.protocolTO = protocolTO;
	}

	public EnumOperation getOperation() {
		return operation;
	}

	public void setOperation(EnumOperation operation) {
		this.operation = operation;
	}

	public Map<String, InputValueObject> getRequisition() {
		return requisition;
	}

	public void setRequisition(Map<String, InputValueObject> requisition) {
		this.requisition = requisition;
	}

}

package com.alt.datacarrier.business.requisition;

import java.util.Map;

import com.alt.datacarrier.core.OutputValueObject;

public class RequisitionBusinessResponse {

	private Map<String,OutputValueObject> requisition;

	public Map<String, OutputValueObject> getRequisition() {
		return requisition;
	}

	public void setRequisition(Map<String, OutputValueObject> requisition) {
		this.requisition = requisition;
	}
	
}

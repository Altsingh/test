package com.alt.datacarrier.business.requisition;

import java.util.Map;

public class RequisitionDetail {

	private Map<String,String> requisition;

	public Map<String, String> getRequisition() {
		return requisition;
	}

	public void setRequisition(Map<String, String> requisition) {
		this.requisition = requisition;
	}
	
}

package com.alt.datacarrier.business.superadmin;

import java.util.List;


import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.SessionDetails;

public class AddNewClassRequest {

	private Protocol protocolTO;

	private SessionDetails sessionDetails;

	private String className;

	private String dataType;

	private String classCode;

	private String isActive;

	private String iscountryspecific;

	private String isSystemType;

	private String packageCode;

	List<SuperadminClassAttribute> attributeList;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIscountryspecific() {
		return iscountryspecific;
	}

	public void setIscountryspecific(String iscountryspecific) {
		this.iscountryspecific = iscountryspecific;
	}

	public String getIsSystemType() {
		return isSystemType;
	}

	public void setIsSystemType(String isSystemType) {
		this.isSystemType = isSystemType;
	}

	public String getPackageCode() {
		return packageCode;
	}

	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public List<SuperadminClassAttribute> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<SuperadminClassAttribute> attributeList) {
		this.attributeList = attributeList;
	}

}

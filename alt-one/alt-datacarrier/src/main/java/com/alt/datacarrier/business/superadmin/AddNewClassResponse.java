package com.alt.datacarrier.business.superadmin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddNewClassResponse {
	
	@JsonProperty("name")
	private String className;
	
	@JsonProperty("classCode")
	private String classCode;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	
	
}

package com.alt.datacarrier.business.superadmin;


import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.SessionDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AddNewPackageRequest {
	
	@JsonIgnore
	@JsonProperty("userDetail")
	private Protocol protocolTO;

	@JsonIgnore
	@JsonProperty("sessionDetails")
	private SessionDetails sessionDetails;
	
	@JsonProperty("packageName")
	private String packageName;
	
	@JsonProperty("packageCode")
	private String packageCode;
	
	@JsonProperty("parentPackageName")
	private String parentPackageName;

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getPackageCode() {
		return packageCode;
	}

	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}

	public String getParentPackageName() {
		return parentPackageName;
	}

	public void setParentPackageName(String parentPackageName) {
		this.parentPackageName = parentPackageName;
	}

}

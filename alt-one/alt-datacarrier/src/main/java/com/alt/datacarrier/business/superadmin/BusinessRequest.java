package com.alt.datacarrier.business.superadmin;

import java.util.List;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelSingleReadRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BusinessRequest {
	
	@JsonIgnore
	@JsonProperty("protocol")
	private Protocol protocolTO;
	
	@JsonIgnore
	@JsonProperty("requestData")
	private List<KernelSingleReadRequest> requestData;

	public Protocol getProtocolTO() {
		return protocolTO;
	}

	public void setProtocolTO(Protocol protocolTO) {
		this.protocolTO = protocolTO;
	}

	public List<KernelSingleReadRequest> getRequestData() {
		return requestData;
	}

	public void setRequestData(List<KernelSingleReadRequest> requestData) {
		this.requestData = requestData;
	}

}

package com.alt.datacarrier.business.superadmin;

import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;

import java.util.List;

public class ClassCEResponse {

    private String className;

    private String classId;

    private boolean isCustomAttributeAllowed;

    private List<AttributeMeta> attributes;

    private KernelConstants.EnumInstanceLimiter instanceLimiter;

    private String instancePrefix;

    private boolean locationSpecific;

    private String org;

    private List<String> packagecodes;

    private boolean parameterType;

    private KernelConstants.EnumDocStatusType status;

    private boolean systemType;

    private String createdDate;

    private String createdBy;

    private String modifiedDate;

    private String modifiedBy;

    public ClassCEResponse(){}

    private ClassCEResponse(Builder builder) {
        className = builder.classname;
        classId = builder.classId;
        isCustomAttributeAllowed = builder.isCustomAttributeAllowed;
        attributes = builder.attributesDataList;
        instanceLimiter = builder.instanceLimiter;
        instancePrefix = builder.instancePrefix;
        locationSpecific = builder.locationSpecific;
        org = builder.org;
        packagecodes = builder.packagecodes;
        parameterType = builder.parameterType;
        status = builder.status;
        systemType = builder.systemType;
        createdDate = builder.createdDate;
        createdBy = builder.createdBy;
        modifiedDate = builder.modifiedDate;
        modifiedBy = builder.modifiedBy;
    }

    public String getClassName() {
        return className;
    }

    public String getClassId() {
        return classId;
    }

    public boolean isCustomAttributeAllowed() {
        return isCustomAttributeAllowed;
    }

    public List<AttributeMeta> getAttributes() {
        return attributes;
    }

    public KernelConstants.EnumInstanceLimiter getInstanceLimiter() {
        return instanceLimiter;
    }

    public String getInstancePrefix() {
        return instancePrefix;
    }

    public boolean isLocationSpecific() {
        return locationSpecific;
    }

    public String getOrg() {
        return org;
    }

    public List<String> getPackagecodes() {
        return packagecodes;
    }

    public boolean isParameterType() {
        return parameterType;
    }

    public KernelConstants.EnumDocStatusType getStatus() {
        return status;
    }

    public boolean isSystemType() {
        return systemType;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public static final class Builder {
        private String classname;
        private String classId;
        private boolean isCustomAttributeAllowed;
        private List<AttributeMeta> attributesDataList;
        private KernelConstants.EnumInstanceLimiter instanceLimiter;
        private String instancePrefix;
        private boolean locationSpecific;
        private String org;
        private List<String> packagecodes;
        private boolean parameterType;
        private KernelConstants.EnumDocStatusType status;
        private boolean systemType;
        private String createdDate;
        private String createdBy;
        private String modifiedDate;
        private String modifiedBy;

        public Builder() {
        }

        public Builder withClassname(String val) {
            classname = val;
            return this;
        }

        public Builder withClassId(String val) {
            classId = val;
            return this;
        }

        public Builder withIsCustomAttributeAllowed(boolean val) {
            isCustomAttributeAllowed = val;
            return this;
        }

        public Builder withAttributesDataList(List<AttributeMeta> val) {
            attributesDataList = val;
            return this;
        }

        public Builder withInstanceLimiter(KernelConstants.EnumInstanceLimiter val) {
            instanceLimiter = val;
            return this;
        }

        public Builder withInstancePrefix(String val) {
            instancePrefix = val;
            return this;
        }

        public Builder withLocationSpecific(boolean val) {
            locationSpecific = val;
            return this;
        }

        public Builder withOrg(String val) {
            org = val;
            return this;
        }

        public Builder withPackagecodes(List<String> val) {
            packagecodes = val;
            return this;
        }

        public Builder withParameterType(boolean val) {
            parameterType = val;
            return this;
        }

        public Builder withStatus(KernelConstants.EnumDocStatusType val) {
            status = val;
            return this;
        }

        public Builder withSystemType(boolean val) {
            systemType = val;
            return this;
        }

        public Builder withCreatedDate(String val) {
            createdDate = val;
            return this;
        }

        public Builder withCreatedBy(String val) {
            createdBy = val;
            return this;
        }

        public Builder withModifiedDate(String val) {
            modifiedDate = val;
            return this;
        }

        public Builder withModifiedBy(String val) {
            modifiedBy = val;
            return this;
        }

        public ClassCEResponse build() {
            return new ClassCEResponse(this);
        }
    }
}

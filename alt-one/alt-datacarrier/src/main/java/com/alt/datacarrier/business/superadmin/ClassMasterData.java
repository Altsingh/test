package com.alt.datacarrier.business.superadmin;

public class ClassMasterData {

    private String className;

    private String classId;

    private String createdBy;

    private String createdDate;

    private String packageCodes;

    public String getPackageCodes() {
        return packageCodes;
    }

    public void setPackageCodes(String packageCodes) {
        this.packageCodes = packageCodes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    public ClassMasterData(){
    }

    public ClassMasterData(String classId,String className,String createdBy,String createdDate,
                           String packageCodes,String status){
        this.className=className;
        this.classId=classId;
        this.createdBy=createdBy;
        this.createdDate=createdDate;
        this.packageCodes=packageCodes;
        this.status=status;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}

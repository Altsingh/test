package com.alt.datacarrier.business.superadmin;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelClassProcessRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CreateNewClassSuperadminRequest implements Serializable {

	private static final long serialVersionUID = -9004293246303167917L;

	@JsonProperty("protocol")
	Protocol protocol;
	
	@JsonProperty("kernelClassProcessRequest")
	KernelClassProcessRequest kernelClassProcessRequest;

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public KernelClassProcessRequest getKernelClassProcessRequest() {
		return kernelClassProcessRequest;
	}

	public void setKernelClassProcessRequest(KernelClassProcessRequest kernelClassProcessRequest) {
		this.kernelClassProcessRequest = kernelClassProcessRequest;
	}
}

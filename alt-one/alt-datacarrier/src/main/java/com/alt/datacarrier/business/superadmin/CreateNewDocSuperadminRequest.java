package com.alt.datacarrier.business.superadmin;

import com.alt.datacarrier.business.superadmin.SuperAdminConstants.EnumDocumentType;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelClassProcessRequest;
import com.alt.datacarrier.kernel.common.KernelInstanceProcessRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


public class CreateNewDocSuperadminRequest implements Serializable {

	private static final long serialVersionUID = -9004293246303167917L;

	Protocol protocol;

	private EnumDocumentType type;

	private KernelClassProcessRequest kernelClassProcessRequest;

	private KernelInstanceProcessRequest instanceData;

	@JsonProperty("protocol")
	public Protocol getProtocol() {
		return protocol;
	}

	@JsonProperty("protocol")
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	@JsonProperty("kernelClassProcessRequest")
	public KernelClassProcessRequest getKernelClassProcessRequest() {
		return kernelClassProcessRequest;
	}

	@JsonProperty("kernelClassProcessRequest")
	public void setKernelClassProcessRequest(KernelClassProcessRequest kernelClassProcessRequest) {
		this.kernelClassProcessRequest = kernelClassProcessRequest;
	}

	@JsonProperty("instanceData")
	public KernelInstanceProcessRequest getInstanceData() {
		return instanceData;
	}

	@JsonProperty("instanceData")
	public void setInstanceData(KernelInstanceProcessRequest instanceData) {
		this.instanceData = instanceData;
	}

	@JsonProperty("type")
	public EnumDocumentType getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(EnumDocumentType type) {
		this.type = type;
	}

}

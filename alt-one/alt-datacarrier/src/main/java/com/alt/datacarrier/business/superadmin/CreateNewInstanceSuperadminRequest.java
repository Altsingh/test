package com.alt.datacarrier.business.superadmin;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelInstanceProcessRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;


public class CreateNewInstanceSuperadminRequest implements Serializable {

	private static final long serialVersionUID = -7128445582567323820L;

	@JsonProperty("protocol")
	private Protocol protocol;

	@JsonProperty("instanceData")
	private KernelInstanceProcessRequest instanceData;

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public KernelInstanceProcessRequest getInstanceData() {
		return instanceData;
	}

	public void setInstanceData(KernelInstanceProcessRequest instanceData) {
		this.instanceData = instanceData;
	}

}

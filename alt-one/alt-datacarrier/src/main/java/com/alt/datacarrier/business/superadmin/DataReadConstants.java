package com.alt.datacarrier.business.superadmin;

import java.util.HashMap;
import java.util.Map;

public class DataReadConstants {

	public enum EnumClassNames {
		ENTITY("Entity"), POLICY("Policy"), PACKAGE("Package"), ORGANIZATION("Organization"), TENANT("Tenant");

		private String name;

		EnumClassNames(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}	
	}

	public static Object getCount(String key){
		Map<String, Object> data = new HashMap();
		data.put("ENTITY_DATAREAD_COUNT", 2);
		data.put("POLICY_DATAREAD_COUNT", 1);
		data.put("PACKAGE_DATAREAD_COUNT", 1);
		data.put("ENTITY_ATTR1", "entityName");
		data.put("ENTITY_ATTR2", "entityDocId");
		data.put("ENTITY_ATTR1", "entityName");
		data.put("PACKAGE_ATTR1", "packageName");
		
		return data.get(key);
	}

}

package com.alt.datacarrier.business.superadmin;

public class DataReadResponse {

	private Object response;
	
	private Integer doccumentCount;

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

	public Integer getDoccumentCount() {
		return doccumentCount;
	}

	public void setDoccumentCount(Integer doccumentCount) {
		this.doccumentCount = doccumentCount;
	}
}

package com.alt.datacarrier.business.superadmin;

import java.io.Serializable;

import com.alt.datacarrier.business.superadmin.SuperAdminConstants.EnumDocumentType;
import com.alt.datacarrier.core.IndexingData;
import com.alt.datacarrier.core.Protocol;

public class FetchAllSuperadminRequest implements Serializable {
	
	private static final long serialVersionUID = 2526511948551864442L;
	
	private Protocol protocol;
	
	private EnumDocumentType type;
	
	private String docId;
	
	private IndexingData indexingData;

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public EnumDocumentType getType() {
		return type;
	}

	public void setType(EnumDocumentType type) {
		this.type = type;
	}

	public IndexingData getIndexingData() {
		return indexingData;
	}

	public void setIndexingData(IndexingData indexingData) {
		this.indexingData = indexingData;
	}
}

package com.alt.datacarrier.business.superadmin;

import com.alt.datacarrier.core.Protocol;

import java.io.Serializable;

public class GetInstanceDataRequest  implements Serializable {

	private static final long serialVersionUID = 4116924872700690977L;
	
	private Protocol protocol;
	
	private String classId;

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

}

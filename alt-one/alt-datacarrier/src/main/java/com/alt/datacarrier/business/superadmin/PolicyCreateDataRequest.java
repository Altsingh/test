package com.alt.datacarrier.business.superadmin;

import com.alt.datacarrier.business.superadmin.DataReadConstants.EnumClassNames;
import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.core.IndexingData;
import com.alt.datacarrier.core.Protocol;
import com.fasterxml.jackson.annotation.JsonProperty;


public class PolicyCreateDataRequest implements IRequestData {

	private static final long serialVersionUID = 4420895949420309403L;

	private Protocol protocol;

	private EnumClassNames className;
	
	private IndexingData indexcingData;

	@JsonProperty("protocol")
	public Protocol getProtocol() {
		return protocol;
	}

	@JsonProperty("protocol")
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	@JsonProperty("className")
	public EnumClassNames getClassName() {
		return className;
	}

	@JsonProperty("className")
	public void setClassName(EnumClassNames className) {
		this.className = className;
	}

	public IndexingData getIndexcingData() {
		return indexcingData;
	}

	public void setIndexcingData(IndexingData indexcingData) {
		this.indexcingData = indexcingData;
	}
}

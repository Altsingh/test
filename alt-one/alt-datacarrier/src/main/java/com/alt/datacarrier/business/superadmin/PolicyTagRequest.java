package com.alt.datacarrier.business.superadmin;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.core.Protocol;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class PolicyTagRequest implements IRequestData {

	private static final long serialVersionUID = -3561971157675381485L;

	private Protocol protocol;

	private String groupPolicyName;

	private String policyId;

	private List<String> empGroupId;

	private String entityId;

	private String packageId;

	@JsonProperty("protocol")
	public Protocol getProtocol() {
		return protocol;
	}

	@JsonProperty("protocol")
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	@JsonProperty("groupPolicyName")
	public String getGroupPolicyName() {
		return groupPolicyName;
	}

	@JsonProperty("groupPolicyName")
	public void setGroupPolicyName(String groupPolicyName) {
		this.groupPolicyName = groupPolicyName;
	}

	@JsonProperty("policyId")
	public String getPolicyId() {
		return policyId;
	}

	@JsonProperty("policyId")
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	@JsonProperty("empGroupId")
	public List<String> getEmpGroupId() {
		return empGroupId;
	}

	@JsonProperty("empGroupId")
	public void setEmpGroupId(List<String> empGroupId) {
		this.empGroupId = empGroupId;
	}

	@JsonProperty("entityId")
	public String getEntityId() {
		return entityId;
	}

	@JsonProperty("entityId")
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@JsonProperty("packageId")
	public String getPackageId() {
		return packageId;
	}

	@JsonProperty("packageId")
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

}

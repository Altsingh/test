package com.alt.datacarrier.business.superadmin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SinglePackageResponse {

	@JsonProperty("PackageName")
	private String packageName;
	
	@JsonProperty("PackageCode")
	private String packageCode;
	
	@JsonProperty("VersionId")
	private String versionId;
	
	@JsonProperty("ParentPackageCode")
	private String parentPackageCode;
	
	@JsonProperty("Type")
	private String type;
	
	@JsonProperty("InstanceCode")
	private String instanceCode;
	
	@JsonProperty("Class")
	private String className;

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getPackageCode() {
		return packageCode;
	}

	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getParentPackageCode() {
		return parentPackageCode;
	}

	public void setParentPackageCode(String parentPackageCode) {
		this.parentPackageCode = parentPackageCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getInstanceCode() {
		return instanceCode;
	}

	public void setInstanceCode(String instanceCode) {
		this.instanceCode = instanceCode;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

}

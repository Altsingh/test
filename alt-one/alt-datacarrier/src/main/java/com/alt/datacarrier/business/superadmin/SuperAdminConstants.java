package com.alt.datacarrier.business.superadmin;

public class SuperAdminConstants {

	public SuperAdminConstants(){
		// Do nothing
	}
	
	public enum EnumDocumentType {
		PACKAGE, CLASS, INSTANCE, CLASSMETA, PACKAGEMETA;
	}
}

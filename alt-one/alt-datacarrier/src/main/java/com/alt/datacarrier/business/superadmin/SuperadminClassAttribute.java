package com.alt.datacarrier.business.superadmin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SuperadminClassAttribute {

	@JsonProperty("name")
	private String className;
	
	@JsonProperty("datatype")
	private String dataType;
	
	@JsonProperty("parentClassCode")
	private String parentClassCode;
	
	@JsonProperty("isActive")
	private String isActive;
	
	@JsonProperty("isNullable")
	private String isNullable;
	
	@JsonProperty("attributeCode")
	private String attributeCode;
	
	@JsonProperty("isPlaceHolderType")
	private String isPlaceHolderType;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getParentClassCode() {
		return parentClassCode;
	}

	public void setParentClassCode(String parentClassCode) {
		this.parentClassCode = parentClassCode;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsNullable() {
		return isNullable;
	}

	public void setIsNullable(String isNullable) {
		this.isNullable = isNullable;
	}

	public String getAttributeCode() {
		return attributeCode;
	}

	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	public String getIsPlaceHolderType() {
		return isPlaceHolderType;
	}

	public void setIsPlaceHolderType(String isPlaceHolderType) {
		this.isPlaceHolderType = isPlaceHolderType;
	}

}

package com.alt.datacarrier.business.superadmin;


import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.SessionDetails;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties({"userDetail", "sessionDetails"})
public class ViewAllClassRequest {
	
	@JsonProperty("userDetail")
	private Protocol protocolTO;

	@JsonProperty("sessionDetails")
	private SessionDetails sessionDetails;

}

package com.alt.datacarrier.business.superadmin;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ViewAllClassResponse {

	@JsonProperty("CLASS")
	List<ViewSelectedClassResponse> classList;

	public List<ViewSelectedClassResponse> getClassList() {
		return classList;
	}

	public void setClassList(List<ViewSelectedClassResponse> classList) {
		this.classList = classList;
	}
	
	
}

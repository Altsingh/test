package com.alt.datacarrier.business.superadmin;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ViewAllPackageResponse {
	
	@JsonProperty("INSTANCE_PACKAGE")
	List<SinglePackageResponse> packages;

	public List<SinglePackageResponse> getpackages() {
		return packages;
	}

	public void setpackages(List<SinglePackageResponse> packageList) {
		this.packages = packageList;
	}

}

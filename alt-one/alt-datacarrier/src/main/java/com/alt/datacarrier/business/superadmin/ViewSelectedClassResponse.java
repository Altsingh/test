package com.alt.datacarrier.business.superadmin;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ViewSelectedClassResponse {

	@JsonProperty("name")
	private String className;
	
	@JsonProperty("datatype")
	private String datatype;
	
	@JsonProperty("isActive")
	private String isActive;
	
	@JsonProperty("iscountryspecific")
	private String iscountryspecific;
	
	@JsonProperty("isSystemType")
	private String isSystemType;
	
	@JsonProperty("classCode")
	private String classCode;
	
	@JsonProperty("packageCode")
	private String packageCode;
	
	@JsonProperty("attributes")
	List<SuperadminClassAttribute> attributeList;
	
	
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIscountryspecific() {
		return iscountryspecific;
	}

	public void setIscountryspecific(String iscountryspecific) {
		this.iscountryspecific = iscountryspecific;
	}

	public String getIsSystemType() {
		return isSystemType;
	}

	public void setIsSystemType(String isSystemType) {
		this.isSystemType = isSystemType;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getPackageCode() {
		return packageCode;
	}

	public void setPackageCode(String packageCode) {
		this.packageCode = packageCode;
	}

}

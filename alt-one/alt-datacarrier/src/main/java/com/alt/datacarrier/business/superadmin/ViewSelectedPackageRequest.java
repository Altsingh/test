package com.alt.datacarrier.business.superadmin;


import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.SessionDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ViewSelectedPackageRequest {

	@JsonIgnore
	@JsonProperty("userDetail")
	private Protocol protocolTO;

	@JsonIgnore
	@JsonProperty("sessionDetails")
	private SessionDetails sessionDetails;
}

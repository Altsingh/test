package com.alt.datacarrier.business.superadmin;


import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.SessionDetails;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ViewSelectedclassRequest {
	
	@JsonIgnore
	@JsonProperty("userDetail")
	private Protocol protocolTO;

	@JsonIgnore
	@JsonProperty("sessionDetails")
	private SessionDetails sessionDetails;
	
	@JsonProperty("classCode")
	private String classCode;

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	

}

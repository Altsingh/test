package com.alt.datacarrier.business.superadmin.app;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.fasterxml.jackson.annotation.JsonProperty;


public class AppCERequest {
	
	private Protocol protocol;

	private EnumCRUDOperations action;

	private Application app;

	public AppCERequest() {
	}

	public AppCERequest(Protocol protocol, EnumCRUDOperations action, Application app) {
		super();
		this.protocol = protocol;
		this.action = action;
		this.app = app;
	}

	@JsonProperty("protocol")
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	@JsonProperty("action")
	public void setAction(EnumCRUDOperations action) {
		this.action = action;
	}

	@JsonProperty("app")
	public void setApp(Application app) {
		this.app = app;
	}

	@JsonProperty("protocol")
	public Protocol getProtocol() {
		return protocol;
	}

	@JsonProperty("action")
	public EnumCRUDOperations getAction() {
		return action;
	}

	@JsonProperty("app")
	public Application getApp() {
		return app;
	}

}

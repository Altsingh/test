package com.alt.datacarrier.business.superadmin.app;

import java.util.List;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = AppData.class)
public class AppData {

	@JsonProperty("appName")
	private String appName;

	@JsonProperty("appCode")
	private String appCode;

	@JsonProperty("createdBy")
	private String createdBy;

	@JsonProperty("createdDate")
	private String createdDate;

	private EnumDocStatusType status;

	@JsonProperty("appId")
	private String appId;

	@JsonProperty("appLogoUrl")
	private String appLogoUrl;

	private List<Module> modules;

	private List<HomeBuilderRequestTO> homeCards;

	public AppData() {
	}

	private AppData(Builder builder) {
		appName = builder.appName;
		appCode = builder.appCode;
		appId = builder.appId;
		status = builder.status;
		createdBy = builder.createdBy;
		createdDate = builder.createdDate;
		appLogoUrl = builder.appLogoUrl;
		modules = builder.modules;
		homeCards = builder.homeCards;
	}

	public static final class Builder {
		public String appLogoUrl;
		private String appName;
		private String appCode;
		private String appId;
		private EnumDocStatusType status;
		private String createdBy;
		private String createdDate;
		private List<Module> modules;
		private List<HomeBuilderRequestTO> homeCards;

		public Builder() {
		}

		public Builder withAppName(String val) {
			appName = val;
			return this;
		}

		public Builder withAppCode(String val) {
			appCode = val;
			return this;
		}

		public Builder withAppId(String val) {
			appId = val;
			return this;
		}

		public Builder withStatus(EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withCreatedBy(String val) {
			createdBy = val;
			return this;
		}

		public Builder withCreatedDate(String val) {
			createdDate = val;
			return this;
		}

		public Builder withAppLogoUrl(String val) {
			appLogoUrl = val;
			return this;
		}

		public Builder withModules(List<Module> list) {
			modules = list;
			return this;
		}

		public Builder withHomeCards(List<HomeBuilderRequestTO> list) {
			homeCards = list;
			return this;
		}

		public AppData build() {
			return new AppData(this);
		}
	}

	public String getAppName() {
		return appName;
	}

	public String getAppCode() {
		return appCode;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public String getAppId() {
		return appId;
	}

	public String getAppLogoUrl() {
		return appLogoUrl;
	}

	public void setAppLogoUrl(String appLogoUrl) {
		this.appLogoUrl = appLogoUrl;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public List<HomeBuilderRequestTO> getHomeCards() {
		return homeCards;
	}

	public void setHomeCards(List<HomeBuilderRequestTO> homeCards) {
		this.homeCards = homeCards;
	}

}

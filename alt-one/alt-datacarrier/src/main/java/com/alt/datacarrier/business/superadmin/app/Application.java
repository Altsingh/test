package com.alt.datacarrier.business.superadmin.app;

import java.util.List;

import com.alt.datacarrier.employee.EmployeeResponseTO;
import com.alt.datacarrier.organization.RoleTO;

public class Application {

	private String appName;

	private String appCode;

	private String appId;

	private List<Module> modules;
	
	private List<HomeBuilderRequestTO> homeCards;

	private String cas;

	private String tenantId;

	private String appLogoUrl;

	private Annoucement announcement;

	private EmployeeResponseTO employeeInfo;

	private List<Integer> classIds;

	private List<RoleTO> roles;
	
	private Boolean isApplicationForDeletion;
	
	private Boolean isApplicationForEditing;

	public Application() {
	}

	private Application(Builder builder) {
		appName = builder.appName;
		appCode = builder.appCode;
		setAppId(builder.appId);
		modules = builder.modules;
		homeCards = builder.homeCards;
		cas = builder.cas;
		tenantId = builder.tenantId;
		appLogoUrl = builder.appLogoUrl;
		announcement = builder.announcement;
		employeeInfo = builder.employeeInfo;
		classIds = builder.classIds;
		roles = builder.roles;
		isApplicationForDeletion = builder.isApplicationForDeletion;
		isApplicationForEditing = builder.isApplicationForEditing;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}

	public static final class Builder {
		public String appLogoUrl;
		public String tenantId;
		private String appName;
		private String appCode;
		private String appId;
		private String cas;
		private List<Module> modules;
		private List<HomeBuilderRequestTO> homeCards;
		private Annoucement announcement;
		private EmployeeResponseTO employeeInfo;
		private List<Integer> classIds;
		private List<RoleTO> roles;
		private Boolean isApplicationForDeletion;
		private Boolean isApplicationForEditing;

		public Builder() {
		}

		public Builder withAppName(String val) {
			appName = val;
			return this;
		}

		public Builder withAppCode(String val) {
			appCode = val;
			return this;
		}

		public Builder withAppId(String val) {
			appId = val;
			return this;
		}

		public Builder withCas(String val) {
			cas = val;
			return this;
		}

		public Builder withModules(List<Module> val) {
			modules = val;
			return this;
		}
		
		public Builder withHomeCards(List<HomeBuilderRequestTO> val) {
			homeCards = val;
			return this;
		}

		public Builder withTenantId(String val) {
			tenantId = val;
			return this;
		}

		public Builder withAppLogoUrl(String val) {
			appLogoUrl = val;
			return this;
		}

		public Builder withAnnouncement(Annoucement val) {
			announcement = val;
			return this;
		}

		public Builder withEmployeeInfo(EmployeeResponseTO val) {
			employeeInfo = val;
			return this;
		}

		public Builder withClassIds(List<Integer> val) {
			classIds = val;
			return this;
		}

		public Builder withRoles(List<RoleTO> val) {
			roles = val;
			return this;
		}
		
		public Builder withIsApplicationForDeletion(Boolean val) {
			isApplicationForDeletion = val;
			return this;
		}
		
		public Builder withIsApplicationForEditing(Boolean val) {
			isApplicationForEditing = val;
			return this;
		}

		public Application build() {
			return new Application(this);
		}
	}

	public String getAppName() {
		return appName;
	}

	public String getAppCode() {
		return appCode;
	}

	public String getAppId() {
		return appId;
	}

	public List<Module> getModules() {
		return modules;
	}

	public String getCas() {
		return cas;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getAppLogoUrl() {
		return appLogoUrl;
	}

	public void setAppLogoUrl(String appLogoUrl) {
		this.appLogoUrl = appLogoUrl;
	}

	public Annoucement getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(Annoucement announcement) {
		this.announcement = announcement;
	}

	public EmployeeResponseTO getEmployeeInfo() {
		return employeeInfo;
	}

	public void setEmployeeInfo(EmployeeResponseTO employeeInfo) {
		this.employeeInfo = employeeInfo;
	}

	public List<Integer> getClassIds() {
		return classIds;
	}

	public void setClassIds(List<Integer> classIds) {
		this.classIds = classIds;
	}

	public List<RoleTO> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleTO> roles) {
		this.roles = roles;
	}

	public Boolean getIsApplicationForDeletion() {
		return isApplicationForDeletion;
	}

	public void setIsApplicationForDeletion(Boolean isApplicationForDeletion) {
		this.isApplicationForDeletion = isApplicationForDeletion;
	}

	public Boolean getIsApplicationForEditing() {
		return isApplicationForEditing;
	}

	public void setIsApplicationForEditing(Boolean isApplicationForEditing) {
		this.isApplicationForEditing = isApplicationForEditing;
	}

	public List<HomeBuilderRequestTO> getHomeCards() {
		return homeCards;
	}

	public void setHomeCards(List<HomeBuilderRequestTO> homeCards) {
		this.homeCards = homeCards;
	}

}

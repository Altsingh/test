package com.alt.datacarrier.business.superadmin.app;

import java.io.Serializable;
import java.util.List;

import com.alt.datacarrier.organization.RoleTO;

public class AttachedForm implements Serializable{

	public String formName;

	public String formId;

	private String formSecurityId;

	private List<RoleTO> assignedRoles;

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getFormSecurityId() {
		return formSecurityId;
	}

	public void setFormSecurityId(String formSecurityId) {
		this.formSecurityId = formSecurityId;
	}

	public List<RoleTO> getAssignedRoles() {
		return assignedRoles;
	}

	public void setAssignedRoles(List<RoleTO> assignedRoles) {
		this.assignedRoles = assignedRoles;
	}

}

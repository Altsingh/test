package com.alt.datacarrier.business.superadmin.app;

import com.alt.datacarrier.core.Protocol;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HomeBuilderRequestTO implements Serializable{

	@JsonProperty("title")
	private String title;

	@JsonProperty("description")
	private String description;

	@JsonProperty("image")
	private String image;

	public HomeBuilderRequestTO() {}
	
	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getImage() {
		return image;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public static final class HomeBuilderRequestTOBuilder {
		private String title;
		private String description;
		private String image;

		public HomeBuilderRequestTOBuilder() {
		}

		public static HomeBuilderRequestTOBuilder aHomeBuilderRequestTO() {
			return new HomeBuilderRequestTOBuilder();
		}

		public HomeBuilderRequestTOBuilder withTitle(String title) {
			this.title = title;
			return this;
		}

		public HomeBuilderRequestTOBuilder withDescription(String description) {
			this.description = description;
			return this;
		}

		public HomeBuilderRequestTOBuilder withImage(String image) {
			this.image = image;
			return this;
		}

		public HomeBuilderRequestTO build() {
			HomeBuilderRequestTO homeBuilderRequestTO = new HomeBuilderRequestTO();
			homeBuilderRequestTO.setTitle(title);
			homeBuilderRequestTO.setDescription(description);
			homeBuilderRequestTO.setImage(image);
			return homeBuilderRequestTO;
		}
	}
}

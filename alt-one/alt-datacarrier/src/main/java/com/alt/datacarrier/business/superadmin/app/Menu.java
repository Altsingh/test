package com.alt.datacarrier.business.superadmin.app;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Menu implements Serializable{

	@JsonProperty("name")
	private String name;

	@JsonProperty("uiClass")
	private String uiClass;

	@JsonProperty("androidIcon")
	private String androidIcon;

	@JsonProperty("iosIcon")
	private String iosIcon;

	@JsonProperty("webIcon")
	private String webIcon;

	@JsonProperty("webSequence")
	private Integer webSequence;

	@JsonProperty("mobileSequence")
	private Integer mobileSequence;

	@JsonProperty("menus")
	private List<Menu> menus;

	@JsonProperty("form")
	private AttachedForm form;

	private Boolean permitted;

	private Boolean landingPage;

	public Menu() {
	}

	private Menu(Builder builder) {
		name = builder.name;
		uiClass = builder.uiClass;
		androidIcon = builder.androidIcon;
		iosIcon = builder.iosIcon;
		webIcon = builder.webIcon;
		webSequence = builder.webSequence;
		mobileSequence = builder.mobileSequence;
		menus = builder.menus;
		form = builder.form;
		permitted = builder.permitted;
	}

	public Boolean getLandingPage() {
		return landingPage;
	}

	public void setLandingPage(Boolean landingPage) {
		this.landingPage = landingPage;
	}

	public String getName() {
		return name;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public AttachedForm getForm() {
		return form;
	}

	public void setForm(AttachedForm form) {
		this.form = form;
	}

	public Boolean getPermitted() {
		return permitted;
	}

	public void setPermitted(Boolean permitted) {
		this.permitted = permitted;
	}

	public static final class Builder {
		private String name;
		private String uiClass;
		private String androidIcon;
		private String iosIcon;
		private String webIcon;
		private Integer webSequence;
		private Integer mobileSequence;
		private List<Menu> menus;
		private AttachedForm form;
		private Boolean permitted;

		public Builder() {
		}

		public Builder name(String val) {
			name = val;
			return this;
		}

		public Builder uiClass(String val) {
			uiClass = val;
			return this;
		}

		public Builder androidIcon(String val) {
			androidIcon = val;
			return this;
		}

		public Builder iosIcon(String val) {
			iosIcon = val;
			return this;
		}

		public Builder webIcon(String val) {
			webIcon = val;
			return this;
		}

		public Builder webSequence(Integer val) {
			webSequence = val;
			return this;
		}

		public Builder mobileSequence(Integer val) {
			mobileSequence = val;
			return this;
		}

		public Builder menus(List<Menu> val) {
			menus = val;
			return this;
		}

		public Builder form(AttachedForm val) {
			form = val;
			return this;
		}

		public Builder permitted(Boolean val) {
			permitted = val;
			return this;
		}

		public Menu build() {
			return new Menu(this);
		}
	}


}

package com.alt.datacarrier.business.superadmin.app;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;


public class MenuGroups implements Serializable{

	@JsonProperty("name")
	private String name;
	// private String icon;

	@JsonProperty("menus")
	private List<Menu> menus;
	
	public MenuGroups() {}

	public MenuGroups(String name, List<Menu> menus) {
		this.name = name;
		this.menus = menus;
	}

	public String getName() {
		return name;
	}

	public List<Menu> getMenus() {
		return menus;
	}

}

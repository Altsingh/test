package com.alt.datacarrier.business.superadmin.app;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Module implements Serializable{

	@JsonProperty("name")
	private String name;

	@JsonProperty("oldname")
	private String oldname;

	@JsonProperty("iconPath")
	private String iconPath;

	@JsonProperty("sequence")
	private Integer sequence;

	@JsonProperty("menus")
	private List<Menu> menus;

	@JsonProperty("menuGroups")
	private List<MenuGroups> menuGroups;

	@JsonProperty("form")
	private AttachedForm form;

	private Boolean permitted;

	private Module(Builder builder) {
		setName(builder.name);
		setOldname(builder.oldname);
		setIconPath(builder.iconPath);
		setSequence(builder.sequence);
		setMenus(builder.menus);
		setMenuGroups(builder.menuGroups);
		setForm(builder.form);
		setPermitted(builder.permitted);
	}

	public String getName() {
		return name;
	}

	public String getIconPath() {
		return iconPath;
	}

	public Integer getSequence() {
		return sequence;
	}

	public List<Menu> getMenus() {
		return menus;
	}

	public List<MenuGroups> getMenuGroups() {
		return menuGroups;
	}

	public String getOldname() {
		return oldname;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Module() {
	}

	public void setOldname(String oldname) {
		this.oldname = oldname;
	}

	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	public void setMenuGroups(List<MenuGroups> menuGroups) {
		this.menuGroups = menuGroups;
	}

	public AttachedForm getForm() {
		return form;
	}

	public void setForm(AttachedForm form) {
		this.form = form;
	}

	public Boolean getPermitted() {
		return permitted;
	}

	public void setPermitted(Boolean permitted) {
		this.permitted = permitted;
	}

	public static final class Builder {
		private String name;
		private String oldname;
		private String iconPath;
		private Integer sequence;
		private List<Menu> menus;
		private List<MenuGroups> menuGroups;
		private AttachedForm form;
		private Boolean permitted;

		public Builder() {
		}

		public Builder withName(String val) {
			name = val;
			return this;
		}

		public Builder withOldname(String val) {
			oldname = val;
			return this;
		}

		public Builder withIconPath(String val) {
			iconPath = val;
			return this;
		}

		public Builder withSequence(Integer val) {
			sequence = val;
			return this;
		}

		public Builder withMenus(List<Menu> val) {
			menus = val;
			return this;
		}

		public Builder withMenuGroups(List<MenuGroups> val) {
			menuGroups = val;
			return this;
		}

		public Builder withForm(AttachedForm val) {
			form = val;
			return this;
		}

		public Builder withPermitted(Boolean val) {
			permitted = val;
			return this;
		}

		public Module build() {
			return new Module(this);
		}
	}
}

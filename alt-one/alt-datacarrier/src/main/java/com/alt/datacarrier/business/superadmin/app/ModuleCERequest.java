package com.alt.datacarrier.business.superadmin.app;

import com.alt.datacarrier.core.Protocol;

import java.util.List;


public class ModuleCERequest {

	private Protocol protocol;
	private String appId;
	private String cas;
	private List<ModuleRequest> moduleRequest;

	public Protocol getProtocol() {
		return protocol;
	}

	public String getAppId() {
		return appId;
	}

	public List<ModuleRequest> getModuleRequest() {
		return moduleRequest;
	}

	public String getCas() {
		return cas;
	}

	public ModuleCERequest(Protocol protocol, String appId, List<ModuleRequest> moduleRequest, String cas){
		this.protocol = protocol;
		this.appId = appId;
		this.moduleRequest = moduleRequest;
		this.cas = cas;
	}

	public ModuleCERequest() {
	}
}

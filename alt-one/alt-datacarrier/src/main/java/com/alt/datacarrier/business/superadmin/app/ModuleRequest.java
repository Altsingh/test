package com.alt.datacarrier.business.superadmin.app;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.fasterxml.jackson.annotation.JsonProperty;


public class ModuleRequest {

	@JsonProperty("action")
	private EnumCRUDOperations action;

	@JsonProperty("module")
	private Module module;

	public ModuleRequest(EnumCRUDOperations action, Module module) {
		this.action = action;
		this.module = module;
	}

	public ModuleRequest() {}

	public EnumCRUDOperations getAction() {
		return action;
	}

	public Module getModule() {
		return module;
	}
}

package com.alt.datacarrier.business.user;


public class AllUsersRead {

	private User userTo;

	private UserFilter userFilterTo;

	private UserSort userSortTo;

	public User getUserTo() {
		return userTo;
	}

	public void setUserTo(User userTo) {
		this.userTo = userTo;
	}

	public UserFilter getUserFilterTo() {
		return userFilterTo;
	}

	public void setUserFilterTo(UserFilter userFilterTo) {
		this.userFilterTo = userFilterTo;
	}

	public UserSort getUserSortTo() {
		return userSortTo;
	}

	public void setUserSortTo(UserSort userSortTo) {
		this.userSortTo = userSortTo;
	}
}

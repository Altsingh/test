package com.alt.datacarrier.business.user;

import java.util.List;

public class UsersBusinessRequest{

	private List<String> fields;

	private UserFilter userFilterTo;

	private UserSort userSortTo;

	public List<String> getFields() {
		return fields;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	public UserFilter getUserFilterTo() {
		return userFilterTo;
	}

	public void setUserFilterTo(UserFilter userFilterTo) {
		this.userFilterTo = userFilterTo;
	}

	public UserSort getUserSortTo() {
		return userSortTo;
	}

	public void setUserSortTo(UserSort userSortTo) {
		this.userSortTo = userSortTo;
	}	
}

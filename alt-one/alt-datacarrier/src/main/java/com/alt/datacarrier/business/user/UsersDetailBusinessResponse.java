package com.alt.datacarrier.business.user;

import java.util.List;
import java.util.Map;

import com.alt.datacarrier.core.OutputValueObject;

public class UsersDetailBusinessResponse {

	private List<Map<String, OutputValueObject>> userList;
	
	public List<Map<String, OutputValueObject>> getuserList() {
		return userList;
	}

	public void setUserList(List<Map<String, OutputValueObject>> userList) {
		this.userList = userList;
	}

}

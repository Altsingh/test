package com.alt.datacarrier.business.user.login;

import com.alt.datacarrier.business.superadmin.app.AppData;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * @author vaibhav.kashyap
 * */

@JsonDeserialize(as = SecureLoginModel.class)
public class SecureLoginModel {
	@JsonProperty("accesstoken")
	private String accesstoken;
	
	@JsonProperty("refreshtoken")
	private String refreshtoken;

	@JsonProperty("username")
	private String userName;
	
	@JsonProperty("received")
	private boolean received;
	
	@JsonProperty("authtoken")
	private String authtoken;
	
	@JsonProperty("appCode")
	private String appCode;
	
	@JsonProperty("custom")
	private boolean custom;
	
	@JsonProperty("appdata")
	private AppData appData;
	
	@JsonProperty("orgId")
	private String orgId;
	
	@JsonProperty("userId")
	private String userId;
	
	@JsonProperty("photoPath")
	private String photoPath;

	private String appId;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public SecureLoginModel(String accesstoken, String refreshtoken, String userName, boolean received) {
		this.refreshtoken = refreshtoken;
		this.accesstoken = accesstoken;
		this.userName = userName;
		this.received = received;
	}
	
	public SecureLoginModel() {
		
	}

	public String getAccesstoken() {
		return accesstoken;
	}

	public void setAccesstoken(String accesstoken) {
		this.accesstoken = accesstoken;
	}

	public String getRefreshtoken() {
		return refreshtoken;
	}

	public void setRefreshtoken(String refreshtoken) {
		this.refreshtoken = refreshtoken;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isReceived() {
		return received;
	}

	public void setReceived(boolean received) {
		this.received = received;
	}

	public String getAuthtoken() {
		return authtoken;
	}

	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public boolean isCustom() {
		return custom;
	}

	public void setCustom(boolean custom) {
		this.custom = custom;
	}

	public AppData getAppData() {
		return appData;
	}

	public void setAppData(AppData appData) {
		this.appData = appData;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}
}

package com.alt.datacarrier.common.enumeration;

public enum EnumDataType {
	
	INTEGER("INTEGER"), DOUBLE("DOUBLE"), FLOAT("FLOAT"), BOOLEAN("BOOLEAN"), TIMESTAMP("TIMESTAMP"), LONG("LONG"), 
		STRING("STRING"), REFERENCE("REFERENCE"), REFERENCELIST("REFERENCELIST"), LIST("LIST<?>"),
			MAP("MAP<?,?>") ,SELFREFERENCE("SELFREFERENCE"), GLOBALREFERENCE("GLOBALREFERENCE"), DATE("DATE"), BLANK("BLANK"), IMAGE("IMAGE"), TEXTAREA("TEXTAREA") ;

    private final String name;

    private EnumDataType(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return this.name;
    }
}

package com.alt.datacarrier.common.enumeration;

public enum EnumFilterOption {
	A("AND"),O("OR");
	
	private final String name;
	
	private EnumFilterOption(String name) {
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
}

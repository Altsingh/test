package com.alt.datacarrier.common.enumeration;

public enum EnumJoinType {

	JOIN("join"), LEFTJOIN("left join"),
	RIGHTJOIN("right join"), FULLJOIN("full join");
	
	private final String name;
	
	EnumJoinType(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}

}
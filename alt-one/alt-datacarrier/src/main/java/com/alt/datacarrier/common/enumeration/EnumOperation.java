package com.alt.datacarrier.common.enumeration;

public enum EnumOperation {
	I("INSERT"), U("UPDATE"), D("DELETE"), R("READ");
	
	private final String name;
	
	private EnumOperation(String name) {
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
}

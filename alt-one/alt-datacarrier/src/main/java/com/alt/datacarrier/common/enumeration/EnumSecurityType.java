package com.alt.datacarrier.common.enumeration;

public enum EnumSecurityType {
	DENY,MANDATORY,VIEWABLE;
	
	private EnumSecurityType() {
	}
	
}

package com.alt.datacarrier.common.enumeration;

public enum EnumSortDirection {
	ASC, DESC
}

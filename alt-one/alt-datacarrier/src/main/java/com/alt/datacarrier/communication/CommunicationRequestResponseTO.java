package com.alt.datacarrier.communication;

import java.io.Serializable;
import java.util.List;

import com.alt.datacarrier.formsecurity.ResponseTO;

public class CommunicationRequestResponseTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String action;

	private Integer organizationId;

	private Integer tenantId;

	private ResponseTO message;

	private MailConfigTO mailConfig;

	private List<Integer> selectMailIds;

	private List<MailConfigTO> mailConfigs;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public Integer getTenantId() {
		return tenantId;
	}

	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	public ResponseTO getMessage() {
		return message;
	}

	public void setMessage(ResponseTO message) {
		this.message = message;
	}

	public MailConfigTO getMailConfig() {
		return mailConfig;
	}

	public void setMailConfig(MailConfigTO mailConfig) {
		this.mailConfig = mailConfig;
	}

	public List<Integer> getSelectMailIds() {
		return selectMailIds;
	}

	public void setSelectMailIds(List<Integer> selectMailIds) {
		this.selectMailIds = selectMailIds;
	}

	public List<MailConfigTO> getMailConfigs() {
		return mailConfigs;
	}

	public void setMailConfigs(List<MailConfigTO> mailConfigs) {
		this.mailConfigs = mailConfigs;
	}

}

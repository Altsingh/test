package com.alt.datacarrier.core;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AbstractDocument {

	
	private Long createdDate;
	
	private Long modifiedDate;
	
	private String createdBy;
	
	private String modifiedBy;

	
	@JsonProperty("createdDate")
	public Long getCreatedDate() {
		return createdDate;
	}
	@JsonProperty("createDate")
	public void setCreatedDate(Long createdDate) {
		this.createdDate = createdDate;
	}
	@JsonProperty("modifiedDate")
	public Long getModifiedDate() {
		return modifiedDate;
	}
	@JsonProperty("modifiedDate")
	public void setModifiedDate(Long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@JsonProperty("createdBy")
	public String getCreatedBy() {
		return createdBy;
	}
	@JsonProperty("createdBy")
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@JsonProperty("modifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	@JsonProperty("modifiedBy")
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	
	
	
	
	
}

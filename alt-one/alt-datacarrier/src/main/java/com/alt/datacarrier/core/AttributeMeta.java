package com.alt.datacarrier.core;

import java.io.Serializable;


import com.alt.datacarrier.common.enumeration.EnumDataType;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AttributeMeta implements Serializable {

	private static final long serialVersionUID = -5352550890847305034L;

	private String attributeCode;

	private String attributeName;

	private EnumDataType dataType; // REFERENCE, DATE, STRING, DOUBLE, INTEGER etc

	private String referenceClassCode; // When dataType is REFERENCE

	private EnumDocStatusType status;

	private Boolean placeholderType;

	private String defaultValue;

	private AttributeValidator validator;
	
	private Boolean unique;
	
	private Boolean mandatory;	
	
	private Boolean searchable;
	
	private Boolean indexable;
	
	private Boolean displayUnit;
	
	private Integer displaySequence;
	
	private ClassMeta refClassMeta;

	@JsonProperty("attributeCode")
	public String getAttributeCode() {
		return attributeCode;
	}

	@JsonProperty("attributeCode")
	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	@JsonProperty("attributeName")
	public String getAttributeName() {
		return attributeName;
	}

	@JsonProperty("attributeName")
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	@JsonProperty("referenceClassCode")
	public String getReferenceClassCode() {
		return referenceClassCode;
	}

	@JsonProperty("referenceClassCode")
	public void setReferenceClassCode(String referenceClassCode) {
		this.referenceClassCode = referenceClassCode;
	}

    @JsonProperty("placeholderType")
	public Boolean getPlaceholderType() {
		return placeholderType;
	}
    
    @JsonProperty("placeholderType")
	public void setPlaceholderType(Boolean placeholderType) {
		this.placeholderType = placeholderType;
	}
    
    @JsonProperty("defaultValue")
	public String getDefaultValue() {
		return defaultValue;
	}
    
    @JsonProperty("defaultValue")
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

    @JsonProperty("validator")
	public AttributeValidator getValidator() {
		return validator;
	}
    
    @JsonProperty("validator")
	public void setValidator(AttributeValidator validator) {
		this.validator = validator;
	}
    
    @JsonProperty("unique")
	public Boolean getUnique() {
		return unique;
	}
    
    @JsonProperty("unique")
	public void setUnique(Boolean unique) {
		this.unique = unique;
	}
    
    @JsonProperty("mandatory")
	public Boolean getMandatory() {
		return mandatory;
	}
    
    @JsonProperty("mandatory")
	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

    @JsonProperty("dataType")
	public EnumDataType getDataType() {
		return dataType;
	}

    @JsonProperty("dataType")
	public void setDataType(EnumDataType dataType) {
		this.dataType = dataType;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	public Boolean getSearchable() {
		return searchable;
	}

	public void setSearchable(Boolean searchable) {
		this.searchable = searchable;
	}

	public Boolean getIndexable() {
		return indexable;
	}

	public void setIndexable(Boolean indexable) {
		this.indexable = indexable;
	}

	public Boolean getDisplayUnit() {
		return displayUnit;
	}

	public void setDisplayUnit(Boolean displayUnit) {
		this.displayUnit = displayUnit;
	}

	public Integer getDisplaySequence() {
		return displaySequence;
	}

	public void setDisplaySequence(Integer displaySequence) {
		this.displaySequence = displaySequence;
	}
	
    @JsonProperty("refClassMeta")
	public ClassMeta getRefClassMeta() {
		return refClassMeta;
	}

    @JsonProperty("refClassMeta")
	public void setRefClassMeta(ClassMeta refClassMeta) {
		this.refClassMeta = refClassMeta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attributeName == null) ? 0 : attributeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		AttributeMeta other = (AttributeMeta) obj;
		if (attributeName == null) {
			if (other.attributeName != null){
				return false;
			}
		} else if (!attributeName.equals(other.attributeName)){
			return false;
		}
		return true;
	}

}
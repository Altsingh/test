/**
 * 
 */
package com.alt.datacarrier.core;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author rahul.chabba
 *
 */
public class AttributeValidator implements Serializable {
	private static final long serialVersionUID = 1L;

	private String regex;

	private Double minvalue;

	private Double maxvalue;

	private Integer precision;

	@JsonProperty("regex")
	public String getRegex() {
		return regex;
	}

	@JsonProperty("regex")
	public void setRegex(String regex) {
		this.regex = regex;
	}

	@JsonProperty("minvalue")
	public Double getMinvalue() {
		return minvalue;
	}

	@JsonProperty("minvalue")
	public void setMinvalue(Double minvalue) {
		this.minvalue = minvalue;
	}

	@JsonProperty("maxvalue")
	public Double getMaxvalue() {
		return maxvalue;
	}

	@JsonProperty("maxvalue")
	public void setMaxvalue(Double maxvalue) {
		this.maxvalue = maxvalue;
	}

	@JsonProperty("precision")
	public Integer getPrecision() {
		return precision;
	}

	@JsonProperty("precision")
	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

}

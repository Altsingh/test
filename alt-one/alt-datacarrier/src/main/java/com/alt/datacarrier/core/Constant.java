package com.alt.datacarrier.core;

public class Constant {

	public static final String HEADER_ORG = "organization";
	
	public static final String HEADER_MODULE = "module";
	
	public static final String HEADER_ACTION = "action";
	
	public static final String HEADER_PLATFORM = "platform";
	
	public static final String DOMAIN = "domain";
	
	public static final String PATH = "path";
	
	public static final String HEADER_PORTALTYPE = "portaltype";
	
	public static final String CONSTANT_TILDE = "#";
	
	public static final String CONSTANT_COLON = ":";
	
	public static final String CONSTANT_SLASH = "/";
	
	public static final String CONSTANT_HYPHEN = "-";	

	public static final String CONSTANT_BACKTICK = "`";	
	
	public static final String CONSTANT_UNDERSCORE = "_";	
	
	public static final String CONSTANT_PERIOD = ".";
	
	public static final String CONSTANT_NAME_IDENTIFIER_ORG = "org-";
	
	public static final String CONSTANT_CLASS_IDENTIFIER = "class-";
			
	public static final String CONSTANT_INSTANCE_IDENTIFIER = "instance-";
	
	public static final String CONSTANT_FORM_IDENTIFIER = "form-";

	public static final String CONSTANT_ATTRIBUTE_IDENTIFIER = "attribute-";
	
	public static final String CONSTANT_ATTRIBUTE_PERIOD = "attribute.";
	
	public static final String CONSTANT_ATTRIBUTE_VALUE_IDENTIFIER = "attributevalue-";

	public static final String CONSTANT_CLASSNAME_IDENTIFIER = "classname-";
	
	public static final String CONSTANT_TRANSACTION_REQUEST = "txn-request-";
	
	public static final String CONSTANT_TRANSACTION_TMP = "txn-tmp-";
	
	public static final String CONSTANT_CLASSCODE = "classCode";
	
	public static final String CONSTANT_SUCCESS = "success";
	
}

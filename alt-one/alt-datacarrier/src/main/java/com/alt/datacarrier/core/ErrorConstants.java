package com.alt.datacarrier.core;

public final class ErrorConstants {
	
	private ErrorConstants(){
	}

	public final static String CONSTANT_ERROR_DUPLICATE_CLASSNAME_MSG = "Duplicate class name";
	
	public final static String CONSTANT_ERROR_DUPLICATE_UNIQUE_ATTRIBUTE_VALUE_MSG = "Duplicate unique attribute value";
	
	public final static String CONSTANT_ERROR_PARENT_CLASS_NOT_FOUND_MSG = "Parent class of this instance not found";
	
	public final static String CONSTANT_ERROR_ATTRIBUTE_CODE_NOT_FOUND_IN_CLASS_MSG = "Attribute name not present in parent class";
	
	public final static String CONSTANT_INSTANCE_REFERENCE_VALUE_NOT_CORRECT_MSG = "Instance reference value not correct";

	public final static String CONSTANT_RESPONSE_CODE_INSTANCE_REFERENCE_ERROR = "Error-214";

	public final static String CONSTANT_ERROR_DUPLICATE_DOCUMENT_MSG = "Duplicate documents found in request";

	public final static String CONSTANT_ERROR_EMPTY_REQUEST_MSG = "Empty request";
	
	public final static String CONSTANT_SUCCESS_REQUEST_VALIDATE_MSG = "Validation success";
	
	public final static String CONSTANT_RESPONSE_CODE_EMPTY_REQUEST = "Error-201";

	public final static String CONSTANT_RESPONSE_CODE_INCORRECT_REQUEST = "Error-202";

	public final static String CONSTANT_RESPONSE_CODE_DOC_LOCK_FAILED = "Error-203";

	public final static String CONSTANT_ERROR_DOC_LOCK_FAILED_MSG = "Document locking failure";

	public final static String CONSTANT_RESPONSE_CODE_UNEXPECTED_ERROR = "Error-204";

	public final static String CONSTANT_ERROR_UNEXPECTED_ERROR_MSG = "Unexpected Error";
	
	public final static String CONSTANT_ERROR_MANDATORY_FIELDS_ABSENSE_MSG = "Mandatory fields not present";

	public final static String CONSTANT_RESPONSE_CODE_MANDATORY_FIELDS_ABSENSE = "Error-205";
	
	public final static String CONSTANT_RESPONSE_CODE_INTERMEDIATE_TRANSACTION_DOC_UPDATION = "Error-206";
	
	public final static String CONSTANT_ERROR_INTERMEDIATE_TRANSACTION_DOC_UPDATION_MSG = "Transaction failure in updating temporary documents with actual doc content";

	public final static String CONSTANT_STATUS_FAILED = "Failed";
	
	public final static String CONSTANT_STATUS_SUCCESS = "Success";
	
	public final static String CONSTANT_RESPONSE_CODE_TRANSACTION_COMPLETE = "Success-100";
	
	public final static String CONSTANT_MESSAGE_WRITE_COMPLETE_MSG = "Message written to kafka successfully";
	
	public final static String CONSTANT_MESSAGE_WRITE_FAILURE_MSG = "Message writing to kafka failed";
	
	public final static String CONSTANT_RESPONSE_CODE_MESSAGE_WRITE_COMPLETE = "Success-101";
	
	public final static String CONSTANT_RESPONSE_CODE_MESSAGE_WRITE_FAILED = "Error-213";
	
	public final static String CONSTANT_TRANSACTION_COMPLETE_MSG = "Transaction completed"; 
	
	public final static String CONSTANT_RESPONSE_CODE_ATTRIBUTE_REFERENCE_ERROR = "Error-207";

	public final static String CONSTANT_ERROR_CLASS_REFERENCE_ABSENT_MSG = "Request class reference does not exist";
	
	public final static String CONSTANT_ERROR_CLASS_REFERENCEBY_MSG = "ReferredBy classes attribute deletion required";
	
	public final static String CONSTANT_ERROR_CLASS_ATTRIBUTE_REFERENCE_ABSENT_MSG = "Reference attribute class code does not exists";
	
	public final static String CONSTANT_ERROR_CLASS_ATTRIBUTE_REFERENCE_NULL_MSG = "Reference attribute class code is null";
	
	public final static String CONSTANT_RESPONSE_CODE_CLASS_REFERENCE_ERROR = "Error-208";
	
	public final static String CONSTANT_ERROR_UNIQUE_ATTRIBUTE_VALUE_NULL_OR_DOC_NOT_EXIST_MSG = "Either unique value attribute is null or unique attribute document does not exist";

	public final static String CONSTANT_RESPONSE_CODE_UNIQUE_ATTRIBUTE_ERROR = "Error-217";
	
	public final static String CONSTANT_RESPONSE_CODE_STALE_DATA_ERROR = "Error-209";
	
	public final static String CONSTANT_RESPONSE_CODE_VALIDATION_EXCEPTION_ERROR = "Error-210";
	
	public final static String CONSTANT_RESPONSE_CODE_ERROR_PARENT_CLASS_ABSENSE = "Error-211";
	
	public final static String CONSTANT_RESPONSE_CODE_INSTANCE_CODE_ABSENSE = "Error-212";
	
	public final static String CONSTANT_VALIDATION_FAILURE_MSG = "Request validation failed.";
	
	public final static String CONSTANT_STALE_DATA_MSG = "Data is stale. Please refresh and try again";
	
	public final static String CONSTANT_INTERMEDIATE_TXN_TEMP_CONTENT_COPY_SUCCESS_MSG = "Intermediate transaction content copied.";
	
	public final static String CONSTANT_INSTANCE_IS_REFERRED_BY_ANOTHER_INSTANCE_ATTRIBUTE_MSG = "Instance to be deleted is referred in another instance.";
	
	public static  final String CONSTANT_INSTANCE_IS_REFERRED_BY_ANOTHER_INSTANCE_ERROR = "Error-215";
	
	public static final  String CONSTANT_INSTANCE_TO_DELETED_NOT_EXIST_MSG = "Instance to delete doesn't exist.";
	
	public static final  String CONSTANT_UNABLE_TO_LOCK_DOCUMENT_ERROR = "Error-216";
	
	public static final  String CONSTANT_ERROR_OCCURED_WHILE_TRYING_TO_LOCK_DOCUMENT_MSG = "Some error occured while trying to lock document for write.";
	
	public static final  String CONSTANT_DUPLICATE_ATTRIBUTE_NAME_MSG = "Duplicate attribute name found in request.";
	
	public static final  String CONSTANT_ALPHANUMERIC_ATTRIBUTE_NAME_ERROR_MSG = "Only alphanumeric values are allowed in attribute name.";
	
	public static final  String CONSTANT_ATTRIBUTE_NAME_IN_REQUEST_DOESNOT_EXIST_IN_CLASS_META_ERROR_MSG = "Attribute name in request doesn't exist in class meta.";
	
}
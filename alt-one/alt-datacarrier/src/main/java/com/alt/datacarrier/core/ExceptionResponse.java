package com.alt.datacarrier.core;

public class ExceptionResponse {

	private String errorMessage;
	
	private String errorCode;

	public ExceptionResponse() {
	}

	public ExceptionResponse(String _errorMessage, String _errorCode) {
		this.errorMessage = _errorMessage;
		this.errorCode = _errorCode;
	}
		
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
}

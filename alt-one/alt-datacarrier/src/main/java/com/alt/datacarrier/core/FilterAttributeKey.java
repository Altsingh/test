package com.alt.datacarrier.core;

import com.alt.datacarrier.common.enumeration.EnumFilterOption;

public class FilterAttributeKey {

	private String attribute;
	
	private EnumFilterOption operation;

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public EnumFilterOption getOperation() {
		return operation;
	}

	public void setOperation(EnumFilterOption operation) {
		this.operation = operation;
	}
	
	
}

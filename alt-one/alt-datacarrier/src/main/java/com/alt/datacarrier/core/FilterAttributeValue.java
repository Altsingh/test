package com.alt.datacarrier.core;

import com.alt.datacarrier.common.enumeration.EnumFilterOption;

public class FilterAttributeValue {

	private String value;
	
	private EnumFilterOption operation;

	public EnumFilterOption getOperation() {
		return operation;
	}

	public void setOperation(EnumFilterOption operation) {
		this.operation = operation;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}

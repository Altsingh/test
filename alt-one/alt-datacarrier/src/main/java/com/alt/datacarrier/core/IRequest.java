package com.alt.datacarrier.core;

import java.io.Serializable;

/**
 * The class <code>IRequest.java</code>
 *
 * @author  rishi.gautam
 *
 * @createdOn   Mar 3, 2017
 */

public interface IRequest extends Serializable
{
    Protocol getProtocol();
}

package com.alt.datacarrier.core;

import java.io.Serializable;

/**
 * The class <code>IResponse.java</code>
 *
 * @author  rishi.gautam
 *
 * @createdOn   Mar 3, 2017
 */

public interface IResponse extends Serializable
{

}

package com.alt.datacarrier.core;

import com.alt.datacarrier.common.enumeration.EnumSortDirection;

import java.io.Serializable;


public class IndexingData implements Serializable {

	private static final long serialVersionUID = -68207969974736579L;
	private Integer offset;
	private Integer limit;
	private EnumSortDirection order;
	private String orderBy;

	public IndexingData(Integer offset) {
		this.offset = offset;
	}

	public IndexingData() {
	}

	public IndexingData(Builder builder) {
		setOffset(builder.offset);
		setLimit(builder.limit);
		setOrder(builder.order);
		setOrderBy(builder.orderBy);
	}

	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public EnumSortDirection getOrder() {
		return order;
	}
	public void setOrder(EnumSortDirection order) {
		this.order = order;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}


	public static final class Builder {
		private Integer offset;
		private Integer limit;
		private EnumSortDirection order;
		private String orderBy;

		public Builder() {
		}

		public Builder withOffset(Integer val) {
			offset = val;
			return this;
		}

		public Builder withLimit(Integer val) {
			limit = val;
			return this;
		}

		public Builder withOrder(EnumSortDirection val) {
			order = val;
			return this;
		}

		public Builder withOrderBy(String val) {
			orderBy = val;
			return this;
		}

		public IndexingData build() {
			return new IndexingData(this);
		}
	}
}

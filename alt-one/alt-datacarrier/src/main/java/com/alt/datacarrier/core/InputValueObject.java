package com.alt.datacarrier.core;

public class InputValueObject implements java.io.Serializable{

	private static final long serialVersionUID = 3101129788165482956L;

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}

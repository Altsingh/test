package com.alt.datacarrier.core;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class <code>InstanceData.java</code>
 *
 * @author rishi.gautam
 *
 * @createdOn Mar 3, 2017
 */
public class InstanceData implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String docIdentifier;

	private String classCode;

	private String cas;

	private String type;

	private Map<String, SingleAttributeValue> attributes;

	private String status;

	private Long createdDate; 
	
	private Long modifiedDate;
	
	private String createdBy;
	
	private String modifiedBy;
	
	@JsonProperty("docIdentifier")
	public String getDocIdentifier() {
		return docIdentifier;
	}
	@JsonProperty("docIdentifier")
	public void setDocIdentifier(String docIdentifier) {
		this.docIdentifier = docIdentifier;
	}
	@JsonProperty("classCode")
	public String getClassCode() {
		return classCode;
	}
	@JsonProperty("classCode")
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	@JsonProperty("cas")
	public String getCas() {
		return cas;
	}
	@JsonProperty("cas")
	public void setCas(String cas) {
		this.cas = cas;
	}
	
	@JsonProperty("type")
	public String getType() {
		return type;
	}
	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}
	@JsonProperty("status")
	public void setStatus(String status) {
		this.status = status;
	}
	@JsonProperty("createdDate")
	public Long getCreatedDate() {
		return createdDate;
	}
	@JsonProperty("createdDate")
	public void setCreatedDate(Long createdDate) {
		this.createdDate = createdDate;
	}
	@JsonProperty("modifiedDate")
	public Long getModifiedDate() {
		return modifiedDate;
	}
	@JsonProperty("modifiedDate")
	public void setModifiedDate(Long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@JsonProperty("createdBy")
	public String getCreatedBy() {
		return createdBy;
	}
	@JsonProperty("createdBy")
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@JsonProperty("modifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	@JsonProperty("modifiedBy")
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	@JsonProperty("attributes")
	public Map<String, SingleAttributeValue> getAttributes() {
		return attributes;
	}
	
	@JsonProperty("attributes")
	public void setAttributes(Map<String, SingleAttributeValue> attributes) {
		this.attributes = attributes;
	}

}

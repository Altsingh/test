package com.alt.datacarrier.core;

import com.alt.datacarrier.common.enumeration.EnumSecurityType;

public class OutputValueObject implements java.io.Serializable{

	private static final long serialVersionUID = -5042332097879109589L;

	private String value;
	
	private String dataType;
	
	private String label;
	
	private EnumSecurityType securityType;
	
	public OutputValueObject(String dataType, String label,
			EnumSecurityType securityType) {
		super();
		this.dataType = dataType;
		this.label = label;
		this.securityType = securityType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public EnumSecurityType getSecurityType() {
		return securityType;
	}

	public void setSecurityType(EnumSecurityType securityType) {
		this.securityType = securityType;
	}
}

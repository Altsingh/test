package com.alt.datacarrier.core;

import java.io.Serializable;

import com.alt.datacarrier.common.enumeration.EnumPortalTypes;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Protocol implements Serializable {

	private static final long serialVersionUID = -6984311066064136071L;

	private String userCode;

	private String userName;

	private String orgCode;

	private String tenantCode;

	private String sysFormCode;

	private EnumPortalTypes portalType;

	private String loggedInUserIdentifier;

	private Long loggedInTimestamp;

	private String jwt;

	private String domain;

	private Integer orgId;

	private Integer userId;

	private Integer employeeId;

	private Integer tenantId;

	private String appCode;

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	@JsonProperty("domain")
	public String getDomain() {
		return domain;
	}

	@JsonProperty("domain")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	@JsonProperty("jwt")
	public String getJwt() {
		return jwt;
	}

	@JsonProperty("jwt")
	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	@JsonProperty("userCode")
	public String getUserCode() {
		return userCode;
	}

	@JsonProperty("userCode")
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@JsonProperty("userName")
	public String getUserName() {
		return userName;
	}

	@JsonProperty("userName")
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@JsonProperty("orgCode")
	public String getOrgCode() {
		return orgCode;
	}

	@JsonProperty("orgCode")
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	@JsonProperty("tenantCode")
	public String getTenantCode() {
		return tenantCode;
	}

	@JsonProperty("tenantCode")
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}

	@JsonProperty("sysFormCode")
	public String getSysFormCode() {
		return sysFormCode;
	}

	@JsonProperty("sysFormCode")
	public void setSysFormCode(String sysFormCode) {
		this.sysFormCode = sysFormCode;
	}

	@JsonProperty("portalType")
	public EnumPortalTypes getPortalType() {
		return portalType;
	}

	@JsonProperty("portalType")
	public void setPortalType(EnumPortalTypes portalType) {
		this.portalType = portalType;
	}

	@JsonProperty("loggedInUserIdentifier")
	public String getLoggedInUserIdentifier() {
		return loggedInUserIdentifier;
	}

	@JsonProperty("loggedInUserIdentifier")
	public void setLoggedInUserIdentifier(String loggedInUserIdentifier) {
		this.loggedInUserIdentifier = loggedInUserIdentifier;
	}

	@JsonProperty("loggedInTimestamp")
	public Long getLoggedInTimestamp() {
		return loggedInTimestamp;
	}

	@JsonProperty("loggedInTimestamp")
	public void setLoggedInTimestamp(Long loggedInTimestamp) {
		this.loggedInTimestamp = loggedInTimestamp;
	}

	@JsonProperty("orgId")
	public Integer getOrgId() {
		return orgId;
	}

	@JsonProperty("orgId")
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	@Override
	public String toString() {
		return "Protocol{" + "userCode='" + userCode + '\'' + ", userName='" + userName + '\'' + ", orgCode='" + orgCode
				+ '\'' + ", tenantCode='" + tenantCode + '\'' + ", sysFormCode='" + sysFormCode + '\'' + ", portalType="
				+ portalType + ", loggedInUserIdentifier='" + loggedInUserIdentifier + '\'' + ", loggedInTimestamp="
				+ loggedInTimestamp + ", jwt='" + jwt + '\'' + ", domain='" + domain + '\'' + ", orgId='" + orgId + '\''
				+ ", userId='" + userId + '\'' + ", employeeId='" + employeeId + '\'' + ", tenantId='" + tenantId + '\''
				+ '}';
	}

	@JsonProperty("userId")
	public Integer getUserId() {
		return userId;
	}

	@JsonProperty("userId")
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@JsonProperty("employeeId")
	public Integer getEmployeeId() {
		return employeeId;
	}

	@JsonProperty("employeeId")
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	@JsonProperty("tenantId")
	public Integer getTenantId() {
		return tenantId;
	}

	@JsonProperty("tenantId")
	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

}

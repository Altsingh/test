package com.alt.datacarrier.core;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The class <code>Request.java</code>
 *
 * @author  rishi.gautam
 *
 * @createdOn   Mar 3, 2017
 */

public class Request<T extends IRequestData> implements IRequest
{
    private static final long serialVersionUID = 5737374093253207L;
    
    @JsonProperty("protocol")
    private Protocol protocol;

    @JsonProperty("requestData")
    private List<T> requestData;
	
    public Protocol getProtocol()
    {
        return protocol;
    }
	
    public void setProtocol(Protocol protocol)
    {
        this.protocol = protocol;
    }
	
    public List<T> getRequestData()
    {
        return requestData;
    }
	
    public void setRequestData(List<T> requestData)
    {
        this.requestData = requestData;
    }
    
}
package com.alt.datacarrier.core;

import java.util.List;

/**
 * The class <code>Response.java</code>
 *
 * @author rishi.gautam
 *
 * @createdOn Mar 3, 2017
 */

public class Response<T extends IResponseData> implements IResponse, IResponseData {
	private static final long serialVersionUID = 33204024093702740L;

	private String responseCode;

	private String responseStatus;

	private String errMsg;
	
	private List<ResponseDocAndStatus> docAndStatusList;

	private T responseData;
	
	private List<T> responseList;

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public T getResponseData() {
		return responseData;
	}

	public void setResponseData(T responseData) {
		this.responseData = responseData;
	}

	public List<T> getResponseList() {
		return responseList;
	}

	public void setResponseList(List<T> responseList) {
		this.responseList = responseList;
	}

	public List<ResponseDocAndStatus> getDocAndStatusList() {
		return docAndStatusList;
	}

	public void setDocAndStatusList(List<ResponseDocAndStatus> docAndStatusList) {
		this.docAndStatusList = docAndStatusList;
	}

}
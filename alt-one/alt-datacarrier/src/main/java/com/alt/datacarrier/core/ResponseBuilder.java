package com.alt.datacarrier.core;

import java.util.List;

public final class ResponseBuilder<T extends IResponseData> {
    private String responseCode;
    private String responseStatus;
    private String errMsg;
    private List<ResponseDocAndStatus> docAndStatusList;
    private T responseData;
    private List<T> responseList;

    private ResponseBuilder() {
    }

    public static <T extends IResponseData> ResponseBuilder<T> aResponse() {
        return new ResponseBuilder();
    }

    public ResponseBuilder<T> withResponseCode(String responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    public ResponseBuilder<T> withResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
        return this;
    }

    public ResponseBuilder<T>  withErrMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }

    public ResponseBuilder <T> withDocAndStatusList(List<ResponseDocAndStatus> docAndStatusList) {
        this.docAndStatusList = docAndStatusList;
        return this;
    }

    public ResponseBuilder<T>  withResponseData(T responseData) {
        this.responseData = responseData;
        return this;
    }

    public ResponseBuilder<T>  withResponseList(List<T> responseList) {
        this.responseList = responseList;
        return this;
    }

    public Response<T> build() {
        Response<T> response = new Response();
        response.setResponseCode(responseCode);
        response.setResponseStatus(responseStatus);
        response.setErrMsg(errMsg);
        response.setDocAndStatusList(docAndStatusList);
     //   response.setResponseData(responseData);
        response.setResponseList(responseList);
        return response;
    }
}

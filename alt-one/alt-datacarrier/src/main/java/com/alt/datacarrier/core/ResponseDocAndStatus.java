package com.alt.datacarrier.core;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;

public class ResponseDocAndStatus {

	private String document;
	
	private Boolean status;
	
	private EnumCRUDOperations operation;
	
	private EnumRequestType type;

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public EnumCRUDOperations getOperation() {
		return operation;
	}

	public void setOperation(EnumCRUDOperations operation) {
		this.operation = operation;
	}

	public EnumRequestType getType() {
		return type;
	}

	public void setType(EnumRequestType type) {
		this.type = type;
	}
	
}

package com.alt.datacarrier.core;

import java.util.List;
import java.util.Map;

import com.alt.datacarrier.common.enumeration.EnumOperation;

public class ServiceRequest implements java.io.Serializable{

	private static final long serialVersionUID = -2977061033235419642L;

	private Protocol protocolTO;
	
	private List<Map<EnumOperation,Map<String,InputValueObject>>> request;

	public Protocol getProtocolTO() {
		return protocolTO;
	}

	public void setProtocolTO(Protocol protocolTO) {
		this.protocolTO = protocolTO;
	}

	public List<Map<EnumOperation, Map<String, InputValueObject>>> getRequest() {
		return request;
	}

	public void setRequest(List<Map<EnumOperation, Map<String, InputValueObject>>> request) {
		this.request = request;
	}
	
}

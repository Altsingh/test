package com.alt.datacarrier.core;

import java.util.List;
import java.util.Map;

public class ServiceResponse implements java.io.Serializable{

	private static final long serialVersionUID = 8489068151861205622L;

	private List<Map<String,OutputValueObject>> response;

	private String code;
	
	private String alertMessage;
	
	public List<Map<String, OutputValueObject>> getResponse() {
		return response;
	}

	public void setResponse(List<Map<String, OutputValueObject>> response) {
		this.response = response;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAlertMessage() {
		return alertMessage;
	}

	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}

}

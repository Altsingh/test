package com.alt.datacarrier.core;

import java.io.Serializable;
import java.util.List;

/**
 * The class <code>SingleAttributeValue.java</code>
 *
 * @author  rishi.gautam
 *
 * @createdOn   Mar 3, 2017
 */

public class SingleAttributeValue implements Serializable
{
    private static final long serialVersionUID = 56938422424620L;

    private boolean reference;
    
    private String value;
    
    private List<InstanceData> referenceValue;    

    public boolean isReference()
    {
        return reference;
    }

    public void setReference(boolean reference)
    {
        this.reference = reference;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public  List<InstanceData> getReferenceValue()
    {
        return referenceValue;
    }

    public void setReferenceValue( List<InstanceData> referenceValue)
    {
        this.referenceValue = referenceValue;
    }

}

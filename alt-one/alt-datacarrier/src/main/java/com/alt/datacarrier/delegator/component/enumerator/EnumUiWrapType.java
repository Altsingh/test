package com.alt.datacarrier.delegator.component.enumerator;

public enum EnumUiWrapType {

	H("hard"), S("soft");
	
	private String wrapType;

	private EnumUiWrapType(String wrapType) {
		this.wrapType = wrapType;
	}
	
	public String getWrapType() {
		return wrapType;
	}
}

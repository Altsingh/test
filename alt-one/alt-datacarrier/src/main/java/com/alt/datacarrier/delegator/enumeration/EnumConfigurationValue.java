package com.alt.datacarrier.delegator.enumeration;

public enum EnumConfigurationValue {

	D("DENY"),V("VIEWABLE"),EM("EDITABLE_MANDATORY"),ENM("EDITABLE_NONMANDATORY");
	
	private String value;
	
	private EnumConfigurationValue(String value){
		this.value = value;
	}
}

package com.alt.datacarrier.delegator.enumeration;

public enum EnumSingleValueDataType {
	
	STRING,INT,DOUBLE,CHAR,LONG;

}

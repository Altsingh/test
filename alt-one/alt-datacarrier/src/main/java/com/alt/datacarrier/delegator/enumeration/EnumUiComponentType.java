package com.alt.datacarrier.delegator.enumeration;

public enum EnumUiComponentType {
	
	P("PANEL"), F("FIELD"), W("WIDGET");
	
	private String name;
	
	private EnumUiComponentType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}

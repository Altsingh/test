package com.alt.datacarrier.employee;

import java.util.Collection;

public class EmployeeResponseTO {

	private String employeeName;

	private String photoPath;

	private Collection<Integer> assignedRoleIds;

	private Integer userId;

	private Integer employeeId;

	private Integer tenantId;

	private String mobile;

	private String email;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public Collection<Integer> getAssignedRoleIds() {
		return assignedRoleIds;
	}

	public void setAssignedRoleIds(Collection<Integer> assignedRoleIds) {
		this.assignedRoleIds = assignedRoleIds;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Integer getTenantId() {
		return tenantId;
	}

	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}

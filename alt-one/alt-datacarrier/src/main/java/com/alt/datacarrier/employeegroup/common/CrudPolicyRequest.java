package com.alt.datacarrier.employeegroup.common;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.core.Protocol;

public class CrudPolicyRequest implements IRequestData {

	private static final long serialVersionUID = -6964511892584875116L;

	private Protocol protocol;

	private UserRequestType type;

	private String policyName;

	private String policyCode;

	private String module;

	private String entity;

	private boolean status;

	private String cas;

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public UserRequestType getType() {
		return type;
	}

	public void setType(UserRequestType type) {
		this.type = type;
	}

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public String getPolicyCode() {
		return policyCode;
	}

	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCas() {
		return cas;
	}

	public void setCas(String cas) {
		this.cas = cas;
	}
}

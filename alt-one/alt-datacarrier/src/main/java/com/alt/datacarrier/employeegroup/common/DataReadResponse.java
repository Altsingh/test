package com.alt.datacarrier.employeegroup.common;

import java.util.List;
import java.util.Map;

public class DataReadResponse {

	private List<Map<String, Object>> response;

	private Integer doccumentCount;

	public List<Map<String, Object>> getResponse() {
		return response;
	}

	public void setResponse(List<Map<String, Object>> response) {
		this.response = response;
	}

	public Integer getDoccumentCount() {
		return doccumentCount;
	}

	public void setDoccumentCount(Integer doccumentCount) {
		this.doccumentCount = doccumentCount;
	}
}

package com.alt.datacarrier.employeegroup.common;

import com.alt.datacarrier.core.IRequestData;


public class EmpGroupResponse implements IRequestData {

	private static final long serialVersionUID = -2321153894468786810L;
	private Integer respCode;
	private String respMsg;
	private Object respData;
	private String jwt;
	
	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	public Integer getRespCode() {
		return respCode;
	}

	public void setRespCode(Integer respCode) {
		this.respCode = respCode;
	}

	public String getRespMsg() {
		return respMsg;
	}

	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}

	public Object getRespData() {
		return respData;
	}

	public void setRespData(String respData) {
		this.respData = respData;
	}

	public void setData(Integer respCode, String respMsg,Object respData,String jwt){
		this.respCode = respCode;
		this.respMsg = respMsg;
		this.respData = respData;
		this.jwt = jwt;
	}
}

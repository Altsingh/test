package com.alt.datacarrier.employeegroup.common;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;


public class EmployeeGroup {

	private String groupName;
	private ParameterList parameterList;
	private Map<String, ValueList> parameterValue;

	@JsonProperty("GroupName")
	public String getGroupName() {
		return groupName;
	}

	@JsonProperty("GroupName")
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public ParameterList getParameterList() {
		return parameterList;
	}

	public void setParameterList(ParameterList parameterList) {
		this.parameterList = parameterList;
	}

	public Map<String, ValueList> getParameterValue() {
		return parameterValue;
	}

	public void setParameterValue(Map<String, ValueList> parameterValue) {
		this.parameterValue = parameterValue;
	}

}

class ValueList {
	Map<String, List<String>> parameterValues;

	public Map<String, List<String>> getParameterValues() {
		return parameterValues;
	}

	public void setParameterValues(Map<String, List<String>> parameterValues) {
		this.parameterValues = parameterValues;
	}
}

class ParameterList {
	List<String> parameters;

	public List<String> getParameters() {
		return parameters;
	}

	public void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}

}

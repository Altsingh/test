package com.alt.datacarrier.employeegroup.common;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.core.IndexingData;
import com.alt.datacarrier.core.Protocol;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


public class GetEmpParamsRequest implements IRequestData {

	private static final long serialVersionUID = 6843244138348596908L;

	@JsonIgnore
	@JsonProperty("protocol")
	private Protocol protocol;

	@JsonIgnore
	@JsonProperty("parameterGroupList")
	private List<String> parameterGroupList;

	@JsonProperty("indexingData")
	private IndexingData indexingData;

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public List<String> getParameterGroupList() {
		return parameterGroupList;
	}

	public void setParameterGroupList(List<String> parameterGroupList) {
		this.parameterGroupList = parameterGroupList;
	}

	public IndexingData getIndexingData() {
		return indexingData;
	}

	public void setIndexingData(IndexingData indexingData) {
		this.indexingData = indexingData;
	}

}

package com.alt.datacarrier.employeegroup.common;

import com.alt.datacarrier.core.Protocol;


public class GetGroupEmployeesRequest {

	private Protocol protocol;

	private String empGroupId;

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public String getEmpGroupId() {
		return empGroupId;
	}

	public void setEmpGroupId(String empGroupId) {
		this.empGroupId = empGroupId;
	}

}

package com.alt.datacarrier.employeegroup.common;


import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.core.IndexingData;
import com.alt.datacarrier.core.Protocol;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class GetPolicyListRequest implements IRequestData {

	private static final long serialVersionUID = -4450303506760844976L;

	private Protocol protocol;

	private IndexingData indexingData;

	@JsonIgnore
	@JsonProperty("protocol")
	public Protocol getProtocol() {
		return protocol;
	}

	@JsonIgnore
	@JsonProperty("protocol")
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public IndexingData getIndexingData() {
		return indexingData;
	}

	public void setIndexingData(IndexingData indexingData) {
		this.indexingData = indexingData;
	}
}

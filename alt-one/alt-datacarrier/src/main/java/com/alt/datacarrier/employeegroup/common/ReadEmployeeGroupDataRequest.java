package com.alt.datacarrier.employeegroup.common;


import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.core.IndexingData;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.employeegroup.common.EmployeeGroupContants.EnumDataType;
import com.fasterxml.jackson.annotation.JsonProperty;
public class ReadEmployeeGroupDataRequest implements IRequestData {

	private static final long serialVersionUID = -5013220668758182109L;

	private Protocol protocol;
	
	private EnumDataType type;
	
	private IndexingData indexingData;
	
	public IndexingData getIndexingData() {
		return indexingData;
	}

	public void setIndexingData(IndexingData indexingData) {
		this.indexingData = indexingData;
	}

	@JsonProperty("protocol")
	public Protocol getProtocol() {
		return protocol;
	}

	@JsonProperty("protocol")
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public EnumDataType getType() {
		return type;
	}

	public void setType(EnumDataType type) {
		this.type = type;
	}

	
	
}

package com.alt.datacarrier.employeegroup.common;

import java.util.List;
import java.util.Map;


import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.core.Protocol;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SaveEmpGroupRequest implements IRequestData {

	private static final long serialVersionUID = 6456808498620358461L;

	private Protocol protocol;

	private String groupName;

	@JsonProperty("empGroupParameter")
	private List<String> parameterList;

	@JsonProperty("empGroupParameterValue")
	private Map<String, List<String>> paramValueMap;

	private String orgEmployeeGroupClassCode;

	private boolean isActive;

	@JsonProperty("protocol")
	public Protocol getProtocol() {
		return protocol;
	}

	@JsonProperty("protocol")
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	@JsonProperty("orgEmployeeGroupClassCode")
	public String getOrgEmployeeGroupClassCode() {
		return orgEmployeeGroupClassCode;
	}

	@JsonProperty("orgEmployeeGroupClassCode")
	public void setOrgEmployeeGroupClassCode(String orgEmployeeGroupClassCode) {
		this.orgEmployeeGroupClassCode = orgEmployeeGroupClassCode;
	}

	@JsonProperty("groupName")
	public String getGroupName() {
		return groupName;
	}

	@JsonProperty("groupName")
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@JsonProperty("empGroupParameter")
	public List<String> getParameterList() {
		return parameterList;
	}

	@JsonProperty("empGroupParameter")
	public void setParameterList(List<String> parameterList) {
		this.parameterList = parameterList;
	}

	@JsonProperty("empGroupParameterValue")
	public Map<String, List<String>> getParamValueMap() {
		return paramValueMap;
	}

	@JsonProperty("empGroupParameterValue")
	public void setParamValueMap(Map<String, List<String>> paramValueMap) {
		this.paramValueMap = paramValueMap;
	}

	@JsonProperty("isActive")
	public boolean isActive() {
		return isActive;
	}

	@JsonProperty("isActive")
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

}

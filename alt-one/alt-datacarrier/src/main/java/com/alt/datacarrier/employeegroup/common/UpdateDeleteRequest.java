package com.alt.datacarrier.employeegroup.common;

import java.util.List;
import java.util.Map;


import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.core.Protocol;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateDeleteRequest implements IRequestData {

	private static final long serialVersionUID = 6302860691860514394L;

	private Protocol protocol;

	private String groupName;

	private List<String> parameterList;

	private Map<String, List<String>> paramValueMap;

	private String orgEmployeeGroupClassCode;

	private String empGroupInstanceCode;

	private boolean isActive;

	private UserRequestType requestType;
	
	private String cas;

	@JsonProperty("cas")
	public String getCas() {
		return cas;
	}

	@JsonProperty("cas")
	public void setCas(String cas) {
		this.cas = cas;
	}

	@JsonProperty("protocol")
	public Protocol getProtocol() {
		return protocol;
	}

	@JsonProperty("protocol")
	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	@JsonIgnore
	@JsonProperty("groupName")
	public String getGroupName() {
		return groupName;
	}

	@JsonIgnore
	@JsonProperty("groupName")
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@JsonIgnore
	@JsonProperty("empGroupParameter")
	public List<String> getParameterList() {
		return parameterList;
	}

	@JsonIgnore
	@JsonProperty("empGroupParameter")
	public void setParameterList(List<String> parameterList) {
		this.parameterList = parameterList;
	}

	@JsonIgnore
	@JsonProperty("empGroupParameterValue")
	public Map<String, List<String>> getParamValueMap() {
		return paramValueMap;
	}

	@JsonIgnore
	@JsonProperty("empGroupParameterValue")
	public void setParamValueMap(Map<String, List<String>> paramValueMap) {
		this.paramValueMap = paramValueMap;
	}

	@JsonProperty("orgEmployeeGroupClassCode")
	public String getOrgEmployeeGroupClassCode() {
		return orgEmployeeGroupClassCode;
	}

	@JsonIgnore
	@JsonProperty("orgEmployeeGroupClassCode")
	public void setOrgEmployeeGroupClassCode(String orgEmployeeGroupClassCode) {
		this.orgEmployeeGroupClassCode = orgEmployeeGroupClassCode;
	}

	@JsonProperty("requestType")
	public UserRequestType getRequestType() {
		return requestType;
	}

	@JsonProperty("requestType")
	public void setRequestType(UserRequestType requestType) {
		this.requestType = requestType;
	}

	@JsonIgnore
	@JsonProperty("isActive")
	public boolean isActive() {
		return isActive;
	}

	@JsonIgnore
	@JsonProperty("isActive")
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@JsonProperty("empGroupInstanceCode")
	public String getEmpGroupInstanceCode() {
		return empGroupInstanceCode;
	}

	@JsonProperty("empGroupInstanceCode")
	public void setEmpGroupInstanceCode(String empGroupInstanceCode) {
		this.empGroupInstanceCode = empGroupInstanceCode;
	}

}

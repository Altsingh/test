package com.alt.datacarrier.employeegroup.common;

public enum UserRequestType {
	
	INSERT("INSERT"), UPDATE("UPDATE"), DELETE("DELETE");

private final String requestType;

private UserRequestType(String requestType)
{
    this.requestType = requestType;
}

public String getRequestType()
{
    return this.requestType;
}


}

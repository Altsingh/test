package com.alt.datacarrier.exception;

public class AppRelatedException extends RuntimeException{

	private static final long serialVersionUID = -831792605184002404L;

	public AppRelatedException(String message) {
		super(message);
	}

	public AppRelatedException(String message, Throwable e) {
		super(message, e);
	}
}

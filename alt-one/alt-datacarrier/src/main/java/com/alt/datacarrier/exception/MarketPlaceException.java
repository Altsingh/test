package com.alt.datacarrier.exception;

public class MarketPlaceException extends RuntimeException {

	public MarketPlaceException(String message) {
		super(message);
	}
	
	public MarketPlaceException(String message, Throwable e) {
		super(message,e);
	}
}

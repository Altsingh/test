package com.alt.datacarrier.exception;

public class SuperAdminException extends RuntimeException{
     
	public SuperAdminException(String message) {
		super(message);
	}

}

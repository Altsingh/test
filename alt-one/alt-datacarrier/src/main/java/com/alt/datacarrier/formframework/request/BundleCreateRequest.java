package com.alt.datacarrier.formframework.request;

import com.alt.datacarrier.core.IRequestData;

public class BundleCreateRequest implements IRequestData{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2239040989033074875L;
	
	private String language;
	private String languageCode;
	private String localeCode;
	private String locale;
	
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getLocaleCode() {
		return localeCode;
	}
	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	
	

}

package com.alt.datacarrier.formframework.request;

import com.alt.datacarrier.core.IRequestData;

public class BundleDeleteRequest implements IRequestData{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String languageCode;
	private String localeCode;
	
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getLocaleCode() {
		return localeCode;
	}
	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}
	
	
}

package com.alt.datacarrier.formframework.request;

import java.io.Serializable;
import java.util.List;

public class CommEnvlp implements Serializable {

	private String templateName;
	private Integer commId;
	private String from;
	private String to;
	private String cc;
	private String bcc;
	private String subject;
	private String body;
	private String commType;
	private String action;
	private String successMessage;
	private List<Integer> selectMailIds;
	private List<CommEnvlp> commEnvlpList;

	public CommEnvlp() {

	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public Integer getCommId() {
		return commId;
	}

	public void setCommId(Integer commId) {
		this.commId = commId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getCommType() {
		return commType;
	}

	public void setCommType(String commType) {
		this.commType = commType;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public List<Integer> getSelectMailIds() {
		return selectMailIds;
	}

	public void setSelectMailIds(List<Integer> selectMailIds) {
		this.selectMailIds = selectMailIds;
	}

	public List<CommEnvlp> getCommEnvlpList() {
		return commEnvlpList;
	}

	public void setCommEnvlpList(List<CommEnvlp> commEnvlpList) {
		this.commEnvlpList = commEnvlpList;
	}
}

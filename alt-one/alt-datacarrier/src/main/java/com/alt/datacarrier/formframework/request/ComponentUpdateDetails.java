package com.alt.datacarrier.formframework.request;

import java.io.Serializable;

import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;

public class
ComponentUpdateDetails implements Serializable{

	private static final long serialVersionUID = -3919951475030661228L;

	private Integer location;
	
	private AltAbstractComponent component;

	public Integer getLocation() {
		return location;
	}

	public void setLocation(Integer location) {
		this.location = location;
	}

	public AltAbstractComponent getComponent() {
		return component;
	}

	public void setComponent(AltAbstractComponent component) {
		this.component = component;
	}
	
}

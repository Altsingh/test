package com.alt.datacarrier.formframework.request;

import com.alt.datacarrier.core.IRequestData;

public class DBClassListRequest implements IRequestData{
	
	private String orgId;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
	

}

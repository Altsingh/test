package com.alt.datacarrier.formframework.request;

import java.io.Serializable;

import com.alt.datacarrier.kernel.uiclass.AltDataTable;

public class DataTableOnDemand implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AltDataTable table;

	public DataTableOnDemand() {

	}
	
	public DataTableOnDemand(AltDataTable table) {
		this.table = table;
	}

	public AltDataTable getTable() {
		return table;
	}

	public void setTable(AltDataTable table) {
		this.table = table;
	}
}

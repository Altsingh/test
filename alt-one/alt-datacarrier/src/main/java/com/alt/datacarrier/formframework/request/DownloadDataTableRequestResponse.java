package com.alt.datacarrier.formframework.request;

import java.util.Collection;

import com.alt.datacarrier.kernel.uiclass.AltDataTable;
import com.alt.datacarrier.organization.RoleTO;

public class DownloadDataTableRequestResponse {

	private String tenantId;

	private Collection<RoleTO> formRoles;

	private AltDataTable table;

	private String fileName;

	private String fileData;

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Collection<RoleTO> getFormRoles() {
		return formRoles;
	}

	public void setFormRoles(Collection<RoleTO> formRoles) {
		this.formRoles = formRoles;
	}

	public AltDataTable getTable() {
		return table;
	}

	public void setTable(AltDataTable table) {
		this.table = table;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileData() {
		return fileData;
	}

	public void setFileData(String fileData) {
		this.fileData = fileData;
	}

}

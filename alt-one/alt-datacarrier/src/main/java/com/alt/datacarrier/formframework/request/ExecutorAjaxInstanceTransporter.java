package com.alt.datacarrier.formframework.request;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import com.alt.datacarrier.organization.RoleTO;

public class ExecutorAjaxInstanceTransporter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * class id
	 * */
	private Integer id;
	private String authToken;
	private String className;
	private Map<String,String> attributes;
	
	private String componentName;
	private String formName;
	private RoleTO formRoles;
	
	public ExecutorAjaxInstanceTransporter() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public RoleTO getFormRoles() {
		return formRoles;
	}

	public void setFormRoles(RoleTO formRoles) {
		this.formRoles = formRoles;
	}

}

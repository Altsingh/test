package com.alt.datacarrier.formframework.request;

import com.alt.datacarrier.common.enumeration.EnumSecurityType;

public class FieldSecurity {
	private String uiClassCode;
	private String uiComponentId;
	private String roleId;
	private String stage;
	private String employeeGroupCode;
	private String orgCode;
	private EnumSecurityType securityType;
	
	public String getUiClassCode() {
		return uiClassCode;
	}
	public void setUiClassCode(String uiClassCode) {
		this.uiClassCode = uiClassCode;
	}
	public String getUiComponentId() {
		return uiComponentId;
	}
	public void setUiComponentId(String uiComponentId) {
		this.uiComponentId = uiComponentId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public String getEmployeeGroupCode() {
		return employeeGroupCode;
	}
	public void setEmployeeGroupCode(String employeeGroupCode) {
		this.employeeGroupCode = employeeGroupCode;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public EnumSecurityType getSecurityType() {
		return securityType;
	}
	public void setSecurityType(EnumSecurityType securityType) {
		this.securityType = securityType;
	}
	
	
}

package com.alt.datacarrier.formframework.request;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;

public class FieldSecurityReadRequest implements IRequestData{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5554489176989621507L;
	
	private String uiFormDocId;
	private String orgId;
	private String employeeGroupId;
	private String roleId;
	private String stageId;
	private String bundleId;
	private String userId;
	private List<EnumPlatform> platform;
	public String getUiFormDocId() {
		return uiFormDocId;
	}
	public void setUiFormDocId(String uiFormDocId) {
		this.uiFormDocId = uiFormDocId;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getEmployeeGroupId() {
		return employeeGroupId;
	}
	public void setEmployeeGroupId(String employeeGroupId) {
		this.employeeGroupId = employeeGroupId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getStageId() {
		return stageId;
	}
	public void setStageId(String stageId) {
		this.stageId = stageId;
	}
	public String getBundleId() {
		return bundleId;
	}
	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<EnumPlatform> getPlatform() {
		return platform;
	}
	public void setPlatform(List<EnumPlatform> platform) {
		this.platform = platform;
	}
	
	

}

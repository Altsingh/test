package com.alt.datacarrier.formframework.request;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.AltOption;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datacarrier.kernel.uiclass.EnumFormType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FormCRUDData implements Serializable {

	private static final long serialVersionUID = -4229919855943668860L;

	@JsonIgnore
	@JsonProperty("status")
	private EnumDocStatusType status;

	@JsonIgnore
	@JsonProperty("type")
	private EnumFormType type;

	@JsonIgnore
	@JsonProperty("orgCode")
	private String orgCode;

	@JsonIgnore
	@JsonProperty("uiClassName")
	private String uiClassName;

	@JsonIgnore
	@JsonProperty("uiClassCode")
	private String uiClassCode;

	@JsonIgnore
	@JsonProperty("customComponentAllowed")
	private Boolean customComponentAllowed;

	@JsonIgnore
	@JsonProperty("moduleCode")
	private String moduleCode;

	@JsonIgnore
	@JsonProperty("description")
	private String description;

	@JsonIgnore
	@JsonProperty("formUpdateDetails")
	private List<FormUpdateDetails> formUpdateDetails;

	@JsonIgnore
	@JsonProperty("componentList")
	private List<AltAbstractComponent> componentList;

	@JsonIgnore
	@JsonProperty("cas")
	private String cas;

	@JsonIgnore
	@JsonProperty("readLock")
	private Boolean readLock = false;

	@JsonIgnore
	@JsonProperty("versionId")
	private String versionId;

	@JsonIgnore
	@JsonProperty("formState")
	private EnumFormState formState;

	@JsonIgnore
	@JsonProperty("componentCounter")
	private String componentCounter;

	@JsonIgnore
	@JsonProperty("metaclassid")
	private Integer metaclassid;

	@JsonIgnore
	@JsonProperty("attributevaluebyattributename")
	private HashMap<String, List<AltOption>> attributevaluebyattributename;

	@JsonIgnore
	@JsonProperty("userfilledValuesMap")
	private Map<String, List<String>> userFilledValuesMap;
	
	@JsonIgnore
	@JsonProperty("taskCodeRequired")
	private Boolean taskCodeRequired;

	private Long hrFormId;

	private Integer appid;

	private Integer parentFormId;

	private FormCRUDData(Builder builder) {
		setStatus(builder.status);
		setType(builder.type);
		setOrgCode(builder.orgCode);
		setUiClassName(builder.uiClassName);
		setUiClassCode(builder.uiClassCode);
		setCustomComponentAllowed(builder.customComponentAllowed);
		setModuleCode(builder.moduleCode);
		setDescription(builder.description);
		setFormUpdateDetails(builder.formUpdateDetails);
		setComponentList(builder.componentList);
		setCas(builder.cas);
		setReadLock(builder.readLock);
		setVersionId(builder.versionId);
		setFormState(builder.formState);
		setComponentCounter(builder.componentCounter);
		setMetaClassId(builder.metaclassid);
		setAttributevaluebyattributename(builder.attributevaluebyattributename);
		setUserFilledValuesMap(builder.userFilledValuesMap);
		setHrFormId(builder.hrFormId);
		setAppid(builder.appid);
		setParentFormId(builder.parentFormId);
		setTaskCodeRequired(builder.taskCodeRequired);
	}

	public EnumFormState getFormState() {
		return formState;
	}

	public void setFormState(EnumFormState formState) {
		this.formState = formState;
	}

	public String getComponentCounter() {
		return componentCounter;
	}

	public void setComponentCounter(String componentCounter) {
		this.componentCounter = componentCounter;
	}

	public FormCRUDData() {

	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getCas() {
		return cas;
	}

	public void setCas(String cas) {
		this.cas = cas;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	public EnumFormType getType() {
		return type;
	}

	public void setType(EnumFormType type) {
		this.type = type;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getUiClassName() {
		return uiClassName;
	}

	public void setUiClassName(String uiClassName) {
		this.uiClassName = uiClassName;
	}

	public String getUiClassCode() {
		return uiClassCode;
	}

	public void setUiClassCode(String uiClassCode) {
		this.uiClassCode = uiClassCode;
	}

	public void setMetaClassId(Integer metaclassid) {
		this.metaclassid = metaclassid;
	}

	public HashMap<String, List<AltOption>> getAttributevaluebyattributename() {
		return attributevaluebyattributename;
	}

	public void setAttributevaluebyattributename(HashMap<String, List<AltOption>> attributevaluebyattributename) {
		this.attributevaluebyattributename = attributevaluebyattributename;
	}

	public Map<String, List<String>> getUserFilledValuesMap() {
		return userFilledValuesMap;
	}

	public void setUserFilledValuesMap(Map<String, List<String>> userFilledValuesMap) {
		this.userFilledValuesMap = userFilledValuesMap;
	}

	public Boolean isCustomComponentAllowed() {
		return customComponentAllowed;
	}

	public void setCustomComponentAllowed(Boolean customComponentAllowed) {
		this.customComponentAllowed = customComponentAllowed;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<FormUpdateDetails> getFormUpdateDetails() {
		return formUpdateDetails;
	}

	public void setFormUpdateDetails(List<FormUpdateDetails> formUpdateDetails) {
		this.formUpdateDetails = formUpdateDetails;
	}

	public List<AltAbstractComponent> getComponentList() {
		return componentList;
	}

	public void setComponentList(List<AltAbstractComponent> componentList) {
		this.componentList = componentList;
	}

	public Boolean getReadLock() {
		return readLock;
	}

	public void setReadLock(Boolean readLock) {
		this.readLock = readLock;
	}

	public Integer getMetaclassid() {
		return metaclassid;
	}

	public void setMetaclassid(Integer metaclassid) {
		this.metaclassid = metaclassid;
	}

	public Long getHrFormId() {
		return hrFormId;
	}

	public void setHrFormId(Long hrFormId) {
		this.hrFormId = hrFormId;
	}

	public Integer getAppid() {
		return appid;
	}

	public void setAppid(Integer appid) {
		this.appid = appid;
	}

	public Integer getParentFormId() {
		return parentFormId;
	}

	public void setParentFormId(Integer parentFormId) {
		this.parentFormId = parentFormId;
	}

	public Boolean getTaskCodeRequired() {
		return taskCodeRequired;
	}

	public void setTaskCodeRequired(Boolean taskCodeRequired) {
		this.taskCodeRequired = taskCodeRequired;
	}

	public static final class Builder {
		private Boolean taskCodeRequired;
		private EnumDocStatusType status;
		private EnumFormType type;
		private String orgCode;
		private String uiClassName;
		private String uiClassCode;
		private Boolean customComponentAllowed;
		private String moduleCode;
		private String description;
		private List<FormUpdateDetails> formUpdateDetails;
		private List<AltAbstractComponent> componentList;
		private String cas;
		private Boolean readLock;
		private String versionId;
		private EnumFormState formState;
		private String componentCounter;
		private Integer metaclassid;
		private HashMap<String, List<AltOption>> attributevaluebyattributename;
		private HashMap<String, List<String>> userFilledValuesMap;
		private Long hrFormId;
		private Integer appid;
		private Integer parentFormId;

		public Builder() {
		}

		public Builder taskCodeRequired(Boolean val) {
			taskCodeRequired = val;
			return this;
		}
		
		public Builder status(EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder type(EnumFormType val) {
			type = val;
			return this;
		}

		public Builder orgCode(String val) {
			orgCode = val;
			return this;
		}

		public Builder uiClassName(String val) {
			uiClassName = val;
			return this;
		}

		public Builder uiClassCode(String val) {
			uiClassCode = val;
			return this;
		}

		public Builder customComponentAllowed(Boolean val) {
			customComponentAllowed = val;
			return this;
		}

		public Builder moduleCode(String val) {
			moduleCode = val;
			return this;
		}

		public Builder description(String val) {
			description = val;
			return this;
		}

		public Builder formUpdateDetails(List<FormUpdateDetails> val) {
			formUpdateDetails = val;
			return this;
		}

		public Builder componentList(List<AltAbstractComponent> val) {
			componentList = val;
			return this;
		}

		public Builder cas(String val) {
			cas = val;
			return this;
		}

		public Builder readLock(Boolean val) {
			readLock = val;
			return this;
		}

		public Builder versionId(String val) {
			versionId = val;
			return this;
		}

		public Builder formState(EnumFormState val) {
			formState = val;
			return this;
		}

		public Builder componentCounter(String val) {
			componentCounter = val;
			return this;
		}

		public Builder metaClassId(Integer val) {
			metaclassid = val;
			return this;
		}

		public Builder attributeValueByAttributeName(HashMap<String, List<AltOption>> val) {
			attributevaluebyattributename = val;
			return this;
		}

		public Builder userFilledValuesMap(HashMap<String, List<String>> val) {
			userFilledValuesMap = val;
			return this;
		}

		public Builder withHrFormId(Long val) {
			hrFormId = val;
			return this;
		}

		public Builder withAppid(Integer val) {
			appid = val;
			return this;
		}

		public Builder withParentFormId(Integer val) {
			parentFormId = val;
			return this;
		}

		public FormCRUDData build() {
			return new FormCRUDData(this);
		}
	}
}

package com.alt.datacarrier.formframework.request;


import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FormCRUDRequest implements IRequestData{
	
	private static final long serialVersionUID = 5634605164582110529L;

	@JsonProperty("action")
	private EnumCRUDOperations action;

	@JsonProperty("uiClassData")
	private FormCRUDData uiClassData;
	
	public EnumCRUDOperations getAction() {
		return action;
	}
	
	public void setAction(EnumCRUDOperations action) {
		this.action = action;
	}

	public FormCRUDData getUiClassData() {
		return uiClassData;
	}

	public void setUiClassData(FormCRUDData uiClassData) {
		this.uiClassData = uiClassData;
	}

}

package com.alt.datacarrier.formframework.request;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;

public class FormCopyRequest implements IRequestData{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8376382782196569566L;
	private List<String> systemUiFormId;
	private String orgId;
	
	public List<String> getSystemUiFormId() {
		return systemUiFormId;
	}
	public void setSystemUiFormId(List<String> systemUiFormId) {
		this.systemUiFormId = systemUiFormId;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
	

}

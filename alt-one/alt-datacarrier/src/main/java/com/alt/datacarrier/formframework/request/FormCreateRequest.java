package com.alt.datacarrier.formframework.request;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datacarrier.kernel.uiclass.EnumFormType;

public class FormCreateRequest implements IRequestData {

	private static final long serialVersionUID = 2007095249758302419L;

	private EnumDocStatusType status;

	private EnumFormType type;

	private String orgCode;

	private Integer orgId;

	private String uiClassName;

	private boolean customComponentAllowed;

	private String moduleCode;

	private String description;

	private String componentCounter;

	private List<AltAbstractComponent> componentList;

	private String bundleId;

	private FormMapping formMapping;

	private EnumFormState formState;

	private Long hrFormId;

	private Integer appId;

	private Integer metaclassid;

	private Integer parentFormId;

	public FormCreateRequest() {
	}

	private FormCreateRequest(Builder builder) {
		status = builder.status;
		type = builder.type;
		orgCode = builder.orgCode;
		orgId = builder.orgId;
		uiClassName = builder.uiClassName;
		customComponentAllowed = builder.customComponentAllowed;
		moduleCode = builder.moduleCode;
		description = builder.description;
		componentCounter = builder.componentCounter;
		componentList = builder.componentList;
		bundleId = builder.bundleId;
		setFormMapping(builder.formMapping);
		formState = builder.formState;
		hrFormId = builder.hrFormId;
		appId = builder.appId;
		metaclassid = builder.metaclassid;
		parentFormId = builder.parentFormId;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public EnumFormType getType() {
		return type;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public String getUiClassName() {
		return uiClassName;
	}

	public boolean isCustomComponentAllowed() {
		return customComponentAllowed;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public String getDescription() {
		return description;
	}

	public String getComponentCounter() {
		return componentCounter;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public List<AltAbstractComponent> getComponentList() {
		return componentList;
	}

	public String getBundleId() {
		return bundleId;
	}

	public FormMapping getFormMapping() {
		return formMapping;
	}

	public void setFormMapping(FormMapping formMapping) {
		this.formMapping = formMapping;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public EnumFormState getFormState() {
		return formState;
	}

	public Long getHrFormId() {
		return hrFormId;
	}

	public void setHrFormId(Long hrFormId) {
		this.hrFormId = hrFormId;
	}

	public Integer getMetaclassid() {
		return metaclassid;
	}

	public void setMetaclassid(Integer metaclassid) {
		this.metaclassid = metaclassid;
	}

	public Integer getParentFormId() {
		return parentFormId;
	}

	public void setParentFormId(Integer parentFormId) {
		this.parentFormId = parentFormId;
	}

	public static final class Builder {
		private EnumDocStatusType status;
		private EnumFormType type;
		private String orgCode;
		private Integer orgId;
		private String uiClassName;
		private boolean customComponentAllowed;
		private String moduleCode;
		private String description;
		private String componentCounter;
		private List<AltAbstractComponent> componentList;
		private String bundleId;
		private FormMapping formMapping;
		private EnumFormState formState;
		private Long hrFormId;
		public Integer appId;
		private Integer metaclassid;
		private Integer parentFormId;

		public Builder() {
		}

		public Builder withStatus(EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withType(EnumFormType val) {
			type = val;
			return this;
		}

		public Builder withOrgCode(String val) {
			orgCode = val;
			return this;
		}

		public Builder withUiClassName(String val) {
			uiClassName = val;
			return this;
		}

		public Builder withCustomComponentAllowed(boolean val) {
			customComponentAllowed = val;
			return this;
		}

		public Builder withModuleCode(String val) {
			moduleCode = val;
			return this;
		}

		public Builder withDescription(String val) {
			description = val;
			return this;
		}

		public Builder withComponentCounter(String val) {
			componentCounter = val;
			return this;
		}

		public Builder withComponentList(List<AltAbstractComponent> val) {
			componentList = val;
			return this;
		}

		public Builder withBundleId(String val) {
			bundleId = val;
			return this;
		}

		public Builder withOrgId(Integer orgId) {
			this.orgId = orgId;
			return this;
		}

		public Builder withAppId(Integer appId) {
			this.appId = appId;
			return this;
		}

		public Builder withFormMapping(FormMapping val) {
			formMapping = val;
			return this;
		}

		public Builder withFormState(EnumFormState val) {
			formState = val;
			return this;
		}

		public Builder withHrFormId(Long val) {
			hrFormId = val;
			return this;
		}

		public Builder withMetaclassid(Integer val) {
			metaclassid = val;
			return this;
		}

		public Builder withParentFormId(Integer val) {
			parentFormId = val;
			return this;
		}

		public FormCreateRequest build() {
			return new FormCreateRequest(this);
		}
	}

	@Override
	public String toString() {
		return "FormCreateRequest [status=" + status + ", type=" + type + ", orgCode=" + orgCode + ", orgId=" + orgId
				+ ", uiClassName=" + uiClassName + ", customComponentAllowed=" + customComponentAllowed
				+ ", moduleCode=" + moduleCode + ", description=" + description + ", componentCounter="
				+ componentCounter + ", componentList=" + componentList + ", bundleId=" + bundleId + ", formMapping="
				+ formMapping + ", formState=" + formState + ", hrFormId=" + hrFormId + "]";
	}
}

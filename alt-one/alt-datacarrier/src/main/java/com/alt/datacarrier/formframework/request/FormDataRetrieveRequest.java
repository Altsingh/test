package com.alt.datacarrier.formframework.request;

import java.io.Serializable;
import java.util.Collection;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.uiclass.FormNavigation;
import com.alt.datacarrier.organization.RoleTO;

public class FormDataRetrieveRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	String tenantId = "";
	String appId = "";
	String formId = "";
	String formName = "";
	String userName = "";
	String classid = "";
	String instanceId = null;
	Protocol protocol = null;
	Integer stageId = null;
	Collection<Integer> assignedRoleIds;
	Collection<RoleTO> formRoles;
	FormNavigation navigation;

	public FormDataRetrieveRequest(String tenantId, String appId, String formId, String formName, String instanceId) {
		this.tenantId = tenantId;
		this.appId = appId;
		this.formId = formId;
		this.formName = formName;
		this.instanceId = instanceId;
	}

	public FormDataRetrieveRequest() {

	}

	public String getAppId() {
		return appId;
	}

	public String getFormId() {
		return formId;
	}

	public String getFormName() {
		return formName;
	}

	public String getUserName() {
		return userName;
	}

	public String getClassid() {
		return classid;
	}

	public Protocol getProtocol() {
		return protocol;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setClassid(String classid) {
		this.classid = classid;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Integer getStageId() {
		return stageId;
	}

	public void setStageId(Integer stageId) {
		this.stageId = stageId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Collection<Integer> getAssignedRoleIds() {
		return assignedRoleIds;
	}

	public void setAssignedRoleIds(Collection<Integer> assignedRoleIds) {
		this.assignedRoleIds = assignedRoleIds;
	}

	public Collection<RoleTO> getFormRoles() {
		return formRoles;
	}

	public void setFormRoles(Collection<RoleTO> formRoles) {
		this.formRoles = formRoles;
	}

	public FormNavigation getNavigation() {
		return navigation;
	}

	public void setNavigation(FormNavigation navigation) {
		this.navigation = navigation;
	}

}

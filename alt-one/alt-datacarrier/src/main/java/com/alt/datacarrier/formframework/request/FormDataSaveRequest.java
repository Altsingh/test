package com.alt.datacarrier.formframework.request;

import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;

import java.util.List;

public class FormDataSaveRequest {

	private List<AltAbstractComponent> components;
	private String formId = "";
	private String formName = "";
	private String appId = "";
	private String instanceId = "";
	private Long sysUserWorkflowHistoryId;
	private Integer employeeId;
	private String appName;
	private boolean processHistory;

	public boolean isProcessHistory() {
		return processHistory;
	}

	public void setProcessHistory(boolean processHistory) {
		this.processHistory = processHistory;
	}

	public List<AltAbstractComponent> getComponents() {
		return components;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Long getSysUserWorkflowHistoryId() {
		return sysUserWorkflowHistoryId;
	}

	public void setSysUserWorkflowHistoryId(Long sysUserWorkflowHistoryId) {
		this.sysUserWorkflowHistoryId = sysUserWorkflowHistoryId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	@Override
	public String toString() {
		return "FormDataSaveRequest{" +
				"components=" + components +
				", formId='" + formId + '\'' +
				", formName='" + formName + '\'' +
				", appId='" + appId + '\'' +
				", instanceId='" + instanceId + '\'' +
				", sysUserWorkflowHistoryId=" + sysUserWorkflowHistoryId +
				", employeeId=" + employeeId +
				'}';
	}
}

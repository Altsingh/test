package com.alt.datacarrier.formframework.request;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.uiclass.AltCustomFormDataContextRequest;

public class FormInstanceFetchRequest implements IRequestData{

	private static final long serialVersionUID = -7193061337457121886L;

	private List<AltCustomFormDataContextRequest> contextRequests;
	
	private Long formSecurityId;

	public List<AltCustomFormDataContextRequest> getContextRequests() {
		return contextRequests;
	}

	public void setContextRequests(List<AltCustomFormDataContextRequest> contextRequests) {
		this.contextRequests = contextRequests;
	}

	public Long getFormSecurityId() {
		return formSecurityId;
	}

	public void setFormSecurityId(Long formSecurityId) {
		this.formSecurityId = formSecurityId;
	}

}

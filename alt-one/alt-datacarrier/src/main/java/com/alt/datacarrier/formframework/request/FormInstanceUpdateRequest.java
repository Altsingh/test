package com.alt.datacarrier.formframework.request;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.uiclass.AltCustomFormDataContextRequest;

public class FormInstanceUpdateRequest implements IRequestData{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -727334815804598466L;

	private List<AltCustomFormDataContextRequest> contextRequests;
	
	private AltForm formData;

	public List<AltCustomFormDataContextRequest> getContextRequests() {
		return contextRequests;
	}

	public void setContextRequests(List<AltCustomFormDataContextRequest> contextRequests) {
		this.contextRequests = contextRequests;
	}

	public AltForm getFormData() {
		return formData;
	}

	public void setFormData(AltForm formData) {
		this.formData = formData;
	}
	
	
	

}

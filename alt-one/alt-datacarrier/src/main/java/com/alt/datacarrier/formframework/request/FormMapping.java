package com.alt.datacarrier.formframework.request;

public class FormMapping {

    private FormMapping(Builder builder) {
        setFormId(builder.formId);
        customFormType = builder.customFormType;
        organization = builder.organization;
        orgId = builder.orgId;
        tenantId = builder.tenantId;
        customFormId = builder.customFormId;
        customFieldGroupId = builder.customFieldGroupId;
        customFieldGroupName = builder.customFieldGroupName;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    private String formId;

    private EnumFormMappingType customFormType;

    private String organization;

    private Integer orgId;

    private String tenantId;

    private String customFormId;

    private String customFieldGroupId;

    private String customFieldGroupName;

    public String getFormId() {
        return formId;
    }

    public EnumFormMappingType getCustomFormType() {
        return customFormType;
    }

    public String getOrganization() {
        return organization;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public String getCustomFormId() {
        return customFormId;
    }

    public String getCustomFieldGroupId() {
        return customFieldGroupId;
    }

    public String getCustomFieldGroupName() {
        return customFieldGroupName;
    }

    public FormMapping() {
    }

    public static final class Builder {
        private String formId;
        private EnumFormMappingType customFormType;
        private String organization;
        private Integer orgId;
        private String tenantId;
        private String customFormId;
        private String customFieldGroupId;
        private String customFieldGroupName;

        public Builder() {
        }

        public Builder withFormId(String val) {
            formId = val;
            return this;
        }

        public Builder withCustomFormType(EnumFormMappingType val) {
            customFormType = val;
            return this;
        }

        public Builder withOrganization(String val) {
            organization = val;
            return this;
        }

        public Builder withOrgId(Integer val) {
            orgId = val;
            return this;
        }

        public Builder withTenantId(String val) {
            tenantId = val;
            return this;
        }

        public Builder withCustomFormId(String val) {
            customFormId = val;
            return this;
        }

        public Builder withCustomFieldGroupId(String val) {
            customFieldGroupId = val;
            return this;
        }

        public Builder withCustomFieldGroupName(String val) {
            customFieldGroupName = val;
            return this;
        }

        public FormMapping build() {
            return new FormMapping(this);
        }
    }
}

package com.alt.datacarrier.formframework.request;

import java.util.List;

public class FormMetaRequest  {

	private String formUqnCode;

	private String bundle;
			
	private List<String> contextRoleIdentifiers;
	
	private String contextStageIdentifier;
	
	private List<String> contextEntityAttributeIdentifier;

	public String getFormUqnCode() {
		return formUqnCode;
	}

	public void setFormUqnCode(String formUqnCode) {
		this.formUqnCode = formUqnCode;
	}

	public String getBundle() {
		return bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

	public List<String> getContextRoleIdentifiers() {
		return contextRoleIdentifiers;
	}

	public void setContextRoleIdentifiers(List<String> contextRoleIdentifiers) {
		this.contextRoleIdentifiers = contextRoleIdentifiers;
	}

	public String getContextStageIdentifier() {
		return contextStageIdentifier;
	}

	public void setContextStageIdentifier(String contextStageIdentifier) {
		this.contextStageIdentifier = contextStageIdentifier;
	}

	public List<String> getContextEntityAttributeIdentifier() {
		return contextEntityAttributeIdentifier;
	}

	public void setContextEntityAttributeIdentifier(
			List<String> contextEntityAttributeIdentifier) {
		this.contextEntityAttributeIdentifier = contextEntityAttributeIdentifier;
	}
}
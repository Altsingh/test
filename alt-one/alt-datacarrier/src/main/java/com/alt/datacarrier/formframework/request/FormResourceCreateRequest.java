package com.alt.datacarrier.formframework.request;

import java.util.Map;

import com.alt.datacarrier.core.IRequestData;

public class FormResourceCreateRequest  implements IRequestData{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String formId;
	private String bundleId;
	private Map<String,String> resources;
	
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}
	public String getBundleId() {
		return bundleId;
	}
	public void setBundleId(String bundleId) {
		this.bundleId = bundleId;
	}
	public Map<String, String> getResources() {
		return resources;
	}
	public void setResources(Map<String, String> resources) {
		this.resources = resources;
	}
	
	

}

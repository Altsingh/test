package com.alt.datacarrier.formframework.request;

import com.alt.datacarrier.core.IRequestData;

public class FormResourceReadRequest implements IRequestData{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7774541089125691515L;
	private String formId;
	private String bundle;

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getBundle() {
		return bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}
}

package com.alt.datacarrier.formframework.request;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;

public class FormRetreiveRequest implements IRequestData{
	
	private static final long serialVersionUID = 6494360527583812074L;

	private List<String> altFormCodeList;
	private String bundle;
	
	public List<String> getAltFormCodeList() {
		return altFormCodeList;
	}

	public void setAltFormCodeList(List<String> altFormCodeList) {
		this.altFormCodeList = altFormCodeList;
	}

	public String getBundle() {
		return bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

}

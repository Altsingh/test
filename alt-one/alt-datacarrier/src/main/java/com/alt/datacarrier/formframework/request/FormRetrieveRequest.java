package com.alt.datacarrier.formframework.request;

import com.alt.datacarrier.core.IRequestData;

public class FormRetrieveRequest implements IRequestData{

	private int limit;

	private int offset;

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

}

package com.alt.datacarrier.formframework.request;

import java.io.Serializable;
import java.util.List;


import com.alt.datacarrier.kernel.common.KernelConstants.EnumComponentOperation;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FormUpdateDetails implements Serializable{

	private static final long serialVersionUID = 8724104622380590217L;

	@JsonIgnore
	@JsonProperty("componentOperation")
	private EnumComponentOperation componentOperation;
	
	/* List of sequence and component, e.g. 5,AltAbstractComponent~1, component will be created at position 5,
	 * panel path of component is determined by parsing id of AltAbstractComponent~1
	*/
	@JsonIgnore
	@JsonProperty("componentUpdateDetailsList")
	private List<ComponentUpdateDetails> componentUpdateDetailsList;
	

	public EnumComponentOperation getComponentOperation() {
		return componentOperation;
	}
	
	public void setComponentOperation(EnumComponentOperation componentOperation) {
		this.componentOperation = componentOperation;
	}

	public List<ComponentUpdateDetails> getComponentUpdateDetailsList() {
		return componentUpdateDetailsList;
	}

	public void setComponentUpdateDetailsList(List<ComponentUpdateDetails> componentUpdateDetailsList) {
		this.componentUpdateDetailsList = componentUpdateDetailsList;
	}

}

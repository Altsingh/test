package com.alt.datacarrier.formframework.request;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datacarrier.kernel.uiclass.EnumFormType;

public class FormUpdateRequest implements IRequestData {

	private static final long serialVersionUID = 2007095249758302419L;

	private Integer formId;

	private EnumDocStatusType status;

	private EnumFormType type;

	private String orgCode;

	private String uiClassName;

	private String uiClassCode;

	private boolean customComponentAllowed;

	private String moduleCode;

	private String description;

	private String componentCounter;

	private List<AltAbstractComponent> componentList;

	private String versionId;

	private String cas;

	private EnumFormState formState;

	private Integer appid;

	private String formName;

	private String userId;

	private Long hrFormId;

	private Integer metaclassid;

	private Integer parentFormId;
	
	private Boolean taskCodeEnabled;

	public FormUpdateRequest() {
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Integer getFormId() {
		return formId;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public EnumFormType getType() {
		return type;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public String getUiClassName() {
		return uiClassName;
	}

	public String getUiClassCode() {
		return uiClassCode;
	}

	public boolean isCustomComponentAllowed() {
		return customComponentAllowed;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public String getDescription() {
		return description;
	}

	public String getComponentCounter() {
		return componentCounter;
	}

	public List<AltAbstractComponent> getComponentList() {
		return componentList;
	}

	public String getVersionId() {
		return versionId;
	}

	public String getCas() {
		return cas;
	}

	public EnumFormState getFormState() {
		return formState;
	}

	public Long getHrFormId() {
		return hrFormId;
	}

	public void setHrFormId(Long hrFormId) {
		this.hrFormId = hrFormId;
	}

	public Integer getAppid() {
		return appid;
	}

	public void setAppid(Integer appid) {
		this.appid = appid;
	}

	public Integer getMetaclassid() {
		return metaclassid;
	}

	public void setMetaclassid(Integer metaclassid) {
		this.metaclassid = metaclassid;
	}

	public Integer getParentFormId() {
		return parentFormId;
	}

	public void setParentFormId(Integer parentFormId) {
		this.parentFormId = parentFormId;
	}

	public Boolean getTaskCodeEnabled() {
		return taskCodeEnabled;
	}

	public void setTaskCodeEnabled(Boolean taskCodeEnabled) {
		this.taskCodeEnabled = taskCodeEnabled;
	}

	private FormUpdateRequest(Builder builder) {
		formId = builder.formId;
		status = builder.status;
		type = builder.type;
		orgCode = builder.orgCode;
		uiClassName = builder.uiClassName;
		uiClassCode = builder.uiClassCode;
		customComponentAllowed = builder.customComponentAllowed;
		moduleCode = builder.moduleCode;
		description = builder.description;
		componentCounter = builder.componentCounter;
		componentList = builder.componentList;
		versionId = builder.versionId;
		cas = builder.cas;
		formState = builder.formState;
		taskCodeEnabled = builder.taskCodeEnabled;
	}

	public static final class Builder {
		private Boolean taskCodeEnabled;
		private Integer formId;
		private EnumDocStatusType status;
		private EnumFormType type;
		private String orgCode;
		private String uiClassName;
		private String uiClassCode;
		private boolean customComponentAllowed;
		private String moduleCode;
		private String description;
		private String componentCounter;
		private List<AltAbstractComponent> componentList;
		private String versionId;
		private String cas;
		private EnumFormState formState;

		public Builder() {
		}

		public Builder withFormId(Integer val) {
			formId = val;
			return this;
		}

		public Builder withStatus(EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withType(EnumFormType val) {
			type = val;
			return this;
		}

		public Builder withOrgCode(String val) {
			orgCode = val;
			return this;
		}

		public Builder withUiClassName(String val) {
			uiClassName = val;
			return this;
		}

		public Builder withUiClassCode(String val) {
			uiClassCode = val;
			return this;
		}

		public Builder withCustomComponentAllowed(boolean val) {
			customComponentAllowed = val;
			return this;
		}

		public Builder withModuleCode(String val) {
			moduleCode = val;
			return this;
		}

		public Builder withDescription(String val) {
			description = val;
			return this;
		}

		public Builder withComponentCounter(String val) {
			componentCounter = val;
			return this;
		}

		public Builder withComponentList(List<AltAbstractComponent> val) {
			componentList = val;
			return this;
		}

		public Builder withVersionId(String val) {
			versionId = val;
			return this;
		}

		public Builder withCas(String val) {
			cas = val;
			return this;
		}

		public Builder withFormState(EnumFormState val) {
			formState = val;
			return this;
		}
		
		public Builder withTaskCodeEnabled(Boolean val) {
			taskCodeEnabled = val;
			return this;
		}

		public FormUpdateRequest build() {
			return new FormUpdateRequest(this);
		}
	}
}

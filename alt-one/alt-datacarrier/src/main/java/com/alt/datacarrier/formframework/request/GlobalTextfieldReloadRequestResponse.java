package com.alt.datacarrier.formframework.request;

import java.util.List;

public class GlobalTextfieldReloadRequestResponse {

	private List<String> objectAttrList;

	private Integer employeeId;

	private List<String> objectAttrValues;

	public List<String> getObjectAttrList() {
		return objectAttrList;
	}

	public void setObjectAttrList(List<String> objectAttrList) {
		this.objectAttrList = objectAttrList;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public List<String> getObjectAttrValues() {
		return objectAttrValues;
	}

	public void setObjectAttrValues(List<String> objectAttrValues) {
		this.objectAttrValues = objectAttrValues;
	}

}

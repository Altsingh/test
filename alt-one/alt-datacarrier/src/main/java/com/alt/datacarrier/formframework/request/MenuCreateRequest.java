package com.alt.datacarrier.formframework.request;

import com.alt.datacarrier.core.Protocol;

import java.util.Map;


public class MenuCreateRequest {

	private Protocol protocol;

	private String name;

	private String modulePackage;

	private String menuType;

	private String parentMenu;

	private String uiClass;

	private String androidIcon;

	private String iosIcon;

	private String webIcon;

	private Integer webSequence;

	private Integer mobileSequence;

	private Map<String, String> bundle;

	public Map<String, String> getBundle() {
		return bundle;
	}

	public void setBundle(Map<String, String> bundle) {
		this.bundle = bundle;
	}

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModulePackage() {
		return modulePackage;
	}

	public void setModulePackage(String modulePackage) {
		this.modulePackage = modulePackage;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getParentMenu() {
		return parentMenu;
	}

	public void setParentMenu(String parentMenu) {
		this.parentMenu = parentMenu;
	}

	public String getUiClass() {
		return uiClass;
	}

	public void setUiClass(String uiClass) {
		this.uiClass = uiClass;
	}

	public String getAndroidIcon() {
		return androidIcon;
	}

	public void setAndroidIcon(String androidIcon) {
		this.androidIcon = androidIcon;
	}

	public String getIosIcon() {
		return iosIcon;
	}

	public void setIosIcon(String iosIcon) {
		this.iosIcon = iosIcon;
	}

	public String getWebIcon() {
		return webIcon;
	}

	public void setWebIcon(String webIcon) {
		this.webIcon = webIcon;
	}

	public Integer getWebSequence() {
		return webSequence;
	}

	public void setWebSequence(Integer webSequence) {
		this.webSequence = webSequence;
	}

	public Integer getMobileSequence() {
		return mobileSequence;
	}

	public void setMobileSequence(Integer mobileSequence) {
		this.mobileSequence = mobileSequence;
	}

}

package com.alt.datacarrier.formframework.request;

import com.alt.datacarrier.core.IRequestData;

public class OrganizationSecurityCreateRequest  implements IRequestData {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3380155820396069649L;
	private String uiClassCode;
	private String orgCode;
	public String getUiClassCode() {
		return uiClassCode;
	}
	public void setUiClassCode(String uiClassCode) {
		this.uiClassCode = uiClassCode;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	
	
	
}

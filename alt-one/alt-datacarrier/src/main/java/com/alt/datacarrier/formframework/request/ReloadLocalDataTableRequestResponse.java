package com.alt.datacarrier.formframework.request;

import java.util.List;

import com.alt.datacarrier.kernel.uiclass.AltDataTable;

public class ReloadLocalDataTableRequestResponse {

	private List<AltDataTable> tables;

	private Integer ownerEmployeeId;

	public List<AltDataTable> getTables() {
		return tables;
	}

	public void setTables(List<AltDataTable> tables) {
		this.tables = tables;
	}

	public Integer getOwnerEmployeeId() {
		return ownerEmployeeId;
	}

	public void setOwnerEmployeeId(Integer ownerEmployeeId) {
		this.ownerEmployeeId = ownerEmployeeId;
	}

}

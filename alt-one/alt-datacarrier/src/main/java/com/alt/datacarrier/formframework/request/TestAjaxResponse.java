package com.alt.datacarrier.formframework.request;

import java.io.Serializable;

public class TestAjaxResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] names;
	private Integer id;
	
	public TestAjaxResponse() {
		names = new String[3];
		names[0]="vaibhav";
		names[1]="kashyap";
		names[2]="peoplestrong";
	}
	
	public String[] getNames() {
		return names;
	}
	public void setNames(String[] names) {
		this.names = names;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}

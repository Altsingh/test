package com.alt.datacarrier.formframework.request;

import java.util.Collection;

import com.alt.datacarrier.kernel.uiclass.AltDataTable;
import com.alt.datacarrier.organization.RoleTO;

public class UploadDataTableRequestResponse {

	private String tenantId;

	private Collection<RoleTO> formRoles;

	private String appId;

	private AltDataTable table;

	private String fileDataBase64;

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Collection<RoleTO> getFormRoles() {
		return formRoles;
	}

	public void setFormRoles(Collection<RoleTO> formRoles) {
		this.formRoles = formRoles;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public AltDataTable getTable() {
		return table;
	}

	public void setTable(AltDataTable table) {
		this.table = table;
	}

	public String getFileDataBase64() {
		return fileDataBase64;
	}

	public void setFileDataBase64(String fileDataBase64) {
		this.fileDataBase64 = fileDataBase64;
	}

}

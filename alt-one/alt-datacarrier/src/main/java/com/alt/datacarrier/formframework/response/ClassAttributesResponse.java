package com.alt.datacarrier.formframework.response;

import java.util.List;

public class ClassAttributesResponse {
	
    private List<String> attributes;

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }
}

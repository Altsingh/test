package com.alt.datacarrier.formframework.response;

public class ClassIdAndNameResponse {
    
	private String id;
    
    private String name;

    public ClassIdAndNameResponse(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

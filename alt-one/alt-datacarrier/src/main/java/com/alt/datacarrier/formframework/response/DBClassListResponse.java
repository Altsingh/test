package com.alt.datacarrier.formframework.response;

import java.util.List;

import com.alt.datacarrier.core.IResponseData;

public class DBClassListResponse implements IResponseData{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2302140625669561069L;
	private List<String> dbClassList;

	public List<String> getDbClassList() {
		return dbClassList;
	}

	public void setDbClassList(List<String> dbClassList) {
		this.dbClassList = dbClassList;
	}

}

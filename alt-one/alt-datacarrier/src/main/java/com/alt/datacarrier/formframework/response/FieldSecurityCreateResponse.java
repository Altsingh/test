package com.alt.datacarrier.formframework.response;

import com.alt.datacarrier.core.IResponseData;

public class FieldSecurityCreateResponse implements IResponseData{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4547488467515969036L;
	private String code;
	private String message;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}

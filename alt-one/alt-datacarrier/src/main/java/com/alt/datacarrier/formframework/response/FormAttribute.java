package com.alt.datacarrier.formframework.response;

public abstract class FormAttribute {
	
	private String attributeIdentifier;
	
	private String css;
	
	// TODO : convert to ENUm PANEL/ WIDGET/ DATEPICKER/ INPUT TEXT/ RADIO/CHECK  
	private String representation;
	
	// TODO : convert to ENUm : DENY/ VIEWABLE/ EM/ ENM 
	private String configurationValue;
	
	private String label;
}

package com.alt.datacarrier.formframework.response;

import java.util.Map;

import com.alt.datacarrier.core.IResponseData;
import com.alt.datacarrier.kernel.uiclass.AltForm;

public class FormCRUDResponse implements IResponseData{
	
	private static final long serialVersionUID = 4304912049307546178L;
	
	private String code;
	
	private String alertMessage;
	
	private String formDocIdentifier;
	
	private Map<String, AltForm> formMap;
	
	public String getFormDocIdentifier() {
		return formDocIdentifier;
	}
	
	public void setFormDocIdentifier(String formDocIdentifier) {
		this.formDocIdentifier = formDocIdentifier;
	}
	
	public Map<String, AltForm> getFormMap() {
		return formMap;
	}

	public void setFormMap(Map<String, AltForm> formMap) {
		this.formMap = formMap;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAlertMessage() {
		return alertMessage;
	}

	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}

}
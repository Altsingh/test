package com.alt.datacarrier.formframework.response;

import java.util.List;

import com.alt.datacarrier.core.IResponseData;

public class FormCopyResponse implements IResponseData{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5292278321620540098L;
	private List<String> uiFormDocIdentifier;
	
	public List<String> getUiFormDocIdentifier() {
		return uiFormDocIdentifier;
	}
	public void setUiFormDocIdentifier(List<String> uiFormDocIdentifier) {
		this.uiFormDocIdentifier = uiFormDocIdentifier;
	}
	
	
}

package com.alt.datacarrier.formframework.response;

import com.alt.datacarrier.core.IResponseData;

public class FormCreateResponse implements IResponseData {

    private static final long serialVersionUID = 950862173451232343L;

    private boolean success;

    private String responseMessage;

    private String cas;

    private String formId;

    public FormCreateResponse() {
    }

    public FormCreateResponse(boolean success, String responseMessage, String cas, String formId) {
        this.success = success;
        this.responseMessage = responseMessage;
        this.cas = cas;
        this.formId = formId;
    }

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }
}

package com.alt.datacarrier.formframework.response;

public class FormDetails {

	private String formName;

	private String createdBy;

	private String formState;

	private String uiClassCode;

	public FormDetails(String formName, String createdBy, String formState, String uiClassCode) {
		this.formName = formName;
		this.createdBy = createdBy;
		this.formState = formState;
		this.uiClassCode = uiClassCode;
	}

	public String getUiClassCode() {
		return uiClassCode;
	}

	public void setUiClassCode(String uiClassCode) {
		this.uiClassCode = uiClassCode;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getFormState() {
		return formState;
	}

	public void setStatus(String formState) {
		this.formState = formState;
	}

}

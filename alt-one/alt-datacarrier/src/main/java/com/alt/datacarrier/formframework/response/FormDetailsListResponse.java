package com.alt.datacarrier.formframework.response;

import java.util.List;

public class FormDetailsListResponse {
	private List<FormDetails> formDetails;

	public List<FormDetails> getFormDetails() {
		return formDetails;
	}

	public void setFormDetails(List<FormDetails> formDetails) {
		this.formDetails = formDetails;
	}

}

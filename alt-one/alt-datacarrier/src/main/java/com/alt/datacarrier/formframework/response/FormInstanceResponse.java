package com.alt.datacarrier.formframework.response;

import com.alt.datacarrier.core.IResponseData;
import com.alt.datacarrier.kernel.db.core.AltForm;

public class FormInstanceResponse implements IResponseData {

	private static final long serialVersionUID = -7172983054417490852L;

	private boolean success;

	private String responseMessage;
	
	private AltForm form;

	public FormInstanceResponse() {
		super();
	}

	public FormInstanceResponse(boolean success, String responseMessage) {
		super();
		this.success = success;
		this.responseMessage = responseMessage;
	}
	
	public FormInstanceResponse(boolean success, String responseMessage, AltForm form) {
		super();
		this.success = success;
		this.responseMessage = responseMessage;
		this.form = form;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public AltForm getForm() {
		return form;
	}

	public void setForm(AltForm form) {
		this.form = form;
	}

}

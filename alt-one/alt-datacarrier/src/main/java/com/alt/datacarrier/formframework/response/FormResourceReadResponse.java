package com.alt.datacarrier.formframework.response;

import java.util.Map;

import com.alt.datacarrier.core.IResponseData;

public class FormResourceReadResponse  implements IResponseData{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2220858576624591729L;
	private Map<String,String> resourceMap;
	public Map<String,String> getResourceMap() {
		return resourceMap;
	}
	public void setResourceMap(Map<String,String> resourceMap) {
		this.resourceMap = resourceMap;
	}

}

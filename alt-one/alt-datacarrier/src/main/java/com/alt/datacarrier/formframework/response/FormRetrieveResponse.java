package com.alt.datacarrier.formframework.response;

import java.util.List;

import com.alt.datacarrier.core.IResponseData;
import com.alt.datacarrier.kernel.uiclass.AltForm;

public class FormRetrieveResponse implements IResponseData{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2345037121850231259L;
	private List<AltForm> formList;
	
	public List<AltForm> getFormList() {
		return formList;
	}
	public void setFormList(List<AltForm> formList) {
		this.formList = formList;
	}

	

}

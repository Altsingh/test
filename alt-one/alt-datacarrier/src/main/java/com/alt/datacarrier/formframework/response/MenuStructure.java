package com.alt.datacarrier.formframework.response;

import java.util.List;


public class MenuStructure {

	private String docId;

	private String name;

	private String modulePackage;

	private String label;

	private List<MenuStructure> childMenu;

	private String uiClass;

	private String icon;

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModulePackage() {
		return modulePackage;
	}

	public void setModulePackage(String modulePackage) {
		this.modulePackage = modulePackage;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<MenuStructure> getChildMenu() {
		return childMenu;
	}

	public void setChildMenu(List<MenuStructure> childMenu) {
		this.childMenu = childMenu;
	}

	public String getUiClass() {
		return uiClass;
	}

	public void setUiClass(String uiClass) {
		this.uiClass = uiClass;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}

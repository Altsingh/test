package com.alt.datacarrier.formsecurity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class GroupPolicyTO implements Serializable {

	private static final long serialVersionUID = 1422521730459826763L;

	@JsonProperty("ALTGROUPID")
	private String altGroupID;

	@JsonProperty("GROUPPOLICYCODE")
	private String groupPolicyCode;

	public String getAltGroupID() {
		return altGroupID;
	}

	public void setAltGroupID(String altGroupID) {
		this.altGroupID = altGroupID;
	}

	public String getGroupPolicyCode() {
		return groupPolicyCode;
	}

	public void setGroupPolicyCode(String groupPolicyCode) {
		this.groupPolicyCode = groupPolicyCode;
	}

}

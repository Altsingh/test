package com.alt.datacarrier.formsecurity;

public interface IFormSecurityConstant {
	public static final String FORM_ROLE = "FORM_ROLE";

	public static final String FORM_STAGE = "FORM_STAGE";

	public static final String FORM_TABLE = "FORM_TABLE";

	public static final String FORM_ROLE_STAGE = "FORM_ROLE_STAGE";

	public static final String FORM_ROLE_TABLE = "FORM_ROLE_TABLE";

	public static final String FORM_STAGE_TABLE = "FORM_STAGE_TABLE";

	public static final String FORM_ROLE_STAGE_TABLE = "FORM_ROLE_STAGE_TABLE";

	public static final String FORM_COUNTRY = "FORM_COUNTRY";

	public static final String FORM_COUNTRY_ROLE = "FORM_COUNTRY_ROLE";

	public static final String FORM_COUNTRY_STAGE = "FORM_COUNTRY_STAGE";

	public static final String FORM_COUNTRY_ROLE_STAGE = "FORM_COUNTRY_ROLE_STAGE";

	public static final String DEFAULT_BUNDLE = "EN";

	public static final String DEFAULT_PLATFORM = "WEB";

	public static final String MOBILE_PLATFORM = "MOBILE";

	public static final String LABEL = "LABEL";

	public static final String TITLE = "TITLE";

	public static enum FormConfiguration {
		DENY, VIEWABLE, EDITABLE_NONMANDATORY, EDITABLE_MANDATORY;

		public static boolean contains(String test) {
			for (FormConfiguration c : FormConfiguration.values()) {
				if (c.name().equals(test)) {
					return true;
				}
			}
			return false;
		}
	}

	public static enum FormCongfigurationResourceType {
		FIELD, FORM;
	}

	public static final String MARKER_CUSTOM_FORM = "CUSTOM_FORM";

	public static final String MARKER_CUSTOM_FIELD_GROUP = "CUSTOM_FORM.CUSTOM_FORM_FIELD_GROUP";

	public static final String MARKER_CUSTOM_FIELD = "CUSTOM_FORM.CUSTOM_FORM_FIELD_GROUP.CUSTOM_FIELD";

	public static final String MARKER_ALTONE_CUSTOM_FORM = "AltOneCustomForm";

	public static final String MARKER_ALTONE_CUSTOM_FIELD_GROUP = "AltOneCustomForm.AltOneCustomFieldGroup";

	public static final String MARKER_ALTONE_CUSTOM_FIELD = "AltOneCustomForm.AltOneCustomFieldGroup.AltOneCustomField";

	public static final Integer SYS_TENANT_ID = 1;

	public static enum FormCongfigurationPlatformType {
		MOBILE, WEB;
	}

	public static final String RECRUIT_STAGE_STATUS = "RECRUIT_STAGE_STATUS";
}

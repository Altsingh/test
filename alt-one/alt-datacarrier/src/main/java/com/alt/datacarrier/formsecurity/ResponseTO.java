package com.alt.datacarrier.formsecurity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ResponseTO<T extends Object> implements Serializable {

	private static final long serialVersionUID = -2224830115923531063L;

	private String responeMessage;

	private String responseCode;

	private List<T> data;

	private Map<String, List<T>> dataMap;

	public String getResponeMessage() {
		return responeMessage;
	}

	public void setResponeMessage(String responeMessage) {
		this.responeMessage = responeMessage;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public Map<String, List<T>> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, List<T>> dataMap) {
		this.dataMap = dataMap;
	}

}

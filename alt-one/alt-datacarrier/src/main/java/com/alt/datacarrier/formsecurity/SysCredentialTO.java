package com.alt.datacarrier.formsecurity;

import java.util.List;
import java.util.Set;

public class SysCredentialTO {

	private String formName;

	private Set<Integer> roleList;

	private Integer tenantID;

	private Integer organizationID;

	private Integer userID;

	private Integer defaultSystemOrgId;

	private Integer defaultSystemTenantId;

	private Integer stageID;

	private String tableName;

	private Integer instanceID;

	private String bundle;

	private boolean tableSecurityEnable;

	private boolean customForm;

	private String platform;

	private Long countryId;

	private Integer recruitWorkflowID;

	private Integer recruitStatusID;

	private List<GroupPolicyTO> groupPolicyList;

	private ResponseTO<Integer> userGroupList;

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public Set<Integer> getRoleList() {
		return roleList;
	}

	public void setRoleList(Set<Integer> roleList) {
		this.roleList = roleList;
	}

	public Integer getTenantID() {
		return tenantID;
	}

	public void setTenantID(Integer tenantID) {
		this.tenantID = tenantID;
	}

	public Integer getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(Integer organizationID) {
		this.organizationID = organizationID;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public Integer getDefaultSystemOrgId() {
		return defaultSystemOrgId;
	}

	public void setDefaultSystemOrgId(Integer defaultSystemOrgId) {
		this.defaultSystemOrgId = defaultSystemOrgId;
	}

	public Integer getDefaultSystemTenantId() {
		return defaultSystemTenantId;
	}

	public void setDefaultSystemTenantId(Integer defaultSystemTenantId) {
		this.defaultSystemTenantId = defaultSystemTenantId;
	}

	public Integer getStageID() {
		return stageID;
	}

	public void setStageID(Integer stageID) {
		this.stageID = stageID;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Integer getInstanceID() {
		return instanceID;
	}

	public void setInstanceID(Integer instanceID) {
		this.instanceID = instanceID;
	}

	public String getBundle() {
		return bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}

	public boolean isTableSecurityEnable() {
		return tableSecurityEnable;
	}

	public void setTableSecurityEnable(boolean tableSecurityEnable) {
		this.tableSecurityEnable = tableSecurityEnable;
	}

	public boolean isCustomForm() {
		return customForm;
	}

	public void setCustomForm(boolean customForm) {
		this.customForm = customForm;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Integer getRecruitWorkflowID() {
		return recruitWorkflowID;
	}

	public void setRecruitWorkflowID(Integer recruitWorkflowID) {
		this.recruitWorkflowID = recruitWorkflowID;
	}

	public Integer getRecruitStatusID() {
		return recruitStatusID;
	}

	public void setRecruitStatusID(Integer recruitStatusID) {
		this.recruitStatusID = recruitStatusID;
	}

	public List<GroupPolicyTO> getGroupPolicyList() {
		return groupPolicyList;
	}

	public void setGroupPolicyList(List<GroupPolicyTO> groupPolicyList) {
		this.groupPolicyList = groupPolicyList;
	}

	public ResponseTO<Integer> getUserGroupList() {
		return userGroupList;
	}

	public void setUserGroupList(ResponseTO<Integer> userGroupList) {
		this.userGroupList = userGroupList;
	}

}

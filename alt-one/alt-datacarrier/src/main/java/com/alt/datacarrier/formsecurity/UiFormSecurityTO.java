package com.alt.datacarrier.formsecurity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.alt.datacarrier.formsecurity.IFormSecurityConstant.FormConfiguration;

public class UiFormSecurityTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer formSecurityID;

	private String formFieldClientID;

	private int uiFormFieldID;

	private String fieldLabel;

	private String title;

	private Boolean required;

	private Boolean viewable;

	private Boolean editable;

	private Integer sequence;

	private Integer roleId;

	private boolean orgLevelActive;

	private Integer resourceId;

	private long hrFormFieldID;

	// If component is 'commondButton or such other, set it true'
	private boolean actionType;

	private Set<String> actionRequiredClientIds;

	private String configurationValue;

	private boolean approvalRequired;

	private boolean effectiveDateRequired;

	private boolean attachmentRequired;

	private boolean attachmentEnabled;

	private String regex;

	private boolean customComponent;

	private boolean groupParameter;

	private Set<FormConfiguration> formConfiguration = new HashSet<FormConfiguration>();

	public UiFormSecurityTO() {
		this.viewable = false;
		this.editable = false;
		this.required = false;
	}

	public boolean isCustomComponent() {
		return customComponent;
	}

	public void setCustomComponent(boolean customComponent) {
		this.customComponent = customComponent;
	}

	public boolean isAttachmentEnabled() {
		return attachmentEnabled;
	}

	public void setAttachmentEnabled(boolean attachmentEnabled) {
		this.attachmentEnabled = attachmentEnabled;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}

	public String getFormFieldClientID() {
		return formFieldClientID;
	}

	public void setFormFieldClientID(String formFieldClientID) {
		this.formFieldClientID = formFieldClientID;
	}

	public String getFieldLabel() {
		return fieldLabel;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public Boolean getViewable() {
		return viewable;
	}

	public void setViewable(Boolean viewable) {
		this.viewable = viewable;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	public Integer getFormSecurityID() {
		return formSecurityID;
	}

	public void setFormSecurityID(Integer formSecurityID) {
		this.formSecurityID = formSecurityID;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getUiFormFieldID() {
		return uiFormFieldID;
	}

	public void setUiFormFieldID(int uiFormFieldID) {
		this.uiFormFieldID = uiFormFieldID;
	}

	public long getHrFormFieldID() {
		return hrFormFieldID;
	}

	public void setHrFormFieldID(long hrFormFieldID) {
		this.hrFormFieldID = hrFormFieldID;
	}

	public boolean isActionType() {
		return actionType;
	}

	public void setActionType(boolean actionType) {
		this.actionType = actionType;
	}

	public Set<String> getActionRequiredClientIds() {
		return actionRequiredClientIds;
	}

	public void setActionRequiredClientIds(Set<String> actionRequiredClientIds) {
		this.actionRequiredClientIds = actionRequiredClientIds;
	}

	public String getConfigurationValue() {
		return configurationValue;
	}

	public void setConfigurationValue(String configurationValue) {
		this.configurationValue = configurationValue;
	}

	public boolean isApprovalRequired() {
		return approvalRequired;
	}

	public void setApprovalRequired(boolean approvalRequired) {
		this.approvalRequired = approvalRequired;
	}

	public boolean isEffectiveDateRequired() {
		return effectiveDateRequired;
	}

	public void setEffectiveDateRequired(boolean effectiveDateRequired) {
		this.effectiveDateRequired = effectiveDateRequired;
	}

	public boolean isAttachmentRequired() {
		return attachmentRequired;
	}

	public void setAttachmentRequired(boolean attachmentRequired) {
		this.attachmentRequired = attachmentRequired;
	}

	public boolean isOrgLevelActive() {
		return orgLevelActive;
	}

	public void setOrgLevelActive(boolean orgLevelActive) {
		this.orgLevelActive = orgLevelActive;
	}

	public Integer getResourceId() {
		return resourceId;
	}

	public void setResourceId(Integer resourceId) {
		this.resourceId = resourceId;
	}

	public boolean isGroupParameter() {
		return groupParameter;
	}

	public void setGroupParameter(boolean groupParameter) {
		this.groupParameter = groupParameter;
	}

	public Set<FormConfiguration> getFormConfiguration() {
		return formConfiguration;
	}

	public void setFormConfiguration(Set<FormConfiguration> formConfiguration) {
		this.formConfiguration = formConfiguration;
	}

	public AltComponentSecurity getAltComponentSecurity() {
		AltComponentSecurity security = new AltComponentSecurity();
		security.setFieldLabel(this.fieldLabel);
		security.setTitle(this.title);
		security.setViewable(this.viewable);
		security.setEditable(this.editable);
		security.setRequired(this.required);
		return security;
	}
}

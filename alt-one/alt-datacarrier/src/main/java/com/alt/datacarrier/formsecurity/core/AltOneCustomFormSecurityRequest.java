package com.alt.datacarrier.formsecurity.core;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class AltOneCustomFormSecurityRequest implements Serializable {

	private static final long serialVersionUID = 2167562879038528156L;

	private List<FieldGroup> fieldGroups;

	private Long hrFormId;

	private String formName;

	private Boolean customComponentAllowed;

	private CRUDRequestType formRequestType;

	private MessageTO message;

	private String persistenceUnitName;

	private long organizationID;

	private long tenantID;

	private long createdBy;

	private long modifiedBy;

	private Date createdDate;

	private Date modifiedDate;

	public List<FieldGroup> getFieldGroups() {
		return fieldGroups;
	}

	public void setFieldGroups(List<FieldGroup> fieldGroups) {
		this.fieldGroups = fieldGroups;
	}

	public Long getHrFormId() {
		return hrFormId;
	}

	public void setHrFormId(Long hrFormId) {
		this.hrFormId = hrFormId;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public Boolean getCustomComponentAllowed() {
		return customComponentAllowed;
	}

	public void setCustomComponentAllowed(Boolean customComponentAllowed) {
		this.customComponentAllowed = customComponentAllowed;
	}

	public CRUDRequestType getFormRequestType() {
		return formRequestType;
	}

	public void setFormRequestType(CRUDRequestType formRequestType) {
		this.formRequestType = formRequestType;
	}

	public MessageTO getMessage() {
		return message;
	}

	public void setMessage(MessageTO message) {
		this.message = message;
	}

	public String getPersistenceUnitName() {
		return persistenceUnitName;
	}

	public void setPersistenceUnitName(String persistenceUnitName) {
		this.persistenceUnitName = persistenceUnitName;
	}

	public long getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(long organizationID) {
		this.organizationID = organizationID;
	}

	public long getTenantID() {
		return tenantID;
	}

	public void setTenantID(long tenantID) {
		this.tenantID = tenantID;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {
		return "AltOneCustomFormSecurityRequest{" + "fieldGroups=" + fieldGroups + ", hrFormId=" + hrFormId
				+ ", formName='" + formName + '\'' + ", customComponentAllowed=" + customComponentAllowed
				+ ", formRequestType=" + formRequestType + ", organizationID=" + organizationID + ", tenantID="
				+ tenantID + '}';
	}
}

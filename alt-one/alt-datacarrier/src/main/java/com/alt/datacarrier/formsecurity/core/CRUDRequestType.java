package com.alt.datacarrier.formsecurity.core;

import java.io.Serializable;

public enum CRUDRequestType implements Serializable{
	CREATE, UPDATE, DELETE, CHILD_UPDATE
}

package com.alt.datacarrier.formsecurity.core;

import java.io.Serializable;

public class Field implements Serializable {

	private static final long serialVersionUID = -8438433987472805861L;

	private String clientId;

	private CRUDRequestType fieldRequestType;

	private Boolean actionFlag;

	private Long hrFormFieldId;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public CRUDRequestType getFieldRequestType() {
		return fieldRequestType;
	}

	public void setFieldRequestType(CRUDRequestType fieldRequestType) {
		this.fieldRequestType = fieldRequestType;
	}

	public Boolean getActionFlag() {
		return actionFlag;
	}

	public void setActionFlag(Boolean actionFlag) {
		this.actionFlag = actionFlag;
	}

	public Long getHrFormFieldId() {
		return hrFormFieldId;
	}

	public void setHrFormFieldId(Long hrFormFieldId) {
		this.hrFormFieldId = hrFormFieldId;
	}

	@Override
	public String toString() {
		return "Field{" +
				"clientId='" + clientId + '\'' +
				", fieldRequestType=" + fieldRequestType +
				", actionFlag=" + actionFlag +
				", hrFormFieldId=" + hrFormFieldId +
				'}';
	}
}

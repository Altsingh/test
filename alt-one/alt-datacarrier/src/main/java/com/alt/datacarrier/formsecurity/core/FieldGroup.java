package com.alt.datacarrier.formsecurity.core;

import java.io.Serializable;
import java.util.List;

public class FieldGroup implements Serializable{
		
	private static final long serialVersionUID = 8531207317172909138L;

	private List<Field> fields; 
	
	private Long hrFormFieldGroupId;

	private String hrFieldGroupName;
	
	private CRUDRequestType fieldGroupRequestType;

	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public Long getHrFormFieldGroupId() {
		return hrFormFieldGroupId;
	}

	public void setHrFormFieldGroupId(Long hrFormFieldGroupId) {
		this.hrFormFieldGroupId = hrFormFieldGroupId;
	}

	public String getHrFieldGroupName() {
		return hrFieldGroupName;
	}

	public void setHrFieldGroupName(String hrFieldGroupName) {
		this.hrFieldGroupName = hrFieldGroupName;
	}

	public CRUDRequestType getFieldGroupRequestType() {
		return fieldGroupRequestType;
	}

	public void setFieldGroupRequestType(CRUDRequestType fieldGroupRequestType) {
		this.fieldGroupRequestType = fieldGroupRequestType;
	}

	@Override
	public String toString() {
		return "FieldGroup{" +
				"fields=" + fields +
				", hrFormFieldGroupId=" + hrFormFieldGroupId +
				", hrFieldGroupName='" + hrFieldGroupName + '\'' +
				", fieldGroupRequestType=" + fieldGroupRequestType +
				'}';
	}
}

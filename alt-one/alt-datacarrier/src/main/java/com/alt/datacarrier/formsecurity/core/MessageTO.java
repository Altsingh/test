package com.alt.datacarrier.formsecurity.core;

import java.io.Serializable;

public class MessageTO implements Serializable {

	private static final long serialVersionUID = -4729796509625152365L;

	private String code;

	private String message;

	private String description;

	public MessageTO() {
	}

	public MessageTO(String code, String message, String description) {
		this.code = code;
		this.message = message;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "MessageTO{" + "code='" + code + '\'' + ", message='" + message + '\'' + ", description='" + description
				+ '\'' + '}';
	}
}
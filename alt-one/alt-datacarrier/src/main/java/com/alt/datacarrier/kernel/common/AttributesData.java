package com.alt.datacarrier.kernel.common;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AttributesData implements Serializable{
	
	private static final long serialVersionUID = 4856045632898895831L;
	private boolean reference;
	private String attribute;
	private String refClass;
	private boolean unnest;
	private AttributesData refAttributeData;
	
	public boolean isReference() {
		return reference;
	}
	public void setReference(boolean reference) {
		this.reference = reference;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getRefClass() {
		return refClass;
	}
	public void setRefClass(String refClass) {
		this.refClass = refClass;
	}
	public AttributesData getRefAttributeData() {
		return refAttributeData;
	}
	
	public void setRefAttributeData(AttributesData refAttributeData) {
		this.refAttributeData = refAttributeData;
	}
	
	@JsonProperty("unnest")
	public boolean isUnnest() {
		return unnest;
	}
	
	@JsonProperty("unnest")
	public void setUnnest(boolean isUnnest) {
		this.unnest = isUnnest;
	}
	
}

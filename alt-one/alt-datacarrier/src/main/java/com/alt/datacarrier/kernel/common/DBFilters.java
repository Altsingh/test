package com.alt.datacarrier.kernel.common;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class <code> DBFilters.java</code>
 *
 * @author sandeep.verma
 *
 */
public class DBFilters  implements Serializable {
	
	private static final long serialVersionUID = -8733139036271303807L;
	private String classCode;
	private String attribute;
	private String condition;
	private String value;
	private boolean unnest;
	
	public String getClassCode() {
		return classCode;
	}
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@JsonProperty("unnest")
	public boolean isUnnest() {
		return unnest;
	}
	@JsonProperty("unnest")
	public void setUnnest(boolean isUnnest) {
		this.unnest = isUnnest;
	}
	
}

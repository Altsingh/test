package com.alt.datacarrier.kernel.common;

import com.alt.datacarrier.core.IResponseData;

public class FieldSecurityCreateResponse implements IResponseData{

	private String code;
	private String message;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}

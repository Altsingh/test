package com.alt.datacarrier.kernel.common;

import java.io.Serializable;

public class JoinDBClass  implements Serializable{
	
	private static final long serialVersionUID = 6436818400843941657L;

	private String dbClass;
	
	private String onKeysDbClass;
	
	private String onKeys;
	
	public String getDbClass() {
		return dbClass;
	}
	
	public void setDbClass(String dbClass) {
		this.dbClass = dbClass;
	}
	
	public String getOnKeys() {
		return onKeys;
	}
	
	public void setOnKeys(String onKeys) {
		this.onKeys = onKeys;
	}
	
	public String getOnKeysDbClass() {
		return onKeysDbClass;
	}
	
	public void setOnKeysDbClass(String onKeysDbClass) {
		this.onKeysDbClass = onKeysDbClass;
	}
	
}

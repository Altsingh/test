package com.alt.datacarrier.kernel.common;

import java.util.List;


import com.alt.datacarrier.core.AttributeMeta;
import com.alt.datacarrier.core.ClassMeta;
import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class <code>KernelCRUDClassRequest.java</code>
 *
 * @author rahul.chabba
 *
 * @createdOn Mar 14, 2017
 */

public class KernelCRUDClassRequest implements IRequestData {
	private static final long serialVersionUID = 6312121286492648923L;

	// TODO:
	// sortBy

	// TODO:
	// filters

	private EnumCRUDOperations action;

	private EnumRequestType type;

	private Boolean copyclass;

	private ClassMeta classMeta;

	private List<AttributeMeta> attributeMetaList;

	@JsonProperty("action")
	public EnumCRUDOperations getAction() {
		return action;
	}

	@JsonProperty("action")
	public void setAction(EnumCRUDOperations action) {
		this.action = action;
	}

	@JsonProperty("type")
	public EnumRequestType getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(EnumRequestType type) {
		this.type = type;
	}

	@JsonProperty("copyclass")
	public Boolean getCopyclass() {
		return copyclass;
	}

	@JsonProperty("copyclass")
	public void setCopyclass(Boolean copyclass) {
		this.copyclass = copyclass;
	}

	@JsonProperty("classMeta")
	public ClassMeta getClassMeta() {
		return classMeta;
	}

	@JsonProperty("classMeta")
	public void setClassMeta(ClassMeta classMeta) {
		this.classMeta = classMeta;
	}

	@JsonProperty("attributeMetaList")
	public List<AttributeMeta> getAttributeMetaList() {
		return attributeMetaList;
	}

	@JsonProperty("attributeMetaList")
	public void setAttributeMetaList(List<AttributeMeta> attributeMetaList) {
		this.attributeMetaList = attributeMetaList;
	}
}

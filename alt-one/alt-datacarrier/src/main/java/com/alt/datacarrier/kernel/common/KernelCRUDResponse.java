package com.alt.datacarrier.kernel.common;

import java.util.List;
import java.util.Map;

import com.alt.datacarrier.core.ClassMeta;
import com.alt.datacarrier.core.IResponseData;
import com.alt.datacarrier.core.InstanceData;

/**
 * The class <code>KernelReadResponse.java</code>
 *
 * @author rishi.gautam
 *
 * @createdOn Mar 3, 2017R
 */

public class KernelCRUDResponse implements IResponseData {
	
	private static final long serialVersionUID = 83484892424829429L;
	
	private String code;
	
	private String alertMessage;
	
	private Map<String, ClassMeta> meta;

	private List<List<InstanceData>> instance;
	
	private int documentCount;

	public List<List<InstanceData>> getInstance() {
		return instance;
	}

	public void setInstance(List<List<InstanceData>> instance) {
		this.instance = instance;
	}

	public Map<String, ClassMeta> getMeta() {
		return meta;
	}

	public void setMeta(Map<String, ClassMeta> meta) {
		this.meta = meta;
	}

	public String getCode() {
		return code;
	}

	public int getDocumentCount() {
		return documentCount;
	}

	public void setDocumentCount(int documentCount) {
		this.documentCount = documentCount;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAlertMessage() {
		return alertMessage;
	}

	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}
	
	

}
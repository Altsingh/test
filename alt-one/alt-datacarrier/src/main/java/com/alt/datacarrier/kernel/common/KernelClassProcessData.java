package com.alt.datacarrier.kernel.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


import com.alt.datacarrier.core.AttributeMeta;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumInstanceLimiter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class <code>KernelClassProcessData.java</code>
 *
 * @author  rishi.gautam
 *
 * @createdOn   Apr 3, 2017
 */

public class KernelClassProcessData implements Serializable{
	
	private static final long serialVersionUID = -8501709546761816627L;

	@JsonProperty("name")
	private String name;
	
	@JsonProperty("classCode")
	private String classCode;
	
	@JsonProperty("status")
	private EnumDocStatusType status;
	
	@JsonProperty("locationSpecific")
	private Boolean locationSpecific; 
	
	@JsonProperty("customAttributeAllowed")
	private Boolean customAttributeAllowed;
	
	@JsonProperty("packageCodes")
	private List<String> packageCodes;
	
	@JsonProperty("instanceLimiter")
	private EnumInstanceLimiter instanceLimiter;
	
	@JsonProperty("instancePrefix")
	private String instancePrefix;
	
	@JsonProperty("systemType")
	private Boolean systemType;
	
	@JsonProperty("attributes")
	private Map<EnumCRUDOperations, List<AttributeMeta>> attributes;
	
	@JsonProperty("parameterType")
	private Boolean parameterType;

	@JsonProperty("cas")
	private String cas;
	
	@JsonProperty("readLock")
	private boolean readLock;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	public Boolean getLocationSpecific() {
		return locationSpecific;
	}

	public void setLocationSpecific(Boolean locationSpecific) {
		this.locationSpecific = locationSpecific;
	}

	public Boolean getCustomAttributeAllowed() {
		return customAttributeAllowed;
	}

	public void setCustomAttributeAllowed(Boolean customAttributeAllowed) {
		this.customAttributeAllowed = customAttributeAllowed;
	}

	public List<String> getPackageCodes() {
		return packageCodes;
	}

	public void setPackageCodes(List<String> packageCodes) {
		this.packageCodes = packageCodes;
	}

	public EnumInstanceLimiter getInstanceLimiter() {
		return instanceLimiter;
	}

	public void setInstanceLimiter(EnumInstanceLimiter instanceLimiter) {
		this.instanceLimiter = instanceLimiter;
	}

	public String getInstancePrefix() {
		return instancePrefix;
	}

	public void setInstancePrefix(String instancePrefix) {
		this.instancePrefix = instancePrefix;
	}

	public Boolean getSystemType() {
		return systemType;
	}

	public void setSystemType(Boolean systemType) {
		this.systemType = systemType;
	}

	public String getCas() {
		return cas;
	}

	public void setCas(String cas) {
		this.cas = cas;
	}

	public Map<EnumCRUDOperations, List<AttributeMeta>> getAttributes() {
		return attributes;
	}

	public void setAttributes(
			Map<EnumCRUDOperations, List<AttributeMeta>> attributes) {
		this.attributes = attributes;
	}

	public Boolean getParameterType() {
		return parameterType;
	}

	public void setParameterType(Boolean parameterType) {
		this.parameterType = parameterType;
	}
	

	public boolean getReadLock() {
		return readLock;
	}

	public void setReadLock(boolean readLock) {
		this.readLock = readLock;
	}

	@Override
	public String toString() {
		return "KernelClassProcessData [name=" + name + ", classCode="
				+ classCode + ", status=" + status + ", locationSpecific="
				+ locationSpecific + ", customAttributeAllowed="
				+ customAttributeAllowed + ", packageCodes=" + packageCodes
				+ ", instanceLimiter=" + instanceLimiter + ", instancePrefix="
				+ instancePrefix + ", systemType=" + systemType
				+ ", attributes=" + attributes + " , readLock=" + readLock + "]";
	}

}
package com.alt.datacarrier.kernel.common;


import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class <code>KernelProcessClassRequest.java</code>
 *
 * @author rishi.gautam
 *
 * @createdOn Apr 3, 2017
 */

public class KernelClassProcessRequest implements IRequestData {

	private static final long serialVersionUID = 4924629466429L;

	@JsonProperty("action")
	private EnumCRUDOperations action;

	@JsonProperty("classData")
	private KernelClassProcessData classData;

	public EnumCRUDOperations getAction() {
		return action;
	}

	public void setAction(EnumCRUDOperations action) {
		this.action = action;
	}

	public KernelClassProcessData getClassData() {
		return classData;
	}

	public void setClassData(KernelClassProcessData classData) {
		this.classData = classData;
	}
	
	@Override
	public String toString() {
		return "KernelClassProcessRequest [action=" + action + ", classData="
				+ classData + "]";
	}
	
}
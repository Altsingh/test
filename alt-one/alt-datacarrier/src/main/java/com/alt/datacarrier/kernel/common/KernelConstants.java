package com.alt.datacarrier.kernel.common;

public class KernelConstants {
	public static final String META = "META";
	public static final String TILDE = "#";

	public enum EnumInstanceLimiter {
		NONE, SYSTEM, ORGANIZATION;
	}

	public enum EnumCRUDOperations {
		READ, FETCH, CREATE, UPDATE, SOFT_DELETE, REMOVE;
	}
	
	public enum EnumComponentOperation {
		CREATE, REMOVE;
	}

	public enum EnumRequestType {
		META, CLASS, INSTANCE, BOTH, UICLASS, UNIQUEVALUE;
	}

	public enum EnumDocStatusType {
		ACTIVE, INACTIVE;
	}

	public enum EnumTxnRequestStatus {
		REGISTERED, DOC_LOCKED, DOC_LOCKED_FAILED, PROCESSING, 
			PROCESS_SUCCESS, PROCESS_FAILED, TXN_COMMIT_SUCCESS_SUCCESS, TXN_COMMIT_SUCCESS_FAILED,
				TXN_COMMIT_FAILED_SUCCESS, TXN_COMMIT_FAILED_FAILED;
	}
}
package com.alt.datacarrier.kernel.common;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;


/**
 * The class <code> KernelDynamicQueryReadRequest.java</code>
 *
 * @author sandeep.verma
 *
 */

public class KernelDynamicQueryReadRequest implements IRequestData {
	
	private static final long serialVersionUID = 3100840222549329229L;
	
    private List<ProjectionAttribute> projection;
	
	private String fromDbClass;
	
	private List<String> keyspace;
	
	private List<JoinDBClass> joinDBClass;
	
	private List<DBFilters> dbFilters;
	
	private List<OrderAttribute> orderBy;
	
	private List<String> filterJoinOperator;
	
	private int offset;
	
	private int limit;
	
	private boolean documentCountNeeded;
	
	private List<UnnestAttribute> unnest;
	
	private boolean casNeeded;

	public List<ProjectionAttribute> getProjection() {
		return projection;
	}

	public void setProjection(List<ProjectionAttribute> projection) {
		this.projection = projection;
	}

	public String getFromDbClass() {
		return fromDbClass;
	}

	public void setFromDbClass(String fromDbClass) {
		this.fromDbClass = fromDbClass;
	}

	public List<String> getKeyspace() {
		return keyspace;
	}
	
	public boolean getCasNeeded() {
		return casNeeded;
	}

	public void setCasNeeded(boolean casNeeded) {
		this.casNeeded = casNeeded;
	}

	public void setKeyspace(List<String> keyspace) {
		this.keyspace = keyspace;
	}

	public List<JoinDBClass> getJoinDBClass() {
		return joinDBClass;
	}

	public void setJoinDBClass(List<JoinDBClass> joinDBClass) {
		this.joinDBClass = joinDBClass;
	}

	public List<DBFilters> getDbFilters() {
		return dbFilters;
	}

	public void setDbFilters(List<DBFilters> dbFilters) {
		this.dbFilters = dbFilters;
	}

	public List<OrderAttribute> getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(List<OrderAttribute> orderBy) {
		this.orderBy = orderBy;
	}

	public List<String> getFilterJoinOperator() {
		return filterJoinOperator;
	}

	public void setFilterJoinOperator(List<String> filterJoinOperator) {
		this.filterJoinOperator = filterJoinOperator;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public boolean isDocumentCountNeeded() {
		return documentCountNeeded;
	}

	public void setDocumentCountNeeded(boolean documentCountNeeded) {
		this.documentCountNeeded = documentCountNeeded;
	}

	public List<UnnestAttribute> getUnnest() {
		return unnest;
	}

	public void setUnnest(List<UnnestAttribute> unnest) {
		this.unnest = unnest;
	}
}

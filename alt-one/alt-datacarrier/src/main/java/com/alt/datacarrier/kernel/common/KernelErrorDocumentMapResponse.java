package com.alt.datacarrier.kernel.common;

import java.util.Map;

public class KernelErrorDocumentMapResponse {
     
	private int code;
	
	private Map<String, KernelErrorClass> message;
	
	private String response;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Map<String, KernelErrorClass> getMessage() {
		return message;
	}

	public void setMessage(Map<String, KernelErrorClass> message) {
		this.message = message;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	
}

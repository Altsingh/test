package com.alt.datacarrier.kernel.common;

import com.alt.datacarrier.core.IRequestData;

public class KernelIDQueryReadRequest implements IRequestData {

	private static final long serialVersionUID = 4893896086210846471L;
	private String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
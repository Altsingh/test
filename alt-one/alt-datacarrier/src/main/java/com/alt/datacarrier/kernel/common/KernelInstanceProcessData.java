package com.alt.datacarrier.kernel.common;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class <code>KernelInstanceProcessData.java</code>
 *
 * @author  sumeet.mahajan
 *
 * @createdOn May 12, 2017
 */

public class KernelInstanceProcessData implements Serializable{
		
	private static final long serialVersionUID = -8176684425264566644L;

	@JsonProperty("instanceCode")
	private String instanceCode;
	
	@JsonProperty("classCode")
	private String classCode;
	
	@JsonProperty("status")
	private EnumDocStatusType status;
	
	@JsonProperty("type")
	private EnumRequestType type;
	
	@JsonProperty("attributes")
	private Map<String, KernelSingleAttributeValueObject> attributes;
	
	@JsonProperty("updationUuidList")
	private List<String> updationUuidList;

	@JsonProperty("cas")
	private String cas;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("createdDate")
	private Long createdDate;
	
	@JsonProperty("modifiedDate")
	private Long modifiedDate;
	
	@JsonProperty("createdBy")
	private String createdBy;
	
	@JsonProperty("modifiedBy")
	private String modifiedBy;
	
	@JsonProperty("readLock")
	private boolean readLock;

    public KernelInstanceProcessData() {
    }

    public KernelInstanceProcessData(Builder builder) {
		setInstanceCode(builder.instanceCode);
		setClassCode(builder.classCode);
		setStatus(builder.status);
		setType(builder.type);
		setAttributes(builder.attributes);
		setUpdationUuidList(builder.updationUuidList);
		setCas(builder.cas);
		setId(builder.id);
		setCreatedDate(builder.createdDate);
		setModifiedDate(builder.modifiedDate);
		setCreatedBy(builder.createdBy);
		setModifiedBy(builder.modifiedBy);
		setReadLock(builder.readLock);
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	public String getCas() {
		return cas;
	}

	public void setCas(String cas) {
		this.cas = cas;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public EnumRequestType getType() {
		return type;
	}

	public void setType(EnumRequestType type) {
		this.type = type;
	}

	public String getInstanceCode() {
		return instanceCode;
	}

	public void setInstanceCode(String instanceCode) {
		this.instanceCode = instanceCode;
	}

	public List<String> getUpdationUuidList() {
		return updationUuidList;
	}

	public void setUpdationUuidList(List<String> updationUuidList) {
		this.updationUuidList = updationUuidList;
	}

	public Long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Long createdDate) {
		this.createdDate = createdDate;
	}

	public Long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Map<String, KernelSingleAttributeValueObject> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, KernelSingleAttributeValueObject> attributes) {
		this.attributes = attributes;
	}
	
	
	public boolean getReadLock() {
		return readLock;
	}

	public void setReadLock(boolean readLock) {
		this.readLock = readLock;
	}

	@Override
	public String toString() {
		return "KernelInstanceProcessData [instanceCode=" + instanceCode + ", classCode=" + classCode + ", status="
				+ status + ", type=" + type + ", attributeValue=" + attributes +  ", readLock=" + readLock +"]";
	}


	public static final class Builder {
		private String instanceCode;
		private String classCode;
		private EnumDocStatusType status;
		private EnumRequestType type;
		private Map<String, KernelSingleAttributeValueObject> attributes;
		private List<String> updationUuidList;
		private String cas;
		private String id;
		private Long createdDate;
		private Long modifiedDate;
		private String createdBy;
		private String modifiedBy;
		private boolean readLock;

		public Builder() {
		}

		public Builder withInstanceCode(String val) {
			instanceCode = val;
			return this;
		}

		public Builder withClassCode(String val) {
			classCode = val;
			return this;
		}

		public Builder withStatus(EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withType(EnumRequestType val) {
			type = val;
			return this;
		}

		public Builder withAttributes(Map<String, KernelSingleAttributeValueObject> val) {
			attributes = val;
			return this;
		}

		public Builder withUpdationUuidList(List<String> val) {
			updationUuidList = val;
			return this;
		}

		public Builder withCas(String val) {
			cas = val;
			return this;
		}

		public Builder withId(String val) {
			id = val;
			return this;
		}

		public Builder withCreatedDate(Long val) {
			createdDate = val;
			return this;
		}

		public Builder withModifiedDate(Long val) {
			modifiedDate = val;
			return this;
		}

		public Builder withCreatedBy(String val) {
			createdBy = val;
			return this;
		}

		public Builder withModifiedBy(String val) {
			modifiedBy = val;
			return this;
		}

		public Builder withReadLock(boolean val) {
			readLock = val;
			return this;
		}

		public KernelInstanceProcessData build() {
			return new KernelInstanceProcessData(this);
		}
	}
}
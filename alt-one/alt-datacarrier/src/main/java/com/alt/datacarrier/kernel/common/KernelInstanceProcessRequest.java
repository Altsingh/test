package com.alt.datacarrier.kernel.common;


import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class <code>KernelInstanceProcessRequest.java</code>
 *
 * @author sumeet.mahajan
 * @createdOn May 12, 2017
 */
public class KernelInstanceProcessRequest implements IRequestData {

    private static final long serialVersionUID = 4924629466429L;

    @JsonProperty("action")
    private EnumCRUDOperations action;

    @JsonProperty("instanceData")
    private KernelInstanceProcessData instanceData;

    public EnumCRUDOperations getAction() {
        return action;
    }

    public void setAction(EnumCRUDOperations action) {
        this.action = action;
    }

    public KernelInstanceProcessData getInstanceData() {
        return instanceData;
    }

    public void setInstanceData(KernelInstanceProcessData instanceData) {
        this.instanceData = instanceData;
    }

    public KernelInstanceProcessRequest() {
    }

    public KernelInstanceProcessRequest(EnumCRUDOperations action, KernelInstanceProcessData instanceData) {
        this.action = action;
        this.instanceData = instanceData;
    }

    @Override
    public String toString() {
        return "KernelInstanceProcessRequest [action=" + action + ", instanceData=" + instanceData + "]";
    }

}
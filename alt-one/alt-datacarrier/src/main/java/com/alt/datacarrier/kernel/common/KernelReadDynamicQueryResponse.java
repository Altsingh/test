package com.alt.datacarrier.kernel.common;

import java.util.List;

import com.alt.datacarrier.core.IResponseData;

public class KernelReadDynamicQueryResponse implements IResponseData{

private static final long serialVersionUID = 7494664247605459146L;
private List<SingleDynamicQueryResponse> queryResponse;
	
	public List<SingleDynamicQueryResponse> getQueryResponse() {
		return queryResponse;
	}
	public void setQueryResponse(List<SingleDynamicQueryResponse> queryResponse) {
		this.queryResponse = queryResponse;
	}
}

package com.alt.datacarrier.kernel.common;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;

public class KernelReadQueryRequest implements IRequestData{
	
	private static final long serialVersionUID = 1591946602419608719L;

	private List<SelectAttribue> projection;
	
	private String fromDbClass;
	
	private List<String> keyspace;
	
	private List<JoinDBClass> joinDBClass;
	
	private List<JoinCondition> joinCondition;
	
	private List<OrderAttribute> orderBy;
	
	private int offset;
	
	private int limit;

	public KernelReadQueryRequest() {
	}

	

	public KernelReadQueryRequest(String fromDbClass, List<SelectAttribue> singletonList, List<String> keyspace) {
		this.fromDbClass = fromDbClass;
		this.projection = singletonList;
		this.keyspace = keyspace;
	}



	public List<SelectAttribue> getProjection() {
		return projection;
	}
	
	public void setProjection(List<SelectAttribue> projection) {
		this.projection = projection;
	}
	
	public List<String> getKeyspace() {
		return keyspace;
	}
	
	public void setKeyspace(List<String> keyspace) {
		this.keyspace = keyspace;
	}
	
	public int getOffset() {
		return offset;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public int getLimit() {
		return limit;
	}
	
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	public String getFromDbClass() {
		return fromDbClass;
	}
	
	public void setFromDbClass(String fromDbClass) {
		this.fromDbClass = fromDbClass;
	}
	
	public List<JoinDBClass> getJoinDBClass() {
		return joinDBClass;
	}
	
	public void setJoinDBClass(List<JoinDBClass> joinDBClass) {
		this.joinDBClass = joinDBClass;
	}
	
	public List<JoinCondition> getJoinCondition() {
		return joinCondition;
	}
	
	public void setJoinCondition(List<JoinCondition> joinCondition) {
		this.joinCondition = joinCondition;
	}
	
	public List<OrderAttribute> getOrderBy() {
		return orderBy;
	}
	
	public void setOrderBy(List<OrderAttribute> orderBy) {
		this.orderBy = orderBy;
	}
		
}

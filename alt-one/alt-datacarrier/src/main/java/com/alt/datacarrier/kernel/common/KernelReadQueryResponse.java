package com.alt.datacarrier.kernel.common;

import java.util.List;

import com.alt.datacarrier.core.IResponseData;

public class KernelReadQueryResponse implements IResponseData{

	private static final long serialVersionUID = -6880904980559854244L;

	private List<SingleQueryResponse> queryResponse;
	
	public List<SingleQueryResponse> getQueryResponse() {
		return queryResponse;
	}
	public void setQueryResponse(List<SingleQueryResponse> queryResponse) {
		this.queryResponse = queryResponse;
	}
	
}

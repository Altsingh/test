package com.alt.datacarrier.kernel.common;

import com.alt.datacarrier.core.Request;

/**
 * The class <code>KernelReadRequest.java</code>
 *
 * @author  rishi.gautam
 *
 * @createdOn   Mar 3, 2017
 */

public class KernelReadRequest extends Request<KernelSingleReadRequest>
{
    private static final long serialVersionUID = 5935937497394739L;
}
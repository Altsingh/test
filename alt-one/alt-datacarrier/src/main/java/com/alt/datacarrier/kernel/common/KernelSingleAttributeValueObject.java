package com.alt.datacarrier.kernel.common;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;


public class KernelSingleAttributeValueObject implements Serializable{
	
	private static final long serialVersionUID = -7192937946507052896L;

	@JsonProperty("value")
	private String value;

	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	public KernelSingleAttributeValueObject() {}

	public KernelSingleAttributeValueObject(String value) {
		this.value = value;
	}
}

package com.alt.datacarrier.kernel.common;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The class <code>KernelSingleReadRequest.java</code>
 *
 * @author rishi.gautam
 *
 * @createdOn Mar 3, 2017
 */

public class KernelSingleReadRequest implements IRequestData {
	private static final long serialVersionUID = 63286492648923L;

	@JsonProperty("docIdentifier")
	private String docIdentifier;

	@JsonIgnore
	@JsonProperty("type")
	private EnumRequestType type;

	@JsonIgnore
	@JsonProperty("limit")
	private int limit;

	@JsonIgnore
	@JsonProperty("offset")
	private int offset;

	@JsonIgnore
	@JsonProperty("depth")
	private int depth;
	
	@JsonIgnore
	@JsonProperty("documentCountNeeded")
	private boolean documentCountNeeded;

	public String getDocIdentifier() {
		return docIdentifier;
	}

	public void setDocIdentifier(String docIdentifier) {
		this.docIdentifier = docIdentifier;
	}

	public EnumRequestType getType() {
		return type;
	}

	public void setType(EnumRequestType type) {
		this.type = type;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public boolean isDocumentCountNeeded() {
		return documentCountNeeded;
	}

	public void setDocumentCountNeeded(boolean documentCountNeeded) {
		this.documentCountNeeded = documentCountNeeded;
	}
	
}

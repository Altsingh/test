package com.alt.datacarrier.kernel.common;

import com.alt.datacarrier.core.IRequestData;

public class KernelStringQueryReadRequest implements IRequestData {

	private static final long serialVersionUID = -7924720561275910837L;
	private String request;

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}
}

package com.alt.datacarrier.kernel.common;

import java.io.Serializable;

import com.alt.datacarrier.common.enumeration.EnumSortDirection;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OrderAttribute   implements Serializable{
	
	private static final long serialVersionUID = -6377456796610501237L;
	private EnumSortDirection sortDirection;
	private String dbClass;
	private String dbAttr;
	private boolean unnest;
	
	public EnumSortDirection getSortDirection() {
		return sortDirection;
	}
	public void setSortDirection(EnumSortDirection sortDirection) {
		this.sortDirection = sortDirection;
	}
	public String getDbClass() {
		return dbClass;
	}
	public void setDbClass(String dbClass) {
		this.dbClass = dbClass;
	}
	public String getDbAttr() {
		return dbAttr;
	}
	public void setDbAttr(String dbAttr) {
		this.dbAttr = dbAttr;
	}
	
	@JsonProperty("unnest")
	public boolean isUnnest() {
		return unnest;
	}
	
	@JsonProperty("unnest")
	public void setUnnest(boolean isUnnest) {
		this.unnest = isUnnest;
	}

}

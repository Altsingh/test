package com.alt.datacarrier.kernel.common;

import java.io.Serializable;
import java.util.List;

/**
 * The class <code> ProjectionAttribute.java</code>
 *
 * @author sandeep.verma
 *
 */
public class ProjectionAttribute   implements Serializable{

	private static final long serialVersionUID = -5326844055667482377L;
	private List<AttributesData> dbAttribute;
	private String classCode;
	
	public List<AttributesData> getDbAttribute() {
		return dbAttribute;
	}
	public void setDbAttribute(List<AttributesData> dbAttribute) {
		this.dbAttribute = dbAttribute;
	}
	public String getClassCode() {
		return classCode;
	}
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	
}

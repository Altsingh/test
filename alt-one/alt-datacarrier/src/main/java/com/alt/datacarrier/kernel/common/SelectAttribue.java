package com.alt.datacarrier.kernel.common;

import com.alt.datacarrier.common.enumeration.EnumDisplay;

public class SelectAttribue {
	
	private String dbattr;
	
	private String dbClass;
	
	private EnumDisplay display;
	
	private String delimiter;
	
	public SelectAttribue(String dbClass, String dbattr) {
		this.dbClass = dbClass;
		this.dbattr = dbattr;
	}

	public String getDbattr() {
		return dbattr;
	}
	
	public void setDbattr(String dbattr) {
		this.dbattr = dbattr;
	}
	
	public EnumDisplay getDisplay() {
		return display;
	}
	
	public void setDisplay(EnumDisplay display) {
		this.display = display;
	}
	
	public String getDelimiter() {
		return delimiter;
	}
	
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
	
	public String getDbClass() {
		return dbClass;
	}
	
	public void setDbClass(String dbClass) {
		this.dbClass = dbClass;
	}
	
}

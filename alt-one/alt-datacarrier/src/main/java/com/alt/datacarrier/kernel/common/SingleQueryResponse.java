package com.alt.datacarrier.kernel.common;

import java.util.List;
import java.util.Map;

public class SingleQueryResponse {
	
	private int code;
	
	private String message;
	
	private List<Map<String, Object>> response;
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<Map<String, Object>> getResponse() {
		return response;
	}
	
	public void setResponse(List<Map<String, Object>> response) {
		this.response = response;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}

}

package com.alt.datacarrier.kernel.common;

import java.io.Serializable;

/**
 * The class <code> UnnestAttribute.java</code>
 *
 * @author sandeep.verma
 *
 */

public class UnnestAttribute implements Serializable{

	private static final long serialVersionUID = 1479963745782687780L;
	private String unNestArrayName;
	private String unNestAlias;
	
	public String getUnNestArrayName() {
		return unNestArrayName;
	}
	public void setUnNestArrayName(String unNestArrayName) {
		this.unNestArrayName = unNestArrayName;
	}
	public String getUnNestAlias() {
		return unNestAlias;
	}
	public void setUnNestAlias(String unNestAlias) {
		this.unNestAlias = unNestAlias;
	}	
}

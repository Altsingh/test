package com.alt.datacarrier.kernel.db.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.alt.datacarrier.kernel.uiclass.*;

public class AltForm implements IRequestData {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5385285366918879442L;

	private EnumDocStatusType status;

	private EnumFormType formType;

	private EnumRequestType type;

	private String orgCode;

	private Integer orgId;

	private Integer metaClassId;

	private String uiClassName;

	private String uiClassCode;

	private boolean customComponentAllowed;

	private String moduleCode;

	private String description;

	private List<AltAbstractComponent> componentList;

	private String id;

	private String version;

	private EnumFormState state;

	private Long createdDate;

	private Long modifiedDate;

	private String createdBy;

	private String modifiedBy;

	private String formSecurityId;

	private Integer appid;

	private Integer parentFormId;
	
	private Boolean taskCodeRequired;

	public AltForm() {
	}

	private AltForm(Builder builder) {
		setStatus(builder.status);
		setFormType(builder.formType);
		setType(builder.type);
		setOrgCode(builder.orgCode);
		setOrgId(builder.orgId);
		setMetaClassId(builder.metaClassId);
		setUiClassName(builder.uiClassName);
		setUiClassCode(builder.uiClassCode);
		setCustomComponentAllowed(builder.customComponentAllowed);
		setModuleCode(builder.moduleCode);
		setDescription(builder.description);
		setComponentList(builder.componentList);
		setId(builder.id);
		setVersion(builder.version);
		setState(builder.state);
		setCreatedDate(builder.createdDate);
		setModifiedDate(builder.modifiedDate);
		setCreatedBy(builder.createdBy);
		setModifiedBy(builder.modifiedBy);
		setParentFormId(builder.parentFormId);
		setTaskCodeRequired(builder.taskCodeRequired);
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	public EnumFormType getFormType() {
		return formType;
	}

	public void setFormType(EnumFormType formType) {
		this.formType = formType;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getUiClassName() {
		return uiClassName;
	}

	public void setUiClassName(String uiClassName) {
		this.uiClassName = uiClassName;
	}

	public String getUiClassCode() {
		return uiClassCode;
	}

	public void setUiClassCode(String uiClassCode) {
		this.uiClassCode = uiClassCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public EnumFormState getState() {
		return state;
	}

	public void setState(EnumFormState state) {
		this.state = state;
	}

	public Long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Long createdDate) {
		this.createdDate = createdDate;
	}

	public Long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public List<AltAbstractComponent> getComponentList() {
		return componentList;
	}

	public List<AltAbstractComponent> getFlatComponentList() {
		List<AltAbstractComponent> flatComponentList = new ArrayList<>();
		flatComponentList = componentList.stream().filter(c->c.getControlType().equals(EnumHTMLControl.PANEL))
							.flatMap(c->((AltPanelComponent)c).getComponentList().stream()).collect(Collectors.toList());
		flatComponentList.addAll(componentList.stream().filter(c->!c.getControlType().equals(EnumHTMLControl.PANEL))
												.collect(Collectors.toList()));
		return flatComponentList;
	}



	public void setComponentList(List<AltAbstractComponent> componentList) {
		this.componentList = componentList;
	}

	public boolean isCustomComponentAllowed() {
		return customComponentAllowed;
	}

	public void setCustomComponentAllowed(boolean customComponentAllowed) {
		this.customComponentAllowed = customComponentAllowed;
	}

	public EnumRequestType getType() {
		return type;
	}

	public void setType(EnumRequestType type) {
		this.type = type;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public void setMetaClassId(Integer metaClassId) {
		this.metaClassId = metaClassId;
	}

	public Integer getMetaClassId() {
		return this.metaClassId;
	}

	public Boolean getTaskCodeRequired() {
		return taskCodeRequired;
	}

	public void setTaskCodeRequired(Boolean taskCodeRequired) {
		this.taskCodeRequired = taskCodeRequired;
	}

	public static final class Builder {
		private Boolean taskCodeRequired;
		private EnumDocStatusType status;
		private EnumFormType formType;
		private EnumRequestType type;
		private String orgCode;
		private Integer orgId;
		private Integer metaClassId;
		private String uiClassName;
		private String uiClassCode;
		private boolean customComponentAllowed;
		private String moduleCode;
		private String description;
		private List<AltAbstractComponent> componentList;
		private String id;
		private String version;
		private EnumFormState state;
		private Long createdDate;
		private Long modifiedDate;
		private String createdBy;
		private String modifiedBy;
		private Integer parentFormId;

		public Builder() {
		}

		public Builder withTaskCodeRequired(Boolean val) {
			taskCodeRequired = val;
			return this;
		}
		
		public Builder withStatus(EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withFormType(EnumFormType val) {
			formType = val;
			return this;
		}

		public Builder withType(EnumRequestType val) {
			type = val;
			return this;
		}

		public Builder withOrgCode(String val) {
			orgCode = val;
			return this;
		}

		public Builder withOrgId(String val) {
			this.orgId = Integer.parseInt(val);
			return this;
		}

		public Builder withMetaClassId(Integer val) {
			this.metaClassId = val;
			return this;
		}

		public Builder withUiClassName(String val) {
			uiClassName = val;
			return this;
		}

		public Builder withUiClassCode(String val) {
			uiClassCode = val;
			return this;
		}

		public Builder withCustomComponentAllowed(boolean val) {
			customComponentAllowed = val;
			return this;
		}

		public Builder withModuleCode(String val) {
			moduleCode = val;
			return this;
		}

		public Builder withDescription(String val) {
			description = val;
			return this;
		}

		public Builder withComponentList(List<AltAbstractComponent> val) {
			componentList = val;
			return this;
		}

		public Builder withId(String val) {
			id = val;
			return this;
		}

		public Builder withVersion(String val) {
			version = val;
			return this;
		}

		public Builder withState(EnumFormState val) {
			state = val;
			return this;
		}

		public Builder withCreatedDate(Long val) {
			createdDate = val;
			return this;
		}

		public Builder withModifiedDate(Long val) {
			modifiedDate = val;
			return this;
		}

		public Builder withCreatedBy(String val) {
			createdBy = val;
			return this;
		}

		public Builder withModifiedBy(String val) {
			modifiedBy = val;
			return this;
		}

		public Builder withParentFormId(Integer val) {
			parentFormId = val;
			return this;
		}

		public AltForm build() {
			return new AltForm(this);
		}

	}

	public boolean validateFormFieldClientIDs(AltForm form1) {
		boolean valid = true;
		for (AltAbstractComponent field1 : form1.componentList) {
			boolean flag = false;
			for (AltAbstractComponent field : this.componentList)
				if (field.getClientId() != null && field1.getClientId() != null
						&& field1.getClientId().equalsIgnoreCase(field.getClientId()))
					flag = true;
			if (!flag)
				valid = false;
		}
		return valid;
	}

	public boolean validateDuplicateClientIDs() {
		boolean valid = true;
		Set<String> clientIDs = new HashSet<>();
		for (AltAbstractComponent field1 : this.componentList) {
			if (clientIDs.contains(field1.getClientId()))
				valid = false;
			clientIDs.add(field1.getClientId());
		}
		return valid;
	}

	public void setValuesInComponents(AltForm formRequest) {
		for (AltAbstractComponent field : this.componentList) {
			for (AltAbstractComponent field1 : formRequest.componentList) {
				if (field.getClientId() != null && field1.getClientId() != null
						&& field1.getClientId().equalsIgnoreCase(field.getClientId()))
					((AltAbstractBaseComponent) field).setValue(((AltAbstractBaseComponent) field1).getValue());
			}
		}
	}

	public String getFormSecurityId() {
		return formSecurityId;
	}

	public void setFormSecurityId(String formSecurityId) {
		this.formSecurityId = formSecurityId;
	}

	public Integer getAppid() {
		return appid;
	}

	public void setAppid(Integer appid) {
		this.appid = appid;
	}

	public Integer getParentFormId() {
		return parentFormId;
	}

	public void setParentFormId(Integer parentFormId) {
		this.parentFormId = parentFormId;
	}

}

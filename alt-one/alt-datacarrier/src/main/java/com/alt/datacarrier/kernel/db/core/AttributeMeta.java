package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;

import com.alt.datacarrier.common.enumeration.EnumDataType;
import com.alt.datacarrier.core.AttributeValidator;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AttributeMeta implements Serializable {

	private static final long serialVersionUID = -5352550890847305034L;

	private String attributeCode;

	private String attributeId;

	private String attributeName;

	private EnumDataType dataType; // REFERENCE, DATE, STRING, DOUBLE, INTEGER etc

	private Integer referenceClassId; // When dataType is REFERENCE

	private String referenceClass;

	private String referenceAttr;

	private EnumDocStatusType status;

	private Boolean placeholderType;
	
	private Integer orgId;

	private String defaultValue;

	private AttributeValidator validator;
	
	private Boolean unique;
	
	private Boolean mandatory;	
	
	private Boolean searchable;
	
	private Boolean indexable;
	
	private Boolean displayUnit;
	
	private Integer displaySequence;
	
	private ClassMeta refClassMeta;

	private Integer dataLakeAttributeId;

	private AttributeMeta(Builder builder) {
		setAttributeCode(builder.attributeCode);
		setAttributeId(builder.attributeId);
		setAttributeName(builder.attributeName);
		setDataType(builder.dataType);
		setReferenceClassId(builder.referenceClassId);
		setStatus(builder.status);
		setPlaceholderType(builder.placeholderType);
		setOrgId(builder.orgId);
		setDefaultValue(builder.defaultValue);
		setValidator(builder.validator);
		setUnique(builder.unique);
		setMandatory(builder.mandatory);
		setSearchable(builder.searchable);
		setIndexable(builder.indexable);
		setDisplayUnit(builder.displayUnit);
		setDisplaySequence(builder.displaySequence);
		setRefClassMeta(builder.refClassMeta);
	}

	public String getReferenceClass() {
		return referenceClass;
	}

	public void setReferenceClass(String referenceClass) {
		this.referenceClass = referenceClass;
	}

	public String getReferenceAttr() {
		return referenceAttr;
	}

	public void setReferenceAttr(String referenceAttr) {
		this.referenceAttr = referenceAttr;
	}

	@JsonProperty("attributeId")
	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	@JsonProperty("attributeCode")
	public String getAttributeCode() {
		return attributeCode;
	}

	@JsonProperty("attributeCode")
	public void setAttributeCode(String attributeCode) {
		this.attributeCode = attributeCode;
	}

	@JsonProperty("attributeName")
	public String getAttributeName() {
		return attributeName;
	}

	@JsonProperty("attributeName")
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	@JsonProperty("referenceClassId")
    public Integer getReferenceClassId() {
		return referenceClassId;
	}

	@JsonProperty("referenceClassId")
	public void setReferenceClassId(Integer referenceClassId) {
		this.referenceClassId = referenceClassId;
	}

	@JsonProperty("placeholderType")
	public Boolean getPlaceholderType() {
		return placeholderType;
	}
    
    @JsonProperty("placeholderType")
	public void setPlaceholderType(Boolean placeholderType) {
		this.placeholderType = placeholderType;
	}
    
    //@JsonProperty("orgId")
    public void setOrgId(Integer orgId) {
    	this.orgId = orgId;
    }
    
    @JsonProperty("orgId")
    public Integer getOrgId() {
    	return orgId;
    }
    
    @JsonProperty("defaultValue")
	public String getDefaultValue() {
		return defaultValue;
	}
    
    @JsonProperty("defaultValue")
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

    @JsonProperty("validator")
	public AttributeValidator getValidator() {
		return validator;
	}
    
    @JsonProperty("validator")
	public void setValidator(AttributeValidator validator) {
		this.validator = validator;
	}
    
    @JsonProperty("unique")
	public Boolean getUnique() {
		return unique;
	}
    
    @JsonProperty("unique")
	public void setUnique(Boolean unique) {
		this.unique = unique;
	}
    
    @JsonProperty("mandatory")
	public Boolean getMandatory() {
		return mandatory;
	}
    
    @JsonProperty("mandatory")
	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

    @JsonProperty("dataType")
	public EnumDataType getDataType() {
		return dataType;
	}

    @JsonProperty("dataType")
	public void setDataType(EnumDataType dataType) {
		this.dataType = dataType;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	public Boolean getSearchable() {
		return searchable;
	}

	public void setSearchable(Boolean searchable) {
		this.searchable = searchable;
	}

	public Boolean getIndexable() {
		return indexable;
	}

	public void setIndexable(Boolean indexable) {
		this.indexable = indexable;
	}

	public Boolean getDisplayUnit() {
		return displayUnit;
	}

	public void setDisplayUnit(Boolean displayUnit) {
		this.displayUnit = displayUnit;
	}

	public Integer getDisplaySequence() {
		return displaySequence;
	}

	public void setDisplaySequence(Integer displaySequence) {
		this.displaySequence = displaySequence;
	}

	public Integer getDataLakeAttributeId() {
		return dataLakeAttributeId;
	}

	public void setDataLakeAttributeId(Integer dataLakeAttributeId) {
		this.dataLakeAttributeId = dataLakeAttributeId;
	}

	@JsonProperty("refClassMeta")
	public ClassMeta getRefClassMeta() {
		return refClassMeta;
	}

    @JsonProperty("refClassMeta")
	public void setRefClassMeta(ClassMeta refClassMeta) {
		this.refClassMeta = refClassMeta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attributeName == null) ? 0 : attributeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		AttributeMeta other = (AttributeMeta) obj;
		if (attributeName == null) {
			if (other.attributeName != null){
				return false;
			}
		} else if (!attributeName.equals(other.attributeName)){
			return false;
		}
		return true;
	}

	public AttributeMeta() {
	}


	public static final class Builder {
		private String attributeCode;
		private String attributeId;
		private String attributeName;
		private EnumDataType dataType;
		private Integer referenceClassId;
		private EnumDocStatusType status;
		private Boolean placeholderType;
		private Integer orgId;
		private String defaultValue;
		private AttributeValidator validator;
		private Boolean unique;
		private Boolean mandatory;
		private Boolean searchable;
		private Boolean indexable;
		private Boolean displayUnit;
		private Integer displaySequence;
		private ClassMeta refClassMeta;

		public Builder() {
		}

		public Builder withAttributeCode(String val) {
			attributeCode = val;
			return this;
		}

		public Builder withAttributeId(String val) {
			attributeId = val;
			return this;
		}

		public Builder withAttributeName(String val) {
			attributeName = val;
			return this;
		}

		public Builder withDataType(EnumDataType val) {
			dataType = val;
			return this;
		}

		public Builder withReferenceClassId(Integer val) {
			referenceClassId = val;
			return this;
		}

		public Builder withStatus(EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withPlaceholderType(Boolean val) {
			placeholderType = val;
			return this;
		}

		public Builder withOrgId(Integer val) {
			orgId = val;
			return this;
		}
		
		public Builder withDefaultValue(String val) {
			defaultValue = val;
			return this;
		}

		public Builder withValidator(AttributeValidator val) {
			validator = val;
			return this;
		}

		public Builder withUnique(Boolean val) {
			unique = val;
			return this;
		}

		public Builder withMandatory(Boolean val) {
			mandatory = val;
			return this;
		}

		public Builder withSearchable(Boolean val) {
			searchable = val;
			return this;
		}

		public Builder withIndexable(Boolean val) {
			indexable = val;
			return this;
		}

		public Builder withDisplayUnit(Boolean val) {
			displayUnit = val;
			return this;
		}

		public Builder withDisplaySequence(Integer val) {
			displaySequence = val;
			return this;
		}

		public Builder withRefClassMeta(ClassMeta val) {
			refClassMeta = val;
			return this;
		}

		public AttributeMeta build() {
			return new AttributeMeta(this);
		}
	}
}
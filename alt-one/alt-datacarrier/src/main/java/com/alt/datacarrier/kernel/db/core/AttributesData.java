package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;

public class AttributesData implements Serializable{

	private static final long serialVersionUID = 5933915625056100396L;

	private boolean jsonbType;
	
	private String attribute;
	
	private String jsonName;
	
	private String jsonbAttribute;
	
	private String as;	
	
    public AttributesData(){
		
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public boolean isJsonbType() {
		return jsonbType;
	}

	public void setJsonbType(boolean jsonbType) {
		this.jsonbType = jsonbType;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public String getJsonName() {
		return jsonName;
	}

	public void setJsonName(String jsonName) {
		this.jsonName = jsonName;
	}

	public String getJsonbAttribute() {
		return jsonbAttribute;
	}

	public void setJsonbAttribute(String jsonbAttribute) {
		this.jsonbAttribute = jsonbAttribute;
	}

	public String getAs() {
		return as;
	}

	public void setAs(String as) {
		this.as = as;
	}

	private AttributesData(Builder builder) {
		jsonbType = builder.jsonbType;
		attribute = builder.attribute;
		jsonName = builder.jsonName;
		jsonbAttribute = builder.jsonbAttribute;
		as = builder.as;
	}


	public static final class Builder {
		private boolean jsonbType;
		private String attribute;
		private String jsonName;
		private String jsonbAttribute;
		private String as;

		public Builder() {
		}

		public Builder withJsonbType(boolean val) {
			jsonbType = val;
			return this;
		}

		public Builder withAttribute(String val) {
			attribute = val;
			return this;
		}

		public Builder withJsonName(String val) {
			jsonName = val;
			return this;
		}

		public Builder withJsonbAttribute(String val) {
			jsonbAttribute = val;
			return this;
		}

		public Builder withAs(String val) {
			as = val;
			return this;
		}

		public AttributesData build() {
			return new AttributesData(this);
		}
	}
}

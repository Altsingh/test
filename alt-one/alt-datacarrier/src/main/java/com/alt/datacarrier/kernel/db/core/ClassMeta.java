package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.alt.datacarrier.core.Metas;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumInstanceLimiter;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClassMeta implements Metas, Serializable{

	private static final long serialVersionUID = 6021235372805837921L;

	private String name;
	
	private EnumRequestType type;
	
	private EnumDocStatusType status;
	
	private Boolean locationSpecific;
	
	private Boolean systemType;
	
	private Boolean parameterType;
	
	private String classCode;
	
	private List<String> packageCodes;
	
	private Boolean customAttributeAllowed;
	
	private List<AttributeMeta> attributes;
	
	private EnumInstanceLimiter instanceLimiter;
	
	private String instancePrefix;
	
	private List<Map<String,List<String>>> referredBy;

	private List<String> updationUuidList;
	
	private String cas;
	
	private String id;
	
	private Long createdDate;
	
	private Long modifiedDate;
	
	private String createdBy;
	
	private String modifiedBy;
	
	private boolean readLock;
	
	private Boolean taskCodeRequired;
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("locationSpecific")
	public Boolean getLocationSpecific() {
		return locationSpecific;
	}
	@JsonProperty("locationSpecific")
	public void setLocationSpecific(Boolean locationSpecific) {
		this.locationSpecific = locationSpecific;
	}
	@JsonProperty("systemType")
	public Boolean getSystemType() {
		return systemType;
	}
	@JsonProperty("systemType")
	public void setSystemType(Boolean systemType) {
		this.systemType = systemType;
	}
	@JsonProperty("classCode")
	public String getClassCode() {
		return classCode;
	}
	@JsonProperty("classCode")
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	
	@JsonProperty("packageCodes")
	public List<String> getPackageCodes() {
		return packageCodes;
	}

	@JsonProperty("packageCodes")
	public void setPackageCodes(List<String> packageCodes) {
		this.packageCodes = packageCodes;
	}
	@JsonProperty("customAttributeAllowed")
	public Boolean getCustomAttributeAllowed() {
		return customAttributeAllowed;
	}
	@JsonProperty("customAttributeAllowed")
	public void setCustomAttributeAllowed(Boolean customAttributeAllowed) {
		this.customAttributeAllowed = customAttributeAllowed;
	}
	@JsonProperty("attributes")
	public List<AttributeMeta> getAttributes() {
		return attributes;
	}
	@JsonProperty("attributes")
	public void setAttributes(List<AttributeMeta> attributes) {
		this.attributes = attributes;
	}
	@JsonProperty("instanceLimiter")
	public EnumInstanceLimiter getInstanceLimiter() {
		return instanceLimiter;
	}
	@JsonProperty("instanceLimiter")
	public void setInstanceLimiter(EnumInstanceLimiter instanceLimiter) {
		this.instanceLimiter = instanceLimiter;
	}
	@JsonProperty("instancePrefix")
	public String getInstancePrefix() {
		return instancePrefix;
	}
	@JsonProperty("instancePrefix")
	public void setInstancePrefix(String instancePrefix) {
		this.instancePrefix = instancePrefix;
	}

	@JsonProperty("referredBy")
	public List<Map<String, List<String>>> getReferredBy() {
		return referredBy;
	}

	@JsonProperty("referredBy")
	public void setReferredBy(List<Map<String, List<String>>> referredBy) {
		this.referredBy = referredBy;
	}

	@JsonProperty("cas")
	public String getCas() {
		return cas;
	}

	@JsonProperty("cas")
	public void setCas(String cas) {
		this.cas = cas;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("updationUuidList")
	public List<String> getUpdationUuidList() {
		return updationUuidList;
	}
	
	@JsonProperty("updationUuidList")
	public void setUpdationUuidList(List<String> updationUuidList) {
		this.updationUuidList = updationUuidList;
	}

	@JsonProperty("createdDate")
	public Long getCreatedDate() {
		return createdDate;
	}
	@JsonProperty("createdDate")
	public void setCreatedDate(Long createdDate) {
		this.createdDate = createdDate;
	}
	@JsonProperty("modifiedDate")
	public Long getModifiedDate() {
		return modifiedDate;
	}
	@JsonProperty("modifiedDate")
	public void setModifiedDate(Long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@JsonProperty("createdBy")
	public String getCreatedBy() {
		return createdBy;
	}
	@JsonProperty("createdBy")
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@JsonProperty("modifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	@JsonProperty("modifiedBy")
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@JsonProperty("type")
	public EnumRequestType getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(EnumRequestType type) {
		this.type = type;
	}

	@JsonProperty("status")
	public EnumDocStatusType getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	@JsonProperty("taskCodeRequired")
	public Boolean getTaskCodeRequired() {
		return taskCodeRequired;
	}

	@JsonProperty("taskCodeRequired")
	public void setTaskCodeRequired(Boolean taskCodeRequired) {
		this.taskCodeRequired = taskCodeRequired;
	}

	@JsonProperty("parameterType")
	public Boolean getParameterType() {
		return parameterType;
	}

	@JsonProperty("parameterType")
	public void setParameterType(Boolean parameterType) {
		this.parameterType = parameterType;
	}

	@JsonProperty("readLock")
	public boolean isReadLock() {
		return readLock;
	}

	@JsonProperty("readLock")
	public void setReadLock(boolean readLock) {
		this.readLock = readLock;
	}
}

package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;
import java.util.List;

public class DbFilters  implements Serializable {
	
	public final static String CONDITION_JSONB_ARRAY_LENGTH = "jsonb_array_length";
	
	public final static String CONDITION_JSONB_COMPLEX_ARRAY_CONTAINS_SINGLE = "@>";
	
	public final static String CONDITION_JSON_STRING_ARRAY_CONTAINS_SINGLE = "?";
	
	private static final long serialVersionUID = -8733139036271303807L;

	private String className;
	
	private String attribute;
	
	private String condition;
	
	private String value;
	
	private boolean jsonbType;
	
	private String jsonName;
	
	private List<String> jsonbAttribute;

    public DbFilters() {
    }

	private DbFilters(Builder builder) {
		setClassName(builder.className);
		setAttribute(builder.attribute);
		setCondition(builder.condition);
		setValue(builder.value);
		setJsonbType(builder.jsonbType);
		setJsonName(builder.jsonName);
		setJsonbAttribute(builder.jsonbAttribute);
	}

	public boolean isJsonbType() {
		return jsonbType;
	}
	
	public void setJsonbType(boolean jsonbType) {
		this.jsonbType = jsonbType;
	}
	
	public String getJsonName() {
		return jsonName;
	}
	
	public void setJsonName(String jsonName) {
		this.jsonName = jsonName;
	}
	
	public List<String> getJsonbAttribute() {
		return jsonbAttribute;
	}

	public void setJsonbAttribute(List<String> jsonbAttribute) {
		this.jsonbAttribute = jsonbAttribute;
	}

	public String getClassName() {
		return className;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}
	
	public String getAttribute() {
		return attribute;
	}
	
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	
	public String getCondition() {
		return condition;
	}
	
	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	public static final class Builder {
		private String className;
		private String attribute;
		private String condition;
		private String value;
		private boolean jsonbType;
		private String jsonName;
		private List<String> jsonbAttribute;

		public Builder() {
		}

		public Builder withClassName(String val) {
			className = val;
			return this;
		}

		public Builder withAttribute(String val) {
			attribute = val;
			return this;
		}

		public Builder withCondition(String val) {
			condition = val;
			return this;
		}

		public Builder withValue(String val) {
			value = val;
			return this;
		}

		public Builder withJsonbType(boolean val) {
			jsonbType = val;
			return this;
		}

		public Builder withJsonName(String val) {
			jsonName = val;
			return this;
		}

		public Builder withJsonbAttribute(List<String> val) {
			jsonbAttribute = val;
			return this;
		}

		public DbFilters build() {
			return new DbFilters(this);
		}
	}
}

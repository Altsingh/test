package com.alt.datacarrier.kernel.db.core;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.core.Protocol;

public class DynamicReadRequest implements IRequestData {
	
	private static final long serialVersionUID = -8592976720709607617L;

	private List<ProjectionAttribute> projections;
	
	private String fromDbClass;
		
	private List<JoinDbClass> joinDbClasses;
	
	private List<DbFilters> dbFilters;
	
	private List<OrderAttribute> orderBy;
		
	private Integer offset;
	
	private Integer limit;
	
	private Boolean documentCountNeeded;

	private Protocol protocol;

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public List<ProjectionAttribute> getProjections() {
		return projections;
	}

	public void setProjections(List<ProjectionAttribute> projections) {
		this.projections = projections;
	}

	public String getFromDbClass() {
		return fromDbClass;
	}

	public void setFromDbClass(String fromDbClass) {
		this.fromDbClass = fromDbClass;
	}

	public List<JoinDbClass> getJoinDbClasses() {
		return joinDbClasses;
	}

	public void setJoinDbClasses(List<JoinDbClass> joinDbClasses) {
		this.joinDbClasses = joinDbClasses;
	}

	public List<DbFilters> getDbFilters() {
		return dbFilters;
	}

	public void setDbFilters(List<DbFilters> dbFilters) {
		this.dbFilters = dbFilters;
	}

	public List<OrderAttribute> getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(List<OrderAttribute> orderBy) {
		this.orderBy = orderBy;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Boolean isDocumentCountNeeded() {
		return documentCountNeeded;
	}

	public void setDocumentCountNeeded(Boolean documentCountNeeded) {
		this.documentCountNeeded = documentCountNeeded;
	}

}

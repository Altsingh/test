package com.alt.datacarrier.kernel.db.core;

import java.util.List;

public class DynamicReadResponse {

	private int code;
	
	private String message;
	
	private List<Object[]> response;
	
	private int documentCount;
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public  List<Object[]> getResponse() {
		return response;
	}
	
	public void setResponse(List<Object[]> response) {
		this.response = response;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}

	public int getDocumentCount() {
		return documentCount;
	}

	public void setDocumentCount(int documentCount) {
		this.documentCount = documentCount;
	}
	
}

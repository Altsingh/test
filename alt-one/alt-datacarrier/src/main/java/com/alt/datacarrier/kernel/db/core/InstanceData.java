package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;
import java.util.Map;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;

public class InstanceData implements Serializable {
	
	private static final long serialVersionUID = -1098595131180400637L;

	private Integer instanceId;
	
	private String org;
	
	private String userId;
	
	private Integer appId;
	
	private Integer formId;
	
	private String formName;

	private String className;
	
	private Integer classId; 

	private Map<String, Object> attributes;

	private EnumDocStatusType status;

	private Long createdDate; 
	
	private Long modifiedDate;
	
	private String createdBy;
	
	private String modifiedBy;

	public InstanceData() {
	}

	public InstanceData(Integer instanceId, String className, String createdBy, Long createdDate, String org, EnumDocStatusType status ) {
		this.instanceId = instanceId;
		this.org = org;
		this.className = className;
		this.status = status;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getAppId() {
		return appId;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public Integer getFormId() {
		return formId;
	}

	public void setFormId(Integer formId) {
		this.formId = formId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	public Long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Long createdDate) {
		this.createdDate = createdDate;
	}

	public Long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Integer getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(Integer instanceId) {
		this.instanceId = instanceId;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
}

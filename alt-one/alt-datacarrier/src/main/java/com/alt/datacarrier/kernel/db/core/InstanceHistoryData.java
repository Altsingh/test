package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;
import java.util.Map;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;

public class InstanceHistoryData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6467822238663353017L;

	private Protocol protocol;
	private Long instancecreateddate;
	private Long instancemodifieddate;
	private String instancecreatedby;
	private String instancemodifiedby;
	private Long modifiedDate;
	private Long createdDate;
	private EnumDocStatusType instancestatus;
	private EnumDocStatusType status;
	private Map<String, Object> attributes;
	private String userid;
	private String formName;
	private Integer formid;
	private Integer appid;
	private Integer classData;
	private Integer organizationid;
	private String className;
	private Integer instanceid;
	private Integer id;

	public InstanceHistoryData() {

	}

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public Long getInstancecreateddate() {
		return instancecreateddate;
	}

	public void setInstancecreateddate(Long instancecreateddate) {
		this.instancecreateddate = instancecreateddate;
	}

	public Long getInstancemodifieddate() {
		return instancemodifieddate;
	}

	public void setInstancemodifieddate(Long instancemodifieddate) {
		this.instancemodifieddate = instancemodifieddate;
	}

	public String getInstancecreatedby() {
		return instancecreatedby;
	}

	public void setInstancecreatedby(String instancecreatedby) {
		this.instancecreatedby = instancecreatedby;
	}

	public String getInstancemodifiedby() {
		return instancemodifiedby;
	}

	public void setInstancemodifiedby(String instancemodifiedby) {
		this.instancemodifiedby = instancemodifiedby;
	}

	public Long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Long createdDate) {
		this.createdDate = createdDate;
	}

	public EnumDocStatusType getInstancestatus() {
		return instancestatus;
	}

	public void setInstancestatus(EnumDocStatusType instancestatus) {
		this.instancestatus = instancestatus;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public Integer getFormid() {
		return formid;
	}

	public void setFormid(Integer formid) {
		this.formid = formid;
	}

	public Integer getAppid() {
		return appid;
	}

	public void setAppid(Integer appid) {
		this.appid = appid;
	}

	public Integer getClassData() {
		return classData;
	}

	public void setClassData(Integer classData) {
		this.classData = classData;
	}

	public Integer getOrganizationid() {
		return organizationid;
	}

	public void setOrganizationid(Integer organizationid) {
		this.organizationid = organizationid;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getInstanceid() {
		return instanceid;
	}

	public void setInstanceid(Integer instanceid) {
		this.instanceid = instanceid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}

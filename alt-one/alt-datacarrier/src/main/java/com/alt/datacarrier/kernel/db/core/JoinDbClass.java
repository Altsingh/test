package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;

import com.alt.datacarrier.common.enumeration.EnumJoinType;

public class JoinDbClass implements Serializable{
	
	private static final long serialVersionUID = 606241334732616820L;

	private EnumJoinType type;
	
	private String dbClass;
	
	private String firstConditionFirstColumnTable;
	
	private String firstConditionFirstColumn;
	
	private String firstConditionSeconfColumnTable;
	
	private String firstConditionSecondColumn;
	
	private String secondConditionFirstColumnTable;
	
	private String secondConditionFirstColumn;
	
	private String secondConditionSecondColumnTable;
	
	private String secondConditionSecondColumn;
	
	public String getFirstConditionFirstColumnTable() {
		return firstConditionFirstColumnTable;
	}

	public void setFirstConditionFirstColumnTable(String firstConditionFirstColumnTable) {
		this.firstConditionFirstColumnTable = firstConditionFirstColumnTable;
	}

	public String getFirstConditionFirstColumn() {
		return firstConditionFirstColumn;
	}

	public void setFirstConditionFirstColumn(String firstConditionFirstColumn) {
		this.firstConditionFirstColumn = firstConditionFirstColumn;
	}

	public String getFirstConditionSeconfColumnTable() {
		return firstConditionSeconfColumnTable;
	}

	public void setFirstConditionSeconfColumnTable(String firstConditionSeconfColumnTable) {
		this.firstConditionSeconfColumnTable = firstConditionSeconfColumnTable;
	}

	public String getFirstConditionSecondColumn() {
		return firstConditionSecondColumn;
	}

	public void setFirstConditionSecondColumn(String firstConditionSecondColumn) {
		this.firstConditionSecondColumn = firstConditionSecondColumn;
	}

	public String getSecondConditionFirstColumnTable() {
		return secondConditionFirstColumnTable;
	}

	public void setSecondConditionFirstColumnTable(String secondConditionFirstColumnTable) {
		this.secondConditionFirstColumnTable = secondConditionFirstColumnTable;
	}

	public String getSecondConditionFirstColumn() {
		return secondConditionFirstColumn;
	}

	public void setSecondConditionFirstColumn(String secondConditionFirstColumn) {
		this.secondConditionFirstColumn = secondConditionFirstColumn;
	}

	public String getSecondConditionSecondColumnTable() {
		return secondConditionSecondColumnTable;
	}

	public void setSecondConditionSecondColumnTable(String secondConditionSecondColumnTable) {
		this.secondConditionSecondColumnTable = secondConditionSecondColumnTable;
	}

	public String getSecondConditionSecondColumn() {
		return secondConditionSecondColumn;
	}

	public void setSecondConditionSecondColumn(String secondConditionSecondColumn) {
		this.secondConditionSecondColumn = secondConditionSecondColumn;
	}

	public void setDbTable(String dbClass) {
		this.dbClass = dbClass;
	}

	public String getDbClass() {
		return dbClass;
	}

	public EnumJoinType getType() {
		return type;
	}

	public void setType(EnumJoinType type) {
		this.type = type;
	}
	
}

package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;

import com.alt.datacarrier.common.enumeration.EnumSortDirection;

public class OrderAttribute implements Serializable{
	
	private static final long serialVersionUID = -6377456796610501237L;
	
	private EnumSortDirection sortDirection;

	private String dbClass;
	
	private String dbAttr;
	
	private boolean jsonbType;
	
	private String jsonName;
	
	private String jsonbAttribute;
	
	public EnumSortDirection getSortDirection() {
		return sortDirection;
	}
	
	public void setSortDirection(EnumSortDirection sortDirection) {
		this.sortDirection = sortDirection;
	}
	
	public String getDbClass() {
		return dbClass;
	}
	
	public void setDbClass(String dbClass) {
		this.dbClass = dbClass;
	}
	
	public String getDbAttr() {
		return dbAttr;
	}
	
	public void setDbAttr(String dbAttr) {
		this.dbAttr = dbAttr;
	}
	
	public boolean isJsonbType() {
		return jsonbType;
	}
	
	public void setJsonbType(boolean jsonbType) {
		this.jsonbType = jsonbType;
	}
	
	public String getJsonName() {
		return jsonName;
	}
	
	public void setJsonName(String jsonName) {
		this.jsonName = jsonName;
	}
	
	public String getJsonbAttribute() {
		return jsonbAttribute;
	}
	
	public void setJsonbAttribute(String jsonbAttribute) {
		this.jsonbAttribute = jsonbAttribute;
	}

}

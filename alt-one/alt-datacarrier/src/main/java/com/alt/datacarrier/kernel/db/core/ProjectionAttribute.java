package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;
import java.util.List;

public class ProjectionAttribute   implements Serializable{

	private static final long serialVersionUID = -5326844055667482377L;
	
	private List<AttributesData> dbAttributes;
	
	private String dbClass;

	public ProjectionAttribute() {
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public List<AttributesData> getDbAttributes() {
		return dbAttributes;
	}

	public void setDbAttributes(List<AttributesData> dbAttributes) {
		this.dbAttributes = dbAttributes;
	}

	public String getDbClass() {
		return dbClass;
	}

	public void setDbClass(String dbClass) {
		this.dbClass = dbClass;
	}

	private ProjectionAttribute(Builder builder) {
		dbAttributes = builder.dbAttributes;
		dbClass = builder.dbClass;
	}


	public static final class Builder {
		private List<AttributesData> dbAttributes;
		private String dbClass;

		public Builder() {
		}

		public Builder withDbAttributes(List<AttributesData> val) {
			dbAttributes = val;
			return this;
		}

		public Builder withDbClass(String val) {
			dbClass = val;
			return this;
		}

		public ProjectionAttribute build() {
			return new ProjectionAttribute(this);
		}
	}
}

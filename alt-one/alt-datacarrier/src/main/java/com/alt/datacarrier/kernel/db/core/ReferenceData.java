package com.alt.datacarrier.kernel.db.core;

import java.io.Serializable;

public class ReferenceData implements Serializable {

	private Integer referenceid;
	private String createdby;
	private Long createddate;
	private String modifiedby;
	private Long modifieddate;
	private String org;
	private ClassMeta classMeta;
	private ClassMeta referenceByClassMeta;
	private AttributeMeta referenceByAttributeMeta;
	private Integer orgId;
	
	public ReferenceData() {
		
	}

	public Integer getReferenceid() {
		return referenceid;
	}

	public void setReferenceid(Integer referenceid) {
		this.referenceid = referenceid;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Long getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Long createddate) {
		this.createddate = createddate;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Long getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Long modifieddate) {
		this.modifieddate = modifieddate;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public ClassMeta getClassMeta() {
		return classMeta;
	}

	public void setClassMeta(ClassMeta classMeta) {
		this.classMeta = classMeta;
	}

	public ClassMeta getReferenceByClassMeta() {
		return referenceByClassMeta;
	}

	public void setReferenceByClassMeta(ClassMeta referenceByClassMeta) {
		this.referenceByClassMeta = referenceByClassMeta;
	}

	public AttributeMeta getReferenceByAttributeMeta() {
		return referenceByAttributeMeta;
	}

	public void setReferenceByAttributeMeta(AttributeMeta referenceByAttributeMeta) {
		this.referenceByAttributeMeta = referenceByAttributeMeta;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	

}

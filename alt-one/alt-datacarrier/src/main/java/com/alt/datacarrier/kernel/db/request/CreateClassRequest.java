package com.alt.datacarrier.kernel.db.request;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumInstanceLimiter;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;

import java.util.List;

public class CreateClassRequest {

    private CreateClassRequest(Builder builder) {
        setProtocol(builder.protocol);
        classId = builder.classId;
        className = builder.className;
        attributes = builder.attributes;
        status = builder.status;
        locationSpecific = builder.locationSpecific;
        customAttributeAllowed = builder.customAttributeAllowed;
        packageCodes = builder.packageCodes;
        instanceLimiter = builder.instanceLimiter;
        instancePrefix = builder.instancePrefix;
        systemType = builder.systemType;
        parameterType = builder.parameterType;
        taskCodeRequired = builder.taskCodeRequired;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    private Protocol protocol;


    private String classId;

    private String className;
    
    private List<AttributeMeta> attributes;
		
	private EnumDocStatusType status;
	
	private Boolean locationSpecific; 
	
	private Boolean customAttributeAllowed;
	
	private List<String> packageCodes;
	
	private EnumInstanceLimiter instanceLimiter;
	
	private String instancePrefix;
	
	private Boolean systemType;
	
	private Boolean parameterType;
	
	private Boolean taskCodeRequired;

    public CreateClassRequest() {
    }



    public Protocol getProtocol() {
        return protocol;
    }
    public String getClassId() {
        return classId;
    }

    public String getClassName() {
        return className;
    }

    public List<AttributeMeta> getAttributes() {
        return attributes;
    }

    public EnumDocStatusType getStatus() {
        return status;
    }

    public Boolean getLocationSpecific() {
		return locationSpecific;
	}

	public Boolean getCustomAttributeAllowed() {
		return customAttributeAllowed;
	}

	public List<String> getPackageCodes() {
		return packageCodes;
	}

	public EnumInstanceLimiter getInstanceLimiter() {
		return instanceLimiter;
	}

	public String getInstancePrefix() {
		return instancePrefix;
	}

	public Boolean getSystemType() {
		return systemType;
	}

	public Boolean getParameterType() {
		return parameterType;
	}


    public Boolean getTaskCodeRequired() {
		return taskCodeRequired;
	}

	public void setTaskCodeRequired(Boolean taskCodeRequired) {
		this.taskCodeRequired = taskCodeRequired;
	}


	public static final class Builder {
        private Protocol protocol;
        private String classId;
        private String className;
        private List<AttributeMeta> attributes;
        private EnumDocStatusType status;
        private Boolean locationSpecific;
        private Boolean customAttributeAllowed;
        private List<String> packageCodes;
        private EnumInstanceLimiter instanceLimiter;
        private String instancePrefix;
        private Boolean systemType;
        private Boolean parameterType;
        private Boolean taskCodeRequired;

        public Builder() {
        }

        public Builder withProtocol(Protocol val) {
            protocol = val;
            return this;
        }

        public Builder withClassId(String val) {
            classId = val;
            return this;
        }

        public Builder withClassName(String val) {
            className = val;
            return this;
        }

        public Builder withAttributes(List<AttributeMeta> val) {
            attributes = val;
            return this;
        }

        public Builder withStatus(EnumDocStatusType val) {
            status = val;
            return this;
        }

        public Builder withLocationSpecific(Boolean val) {
            locationSpecific = val;
            return this;
        }

        public Builder withCustomAttributeAllowed(Boolean val) {
            customAttributeAllowed = val;
            return this;
        }

        public Builder withPackageCodes(List<String> val) {
            packageCodes = val;
            return this;
        }

        public Builder withInstanceLimiter(EnumInstanceLimiter val) {
            instanceLimiter = val;
            return this;
        }

        public Builder withInstancePrefix(String val) {
            instancePrefix = val;
            return this;
        }

        public Builder withSystemType(Boolean val) {
            systemType = val;
            return this;
        }

        public Builder withParameterType(Boolean val) {
            parameterType = val;
            return this;
        }
        
        public Builder withTaskCodeRequired(Boolean val) {
        	taskCodeRequired = val;
        	return this;
        }

        public CreateClassRequest build() {
            return new CreateClassRequest(this);
        }
    }
}

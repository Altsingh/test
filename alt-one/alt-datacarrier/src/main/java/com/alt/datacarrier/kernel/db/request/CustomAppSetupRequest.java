package com.alt.datacarrier.kernel.db.request;

import java.io.Serializable;

public class CustomAppSetupRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2672963986651672250L;

	private Integer organizationId = null;
	private Integer tenantId = null;
	private String appCode = null;
	private String appName = null;
	private String appLogoUrl = null;
	private Boolean applicationForDeletion = null;
	private Boolean applicationForEditing = null;

	public CustomAppSetupRequest() {
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public Integer getTenantId() {
		return tenantId;
	}

	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppLogoUrl() {
		return appLogoUrl;
	}

	public void setAppLogoUrl(String appLogoUrl) {
		this.appLogoUrl = appLogoUrl;
	}

	public Boolean getApplicationForDeletion() {
		return applicationForDeletion;
	}

	public Boolean getApplicationForEditing() {
		return applicationForEditing;
	}

	public void setApplicationForDeletion(Boolean applicationForDeletion) {
		this.applicationForDeletion = applicationForDeletion;
	}

	public void setApplicationForEditing(Boolean applicationForEditing) {
		this.applicationForEditing = applicationForEditing;
	}

}

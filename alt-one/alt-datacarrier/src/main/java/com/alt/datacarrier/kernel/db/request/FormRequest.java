package com.alt.datacarrier.kernel.db.request;

import java.util.Collections;
import java.util.List;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datacarrier.kernel.uiclass.EnumFormType;

public class FormRequest {

	private Protocol protocol;

	private EnumDocStatusType status;

	private EnumFormType formType;

	private String orgCode;

	private Integer orgId;

	private String formName;

	private boolean customComponentAllowed;

	private String description;

	private List<AltAbstractComponent> componentList;

	private Integer formId;

	private String version;

	private EnumFormState state;

	private ClassMeta classMeta;

	private Long hrFormId;

	private Integer appId;

	private Integer parentFormId;

	public Protocol getProtocol() {
		return protocol;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public EnumFormType getFormType() {
		return formType;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}
	
	public boolean isCustomComponentAllowed() {
		return customComponentAllowed;
	}

	public String getDescription() {
		return description;
	}

	public String getVersion() {
		return version;
	}

	public Integer getFormId() {
		return formId;
	}

	public EnumFormState getState() {
		return state;
	}

	public List<AltAbstractComponent> getComponentList() {
		return componentList;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public ClassMeta getClassMeta() {
		return classMeta;
	}

	public Long getHrFormId() {
		return hrFormId;
	}

	public void setHrFormId(Long hrFormId) {
		this.hrFormId = hrFormId;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public Integer getParentFormId() {
		return parentFormId;
	}

	public void setParentFormId(Integer parentFormId) {
		this.parentFormId = parentFormId;
	}

	public FormRequest(Builder builder) {
		this.status = builder.status;
		this.formType = builder.formType;
		this.orgCode = builder.orgCode;
		this.orgId = builder.orgId;
		this.formName = builder.formName;
		this.customComponentAllowed = builder.customComponentAllowed;
		this.description = builder.description;
		this.componentList = builder.componentList;
		this.protocol = builder.protocol;
		this.formId = builder.formId;
		this.version = builder.version;
		this.state = builder.state;
		this.classMeta = builder.classMeta;
		this.hrFormId = builder.hrFormId;
		this.appId = builder.appId;
		this.parentFormId = builder.parentFormId;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {

		private EnumDocStatusType status;

		private EnumFormType formType;

		private String orgCode;

		private Integer orgId;

		private String formName;

		private boolean customComponentAllowed;

		private String description;

		private List<AltAbstractComponent> componentList = Collections.emptyList();

		private Protocol protocol;

		private Integer formId;

		private String version;

		private EnumFormState state;

		private ClassMeta classMeta;

		private Long hrFormId;

		public Integer appId;

		private Integer parentFormId;

		public Builder() {
		}

		public Builder withStatus(EnumDocStatusType status) {
			this.status = status;
			return this;
		}

		public Builder withFormType(EnumFormType formType) {
			this.formType = formType;
			return this;
		}

		public Builder withOrgCode(String orgCode) {
			this.orgCode = orgCode;
			return this;
		}

		public Builder withFormName(String formName) {
			this.formName = formName;
			return this;
		}

		public Builder withCustomComponentAllowed(boolean customComponentAllowed) {
			this.customComponentAllowed = customComponentAllowed;
			return this;
		}

		public Builder withDescription(String description) {
			this.description = description;
			return this;
		}

		public Builder withVersion(String version) {
			this.version = version;
			return this;
		}

		public Builder withComponentList(List<AltAbstractComponent> componentList) {
			this.componentList = componentList;
			return this;
		}

		public Builder withProtocol(Protocol protocol) {
			this.protocol = protocol;
			return this;
		}

		public Builder withFormId(Integer formId) {
			this.formId = formId;
			return this;
		}

		public Builder withAppId(Integer appId) {
			this.appId = appId;
			return this;
		}

		public Builder withState(EnumFormState state) {
			this.state = state;
			return this;
		}

		public Builder withOrgId(Integer orgId) {
			this.orgId = orgId;
			return this;
		}

		public Builder withClassMeta(ClassMeta meta) {
			this.classMeta = meta;
			return this;
		}

		public Builder withHrFormId(Long val) {
			this.hrFormId = val;
			return this;
		}

		public Builder withParentFormId(Integer val) {
			this.parentFormId = val;
			return this;
		}

		public FormRequest build() {
			return new FormRequest(this);
		}
	}

}

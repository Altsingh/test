package com.alt.datacarrier.kernel.db.request;

import java.io.Serializable;
import java.util.Map;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;

public class InstanceHistoryRequest {

	private Protocol protocol;
	private Long instancecreateddate;
	private Long instancemodifieddate;
	private String instancecreatedby;
	private String instancemodifiedby;
	private Long modifiedDate;
	private Long createdDate;
	private EnumDocStatusType instancestatus;
	private EnumDocStatusType status;
	private Map<String, Object> attributes;
	private String userid;
	private String formName;
	private Integer formid;
	private Integer appid;
	private Integer classData;
	private Integer organizationid;
	private String className;
	private Integer instanceid;
	private Integer id;

	public InstanceHistoryRequest(Builder builder) {
		this.protocol = builder.protocol;
		this.instancecreateddate = builder.instancecreateddate;
		this.instancemodifieddate = builder.instancemodifieddate;
		this.instancecreatedby = builder.instancecreatedby;
		this.instancemodifiedby = builder.instancemodifiedby;
		this.modifiedDate = builder.modifiedDate;
		this.createdDate = builder.createdDate;
		this.instancestatus = builder.instancestatus;
		this.status = builder.status;
		this.attributes = builder.attributes;
		this.userid = builder.userid;
		this.formName = builder.formName;
		this.formid = builder.formid;
		this.appid = builder.appid;
		this.classData = builder.classData;
		this.organizationid = builder.organizationid;
		this.className = builder.className;
		this.instanceid = builder.instanceid;
		this.id = builder.id;

	}

	public Long getInstancecreateddate() {
		return instancecreateddate;
	}

	public void setInstancecreateddate(Long instancecreateddate) {
		this.instancecreateddate = instancecreateddate;
	}

	public Long getInstancemodifieddate() {
		return instancemodifieddate;
	}

	public void setInstancemodifieddate(Long instancemodifieddate) {
		this.instancemodifieddate = instancemodifieddate;
	}

	public String getInstancecreatedby() {
		return instancecreatedby;
	}

	public void setInstancecreatedby(String instancecreatedby) {
		this.instancecreatedby = instancecreatedby;
	}

	public String getInstancemodifiedby() {
		return instancemodifiedby;
	}

	public void setInstancemodifiedby(String instancemodifiedby) {
		this.instancemodifiedby = instancemodifiedby;
	}

	public Long getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Long modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Long createdDate) {
		this.createdDate = createdDate;
	}

	public EnumDocStatusType getInstancestatus() {
		return instancestatus;
	}

	public void setInstancestatus(EnumDocStatusType instancestatus) {
		this.instancestatus = instancestatus;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public void setStatus(EnumDocStatusType status) {
		this.status = status;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public Integer getFormid() {
		return formid;
	}

	public void setFormid(Integer formid) {
		this.formid = formid;
	}

	public Integer getAppid() {
		return appid;
	}

	public void setAppid(Integer appid) {
		this.appid = appid;
	}

	public Integer getClassData() {
		return classData;
	}

	public void setClassData(Integer classData) {
		this.classData = classData;
	}

	public Integer getOrganizationid() {
		return organizationid;
	}

	public void setOrganizationid(Integer organizationid) {
		this.organizationid = organizationid;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getInstanceid() {
		return instanceid;
	}

	public void setInstanceid(Integer instanceid) {
		this.instanceid = instanceid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public static final class Builder {
		public Protocol protocol;
		private Long instancecreateddate;
		private Long instancemodifieddate;
		private String instancecreatedby;
		private String instancemodifiedby;
		private Long modifiedDate;
		private Long createdDate;
		private KernelConstants.EnumDocStatusType instancestatus;
		private KernelConstants.EnumDocStatusType status;
		private Map<String, Object> attributes;
		private String userid;
		private String formName;
		private Integer formid;
		private Integer appid;
		private Integer classData;
		private Integer organizationid;
		private String className;
		private Integer instanceid;
		private Integer id;

		public Builder() {

		}

		public Builder withProtocol(Protocol val) {
			protocol = val;
			return this;
		}

		public Builder withModifiedDate(Long val) {
			modifiedDate = val;
			return this;
		}

		public Builder withCreatedDate(Long val) {
			createdDate = val;
			return this;
		}

		public Builder withInstanceCreatedDate(Long val) {
			instancecreateddate = val;
			return this;
		}

		public Builder withInstanceModifiedDate(Long val) {
			instancemodifieddate = val;
			return this;
		}

		public Builder withInstanceCreatedBy(String val) {
			instancecreatedby = val;
			return this;
		}

		public Builder withInstanceModifiedBy(String val) {
			instancemodifiedby = val;
			return this;
		}

		public Builder withOrganizationId(Integer val) {
			organizationid = val;
			return this;
		}

		public Builder withId(Integer val) {
			id = val;
			return this;
		}

		public Builder withInstanceId(Integer val) {
			instanceid = val;
			return this;
		}

		public Builder withClassName(String val) {
			className = val;
			return this;
		}

		public Builder withClassData(Integer val) {
			classData = val;
			return this;
		}

		public Builder withFormName(String val) {
			formName = val;
			return this;
		}

		public Builder withFormId(Integer val) {
			formid = val;
			return this;
		}

		public Builder withUserId(String val) {
			userid = val;
			return this;
		}

		public Builder withAppId(Integer val) {
			appid = val;
			return this;
		}

		public Builder withAttributes(Map<String, Object> val) {
			attributes = val;
			return this;
		}

		public Builder withStatus(KernelConstants.EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withInstanceStatus(KernelConstants.EnumDocStatusType val) {
			instancestatus = val;
			return this;
		}

		public InstanceHistoryRequest build() {
			return new InstanceHistoryRequest(this);
		}

	}

	@Override
	public String toString() {
		return "InstanceHistoryRequest{" + "protocol=" + protocol + ", instancecreateddate='" + instancecreateddate
				+ '\'' + ", instancemodifieddate=" + instancemodifieddate + ", instancecreatedby=" + instancecreatedby
				+ ", instancemodifiedby=" + instancemodifiedby + ", modifiedDate=" + modifiedDate + ", createdDate="
				+ createdDate + ", instancestatus=" + instancestatus + ", attributes=" + attributes + ", userid="
				+ userid + ", formName=" + formName + ", formid=" + formid + ", appid=" + appid + ", classData="
				+ classData + ", organizationid=" + organizationid + ", instancecreatedby=" + className
				+ ", instanceid=" + instanceid + ", status=" + status + '}';
	}
}

package com.alt.datacarrier.kernel.db.request;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants;

import java.util.Map;

public class InstanceRequest {
	
    private Protocol protocol;

    private String className;
    
    private Map<String, Object> attributes;

    private KernelConstants.EnumDocStatusType status;
    
    private Integer instanceId;
    
    private Integer formId;
    
    private Integer appId;
    
    private String userId;
    
    private String formName;

    private boolean processHistory;

    public InstanceRequest(Builder builder) {
        protocol = builder.protocol;
        className = builder.className;
        attributes = builder.attributes;
        status = builder.status;
        instanceId = builder.instanceId;
        formId = builder.formId;
        appId = builder.appId;
        userId = builder.userId;
        formName = builder.formName;
        processHistory = builder.processHistory;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public String getClassName() {
        return className;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }
    
    public Integer getFormId() {
    	return formId;
    }
    
    public Integer getAppId() {
    	return appId;
    }
    
    public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getUserId() {
    	return userId;
    }

    public boolean isProcessHistory() {
        return processHistory;
    }

    public KernelConstants.EnumDocStatusType getStatus() {
        return status;
    }
	
	    public Integer getInstanceId() {
        return instanceId;
    }

    public static final class Builder {
        private Protocol protocol;
        private String className;
        private Map<String, Object> attributes;
        private KernelConstants.EnumDocStatusType status;
        private Integer instanceId;
        private Integer formId;
        private Integer appId;
        private String formName;
        private String userId;
        private boolean processHistory;
        
        public Builder() {
        }

        public Builder withProtocol(Protocol val) {
            protocol = val;
            return this;
        }

        public Builder withClassName(String val) {
            className = val;
            return this;
        }

        public Builder withAttributes(Map<String, Object> val) {
            attributes = val;
            return this;
        }

        public Builder withFormId(Integer val) {
        	formId = val;
        	return this;
        }
        
        public Builder withAppId(Integer val) {
        	appId = val;
        	return this;
        }
        
        public Builder withFormName(String val) {
        	formName = val;
        	return this;
        }
        
        public Builder withUserId(String val) {
        	userId = val;
        	return this;
        }
        
        public Builder withStatus(KernelConstants.EnumDocStatusType val) {
            status = val;
            return this;
        }

        public Builder withInstanceId(Integer val) {
            instanceId = val;
            return this;
        }

        public Builder withProcessHistory(boolean val){
            processHistory = val;
            return this;
        }

        public InstanceRequest build() {
            return new InstanceRequest(this);
        }
    }

    @Override
    public String toString() {
        return "InstanceRequest{" +
                "protocol=" + protocol +
                ", className='" + className + '\'' +
                ", attributes=" + attributes +
                ", formId=" + formId +
                ", appId=" + appId +
                ", formName=" + formName +
                ", userId=" + userId +
                ", status=" + status +
                ", instanceId=" + instanceId +
                '}';
    }
}

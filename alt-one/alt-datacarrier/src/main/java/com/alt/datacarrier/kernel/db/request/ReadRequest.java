package com.alt.datacarrier.kernel.db.request;

import java.util.Map;

import com.alt.datacarrier.core.Protocol;

public class ReadRequest {

	private Protocol protocol;

	private String name;

	private Integer depth;

	private Integer offset;

	private Integer limit;

	private Integer id;

	private String version;

	private String formSecurityId;

	private Integer metaClassId;

	private Integer appId;
	
	private String className;
	
	private String formName;
	
	private Map<String,String> attributes;

	public ReadRequest() {
	}

	public ReadRequest(Protocol protocol, String name, Integer depth, Integer offset, Integer limit, Integer id,
			String version, String formSecurityId) {
		this.protocol = protocol;
		this.name = name;
		this.depth = depth;
		this.offset = offset;
		this.limit = limit;
		this.id = id;
		this.version = version;
		this.formSecurityId = formSecurityId;
	}

	public ReadRequest(Protocol protocol, String name, Integer depth, Integer offset, Integer limit, Integer id,
			String version, String formSecurityId, Integer appId) {
		this.protocol = protocol;
		this.name = name;
		this.depth = depth;
		this.offset = offset;
		this.limit = limit;
		this.id = id;
		this.version = version;
		this.formSecurityId = formSecurityId;
		this.appId = appId;
	}

	public Protocol getProtocol() {
		return protocol;
	}

	public String getName() {
		return name;
	}

	public Integer getDepth() {
		return depth;
	}

	public Integer getOffset() {
		return offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public Integer getId() {
		return id;
	}

	public String getVersion() {
		return version;
	}

	public String getFormSecurityId() {
		return formSecurityId;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDepth(Integer depth) {
		this.depth = depth;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setFormSecurityId(String formSecurityId) {
		this.formSecurityId = formSecurityId;
	}

	public Integer getMetaClassId() {
		return metaClassId;
	}

	public void setMetaClassId(Integer metaClassId) {
		this.metaClassId = metaClassId;
	}

	public Integer getAppId() {
		return appId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Map<String,String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String,String> attributes) {
		this.attributes = attributes;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public static final class ReadRequestBuilder {
		private Protocol protocol;
		private String name;
		private Integer depth;
		private Integer offset;
		private Integer limit;
		private Integer id;
		private String version;
		private String formSecurityId;

		private ReadRequestBuilder() {
		}

		public static ReadRequestBuilder aReadRequest() {
			return new ReadRequestBuilder();
		}

		public ReadRequestBuilder withProtocol(Protocol protocol) {
			this.protocol = protocol;
			return this;
		}

		public ReadRequestBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public ReadRequestBuilder withDepth(Integer depth) {
			this.depth = depth;
			return this;
		}

		public ReadRequestBuilder withOffset(Integer offset) {
			this.offset = offset;
			return this;
		}

		public ReadRequestBuilder withLimit(Integer limit) {
			this.limit = limit;
			return this;
		}

		public ReadRequestBuilder withId(Integer id) {
			this.id = id;
			return this;
		}

		public ReadRequestBuilder withVersion(String version) {
			this.version = version;
			return this;
		}

		public ReadRequestBuilder withFormSecurityId(String formSecurityId) {
			this.formSecurityId = formSecurityId;
			return this;
		}

		public ReadRequest build() {
			ReadRequest readRequest = new ReadRequest();
			readRequest.setProtocol(protocol);
			readRequest.setName(name);
			readRequest.setDepth(depth);
			readRequest.setOffset(offset);
			readRequest.setLimit(limit);
			readRequest.setId(id);
			readRequest.setVersion(version);
			readRequest.setFormSecurityId(formSecurityId);
			return readRequest;
		}
	}
}

package com.alt.datacarrier.kernel.db.request;

import com.alt.datacarrier.core.Protocol;

public class TaskCodeRecordTransport {

	private Protocol protocol;
	private Integer appId;
	private String formId;
	private String formInitials;
	private String lastCodeUsed;
	private String classId;
	private Integer taskCodeID;
	private String formName;

	public TaskCodeRecordTransport() {
	}

	private TaskCodeRecordTransport(Builder builder) {
		protocol = builder.protocol;
		appId = builder.appId;
		formId = builder.formId;
		formInitials = builder.formInitials;
		lastCodeUsed = builder.lastCodeUsed;
		classId = builder.classId;
		taskCodeID = builder.taskCodeID;
		formName = builder.formName;
	}

	public Integer getAppId() {
		return appId;
	}

	public String getFormId() {
		return formId;
	}

	public String getFormInitials() {
		return formInitials;
	}

	public String getLastCodeUsed() {
		return lastCodeUsed;
	}

	public String getClassId() {
		return classId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public void setFormInitials(String formInitials) {
		this.formInitials = formInitials;
	}

	public void setLastCodeUsed(String lastCodeUsed) {
		this.lastCodeUsed = lastCodeUsed;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public Integer getTaskCodeID() {
		return taskCodeID;
	}

	public void setTaskCodeID(Integer taskCodeID) {
		this.taskCodeID = taskCodeID;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public static final class Builder {
		private Protocol protocol;
		private Integer appId;
		private String formId;
		private String formInitials;
		private String lastCodeUsed;
		private String classId;
		private Integer taskCodeID;
		private String formName;

		public Builder() {
		}

		public Builder withProtocol(Protocol val) {
			protocol = val;
			return this;
		}

		public Builder withAppId(Integer val) {
			appId = val;
			return this;
		}

		public Builder withFormId(String val) {
			formId = val;
			return this;
		}

		public Builder withFormInitials(String val) {
			formInitials = val;
			return this;
		}

		public Builder withLastCodeUsed(String val) {
			lastCodeUsed = val;
			return this;
		}

		public Builder withClassId(String val) {
			classId = val;
			return this;
		}
		
		public Builder withTaskCodeID(Integer val) {
			taskCodeID = val;
			return this;
		}
		
		public Builder withFormName(String val) {
			formName = val;
			return this;
		}
		
		public TaskCodeRecordTransport build() {
            return new TaskCodeRecordTransport(this);
        }

	}
}

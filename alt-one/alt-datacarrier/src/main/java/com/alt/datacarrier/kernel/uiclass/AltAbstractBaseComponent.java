package com.alt.datacarrier.kernel.uiclass;

import java.util.List;

public abstract class AltAbstractBaseComponent<T> extends AltAbstractComponent {

	private static final long serialVersionUID = 3325907143148958405L;

	private String dbClassRead;
	
	private String dbAttrRead;
	
	private String dbClassWrite;
	
	private String dbAttrWrite;

	private String dbClassType;
	
	private String hint;
    
	private T value;
    
	private List<AltValidation<T>> validation;
	
	private boolean required;

	private String attributeId;
	
	public AltAbstractBaseComponent(){}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getDbClassWrite() {
		return dbClassWrite;
	}

	public void setDbClassWrite(String dbClassWrite) {
		this.dbClassWrite = dbClassWrite;
	}

	public String getDbAttrWrite() {
		return dbAttrWrite;
	}

	public void setDbAttrWrite(String dbAttrWrite) {
		this.dbAttrWrite = dbAttrWrite;
	}
    
    public AltAbstractBaseComponent(EnumHTMLControl controlType) {
		super(controlType);
	}

	public String getDbClassType() {
		return dbClassType;
	}

	public void setDbClassType(String dbClassType) {
		this.dbClassType = dbClassType;
	}

	public String getDbClassRead() {
		return dbClassRead;
	}

	public void setDbClassRead(String dbClassRead) {
		this.dbClassRead = dbClassRead;
	}

	public String getDbAttrRead() {
		return dbAttrRead;
	}

	public void setDbAttrRead(String dbAttrRead) {
		this.dbAttrRead = dbAttrRead;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public List<AltValidation<T>> getValidation() {
		return validation;
	}

	public void setValidation(List<AltValidation<T>> validation) {
		this.validation = validation;
	}

	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}
    
}

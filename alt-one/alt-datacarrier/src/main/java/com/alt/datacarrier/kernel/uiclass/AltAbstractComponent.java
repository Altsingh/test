package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;
import java.util.List;

import com.alt.datacarrier.common.enumeration.EnumDataType;
import com.alt.datacarrier.common.enumeration.EnumSecurityType;
import com.alt.datacarrier.formsecurity.AltComponentSecurity;
import com.alt.rule.model.Rule;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "controlType")
@JsonSubTypes({

		@JsonSubTypes.Type(value = AltPanelComponent.class, name = "PANEL"),

		@JsonSubTypes.Type(value = AltTabbedPanelComponent.class, name = "TAB_PANEL"),

		@JsonSubTypes.Type(value = AltTextboxComponent.class, name = "TEXTFIELD"),

		@JsonSubTypes.Type(value = AltDropdownComponent.class, name = "DROPDOWN"),

		@JsonSubTypes.Type(value = AltMultiSelectComponent.class, name = "MULTI_SELECT"),

		@JsonSubTypes.Type(value = AltInputTextAreaComponent.class, name = "TEXTAREA"),

		@JsonSubTypes.Type(value = AltRadioGroupComponent.class, name = "RADIO_GROUP"),

		@JsonSubTypes.Type(value = CheckboxGroupComponent.class, name = "CHECKBOX_GROUP"),

		@JsonSubTypes.Type(value = CheckboxSingleComponnet.class, name = "CHECKBOX_SINGLE"),

		@JsonSubTypes.Type(value = AltDateComponent.class, name = "DATE"),

		@JsonSubTypes.Type(value = AltBlankComponent.class, name = "BLANK"),

		@JsonSubTypes.Type(value = AltImageComponent.class, name = "IMAGE"),

		@JsonSubTypes.Type(value = AltLabelComponent.class, name = "LABEL"),

		@JsonSubTypes.Type(value = AltTimeComponent.class, name = "TIME"),

		@JsonSubTypes.Type(value = AltSliderComponent.class, name = "SLIDER"),

		@JsonSubTypes.Type(value = AltDataTable.class, name = "DATATABLE"),

		@JsonSubTypes.Type(value = AltButtonComponent.class, name = "BUTTON"),

		@JsonSubTypes.Type(value = AltExcelComponent.class, name = "EXCEL"),

		@JsonSubTypes.Type(value = AltAttachmentsComponent.class, name = "ATTACHMENTS"),

		@JsonSubTypes.Type(value = AltCheckboxComponent.class, name = "CHECKBOX"),

		@JsonSubTypes.Type(value = AltLinkComponent.class, name = "LINK")

})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AltAbstractComponent implements Serializable {

	private static final long serialVersionUID = 3810417369466030627L;

	private String namespace;

	private String id;

	private String label;

	private String name;

	private String cssClass;

	private String instruction;

	private String helpText;

	private String clientId;

	private String[] events;

	private List<Integer> rules;

	private List<Rule> ruleObjects;

	AltComponentFilter[] filters;

	@JsonProperty("controlType")
	private EnumHTMLControl controlType;

	private List<AltRelation> relation;

	private EnumSecurityType securityType;

	private EnumDataType dataType;

	private Boolean historyEnabled;

	private Boolean instanceSpecificHistory;

	private Boolean cardView;

	private Boolean listView;

	private Long hrFormFieldId;

	private Boolean instanceOwnerFlag;

	private String matchOwnerId;

	private AltComponentSecurity security;

	public AltComponentFilter[] getFilters() {
		return filters;
	}

	public void setFilters(AltComponentFilter[] filter) {
		this.filters = filter;
	}

	public EnumDataType getDataType() {
		return dataType;
	}

	public void setDataType(EnumDataType dataType) {
		this.dataType = dataType;
	}

	public AltAbstractComponent() {
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public String getHelpText() {
		return helpText;
	}

	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public AltAbstractComponent(EnumHTMLControl controlType) {
		this.controlType = controlType;
	}

	public EnumSecurityType getSecurityType() {
		return securityType;
	}

	public void setSecurityType(EnumSecurityType securityType) {
		this.securityType = securityType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EnumHTMLControl getControlType() {
		return controlType;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	@JsonProperty("controlType")
	public void setControlType(EnumHTMLControl controlType) {
		this.controlType = controlType;
	}

	public List<AltRelation> getRelation() {
		return relation;
	}

	public void setRelation(List<AltRelation> relation) {
		this.relation = relation;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public Boolean getHistoryEnabled() {
		return historyEnabled;
	}

	public void setHistoryEnabled(Boolean historyEnabled) {
		this.historyEnabled = historyEnabled;
	}

	public Boolean getInstanceSpecificHistory() {
		return instanceSpecificHistory;
	}

	public void setInstanceSpecificHistory(Boolean instanceSpecificHistory) {
		this.instanceSpecificHistory = instanceSpecificHistory;
	}

	public List<Integer> getRules() {
		return rules;
	}

	public void setRules(List<Integer> rules) {
		this.rules = rules;
	}

	public List<Rule> getRuleObjects() {
		return ruleObjects;
	}

	public void setRuleObjects(List<Rule> ruleObjects) {
		this.ruleObjects = ruleObjects;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AltAbstractComponent other = (AltAbstractComponent) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Long getHrFormFieldId() {
		return hrFormFieldId;
	}

	public void setHrFormFieldId(Long hrFormFieldId) {
		this.hrFormFieldId = hrFormFieldId;
	}

	public AltComponentSecurity getSecurity() {
		return security;
	}

	public void setSecurity(AltComponentSecurity security) {
		this.security = security;
	}

	public Boolean getCardView() {
		return cardView;
	}

	public Boolean getListView() {
		return listView;
	}

	public void setCardView(Boolean cardView) {
		this.cardView = cardView;
	}

	public void setListView(Boolean listView) {
		this.listView = listView;
	}

	public Boolean getInstanceOwnerFlag() {
		return instanceOwnerFlag;
	}

	public void setInstanceOwnerFlag(Boolean instanceOwnerFlag) {
		this.instanceOwnerFlag = instanceOwnerFlag;
	}

	public String getMatchOwnerId() {
		return matchOwnerId;
	}

	public void setMatchOwnerId(String matchOwnerId) {
		this.matchOwnerId = matchOwnerId;
	}

	public String[] getEvents() {
		return events;
	}

	public void setEvents(String[] events) {
		this.events = events;
	}

}

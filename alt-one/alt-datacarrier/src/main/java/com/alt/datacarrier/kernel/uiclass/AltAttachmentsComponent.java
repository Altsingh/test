package com.alt.datacarrier.kernel.uiclass;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("ATTACHMENTS")
public class AltAttachmentsComponent extends AltAbstractComponent {

	private static final long serialVersionUID = 1L;

	public AltAttachmentsComponent() {
		super(EnumHTMLControl.ATTACHMENTS);
	}

	private Integer totalFileSizeLimitMB;

	private List<FileDetail> files;

	public Integer getTotalFileSizeLimitMB() {
		return totalFileSizeLimitMB;
	}

	public void setTotalFileSizeLimitMB(Integer totalFileSizeLimitMB) {
		this.totalFileSizeLimitMB = totalFileSizeLimitMB;
	}

	public List<FileDetail> getFiles() {
		return files;
	}

	public void setFiles(List<FileDetail> files) {
		this.files = files;
	}

}

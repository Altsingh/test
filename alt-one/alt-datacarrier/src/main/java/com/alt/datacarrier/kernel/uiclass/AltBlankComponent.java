package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("BLANK")
public class AltBlankComponent<String> extends AltInputControlComponent<String> {

	public AltBlankComponent() {
		super(EnumHTMLControl.BLANK);
		// TODO Auto-generated constructor stub
	}

}

package com.alt.datacarrier.kernel.uiclass;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("BUTTON")
public class AltButtonComponent extends AltAbstractComponent {

	private String name;

	private String id;

	private String label;

	private String cssClass;

	private String instruction;

	private String helpText;

	private EnumHTMLControl controlType;

	private static final long serialVersionUID = -4455319222909604845L;

	private List<String> componentIds;

	// private List<Integer> rules;

	// private List<Rule> ruleObjects;

	private List<Integer> commIdList;

	private Integer actionId;

	private Boolean clicked;

	// private String[] events;

	private FormNavigation navigation;
	
	/**
	   * if this flag is true, form submission to backend
	   * won't happen.
	   * @author vaibhav.kashayp
	  */
	private boolean validationsOnly ;

	public AltButtonComponent() {
		this.controlType = EnumHTMLControl.BUTTON;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public String getHelpText() {
		return helpText;
	}

	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}

	public EnumHTMLControl getControlType() {
		return controlType;
	}

	public void setControlType(EnumHTMLControl controlType) {
		this.controlType = controlType;
	}

	/*
	 * public List<Integer> getRules() { return rules; }
	 * 
	 * public void setRules(List<Integer> rules) { this.rules = rules; }
	 */

	public List<Integer> getCommIdList() {
		return commIdList;
	}

	public void setCommIdList(List<Integer> commIdList) {
		this.commIdList = commIdList;
	}

	/*
	 * public List<Rule> getRuleObjects() { return ruleObjects; }
	 * 
	 * public void setRuleObjects(List<Rule> ruleObjects) { this.ruleObjects =
	 * ruleObjects; }
	 */
	public Integer getActionId() {
		return actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	public Boolean getClicked() {
		return clicked;
	}

	public void setClicked(Boolean clicked) {
		this.clicked = clicked;
	}

	public FormNavigation getNavigation() {
		return navigation;
	}

	public void setNavigation(FormNavigation navigation) {
		this.navigation = navigation;
	}

	public boolean isValidationsOnly() {
		return validationsOnly;
	}

	public void setValidationsOnly(boolean validationsOnly) {
		this.validationsOnly = validationsOnly;
	}

	/*
	 * public String[] getEvents() { return events; }
	 * 
	 * public void setEvents(String[] events) { this.events = events; }
	 */

}

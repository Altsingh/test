package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("CHECKBOX")
public class AltCheckboxComponent extends AltAbstractComponent {

	private static final long serialVersionUID = 1L;

	public AltCheckboxComponent() {
		super(EnumHTMLControl.CHECKBOX);
	}

	private Boolean value;

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}

}

package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;

public class AltComponentFilter implements Serializable {
	
	private static final long serialVersionUID = 3325907143148953205L;
	
	private String filterField;
    private String filterSourceField;
    private String dbClassName;
    private String filterSourceComponentId;
    private String  filterSourceClassName;
    private String joiner;
	public String getFilterField() {
		return filterField;
	}
	public void setFilterField(String filterField) {
		this.filterField = filterField;
	}
	public String getFilterSourceField() {
		return filterSourceField;
	}
	public void setFilterSourceField(String filterSourceField) {
		this.filterSourceField = filterSourceField;
	}
	public String getDbClassName() {
		return dbClassName;
	}
	public void setDbClassName(String dbClassName) {
		this.dbClassName = dbClassName;
	}
	public String getFilterSourceComponentId() {
		return filterSourceComponentId;
	}
	public void setFilterSourceComponentId(String filterSourceComponentId) {
		this.filterSourceComponentId = filterSourceComponentId;
	}
	public String getFilterSourceClassName() {
		return filterSourceClassName;
	}
	public void setFilterSourceClassName(String filterSourceClassName) {
		this.filterSourceClassName = filterSourceClassName;
	}
	public String getJoiner() {
		return joiner;
	}
	public void setJoiner(String joiner) {
		this.joiner = joiner;
	}
    
}

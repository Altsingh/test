package com.alt.datacarrier.kernel.uiclass;

import com.alt.datacarrier.core.IRequestData;

public class AltCustomFormDataContext  implements IRequestData{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2947412291458832906L;
	
	private String contextType;

	public String getContextType() {
		return contextType;
	}

	public void setContextType(String contextType) {
		this.contextType = contextType;
	}
	
	

}

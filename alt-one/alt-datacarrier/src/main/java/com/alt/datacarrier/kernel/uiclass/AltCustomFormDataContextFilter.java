package com.alt.datacarrier.kernel.uiclass;

import com.alt.datacarrier.core.IRequestData;

public class AltCustomFormDataContextFilter  implements IRequestData{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3774174326813201938L;

	private String contextLabel;
	
	private String contextValue;

	public String getContextLabel() {
		return contextLabel;
	}

	public void setContextLabel(String contextLabel) {
		this.contextLabel = contextLabel;
	}

	public String getContextValue() {
		return contextValue;
	}

	public void setContextValue(String contextValue) {
		this.contextValue = contextValue;
	}

}

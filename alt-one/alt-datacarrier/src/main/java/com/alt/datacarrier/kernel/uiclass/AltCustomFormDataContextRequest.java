package com.alt.datacarrier.kernel.uiclass;

import java.util.List;

import com.alt.datacarrier.core.IRequestData;

public class AltCustomFormDataContextRequest  implements IRequestData {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1950562670731175793L;

	private AltCustomFormDataContext context;
	
	private List<AltCustomFormDataContextFilter> contextFilter;
	

	public AltCustomFormDataContext getContext() {
		return context;
	}

	public void setContext(AltCustomFormDataContext context) {
		this.context = context;
	}

	public List<AltCustomFormDataContextFilter> getContextFilter() {
		return contextFilter;
	}

	public void setContextFilter(List<AltCustomFormDataContextFilter> contextFilter) {
		this.contextFilter = contextFilter;
	}

}

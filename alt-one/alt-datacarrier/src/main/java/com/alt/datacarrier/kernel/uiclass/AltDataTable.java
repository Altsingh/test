package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("rawtypes")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AltDataTable extends AltAbstractBaseComponent implements Serializable {

	private static final long serialVersionUID = -1789748567216508966L;

	@JsonProperty
	private List<String> displayedColumns;

	private List<String> headers;

	@JsonProperty
	private Map<Integer, Object> data;

	private List<String> allColumns;

	private Map<Integer, Object> filteringData;

	@JsonProperty
	private Integer formPopup;

	private List<AltButtonComponent> navigationButtons;

	public AltDataTable() {
		super(EnumHTMLControl.DATATABLE);
	}

	public List<String> getHeaders() {
		return headers;
	}

	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}

	public List<String> getDisplayedColumns() {
		return displayedColumns;
	}

	public void setDisplayedColumns(List<String> displayedColumns) {
		this.displayedColumns = displayedColumns;
	}

	public Map<Integer, Object> getData() {
		return data;
	}

	public void setData(Map<Integer, Object> data) {
		this.data = data;
	}

	public Integer getFormPopup() {
		return formPopup;
	}

	public void setFormPopup(Integer formPopup) {
		this.formPopup = formPopup;
	}

	@Override
	public String toString() {
		return "AltDataTable{" + "displayedColumns=" + displayedColumns + ", headers=" + headers + ", data=" + data
				+ ", formPopup=" + formPopup + '}';
	}

	public Map<Integer, Object> getFilteringData() {
		return filteringData;
	}

	public void setFilteringData(Map<Integer, Object> filteringData) {
		this.filteringData = filteringData;
	}

	public List<String> getAllColumns() {
		return allColumns;
	}

	public void setAllColumns(List<String> allColumns) {
		this.allColumns = allColumns;
	}

	public List<AltButtonComponent> getNavigationButtons() {
		return navigationButtons;
	}

	public void setNavigationButtons(List<AltButtonComponent> navigationButtons) {
		this.navigationButtons = navigationButtons;
	}

}

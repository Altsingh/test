package com.alt.datacarrier.kernel.uiclass;

public class AltDatatableColumn {
	
	private String label;
	private boolean reference;
	private String attribute;
	private String refClass;
	private RefAttributeData refAttributeData;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public boolean isReference() {
		return reference;
	}
	public void setReference(boolean reference) {
		this.reference = reference;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getRefClass() {
		return refClass;
	}
	public void setRefClass(String refClass) {
		this.refClass = refClass;
	}
	public RefAttributeData getRefAttributeData() {
		return refAttributeData;
	}
	public void setRefAttributeData(RefAttributeData refAttributeData) {
		this.refAttributeData = refAttributeData;
	}
	
}

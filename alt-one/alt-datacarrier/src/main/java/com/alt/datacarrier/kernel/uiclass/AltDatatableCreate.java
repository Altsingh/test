package com.alt.datacarrier.kernel.uiclass;

import java.util.List;

public class AltDatatableCreate extends AltAbstractComponent {

	/**
	 * 
	 */
	private AltDatatableColumn dataTableColumn;
	private String classCode;
	private DBFilter dbFilter;
	private List<String> filterJoinOperator;
	private int offset;
	private int limit;
	
	public AltDatatableCreate(EnumHTMLControl controlType) {
		super(EnumHTMLControl.DATATABLE);
	}

	public AltDatatableColumn getDataTableColumn() {
		return dataTableColumn;
	}

	public void setDataTableColumn(AltDatatableColumn dataTableColumn) {
		this.dataTableColumn = dataTableColumn;
	}

	public String getClassCode() {
		return classCode;
	}

	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public DBFilter getDbFilter() {
		return dbFilter;
	}

	public void setDbFilter(DBFilter dbFilter) {
		this.dbFilter = dbFilter;
	}

	public List<String> getFilterJoinOperator() {
		return filterJoinOperator;
	}

	public void setFilterJoinOperator(List<String> filterJoinOperator) {
		this.filterJoinOperator = filterJoinOperator;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}


}

package com.alt.datacarrier.kernel.uiclass;

import java.util.List;

public class AltDatatableRead {

	//private List<DatatableRecord> recordList;
	private AltDatatableCreate altDatatableCreate;
	private List<DatatableHeader> dataTableHeader;
	private List<String> cssClass;
	private String label;
	
	public AltDatatableCreate getAltDatatableCreate() {
		return altDatatableCreate;
	}
	public void setAltDatatableCreate(AltDatatableCreate altDatatableCreate) {
		this.altDatatableCreate = altDatatableCreate;
	}
	public List<DatatableHeader> getDataTableHeader() {
		return dataTableHeader;
	}
	public void setDataTableHeader(List<DatatableHeader> dataTableHeader) {
		this.dataTableHeader = dataTableHeader;
	}
	public List<String> getCssClass() {
		return cssClass;
	}
	public void setCssClass(List<String> cssClass) {
		this.cssClass = cssClass;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	
}

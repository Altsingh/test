package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("DATE")
public class AltDateComponent<String> extends AltInputControlComponent<String> {

	public AltDateComponent() {
		super(EnumHTMLControl.DATE);
		// TODO Auto-generated constructor stub
	}

}


package com.alt.datacarrier.kernel.uiclass;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonTypeName;


@JsonTypeName("DROPDOWN")
public class AltDropdownComponent<T> extends AltGroupComponent<T> {
	private String placeholder;
	private List<AltOption> options;
	private Map<Integer,Object> filteringData;
	private List<String> allColumns;
	private String attributeName;
	 
	
	public AltDropdownComponent() {
		super(EnumHTMLControl.DROPDOWN);
	}

	public String getPlaceholder() {
		return placeholder;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

	public List<AltOption> getOptions() {
		return options;
	}

	public void setOptions(List<AltOption> options) {
		this.options = options;
	}	
	public Map<Integer,Object> getFilteringData() {
		return filteringData;
	}

	public void setFilteringData(Map<Integer,Object> filteringData) {
		this.filteringData = filteringData;
	}

	public List<String> getAllColumns() {
		return allColumns;
	}

	public void setAllColumns(List<String> allColumns) {
		this.allColumns = allColumns;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}	
	
}

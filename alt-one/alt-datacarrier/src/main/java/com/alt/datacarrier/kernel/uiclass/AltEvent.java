package com.alt.datacarrier.kernel.uiclass;

public class AltEvent{

	private String action;//AJAXUPDATE,FOCUS
	
	private WebService webService;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public WebService getWebService() {
		return webService;
	}
	public void setWebService(WebService webService) {
		this.webService = webService;
	}
	
}

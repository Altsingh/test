package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("EXCEL")
public class AltExcelComponent extends AltAbstractComponent {

	private static final long serialVersionUID = 1L;

	private String dataFileName;

	private String dataFilePath;

	public String getDataFileName() {
		return dataFileName;
	}

	public void setDataFileName(String dataFileName) {
		this.dataFileName = dataFileName;
	}

	public String getDataFilePath() {
		return dataFilePath;
	}

	public void setDataFilePath(String dataFilePath) {
		this.dataFilePath = dataFilePath;
	}

	public AltExcelComponent() {
		super(EnumHTMLControl.EXCEL);
	}

}

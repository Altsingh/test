package com.alt.datacarrier.kernel.uiclass;

import java.util.List;

import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;

public class AltForm {
	
	private EnumDocStatusType status;
	
	private EnumFormType formType;
	
	private EnumRequestType type;
	
	private String orgCode;
	
	private String uiClassName;
	
	private String uiClassCode;
		
	private boolean customComponentAllowed;
	
	private String moduleCode;
		
	private String description;
	
	private List<AltAbstractComponent> componentList;
	
	private List<String> updationUuidList;
		
	private String cas;
	
	private String id;
	
	private Long createdDate;
	
	private Long modifiedDate;
	
	private String createdBy;
	
	private String modifiedBy;
	
	private boolean readLock;
	
	private EnumFormState formState;

    public void setStatus(EnumDocStatusType status) {
        this.status = status;
    }

    public void setFormType(EnumFormType formType) {
        this.formType = formType;
    }

    public void setType(EnumRequestType type) {
        this.type = type;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public void setUiClassName(String uiClassName) {
        this.uiClassName = uiClassName;
    }

    public void setUiClassCode(String uiClassCode) {
        this.uiClassCode = uiClassCode;
    }

    public void setCustomComponentAllowed(boolean customComponentAllowed) {
        this.customComponentAllowed = customComponentAllowed;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setComponentList(List<AltAbstractComponent> componentList) {
        this.componentList = componentList;
    }

    public void setUpdationUuidList(List<String> updationUuidList) {
        this.updationUuidList = updationUuidList;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public void setModifiedDate(Long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public void setReadLock(boolean readLock) {
        this.readLock = readLock;
    }

    public void setFormState(EnumFormState formState) {
        this.formState = formState;
    }

    public AltForm() {
	}

	private AltForm(Builder builder) {
		status = builder.status;
		formType = builder.formType;
		type = builder.type;
		orgCode = builder.orgCode;
		uiClassName = builder.uiClassName;
		uiClassCode = builder.uiClassCode;
		customComponentAllowed = builder.customComponentAllowed;
		moduleCode = builder.moduleCode;
		description = builder.description;
		componentList = builder.componentList;
		updationUuidList = builder.updationUuidList;
		cas = builder.cas;
		id = builder.id;
		createdDate = builder.createdDate;
		modifiedDate = builder.modifiedDate;
		createdBy = builder.createdBy;
		modifiedBy = builder.modifiedBy;
		readLock = builder.readLock;
		formState = builder.formState;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public EnumFormType getFormType() {
		return formType;
	}

	public EnumRequestType getType() {
		return type;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public String getUiClassName() {
		return uiClassName;
	}

	public String getUiClassCode() {
		return uiClassCode;
	}

	public boolean isCustomComponentAllowed() {
		return customComponentAllowed;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public String getDescription() {
		return description;
	}

	public List<AltAbstractComponent> getComponentList() {
		return componentList;
	}

	public List<String> getUpdationUuidList() {
		return updationUuidList;
	}

	public String getCas() {
		return cas;
	}

	public String getId() {
		return id;
	}

	public Long getCreatedDate() {
		return createdDate;
	}

	public Long getModifiedDate() {
		return modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public boolean isReadLock() {
		return readLock;
	}

	public EnumFormState getFormState() {
		return formState;
	}


	public static final class Builder {
		private EnumDocStatusType status;
		private EnumFormType formType;
		private EnumRequestType type;
		private String orgCode;
		private String uiClassName;
		private String uiClassCode;
		private boolean customComponentAllowed;
		private String moduleCode;
		private String description;
		private List<AltAbstractComponent> componentList;
		private List<String> updationUuidList;
		private String cas;
		private String id;
		private Long createdDate;
		private Long modifiedDate;
		private String createdBy;
		private String modifiedBy;
		private boolean readLock;
		private EnumFormState formState;

		public Builder() {
		}

		public Builder withStatus(EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withFormType(EnumFormType val) {
			formType = val;
			return this;
		}

		public Builder withType(EnumRequestType val) {
			type = val;
			return this;
		}

		public Builder withOrgCode(String val) {
			orgCode = val;
			return this;
		}

		public Builder withUiClassName(String val) {
			uiClassName = val;
			return this;
		}

		public Builder withUiClassCode(String val) {
			uiClassCode = val;
			return this;
		}

		public Builder withCustomComponentAllowed(boolean val) {
			customComponentAllowed = val;
			return this;
		}

		public Builder withModuleCode(String val) {
			moduleCode = val;
			return this;
		}

		public Builder withDescription(String val) {
			description = val;
			return this;
		}

		public Builder withComponentList(List<AltAbstractComponent> val) {
			componentList = val;
			return this;
		}

		public Builder withUpdationUuidList(List<String> val) {
			updationUuidList = val;
			return this;
		}

		public Builder withCas(String val) {
			cas = val;
			return this;
		}

		public Builder withId(String val) {
			id = val;
			return this;
		}

		public Builder withCreatedDate(Long val) {
			createdDate = val;
			return this;
		}

		public Builder withModifiedDate(Long val) {
			modifiedDate = val;
			return this;
		}

		public Builder withCreatedBy(String val) {
			createdBy = val;
			return this;
		}

		public Builder withModifiedBy(String val) {
			modifiedBy = val;
			return this;
		}

		public Builder withReadLock(boolean val) {
			readLock = val;
			return this;
		}

		public Builder withFormState(EnumFormState val) {
			formState = val;
			return this;
		}

		public AltForm build() {
			return new AltForm(this);
		}
	}
}

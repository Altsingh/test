package com.alt.datacarrier.kernel.uiclass;

public abstract class AltGroupComponent<T> extends AltAbstractBaseComponent<T> {
	
	public AltGroupComponent(EnumHTMLControl controlType) {
		super(controlType);
		// TODO Auto-generated constructor stub
	}
	private String optionDbClass;
	private String optionDbAttr;
	
	public String getOptionDbClass() {
		return optionDbClass;
	}
	public void setOptionDbClass(String optionDbClass) {
		this.optionDbClass = optionDbClass;
	}
	public String getOptionDbAttr() {
		return optionDbAttr;
	}
	public void setOptionDbAttr(String optionDbAttr) {
		this.optionDbAttr = optionDbAttr;
	}
	
	

}

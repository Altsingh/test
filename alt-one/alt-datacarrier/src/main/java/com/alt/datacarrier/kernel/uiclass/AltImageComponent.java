package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("IMAGE")
public class AltImageComponent<String> extends AltInputControlComponent<String> {

	public AltImageComponent() {
		super(EnumHTMLControl.IMAGE);
		// TODO Auto-generated constructor stub
	}

}


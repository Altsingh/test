package com.alt.datacarrier.kernel.uiclass;

public abstract class AltInputControlComponent<T> extends AltAbstractBaseComponent<T> {

	private static final long serialVersionUID = 2523022906039252044L;

	private boolean autoComplete;
    
	private String webService;
    
	private boolean autoFocus;
    
	private String placeholder;
    
	private boolean readOnly;

    public  AltInputControlComponent(){

    }

    public AltInputControlComponent(EnumHTMLControl controlType) {
        super(controlType);
    }

    public boolean isAutoComplete() {
        return autoComplete;
    }

    public void setAutoComplete(boolean autoComplete) {
        this.autoComplete = autoComplete;
    }

    public String getWebService() {
        return webService;
    }

    public void setWebService(String webService) {
        this.webService = webService;
    }

    public boolean isAutoFocus() {
        return autoFocus;
    }

    public void setAutoFocus(boolean autoFocus) {
        this.autoFocus = autoFocus;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

}

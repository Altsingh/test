package com.alt.datacarrier.kernel.uiclass;

import com.alt.datacarrier.delegator.component.enumerator.EnumUiWrapType;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("TEXTAREA")
public class AltInputTextAreaComponent<T> extends AltInputControlComponent<T> {
	
	/**
	 * 
	 */
	private Integer cols;
	private Integer rows;
	private EnumUiWrapType wrap;
	
	public AltInputTextAreaComponent() {
		super(EnumHTMLControl.TEXTAREA);
		// TODO Auto-generated constructor stub
	}

	public Integer getCols() {
		return cols;
	}

	public void setCols(Integer cols) {
		this.cols = cols;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public EnumUiWrapType getWrap() {
		return wrap;
	}

	public void setWrap(EnumUiWrapType wrap) {
		this.wrap = wrap;
	}

	

}

package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("LABEL")
public class AltLabelComponent<String> extends AltInputControlComponent<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2944804565515345984L;

	public AltLabelComponent() {
		super(EnumHTMLControl.LABEL);
		// TODO Auto-generated constructor stub
	}

}

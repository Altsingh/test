package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("LINK")
public class AltLinkComponent extends AltAbstractComponent {

	private static final long serialVersionUID = 1L;

	public AltLinkComponent() {
		super(EnumHTMLControl.LINK);
	}

	private String linkName;

	private String linkUrl;

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

}

package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.List;

@JsonTypeName("MULTI_SELECT")
public class AltMultiSelectComponent<T> extends AltGroupComponent<T> {

    private String placeholder;
    private List<AltOption> options;
    private List<String> values;
    
    public AltMultiSelectComponent() {
        super(EnumHTMLControl.MULTI_SELECT);
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public List<AltOption> getOptions() {
        return options;
    }

    public void setOptions(List<AltOption> options) {
        this.options = options;
    }

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}
	    
}

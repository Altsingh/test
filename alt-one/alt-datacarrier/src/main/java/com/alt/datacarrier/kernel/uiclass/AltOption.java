
package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;

public class AltOption<T> implements Serializable, Comparable<T> {

	private boolean disabled;
	private String label;
	private T value;
	private boolean selected;

	public AltOption(Object label, T value) {
		if (label == null) {
			this.label = value.toString();
		} else {
			this.label = label.toString();
		}
		this.value = value;
	}

	public AltOption(String value) {
		this.value = (T) value;
		this.label = value;

	}

	public AltOption(boolean disabled, String label, T value, boolean selected) {
		this.disabled = disabled;
		this.label = label;
		this.value = value;
		this.selected = selected;
	}

	public AltOption() {
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	@Override
	public int compareTo(T option) {
		Object o1 = this.getValue();
		Object o2 = ((AltOption) option).getValue();
		if (o1 == null || o2 == null)
			return 0;
		return o1.toString().toLowerCase().compareTo(o2.toString().toLowerCase());
	}

}

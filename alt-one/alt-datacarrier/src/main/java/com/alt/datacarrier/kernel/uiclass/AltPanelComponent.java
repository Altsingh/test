package com.alt.datacarrier.kernel.uiclass;

import com.alt.datacarrier.common.enumeration.EnumSecurityType;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.List;

@JsonTypeName("PANEL")
public class AltPanelComponent extends AltContainerComponent {

    private static final long serialVersionUID = -4229103747761169629L;

    private List<AltButtonComponent> buttonList;

    private List<AltAbstractComponent> componentList;

    private AltPanelComponent(Builder builder) {
        setNamespace(builder.namespace);
        setId(builder.id);
        setLabel(builder.label);
        setName(builder.name);
        setCssClass(builder.cssClass);
        setInstruction(builder.instruction);
        setHelpText(builder.helpText);
        setControlType(builder.controlType);
        setRelation(builder.relation);
        setSecurityType(builder.securityType);
        buttonList = builder.buttonsGroups;
        componentList = builder.formGroups;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<AltButtonComponent> getButtonList() {
        return buttonList;
    }

    public List<AltAbstractComponent> getComponentList() {
        return componentList;
    }

    public AltPanelComponent() {
        super(EnumHTMLControl.PANEL);
    }

    public static final class Builder {
        private String namespace;
        private String id;
        private String label;
        private String name;
        private String cssClass;
        private String instruction;
        private String helpText;
        private EnumHTMLControl controlType;
        private List<AltRelation> relation;
        private EnumSecurityType securityType;
        private List<AltButtonComponent> buttonsGroups;
        private List<AltAbstractComponent> formGroups;

        public Builder() {
        }

        public Builder withNamespace(String val) {
            namespace = val;
            return this;
        }

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withLabel(String val) {
            label = val;
            return this;
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }

        public Builder withCssClass(String val) {
            cssClass = val;
            return this;
        }

        public Builder withInstruction(String val) {
            instruction = val;
            return this;
        }

        public Builder withHelpText(String val) {
            helpText = val;
            return this;
        }

        public Builder withControlType(EnumHTMLControl val) {
            controlType = val;
            return this;
        }

        public Builder withRelation(List<AltRelation> val) {
            relation = val;
            return this;
        }

        public Builder withSecurityType(EnumSecurityType val) {
            securityType = val;
            return this;
        }

        public Builder withButtonsGroups(List<AltButtonComponent> val) {
            buttonsGroups = val;
            return this;
        }

        public Builder withFormGroups(List<AltAbstractComponent> val) {
            formGroups = val;
            return this;
        }

        public AltPanelComponent build() {
            return new AltPanelComponent(this);
        }
    }
}

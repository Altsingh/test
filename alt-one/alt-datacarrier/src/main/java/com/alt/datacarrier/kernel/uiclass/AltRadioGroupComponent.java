package com.alt.datacarrier.kernel.uiclass;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("RADIO_GROUP")
public class AltRadioGroupComponent<T> extends AltAbstractBaseComponent<T> {
	
	/**
	 * 
	 */
	private String placeholder;
	private List<CheckboxSingleComponnet<Boolean>> options;
	
	public AltRadioGroupComponent() {
		super(EnumHTMLControl.RADIO_GROUP);
	}

	public String getPlaceholder() {
		return placeholder;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

	public List<CheckboxSingleComponnet<Boolean>> getOptions() {
		return options;
	}

	public void setOptions(List<CheckboxSingleComponnet<Boolean>> options) {
		this.options = options;
	}
	
}

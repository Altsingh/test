
package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class AltRelation implements Serializable{

	private static final long serialVersionUID = -2684556257276500850L;

	@JsonIgnore
	@JsonProperty("trigger")
    private String trigger;//ONSUBMIT,CLICK
    
	@JsonIgnore
	@JsonProperty("targetComponentList")
    private List<TargetComponent> targetComponentList;
	
    public String getTrigger() {
		return trigger;
	}
	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}
	public List<TargetComponent> getTargetComponentList() {
		return targetComponentList;
	}
	public void setTargetComponentList(List<TargetComponent> targetComponentList) {
		this.targetComponentList = targetComponentList;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((targetComponentList == null) ? 0 : targetComponentList.hashCode());
		result = prime * result + ((trigger == null) ? 0 : trigger.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AltRelation other = (AltRelation) obj;
		if (targetComponentList == null) {
			if (other.targetComponentList != null)
				return false;
		} else if (!targetComponentList.equals(other.targetComponentList))
			return false;
		if (trigger == null) {
			if (other.trigger != null)
				return false;
		} else if (!trigger.equals(other.trigger))
			return false;
		return true;
	}
    
    
    }

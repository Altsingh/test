package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("SLIDER")
public class AltSliderComponent<T> extends AltInputControlComponent<T> {

	/**
	 * 
	 */
	private String accept;
	private String min, max;
	
	public AltSliderComponent() {
		super(EnumHTMLControl.SLIDER);
		// TODO Auto-generated constructor stub
	}

	public String getAccept() {
		return accept;
	}

	public void setAccept(String accept) {
		this.accept = accept;
	}

	public String getMax() {
		return max;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	
	
}

package com.alt.datacarrier.kernel.uiclass;

import com.alt.datacarrier.common.enumeration.EnumSecurityType;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.List;

@JsonTypeName("TAB_PANEL")
public class AltTabbedPanelComponent extends AltContainerComponent {

    private static final long serialVersionUID = 1271955226967197018L;

    private List<AltPanelComponent> panels;

    public int getActivePanelIndex() {
        return activePanelIndex;
    }

    private int activePanelIndex;

    public AltTabbedPanelComponent() {
        super(EnumHTMLControl.TAB_PANEL);
    }

    private AltTabbedPanelComponent(Builder builder) {
        setNamespace(builder.namespace);
        setId(builder.id);
        setLabel(builder.label);
        setName(builder.name);
        setCssClass(builder.cssClass);
        setInstruction(builder.instruction);
        setHelpText(builder.helpText);
        setControlType(builder.controlType);
        setRelation(builder.relation);
        setSecurityType(builder.securityType);
        panels = builder.panels;
        activePanelIndex = builder.activePanelIndex;
    }

    public List<AltPanelComponent> getPanels() {
        return panels;
    }


    public static final class Builder {
        private String namespace;
        private String id;
        private String label;
        private String name;
        private String cssClass;
        private String instruction;
        private String helpText;
        private EnumHTMLControl controlType;
        private List<AltRelation> relation;
        private EnumSecurityType securityType;
        private List<AltPanelComponent> panels;
        private int activePanelIndex;

        public Builder() {
        }

        public Builder withNamespace(String val) {
            namespace = val;
            return this;
        }

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withLabel(String val) {
            label = val;
            return this;
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }

        public Builder withCssClass(String val) {
            cssClass = val;
            return this;
        }

        public Builder withInstruction(String val) {
            instruction = val;
            return this;
        }

        public Builder withHelpText(String val) {
            helpText = val;
            return this;
        }

        public Builder withControlType(EnumHTMLControl val) {
            controlType = val;
            return this;
        }

        public Builder withRelation(List<AltRelation> val) {
            relation = val;
            return this;
        }

        public Builder withSecurityType(EnumSecurityType val) {
            securityType = val;
            return this;
        }

        public Builder withPanels(List<AltPanelComponent> val) {
            panels = val;
            return this;
        }

        public Builder withActivePanelIndex(int val) {
            activePanelIndex = val;
            return this;
        }

        public AltTabbedPanelComponent build() {
            return new AltTabbedPanelComponent(this);
        }
    }
}

package com.alt.datacarrier.kernel.uiclass;

import com.alt.datacarrier.common.enumeration.EnumSecurityType;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.List;

@JsonTypeName("TEXTFIELD")
public class AltTextboxComponent<T> extends AltInputControlComponent<T> {

	private static final long serialVersionUID = -1183124133154201891L;

	private String accept;
	
	private EnumInputTextType inputType;

	public AltTextboxComponent() {
		super(EnumHTMLControl.TEXTFIELD);
	}

	private AltTextboxComponent(Builder builder) {
		setNamespace(builder.namespace);
		setId(builder.id);
		setLabel(builder.label);
		setName(builder.name);
		setCssClass(builder.cssClass);
		setInstruction(builder.instruction);
		setHelpText(builder.helpText);
		setControlType(builder.controlType);
		setRelation(builder.relation);
		setSecurityType(builder.securityType);
		setDbClassRead(builder.dbClassRead);
		setDbAttrRead(builder.dbAttrRead);
		setDbClassWrite(builder.dbClassWrite);
		setDbAttrWrite(builder.dbAttrWrite);
		setHint(builder.hint);
		setValue((T) builder.value);
		setValidation(builder.validation);
		setRequired(builder.required);
		setAutoComplete(builder.autoComplete);
		setWebService(builder.webService);
		setAutoFocus(builder.autoFocus);
		setPlaceholder(builder.placeholder);
		setReadOnly(builder.readOnly);
		setAccept(builder.accept);
		setInputType(builder.inputType);
	}

	public String getAccept() {
		return accept;
	}

	public void setAccept(String accept) {
		this.accept = accept;
	}

	public EnumInputTextType getInputType() {
		return inputType;
	}

	public void setInputType(EnumInputTextType inputType) {
		this.inputType = inputType;
	}


	public static final class Builder<T> {
		private String namespace;
		private String id;
		private String label;
		private String name;
		private String cssClass;
		private String instruction;
		private String helpText;
		private EnumHTMLControl controlType;
		private List<AltRelation> relation;
		private EnumSecurityType securityType;
		private String dbClassRead;
		private String dbAttrRead;
		private String dbClassWrite;
		private String dbAttrWrite;
		private String hint;
		private T value;
		private List<AltValidation<T>> validation;
		private boolean required;
		private boolean autoComplete;
		private String webService;
		private boolean autoFocus;
		private String placeholder;
		private boolean readOnly;
		private String accept;
		private EnumInputTextType inputType;

		public Builder() {
		}

		public Builder withNamespace(String val) {
			namespace = val;
			return this;
		}

		public Builder withId(String val) {
			id = val;
			return this;
		}

		public Builder withLabel(String val) {
			label = val;
			return this;
		}

		public Builder withName(String val) {
			name = val;
			return this;
		}

		public Builder withCssClass(String val) {
			cssClass = val;
			return this;
		}

		public Builder withInstruction(String val) {
			instruction = val;
			return this;
		}

		public Builder withHelpText(String val) {
			helpText = val;
			return this;
		}

		public Builder withControlType(EnumHTMLControl val) {
			controlType = val;
			return this;
		}

		public Builder withRelation(List<AltRelation> val) {
			relation = val;
			return this;
		}

		public Builder withSecurityType(EnumSecurityType val) {
			securityType = val;
			return this;
		}

		public Builder withDbClassRead(String val) {
			dbClassRead = val;
			return this;
		}

		public Builder withDbAttrRead(String val) {
			dbAttrRead = val;
			return this;
		}

		public Builder withDbClassWrite(String val) {
			dbClassWrite = val;
			return this;
		}

		public Builder withDbAttrWrite(String val) {
			dbAttrWrite = val;
			return this;
		}

		public Builder withHint(String val) {
			hint = val;
			return this;
		}

		public Builder withValue(T val) {
			value = val;
			return this;
		}

		public Builder withValidation(List<AltValidation<T>> val) {
			validation = val;
			return this;
		}

		public Builder withRequired(boolean val) {
			required = val;
			return this;
		}

		public Builder withAutoComplete(boolean val) {
			autoComplete = val;
			return this;
		}

		public Builder withWebService(String val) {
			webService = val;
			return this;
		}

		public Builder withAutoFocus(boolean val) {
			autoFocus = val;
			return this;
		}

		public Builder withPlaceholder(String val) {
			placeholder = val;
			return this;
		}

		public Builder withReadOnly(boolean val) {
			readOnly = val;
			return this;
		}

		public Builder withAccept(String val) {
			accept = val;
			return this;
		}

		public Builder withInputType(EnumInputTextType val) {
			inputType = val;
			return this;
		}

		public AltTextboxComponent build() {
			return new AltTextboxComponent(this);
		}
	}
}

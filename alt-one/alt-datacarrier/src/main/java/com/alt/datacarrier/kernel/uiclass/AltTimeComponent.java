package com.alt.datacarrier.kernel.uiclass;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("TIME")
public class AltTimeComponent<String> extends AltInputControlComponent<String> {

	public AltTimeComponent() {
		super(EnumHTMLControl.TIME);
		// TODO Auto-generated constructor stub
	}

}

package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;

public class AltValidation<T> implements Serializable{

	private static final long serialVersionUID = 4538518087490660819L;

	private T value;
	
	private String errorMessage;

	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	
}

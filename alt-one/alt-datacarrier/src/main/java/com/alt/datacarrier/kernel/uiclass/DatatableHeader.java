package com.alt.datacarrier.kernel.uiclass;

import java.util.List;

public class DatatableHeader {
	
	private String header;
	private List<String> cssClass;
	
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public List<String> getCssClass() {
		return cssClass;
	}
	public void setCssClass(List<String> cssClass) {
		this.cssClass = cssClass;
	}
	
	
}

package com.alt.datacarrier.kernel.uiclass;

import java.util.Map;

public class DatatableRecord {
	
	private boolean active;
	private Map<DatatableHeader,Object> rowData;
	
	public Map<DatatableHeader, Object> getRowData() {
		return rowData;
	}

	public void setRowData(Map<DatatableHeader, Object> rowData) {
		this.rowData = rowData;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}

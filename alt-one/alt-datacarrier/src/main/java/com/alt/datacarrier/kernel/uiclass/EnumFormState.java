package com.alt.datacarrier.kernel.uiclass;

public enum EnumFormState{
    DRAFT, PUBLISH, OBSOLETE
}

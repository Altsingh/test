package com.alt.datacarrier.kernel.uiclass;

public enum EnumInputTextType {
	
	COLOR("color"),
	DATETIME("datetime"),
	DATETIME_LOCAL("datetime-local"),
	EMAIL("email"),
	FILE("file"),
	MONTH("month"),
	NUMBER("number"),
	PASSWORD("password"),
	RANGE("range"),
	SEARCH("search"),
	TEL("tel"),
	TEXT("text"),
	TIME("time"),
	URL("url"),
	WEEK("week");
	
	private String inputType;
	
	private EnumInputTextType(String inputType) {
		this.inputType = inputType;
	}
	
	public String getInputType() {
		return inputType;
	}
}

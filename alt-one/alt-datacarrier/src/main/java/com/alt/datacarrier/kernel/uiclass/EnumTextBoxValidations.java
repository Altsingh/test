package com.alt.datacarrier.kernel.uiclass;

public enum EnumTextBoxValidations {
    REQUIRED("REQUIRED");

    private String inputType;

    private EnumTextBoxValidations(String inputType) {
        this.inputType = inputType;
    }

    public String getInputType() {
        return inputType;
    }
}

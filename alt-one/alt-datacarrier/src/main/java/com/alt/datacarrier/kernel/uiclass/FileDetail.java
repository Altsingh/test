package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FileDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fileName;

	private String filePath;

	private Integer fileSizeBytes;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Integer getFileSizeBytes() {
		return fileSizeBytes;
	}

	public void setFileSizeBytes(Integer fileSizeBytes) {
		this.fileSizeBytes = fileSizeBytes;
	}

}

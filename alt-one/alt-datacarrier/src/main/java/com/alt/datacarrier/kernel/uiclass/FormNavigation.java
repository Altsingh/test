package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FormNavigation implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer formId;

	private String sourceComponentName;

	private String sourceComponentValue;

	private String targetComponentName;

	public Integer getFormId() {
		return formId;
	}

	public void setFormId(Integer formId) {
		this.formId = formId;
	}

	public String getSourceComponentName() {
		return sourceComponentName;
	}

	public void setSourceComponentName(String sourceComponentName) {
		this.sourceComponentName = sourceComponentName;
	}

	public String getSourceComponentValue() {
		return sourceComponentValue;
	}

	public void setSourceComponentValue(String sourceComponentValue) {
		this.sourceComponentValue = sourceComponentValue;
	}

	public String getTargetComponentName() {
		return targetComponentName;
	}

	public void setTargetComponentName(String targetComponentName) {
		this.targetComponentName = targetComponentName;
	}
}

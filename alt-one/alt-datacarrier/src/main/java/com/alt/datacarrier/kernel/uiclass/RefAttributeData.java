package com.alt.datacarrier.kernel.uiclass;

public class RefAttributeData {
	
	private boolean reference;
	private String attribute;
	private String refClass;
	private String dbAttribute;
	
	public boolean isReference() {
		return reference;
	}
	public void setReference(boolean reference) {
		this.reference = reference;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getRefClass() {
		return refClass;
	}
	public void setRefClass(String refClass) {
		this.refClass = refClass;
	}
	public String getDbAttribute() {
		return dbAttribute;
	}
	public void setDbAttribute(String dbAttribute) {
		this.dbAttribute = dbAttribute;
	}
	
	

}

package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class TargetComponent implements Serializable{

	private static final long serialVersionUID = -1942886754955198993L;

	@JsonIgnore
	@JsonProperty("targetComponent")
	private String targetComponent;
	
	@JsonIgnore
	@JsonProperty("events")
	private List<AltEvent> events;
	
	public String getTargetComponent() {
		return targetComponent;
	}
	public void setTargetComponent(String targetComponent) {
		this.targetComponent = targetComponent;
	}
	public List<AltEvent> getEvents() {
		return events;
	}
	public void setEvents(List<AltEvent> events) {
		this.events = events;
	}
	
	
	
}

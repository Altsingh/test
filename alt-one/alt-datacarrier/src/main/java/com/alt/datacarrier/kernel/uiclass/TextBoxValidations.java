package com.alt.datacarrier.kernel.uiclass;

public class TextBoxValidations extends  AltValidation {

    private EnumTextBoxValidations validations;

    public EnumTextBoxValidations getValidations() {
        return validations;
    }

    public void setValidations(EnumTextBoxValidations validations) {
        this.validations = validations;
    }
}

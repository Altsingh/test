package com.alt.datacarrier.kernel.uiclass;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


public class WebService implements Serializable{
	
	private static final long serialVersionUID = 376839904037066155L;

	@JsonIgnore
	@JsonProperty("type")
	private WebServiceType type;
	
	@JsonIgnore
	@JsonProperty("url")
	private String url;
	
	@JsonIgnore
	@JsonProperty("webServiceParam")
	private Map<String,String> webServiceParam;
	
	public WebServiceType getType() {
		return type;
	}
	
	public void setType(WebServiceType type) {
		this.type = type;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public Map<String, String> getWebServiceParam() {
		return webServiceParam;
	}
	
	public void setWebServiceParam(Map<String, String> webServiceParam) {
		this.webServiceParam = webServiceParam;
	}
	
}

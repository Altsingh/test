package com.alt.datacarrier.marketplace.core;

import com.alt.datacarrier.marketplace.enums.EnumBannerStatus;
import com.alt.datacarrier.marketplace.enums.EnumBannerType;

public class BannerEntity {
    private String imageBanner;
    private String imageThumbnail;
    private EnumBannerType type;
    private EnumBannerStatus status;
    private String link;
    private int bannerId;

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    private int sequence;

    public BannerEntity(){}

    public void setBannerId(int bannerId) {
        this.bannerId = bannerId;
    }

    private BannerEntity(Builder builder) {
        imageBanner = builder.imageBanner;
        imageThumbnail = builder.imageThumbnail;
        type = builder.type;
        status = builder.status;
        link = builder.link;
        bannerId = builder.bannerId;
        sequence = builder.sequence;
    }

    public void setImageBanner(String imageBanner) {
        this.imageBanner = imageBanner;
    }

    public void setImageThumbnail(String imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }

    public String getImageBanner() {
        return imageBanner;
    }

    public String getImageThumbnail() {
        return imageThumbnail;
    }

    public EnumBannerType getType() {
        return type;
    }

    public EnumBannerStatus getStatus() {
        return status;
    }

    public String getLink() {
        return link;
    }

    public int getBannerId() {
        return bannerId;
    }

    public int getSequence() {
        return sequence;
    }

    public static final class Builder {
        private String imageBanner;
        private String imageThumbnail;
        private EnumBannerType type;
        private EnumBannerStatus status;
        private String link;
        private int bannerId;
        private int sequence;

        public Builder() {
        }

        public Builder withImageBanner(String val) {
            imageBanner = val;
            return this;
        }

        public Builder withImageThumbnail(String val) {
            imageThumbnail = val;
            return this;
        }

        public Builder withType(EnumBannerType val) {
            type = val;
            return this;
        }

        public Builder withStatus(EnumBannerStatus val) {
            status = val;
            return this;
        }

        public Builder withLink(String val) {
            link = val;
            return this;
        }

        public Builder withBannerId(int val) {
            bannerId = val;
            return this;
        }

        public Builder withSequence(int val) {
            sequence = val;
            return this;
        }

        public BannerEntity build() {
            return new BannerEntity(this);
        }
    }
}

package com.alt.datacarrier.marketplace.core;

public class Category {

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	private String categoryId;
	
	private String categoryName;

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	private String sequence;

	public Category() {
	}

	public String getCategoryId() {
		return categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public String getSequence() {
		return sequence;
	}
	
	public Category(Builder builder) {
		this.categoryId = builder.categoryId;
		this.categoryName = builder.categoryName;
		this.sequence = builder.sequence;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		
		private String categoryId;
		
		private String categoryName;
		
		private String sequence;

		public Builder() {
		}

		public Builder withCategoryId(String categoryId) {
			this.categoryId = categoryId;
			return this;
		}

		public Builder withCategoryName(String categoryName) {
			this.categoryName = categoryName;
			return this;
		}
		
		public Builder withSequence(String sequence) {
			this.sequence = sequence;
			return this;
		}

		public Category build() {
			return new Category(this);
		}
	}
	
}

package com.alt.datacarrier.marketplace.core;

import java.util.List;

public class CategoryAppDetail {

	private Category category;
	
	private List<SubCategoryAppDetail> subCategories;

	public Category getCategory() {
		return category;
	}

	public List<SubCategoryAppDetail> getSubCategories() {
		return subCategories;
	}

	private CategoryAppDetail(Builder builder) {
		this.category = builder.category;
		this.subCategories = builder.subCategories;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		
		private Category category;
		
		private List<SubCategoryAppDetail> subCategories;

		private Builder() {
		}

		public Builder withCategory(Category category) {
			this.category = category;
			return this;
		}

		public Builder withSubCategories(List<SubCategoryAppDetail> subCategories) {
			this.subCategories = subCategories;
			return this;
		}

		public CategoryAppDetail build() {
			return new CategoryAppDetail(this);
		}
	}
		
}
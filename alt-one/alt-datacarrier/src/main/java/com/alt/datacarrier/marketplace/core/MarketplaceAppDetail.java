package com.alt.datacarrier.marketplace.core;

import java.util.List;

/**
 * @author navjot.s
 *
 */
public class MarketplaceAppDetail {

	private Integer appId;
	
	private String appName;
	
	private String company;

	private String partnerId;
	
	private String rating;
	
	private String launchDate;

	private String description;
	
	private List<String> screenShots;

	private String appLogo;

	private String categoryId;
	
	private String subCategory;
	
	private String websiteUrl;
	
	private String videoUrl;
	
	private String appUrl;
	
	private String shortDescription;
	
	private String headerCoverImage;
	
	private String previewBanner;
	
	private String sequence;
	
	public MarketplaceAppDetail(){
		
	}

	private MarketplaceAppDetail(Builder builder) {
		setAppId(builder.appId);
		appName = builder.appName;
		company = builder.company;
		partnerId = builder.partnerId;
		rating = builder.rating;
		launchDate = builder.launchDate;
		description = builder.description;
		screenShots = builder.screenShots;
		setAppLogo(builder.appLogo);
		categoryId = builder.categoryId;
		subCategory = builder.subCategory;
		websiteUrl = builder.websiteUrl;
		videoUrl = builder.videoUrl;
		appUrl = builder.appUrl;
		shortDescription = builder.shortDescription;
		setHeaderCoverImage(builder.headerCoverImage);
		setPreviewBanner(builder.previewBanner);
		sequence = builder.sequence;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public Integer getAppId() {
		return appId;
	}
	
	public String getPartnerId() {
		return partnerId;
	}

	public void setAppId(Integer appId) {
		this.appId = appId;
	}

	public String getAppName() {
		return appName;
	}

	public String getCompany() {
		return company;
	}

	public String getRating() {
		return rating;
	}

	public String getLaunchDate() {
		return launchDate;
	}

	public String getDescription() {
		return description;
	}

	public List<String> getScreenShots() {
		return screenShots;
	}

	public String getAppLogo() {
		return appLogo;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public String getHeaderCoverImage() {
		return headerCoverImage;
	}

	public String getPreviewBanner() {
		return previewBanner;
	}

	public String getSequence() {
		return sequence;
	}

	public void setAppLogo(String appLogo) {
		this.appLogo = appLogo;
	}

	public void setHeaderCoverImage(String headerCoverImage) {
		this.headerCoverImage = headerCoverImage;
	}

	public void setPreviewBanner(String previewBanner) {
		this.previewBanner = previewBanner;
	}
	
	public void setScreenshots(List<String> screenShots) {
		this.screenShots = screenShots;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private Integer appId;
		private String appName;
		private String company;
		private String partnerId;
		private String rating;
		private String launchDate;
		private String description;
		private List<String> screenShots;
		private String appLogo;
		private String categoryId;
		private String subCategory;
		private String websiteUrl;
		private String videoUrl;
		private String appUrl;
		private String shortDescription;
		private String headerCoverImage;
		private String previewBanner;
		private String sequence;

		public Builder() {
		}

		public Builder withAppId(Integer val) {
			appId = val;
			return this;
		}

		public Builder withAppName(String val) {
			appName = val;
			return this;
		}

		public Builder withCompany(String val) {
			company = val;
			return this;
		}

		public Builder withPartnerId(String val) {
			partnerId = val;
			return this;
		}

		public Builder withRating(String val) {
			rating = val;
			return this;
		}

		public Builder withLaunchDate(String val) {
			launchDate = val;
			return this;
		}

		public Builder withDescription(String val) {
			description = val;
			return this;
		}

		public Builder withScreenShots(List<String> val) {
			screenShots = val;
			return this;
		}

		public Builder withAppLogo(String val) {
			appLogo = val;
			return this;
		}

		public Builder withCategoryId(String val) {
			categoryId = val;
			return this;
		}

		public Builder withSubCategory(String val) {
			subCategory = val;
			return this;
		}

		public Builder withWebsiteUrl(String val) {
			websiteUrl = val;
			return this;
		}

		public Builder withVideoUrl(String val) {
			videoUrl = val;
			return this;
		}

		public Builder withAppUrl(String val) {
			appUrl = val;
			return this;
		}

		public Builder withShortDescription(String val) {
			shortDescription = val;
			return this;
		}

		public Builder withHeaderCoverImage(String val) {
			headerCoverImage = val;
			return this;
		}

		public Builder withPreviewBanner(String val) {
			previewBanner = val;
			return this;
		}

		public Builder withSequence(String val) {
			sequence = val;
			return this;
		}
		
		public MarketplaceAppDetail build() {
			return new MarketplaceAppDetail(this);
		}
	}
	
}

package com.alt.datacarrier.marketplace.core;

public class SubCategory {

	private String subCategoryId;
	
	private String subCategoryName;

	private String sequence;
	
	public String getSubCategoryId() {
		return subCategoryId;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public String getSequence() {
		return sequence;
	}

	public SubCategory(){}

	public SubCategory(Builder builder) {
		this.subCategoryId = builder.subCategoryId;
		this.subCategoryName = builder.subCategoryName;
		this.sequence = builder.sequence;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		
		private String subCategoryId;
		
		private String subCategoryName;
		
		private String sequence;

		public Builder() {
		}

		public Builder withSubCategoryId(String subCategoryId) {
			this.subCategoryId = subCategoryId;
			return this;
		}

		public Builder withSubCategoryName(String subCategoryName) {
			this.subCategoryName = subCategoryName;
			return this;
		}
		
		public Builder withSequence(String sequence) {
			this.sequence = sequence;
			return this;
		}

		public SubCategory build() {
			return new SubCategory(this);
		}
	}
	
}

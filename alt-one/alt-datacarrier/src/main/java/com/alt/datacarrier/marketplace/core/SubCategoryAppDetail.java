package com.alt.datacarrier.marketplace.core;

import java.util.List;

public class SubCategoryAppDetail {

	private SubCategory subCategory;
	
	private List<MarketplaceAppDetail> apps;

	public SubCategory getSubCategory() {
		return subCategory;
	}

	public List<MarketplaceAppDetail> getApps() {
		return apps;
	}

	private SubCategoryAppDetail(Builder builder) {
		this.subCategory = builder.subCategory;
		this.apps = builder.apps;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		
		private SubCategory subCategory;
		
		private List<MarketplaceAppDetail> apps;

		private Builder() {
		}

		public Builder withSubCategory(SubCategory subCategory) {
			this.subCategory = subCategory;
			return this;
		}

		public Builder withApp(List<MarketplaceAppDetail> apps) {
			this.apps = apps;
			return this;
		}

		public SubCategoryAppDetail build() {
			return new SubCategoryAppDetail(this);
		}
	}
		
}
package com.alt.datacarrier.marketplace.request;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.marketplace.core.MarketplaceAppDetail;

public class MarketPlaceCERequest {
	
	private EnumCRUDOperations action;

	private MarketplaceAppDetail marketplaceAppDetails;	
	
	public EnumCRUDOperations getAction() {
		return action;
	}

	public void setAction(EnumCRUDOperations action) {
		this.action = action;
	}

	public MarketplaceAppDetail getMarketplaceAppDetails() {
		return marketplaceAppDetails;
	}

	public void setMarketplaceAppDetails(MarketplaceAppDetail marketplaceAppDetails) {
		this.marketplaceAppDetails = marketplaceAppDetails;
	}

	
	
}

package com.alt.datacarrier.marketplace.request;

public class UserEnquiryDetails {

	private String organizationName;

	private String firstName;

	private String lastName;

	private String email;

	private String howDidYouHearText;

	private String mobileNumber;

	private String referredBy;

	private int employeeCount;	
	
	private String appName;
	  
	
	private UserEnquiryDetails(Builder builder){
		setOrganizationName(builder.organizationName);
		setFirstName(builder.firstName);
		setLastName(builder.lastName);
		setEmail(builder.email);
		setEmployeeCount(builder.employeeCount);
		setHowDidYouHearText(builder.howDidYouHearText);
		setMobileNumber(builder.mobileNumber);
		setReferredBy(builder.referredBy);
		setAppName(builder.appName);
	}
	

	private UserEnquiryDetails(){
		
	}
	
	public String getAppName() {
		return appName;
	}
	
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHowDidYouHearText() {
		return howDidYouHearText;
	}

	public void setHowDidYouHearText(String howDidYouHearText) {
		this.howDidYouHearText = howDidYouHearText;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(String referredBy) {
		this.referredBy = referredBy;
	}

	public int getEmployeeCount() {
		return employeeCount;
	}

	public void setEmployeeCount(int employeeCount) {
		this.employeeCount = employeeCount;
	}

	public static class Builder {
		private String organizationName;

		private String firstName;

		private String lastName;

		private String email;

		private String howDidYouHearText;

		private String mobileNumber;

		private String referredBy;

		private int employeeCount;
		
		private String appName;

		public Builder() {

		}

		public Builder organizationName(String val) {
			organizationName = val;
			return this;

		}

		public Builder firstName(String val) {
			firstName = val;
			return this;

		}

		public Builder lastName(String val) {
			lastName = val;
			return this;

		}

		public Builder email(String val) {
			email = val;
			return this;

		}

		public Builder howDidYouHearText(String val) {
			howDidYouHearText = val;
			return this;

		}

		public Builder mobileNumber(String val) {
			mobileNumber = val;
			return this;

		}

		public Builder referredBy(String val) {
			referredBy = val;
			return this;

		}

		public Builder employeeCount(int val) {
			employeeCount = val;
			return this;

		}
		
		public Builder appName(String val) {
			appName = val;
			return this;

		}
		
		public UserEnquiryDetails build(){
			return new UserEnquiryDetails(this);
		}

	}
}

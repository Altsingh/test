package com.alt.datacarrier.message.framework.common;

import java.io.Serializable;

import com.alt.datacarrier.core.IRequestData;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;

public class KafkaMessage implements IRequestData,Serializable{

	private static final long serialVersionUID = -211149760642405525L;

	private String topic;
	
	private String documentCode;
	
	private EnumRequestType type;
	
	private EnumCRUDOperations operation;

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public EnumRequestType getType() {
		return type;
	}

	public void setType(EnumRequestType type) {
		this.type = type;
	}

	public EnumCRUDOperations getOperation() {
		return operation;
	}

	public void setOperation(EnumCRUDOperations operation) {
		this.operation = operation;
	}
	
}

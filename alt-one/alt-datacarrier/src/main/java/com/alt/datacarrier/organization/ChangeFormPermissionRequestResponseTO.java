package com.alt.datacarrier.organization;

public class ChangeFormPermissionRequestResponseTO {

	private Integer organizationId;

	private String username;

	private FormRolePermissionTO permission;

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public FormRolePermissionTO getPermission() {
		return permission;
	}

	public void setPermission(FormRolePermissionTO permission) {
		this.permission = permission;
	}

}

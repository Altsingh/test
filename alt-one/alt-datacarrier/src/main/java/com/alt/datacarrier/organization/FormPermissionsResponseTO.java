package com.alt.datacarrier.organization;

import java.util.List;

public class FormPermissionsResponseTO {

	private List<FormRolePermissionTO> permissions;

	public List<FormRolePermissionTO> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<FormRolePermissionTO> permissions) {
		this.permissions = permissions;
	}

}

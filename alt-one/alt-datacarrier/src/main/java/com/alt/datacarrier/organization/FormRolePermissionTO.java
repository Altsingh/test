package com.alt.datacarrier.organization;

public class FormRolePermissionTO {

	private Long hrFormRolePermissionId;

	private Long hrFormId;

	private Integer roleId;

	private Boolean webAccessible;

	public Long getHrFormRolePermissionId() {
		return hrFormRolePermissionId;
	}

	public void setHrFormRolePermissionId(Long hrFormRolePermissionId) {
		this.hrFormRolePermissionId = hrFormRolePermissionId;
	}

	public Long getHrFormId() {
		return hrFormId;
	}

	public void setHrFormId(Long hrFormId) {
		this.hrFormId = hrFormId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Boolean getWebAccessible() {
		return webAccessible;
	}

	public void setWebAccessible(Boolean webAccessible) {
		this.webAccessible = webAccessible;
	}

}

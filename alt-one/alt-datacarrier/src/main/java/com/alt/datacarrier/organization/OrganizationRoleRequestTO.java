package com.alt.datacarrier.organization;

public class OrganizationRoleRequestTO {

	private Integer organizationId;

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

}

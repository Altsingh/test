package com.alt.datacarrier.organization;

import java.util.List;

public class OrganizationRoleResponseTO {

	private List<RoleTO> roles;

	public List<RoleTO> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleTO> roles) {
		this.roles = roles;
	}

}

package com.alt.datacarrier.search;

import java.util.List;
import java.util.Map;

import com.alt.datacarrier.search.SearchConstants.EnumSearchRequestType;

public class SearchRequest {

	private String searchString;	
	
	private List<String> queryFields;	

	private List<String> filterList;
	
	private List<Map<String,String>> filterQuery;
	
	private int start;		

	private int rows;	
	
	private EnumSearchRequestType searchRequestType;
	
	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	
	public List<String> getQueryFields() {
		return queryFields;
	}

	public void setQueryFields(List<String> queryFields) {
		this.queryFields = queryFields;
	}
	
	public List<String> getFilterList() {
		return filterList;
	}

	public void setFilterList(List<String> filterList) {
		this.filterList = filterList;
	}
	
	public List<Map<String, String>> getFilterQuery() {
		return filterQuery;
	}

	public void setFilterQuery(List<Map<String, String>> filterQuery) {
		this.filterQuery = filterQuery;
	}
	
	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
	
	public EnumSearchRequestType getSearchRequestType() {
		return searchRequestType;
	}

	public void setSearchRequestType(EnumSearchRequestType searchRequestType) {
		this.searchRequestType = searchRequestType;
	}
}

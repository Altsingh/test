package com.alt.datacarrier.workflow;

public class ActionResponseTO {

	private Integer actionId;

	private Long hrFormFieldId;

	private String formFieldName;

	private String ruleMethod;

	private String ruleClass;
	
	private String methodParameters;
	
	private String actionType;
	
	public Integer getActionId() {
		return actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	public Long getHrFormFieldId() {
		return hrFormFieldId;
	}

	public void setHrFormFieldId(Long hrFormFieldId) {
		this.hrFormFieldId = hrFormFieldId;
	}

	public String getFormFieldName() {
		return formFieldName;
	}

	public void setFormFieldName(String formFieldName) {
		this.formFieldName = formFieldName;
	}

	public String getRuleMethod() {
		return ruleMethod;
	}

	public void setRuleMethod(String ruleMethod) {
		this.ruleMethod = ruleMethod;
	}

	public String getRuleClass() {
		return ruleClass;
	}

	public void setRuleClass(String ruleClass) {
		this.ruleClass = ruleClass;
	}

	public String getMethodParameters() {
		return methodParameters;
	}

	public void setMethodParameters(String methodParameters) {
		this.methodParameters = methodParameters;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	
}

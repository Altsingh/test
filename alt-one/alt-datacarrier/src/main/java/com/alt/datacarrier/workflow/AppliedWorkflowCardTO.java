package com.alt.datacarrier.workflow;

import java.util.List;

public class AppliedWorkflowCardTO {

	private Integer appliedTaskCount;

	private List<TaskTO> appliedTasks;

	private List<TaskGroupTO> appliedTaskGroups;

	public Integer getAppliedTaskCount() {
		return appliedTaskCount;
	}

	public void setAppliedTaskCount(Integer appliedTaskCount) {
		this.appliedTaskCount = appliedTaskCount;
	}

	public List<TaskTO> getAppliedTasks() {
		return appliedTasks;
	}

	public void setAppliedTasks(List<TaskTO> appliedTasks) {
		this.appliedTasks = appliedTasks;
	}

	public List<TaskGroupTO> getAppliedTaskGroups() {
		return appliedTaskGroups;
	}

	public void setAppliedTaskGroups(List<TaskGroupTO> appliedTaskGroups) {
		this.appliedTaskGroups = appliedTaskGroups;
	}
	
}

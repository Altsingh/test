package com.alt.datacarrier.workflow;

import java.util.List;

public class ApprovalWorkflowCardTO {

	private Integer approvalTaskCount;

	private List<TaskTO> approvalTasks;

	public Integer getApprovalTaskCount() {
		return approvalTaskCount;
	}

	public void setApprovalTaskCount(Integer approvalTaskCount) {
		this.approvalTaskCount = approvalTaskCount;
	}

	public List<TaskTO> getApprovalTasks() {
		return approvalTasks;
	}

	public void setApprovalTasks(List<TaskTO> approvalTasks) {
		this.approvalTasks = approvalTasks;
	}

}

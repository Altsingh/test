package com.alt.datacarrier.workflow;

public class RoleActorTO {

	private Integer actorId;

	private Integer actorSla;

	private Integer actorRoleId;

	private String roleName;

	private String actorEmail;

	private Integer organizationRoleId;

	private Boolean primaryActor;

	public Integer getActorId() {
		return actorId;
	}

	public void setActorId(Integer actorId) {
		this.actorId = actorId;
	}

	public Integer getActorSla() {
		return actorSla;
	}

	public void setActorSla(Integer actorSla) {
		this.actorSla = actorSla;
	}

	public Integer getActorRoleId() {
		return actorRoleId;
	}

	public void setActorRoleId(Integer actorRoleId) {
		this.actorRoleId = actorRoleId;
	}

	public Integer getOrganizationRoleId() {
		return organizationRoleId;
	}

	public void setOrganizationRoleId(Integer organizationRoleId) {
		this.organizationRoleId = organizationRoleId;
	}

	public Boolean getPrimaryActor() {
		return primaryActor;
	}

	public void setPrimaryActor(Boolean primaryActor) {
		this.primaryActor = primaryActor;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getActorEmail() {
		return actorEmail;
	}

	public void setActorEmail(String actorEmail) {
		this.actorEmail = actorEmail;
	}

}

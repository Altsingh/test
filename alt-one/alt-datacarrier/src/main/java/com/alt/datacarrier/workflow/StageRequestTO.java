package com.alt.datacarrier.workflow;

public class StageRequestTO {

	private Long hrFormId;

	private Integer stageId;

	private Boolean customFlag;

	private String workflowType;

	private ProtocolTO protocolTO;

	public Long getHrFormId() {
		return hrFormId;
	}

	public void setHrFormId(Long hrFormId) {
		this.hrFormId = hrFormId;
	}

	public Integer getStageId() {
		return stageId;
	}

	public void setStageId(Integer stageId) {
		this.stageId = stageId;
	}

	public Boolean getCustomFlag() {
		return customFlag;
	}

	public void setCustomFlag(Boolean customFlag) {
		this.customFlag = customFlag;
	}

	public String getWorkflowType() {
		return workflowType;
	}

	public void setWorkflowType(String workflowType) {
		this.workflowType = workflowType;
	}

	public ProtocolTO getProtocolTO() {
		return protocolTO;
	}

	public void setProtocolTO(ProtocolTO protocolTO) {
		this.protocolTO = protocolTO;
	}
}

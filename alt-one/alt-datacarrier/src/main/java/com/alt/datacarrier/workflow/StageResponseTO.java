package com.alt.datacarrier.workflow;

import java.util.List;

public class StageResponseTO extends WorkflowResponseTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<ActionResponseTO> actions;

	private Integer workflowStageId;

	public List<ActionResponseTO> getActions() {
		return actions;
	}

	public void setActions(List<ActionResponseTO> actions) {
		this.actions = actions;
	}

	public Integer getWorkflowStageId() {
		return workflowStageId;
	}

	public void setWorkflowStageId(Integer workflowStageId) {
		this.workflowStageId = workflowStageId;
	}

}

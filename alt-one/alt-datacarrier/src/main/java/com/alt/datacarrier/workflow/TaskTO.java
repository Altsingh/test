package com.alt.datacarrier.workflow;

import java.util.Date;
import java.util.List;

public class TaskTO {

	private int actorID;

	private String actionDate;

	private Date actionDateType;

	private String actorName;

	private String defaultClassCall;

	private int delegateUserID;

	private int entityId;

	private String employeeCode;

	private String employeeName;

	private int employeeId;

	private int formId;

	private String formName;

	private int groupId;

	private List<TaskTO> groupList;

	private long instanceID;

	private int instanceUserId;

	private int moduleId;

	private String moduleName;

	private String photoPath;

	private String resignationCode;

	private String resignationStatus;

	private int roleId;

	private String roleName;

	private String serialNumber;

	private String statusMessage;

	private long sysUserWorkflowHistoryId;

	private String startDate;

	private Date startDateType;

	private int stageId;

	private int sysworkflowstageTypeID;

	private Long taskNumber;

	private String stageType;

	private String stageName;

	private String url;

	private String workflowType;

	private String workflowstageName;

	public int getActorID() {
		return actorID;
	}

	public void setActorID(int actorID) {
		this.actorID = actorID;
	}

	public String getActionDate() {
		return actionDate;
	}

	public void setActionDate(String actionDate) {
		this.actionDate = actionDate;
	}

	public Date getActionDateType() {
		return actionDateType;
	}

	public void setActionDateType(Date actionDateType) {
		this.actionDateType = actionDateType;
	}

	public String getActorName() {
		return actorName;
	}

	public void setActorName(String actorName) {
		this.actorName = actorName;
	}

	public String getDefaultClassCall() {
		return defaultClassCall;
	}

	public void setDefaultClassCall(String defaultClassCall) {
		this.defaultClassCall = defaultClassCall;
	}

	public int getDelegateUserID() {
		return delegateUserID;
	}

	public void setDelegateUserID(int delegateUserID) {
		this.delegateUserID = delegateUserID;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public List<TaskTO> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<TaskTO> groupList) {
		this.groupList = groupList;
	}

	public long getInstanceID() {
		return instanceID;
	}

	public void setInstanceID(long instanceID) {
		this.instanceID = instanceID;
	}

	public int getInstanceUserId() {
		return instanceUserId;
	}

	public void setInstanceUserId(int instanceUserId) {
		this.instanceUserId = instanceUserId;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getResignationCode() {
		return resignationCode;
	}

	public void setResignationCode(String resignationCode) {
		this.resignationCode = resignationCode;
	}

	public String getResignationStatus() {
		return resignationStatus;
	}

	public void setResignationStatus(String resignationStatus) {
		this.resignationStatus = resignationStatus;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public long getSysUserWorkflowHistoryId() {
		return sysUserWorkflowHistoryId;
	}

	public void setSysUserWorkflowHistoryId(long sysUserWorkflowHistoryId) {
		this.sysUserWorkflowHistoryId = sysUserWorkflowHistoryId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public Date getStartDateType() {
		return startDateType;
	}

	public void setStartDateType(Date startDateType) {
		this.startDateType = startDateType;
	}

	public int getStageId() {
		return stageId;
	}

	public void setStageId(int stageId) {
		this.stageId = stageId;
	}

	public int getSysworkflowstageTypeID() {
		return sysworkflowstageTypeID;
	}

	public void setSysworkflowstageTypeID(int sysworkflowstageTypeID) {
		this.sysworkflowstageTypeID = sysworkflowstageTypeID;
	}

	public Long getTaskNumber() {
		return taskNumber;
	}

	public void setTaskNumber(Long taskNumber) {
		this.taskNumber = taskNumber;
	}

	public String getStageType() {
		return stageType;
	}

	public void setStageType(String stageType) {
		this.stageType = stageType;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getWorkflowType() {
		return workflowType;
	}

	public void setWorkflowType(String workflowType) {
		this.workflowType = workflowType;
	}

	public String getWorkflowstageName() {
		return workflowstageName;
	}

	public void setWorkflowstageName(String workflowstageName) {
		this.workflowstageName = workflowstageName;
	}

}

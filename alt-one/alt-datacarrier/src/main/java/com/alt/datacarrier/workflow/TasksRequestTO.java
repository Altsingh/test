package com.alt.datacarrier.workflow;

import java.util.List;

public class TasksRequestTO {

	private Integer organizationId;

	private String username;

	private List<Integer> contextEntityIds;

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Integer> getContextEntityIds() {
		return contextEntityIds;
	}

	public void setContextEntityIds(List<Integer> contextEntityIds) {
		this.contextEntityIds = contextEntityIds;
	}
	
}

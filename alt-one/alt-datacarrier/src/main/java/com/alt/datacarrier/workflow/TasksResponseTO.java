package com.alt.datacarrier.workflow;

import java.util.List;

public class TasksResponseTO {

	private List<TaskTO> tasks;

	public List<TaskTO> getTasks() {
		return tasks;
	}

	public void setTasks(List<TaskTO> tasks) {
		this.tasks = tasks;
	}

}

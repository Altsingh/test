package com.alt.datacarrier.workflow;

public class TransitionRequestTO {
	private Integer stageActionId;

	private String ruleOutcome;

	private String entityName;

	private Integer employeeId;

	private Object actionInfoTO;

	private Object methodParamMap;

	private Integer userID;

	private ProtocolTO protocolTO;

	public Integer getStageActionId() {
		return stageActionId;
	}

	public void setStageActionId(Integer stageActionId) {
		this.stageActionId = stageActionId;
	}

	public String getRuleOutcome() {
		return ruleOutcome;
	}

	public void setRuleOutcome(String ruleOutcome) {
		this.ruleOutcome = ruleOutcome;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Object getActionInfoTO() {
		return actionInfoTO;
	}

	public void setActionInfoTO(Object actionInfoTO) {
		this.actionInfoTO = actionInfoTO;
	}

	public Object getMethodParamMap() {
		return methodParamMap;
	}

	public void setMethodParamMap(Object methodParamMap) {
		this.methodParamMap = methodParamMap;
	}

	public Integer getUserID() {
		return userID;
	}

	public void setUserID(Integer userID) {
		this.userID = userID;
	}

	public ProtocolTO getProtocolTO() {
		return protocolTO;
	}

	public void setProtocolTO(ProtocolTO protocolTO) {
		this.protocolTO = protocolTO;
	}
}

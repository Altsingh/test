package com.alt.datacarrier.workflow;

import java.util.List;
import java.util.Map;

public class TransitionResponseTO extends WorkflowResponseTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer nextStageId;

	private Integer transitionId;

	private String fromMessage;

	private String toMessage;

	private Integer previousStageID;

	private String actionName;

	private String NextStageType;

	private String previousStageType;

	private String previousStageName;

	private Map<String, List<RoleActorTO>> roleActorsMap;

	public Integer getNextStageId() {
		return nextStageId;
	}

	public void setNextStageId(Integer nextStageId) {
		this.nextStageId = nextStageId;
	}

	public Integer getTransitionId() {
		return transitionId;
	}

	public void setTransitionId(Integer transitionId) {
		this.transitionId = transitionId;
	}

	public String getFromMessage() {
		return fromMessage;
	}

	public void setFromMessage(String fromMessage) {
		this.fromMessage = fromMessage;
	}

	public String getToMessage() {
		return toMessage;
	}

	public void setToMessage(String toMessage) {
		this.toMessage = toMessage;
	}

	public Map<String, List<RoleActorTO>> getRoleActorsMap() {
		return roleActorsMap;
	}

	public void setRoleActorsMap(Map<String, List<RoleActorTO>> roleActorsMap) {
		this.roleActorsMap = roleActorsMap;
	}

	public Integer getPreviousStageID() {
		return previousStageID;
	}

	public void setPreviousStageID(Integer previousStageID) {
		this.previousStageID = previousStageID;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public String getNextStageType() {
		return NextStageType;
	}

	public void setNextStageType(String nextStageType) {
		NextStageType = nextStageType;
	}

	public String getPreviousStageType() {
		return previousStageType;
	}

	public void setPreviousStageType(String previousStageType) {
		this.previousStageType = previousStageType;
	}

	public String getPreviousStageName() {
		return previousStageName;
	}

	public void setPreviousStageName(String previousStageName) {
		this.previousStageName = previousStageName;
	}

}

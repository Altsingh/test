package com.alt.datacarrier.workflow;

import java.util.List;

public class UserWorkflowInfo {

	private List<TaskTO> tasks;
	
	private List<WorkflowCardTO> cards;

	public List<TaskTO> getTasks() {
		return tasks;
	}

	public void setTasks(List<TaskTO> tasks) {
		this.tasks = tasks;
	}

	public List<WorkflowCardTO> getCards() {
		return cards;
	}

	public void setCards(List<WorkflowCardTO> cards) {
		this.cards = cards;
	}
	
}

package com.alt.datacarrier.workflow;

public class WorkflowCardTO {

	private String formName;

	private AppliedWorkflowCardTO appliedWorkflowCard;

	private ApprovalWorkflowCardTO approvalWorkflowCard;

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public AppliedWorkflowCardTO getAppliedWorkflowCard() {
		return appliedWorkflowCard;
	}

	public void setAppliedWorkflowCard(AppliedWorkflowCardTO appliedWorkflowCard) {
		this.appliedWorkflowCard = appliedWorkflowCard;
	}

	public ApprovalWorkflowCardTO getApprovalWorkflowCard() {
		return approvalWorkflowCard;
	}

	public void setApprovalWorkflowCard(ApprovalWorkflowCardTO approvalWorkflowCard) {
		this.approvalWorkflowCard = approvalWorkflowCard;
	}

}

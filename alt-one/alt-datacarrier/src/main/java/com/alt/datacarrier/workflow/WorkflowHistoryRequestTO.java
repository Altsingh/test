package com.alt.datacarrier.workflow;

import java.util.List;

public class WorkflowHistoryRequestTO {

	private Integer tenantId;

	private Integer contextEntityId;

	private Long contextInstanceId;

	private List<RoleActorTO> roleActors;

	private Integer transitionId;

	private String taskCode;

	private String taskStatus;

	private Integer userId;

	private String username;

	private Integer organizationId;

	private Long previousWorkflowHistoryId;

	private ProtocolTO protocolTO;

	public Integer getTenantId() {
		return tenantId;
	}

	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}

	public Integer getContextEntityId() {
		return contextEntityId;
	}

	public void setContextEntityId(Integer contextEntityId) {
		this.contextEntityId = contextEntityId;
	}

	public Long getContextInstanceId() {
		return contextInstanceId;
	}

	public void setContextInstanceId(Long contextInstanceId) {
		this.contextInstanceId = contextInstanceId;
	}

	public List<RoleActorTO> getRoleActors() {
		return roleActors;
	}

	public void setRoleActors(List<RoleActorTO> roleActors) {
		this.roleActors = roleActors;
	}

	public Integer getTransitionId() {
		return transitionId;
	}

	public void setTransitionId(Integer transitionId) {
		this.transitionId = transitionId;
	}

	public String getTaskCode() {
		return taskCode;
	}

	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public Long getPreviousWorkflowHistoryId() {
		return previousWorkflowHistoryId;
	}

	public void setPreviousWorkflowHistoryId(Long previousWorkflowHistoryId) {
		this.previousWorkflowHistoryId = previousWorkflowHistoryId;
	}

}

package com.alt.datacarrier.workflow;

import java.util.List;

public class WorkflowHistoryResponseTO extends WorkflowResponseTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Boolean successful;

	private String alertMessage;

	private String status;

	private String stage;

	private List<Long> workflowIDs;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public Boolean getSuccessful() {
		return successful;
	}

	public void setSuccessful(Boolean successful) {
		this.successful = successful;
	}

	public String getAlertMessage() {
		return alertMessage;
	}

	public void setAlertMessage(String alertMessage) {
		this.alertMessage = alertMessage;
	}

	public List<Long> getWorkflowIDs() {
		return workflowIDs;
	}

	public void setWorkflowIDs(List<Long> workflowIDs) {
		this.workflowIDs = workflowIDs;
	}

}

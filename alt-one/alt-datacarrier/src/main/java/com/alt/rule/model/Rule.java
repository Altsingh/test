package com.alt.rule.model;

import com.alt.datacarrier.kernel.uiclass.EnumHTMLControl;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "ruleType")
@JsonSubTypes({

        @JsonSubTypes.Type(value = ValidationRule.class, name = "VALIDATION"),

        @JsonSubTypes.Type(value = TransactionRule.class, name = "TRANSACTION")
})

public abstract class Rule implements Serializable{
    private static final long serialVersionUID = 3810417369466030627L;

    private RuleType ruleType;
    private String ruleName;
    private Long ruleId;

    public Rule(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public Rule() {
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public Long getRuleId() {
        return ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }
}

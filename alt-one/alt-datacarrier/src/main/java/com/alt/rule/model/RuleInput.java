package com.alt.rule.model;

import java.io.Serializable;

public class RuleInput implements Serializable{
    private InputType inputType;
    private String dbClass;
    private String dbClassAttr;
    private String value;


    public static final class RuleInputBuilder {
        private InputType inputType;
        private String dbClass;
        private String dbClassAttr;
        private String value;

        private RuleInputBuilder() {
        }

        public static RuleInputBuilder aRuleInput() {
            return new RuleInputBuilder();
        }

        public RuleInputBuilder withInputType(InputType inputType) {
            this.inputType = inputType;
            return this;
        }

        public RuleInputBuilder withDbClass(String dbClass) {
            this.dbClass = dbClass;
            return this;
        }

        public RuleInputBuilder withDbClassAttr(String dbClassAttr) {
            this.dbClassAttr = dbClassAttr;
            return this;
        }

        public RuleInputBuilder withValue(String value) {
            this.value = value;
            return this;
        }

        public RuleInput build() {
            RuleInput ruleInput = new RuleInput();
            ruleInput.value = this.value;
            ruleInput.inputType = this.inputType;
            ruleInput.dbClassAttr = this.dbClassAttr;
            ruleInput.dbClass = this.dbClass;
            return ruleInput;
        }
    }

    public InputType getInputType() {
        return inputType;
    }

    public String getDbClass() {
        return dbClass;
    }

    public String getDbClassAttr() {
        return dbClassAttr;
    }

    public String getValue() {
        return value;
    }
}

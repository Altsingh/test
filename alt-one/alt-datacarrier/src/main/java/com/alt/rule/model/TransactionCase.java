package com.alt.rule.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = TransactionCase.class)
public class TransactionCase extends Case{
    /**
	 *
	 */
	private static final long serialVersionUID = -7699410188076484919L;
	private RuleInput firstInput;
    private RuleInput secondInput;
    private TransactionCondition validationType;
    private String failureMessage;
    private RuleInput assignToAttribute;
    private String event;
    private String componentName;
    private String componentType;
    private String ruleExecutionEnd;
    private String scriptData;
	private String caseType;
    
    public TransactionCase() {

    }


    public TransactionCase(RuleInput firstInput, RuleInput secondInput, String string,
			String failureMessage,RuleInput assignToAttribute, String event, String componentName, String componentType, String ruleExecutionEnd,
			String scriptData, String caseType) {
		super();
		this.firstInput = firstInput;
		this.secondInput = secondInput;
		this.validationType = TransactionCondition.valueOf(string);
		this.failureMessage = failureMessage;
		this.assignToAttribute = assignToAttribute;
		this.event = event;
		this.componentName = componentName;
		this.componentType = componentType;
		this.ruleExecutionEnd = ruleExecutionEnd;
		this.scriptData = scriptData;
		this.caseType = caseType;
	}


    public RuleInput getAssignToAttribute() {
        return assignToAttribute;
    }

    public void setAssignToAttribute(RuleInput assignToAttribute) {
        this.assignToAttribute = assignToAttribute;
    }

	public RuleInput getFirstInput() {
        return firstInput;
    }

    public RuleInput getSecondInput() {
        return secondInput;
    }

    public TransactionCondition getValidationType() {
        return validationType;
    }

    public String getFailureMessage() {
        return failureMessage;
    }



    public void setFirstInput(RuleInput firstInput) {
		this.firstInput = firstInput;
	}


	public void setSecondInput(RuleInput secondInput) {
		this.secondInput = secondInput;
	}


	public void setValidationType(TransactionCondition validationType) {
		this.validationType = validationType;
	}


	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}



	public String getEvent() {
		return event;
	}


	public void setEvent(String event) {
		this.event = event;
	}


	public String getComponentName() {
		return componentName;
	}


	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}


	public String getComponentType() {
		return componentType;
	}


	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}



	public String getRuleExecutionEnd() {
		return ruleExecutionEnd;
	}


	public void setRuleExecutionEnd(String ruleExecutionEnd) {
		this.ruleExecutionEnd = ruleExecutionEnd;
	}



	public String getScriptData() {
		return scriptData;
	}


	public void setScriptData(String scriptData) {
		this.scriptData = scriptData;
	}


	public String getCaseType() {
		return caseType;
	}


	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}



	public static final class TransactionCaseBuilder {
        private RuleInput firstInput;
        private RuleInput secondInput;
        private TransactionCondition validationType;
        private String failureMessage;
        private String event;
        private String componentName;
        private String componentType;
        private String ruleExecutionEnd;
        private String scriptData;
        private String caseType;
        
        private TransactionCaseBuilder() {
        }

        public static TransactionCaseBuilder aTransactionCase() {
            return new TransactionCaseBuilder();
        }

        public TransactionCaseBuilder withFirstInput(RuleInput firstInput) {
            this.firstInput = firstInput;
            return this;
        }

        public TransactionCaseBuilder withSecondInput(RuleInput secondInput) {
            this.secondInput = secondInput;
            return this;
        }

        public TransactionCaseBuilder withValidationType(TransactionCondition validationType) {
            this.validationType = validationType;
            return this;
        }

        public TransactionCaseBuilder withFailureMessage(String failureMessage) {
            this.failureMessage = failureMessage;
            return this;
        }
        
        public TransactionCaseBuilder withEvent(String event) {
        	this.event = event;
        	return this;
        }
        
        public TransactionCaseBuilder withComponentName(String componentName) {
        	this.componentName = componentName;
        	return this;
        }
        
        public TransactionCaseBuilder withRuleExecutionEnd(String ruleExecutionEnd) {
        	this.ruleExecutionEnd = ruleExecutionEnd;
        	return this;
        }
        
        public TransactionCaseBuilder withScriptData(String scriptData) {
        	this.scriptData = scriptData;
        	return this;
        }
        
        public TransactionCaseBuilder withCaseType(String caseType) {
        	this.caseType = caseType;
        	return this;
        }

        
        public TransactionCase build() {
            TransactionCase transactionCase = new TransactionCase();
            transactionCase.failureMessage = this.failureMessage;
            transactionCase.validationType = this.validationType;
            transactionCase.firstInput = this.firstInput;
            transactionCase.secondInput = this.secondInput;
            transactionCase.event = this.event;
            transactionCase.componentName = this.componentName;
            transactionCase.componentType = this.componentType;
            transactionCase.ruleExecutionEnd = this.ruleExecutionEnd;
            transactionCase.scriptData = this.scriptData;
            transactionCase.caseType = this.caseType;
            return transactionCase;
        }
    }
}

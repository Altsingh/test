package com.alt.rule.model;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum TransactionCondition {
    ADD("ADD"),SUBTRACT("SUBTRACT"),CONCAT("CONCAT");
    
    

    private String text;

    TransactionCondition(String text) {
        this.text = text;
    }

    @JsonCreator
    public static RuleType fromString(String raw) {
        return RuleType.valueOf(raw.toUpperCase());
    }

    @Override
    public String toString() {
        return text;
    }
}

package com.alt.rule.model;

import java.util.List;

public class TransactionRule extends Rule{

    private List<TransactionCase> cases;
    private String successMessage;

    public TransactionRule(List<TransactionCase> cases, String successMessage) {
        this.cases = cases;
        this.successMessage = successMessage;
    }

    public TransactionRule(){
        super(RuleType.TRANSACTION);
    }

    public List<TransactionCase> getCases() {
        return cases;
    }

    public String getSuccessMessage() {
        return successMessage;
    }


    public static final class TransactionRuleBuilder {
        private List<TransactionCase> cases;
        private String successMessage;
        private RuleType ruleType;
        private String ruleName;
        private Long ruleId;

        private TransactionRuleBuilder() {
        }

        public static TransactionRuleBuilder aTransactionRule() {
            return new TransactionRuleBuilder();
        }

        public TransactionRuleBuilder withCases(List<TransactionCase> cases) {
            this.cases = cases;
            return this;
        }

        public TransactionRuleBuilder withSuccessMessage(String successMessage) {
            this.successMessage = successMessage;
            return this;
        }

        public TransactionRuleBuilder withRuleType(RuleType ruleType) {
            this.ruleType = ruleType;
            return this;
        }

        public TransactionRuleBuilder withRuleName(String ruleName) {
            this.ruleName = ruleName;
            return this;
        }

        public TransactionRuleBuilder withRuleId(Long ruleId) {
            this.ruleId = ruleId;
            return this;
        }

        public TransactionRule build() {
            TransactionRule transactionRule = new TransactionRule(cases, successMessage);
            transactionRule.setRuleType(ruleType);
            transactionRule.setRuleName(ruleName);
            transactionRule.setRuleId(ruleId);
            return transactionRule;
        }
    }
}

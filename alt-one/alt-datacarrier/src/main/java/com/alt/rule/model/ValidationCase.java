package com.alt.rule.model;


public class ValidationCase extends Case{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1923527952422497494L;
	private RuleInput firstInput;
    private RuleInput secondInput;
    private ValidationCondition validationType;
    private String failureMessage;
    private String event;
    private String componentName;
    private String componentType;
    private String ruleExecutionEnd;
    private String scriptData;
	private String caseType;
    
    public ValidationCase() {
    	
    }

    public ValidationCase(RuleInput firstInput, RuleInput secondInput, String validationType,
			String failureMessage, String event, String componentName, String componentType, String ruleExecutionEnd,
			String scriptData, String caseType) {
    	this.firstInput = firstInput;
		this.secondInput = secondInput;
		this.validationType = ValidationCondition.valueOf(validationType);
		this.failureMessage = failureMessage;
		this.event = event;
		this.componentName = componentName;
		this.componentType = componentType;
		this.ruleExecutionEnd = ruleExecutionEnd;
		this.scriptData = scriptData;
		this.caseType = caseType;
	}

	public RuleInput getFirstInput() {
        return firstInput;
    }

    public RuleInput getSecondInput() {
        return secondInput;
    }

    public ValidationCondition getValidationType() {
        return validationType;
    }
    
    

    public void setFirstInput(RuleInput firstInput) {
		this.firstInput = firstInput;
	}

	public void setSecondInput(RuleInput secondInput) {
		this.secondInput = secondInput;
	}

	public void setValidationType(ValidationCondition validationType) {
		this.validationType = validationType;
	}

	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}

	public String getFailureMessage() {
        return failureMessage;
    }

    public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getComponentType() {
		return componentType;
	}

	public void setComponentType(String componentType) {
		this.componentType = componentType;
	}

	public String getRuleExecutionEnd() {
		return ruleExecutionEnd;
	}

	public void setRuleExecutionEnd(String ruleExecutionEnd) {
		this.ruleExecutionEnd = ruleExecutionEnd;
	}

	public String getScriptData() {
		return scriptData;
	}

	public void setScriptData(String scriptData) {
		this.scriptData = scriptData;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public static final class ValidationCaseBuilder {
        private RuleInput firstInput;
        private RuleInput secondInput;
        private ValidationCondition validationType;
        private String failureMessage;
        private String event;
        private String componentName;
        private String componentType;
        private String ruleExecutionEnd;
        private String scriptData;
        private String caseType;

        private ValidationCaseBuilder() {
        }

        public static ValidationCaseBuilder aValidationCase() {
            return new ValidationCaseBuilder();
        }

        public ValidationCaseBuilder withFirstInput(RuleInput firstInput) {
            this.firstInput = firstInput;
            return this;
        }

        public ValidationCaseBuilder withSecondInput(RuleInput secondInput) {
            this.secondInput = secondInput;
            return this;
        }

        public ValidationCaseBuilder withValidationType(ValidationCondition validationType) {
            this.validationType = validationType;
            return this;
        }

        public ValidationCaseBuilder withFailureMessage(String failureMessage) {
            this.failureMessage = failureMessage;
            return this;
        }
        
        public ValidationCaseBuilder withEvent(String event) {
            this.event = event;
            return this;
        }
        
        public ValidationCaseBuilder withComponentName(String componentName) {
            this.componentName = componentName;
            return this;
        }
        
        public ValidationCaseBuilder withComponentType(String componentType) {
            this.componentType = componentType;
            return this;
        }
        
        public ValidationCaseBuilder withRuleExecutionEnd(String ruleExecutionEnd) {
            this.ruleExecutionEnd = ruleExecutionEnd;
            return this;
        }
        
        public ValidationCaseBuilder withScriptData(String scriptData) {
        	this.scriptData = scriptData;
        	return this;
        }
        
        public ValidationCaseBuilder withCaseType(String caseType) {
        	this.caseType = caseType;
        	return this;
        }
        

        public ValidationCase build() {
            ValidationCase validationCase = new ValidationCase();
            validationCase.secondInput = this.secondInput;
            validationCase.firstInput = this.firstInput;
            validationCase.failureMessage = this.failureMessage;
            validationCase.validationType = this.validationType;
            validationCase.event = this.event;
            validationCase.componentName = this.componentName;
            validationCase.componentType = this.componentType;
            validationCase.ruleExecutionEnd = this.ruleExecutionEnd;
            validationCase.scriptData = this.scriptData;
            validationCase.caseType = this.caseType;
            return validationCase;
        }
    }
}

package com.alt.rule.model;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum ValidationCondition{
    EQUALS("EQUALS"),GREATER_THAN("GREATER_THAN"),LESS_THAN("LESS_THAN");
    
    public String text;

	ValidationCondition(String text) {
        this.text = text;
    }
	
	public static String contains(String condition) {

		String value = "";

		for (ValidationCondition o : ValidationCondition.values()) {
			if (o.name().equalsIgnoreCase(condition)) {
				value = o.name();
				break;
			}
		}

		return value;
	}

    @Override
    public String toString() {
        return text;
    }
}

package com.alt.rule.model;

import java.util.List;

public class ValidationRule extends Rule{
    private List<ValidationCase> cases;
    private String ruleExpresssion;
    private String successMessage;

    public ValidationRule() {
        super(RuleType.VALIDATION);
    }

    public ValidationRule(List<ValidationCase> cases, String ruleExpresssion, String successMessage) {
        this.cases = cases;
        this.ruleExpresssion = ruleExpresssion;
        this.successMessage = successMessage;
    }

    public List<ValidationCase> getCases() {
        return cases;
    }

    public String getRuleExpresssion() {
        return ruleExpresssion;
    }

    public String getSuccessMessage() {
        return successMessage;
    }


    public static final class ValidationRuleBuilder {
        private List<ValidationCase> cases;
        private String ruleExpresssion;
        private String successMessage;
        private RuleType ruleType;
        private String ruleName;
        private Long ruleId;
        private String event;
        private String componentName;
        private String componentType;
        

        private ValidationRuleBuilder() {
        }

        public static ValidationRuleBuilder aValidationRule() {
            return new ValidationRuleBuilder();
        }

        public ValidationRuleBuilder withCases(List<ValidationCase> cases) {
            this.cases = cases;
            return this;
        }

        public ValidationRuleBuilder withRuleExpresssion(String ruleExpresssion) {
            this.ruleExpresssion = ruleExpresssion;
            return this;
        }

        public ValidationRuleBuilder withSuccessMessage(String successMessage) {
            this.successMessage = successMessage;
            return this;
        }

        public ValidationRuleBuilder withRuleType(RuleType ruleType) {
            this.ruleType = ruleType;
            return this;
        }

        public ValidationRuleBuilder withRuleName(String ruleName) {
            this.ruleName = ruleName;
            return this;
        }

        public ValidationRuleBuilder withRuleId(Long ruleId) {
            this.ruleId = ruleId;
            return this;
        }
        
        public ValidationRuleBuilder withEvent(String event) {
        	this.event = event;
        	return this;
        }
        
        public ValidationRuleBuilder withComponentName(String componentName) {
        	this.componentName = componentName;
        	return this;
        }
        
        public ValidationRuleBuilder withComponentType(String componentType) {
        	this.componentType = componentType;
        	return this;
        }

        public ValidationRule build() {
            ValidationRule validationRule = new ValidationRule(cases, ruleExpresssion, successMessage);
            validationRule.setRuleType(ruleType);
            validationRule.setRuleName(ruleName);
            validationRule.setRuleId(ruleId);
            return validationRule;
        }
    }
}

package com.alt.rule.transport.request;

import com.alt.rule.model.Rule;

import java.util.List;

public class RuleCreationRequest {
    private List<? extends Rule> rules;
    private List<Long> ruleIdList;

    public List<? extends Rule> getRules() {
        return rules;
    }

    public void setRules(List<? extends Rule> rules) {
        this.rules = rules;
    }

	public List<Long> getRuleIdList() {
		return ruleIdList;
	}

	public void setRuleIdList(List<Long> ruleIdList) {
		this.ruleIdList = ruleIdList;
	}

	
}

package com.alt.rule.transport.response;

public class MessageResponseTO
{

    private String message;

    public MessageResponseTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

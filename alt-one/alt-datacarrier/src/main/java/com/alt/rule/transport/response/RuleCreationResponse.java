package com.alt.rule.transport.response;

import com.alt.rule.model.Rule;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

@JsonDeserialize(as = RuleCreationResponse.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RuleCreationResponse {
    private boolean  success;
    private List<? extends Rule> rules;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<? extends Rule> getRules() {
        return rules;
    }

    public void setRules(List<? extends Rule> rules) {
        this.rules = rules;
    }
}

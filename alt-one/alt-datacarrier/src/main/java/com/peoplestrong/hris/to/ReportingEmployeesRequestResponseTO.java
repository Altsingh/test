package com.peoplestrong.hris.to;

import java.util.Set;

public class ReportingEmployeesRequestResponseTO {

	private Integer organizationId;

	private Integer userId;

	private Set<Integer> roleIds;

	private Set<Integer> employeeIds;

	public Integer getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Set<Integer> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(Set<Integer> roleIds) {
		this.roleIds = roleIds;
	}

	public Set<Integer> getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(Set<Integer> employeeIds) {
		this.employeeIds = employeeIds;
	}

}

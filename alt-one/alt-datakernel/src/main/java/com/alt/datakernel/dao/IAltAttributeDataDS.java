package com.alt.datakernel.dao;

import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;

@Service
public interface IAltAttributeDataDS {

	void getAttributeNameByAttributeIdAndClassId(String attributeId, String classId, Protocol protocol);

}

package com.alt.datakernel.dao;

import java.util.List;
import java.util.Set;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.model.AltAttributeData;
import com.alt.datakernel.model.AltClassData;
import com.alt.datakernel.model.AltGlobalReferenceData;
import com.alt.datakernel.model.AltReferenceData;
import org.springframework.stereotype.Service;

@Service
public interface IClassDS {

	ClassMeta add(Protocol protocol, AltClassData classData, Set<AltAttributeData> attributes, List<AltReferenceData> references, List<AltGlobalReferenceData> globalReferenceDataList) throws ClassRelatedException;

	ClassMeta get(String name, Protocol protocol) throws ClassRelatedException;

	List<ClassMeta> getAll(Protocol protocol) throws ClassRelatedException;

	ClassMeta get(Integer id, Protocol protocol) throws ClassRelatedException;

	AltClassData getFromId(Protocol protocol, Integer id) throws ClassRelatedException;

	AltAttributeData getAttributeFromClassAndAttributeName(Protocol protocol, String referenceClass, String referenceAttr) throws ClassRelatedException;

    void markInactiveFormClass(Protocol protocol, String s) throws ClassRelatedException;

	List<ClassMeta> listAll(Protocol protocol, Set<Integer> classIds);
}

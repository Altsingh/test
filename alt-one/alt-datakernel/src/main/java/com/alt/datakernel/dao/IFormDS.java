package com.alt.datakernel.dao;

import java.util.Collection;
import java.util.List;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.model.AltFormData;

public interface IFormDS {

	AltForm add(Protocol protocol, AltFormData classData);

	AltForm get(Protocol protocol, String name);

	AltForm get(Protocol protocol, String name, String version);

	List<AltForm> getAll(Protocol protocol, Integer appId);

	AltForm getFromId(Protocol protocol, Integer id) throws FormRelatedException;

	AltForm updateComponentsVersionState(Protocol protocol, Integer formId, List<AltAbstractComponent> componentList,
			String version, EnumFormState state);

	AltForm updateFormSecurity(Protocol protocol, Integer formId, List<AltAbstractComponent> componentList,
			String formSecurityId);

	AltForm updateState(Protocol protocol, Integer formId, EnumFormState state, ClassMeta classMeta);

	AltForm updateComponents(Protocol protocol, Integer formId, List<AltAbstractComponent> componentList);

	AltForm getFormFromSecurityId(Protocol protocol, String formSecurityId);

	List<AltForm> listByIds(Protocol protocol, List<Integer> formIds);

	List<AltForm> listByNames(Protocol protocol, Collection<String> formNames);

	AltFormData findForm(Protocol protocol, AltFormData altFormData, String formid);

	void editForm(AltFormData form);

	public AltFormData getFormEntityFromMetaClassId(Protocol protocol, Integer metaClassId) throws FormRelatedException;
}

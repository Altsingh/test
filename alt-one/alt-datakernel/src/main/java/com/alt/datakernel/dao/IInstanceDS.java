package com.alt.datakernel.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import com.alt.datacarrier.core.Protocol;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.model.Instance;

public interface IInstanceDS {

	Instance persistInstance(Instance instance, Protocol protocol, boolean processHistory)
			throws InstanceRelatedException, InstanceHistoryRelatedException;

	List<Instance> findAllInstancesForAClass(Protocol protocol, String name);

	Instance updateAttributes(Protocol protocol, Integer instanceId, Map<String, Object> attributes);

	Instance find(Protocol protocol, Integer id);

	List<Instance> findAllInstancesForAClassId(Protocol protocol, Integer id, String className, Map<String,String> attributes);

	List<Object[]> getAllAttributeValuesByAttributeName(String string, String id, Protocol protocol)
			throws InstanceRelatedException;

	List<Instance> getSavedFormDataByContext(Instance instance, Protocol protocol) throws InstanceRelatedException;

	public List<Object[]> executeNativeQuery(Protocol protocol, String query);

	public Query createNativeQuery(Protocol protocol, String query);

	public List<Object[]> executeQueryForFetchingData(Protocol protocol, Query query);

	Instance editApp(Instance instance, Protocol protocol)
			throws InstanceRelatedException, InstanceHistoryRelatedException;

	public String getInstanceTableName(Protocol protocol);

}

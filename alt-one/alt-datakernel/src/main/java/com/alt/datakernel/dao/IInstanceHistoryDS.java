package com.alt.datakernel.dao;

import com.alt.datacarrier.core.Protocol;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.model.InstanceHistory;

public interface IInstanceHistoryDS {
	public InstanceHistory persistInstanceHistory(InstanceHistory instanceHistory, Protocol protocol)
			throws InstanceHistoryRelatedException;
}

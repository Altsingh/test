package com.alt.datakernel.dao;

import java.math.BigInteger;
import java.util.List;

import com.alt.datacarrier.core.Protocol;
import com.alt.datakernel.exception.QueryRelatedException;

public interface IQueryDS {

	List<Object[]> executeQuery(Protocol protocol, String queryString) throws QueryRelatedException;

	BigInteger executeCountQuery(Protocol protocol, String queryString) throws QueryRelatedException;

}

package com.alt.datakernel.dao;

import java.util.List;

import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.ReferenceData;
import com.alt.datakernel.exception.ReferenceRelatedException;

@Service
public interface IReferenceDS {
	public List<AttributeMeta> getAllAttributesByClassID(Protocol protocol, Integer metaClassID)
			throws ReferenceRelatedException;

	public ReferenceData getDataByReferenceID(Protocol protocol, Integer id) throws ReferenceRelatedException;
}

/**
 * @author vaibhav.kashyap
 * */
package com.alt.datakernel.dao;

import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.request.TaskCodeRecordTransport;
import com.alt.datakernel.exception.TaskCodeRecordRelatedException;
import com.alt.datakernel.model.TaskCodeRecord;

@Service
public interface ITaskCodeRecordDS {

	TaskCodeRecordTransport add(TaskCodeRecord taskCodeRecord, Protocol protocol) throws TaskCodeRecordRelatedException;

	TaskCodeRecordTransport update(TaskCodeRecord taskCodeRecord, Protocol protocol) throws TaskCodeRecordRelatedException;

	TaskCodeRecordTransport get(TaskCodeRecord taskCodeRecord) throws TaskCodeRecordRelatedException;

	TaskCodeRecordTransport delete(TaskCodeRecord taskCodeRecord) throws TaskCodeRecordRelatedException;

	TaskCodeRecord find(Protocol protocol, Integer id);
}

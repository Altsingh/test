package com.alt.datakernel.dao.converter;

import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datakernel.model.AltAttributeData;

public class AltAttributeConverter {
	public static AttributeMeta convertAltAttributeEntityToMetaTO(AltAttributeData altAttributeData) {
		AttributeMeta attributeMeta = new AttributeMeta();
		if (altAttributeData.getAttributeName() != null)
			attributeMeta.setAttributeName(altAttributeData.getAttributeName());
		if (altAttributeData.getAttributeId() != null)
			attributeMeta.setAttributeId(Integer.valueOf((Integer) altAttributeData.getAttributeId()).toString());
		if (altAttributeData.getDataType() != null)
			attributeMeta.setDataType(altAttributeData.getDataType());
		if (altAttributeData.getDefaultValue() != null)
			attributeMeta.setDefaultValue(altAttributeData.getDefaultValue());
		if (altAttributeData.getDisplayUnit() != null)
			attributeMeta.setDisplayUnit(altAttributeData.getDisplayUnit());
		if (altAttributeData.getIndexable() != null)
			attributeMeta.setIndexable(altAttributeData.getIndexable());
		if (altAttributeData.getMandatory())
			attributeMeta.setMandatory(altAttributeData.getMandatory());
		if (altAttributeData.getPlaceholderType() != null)
			attributeMeta.setPlaceholderType(altAttributeData.getPlaceholderType());
		if (altAttributeData.getReferenceClassId() != null)
			attributeMeta.setReferenceClassId(altAttributeData.getReferenceClassId());
		if (altAttributeData.getSearchable() != null)
			attributeMeta.setSearchable(altAttributeData.getSearchable());
		if (altAttributeData.getStatus() != null)
			attributeMeta.setStatus(altAttributeData.getStatus());
		if (altAttributeData.getUnique1() != null)
			attributeMeta.setUnique(altAttributeData.getUnique1());
		if (altAttributeData.getOrgId() != null)
			attributeMeta.setOrgId(altAttributeData.getOrgId());
		if (altAttributeData.getClassData() != null) {
			attributeMeta.setRefClassMeta(
					AltClassConverter.convertToClassMetaDataFromEntity(altAttributeData.getClassData()));
		}
		return attributeMeta;
	}
}

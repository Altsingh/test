package com.alt.datakernel.dao.converter;

import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datakernel.model.AltClassData;

public class AltClassConverter {
	public static ClassMeta convertToClassMetaDataFromEntity(AltClassData altclassDataEntity) {
		ClassMeta meta = new ClassMeta();
		if (altclassDataEntity.getCreatedDate() != null)
			meta.setCreatedDate(altclassDataEntity.getCreatedDate());
		if (altclassDataEntity.getCreatedBy() != null)
			meta.setCreatedBy(altclassDataEntity.getCreatedBy());
		if (altclassDataEntity.getCustomAttributeAllowed())
			meta.setCustomAttributeAllowed(altclassDataEntity.getCustomAttributeAllowed());
		if (altclassDataEntity.getClassId() != null)
			meta.setId(altclassDataEntity.getClassId().toString());
		if (altclassDataEntity.getInstanceLimiter() != null)
			meta.setInstanceLimiter(altclassDataEntity.getInstanceLimiter());
		if (altclassDataEntity.getInstancePrefix() != null)
			meta.setInstancePrefix(altclassDataEntity.getInstancePrefix());
		if (altclassDataEntity.getLocationSpecific() != null)
			meta.setLocationSpecific(altclassDataEntity.getLocationSpecific());
		if (altclassDataEntity.getModifiedDate() != null)
			meta.setModifiedDate(altclassDataEntity.getModifiedDate());
		if (altclassDataEntity.getModifiedBy() != null)
			meta.setModifiedBy(altclassDataEntity.getModifiedBy());
		if (altclassDataEntity.getParameterType() != null)
			meta.setParameterType(altclassDataEntity.getParameterType());
		if (altclassDataEntity.getStatus() != null)
			meta.setStatus(altclassDataEntity.getStatus());
		if (altclassDataEntity.getSystemType() != null)
			meta.setSystemType(altclassDataEntity.getSystemType());
		if (altclassDataEntity.getClassName() != null)
			meta.setName(altclassDataEntity.getClassName());
		return meta;
	}
}

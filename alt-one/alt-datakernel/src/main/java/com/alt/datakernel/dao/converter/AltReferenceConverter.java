package com.alt.datakernel.dao.converter;

import com.alt.datacarrier.kernel.db.core.ReferenceData;
import com.alt.datakernel.model.AltReferenceData;

public class AltReferenceConverter {

	public static ReferenceData convertAltReferenceEntityToAltReferenceData(AltReferenceData refDataFetched) {
		ReferenceData convertedData = null;
		if (refDataFetched == null)
			return null;
		convertedData = new ReferenceData();
		convertedData.setReferenceid(refDataFetched.getReferenceid());
		if (refDataFetched.getCreatedby() != null)
			convertedData.setCreatedby(refDataFetched.getCreatedby());
		if (refDataFetched.getCreateddate() != null)
			convertedData.setCreateddate(refDataFetched.getCreateddate());
		if (refDataFetched.getModifiedby() != null)
			convertedData.setModifiedby(refDataFetched.getModifiedby());
		if (refDataFetched.getModifieddate() != null)
			convertedData.setModifieddate(refDataFetched.getModifieddate());
		if (refDataFetched.getOrg() != null)
			convertedData.setOrg(refDataFetched.getOrg());

		if (refDataFetched.getClassData() != null) {
			convertedData
					.setClassMeta(AltClassConverter.convertToClassMetaDataFromEntity(refDataFetched.getClassData()));
		}

		if (refDataFetched.getReferenceByAttribute() != null) {
			convertedData.setReferenceByAttributeMeta(
					AltAttributeConverter.convertAltAttributeEntityToMetaTO(refDataFetched.getReferenceByAttribute()));
		}

		if (refDataFetched.getReferenceByClass() != null) {
			convertedData.setReferenceByClassMeta(
					AltClassConverter.convertToClassMetaDataFromEntity(refDataFetched.getReferenceByClass()));
		}

		if (refDataFetched.getOrgId() != null)
			convertedData.setOrgId(refDataFetched.getOrgId());

		return convertedData;
	}
}

package com.alt.datakernel.dao.converter;

import java.util.ArrayList;
import java.util.List;

import com.alt.datakernel.model.Instance;
import com.alt.datakernel.model.InstanceHistory;

public class InstanceHistoryUtil {

	public static List<InstanceHistory> getInstanceHistoryRequestFromInstanceObject(List<Instance> instanceList)
			throws ClassCastException, NumberFormatException {
		if (instanceList == null || instanceList.isEmpty())
			return null;

		List<InstanceHistory> instanceHistoryList = new ArrayList<>();
		InstanceHistory instanceHistory = null;
		try {
			for (Instance instance : instanceList) {
				instanceHistory = new InstanceHistory(new InstanceHistory.Builder()
						.withId(instance.getId())
						.withInstanceId(instance.getId())
						.withInstanceCreatedBy((instance.getCreatedBy() != null) ? instance.getCreatedBy() : null)
						.withInstanceCreatedDate((instance.getCreatedDate() != null) ? instance.getCreatedDate() : null)
						.withInstanceModifiedBy((instance.getModifiedBy() != null) ? instance.getModifiedBy() : null)
						.withInstanceModifiedDate(
								(instance.getModifiedDate() != null) ? instance.getModifiedDate() : null)
						.withAppId((instance.getAppId() != null) ? instance.getAppId() : null)
						.withFormId((instance.getFormId() != null) ? instance.getFormId() : null)
						.withFormName((instance.getFormname() != null) ? instance.getFormname() : null)
						.withClassName((instance.getClassName() != null) ? instance.getClassName() : null)
						.withClassData((instance.getClassData() != null) ? instance.getClassData() : null)
						.withAttributes((instance.getAttributes() != null) ? instance.getAttributes() : null)
						.withInstanceStatus((instance.getStatus() != null) ? instance.getStatus() : null)
						.withStatus((instance.getStatus() != null) ? instance.getStatus() : null)
						.withUserId((instance.getUserId() != null) ? instance.getUserId() : null)
						.withOrganizationId(
								(instance.getOrganizationId() != null) ? instance.getOrganizationId() : null)
						.withCreatedDate(System.currentTimeMillis()).withModifiedDate(System.currentTimeMillis()));

				instanceHistoryList.add(instanceHistory);

			}
		} catch (Exception ex) {
			instanceHistoryList = null;
		}

		return instanceHistoryList;
	}

}

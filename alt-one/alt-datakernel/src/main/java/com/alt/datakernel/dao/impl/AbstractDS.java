package com.alt.datakernel.dao.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.alt.datacarrier.core.Protocol;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Scope(proxyMode = ScopedProxyMode.INTERFACES)
@Transactional
public abstract class AbstractDS<T>{

	@PersistenceContext
	private EntityManager entityManager;

    protected EntityManager getEntityManager(Protocol protocol){
		return entityManager;
    }

    protected EntityManager getEntityManager(){
        return entityManager;
    }

    public void executeInsertNativeQuery(Protocol protocol, String query) {
		getEntityManager(protocol).joinTransaction();
		int result =getEntityManager(protocol).createNativeQuery(query).executeUpdate();
	}

	public void create(Protocol protocol, T entity) {
		getEntityManager(protocol).persist(entity);
	}

	@Transactional
    public void create(T entity) {
        getEntityManager().persist(entity);
    }

	public T edit(Protocol protocol, T entity) {
		return getEntityManager(protocol).merge(entity);
	}

    public T edit(T entity) {
        return getEntityManager().merge(entity);
    }

	public void remove(Protocol protocol, T entity){
		getEntityManager(protocol).remove(getEntityManager(protocol).merge(entity));
	}

	@Transactional(readOnly=true)
	public T find(Protocol protocol, T entity, Object id){
		return (T) getEntityManager(protocol).find(entity.getClass(), id);
	}

	public List<T> findAll(Protocol protocol, T entity){
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager(protocol).getCriteriaBuilder().createQuery();
		cq.select(cq.from(entity.getClass()));
		return getEntityManager(protocol).createQuery(cq).getResultList();
	}

	public List<T> findRange(Protocol protocol,T entity,int[] range){
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager(protocol).getCriteriaBuilder().createQuery();
		cq.select(cq.from(entity.getClass()));
		javax.persistence.Query q = getEntityManager(protocol).createQuery(cq);
		q.setMaxResults(range[1] - range[0]);
		q.setFirstResult(range[0]);
		return q.getResultList();
	}

	public int count(Protocol protocol,T entity){
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager(protocol).getCriteriaBuilder().createQuery();
		javax.persistence.criteria.Root<T> rt = cq.from(entity.getClass());
		cq.select(getEntityManager(protocol).getCriteriaBuilder().count(rt));
		javax.persistence.Query q = getEntityManager(protocol).createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}

	public List<? extends T> findWithParameters(Protocol protocol,T entity, javax.persistence.Query q){
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager(protocol).getCriteriaBuilder().createQuery();
		javax.persistence.criteria.Root<T> rt = cq.from(entity.getClass());
		cq.select(getEntityManager(protocol).getCriteriaBuilder().count(rt));
		return q.getResultList();
	}

}
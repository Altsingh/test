package com.alt.datakernel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.alt.datakernel.model.AltApp;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class AltAppDS extends AbstractDS<AltApp> {

	@Transactional(readOnly = true)
	public List<AltApp> getAppsForOrganization(Integer orgId) {
		List<AltApp> appList = new ArrayList<>();
		/**
		 * @author harshvardan.singh modifying query for sorting apps list available on
		 *         app list page.
		 */
		Query query = getEntityManager().createQuery("SELECT f FROM AltApp f WHERE f.organizationid = :orgId and f.status=0 ORDER BY f.modifieddate DESC")
				.setParameter("orgId", orgId);
		appList = query.getResultList();

		return appList;

	}

	@Transactional(readOnly = true)
	public AltApp getAppByAppCode(String appCode, int organizationId) {
		Query query = getEntityManager()
				.createQuery("SELECT f FROM AltApp f WHERE f.organizationid = :orgId and f.appcode=:appcode")
				.setParameter("orgId", organizationId).setParameter("appcode", appCode);
			return (AltApp) query.getSingleResult();
	}

	@Transactional
	public AltApp persistAndFlush(AltApp app) {
		getEntityManager().persist(app);
		getEntityManager().flush();
		return app;
	}
}

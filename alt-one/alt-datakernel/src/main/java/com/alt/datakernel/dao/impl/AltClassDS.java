package com.alt.datakernel.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datakernel.dao.IClassDS;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.model.AltAttributeData;
import com.alt.datakernel.model.AltClassData;
import com.alt.datakernel.model.AltGlobalReferenceData;
import com.alt.datakernel.model.AltReferenceData;
import com.alt.datakernel.model.BaseModel;
import com.alt.datakernel.service.impl.ClassService;

@Repository
public class AltClassDS extends AbstractDS<BaseModel> implements IClassDS {

	private Logger LOGGER = LoggerFactory.getLogger(ClassService.class);

	@Override
	@Transactional
	public ClassMeta add(Protocol protocol, AltClassData classData, Set<AltAttributeData> attributes,
			List<AltReferenceData> references, List<AltGlobalReferenceData> globalReferences)
			throws ClassRelatedException {
		AltClassData classDataReturn = (AltClassData) edit(protocol, classData);
		List<AltAttributeData> list = new ArrayList<>();
		for (AltAttributeData attribute : attributes) {
			attribute.setClassData(classDataReturn);
			AltAttributeData data = (AltAttributeData) edit(protocol, attribute);
			list.add(data);
		}
		references.forEach(r -> {
			r.setClassData(classDataReturn);
			AltReferenceData data = (AltReferenceData) edit(protocol, r);
		});
		globalReferences.forEach(r -> {
			r.setClassData(classDataReturn);
			AltGlobalReferenceData globalRef = (AltGlobalReferenceData) edit(protocol, r);
		});

		getEntityManager().flush();
		getEntityManager().clear();
		return convertToClassMeta(classDataReturn, list);
	}

	@Transactional
	private void addReference(Protocol protocol, AltClassData classData, AltClassData referenceByClassData,
			AltAttributeData referenceByAttribute, String org, String user) {
		AltReferenceData data = new AltReferenceData(new AltReferenceData.Builder().withClassData(classData)
				.withReferenceByClass(referenceByClassData).withOrg(org).withReferenceByAttribute(referenceByAttribute)
				.withCreatedDate(new Date().getTime()).withCreatedBy(user).withModifiedDate(new Date().getTime())
				.withModifiedBy(user).withOrgId(protocol.getOrgId()));
		create(protocol, data);
	}

	@Override
	@Transactional(readOnly=true)
	public ClassMeta get(String name, Protocol protocol) throws ClassRelatedException {
		List<AltClassData> list1 = null;
		List<AltAttributeData> list2 = null;
		try {
			javax.persistence.Query q1 = getEntityManager(protocol)
					.createQuery("SELECT c FROM AltClassData c WHERE c.classname = :className"
							+ " and c.orgId = :orgId and c.status = 0")
					.setParameter("className", name).setParameter("orgId", protocol.getOrgId());
			list1 = q1.getResultList();
			if (list1 == null || list1.isEmpty()) {
				throw new ClassRelatedException(
						"Class with name : " + name + " and orgId : " + protocol.getOrgId() + " does not exist.");
			}
			Integer classId = list1.get(0).getClassId();
			javax.persistence.Query q2 = getEntityManager(protocol)
					.createQuery("SELECT a FROM AltAttributeData a WHERE a.classData.classid = :classId"
							+ " and a.classData.orgId = :orgId")
					.setParameter("classId", classId).setParameter("orgId", protocol.getOrgId());
			list2 = q2.getResultList();
		} catch (Exception e) {
			throw new ClassRelatedException(
					"Error while fetching class with name : " + name + " and org : " + protocol.getOrgCode() + ":");
		}

		return convertToClassMeta(list1.get(0), list2);
	}

	@Override
	@Transactional(readOnly=true)
	public List<ClassMeta> getAll(Protocol protocol) throws ClassRelatedException {
		Integer orgId = protocol.getOrgId();
		javax.persistence.Query q = getEntityManager(protocol)
				.createQuery("SELECT c FROM AltClassData c WHERE c.orgId = :orgId").setParameter("orgId", orgId);
		List<AltClassData> dataList = q.getResultList();
		if (dataList == null || dataList.isEmpty()) {
			throw new ClassRelatedException("Class from orgId : " + orgId + " does not exist.");
		}
		List<ClassMeta> metaList = new ArrayList<>();
		for (AltClassData data : dataList) {
			Integer classId = data.getClassId();
			javax.persistence.Query q1 = getEntityManager(protocol)
					.createQuery("SELECT a FROM AltAttributeData a WHERE a.classData.classid = :classId"
							+ " and a.classData.orgId = :orgId")
					.setParameter("classId", classId).setParameter("orgId", protocol.getOrgId());
			List<AltAttributeData> list = q1.getResultList();
			metaList.add(convertToClassMeta(data, list));
		}
		return metaList;
	}

	@Override
	@Transactional(readOnly=true)
	public AltClassData getFromId(Protocol protocol, Integer id) throws ClassRelatedException {
		AltClassData data = new AltClassData();
		try {
			data = (AltClassData) find(protocol, data, id);
			if (data == null) {
				throw new ClassRelatedException("Class with id : " + id + "does not exist.");
			}
		} catch (Exception e) {
			throw new ClassRelatedException("Error while fetching class with id : " + id);
		}
		return data;
	}

	@Override
	@Transactional(readOnly=true)
	public AltAttributeData getAttributeFromClassAndAttributeName(Protocol protocol, String referenceClass,
			String referenceAttr) throws ClassRelatedException {
		AltAttributeData referenceAttribute = null;
		try {
			javax.persistence.Query q = getEntityManager(protocol)
					.createQuery("SELECT a FROM AltAttributeData a WHERE a.classData.classid = :classid"
							+ " and a.attributename = :attributeName and a.classData.orgId = :orgId")
					.setParameter("classid", Integer.parseInt(referenceClass))
					.setParameter("attributeName", referenceAttr).setParameter("orgId", protocol.getOrgId());
			referenceAttribute = (AltAttributeData) q.getSingleResult();
		} catch (Exception e) {
			throw new ClassRelatedException(
					"Error while getting attribute details for given reference attribute: " + referenceAttr);
		}
		return referenceAttribute;
	}

	@Override
	@Transactional(readOnly=true)
	public ClassMeta get(Integer id, Protocol protocol) throws ClassRelatedException {
		AltClassData data = new AltClassData();
		List<AltAttributeData> list = null;
		try {
			data = (AltClassData) find(protocol, data, id);
			if (data == null) {
				throw new ClassRelatedException("Class with id : " + id + "does not exist.");
			}
			javax.persistence.Query q2 = getEntityManager(protocol)
					.createQuery("SELECT a FROM AltAttributeData a WHERE a.classData.classid = :classId"
							+ " and a.classData.orgId = :orgId")
					.setParameter("classId", id).setParameter("orgId", protocol.getOrgId());
			list = q2.getResultList();
		} catch (Exception e) {
			throw new ClassRelatedException("Error while fetching class with id : " + id);
		}
		return convertToClassMeta(data, list);
	}

	private boolean executeNativeQuery(Protocol protocol, String queryString) throws ClassRelatedException {
		try {
			executeInsertNativeQuery(protocol, queryString);
		} catch (Exception e) {
			LOGGER.error("Error executing native query", e);
			throw new ClassRelatedException("Issue in index creation for query : " + queryString, e);
		}
		return true;

	}

	public void markInactiveFormClass(com.alt.datacarrier.core.Protocol protocol, String className)
			throws ClassRelatedException {
		try {
			executeNativeQuery(protocol, "update class set status=1 where classname='" + className + "'");
		} catch (Exception e) {
			throw new ClassRelatedException("Failed to mark inactive class: " + className, e);
		}

	}

	private ClassMeta convertToClassMeta(AltClassData data, List<AltAttributeData> list) {
		ClassMeta meta = new ClassMeta();
		List<AttributeMeta> attributeMetaList = new ArrayList<>();
		for (AltAttributeData attribute : list) {
			AttributeMeta attr = new AttributeMeta();
			attr.setAttributeName(attribute.getAttributeName());
			attr.setAttributeId(Integer.valueOf((Integer) attribute.getAttributeId()).toString());
			attr.setDataType(attribute.getDataType());
			attr.setDefaultValue(attribute.getDefaultValue());
			attr.setDisplaySequence(attribute.getDisplaySequence());
			attr.setDisplayUnit(attribute.getDisplayUnit());
			attr.setIndexable(attribute.getIndexable());
			attr.setMandatory(attribute.getMandatory());
			attr.setPlaceholderType(attribute.getPlaceholderType());
			attr.setReferenceClassId(attribute.getReferenceClassId());
			attr.setSearchable(attribute.getSearchable());
			attr.setStatus(attribute.getStatus());
			attr.setUnique(attribute.getUnique1());
			attr.setOrgId(attribute.getOrgId());
			attr.setDataLakeAttributeId(attribute.getDatalakeattributeid());
			attributeMetaList.add(attr);
		}
		meta.setAttributes(attributeMetaList);
		meta.setCreatedDate(data.getCreatedDate());
		meta.setCreatedBy(data.getCreatedBy());
		meta.setCustomAttributeAllowed(data.getCustomAttributeAllowed());
		meta.setId(data.getClassId().toString());
		meta.setInstanceLimiter(data.getInstanceLimiter());
		meta.setInstancePrefix(data.getInstancePrefix());
		meta.setLocationSpecific(data.getLocationSpecific());
		meta.setModifiedDate(data.getModifiedDate());
		meta.setModifiedBy(data.getModifiedBy());
		meta.setParameterType(data.getParameterType());
		meta.setStatus(data.getStatus());
		meta.setSystemType(data.getSystemType());
		meta.setName(data.getClassName());
		meta.setTaskCodeRequired(data.getTaskCodeRequired());
		return meta;

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<ClassMeta> listAll(Protocol protocol, Set<Integer> classIds) {
		Integer orgId = protocol.getOrgId();
		javax.persistence.Query q = getEntityManager(protocol)
				.createQuery("SELECT c FROM AltClassData c WHERE c.orgId = :orgId and c.classid in (:classIds)")
				.setParameter("orgId", orgId).setParameter("classIds", classIds);
		List<AltClassData> dataList = q.getResultList();
		if (dataList == null || dataList.isEmpty()) {
			return new ArrayList<ClassMeta>();
		}
		List<ClassMeta> metaList = new ArrayList<>();
		for (AltClassData data : dataList) {
			Integer classId = data.getClassId();
			javax.persistence.Query q1 = getEntityManager(protocol)
					.createQuery("SELECT a FROM AltAttributeData a WHERE a.classData.classid = :classId"
							+ " and a.classData.orgId = :orgId")
					.setParameter("classId", classId).setParameter("orgId", protocol.getOrgId());
			List<AltAttributeData> list = q1.getResultList();
			metaList.add(convertToClassMeta(data, list));
		}
		return metaList;
	}

}

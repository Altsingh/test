package com.alt.datakernel.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datakernel.dao.IFormDS;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.model.AltApp;
import com.alt.datakernel.model.AltFormData;
import com.alt.datakernel.model.Components;

@Repository
public class AltFormDS extends AbstractDS<AltFormData> implements IFormDS {
	
	@Autowired(required = true)
	AltAppDS appDs;
	
	@Override
	@Transactional
	public AltForm add(Protocol protocol, AltFormData classData) {
		AltFormData form = (AltFormData) edit(protocol, classData);
		return convertToAltForm(form);
	}

	@Override
	@Transactional
	public AltForm updateComponents(Protocol protocol, Integer formId, List<AltAbstractComponent> componentList) {
		AltFormData form = new AltFormData();
		form = (AltFormData) find(protocol, form, formId);
		Components components = Components.builder().withComponents(componentList).build();
		form.setComponents(components);
		AltFormData formReturned = (AltFormData) edit(protocol, form);
		return convertToAltForm(formReturned);
	}

	@Override
	@Transactional(readOnly = true)
	public AltForm get(Protocol protocol, String formName, String version) {
		javax.persistence.Query q1 = getEntityManager(protocol)
				.createQuery("SELECT f FROM AltFormData f WHERE lower(f.formname) = :formName"
						+ " and f.orgId = :orgId and f.version = :version")
				.setParameter("formName", formName.toLowerCase()).setParameter("version", version)
				.setParameter("orgId", protocol.getOrgId());
		List<AltFormData> list1 = q1.getResultList();
		return convertToAltForm(list1.get(0));
	}

	@Override
	@Transactional(readOnly = true)
	public AltForm getFormFromSecurityId(Protocol protocol, String formSecurityId) {
		javax.persistence.Query q1 = getEntityManager(protocol)
				.createQuery("SELECT f FROM AltFormData f WHERE f.formsecurityid = :formsecurityid"
						+ " and f.orgId = :orgId  order by f.version  desc")
				.setParameter("formsecurityid", formSecurityId).setParameter("orgId", protocol.getOrgId());
		List<AltFormData> list1 = q1.getResultList();
		return convertToAltForm(list1.get(0));
	}

	@Override
	@Transactional(readOnly = true)
	public AltForm get(Protocol protocol, String formName) {
		AltApp altApp = appDs.getAppByAppCode(protocol.getAppCode(), protocol.getOrgId());
		javax.persistence.Query q1 = getEntityManager(protocol)
				.createQuery("SELECT f FROM AltFormData f WHERE f.formname = :formName"
						+ " and f.orgId = :orgId and f.appid = :appId order by f.formid desc")
				.setParameter("formName", formName).setParameter("appId", altApp.getAppid())
				.setParameter("orgId", protocol.getOrgId());
		List<AltFormData> list1 = q1.getResultList();
		return convertToAltForm(list1.get(0));
	}

	@Override
	@Transactional(readOnly = true)
	public List<AltForm> getAll(Protocol protocol, Integer appId) {
		List<AltForm> formList = new ArrayList<>();
		javax.persistence.Query q1 = getEntityManager(protocol)
				.createQuery("SELECT f FROM AltFormData f WHERE f.orgId=:orgId and f.appid=:appid")
				.setParameter("orgId", protocol.getOrgId()).setParameter("appid", appId);
		List<AltFormData> list1 = q1.getResultList();
		for (AltFormData form : list1) {
			formList.add(convertToAltForm(form));
		}
		return formList;
	}

	private AltForm convertToAltForm(AltFormData result) {
		AltForm form = new AltForm();
		form.setComponentList(result.getComponents().getComponents());
		form.setCreatedBy(result.getCreatedBy());
		form.setCreatedDate(result.getCreatedDate());
		form.setCustomComponentAllowed(result.getCustomComponentAllowed());
		form.setDescription(result.getDescription());
		form.setFormType(result.getFormType());
		form.setId(result.getFormId().toString());
		form.setModifiedBy(result.getModifiedBy());
		form.setModifiedDate(result.getModifiedDate());
		form.setOrgId(result.getOrgId());
		form.setStatus(result.getStatus());
		form.setUiClassName(result.getFormName());
		form.setVersion(result.getVersion());
		form.setState(result.getState());
		form.setMetaClassId(result.getMetaClassId());
		form.setFormSecurityId(result.getFormsecurityid());
		form.setAppid(result.getAppid());
		form.setParentFormId(result.getParentFormId());
		return form;
	}

	@Override
	@Transactional(readOnly = true)
	public AltForm getFromId(Protocol protocol, Integer id) throws FormRelatedException {
		AltFormData form = new AltFormData();
		// id = 938;
		form = (AltFormData) find(protocol, form, id);
		if (form == null)
			throw new FormRelatedException("Form id does not match with existing formids");
		getEntityManager().clear();
		return convertToAltForm(form);
	}

	public AltFormData getFormEntityFromMetaClassId(Protocol protocol, Integer metaClassId)
			throws FormRelatedException {
		StringBuilder sb = new StringBuilder();
		sb.append("select form from AltFormData form where form.metaClassId=:metaClassId order by formid asc");
		Query query = getEntityManager(protocol).createQuery(sb.toString());
		query.setParameter("metaClassId", metaClassId);
		// query.setParameter("state", EnumFormState.PUBLISH);
		AltFormData form = null;
		try {
			form = (AltFormData) query.getResultList().get(0);
		} catch (Exception e) {
			return null;
		}
		return form;
	}

	@Override
	@Transactional
	public AltForm updateComponentsVersionState(Protocol protocol, Integer formId,
			List<AltAbstractComponent> componentList, String version, EnumFormState state) {
		AltFormData form = new AltFormData();
		form = (AltFormData) find(protocol, form, formId);
		Components components = Components.builder().withComponents(componentList).build();
		form.setComponents(components);
		form.setState(state);
		form.setVersion(version);
		AltFormData formReturned = (AltFormData) edit(protocol, form);
		return convertToAltForm(formReturned);
	}

	@Override
	@Transactional
	public AltForm updateFormSecurity(Protocol protocol, Integer formId, List<AltAbstractComponent> componentList,
			String formSecurityId) {
		AltFormData form = new AltFormData();
		form = (AltFormData) find(protocol, form, formId);
		Components components = Components.builder().withComponents(componentList).build();
		form.setComponents(components);
		form.setFormsecurityid(formSecurityId);
		form.setModifiedby(protocol.getUserName());
		form.setModifieddate(new Date().getTime());
		AltFormData formReturned = (AltFormData) edit(protocol, form);
		return convertToAltForm(formReturned);
	}

	@Override
	@Transactional
	public AltForm updateState(Protocol protocol, Integer formId, EnumFormState state, ClassMeta classMeta) {
		AltFormData form = new AltFormData();
		form = (AltFormData) find(protocol, form, formId);
		form.setState(state);
		if (null != classMeta) {
			form.setMetaClassId(Integer.parseInt(classMeta.getId()));
		}
		AltFormData formReturned = (AltFormData) edit(protocol, form);
		return convertToAltForm(formReturned);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<AltForm> listByIds(Protocol protocol, List<Integer> formIds) {
		StringBuilder sb = new StringBuilder();
		sb.append("select form from AltFormData form where form.formid in (:formIds)");
		Query query = getEntityManager(protocol).createQuery(sb.toString());
		query.setParameter("formIds", formIds);
		List<AltFormData> forms = query.getResultList();
		List<AltForm> convertedForms = new ArrayList<AltForm>();
		for (AltFormData form : forms) {
			convertedForms.add(convertToAltForm(form));
		}
		return convertedForms;
	}

	@Override
	@Transactional(readOnly = true)
	public List<AltForm> listByNames(Protocol protocol, Collection<String> formNames) {
		StringBuilder sb = new StringBuilder();
		sb.append("select form from AltFormData form where form.formname in (:formNames) order by form.formid asc");
		Query query = getEntityManager(protocol).createQuery(sb.toString());
		query.setParameter("formNames", formNames);
		List<AltFormData> forms = query.getResultList();
		List<AltForm> convertedForms = new ArrayList<AltForm>();
		for (AltFormData form : forms) {
			convertedForms.add(convertToAltForm(form));
		}
		return convertedForms;
	}

	@Override
	@Transactional(readOnly = true)
	public AltFormData findForm(Protocol protocol, AltFormData altFormData, String formid) {
		return find(protocol, altFormData, Integer.parseInt(formid));
	}

	@Override
	@Transactional
	public void editForm(AltFormData form) {
		edit(form);
	}
}

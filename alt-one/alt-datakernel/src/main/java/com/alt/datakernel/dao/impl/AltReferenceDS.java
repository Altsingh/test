package com.alt.datakernel.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.ReferenceData;
import com.alt.datakernel.dao.IReferenceDS;
import com.alt.datakernel.dao.converter.AltAttributeConverter;
import com.alt.datakernel.dao.converter.AltReferenceConverter;
import com.alt.datakernel.exception.ReferenceRelatedException;
import com.alt.datakernel.model.AltAttributeData;
import com.alt.datakernel.model.AltReferenceData;
import com.alt.datakernel.model.BaseModel;

@Repository
public class AltReferenceDS extends AbstractDS<BaseModel> implements IReferenceDS {

	@Override
	public List<AttributeMeta> getAllAttributesByClassID(Protocol protocol, Integer metaClassID)
			throws ReferenceRelatedException {
		List<AltAttributeData> attributesList = null;
		List<AttributeMeta> attributesMetaList = null;
		try {
			javax.persistence.Query q2 = getEntityManager(protocol)
					.createQuery("select atr from  AltAttributeData atr "
							+ "left join AltReferenceData ref "
							+ "on ref.referenceByClass.classid = atr.classData.classid "
							+ "and ref.referenceByAttribute.attributeid = atr.attributeid "
							+ "where atr.orgId = :orgId and ref.classData.classid = :classId")
					.setParameter("classId", metaClassID).setParameter("orgId", protocol.getOrgId());
			attributesList = q2.getResultList();
			if (attributesList != null && attributesList.size() > 0) {
				attributesMetaList = new ArrayList<>();
				for (AltAttributeData data : attributesList) {
					attributesMetaList.add(AltAttributeConverter.convertAltAttributeEntityToMetaTO(data));
				}
			}
		} catch (Exception ex) {
			throw ex;
		}
		return attributesMetaList;
	}

	@Override
	public ReferenceData getDataByReferenceID(Protocol protocol, Integer id) throws ReferenceRelatedException {
		AltReferenceData refDataFetched = new AltReferenceData();
		refDataFetched = (AltReferenceData) find(protocol, refDataFetched, id);
		if (refDataFetched == null)
			throw new ReferenceRelatedException("Reference id does not match with existing referenceIds");
		getEntityManager().clear();
		return AltReferenceConverter.convertAltReferenceEntityToAltReferenceData(refDataFetched);
	}

}

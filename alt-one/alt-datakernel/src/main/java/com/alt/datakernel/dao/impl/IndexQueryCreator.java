package com.alt.datakernel.dao.impl;

public class IndexQueryCreator {

	private IndexQueryCreator() {
		
	}
	
	public static String create(String table, String column, String className, String org, String attribute) {
		return "Create index " + table + "_" + column + "_" + attribute + "_" + className + "_"+ org + " on "
				+ table + " using gin (( " + column + " -> 'attribute' -> '" + attribute + "' ))";
		
	}
	
}

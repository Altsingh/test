package com.alt.datakernel.dao.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datakernel.dao.IInstanceDS;
import com.alt.datakernel.dao.IInstanceHistoryDS;
import com.alt.datakernel.dao.converter.InstanceHistoryUtil;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.model.Attributes;
import com.alt.datakernel.model.BaseModel;
import com.alt.datakernel.model.Instance;
import com.alt.datakernel.model.InstanceHistory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository
public class InstanceDS extends AbstractDS<BaseModel> implements IInstanceDS {

	private static final Logger logger = Logger.getLogger(InstanceDS.class);

	@Autowired(required = true)
	private IInstanceHistoryDS iInstanceHistoryDS;

	private String instanceTable = null;

	@Override
	// @Transactional
	public Instance persistInstance(Instance instance, Protocol protocol, boolean processHistory)
			throws InstanceRelatedException, InstanceHistoryRelatedException {

		boolean isCreate = false;
		if (instance.getId() == null || instance.getId() <= 0) {
			isCreate = true;
		}

		List<Instance> savedFormDataList = null;
		/**
		 * If below flag is true, this signifies that the persistence is in edit mode.
		 * For editing first, get the already persisted data
		 */
		if (!isCreate) {
			savedFormDataList = getSavedFormDataByContext(instance, protocol);
		}

		if (savedFormDataList != null && !savedFormDataList.isEmpty()) {
			if (processHistory) {
				boolean isHistorySaved = false;
				List<InstanceHistory> instanceHistoryList = InstanceHistoryUtil
						.getInstanceHistoryRequestFromInstanceObject(savedFormDataList);

				if (instanceHistoryList != null && !instanceHistoryList.isEmpty()) {
					List<InstanceHistory> history = persistInstanceHistory(instanceHistoryList, protocol);
					if (!history.isEmpty()) {
						isHistorySaved = true;
					} else {
						isHistorySaved = false;
					}
				} else {
					isHistorySaved = false;
				}

				if (!isHistorySaved) {
					throw new InstanceHistoryRelatedException("Exception thrown while saving instance history");
				} else {
					logger.warn("instance history saved");
				}
			}

			logger.warn("Editing : object found for updation");
			if (instance.getAttributes() != null)
				savedFormDataList.get(0).setAttributes(instance.getAttributes());

			if (instance.getStatus() != null)
				savedFormDataList.get(0).setStatus(instance.getStatus());

			savedFormDataList.get(0).setModifiedDate(System.currentTimeMillis());
			return (Instance) saveOrUpdateInstance(protocol, savedFormDataList.get(0));
		} else {
			logger.warn("Persisting : object not found for updation");
			return (Instance) saveOrUpdateInstance(protocol, instance);
		}
	}

	@Override
	@Transactional
	@Deprecated
	public Instance editApp(Instance instance, Protocol protocol)
			throws InstanceRelatedException, InstanceHistoryRelatedException {

		List<Instance> savedFormDataList = new ArrayList<>();
		savedFormDataList.add(instance);

		boolean isHistorySaved = false;
		List<InstanceHistory> instanceHistoryList = InstanceHistoryUtil
				.getInstanceHistoryRequestFromInstanceObject(savedFormDataList);

		if (instanceHistoryList != null && !instanceHistoryList.isEmpty()) {
			List<InstanceHistory> history = persistInstanceHistory(instanceHistoryList, protocol);
			if (!history.isEmpty()) {
				isHistorySaved = true;
			} else {
				isHistorySaved = false;
			}
		} else {
			isHistorySaved = false;
		}

		if (!isHistorySaved) {
			throw new InstanceHistoryRelatedException("Exception thrown while saving instance history");
		} else {
			logger.warn("instance history saved");
		}

		return (Instance) edit(protocol, savedFormDataList.get(0));

	}

	@Transactional
	public List<InstanceHistory> persistInstanceHistory(List<InstanceHistory> instanceHistoryList, Protocol protocol)
			throws InstanceHistoryRelatedException {
		List<InstanceHistory> instanceHistorySaved = new ArrayList<>();
		for (InstanceHistory insHistory : instanceHistoryList) {
			instanceHistorySaved.add(iInstanceHistoryDS.persistInstanceHistory(insHistory, protocol));
		}
		return instanceHistorySaved;
	}

	@Override
	@Transactional(readOnly = true)
	public Instance find(Protocol protocol, Integer id) {
		String instanceTable = getInstanceTableName(protocol);
		String result = getEntityManager(protocol)
				.createNativeQuery(
						"SELECT cast(row_to_json(c) as text) FROM " + instanceTable + " c WHERE c.id = " + id)
				.getSingleResult().toString();
		// .setParameter("id", id);
		Instance instance = null;
		try {
			instance = new ObjectMapper().readValue(result, Instance.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// instance = (Instance) find(protocol, instance, id);
		return instance;

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<Instance> getSavedFormDataByContext(Instance instance, Protocol protocol)
			throws InstanceRelatedException {
		javax.persistence.Query q = null;
		List<Instance> instanceList = null;
		String instanceTable = getInstanceTableName(protocol);

		// When directly instance id is passed
		if (instance.getId() != null && instance.getId() > 0) {

			Instance instc = find(protocol, instance.getId());
			if (instc.getAttributes() != null) {
				instanceList = new ArrayList<>();
				instanceList.add(instc);
			}
		} else {
			// when classname is not null
			if (instance.getClassName() != null && !instance.getClassName().equalsIgnoreCase("")) {
				q = getEntityManager(protocol).createQuery("SELECT c FROM " + instanceTable
						+ " c WHERE c.classname = :classname AND"
						+ " c.userid = :userid AND c.status = :status AND c.formname = :formname AND c.formid = :formid AND c.appid = :appid AND c.organizationid = :organizationid")
						.setParameter("classname", instance.getClassName()).setParameter("formid", instance.getFormId())
						.setParameter("appid", instance.getAppId()).setParameter("formname", instance.getFormname())
						.setParameter("status", KernelConstants.EnumDocStatusType.ACTIVE)
						.setParameter("organizationid", instance.getOrganizationId())
						.setParameter("userid", protocol.getUserName());

				instanceList = q.getResultList();
			} else {

				if (instance.getFormId() != null && instance.getFormId() > 0) {
					q = getEntityManager(protocol).createQuery("SELECT c FROM " + instanceTable + " c WHERE"
							+ " c.userid = :userid AND c.formname = :formname AND c.status = :status AND c.formid = :formid AND c.appid = :appid AND c.organizationid = :organizationid")
							.setParameter("formid", instance.getFormId()).setParameter("appid", instance.getAppId())
							.setParameter("formname", instance.getFormname())
							.setParameter("organizationid", instance.getOrganizationId())
							.setParameter("status", KernelConstants.EnumDocStatusType.ACTIVE)
							.setParameter("userid", protocol.getUserName());
				} else {
					q = getEntityManager(protocol).createQuery("SELECT c FROM " + instanceTable + " c WHERE"
							+ " c.userid = :userid AND c.formname = :formname AND c.status = :status AND c.appid = :appid AND c.organizationid = :organizationid")
							.setParameter("formname", instance.getFormname()).setParameter("appid", instance.getAppId())
							.setParameter("organizationid", instance.getOrganizationId())
							.setParameter("status", KernelConstants.EnumDocStatusType.ACTIVE)
							.setParameter("userid", protocol.getUserName());
				}

				instanceList = q.getResultList();
			}
		}

		logger.warn(instanceList.toString());
		return instanceList;
	}

	@Override
	public List<Object[]> executeNativeQuery(Protocol protocol, String query) {
		List<Object[]> resultList = getEntityManager().createNativeQuery(query).getResultList();
		return resultList;
	}

	@Override
	@Transactional(readOnly = true)
	/**
	 * @author vaibhav.a1
	 * */
	public List<Instance> findAllInstancesForAClassId(Protocol protocol, Integer id, String className,
			Map<String, String> attributes) {
		Instance instance = new Instance();
		String instanceTable = getInstanceTableName(protocol);
		javax.persistence.Query q = null;
		String result = "";
		if ((className != null && !className.equalsIgnoreCase("")) || (id != null && id > 0)
				|| (attributes != null && attributes.size() > 0)) {
			if (id != null && id > 0) {
				result = getEntityManager(protocol)
						.createNativeQuery(
								"SELECT cast(row_to_json(c) as text) FROM " + instanceTable + " c WHERE c.classid = "
										+ id + " AND c.status = 0 AND c.organizationid = " + protocol.getOrgId())
						.getResultList().toString();
			} else if (className != null && !className.equalsIgnoreCase("")
					|| (attributes != null && attributes.size() > 0)) {
				if ((className != null && !className.equalsIgnoreCase(""))
						&& (attributes != null && attributes.size() > 0)) {
					String attributesMap = "";
					for (Map.Entry<String, String> attrMap : attributes.entrySet()) {
						String key = (String) attrMap.getKey();
						String value = (String) attrMap.getValue();
						attributesMap = attributesMap + " attributes->'attribute'->>'" + key + "'='" + value + "' AND ";
					}
					result = getEntityManager(protocol)
							.createNativeQuery("SELECT cast(row_to_json(c) as text) FROM " + instanceTable + " c WHERE "
									+ attributesMap + " c.classname = '" + className
									+ "' AND c.status = 0 AND c.organizationid = " + protocol.getOrgId())
							.getResultList().toString();
				} else {
					result = getEntityManager(protocol)
							.createNativeQuery("SELECT cast(row_to_json(c) as text) FROM " + instanceTable
									+ " c WHERE c.classname = '" + className
									+ "' AND c.status = 0 AND c.organizationid = " + protocol.getOrgId())
							.getResultList().toString();
				}

			}

		} else {
			result = getEntityManager(protocol).createNativeQuery("SELECT cast(row_to_json(c) as text) FROM "
					+ instanceTable + " c WHERE " + " c.status = 0 AND c.organizationid = " + protocol.getOrgId())
					.getResultList().toString();
		}

		List<Instance> instanceList = null;
		try {
			instanceList = new ObjectMapper().readValue(result, new TypeReference<List<Instance>>() {
			});
		} catch (IOException e) {
			instanceList = null;
			e.printStackTrace();
		}
		return instanceList;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Instance> findAllInstancesForAClass(Protocol protocol, String className) {
		Instance instance = new Instance();
		String instanceTable = getInstanceTableName(protocol);
		javax.persistence.Query q;
		if (className.equals("-1")) {
			q = getEntityManager(protocol)
					.createQuery("SELECT c FROM " + instanceTable
							+ " c WHERE c.organizationid = :orgId and c.status = :status")
					.setParameter("status", KernelConstants.EnumDocStatusType.ACTIVE)
					.setParameter("orgId", protocol.getOrgId());
		} else {
			q = getEntityManager(protocol)
					.createQuery("SELECT c FROM " + instanceTable
							+ " c WHERE c.classname = :class AND c.organizationid = :orgId AND c.status = :status")
					.setParameter("class", className).setParameter("status", KernelConstants.EnumDocStatusType.ACTIVE)
					.setParameter("orgId", protocol.getOrgId());
		}
		return (List<Instance>) findWithParameters(protocol, instance, q);
	}

	@Override
	@Transactional
	public Instance updateAttributes(Protocol protocol, Integer id, Map<String, Object> attributes) {
		Instance instance = new Instance();
		instance = (Instance) find(protocol, instance, id);
		instance.setAttributes(new Attributes(attributes));
		return (Instance) saveOrUpdateInstance(protocol, instance);
	}

	@Transactional
	public Instance saveOrUpdateInstance(Protocol protocol, Instance instance) {
		String instanceTable;
		if (instance.getClassName().equalsIgnoreCase("FormMapping")) {
			instanceTable = "Instance";
		} else {
			instanceTable = getInstanceTableName(protocol);
		}

		if (instance.getId() == null) {
			try {
				Query q = getEntityManager().createNativeQuery("insert into " + instanceTable
						+ " ( classname, classid, organizationid, appid, formid, userid,formname,attributes, modifiedby, createdby, modifieddate, createddate, status )"
						+ " VALUES ( :a, :b, :c, :d, :e, :f, :g, cast(:h as json), :i, :j, :k, :l, :m ) returning ID")
						.setParameter("a", instance.getClassName())
						.setParameter("b", instance.getClassData().getClassId())
						.setParameter("c", instance.getOrganizationId()).setParameter("d", instance.getAppId())
						.setParameter("e", instance.getFormId()).setParameter("f", instance.getUserId())
						.setParameter("g", instance.getFormname())
						.setParameter("h", new ObjectMapper().writeValueAsString(instance.getAttributes()))
						.setParameter("i", instance.getModifiedBy()).setParameter("j", instance.getCreatedBy())
						.setParameter("k", instance.getModifiedDate()).setParameter("l", instance.getCreatedDate())
						.setParameter("m", 0);
				Integer instanceId = (Integer) q.getSingleResult();
				// long id = instanceId.longValue();
				instance.setId(instanceId);
			} catch (JsonProcessingException e) {
				logger.error("Error processing attributes json", e);
			}
		} else {
			try {
				Query q = getEntityManager().createNativeQuery("UPDATE " + instanceTable
						+ " set classname = :a, organizationid =:c,"
						+ " appid = :d, formid =:e, userid=:f, formname=:g, attributes=cast(:h as json), modifiedby=:i, modifieddate=:j, status=:k "
						+ " where id=:l").setParameter("a", instance.getClassName())
						// .setParameter("b", instance.getClassData().getClassId())
						.setParameter("c", instance.getOrganizationId()).setParameter("d", instance.getAppId())
						.setParameter("e", instance.getFormId()).setParameter("f", instance.getUserId())
						.setParameter("g", instance.getFormname())
						.setParameter("h", new ObjectMapper().writeValueAsString(instance.getAttributes()))
						.setParameter("i", instance.getModifiedBy()).setParameter("j", instance.getModifiedDate())
						.setParameter("k", instance.getStatus().equals(EnumDocStatusType.ACTIVE) ? 0 : 1)
						.setParameter("l", instance.getId());
				q.executeUpdate();
			} catch (Exception e) {
				logger.error("Error processing attributes json while editing instance", e);
			}

		}
		// getEntityManager().flush();
		return instance;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Object[]> getAllAttributeValuesByAttributeName(String attributeName, String classId, Protocol protocol)
			throws InstanceRelatedException {
		Instance instance = null;
		String instanceTable = getInstanceTableName(protocol);
		List<Object[]> attributeValues = null;
		try {
			javax.persistence.Query q2 = getEntityManager(protocol)
					.createNativeQuery("select i.id, (i.attributes->'attribute'->>'" + attributeName + "') from "
							+ instanceTable + " i "
							+ "where i.organizationid = :orgId and i.status = :status and i.classid = :classId")
					.setParameter("classId", Integer.parseInt(classId)).setParameter("status", 0)
					.setParameter("orgId", protocol.getOrgId());
			attributeValues = q2.getResultList();
		} catch (Exception ex) {
			logger.error("Error getting attributes for class:" + classId, ex);
		}
		return attributeValues;
	}

	private String getJson() {
		String[] chars = new String[26];
		int i = 0;
		for (Character c = 'a'; c <= 'z'; c++) {
			chars[i++] = c.toString();
		}
		String key = chars[(int) (26 * Math.random())];
		String value = chars[(int) (26 * Math.random())];
		return "{\"" + key + "\":\"" + value + "\"}";
	}

	@Override
	@Transactional(readOnly=true)
	public Query createNativeQuery(Protocol protocol, String query) {
		return getEntityManager().createNativeQuery(query);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Object[]> executeQueryForFetchingData(Protocol protocol, Query query) {
		return (List<Object[]>) query.getResultList();
	}

	@Transactional(readOnly = true)
	public String getInstanceTableName(Protocol protocol) {
		// String instanceTable = null;
		if (instanceTable == null) {
			if (protocol.getAppCode() != null) {
				Query q = getEntityManager(protocol).createNativeQuery(
						"SELECT c.instancetable FROM instancemaster c WHERE c.appCode = '" + protocol.getAppCode()
								+ "' and c.organizationid = '" + protocol.getOrgId() + "' and status = true");
				try {
					instanceTable = (String) q.getSingleResult();
				} catch (NoResultException nre) {
					// Ignore this because as per your logic this is ok!
				}
				if (instanceTable != null) {
					logger.warn("App level instance table found:" + instanceTable);
					return instanceTable;
				}
			}
			if (protocol.getTenantId() != null && protocol.getOrgId() != null) {
				Query q = getEntityManager(protocol)
						.createNativeQuery("SELECT c.instancetable FROM instancemaster c WHERE c.organizationid = '"
								+ protocol.getOrgId() + "' and c.appCode is null and status = true");
				try {
					instanceTable = (String) q.getSingleResult();
				} catch (NoResultException nre) {
					// Ignore this because as per your logic this is ok!
				}
				if (instanceTable != null) {
					logger.warn("Organization level instance table found:" + instanceTable);
					return instanceTable;
				}
			}
			Query q = getEntityManager(protocol)
					.createNativeQuery("SELECT c.instancetable FROM instancemaster c WHERE c.organizationid = null "
							+ "and c.appCode = null and c.tenantid = null and status=true");
			// .setParameter("organizationid", null)
			// .setParameter("appCode",null)
			// .setParameter("tenantid",null);

			try {
				instanceTable = (String) q.getSingleResult();
			} catch (NoResultException nre) {
				// Ignore this because as per your logic this is ok!
			}
			if (instanceTable != null) {
				logger.warn("Default instance table found:" + instanceTable);
				return instanceTable;
			}
			instanceTable = "Instance";
		}
		logger.warn("No instance table found, using default instance table as:" + instanceTable);
		return instanceTable;
	}
}

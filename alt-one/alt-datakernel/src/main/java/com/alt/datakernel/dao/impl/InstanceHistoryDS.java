package com.alt.datakernel.dao.impl;

import com.alt.datacarrier.core.Protocol;
import com.alt.datakernel.dao.IInstanceHistoryDS;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.model.BaseModel;
import com.alt.datakernel.model.InstanceHistory;

public class InstanceHistoryDS extends AbstractDS<BaseModel> implements IInstanceHistoryDS {

	@Override
	public InstanceHistory persistInstanceHistory(InstanceHistory instanceHistory, Protocol protocol)
			throws InstanceHistoryRelatedException {
		return (InstanceHistory) edit(instanceHistory);
	}

}

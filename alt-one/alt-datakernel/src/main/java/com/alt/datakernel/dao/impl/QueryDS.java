package com.alt.datakernel.dao.impl;

import java.math.BigInteger;
import java.util.List;

import com.alt.datacarrier.core.Protocol;
import com.alt.datakernel.dao.IQueryDS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.model.BaseModel;

@Repository
public class QueryDS extends AbstractDS<BaseModel> implements IQueryDS {

	private Logger logger = LoggerFactory.getLogger(QueryDS.class);

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Object[]> executeQuery(Protocol protocol, String queryString) throws QueryRelatedException {
		List<Object[]> result = null;
		try {
			 result = getEntityManager(protocol).createNativeQuery(queryString).getResultList();
		}catch(Exception e) {
			throw new QueryRelatedException("Issue in query execution : "+ queryString, e);
		}
		return result;
	}
	
	@Override
	@Transactional
	public BigInteger executeCountQuery(Protocol protocol, String queryString) throws QueryRelatedException {
		BigInteger result;
		try {
			 result = (BigInteger) getEntityManager(protocol).createNativeQuery(queryString).getSingleResult();
		}catch(Exception e) {
			throw new QueryRelatedException("Issue in query execution : "+ queryString, e);
		}
		return result;
	}
	
}

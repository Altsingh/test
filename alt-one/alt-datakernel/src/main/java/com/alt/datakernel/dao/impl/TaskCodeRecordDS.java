/**
 * @author vaibhav.kashyap
 * */
package com.alt.datakernel.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.request.TaskCodeRecordTransport;
import com.alt.datakernel.dao.ITaskCodeRecordDS;
import com.alt.datakernel.exception.TaskCodeRecordRelatedException;
import com.alt.datakernel.model.BaseModel;
import com.alt.datakernel.model.TaskCodeRecord;

@Repository
public class TaskCodeRecordDS extends AbstractDS<BaseModel> implements ITaskCodeRecordDS {

	private Logger LOGGER = LoggerFactory.getLogger(TaskCodeRecordDS.class);

	@Override
	@Transactional
	public TaskCodeRecordTransport add(TaskCodeRecord taskCodeRecord, Protocol protocol)
			throws TaskCodeRecordRelatedException {
		taskCodeRecord = (TaskCodeRecord) edit(protocol, taskCodeRecord);
		return convertEntityToTransferObject(taskCodeRecord);
	}

	@Override
	@Transactional
	public TaskCodeRecordTransport update(TaskCodeRecord taskCodeRecord, Protocol protocol)
			throws TaskCodeRecordRelatedException {
		TaskCodeRecord recordFetched = find(protocol, taskCodeRecord.getTaskcoderecordid());
		if (recordFetched.getTaskcoderecordid() != null) {
			recordFetched.setLastcodeused(taskCodeRecord.getLastcodeused());
			recordFetched = (TaskCodeRecord) edit(protocol, recordFetched);
		}
		return (recordFetched.getTaskcoderecordid() != null) ? convertEntityToTransferObject(recordFetched) : null;
	}

	@Override
	public TaskCodeRecordTransport get(TaskCodeRecord taskCodeRecord) throws TaskCodeRecordRelatedException {
		javax.persistence.Query q;

		q = getEntityManager()
				.createQuery("SELECT tcr FROM TaskCodeRecord tcr WHERE tcr.appid = :appId and tcr.classid = :classId")
				.setParameter("appId", taskCodeRecord.getAppid()).setParameter("classId", taskCodeRecord.getClassid());
		TaskCodeRecord record = null;

		try {
			record = (TaskCodeRecord) q.getSingleResult();
		} catch (Exception e) {
			LOGGER.error("Task code fetch failed for taskcoderecord: "+taskCodeRecord.toString(),e);
			return null;
		}

		return (record != null) ? convertEntityToTransferObject(record) : null;
	}

	@Override
	@Transactional
	public TaskCodeRecordTransport delete(TaskCodeRecord taskCodeRecord) throws TaskCodeRecordRelatedException {
		// TODO Auto-generated method stub
		return null;
	}

	private TaskCodeRecordTransport convertEntityToTransferObject(TaskCodeRecord taskCodeRecord) {
		if (taskCodeRecord == null) {
			return null;
		}

		TaskCodeRecordTransport transport = new TaskCodeRecordTransport();

		if (taskCodeRecord.getAppid() != null && taskCodeRecord.getAppid() > 0)
			transport.setAppId(taskCodeRecord.getAppid());
		if (taskCodeRecord.getClassid() != null && !taskCodeRecord.getClassid().equals(""))
			transport.setClassId(taskCodeRecord.getClassid());
		if (taskCodeRecord.getFormid() != null && !taskCodeRecord.getFormid().equals(""))
			transport.setFormId(taskCodeRecord.getFormid());
		if (taskCodeRecord.getForminitials() != null && !taskCodeRecord.getForminitials().equals(""))
			transport.setFormInitials(taskCodeRecord.getForminitials());
		if (taskCodeRecord.getLastcodeused() != null && !taskCodeRecord.getLastcodeused().equals(""))
			transport.setLastCodeUsed(taskCodeRecord.getLastcodeused());
		transport.setTaskCodeID(taskCodeRecord.getTaskcoderecordid());

		return transport;

	}

	@Override
	@Transactional(readOnly=true)
	public TaskCodeRecord find(Protocol protocol, Integer id) {
		TaskCodeRecord record = new TaskCodeRecord();
		record = (TaskCodeRecord) find(protocol, record, id);
		return record;
	}

}

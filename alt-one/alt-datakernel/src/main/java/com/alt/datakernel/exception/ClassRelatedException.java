package com.alt.datakernel.exception;

public class ClassRelatedException extends Exception{

	private static final long serialVersionUID = 2334528391288066045L;

	public ClassRelatedException(String message) {
		super(message);
	}

	public ClassRelatedException(String message, Throwable e) {
		super(message, e);
	}
}

package com.alt.datakernel.exception;

public class ErrorConstants {

    public static final String NULL_CHECK_FAIL = "Null check validation failed";
    public static final String ORG_NAME_EMPTY ="Organization Code cannot be empty";
    public static final String CLASS_NAME_EMPTY ="Class name cannot be empty";
    public static final String VALIDATION_FAILED ="Request validation failed";
    public static final String CLASS_META_READ_FAILED = "Class meta read failed";
    public static final String REFERENCE_VALUE_VALIDATION_FAILED = "Reference value is invalid";
    public static final String REFERENCE_VALUE_MISMATCH = "Reference value is not an instance of reference class";
    public static final String REFERENCE_VALUE_INACTIVE = "Reference value is inactive";
    public static final String DUPLICATE_VALUE = "Attribute value already exist";
    public static final String VALUE_NULL = "Attribute value cannot be null";
    public static final String READ_FAIL = "Data read failed";
}

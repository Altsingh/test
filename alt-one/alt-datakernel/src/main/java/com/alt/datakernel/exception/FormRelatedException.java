package com.alt.datakernel.exception;

public class FormRelatedException extends Exception{

	private static final long serialVersionUID = 187693505496344281L;

	public FormRelatedException(String message) {
		super(message);
	}

	public FormRelatedException(String message, Throwable e) {
		super(message, e);
	}
}

package com.alt.datakernel.exception;

public class InstanceHistoryRelatedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InstanceHistoryRelatedException(String message) {
		super(message);
	}

	public InstanceHistoryRelatedException(String message, Throwable throwable) {
		super(message, throwable);
	}
}

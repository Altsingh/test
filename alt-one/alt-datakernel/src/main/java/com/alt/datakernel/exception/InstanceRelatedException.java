package com.alt.datakernel.exception;

public class InstanceRelatedException extends Exception{

	private static final long serialVersionUID = -2948729715926750036L;

	public InstanceRelatedException(String message) {
		super(message);
	}

	public InstanceRelatedException(String message, Throwable e) {
		super(message, e);
	}
}

package com.alt.datakernel.exception;

public class MailRelatedException extends Exception {

	private static final long serialVersionUID = -2948729715926750036L;

	public MailRelatedException(String message) {
		super(message);
	}

	public MailRelatedException(String message, Throwable e) {
		super(message, e);
	}
}

package com.alt.datakernel.exception;

public class QueryRelatedException extends Exception{

	private static final long serialVersionUID = -4323033870654258196L;

	public QueryRelatedException(String message) {
		super(message);
	}

	public QueryRelatedException(String message, Throwable e) {
		super(message, e);
	}
}

package com.alt.datakernel.exception;

public class ReferenceRelatedException extends Exception {
	private static final long serialVersionUID = 2334528391288066045L;

	public ReferenceRelatedException(String message) {
		super(message);
	}

	public ReferenceRelatedException(String message, Throwable e) {
		super(message, e);
	}
}

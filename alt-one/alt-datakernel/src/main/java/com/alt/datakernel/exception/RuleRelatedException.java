package com.alt.datakernel.exception;

public class RuleRelatedException extends Exception {

	private static final long serialVersionUID = -2948729715926750036L;

	public RuleRelatedException(String message) {
		super(message);
	}

	public RuleRelatedException(String message, Throwable e) {
		super(message, e);
	}
}

package com.alt.datakernel.exception;

public class TaskCodeRecordRelatedException extends Exception {

	private static final long serialVersionUID = 2334528391288066045L;

	public TaskCodeRecordRelatedException(String message) {
		super(message);
	}

	public TaskCodeRecordRelatedException(String message, Throwable e) {
		super(message, e);
	}
}

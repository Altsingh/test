package com.alt.datakernel.model;

import com.alt.datacarrier.kernel.common.KernelConstants;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "app")
public class AltApp {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="appid")
    private Integer appid;

    private String appname;
    private String appcode;

    @Column
    @Type(type = "ModulesJsonType")
    private Modules modules;

    @Column
    @Type(type = "HomeCardsJsonType")
    private HomeCards homecards;
    private Integer organizationid;
    private KernelConstants.EnumDocStatusType status;
    private boolean isdeleted;
    private String createdby;
    private Long createddate;
    private String modifiedby;
    private Long modifieddate;
    //private String publickey;
    private String applogourl;


    public static final class AltAppBuilder {
        private Integer appid;
        private String appName;
        private String appCode;
        private Modules modules;
        private HomeCards homeCards;
        private Integer organizationId;
        private KernelConstants.EnumDocStatusType status;
        private boolean isDeleted;
        private String createdBy;
        private Long createdDate;
        private String modifiedBy;
        private Long modifiedDate;
        private String uuid;
        private String appLogoUrl;

        private AltAppBuilder() {
        }

        public static AltAppBuilder anAltApp() {
            return new AltAppBuilder();
        }

        public AltAppBuilder withAppid(Integer appid) {
            this.appid = appid;
            return this;
        }

        public AltAppBuilder withAppName(String appName) {
            this.appName = appName;
            return this;
        }

        public AltAppBuilder withAppCode(String appCode) {
            this.appCode = appCode;
            return this;
        }

        public AltAppBuilder withModules(Modules modules) {
            this.modules = modules;
            return this;
        }

        public AltAppBuilder withHomeCards(HomeCards homeCards) {
            this.homeCards = homeCards;
            return this;
        }

        public AltAppBuilder withOrganizationId(Integer organizationId) {
            this.organizationId = organizationId;
            return this;
        }

        public AltAppBuilder withStatus(KernelConstants.EnumDocStatusType status) {
            this.status = status;
            return this;
        }

        public AltAppBuilder withIsDeleted(boolean isDeleted) {
            this.isDeleted = isDeleted;
            return this;
        }

        public AltAppBuilder withCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public AltAppBuilder withCreatedDate(Long createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public AltAppBuilder withModifiedBy(String modifiedBy) {
            this.modifiedBy = modifiedBy;
            return this;
        }

        public AltAppBuilder withModifiedDate(Long modifiedDate) {
            this.modifiedDate = modifiedDate;
            return this;
        }

        public AltAppBuilder withUuid(String uuid) {
            this.uuid = uuid;
            return this;
        }

        public AltAppBuilder withAppLogoUrl(String logo) {
            this.appLogoUrl = logo;
            return this;
        }

        public AltApp build() {
            AltApp altApp = new AltApp();
            altApp.homecards = this.homeCards;
            altApp.isdeleted = this.isDeleted;
            altApp.modules = this.modules;
            altApp.organizationid = this.organizationId;
            //altApp.publickey = this.uuid;
            altApp.createdby = this.createdBy;
            altApp.modifieddate = this.modifiedDate;
            altApp.createddate = this.createdDate;
            altApp.appname = this.appName;
            altApp.modifiedby = this.modifiedBy;
            altApp.appcode = this.appCode;
            altApp.appid = this.appid;
            altApp.status = this.status;
            altApp.applogourl = this.appLogoUrl;
            return altApp;
        }
    }

    public Integer getAppid() {
        return appid;
    }

    public String getAppname() {
        return appname;
    }

    public String getAppcode() {
        return appcode;
    }

    public Modules getModules() {
        return modules;
    }

    public HomeCards getHomecards() {
        return homecards;
    }

    public Integer getOrganizationid() {
        return organizationid;
    }

    public KernelConstants.EnumDocStatusType isStatus() {
        return status;
    }

    public boolean isIsdeleted() {
        return isdeleted;
    }

    public String getCreatedby() {
        return createdby;
    }

    public Long getCreateddate() {
        return createddate;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public Long getModifieddate() {
        return modifieddate;
    }

    /*public String getPublickey() {
        return publickey;
    }*/

    public String getApplogourl() {
        return applogourl;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public void setAppcode(String appcode) {
        this.appcode = appcode;
    }

    public void setModules(Modules modules) {
        this.modules = modules;
    }

    public void setHomecards(HomeCards homecards) {
        this.homecards = homecards;
    }

    public void setOrganizationid(Integer organizationid) {
        this.organizationid = organizationid;
    }

    public void setStatus(KernelConstants.EnumDocStatusType status) {
        this.status = status;
    }

    public void setIsdeleted(boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public void setCreateddate(Long createddate) {
        this.createddate = createddate;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public void setModifieddate(Long modifieddate) {
        this.modifieddate = modifieddate;
    }

    public void setApplogourl(String applogourl) {
        this.applogourl = applogourl;
    }
}

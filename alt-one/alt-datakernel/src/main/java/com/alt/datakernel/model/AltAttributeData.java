package com.alt.datakernel.model;

import java.io.Serializable;

import javax.persistence.*;

import com.alt.datacarrier.common.enumeration.EnumDataType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;

@Entity
@Table(name = "attribute")
public class AltAttributeData extends BaseModel implements Serializable {

	private static final long serialVersionUID = -5949885195840591175L;

	public void setAttributeid(Integer attributeid) {
		this.attributeid = attributeid;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer attributeid;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "classid")
	private AltClassData classData;

	private String attributename;

	private EnumDataType datatype; // REFERENCE, DATE, STRING, DOUBLE, INTEGER etc

	private Integer referenceclassid; // When dataType is REFERENCE

	private EnumDocStatusType status;

	private Boolean placeholdertype;

	private String defaultvalue;

	private Boolean unique1;

	private Boolean mandatory;

	private Boolean searchable;

	private Boolean indexable;

	private Boolean displayunit;

	private Integer displaysequence;

	private String org;

	private Long createddate;

	private Long modifieddate;

	private String createdby;

	private String modifiedby;

	@Column(name="orgid")
	private Integer orgId;

	private Integer datalakeattributeid;

	public AltAttributeData(Builder builder) {
		this.attributeid = builder.attributeId;
		this.classData = builder.classData;
		this.datatype = builder.dataType;
		this.referenceclassid = builder.referenceClassId;
		this.status = builder.status;
		this.placeholdertype = builder.placeholderType;
		this.defaultvalue = builder.defaultValue;
		this.org = builder.org;
		this.orgId = builder.orgId;
		this.unique1 = builder.unique1;
		this.mandatory = builder.mandatory;
		this.searchable = builder.searchable;
		this.indexable = builder.indexable;
		this.displayunit = builder.displayUnit;
		this.displaysequence = builder.displaySequence;
		this.attributename = builder.attributeName;
		this.modifiedby = builder.modifiedBy;
		this.modifieddate = builder.modifiedDate;
		this.createdby = builder.createdBy;
		this.createddate = builder.createdDate;
		this.datalakeattributeid=builder.datalakeattributeid;
	}

	public AltAttributeData() {
		super();
	}

	public String getAttributeName() {
		return attributename;
	}

	public Integer getAttributeId() {
		return attributeid;
	}

	public AltClassData getClassData() {
		return classData;
	}

	public void setClassData(AltClassData classData) {
		this.classData = classData;
	}

	public EnumDataType getDataType() {
		return datatype;
	}

	public Integer getReferenceClassId() {
		return referenceclassid;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public Boolean getPlaceholderType() {
		return placeholdertype;
	}

	public String getDefaultValue() {
		return defaultvalue;
	}

	public Boolean getUnique1() {
		return unique1;
	}

	public Boolean getMandatory() {
		return mandatory;
	}

	public Boolean getSearchable() {
		return searchable;
	}

	public Boolean getIndexable() {
		return indexable;
	}

	public Boolean getDisplayUnit() {
		return displayunit;
	}

	public Integer getDisplaySequence() {
		return displaysequence;
	}

	public String getOrg() {
		return org;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public Integer getDatalakeattributeid() {
		return datalakeattributeid;
	}

	public void setDatalakeattributeid(Integer datalakeattributeid) {
		this.datalakeattributeid = datalakeattributeid;
	}

	public Long getCreatedDate() {
		return createddate;
	}

	public Long getModifiedDate() {
		return modifieddate;
	}

	public String getCreatedBy() {
		return createdby;
	}

	public String getModifiedBy() {
		return modifiedby;
	}

	public static final class Builder {

		private Integer orgId;

		private Integer attributeId;

		private AltClassData classData;

		private EnumDataType dataType;

		private Integer referenceClassId;

		private EnumDocStatusType status;

		private Boolean placeholderType;

		private String defaultValue;

		private String org;

		private Boolean unique1;

		private Boolean mandatory;

		private Boolean searchable;

		private Boolean indexable;

		private Boolean displayUnit;

		private Integer displaySequence;

		private String attributeName;

		private Long createdDate;

		private Long modifiedDate;

		private String createdBy;

		private String modifiedBy;

		private Integer datalakeattributeid;

		public Builder() {
			super();
		}

		public Builder withAttributeId(Integer attributeId) {
			this.attributeId = attributeId;
			return this;
		}

		public Builder withClassData(AltClassData classData) {
			this.classData = classData;
			return this;
		}

		public Builder withDataType(EnumDataType dataType) {
			this.dataType = dataType;
			return this;
		}

		public Builder withReferenceClassId(Integer referenceClassId) {
			this.referenceClassId = referenceClassId;
			return this;
		}

		public Builder withStatus(EnumDocStatusType status) {
			this.status = status;
			return this;
		}

		public Builder withPlaceholderType(Boolean placeholderType) {
			this.placeholderType = placeholderType;
			return this;
		}

		public Builder withDefaultValue(String defaultValue) {
			this.defaultValue = defaultValue;
			return this;
		}

		public Builder withOrg(String org) {
			this.org = org;
			return this;
		}

		public Builder withOrgId(Integer orgId) {
			this.orgId = orgId;
			return this;
		}

		public Builder withUnique1(Boolean unique) {
			this.unique1 = unique;
			return this;
		}

		public Builder withMandatory(Boolean mandatory) {
			this.mandatory = mandatory;
			return this;
		}

		public Builder withSearchable(Boolean searchable) {
			this.searchable = searchable;
			return this;
		}

		public Builder withIndexable(Boolean indexable) {
			this.indexable = indexable;
			return this;
		}

		public Builder withDisplayUnit(Boolean displayUnit) {
			this.displayUnit = displayUnit;
			return this;
		}

		public Builder withDisplaySequence(Integer displaySequence) {
			this.displaySequence = displaySequence;
			return this;
		}

		public Builder withAttributeName(String attributeName) {
			this.attributeName = attributeName;
			return this;
		}

		public Builder withCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder withModifiedBy(String modifiedBy) {
			this.modifiedBy = modifiedBy;
			return this;
		}

		public Builder withCreatedDate(Long createdDate) {
			this.createdDate = createdDate;
			return this;
		}

		public Builder withModifiedDate(Long modifiedDate) {
			this.modifiedDate = modifiedDate;
			return this;
		}

		public Builder withDataLakeAttributeId(Integer dataLakeAttributeId) {
			this.datalakeattributeid = dataLakeAttributeId;
			return this;
		}

		public AltAttributeData build() {
			return new AltAttributeData(this);
		}
	}

}

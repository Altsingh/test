package com.alt.datakernel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumInstanceLimiter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "class")
public class AltClassData extends BaseModel implements Serializable {

	private static final long serialVersionUID = 9220366817856389346L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="classid")
	private Integer classid;
	
    private String classname;
    		
	private EnumDocStatusType status;
	
	private Boolean locationspecific;
	
	private Boolean customattributeallowed;
	
	private String packagecodes;
	
	private EnumInstanceLimiter instancelimiter;
	
	private String instanceprefix;
	
	private Boolean systemtype;
	
	private Boolean parametertype;

	private String org;
	
	@Column(name="orgid")
	private Integer orgId;
	
	private Long createddate;
	
	private Long modifieddate;
	
	private String createdby;
	
	private String modifiedby;
	
	private Boolean taskcoderequired;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "classData")
	private Set<AltAttributeData> attributes = new HashSet<>();

	public AltClassData(Builder builder) {
		this.classid = builder.classId;
		this.classname = builder.className;
		this.status = builder.status;
		this.locationspecific = builder.locationSpecific;
		this.customattributeallowed = builder.customAttributeAllowed;
		this.packagecodes = builder.packageCodes;
		this.instancelimiter = builder.instanceLimiter;
		this.instanceprefix = builder.instancePrefix;
		this.systemtype = builder.systemType;
		this.parametertype = builder.parameterType;
		this.org = builder.org;
		this.orgId = builder.orgId;
		this.modifiedby = builder.modifiedBy;
		this.modifieddate = builder.modifiedDate;
		this.createdby = builder.createdBy;
		this.createddate = builder.createdDate;
		this.attributes = builder.attributes;
		this.taskcoderequired = builder.taskcoderequired;
	}

	public AltClassData() {}

	public Integer getClassId() {
		return classid;
	}

	public String getClassName() {
		return classname;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public Boolean getLocationSpecific() {
		return locationspecific;
	}

	public Boolean getCustomAttributeAllowed() {
		return customattributeallowed;
	}

	public String getPackageCodes() {
		return packagecodes;
	}

	public EnumInstanceLimiter getInstanceLimiter() {
		return instancelimiter;
	}

	public String getInstancePrefix() {
		return instanceprefix;
	}

	public Boolean getSystemType() {
		return systemtype;
	}

	public Boolean getParameterType() {
		return parametertype;
	}

	public String getOrg() {
		return org;
	}
	
	public Integer getOrgId() {
		return orgId;
	}

	public Set<AltAttributeData> getAttributes() {
		return attributes;
	}

	public Long getCreatedDate() {
		return createddate;
	}

	public Long getModifiedDate() {
		return modifieddate;
	}

	public String getCreatedBy() {
		return createdby;
	}

	public String getModifiedBy() {
		return modifiedby;
	}

	public Boolean getTaskCodeRequired() {
		return taskcoderequired;
	}

	public void setTaskCodeRequired(Boolean taskcoderequired) {
		this.taskcoderequired = taskcoderequired;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		
		private Integer classId;
		
		private String className;
		
		private EnumDocStatusType status;
		
		private Boolean locationSpecific;
		
		private Boolean customAttributeAllowed;
		
		private String packageCodes;
		
		private EnumInstanceLimiter instanceLimiter;
		
		private String instancePrefix;
		
		private Boolean systemType;
		
		private Boolean parameterType;
		
		private String org;
		
		private Integer orgId;
		
		private Long createdDate;
		
		private Long modifiedDate;
		
		private String createdBy;
		
		private String modifiedBy;
		
		private Set<AltAttributeData> attributes;
		
		private Boolean taskcoderequired;

		public Builder() {
		}

		public Builder withClassId(Integer classId) {
			this.classId = classId;
			return this;
		}

		public Builder withClassName(String className) {
			this.className = className;
			return this;
		}

		public Builder withStatus(EnumDocStatusType status) {
			this.status = status;
			return this;
		}

		public Builder withLocationSpecific(Boolean locationSpecific) {
			this.locationSpecific = locationSpecific;
			return this;
		}

		public Builder withCustomAttributeAllowed(Boolean customAttributeAllowed) {
			this.customAttributeAllowed = customAttributeAllowed;
			return this;
		}

		public Builder withPackageCodes(String packageCodes) {
			this.packageCodes = packageCodes;
			return this;
		}

		public Builder withInstanceLimiter(EnumInstanceLimiter instanceLimiter) {
			this.instanceLimiter = instanceLimiter;
			return this;
		}

		public Builder withInstancePrefix(String instancePrefix) {
			this.instancePrefix = instancePrefix;
			return this;
		}

		public Builder withSystemType(Boolean systemType) {
			this.systemType = systemType;
			return this;
		}

		public Builder withParameterType(Boolean parameterType) {
			this.parameterType = parameterType;
			return this;
		}

		public Builder withOrg(String org) {
			this.org = org;
			return this;
		}
		
		public Builder withOrgId(Integer orgId) {
			this.orgId = orgId;
			return this;
		}
		
		public Builder withCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder withModifiedBy(String modifiedBy) {
			this.modifiedBy = modifiedBy;
			return this;
		}
		
		public Builder withCreatedDate(Long createdDate) {
			this.createdDate = createdDate;
			return this;
		}
		
		public Builder withModifiedDate(Long modifiedDate) {
			this.modifiedDate = modifiedDate;
			return this;
		}
		
		public Builder withAttributes(Set<AltAttributeData> attributes) {
			this.attributes = attributes;
			return this;
		}
		
		public Builder withTaskCodeRequired(Boolean taskcoderequired) {
			this.taskcoderequired = taskcoderequired;
			return this;
		}

		public AltClassData build() {
			return new AltClassData(this);
		}
	}
	
}

package com.alt.datakernel.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datacarrier.kernel.uiclass.EnumFormType;

@Entity
@Table(name = "form")
public class AltFormData extends BaseModel implements Serializable {

	private static final long serialVersionUID = -5035585982926219818L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer formid;

	private String formsecurityid;

	private EnumDocStatusType status;

	private EnumFormType formtype;

	private String orgcode;

	private String formname;

	private Boolean customcomponentallowed;

	private String description;

	@Column(name = "metaclassid")
	private Integer metaClassId;

	@Column(name = "orgid")
	private Integer orgId;

	@Column
	@Type(type = "ComponentsJsonType")
	private Components components;

	private String version;

	private EnumFormState state;

	private Long createddate;

	private Long modifieddate;

	private String createdby;

	private String modifiedby;

	private Integer appid;

	@Column(name = "parentformid")
	private Integer parentFormId;

	private AltFormData(Builder builder) {
		formid = builder.formid;
		formsecurityid = builder.formsecurityid;
		status = builder.status;
		formtype = builder.formtype;
		orgcode = builder.orgcode;
		formname = builder.formname;
		customcomponentallowed = builder.customcomponentallowed;
		description = builder.description;
		setComponents(builder.components);
		setVersion(builder.version);
		setState(builder.state);
		createddate = builder.createddate;
		modifieddate = builder.modifieddate;
		createdby = builder.createdby;
		modifiedby = builder.modifiedby;
		orgId = builder.orgId;
		appid = builder.appid;
		metaClassId = builder.metaClassId;
		parentFormId = builder.parentFormId;
	}

	public Integer getFormId() {
		return formid;
	}

	public EnumDocStatusType getStatus() {
		return status;
	}

	public EnumFormType getFormType() {
		return formtype;
	}

	public String getOrgCode() {
		return orgcode;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public String getFormName() {
		return formname;
	}

	public Boolean getCustomComponentAllowed() {
		return customcomponentallowed;
	}

	public String getDescription() {
		return description;
	}

	public String getVersion() {
		return version;
	}

	public EnumFormState getState() {
		return state;
	}

	public Long getCreatedDate() {
		return createddate;
	}

	public Long getModifiedDate() {
		return modifieddate;
	}

	public String getCreatedBy() {
		return createdby;
	}

	public String getModifiedBy() {
		return modifiedby;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setState(EnumFormState state) {
		this.state = state;
	}

	public Components getComponents() {
		return components;
	}

	public String getFormsecurityid() {
		return formsecurityid;
	}

	public void setFormsecurityid(String formsecurityid) {
		this.formsecurityid = formsecurityid;
	}

	public void setComponents(Components components) {
		this.components = components;
	}

	public Integer getMetaClassId() {
		return metaClassId;
	}

	public void setMetaClassId(Integer metaClassId) {
		this.metaClassId = metaClassId;
	}

	public AltFormData() {

	}

	public Long getModifieddate() {
		return modifieddate;
	}

	public void setModifieddate(Long modifieddate) {
		this.modifieddate = modifieddate;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	public Integer getAppid() {
		return appid;
	}

	public void setAppid(Integer appid) {
		this.appid = appid;
	}

	public Integer getParentFormId() {
		return parentFormId;
	}

	public void setParentFormId(Integer parentFormId) {
		this.parentFormId = parentFormId;
	}

	public static final class Builder {
		private Integer formid;
		private String formsecurityid;
		private EnumDocStatusType status;
		private EnumFormType formtype;
		private String orgcode;
		private String formname;
		private Boolean customcomponentallowed;
		private String description;
		private Components components;
		private String version;
		private EnumFormState state;
		private Long createddate;
		private Long modifieddate;
		private String createdby;
		private String modifiedby;
		private Integer orgId;
		public Integer appid;
		private Integer metaClassId;
		private Integer parentFormId;

		public Builder() {
		}

		public Builder withAppid(Integer val) {
			appid = val;
			return this;
		}

		public Builder withFormid(Integer val) {
			formid = val;
			return this;
		}

		public Builder withFormsecurityid(String val) {
			formsecurityid = val;
			return this;
		}

		public Builder withStatus(EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withFormtype(EnumFormType val) {
			formtype = val;
			return this;
		}

		public Builder withOrgcode(String val) {
			orgcode = val;
			return this;
		}

		public Builder withOrgId(Integer val) {
			orgId = val;
			return this;
		}

		public Builder withFormname(String val) {
			formname = val;
			return this;
		}

		public Builder withCustomcomponentallowed(Boolean val) {
			customcomponentallowed = val;
			return this;
		}

		public Builder withDescription(String val) {
			description = val;
			return this;
		}

		public Builder withComponents(Components val) {
			components = val;
			return this;
		}

		public Builder withVersion(String val) {
			version = val;
			return this;
		}

		public Builder withState(EnumFormState val) {
			state = val;
			return this;
		}

		public Builder withCreateddate(Long val) {
			createddate = val;
			return this;
		}

		public Builder withModifieddate(Long val) {
			modifieddate = val;
			return this;
		}

		public Builder withCreatedby(String val) {
			createdby = val;
			return this;
		}

		public Builder withModifiedby(String val) {
			modifiedby = val;
			return this;
		}

		public Builder withMetaClassId(Integer val) {
			metaClassId = val;
			return this;
		}

		public Builder withParentFormId(Integer val) {
			parentFormId = val;
			return this;
		}

		public AltFormData build() {
			return new AltFormData(this);
		}
	}
}

package com.alt.datakernel.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "globalreference")
public class AltGlobalReferenceData  extends BaseModel
{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer referenceid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "classid")
    private AltClassData classData;

    private String referenceobject;

    private String referenceattribute;

    private String referenceattributelabel;

    private Long createddate;

    private Long modifieddate;

    private String createdby;

    private String modifiedby;

    @Column(name="orgid")
    private Integer orgId;

    public AltGlobalReferenceData() {

    }

    public void setClassData(AltClassData classData) {
        this.classData = classData;
    }

    public Integer getReferenceid() {
        return referenceid;
    }

    public AltClassData getClassData() {
        return classData;
    }

    public String getReferenceobject() {
        return referenceobject;
    }

    public String getReferenceattribute() {
        return referenceattribute;
    }

    public String getReferenceattributelabel() {
        return referenceattributelabel;
    }

    public Long getCreateddate() {
        return createddate;
    }

    public Long getModifieddate() {
        return modifieddate;
    }

    public String getCreatedby() {
        return createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public Integer getOrgId() {
        return orgId;
    }


    public static final class AltGlobalReferenceDataBuilder {
        private Integer referenceid;
        private AltClassData classData;
        private String referenceobject;
        private String referenceattribute;
        private String referenceattributelabel;
        private Long createddate;
        private Long modifieddate;
        private String createdby;
        private String modifiedby;
        private Integer orgId;

        public AltGlobalReferenceDataBuilder() {
        }

        public static AltGlobalReferenceDataBuilder anAltGlobalReferenceData() {
            return new AltGlobalReferenceDataBuilder();
        }

        public AltGlobalReferenceDataBuilder withReferenceid(Integer referenceid) {
            this.referenceid = referenceid;
            return this;
        }

        public AltGlobalReferenceDataBuilder withClassData(AltClassData classData) {
            this.classData = classData;
            return this;
        }

        public AltGlobalReferenceDataBuilder withReferenceobject(String referenceobject) {
            this.referenceobject = referenceobject;
            return this;
        }

        public AltGlobalReferenceDataBuilder withReferenceattribute(String referenceattribute) {
            this.referenceattribute = referenceattribute;
            return this;
        }

        public AltGlobalReferenceDataBuilder withReferenceattributelabel(String referenceattributelabel) {
            this.referenceattributelabel = referenceattributelabel;
            return this;
        }

        public AltGlobalReferenceDataBuilder withCreateddate(Long createddate) {
            this.createddate = createddate;
            return this;
        }

        public AltGlobalReferenceDataBuilder withModifieddate(Long modifieddate) {
            this.modifieddate = modifieddate;
            return this;
        }

        public AltGlobalReferenceDataBuilder withCreatedby(String createdby) {
            this.createdby = createdby;
            return this;
        }

        public AltGlobalReferenceDataBuilder withModifiedby(String modifiedby) {
            this.modifiedby = modifiedby;
            return this;
        }

        public AltGlobalReferenceDataBuilder withOrgId(Integer orgId) {
            this.orgId = orgId;
            return this;
        }

        public AltGlobalReferenceData build() {
            AltGlobalReferenceData altGlobalReferenceData = new AltGlobalReferenceData();
            altGlobalReferenceData.modifiedby = this.modifiedby;
            altGlobalReferenceData.referenceid = this.referenceid;
            altGlobalReferenceData.classData = this.classData;
            altGlobalReferenceData.createdby = this.createdby;
            altGlobalReferenceData.orgId = this.orgId;
            altGlobalReferenceData.createddate = this.createddate;
            altGlobalReferenceData.referenceobject = this.referenceobject;
            altGlobalReferenceData.referenceattributelabel = this.referenceattributelabel;
            altGlobalReferenceData.referenceattribute = this.referenceattribute;
            altGlobalReferenceData.modifieddate = this.modifieddate;
            return altGlobalReferenceData;
        }
    }
}

package com.alt.datakernel.model;

import java.sql.Types;

import org.hibernate.dialect.PostgreSQL94Dialect;

public class AltOnePostgreSQL94Dialect extends PostgreSQL94Dialect {

	public AltOnePostgreSQL94Dialect() {
		this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
	}
}

package com.alt.datakernel.model;

import javax.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "reference")
public class AltReferenceData extends BaseModel implements Serializable {

	private static final long serialVersionUID = 6484861272282414306L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Integer referenceid;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "classid")
    private AltClassData classData;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "referencebyclassid")
    private AltClassData referenceByClass;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "referencebyattributeid")
    private AltAttributeData referenceByAttribute;

	private String org;
	
	private Long createddate;
	
	private Long modifieddate;
	
	private String createdby;
	
	private String modifiedby;

	@Column(name="orgid")
	private Integer orgId;

	public AltReferenceData() {
		
	}
	
	public AltClassData getReferenceByClass() {
		return referenceByClass;
	}

	public AltAttributeData getReferenceByAttribute() {
		return referenceByAttribute;
	}

	public Integer getReferenceid() {
		return referenceid;
	}

	public AltClassData getClassData() {
		return classData;
	}

	public String getOrg() {
		return org;
	}
	
	public Integer getOrgId() {
		return orgId;
	}

	public Long getCreateddate() {
		return createddate;
	}

	public Long getModifieddate() {
		return modifieddate;
	}

	public String getCreatedby() {
		return createdby;
	}

	public String getModifiedby() {
		return modifiedby;
	}

	public void setClassData(AltClassData classData) {
		this.classData = classData;
	}

	public AltReferenceData(Builder builder) {
		this.referenceid = builder.referenceId;
		this.classData = builder.classData;
		this.referenceByClass = builder.referenceByClass;
		this.referenceByAttribute = builder.referenceByAttribute;
		this.org = builder.org;
		this.orgId = builder.orgId;
		this.createddate = builder.createdDate;
		this.modifieddate = builder.modifiedDate;
		this.createdby = builder.createdBy;
		this.modifiedby = builder.modifiedBy;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		
		private Integer referenceId;
		
		private AltClassData classData;
		
		private AltClassData referenceByClass;
		
		private AltAttributeData referenceByAttribute;
		
		private String org;
		
		private Integer orgId;
		
		private Long createdDate;
		
		private Long modifiedDate;
		
		private String createdBy;
		
		private String modifiedBy;

		public Builder() {
		}

		public Builder withReferenceId(Integer referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public Builder withClassData(AltClassData classData) {
			this.classData = classData;
			return this;
		}

		public Builder withReferenceByClass(AltClassData referenceByClass) {
			this.referenceByClass = referenceByClass;
			return this;
		}

		public Builder withReferenceByAttribute(AltAttributeData referenceByAttribute) {
			this.referenceByAttribute = referenceByAttribute;
			return this;
		}

		public Builder withOrg(String org) {
			this.org = org;
			return this;
		}
		
		public Builder withOrgId(Integer orgId) {
			this.orgId = orgId;
			return this;
		}

		public Builder withCreatedDate(Long createdDate) {
			this.createdDate = createdDate;
			return this;
		}

		public Builder withModifiedDate(Long modifiedDate) {
			this.modifiedDate = modifiedDate;
			return this;
		}

		public Builder withCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public Builder withModifiedBy(String modifiedBy) {
			this.modifiedBy = modifiedBy;
			return this;
		}

		public AltReferenceData build() {
			return new AltReferenceData(this);
		}
	}

}
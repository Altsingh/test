package com.alt.datakernel.model;

import java.io.Serializable;
import java.util.Map;

public class Attributes implements Serializable {

	private static final long serialVersionUID = -6111404230779330775L;

	private Map<String, Object> attribute;

    public Attributes() {
    }

    public Map<String, Object> getAttribute() {
        return attribute;
    }

    public Attributes(Map<String, Object> attribute) {
        this.attribute = attribute;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "attribute=" + attribute +
                '}';
    }


}

package com.alt.datakernel.model;

import java.io.Serializable;
import java.util.List;

import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;

import java.util.Collections;

public class Components implements Serializable {

	private static final long serialVersionUID = -1632760741947902658L;

	private List<AltAbstractComponent> components;

	public Components(Builder builder) {
		this.components = builder.components;
	}

    public Components() {
    }

	public List<AltAbstractComponent> getComponents() {
		return components;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static final class Builder {
		private List<AltAbstractComponent> components = Collections.emptyList();

		public Builder() {
		}

		public Builder withComponents(List<AltAbstractComponent> components) {
			this.components = components;
			return this;
		}

		public Components build() {
			return new Components(this);
		}
	}

}

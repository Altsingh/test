package com.alt.datakernel.model;

import com.alt.datacarrier.business.superadmin.app.HomeBuilderRequestTO;

import java.io.Serializable;
import java.util.List;

public class HomeCards implements Serializable {
    private List<HomeBuilderRequestTO> homeCards;

    public HomeCards(List<HomeBuilderRequestTO> requests) {
        this.homeCards = requests;
    }

    public HomeCards() {
    }

    public List<HomeBuilderRequestTO> getHomeCards() {
        return homeCards;
    }

    public void setHomeCards(List<HomeBuilderRequestTO> homeCards) {
        this.homeCards = homeCards;
    }
}

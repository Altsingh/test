package com.alt.datakernel.model;

import com.alt.datacarrier.kernel.common.KernelConstants;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

//@Entity
//@Table(name = "instance")
public class Instance extends BaseModel implements Serializable {
   
	private static final long serialVersionUID = -1243273910959103810L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String classname;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "classid")
    private AltClassData classData;

    private Integer classid;

    private String orgid;
    
    @Column(name="organizationid")
    private Integer organizationid;

    @Column(name="appid")
    private Integer appid;
    
    @Column(name="formid")
    private Integer formid;
    
    @Column(name="userid")
    private String userid;
    
    @Column(name="formname")
    private String formname;
    
    @Column
    @Type(type = "AttributesJsonType")
    private Attributes attributes;

    private KernelConstants.EnumDocStatusType status;

    private Long createddate;

    private Long modifieddate;

    private String createdby;

    private String modifiedby;

    public Instance() {
    }

    public Instance(Builder builder) {
        id = builder.id;
        classname = builder.className;
        classData = builder.classData;
        orgid = builder.orgId;
        organizationid = builder.organizationid;
        appid = builder.appid;
        formid = builder.formid;
        formname = builder.formName;
        userid = builder.userid;
        attributes = builder.attributes;
        status = builder.status;
        createddate = builder.createdDate;
        modifieddate = builder.modifiedDate;
        createdby = builder.createdBy;
        modifiedby = builder.modifiedBy;
    }


    public static final class Builder {
        private Integer id;
        private String className;
        private AltClassData classData;
        private String orgId;
        private Integer organizationid;
        private Integer formid;
        private Integer appid;
        private String formName;
        private String userid;
        private Attributes attributes;
        private KernelConstants.EnumDocStatusType status;
        private Long createdDate;
        private Long modifiedDate;
        private String createdBy;
        private String modifiedBy;

        public Builder() {
        }

        public Builder withId(Integer val) {
            id = val;
            return this;
        }

        public Builder withClassName(String val) {
            className = val;
            return this;
        }

        public Builder withClassData(AltClassData val) {
            classData = val;
            return this;
        }

        public Builder withOrgId(String val) {
            orgId = val;
            return this;
        }
        
        public Builder withOrganizationId(Integer val) {
        	organizationid = val;
        	return this;
        }
        
        public Builder withAppId(Integer val) {
        	appid = val;
        	return this;
        }
        
        public Builder withFormName(String val) {
        	formName = val;
        	return this;
        }
        
        public Builder withFormId(Integer val) {
        	formid = val;
        	return this;
        }
        
        public Builder withUserId(String val) {
        	userid = val;
        	return this;
        }

        public Builder withAttributes(Attributes val) {
            attributes = val;
            return this;
        }

        public Builder withStatus(KernelConstants.EnumDocStatusType val) {
            status = val;
            return this;
        }

        public Builder withCreatedDate(Long val) {
            createdDate = val;
            return this;
        }

        public Builder withModifiedDate(Long val) {
            modifiedDate = val;
            return this;
        }

        public Builder withCreatedBy(String val) {
            createdBy = val;
            return this;
        }

        public Builder withModifiedBy(String val) {
            modifiedBy = val;
            return this;
        }

        public Instance build() {
            return new Instance(this);
        }
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public Integer getId() {
        return id;
    }

    public String getClassName() {
        return classname;
    }

    public AltClassData getClassData() {
        return classData;
    }

    public String getOrgId() {
        return orgid;
    }
    
    public Integer getOrganizationId() {
    	return organizationid;
    }

    public Integer getAppId() {
    	return appid;
    }

	public Integer getFormId() {
    	return formid;
    }
    
    public String getUserId() {
    	return userid;
    }
    
    public KernelConstants.EnumDocStatusType getStatus() {
        return status;
    }

    public Long getCreatedDate() {
        return createddate;
    }

    public Long getModifiedDate() {
        return modifieddate;
    }

    public String getCreatedBy() {
        return createdby;
    }

    public String getModifiedBy() {
        return modifiedby;
    }

	public void setAttributes(Attributes attributes) {
		this.attributes = attributes;
	}
	
	public void setModifiedDate(Long modifiedDate) {
		this.modifieddate = modifiedDate;
	}

	public String getFormname() {
		return formname;
	}

	public void setFormname(String formname) {
		this.formname = formname;
	}

	public void setStatus(KernelConstants.EnumDocStatusType status) {
		this.status = status;
	}

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public void setClassData(AltClassData classData) {
        this.classData = classData;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public Integer getOrganizationid() {
        return organizationid;
    }

    public void setOrganizationid(Integer organizationid) {
        this.organizationid = organizationid;
    }

    public Integer getAppid() {
        return appid;
    }

    public void setAppid(Integer appid) {
        this.appid = appid;
    }

    public Integer getFormid() {
        return formid;
    }

    public void setFormid(Integer formid) {
        this.formid = formid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Long getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Long createddate) {
        this.createddate = createddate;
    }

    public Long getModifieddate() {
        return modifieddate;
    }

    public void setModifieddate(Long modifieddate) {
        this.modifieddate = modifieddate;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }
}

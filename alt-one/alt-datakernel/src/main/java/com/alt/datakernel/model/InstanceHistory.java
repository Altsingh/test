package com.alt.datakernel.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;

@Entity
@Table(name = "instancehistory")
public class InstanceHistory extends BaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "instanceid")
	private Integer instanceid;

	@Column(name = "classname")
	private String classname;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "classid")
	private AltClassData classData;

	@Column(name = "organizationid")
	private Integer organizationid;

	@Column(name = "appid")
	private Integer appid;

	@Column(name = "formid")
	private Integer formid;

	@Column(name = "userid")
	private String userid;

	@Column(name = "formname")
	private String formname;

	@Column
	@Type(type = "AttributesJsonType")
	private Attributes attributes;

	@Column(name = "instancestatus")
	private KernelConstants.EnumDocStatusType instancestatus;

	@Column(name = "status")
	private KernelConstants.EnumDocStatusType status;

	@Column(name = "createddate")
	private Long createddate;

	@Column(name = "modifieddate")
	private Long modifieddate;

	@Column(name = "instancecreateddate")
	private Long instancecreateddate;

	@Column(name = "instancemodifieddate")
	private Long instancemodifieddate;

	@Column(name = "instancecreatedby")
	private String instancecreatedby;

	@Column(name = "instancemodifiedby")
	private String instancemodifiedby;

	public InstanceHistory() {

	}

	public InstanceHistory(Builder builder) {
		id = builder.id;
		instanceid = builder.instanceid;
		classname = builder.className;
		classData = builder.classData;
		organizationid = builder.organizationid;
		appid = builder.appid;
		formid = builder.formid;
		formname = builder.formName;
		userid = builder.userid;
		attributes = builder.attributes;
		status = builder.status;
		instancestatus = builder.instancestatus;
		createddate = builder.createdDate;
		modifieddate = builder.modifiedDate;
		instancemodifiedby = builder.instancemodifiedby;
		instancecreatedby = builder.instancecreatedby;
		instancemodifieddate = builder.instancemodifieddate;
		instancecreateddate = builder.instancecreateddate;
	}

	public static final class Builder {

		public Long instancecreateddate;
		public Long instancemodifieddate;
		public String instancecreatedby;
		public String instancemodifiedby;
		public Long modifiedDate;
		public Long createdDate;
		public EnumDocStatusType instancestatus;
		public EnumDocStatusType status;
		public Attributes attributes;
		public String userid;
		public String formName;
		public Integer formid;
		public Integer appid;
		public AltClassData classData;
		public Integer organizationid;
		public String className;
		public Integer instanceid;
		public Integer id;

		public Builder() {

		}

		public Builder withModifiedDate(Long val) {
			modifiedDate = val;
			return this;
		}

		public Builder withCreatedDate(Long val) {
			createdDate = val;
			return this;
		}

		public Builder withInstanceCreatedDate(Long val) {
			instancecreateddate = val;
			return this;
		}

		public Builder withInstanceModifiedDate(Long val) {
			instancemodifieddate = val;
			return this;
		}

		public Builder withInstanceCreatedBy(String val) {
			instancecreatedby = val;
			return this;
		}

		public Builder withInstanceModifiedBy(String val) {
			instancemodifiedby = val;
			return this;
		}

		public Builder withOrganizationId(Integer val) {
			organizationid = val;
			return this;
		}

		public Builder withId(Integer val) {
			id = val;
			return this;
		}

		public Builder withInstanceId(Integer val) {
			instanceid = val;
			return this;
		}

		public Builder withClassName(String val) {
			className = val;
			return this;
		}

		public Builder withClassData(AltClassData val) {
			classData = val;
			return this;
		}

		public Builder withFormName(String val) {
			formName = val;
			return this;
		}

		public Builder withFormId(Integer val) {
			formid = val;
			return this;
		}

		public Builder withUserId(String val) {
			userid = val;
			return this;
		}

		public Builder withAppId(Integer val) {
			appid = val;
			return this;
		}

		public Builder withAttributes(Attributes val) {
			attributes = val;
			return this;
		}

		public Builder withStatus(KernelConstants.EnumDocStatusType val) {
			status = val;
			return this;
		}

		public Builder withInstanceStatus(KernelConstants.EnumDocStatusType val) {
			instancestatus = val;
			return this;
		}

		public InstanceHistory build() {
			return new InstanceHistory(this);
		}

	}

	public Integer getId() {
		return id;
	}

	public Integer getInstanceid() {
		return instanceid;
	}

	public String getClassname() {
		return classname;
	}

	public AltClassData getClassData() {
		return classData;
	}

	public Integer getOrganizationid() {
		return organizationid;
	}

	public Integer getAppid() {
		return appid;
	}

	public Integer getFormid() {
		return formid;
	}

	public String getUserid() {
		return userid;
	}

	public String getFormname() {
		return formname;
	}

	public Attributes getAttributes() {
		return attributes;
	}

	public KernelConstants.EnumDocStatusType getInstancestatus() {
		return instancestatus;
	}

	public KernelConstants.EnumDocStatusType getStatus() {
		return status;
	}

	public Long getCreateddate() {
		return createddate;
	}

	public Long getModifieddate() {
		return modifieddate;
	}

	public Long getInstancecreateddate() {
		return instancecreateddate;
	}

	public Long getInstancemodifieddate() {
		return instancemodifieddate;
	}

	public String getInstancecreatedby() {
		return instancecreatedby;
	}

	public String getInstancemodifiedby() {
		return instancemodifiedby;
	}

}

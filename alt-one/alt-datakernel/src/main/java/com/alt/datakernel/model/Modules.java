package com.alt.datakernel.model;

import com.alt.datacarrier.business.superadmin.app.Module;

import java.io.Serializable;
import java.util.List;

public class Modules  implements Serializable {
    private List<Module> modules;

    public List<Module> getModules() {
        return modules;
    }

    public Modules(List<Module> modules) {
        this.modules=modules;
    }

    public Modules() {
    }

    public static final class ModulesBuilder {
        private List<Module> modules;

        private ModulesBuilder() {
        }

        public static ModulesBuilder aModules() {
            return new ModulesBuilder();
        }

        public ModulesBuilder withModules(List<Module> modules) {
            this.modules = modules;
            return this;
        }

        public Modules build() {
            Modules modules = new Modules();
            modules.modules = this.modules;
            return modules;
        }
    }
}

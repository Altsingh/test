package com.alt.datakernel.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "taskcoderecord")
public class TaskCodeRecord extends BaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "taskcoderecordid")
	private Integer taskcoderecordid;

	@Column(name = "appid")
	private Integer appid;

	@Column(name = "formid")
	private String formid;

	@Column(name = "forminitials")
	private String forminitials;

	@Column(name = "lastcodeused")
	private String lastcodeused;

	@Column(name = "classid")
	private String classid;
	
	@Column(name = "formname")
	private String formname;

	public TaskCodeRecord(Builder builder) {
		this.taskcoderecordid = builder.taskCodeRecordid;
		this.appid = builder.appid;
		this.formid = builder.formid;
		this.classid = builder.classid;
		this.forminitials = builder.forminitials;
		this.lastcodeused = builder.lastcodeused;
		this.formname = builder.formname;
	}

	public TaskCodeRecord() {
	}

	public Integer getAppid() {
		return appid;
	}

	public String getFormid() {
		return formid;
	}

	public String getForminitials() {
		return forminitials;
	}

	public String getLastcodeused() {
		return lastcodeused;
	}

	public String getClassid() {
		return classid;
	}

	public void setAppid(Integer appid) {
		this.appid = appid;
	}

	public void setFormid(String formid) {
		this.formid = formid;
	}

	public void setForminitials(String forminitials) {
		this.forminitials = forminitials;
	}

	public void setLastcodeused(String lastcodeused) {
		this.lastcodeused = lastcodeused;
	}

	public void setClassid(String classid) {
		this.classid = classid;
	}

	public Integer getTaskcoderecordid() {
		return taskcoderecordid;
	}

	public void setTaskcoderecordid(Integer taskcoderecordid) {
		this.taskcoderecordid = taskcoderecordid;
	}

	public String getFormname() {
		return formname;
	}

	public void setFormname(String formname) {
		this.formname = formname;
	}

	public static final class Builder {
		private Integer taskCodeRecordid;
		private Integer appid;
		private String formid;
		private String forminitials;
		private String lastcodeused;
		private String classid;
		private String formname;

		public Builder() {
		}

		public Builder withTaskCodeRecordId(Integer val) {
			this.taskCodeRecordid = val;
			return this;
		}

		public Builder withAppId(Integer val) {
			this.appid = val;
			return this;
		}

		public Builder withFormId(String val) {
			this.formid = val;
			return this;
		}

		public Builder withFormInitials(String val) {
			this.forminitials = val;
			return this;
		}

		public Builder withLastCodeUsed(String val) {
			this.lastcodeused = val;
			return this;
		}

		public Builder withClasId(String val) {
			this.classid = val;
			return this;
		}
		
		public Builder withFormName(String val) {
			this.formname = val;
			return this;
		}

		public TaskCodeRecord build() {
			return new TaskCodeRecord(this);
		}
	}

	@Override
	public String toString() {
		return "TaskCodeRecord{" +
				"taskcoderecordid=" + taskcoderecordid +
				", appid=" + appid +
				", formid='" + formid + '\'' +
				", forminitials='" + forminitials + '\'' +
				", lastcodeused='" + lastcodeused + '\'' +
				", classid='" + classid + '\'' +
				", formname='" + formname + '\'' +
				'}';
	}
}

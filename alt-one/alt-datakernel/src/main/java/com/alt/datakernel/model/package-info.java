@org.hibernate.annotations.TypeDef(name = "AttributesJsonType", typeClass = AttributesJsonType.class)
@org.hibernate.annotations.TypeDef(name = "ComponentsJsonType", typeClass = ComponentsJsonType.class)
@org.hibernate.annotations.TypeDef(name = "ModulesJsonType", typeClass = ModulesJsonType.class)
@org.hibernate.annotations.TypeDef(name = "HomeCardsJsonType", typeClass = HomeCardsJsonType.class)
package com.alt.datakernel.model;
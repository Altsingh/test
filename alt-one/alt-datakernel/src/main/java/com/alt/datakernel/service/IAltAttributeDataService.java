package com.alt.datakernel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;

@Service
public interface IAltAttributeDataService {

	public void  getAttributeNameByAttributeIdAndClassId(List<AttributeMeta> fetchedAttributes, Protocol protocol);

}

package com.alt.datakernel.service;

import java.util.List;
import java.util.Set;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.request.CreateClassRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datakernel.exception.ClassRelatedException;

public interface IClassService {

    ClassMeta read(ReadRequest request) throws ClassRelatedException;

    ClassMeta create(CreateClassRequest request) throws ClassRelatedException;

    List<ClassMeta> readAll(ReadRequest request) throws ClassRelatedException;

	ClassMeta read(Protocol protocol, Integer id) throws ClassRelatedException;

	ClassMeta update(CreateClassRequest request) throws ClassRelatedException;

	List<ClassMeta> listAll(Protocol protocol, Set<Integer> classIds);

}

package com.alt.datakernel.service;

import com.alt.datacarrier.kernel.db.core.DynamicReadRequest;
import com.alt.datacarrier.kernel.db.core.DynamicReadResponse;
import com.alt.datakernel.exception.QueryRelatedException;

public interface IDynamicQuery {

	public DynamicReadResponse read(DynamicReadRequest request) throws QueryRelatedException;
	
}

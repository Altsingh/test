package com.alt.datakernel.service;

import java.util.Collection;
import java.util.List;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.request.FormRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datakernel.exception.FormRelatedException;

public interface IFormService {

	AltForm read(ReadRequest request) throws FormRelatedException;

	AltForm create(FormRequest request) throws FormRelatedException;

	List<AltForm> readAll(ReadRequest request) throws FormRelatedException;

	AltForm readFromId(ReadRequest request) throws FormRelatedException;

	AltForm updateComponentsVersionState(FormRequest request) throws FormRelatedException;

	AltForm updateFormSecurity(FormRequest request, String formSecurityId) throws FormRelatedException;

	AltForm updateState(FormRequest request) throws FormRelatedException;

	AltForm readFormFromSecurityId(ReadRequest request) throws FormRelatedException;

	List<AltForm> listByIds(Protocol protocol, List<Integer> formIds);

	List<AltForm> listByNames(Protocol protocol, Collection<String> keySet);

}

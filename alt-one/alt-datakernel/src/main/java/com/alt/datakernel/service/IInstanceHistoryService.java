package com.alt.datakernel.service;

import org.springframework.stereotype.Service;

import com.alt.datacarrier.kernel.db.core.InstanceHistoryData;
import com.alt.datacarrier.kernel.db.request.InstanceHistoryRequest;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;

@Service
public interface IInstanceHistoryService {
	public InstanceHistoryData saveInstanceHistory(InstanceHistoryRequest instanceHistoryRequest)
			throws InstanceHistoryRelatedException;

}

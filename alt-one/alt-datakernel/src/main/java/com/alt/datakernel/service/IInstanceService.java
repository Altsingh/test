package com.alt.datakernel.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.InstanceData;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datacarrier.kernel.uiclass.AltOption;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;

@Service
public interface IInstanceService {

	InstanceData create(InstanceRequest request)
			throws InstanceRelatedException, ClassRelatedException, InstanceHistoryRelatedException;

	List<InstanceData> readClassInstances(ReadRequest request) throws InstanceRelatedException;

	InstanceData updateAttributes(InstanceRequest request) throws InstanceRelatedException;

	InstanceData read(Protocol protocol, int id) throws InstanceRelatedException;

	List<InstanceData> readClassInstancesById(ReadRequest request) throws InstanceRelatedException;

	HashMap<String, List<AltOption>> getAllAttributeValuesByAttributeName(List<AttributeMeta> fetchedAttributes,
			Protocol protocol) throws InstanceRelatedException;

	HashMap<String, List<String>> getSavedFormDataByContext(InstanceRequest instanceRequest, Protocol protocol)
			throws InstanceRelatedException;

	InstanceData deleteInstance(InstanceRequest instanceRequest)
			throws InstanceRelatedException, InstanceHistoryRelatedException;

	List<Object[]> getNestedDataForColumns(Protocol protocol, String dbClassRead, List<String> displayedColumns,
			Collection<String> ownerEmployeeIds, Map<String, String> filterAttributes) throws ClassRelatedException;

	List<Object[]> getNestedInstanceDataForColumns(Protocol protocol, String dbClassRead, List<String> displayedColumns,
			Map<String, String> filterAttributes, Integer instanceId);

	InstanceData edit(InstanceRequest appRequest)
			throws InstanceRelatedException, ClassRelatedException, InstanceHistoryRelatedException;

	List<Object[]> getNestedInstanceDataForColumnsForFiltering(Protocol protocol, String dbClassRead,
			List<String> columns1);
}

package com.alt.datakernel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.ReferenceData;
import com.alt.datakernel.exception.ReferenceRelatedException;

@Service
public interface IReferenceService {

	List<AttributeMeta> getAllAttributesByClassID(Integer metaclassid, Protocol protocol) throws ReferenceRelatedException;

}

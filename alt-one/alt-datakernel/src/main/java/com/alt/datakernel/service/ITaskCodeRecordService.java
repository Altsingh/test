package com.alt.datakernel.service;

import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.request.TaskCodeRecordTransport;
import com.alt.datakernel.exception.TaskCodeRecordRelatedException;

@Service
public interface ITaskCodeRecordService {

	TaskCodeRecordTransport getLastUpdateCodeDetailsByClassId(TaskCodeRecordTransport taskCodeRecordTransport)
			throws TaskCodeRecordRelatedException;

	TaskCodeRecordTransport createTaskCodeRecordForClass(TaskCodeRecordTransport taskCodeRecordTransport, Protocol protocol)
			throws TaskCodeRecordRelatedException;

	TaskCodeRecordTransport updateTaskCodeForClass(TaskCodeRecordTransport taskCodeRecordTransport)
			throws TaskCodeRecordRelatedException;

}

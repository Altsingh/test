package com.alt.datakernel.service.impl;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alt.datacarrier.common.enumeration.EnumDataType;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.request.CreateClassRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datakernel.dao.IClassDS;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.model.AltAttributeData;
import com.alt.datakernel.model.AltClassData;
import com.alt.datakernel.model.AltGlobalReferenceData;
import com.alt.datakernel.model.AltReferenceData;
import com.alt.datakernel.service.IClassService;
import com.alt.datakernel.validation.ClassValidator;

@Service
public class ClassService implements IClassService {

	@Autowired(required = true)
	ClassValidator classValidator;

	@Autowired(required = true)
	IClassDS classDS;

	private Logger LOGGER = LoggerFactory.getLogger(ClassService.class);

	@Override
	public ClassMeta read(ReadRequest request) throws ClassRelatedException {
		validateReadRequest(request);
		return classDS.get(request.getName(), request.getProtocol());
	}

	@Override
	public ClassMeta read(Protocol protocol, Integer id) throws ClassRelatedException {
		return classDS.get(id, protocol);
	}

	@Override
	public List<ClassMeta> readAll(ReadRequest request) throws ClassRelatedException {
		validateReadRequest(request);
		return classDS.getAll(request.getProtocol());
	}

	@Override
	public ClassMeta update(CreateClassRequest request) throws ClassRelatedException {
		classValidator.validateUpdateWriteRequest(request);
		Set<AltAttributeData> attributes = new HashSet<>();

		List<AltReferenceData> referenceDataList = getLocalReferences(request);
		List<AltGlobalReferenceData> globalReferenceDataList = getGlobalReferences(request);

		Integer dataLakeAttribute = request.getAttributes().stream()
				.map(attributeMeta -> (attributeMeta.getDataLakeAttributeId() == null) ? 1
						: attributeMeta.getDataLakeAttributeId())
				.max(Integer::compare).get();

		for (AttributeMeta attr : request.getAttributes()) {
			AltAttributeData attributeData = new AltAttributeData(new AltAttributeData.Builder()
					.withDataType(attr.getDataType()).withDefaultValue(attr.getDefaultValue())
					.withDisplaySequence(attr.getDisplaySequence()).withDisplayUnit(attr.getDisplayUnit())
					.withIndexable(attr.getIndexable()).withMandatory(attr.getMandatory())
					.withPlaceholderType(attr.getPlaceholderType()).withReferenceClassId(attr.getReferenceClassId())
					.withSearchable(attr.getSearchable()).withStatus(attr.getStatus()).withUnique1(attr.getUnique())
					.withOrg(request.getProtocol().getOrgCode()).withAttributeName(attr.getAttributeName())
					.withCreatedBy(request.getProtocol().getUserName()).withCreatedDate(new Date().getTime())
					.withModifiedBy(request.getProtocol().getUserName()).withModifiedDate(new Date().getTime())
					.withOrgId(request.getProtocol().getOrgId())
					.withAttributeId(attr.getAttributeId() == null ? null : Integer.parseInt(attr.getAttributeId())));
			if (attr.getAttributeId() != null) {
				attributeData.setAttributeid(Integer.valueOf(attr.getAttributeId()));
			}
			if (attr.getDataLakeAttributeId() == null) {
				dataLakeAttribute++;
				attributeData.setDatalakeattributeid(dataLakeAttribute);
			} else {
				attributeData.setDatalakeattributeid(attr.getDataLakeAttributeId());
			}
			attributes.add(attributeData);
		}
		AltClassData classData = new AltClassData(new AltClassData.Builder().withClassName(request.getClassName())
				.withCustomAttributeAllowed(request.getCustomAttributeAllowed())
				.withInstanceLimiter(request.getInstanceLimiter()).withInstancePrefix(request.getInstancePrefix())
				.withLocationSpecific(request.getLocationSpecific()).withParameterType(request.getParameterType())
				.withStatus(request.getStatus()).withSystemType(request.getSystemType())
				.withCreatedBy(request.getProtocol().getUserName()).withCreatedDate(new Date().getTime())
				.withModifiedBy(request.getProtocol().getUserName()).withModifiedDate(new Date().getTime())
				.withOrg(request.getProtocol().getOrgCode()).withOrgId(request.getProtocol().getOrgId())
				.withTaskCodeRequired(request.getTaskCodeRequired())
				.withClassId(Integer.valueOf(request.getClassId())));

		return classDS.add(request.getProtocol(), classData, attributes, referenceDataList, globalReferenceDataList);
	}

	@Override
	public ClassMeta create(CreateClassRequest request) throws ClassRelatedException {
		classValidator.validateWriteRequest(request);
		Set<AltAttributeData> attributes = new HashSet<>();
		Integer dataLakeAttribute = 1;
		List<AltReferenceData> referenceDataList = getLocalReferences(request);
		List<AltGlobalReferenceData> globalReferenceDataList = getGlobalReferences(request);
		if (request.getTaskCodeRequired()) {
			AltAttributeData attributeData = new AltAttributeData(
					new AltAttributeData.Builder().withDataType(EnumDataType.STRING).withDefaultValue("")
							.withDisplaySequence(0).withDisplayUnit(false).withIndexable(false).withMandatory(true)
							.withPlaceholderType(true).withReferenceClassId(null).withSearchable(true)
							.withStatus(KernelConstants.EnumDocStatusType.ACTIVE).withUnique1(true)
							.withOrg(request.getProtocol().getOrgCode()).withAttributeName("taskCode")
							.withCreatedBy(request.getProtocol().getUserName()).withCreatedDate(new Date().getTime())
							.withModifiedBy(request.getProtocol().getUserName()).withModifiedDate(new Date().getTime())
							.withOrgId(request.getProtocol().getOrgId()).withDataLakeAttributeId(dataLakeAttribute));
			dataLakeAttribute++;
			attributes.add(attributeData);
		}

		for (AttributeMeta attr : request.getAttributes()) {
			AltAttributeData attributeData = new AltAttributeData(new AltAttributeData.Builder()
					.withDataType(attr.getDataType()).withDefaultValue(attr.getDefaultValue())
					.withDisplaySequence(attr.getDisplaySequence()).withDisplayUnit(attr.getDisplayUnit())
					.withIndexable(attr.getIndexable()).withMandatory(attr.getMandatory())
					.withPlaceholderType(attr.getPlaceholderType()).withReferenceClassId(attr.getReferenceClassId())
					.withSearchable(attr.getSearchable()).withStatus(attr.getStatus()).withUnique1(attr.getUnique())
					.withOrg(request.getProtocol().getOrgCode()).withAttributeName(attr.getAttributeName())
					.withCreatedBy(request.getProtocol().getUserName()).withCreatedDate(new Date().getTime())
					.withModifiedBy(request.getProtocol().getUserName()).withModifiedDate(new Date().getTime())
					.withOrgId(request.getProtocol().getOrgId()).withDataLakeAttributeId(dataLakeAttribute));
			dataLakeAttribute++;
			attributes.add(attributeData);
		}
		AltClassData classData = new AltClassData(new AltClassData.Builder().withClassName(request.getClassName())
				.withCustomAttributeAllowed(request.getCustomAttributeAllowed())
				.withInstanceLimiter(request.getInstanceLimiter()).withInstancePrefix(request.getInstancePrefix())
				.withLocationSpecific(request.getLocationSpecific()).withParameterType(request.getParameterType())
				.withStatus(request.getStatus()).withSystemType(request.getSystemType())
				.withCreatedBy(request.getProtocol().getUserName()).withCreatedDate(new Date().getTime())
				.withModifiedBy(request.getProtocol().getUserName()).withModifiedDate(new Date().getTime())
				.withTaskCodeRequired(request.getTaskCodeRequired()).withOrg(request.getProtocol().getOrgCode())
				.withOrgId(request.getProtocol().getOrgId()));
		return classDS.add(request.getProtocol(), classData, attributes, referenceDataList, globalReferenceDataList);
	}

	private List<AltGlobalReferenceData> getGlobalReferences(CreateClassRequest request) {
		return request.getAttributes().stream().filter(a -> a.getReferenceClass() != null
				&& a.getReferenceAttr() != null && a.getDataType().equals(EnumDataType.GLOBALREFERENCE)).map(a -> {

					AltGlobalReferenceData data = new AltGlobalReferenceData.AltGlobalReferenceDataBuilder()
							.withReferenceobject(a.getReferenceClass()).withReferenceattribute(a.getReferenceAttr())
							.withOrgId(request.getProtocol().getOrgId()).withCreateddate(new Date().getTime())
							.withCreatedby(request.getProtocol().getUserName()).withModifieddate(new Date().getTime())
							.withModifiedby(request.getProtocol().getUserName()).build();
					return data;
				}).collect(Collectors.toList());
	}

	private List<AltReferenceData> getLocalReferences(CreateClassRequest request) {
		return request.getAttributes().stream().filter(a -> a.getReferenceClass() != null
				&& a.getReferenceAttr() != null && a.getDataType().equals(EnumDataType.REFERENCE)).map(a -> {
					AltAttributeData referenceAttribute = null;
					try {
						referenceAttribute = classDS.getAttributeFromClassAndAttributeName(request.getProtocol(),
								a.getReferenceClass(), a.getReferenceAttr());
					} catch (ClassRelatedException e) {
						LOGGER.error("Error saving reference for attribute", a.getReferenceAttr());
					}
					AltReferenceData data = new AltReferenceData(new AltReferenceData.Builder()
							.withReferenceByClass(referenceAttribute.getClassData())
							.withOrg(referenceAttribute.getOrg()).withReferenceByAttribute(referenceAttribute)
							.withCreatedDate(new Date().getTime()).withCreatedBy(request.getProtocol().getUserName())
							.withModifiedDate(new Date().getTime()).withModifiedBy(request.getProtocol().getUserName())
							.withOrgId(request.getProtocol().getOrgId()));
					return data;
				}).collect(Collectors.toList());
	}

	private boolean validateReadRequest(ReadRequest request) throws ClassRelatedException {
		if (request.getProtocol().getOrgCode().isEmpty() || request.getProtocol().getOrgCode() == null) {
			throw new ClassRelatedException("Error occurred in class ClassService and method checkNulls: "
					+ "Organization Code cannot be empty");
		}
		if (request.getName().isEmpty() || request.getName() == null) {
			throw new ClassRelatedException(
					"Error occurred in class ClassService and method checkNulls: " + "Class name cannot be empty");
		}
		return true;
	}

	public void markInactiveFormClass(com.alt.datacarrier.core.Protocol protocol, String className) {
		try {
			classDS.markInactiveFormClass(protocol, className);
		} catch (ClassRelatedException e) {
			LOGGER.error("Failed to mark inactive form class: " + className, e);
		}

	}

	@Override
	public List<ClassMeta> listAll(Protocol protocol, Set<Integer> classIds) {
		return classDS.listAll(protocol, classIds);
	}

}

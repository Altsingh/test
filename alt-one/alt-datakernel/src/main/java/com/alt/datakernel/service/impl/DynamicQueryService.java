package com.alt.datakernel.service.impl;

import com.alt.datacarrier.kernel.db.core.DynamicReadRequest;
import com.alt.datacarrier.kernel.db.core.DynamicReadResponse;
import com.alt.datakernel.dao.IQueryDS;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.datakernel.util.DynamicDbQueryCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DynamicQueryService implements IDynamicQuery {

    @Autowired(required = true)
    IQueryDS queryDS;

    DynamicDbQueryCreator creator = new DynamicDbQueryCreator();

    @Override
    public DynamicReadResponse read(DynamicReadRequest request) throws QueryRelatedException {
        String query = creator.create(request);
        
        List<Object[]> result = queryDS.executeQuery(request.getProtocol(), query);
        int count = -1;
        if (request.isDocumentCountNeeded() != null && request.isDocumentCountNeeded()) {
            String countQuery = creator.createCountQuery(request);
            count = queryDS.executeCountQuery(request.getProtocol(), countQuery).intValue();
        }
        return createResponse(result, count);
    }

    private DynamicReadResponse createResponse(List<Object[]> result, int count) {
        DynamicReadResponse response = new DynamicReadResponse();
        response.setResponse(result);
        response.setDocumentCount(count);
        response.setMessage("Query Successfully Executed");
        response.setCode(200);
        return response;
    }

}

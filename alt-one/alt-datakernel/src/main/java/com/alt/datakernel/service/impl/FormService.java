package com.alt.datakernel.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.request.FormRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datakernel.dao.IFormDS;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.model.AltFormData;
import com.alt.datakernel.model.Components;
import com.alt.datakernel.service.IFormService;
import com.alt.datakernel.validation.FormValidator;

@Service
public class FormService implements IFormService {

	@Autowired
	IFormDS formDS;

	@Autowired(required = true)
	FormValidator formValidator;

	@Override
	public AltForm read(ReadRequest request) throws FormRelatedException {
		validateReadRequest(request);
		return formDS.get(request.getProtocol(), request.getFormName());
	}

	@Override
	public AltForm readFormFromSecurityId(ReadRequest request) throws FormRelatedException {
		validateReadRequestForSecurityId(request);
		return formDS.getFormFromSecurityId(request.getProtocol(), request.getFormSecurityId());
	}

	@Override
	public AltForm readFromId(ReadRequest request) throws FormRelatedException {
		validateReadRequestForId(request);
		return formDS.getFromId(request.getProtocol(), request.getId());
	}

	@Override
	public AltForm updateComponentsVersionState(FormRequest request) throws FormRelatedException {
		formValidator.validateUpdateRequest(request);
		return formDS.updateComponentsVersionState(request.getProtocol(), request.getFormId(),
				request.getComponentList(), request.getVersion(), request.getState());
	}

	@Override
	public AltForm updateFormSecurity(FormRequest request, String formSecurityId) throws FormRelatedException {
		formValidator.validateUpdateRequest(request);
		return formDS.updateFormSecurity(request.getProtocol(), request.getFormId(), request.getComponentList(),
				formSecurityId);
	}

	@Override
	public AltForm updateState(FormRequest request) throws FormRelatedException {
		formValidator.validateStateUpdateRequest(request);
		return formDS.updateState(request.getProtocol(), request.getFormId(), request.getState(),
				request.getClassMeta());
	}

	@Override
	public List<AltForm> readAll(ReadRequest request) throws FormRelatedException {
		validateReadRequest(request);
		return formDS.getAll(request.getProtocol(), request.getAppId());
	}

	@Override
	public AltForm create(FormRequest request) throws FormRelatedException {
		formValidator.validateWriteRequest(request);
		String hrFormIdString = (request.getHrFormId() == null) ? null : request.getHrFormId().toString();
		Integer metaClassId = request.getClassMeta() == null ? null : Integer.parseInt(request.getClassMeta().getId());
		AltFormData formData = new AltFormData.Builder().withCreatedby(request.getProtocol().getUserName())
				.withCreateddate(new Date().getTime()).withCustomcomponentallowed(request.isCustomComponentAllowed())
				.withDescription(request.getDescription()).withFormname(request.getFormName())
				.withFormtype(request.getFormType()).withModifiedby(request.getProtocol().getUserName())
				.withModifieddate(new Date().getTime()).withOrgId(request.getProtocol().getOrgId())
				.withStatus(request.getStatus()).withVersion(request.getVersion()).withState(request.getState())
				.withComponents(Components.builder().withComponents(request.getComponentList()).build())
				.withFormsecurityid(hrFormIdString).withAppid(request.getAppId()).withMetaClassId(metaClassId)
				.withParentFormId(request.getParentFormId()).build();
		return formDS.add(request.getProtocol(), formData);
	}

	private boolean validateReadRequest(ReadRequest request) throws FormRelatedException {
		if (request.getProtocol() == null || request.getProtocol().getOrgId() == null
				|| request.getProtocol().getOrgId() <= 0) {
			throw new FormRelatedException("Error occurred in class FormService and method validateReadRequest: "
					+ "Protocol/Organization Code cannot be empty");
		}
		return true;
	}

	private boolean validateReadRequestForId(ReadRequest request) throws FormRelatedException {
		if (request.getProtocol() == null || request.getProtocol().getOrgId() == null
				|| request.getProtocol().getOrgId() <= 0) {
			throw new FormRelatedException("Error occurred in class FormService and method validateReadRequestForId: "
					+ "Protocol/Organization Code cannot be empty");
		}
		if (request.getId() == null || request.getId() == -1) {
			throw new FormRelatedException("Error occurred in class FormService and method validateReadRequestForId: "
					+ "FormId cannot be null or -1");
		}
		return true;
	}

	private boolean validateReadRequestForMetaClassId(ReadRequest request) throws FormRelatedException {
		if (request.getProtocol() == null || request.getProtocol().getOrgId() == null
				|| request.getProtocol().getOrgId() <= 0) {
			throw new FormRelatedException(
					"Error occurred in class FormService and method validateReadRequestForMetaClassId: "
							+ "Protocol/Organization Code cannot be empty");
		}
		if (request.getMetaClassId() == null || request.getMetaClassId() == -1) {
			throw new FormRelatedException(
					"Error occurred in class FormService and method validateReadRequestForMetaClassId: "
							+ "MetaClassId cannot be null or -1");
		}
		return true;
	}

	private boolean validateReadRequestForSecurityId(ReadRequest request) throws FormRelatedException {
		if (request.getProtocol() == null || request.getProtocol().getOrgId() == null
				|| request.getProtocol().getOrgId() <= 0) {
			throw new FormRelatedException("Error occurred in class FormService and method validateReadRequestForId: "
					+ "Protocol/Organization Code cannot be empty");
		}
		if (request.getFormSecurityId() == null || request.getFormSecurityId().equals("")) {
			throw new FormRelatedException("Error occurred in class FormService and method validateReadRequestForId: "
					+ "FormId cannot be null or empty");
		}
		return true;
	}

	@Override
	public List<AltForm> listByIds(Protocol protocol, List<Integer> formIds) {
		return formDS.listByIds(protocol, formIds);
	}

	@Override
	public List<AltForm> listByNames(Protocol protocol, Collection<String> formNames) {
		return formDS.listByNames(protocol, formNames);
	}

}

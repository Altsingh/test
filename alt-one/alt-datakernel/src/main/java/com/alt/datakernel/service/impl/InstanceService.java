package com.alt.datakernel.service.impl;

import static com.alt.datakernel.exception.ErrorConstants.CLASS_NAME_EMPTY;
import static com.alt.datakernel.exception.ErrorConstants.DUPLICATE_VALUE;
import static com.alt.datakernel.exception.ErrorConstants.ORG_NAME_EMPTY;
import static com.alt.datakernel.exception.ErrorConstants.READ_FAIL;
import static com.alt.datakernel.exception.ErrorConstants.VALUE_NULL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.core.InstanceData;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datacarrier.kernel.uiclass.AltOption;
import com.alt.datakernel.dao.IClassDS;
import com.alt.datakernel.dao.IInstanceDS;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.model.Attributes;
import com.alt.datakernel.model.Instance;
import com.alt.datakernel.service.IClassService;
import com.alt.datakernel.service.IInstanceService;

@Service
public class InstanceService implements IInstanceService {

	@Autowired(required = true)
	IInstanceDS instanceDS;

	@Autowired(required = true)
	IClassDS iClassDS;

	@Autowired
	IClassService classService;

	private static final Logger logger = Logger.getLogger(InstanceService.class);

	@Override
	public InstanceData read(Protocol protocol, int id) throws InstanceRelatedException {
		Instance instance = new Instance();
		try {
			instance = instanceDS.find(protocol, Integer.valueOf(id));
		} catch (Exception e) {
			throw new InstanceRelatedException(
					"Error occurred in class InstanceService and method readInstance: " + READ_FAIL, e);
		}

		return convertToInstanceData(instance);
	}

	@Override
	public InstanceData updateAttributes(InstanceRequest request) throws InstanceRelatedException {
		Instance instance = new Instance();
		try {
			instance = instanceDS.updateAttributes(request.getProtocol(), request.getInstanceId(),
					request.getAttributes());
		} catch (Exception e) {
			throw new InstanceRelatedException("Error occurred in class InstanceService and method update.", e);
		}

		return convertToInstanceData(instance);
	}

	private InstanceData convertToInstanceData(Instance instance) throws InstanceRelatedException {
		InstanceData data = new InstanceData();
		if (instance != null && (instance.getClassData() != null || instance.getClassid() != null)) {
			data.setClassId(instance.getClassid());
		} else {
			throw new InstanceRelatedException(
					"Error occurred in class InstanceService and no instance corresponding to this.");
		}
		data.setClassName(instance.getClassName());
		data.setInstanceId(instance.getId());
		data.setOrg(instance.getOrganizationId().toString());
		data.setAppId(instance.getAppId());
		data.setFormName(instance.getFormname());
		data.setFormId(instance.getFormId());
		data.setUserId(instance.getUserId());
		data.setStatus(instance.getStatus());
		data.setCreatedBy(instance.getCreatedBy());
		data.setModifiedBy(instance.getModifiedBy());
		data.setCreatedDate(instance.getCreatedDate());
		data.setModifiedDate(instance.getModifiedDate());
		if (instance.getAttributes() != null) {
			data.setAttributes(instance.getAttributes().getAttribute());
		}
		return data;
	}

	@Override
	public InstanceData create(InstanceRequest request)
			throws InstanceRelatedException, ClassRelatedException, InstanceHistoryRelatedException {
		String id = validateCreateRequest(request);
		Attributes attr = new Attributes(request.getAttributes());
		Instance instance = new Instance(new Instance.Builder().withId(request.getInstanceId())
				.withClassName(request.getClassName()).withOrganizationId(request.getProtocol().getOrgId())
				.withStatus(
						request.getStatus() == null ? KernelConstants.EnumDocStatusType.ACTIVE : request.getStatus())
				.withAttributes(attr).withCreatedBy(request.getProtocol().getUserName())
				.withCreatedDate(System.currentTimeMillis())
				.withClassData(iClassDS.getFromId(request.getProtocol(), Integer.valueOf(id)))
				.withModifiedBy(request.getProtocol().getUserName()).withModifiedDate(System.currentTimeMillis())
				.withUserId(request.getProtocol().getUserName()).withAppId(request.getAppId())
				.withFormId(request.getFormId()).withFormName(request.getFormName()));

		Instance instanceReturned = instanceDS.persistInstance(instance, request.getProtocol(),
				request.isProcessHistory());
		return convertToInstanceData(instanceReturned);
	}

	private String validateCreateRequest(InstanceRequest request)
			throws InstanceRelatedException, ClassRelatedException {
		ClassMeta classMeta = new ClassMeta();
		checkNulls(request);
		try {
			classMeta = iClassDS.get(request.getClassName(), request.getProtocol());
		} catch (Exception e) {
			throw new ClassRelatedException(
					"Error occurred in class InstanceService and method checkUnique: " + READ_FAIL, e);
		}
		List<AttributeMeta> attributeMetas = classMeta.getAttributes();
		Map<String, Object> attributeValues = request.getAttributes();
		for (AttributeMeta attrMeta : attributeMetas) {
			validateAttributeValue(request, attributeValues, attrMeta);
		}
		return classMeta.getId();
	}

	private void validateAttributeValue(InstanceRequest request, Map<String, Object> attributeValues,
			AttributeMeta attrMeta) throws InstanceRelatedException {
		// check data type
		// if type is reference, check attr value is correct reference and is active
		// check status, if null set to active
		// if default value is present, and instance has no value, put def value
		// check unique
		// check mandatory
		// if disp unit is true, disp seq cannot be null
		boolean unique = attrMeta.getUnique();
		boolean mandatory = attrMeta.getMandatory();
		boolean defaultGiven = attrMeta.getDefaultValue() == null ? true : false;
		if (attributeValues.containsKey(attrMeta.getAttributeName())) {
			if (unique) {
				// checkUnique(request, attributeValues, attrMeta);
			}
		} else {
			if (mandatory && unique) {
				throw new InstanceRelatedException(
						"Error occurred in class InstanceService and method validateCreateRequest : " + VALUE_NULL
								+ " for attribute: " + attrMeta.getAttributeName());
			} else if (mandatory && !unique) {
				// check default
				// if no default throw error
				if (!defaultGiven) {
					throw new InstanceRelatedException(
							"Error occurred in class InstanceService and method validateCreateRequest : " + VALUE_NULL
									+ " for attribute: " + attrMeta.getAttributeName());
				}
			}
		}
	}

	private void checkUnique(InstanceRequest request, Map<String, Object> attributeValues, AttributeMeta attrMeta)
			throws InstanceRelatedException {
		// check uniqueness
		// set value
		List<Instance> instances = new ArrayList<>();
		try {
			instances = instanceDS.findAllInstancesForAClass(request.getProtocol(), request.getClassName());
		} catch (Exception e) {
			throw new InstanceRelatedException(
					"Error occurred in class InstanceService and method checkUnique: " + READ_FAIL, e);
		}
		boolean attributesValueSame = false;
		for (Instance instance : instances) {
			if (attributeValues.get(attrMeta.getAttributeName())
					.equals(instance.getAttributes().getAttribute().get(attrMeta.getAttributeName()))) {
				attributesValueSame = true;
				break;
			}
		}
		if (attributesValueSame) {
			throw new InstanceRelatedException(
					"Error occurred in class InstanceService and method validateCreateRequest : " + DUPLICATE_VALUE
							+ " for attribute: " + attrMeta.getAttributeName());
		}
	}

	private void checkNulls(InstanceRequest request) throws InstanceRelatedException {
		if (request.getProtocol().getOrgId() == null || request.getProtocol().getOrgId() <= 0) {
			throw new InstanceRelatedException(
					"Error occurred in class InstanceService and method checkNulls: " + ORG_NAME_EMPTY);
		}
		if (request.getClassName().isEmpty() || request.getClassName() == null) {
			throw new InstanceRelatedException(
					"Error occurred in class InstanceService and method checkNulls: " + CLASS_NAME_EMPTY);
		}
	}

	@Override
	public List<InstanceData> readClassInstances(ReadRequest request) throws InstanceRelatedException {
//        try {
//            validateReadRequest(request);
//        } catch (Exception e) {
//            throw new InstanceRelatedException("Error occurred in class InstanceService and method readClassInstances: " + NULL_CHECK_FAIL);
//        }
		List<Instance> instances = instanceDS.findAllInstancesForAClass(request.getProtocol(), request.getName());
		List<InstanceData> instanceDataList = new ArrayList<>();
		for (Instance instance : instances) {
			instanceDataList.add(convertToInstanceData(instance));
		}
		return instanceDataList;
	}

	@Override
	public List<InstanceData> readClassInstancesById(ReadRequest request) throws InstanceRelatedException {
		List<Instance> instances = instanceDS.findAllInstancesForAClassId(request.getProtocol(),
				(request.getId() != null) ? request.getId() : null,
				(request.getClassName() != null) ? request.getClassName() : null,
				(request.getAttributes() != null && request.getAttributes().size() > 0) ? request.getAttributes()
						: null);
		List<InstanceData> instanceDataList = new ArrayList<>();
		for (Instance instance : instances) {
			instanceDataList.add(convertToInstanceData(instance));
		}
		return instanceDataList;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public HashMap<String, List<AltOption>> getAllAttributeValuesByAttributeName(List<AttributeMeta> fetchedAttributes,
			Protocol protocol) throws InstanceRelatedException {
		if (fetchedAttributes == null || fetchedAttributes.size() <= 0)
			return null;
		HashMap<String, List<AltOption>> attributeByValues = new HashMap<>();
		List<Object[]> returnedValuList = null;
		for (AttributeMeta attr : fetchedAttributes) {
			if (attr.getAttributeName() != null && attr.getRefClassMeta() != null
					&& attr.getRefClassMeta().getId() != null) {
				returnedValuList = instanceDS.getAllAttributeValuesByAttributeName(attr.getAttributeName(),
						attr.getRefClassMeta().getId(), protocol);
				if (returnedValuList != null && returnedValuList.size() > 0) {
					if (!(attributeByValues.containsKey(attr.getAttributeName()))) {
						attributeByValues.put(attr.getRefClassMeta().getId() + "_" + attr.getAttributeName(),
								convertAttributeListToOption(returnedValuList));
					}
				}
			}
		}
		return attributeByValues;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private List<AltOption> convertAttributeListToOption(List<Object[]> returnedValueList) {
		return returnedValueList.stream().map(a -> new AltOption(a[1], (String.valueOf(a[0]))))
				.collect(Collectors.toList());
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, List<String>> getSavedFormDataByContext(InstanceRequest instanceRequest, Protocol protocol)
			throws InstanceRelatedException {
		List<InstanceData> instanceDataList = null;
		HashMap<String, List<String>> userSavedFormData = null;

		Instance instance = null;

		if (instanceRequest.getInstanceId() != null && instanceRequest.getInstanceId() > 0) {
			instance = new Instance(new Instance.Builder().withOrganizationId(protocol.getOrgId())
					.withId(instanceRequest.getInstanceId()));
		} else {
			// use username from request object instead of protocol

			// if formId is not null
			if (instanceRequest.getFormId() != null) {
				instance = new Instance(new Instance.Builder().withOrganizationId(protocol.getOrgId())
						.withUserId(protocol.getUserName()).withAppId(instanceRequest.getAppId())
						.withFormId(instanceRequest.getFormId()).withFormName(instanceRequest.getFormName()));
			} else {
				instance = new Instance(new Instance.Builder().withOrganizationId(protocol.getOrgId())
						.withUserId(protocol.getUserName()).withAppId(instanceRequest.getAppId())
						.withFormName(instanceRequest.getFormName()));
			}

		}

		List<Instance> savedInstanceList = instanceDS.getSavedFormDataByContext(instance, protocol);

		if (savedInstanceList != null && !savedInstanceList.isEmpty()) {
			logger.warn("************ APP ID FETCHED ************* :" + savedInstanceList.toString());
			instanceDataList = new ArrayList<>();
			for (Instance instc : savedInstanceList) {
				InstanceData instd = convertToInstanceData(instc);
				if (instd != null) {
					instanceDataList.add(instd);
				}
			}

			Map<String, Object> userSavedFormDataMap = instanceDataList.get(0).getAttributes();
			if (userSavedFormDataMap != null && !userSavedFormDataMap.isEmpty()) {
				userSavedFormData = new HashMap<>();
				for (String key : userSavedFormDataMap.keySet()) {
					if (userSavedFormDataMap.get(key) instanceof List) {
						userSavedFormData.put(key, (List<String>) userSavedFormDataMap.get(key));
					} else {
						List<String> valuesList = new ArrayList<>();
						if (userSavedFormDataMap.get(key) != null) {
							valuesList.add(userSavedFormDataMap.get(key).toString());
						}
						userSavedFormData.put(key, valuesList);
					}

				}
			}

		}

		return userSavedFormData;
	}

	@Override
	public InstanceData deleteInstance(InstanceRequest instanceRequest)
			throws InstanceRelatedException, InstanceHistoryRelatedException {
		Instance instance = new Instance(new Instance.Builder().withId(instanceRequest.getInstanceId())
				.withStatus(KernelConstants.EnumDocStatusType.INACTIVE));

		Instance instanceReturned = instanceDS.persistInstance(instance, instanceRequest.getProtocol(), true);
		return convertToInstanceData(instanceReturned);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getNestedDataForColumns(Protocol protocol, String dbClassRead, List<String> displayedColumns,
			Collection<String> ownerEmployeeIds, Map<String, String> filterAttributes) throws ClassRelatedException {
		String instanceTable = instanceDS.getInstanceTableName(protocol);
		List<String[]> columns = displayedColumns.stream().map(column -> column.split("\\.."))
				.collect(Collectors.toList());

		Query query = null;
		Query referencedQuery = null;
		ClassMeta classMeta = null;
		ClassMeta referencedClassMeta = null;
		StringBuilder queryBuilder = null;
		StringBuilder referencedQueryBuilder = null;
		List<Object[]> rows = null;
		List<Object[]> referencedRows = null;
		Map<String, Query> referencedQueryMap = new HashMap<String, Query>();
		Map<String, Set<Integer>> referencedIdsMap = new HashMap<String, Set<Integer>>();
		Map<String, Map<String, Object[]>> referencedDataMap = new HashMap<String, Map<String, Object[]>>();

		queryBuilder = new StringBuilder();
		classMeta = classService.read(protocol, Integer.parseInt(dbClassRead));
		queryBuilder.append("select id");
		for (String[] column : columns) {
			queryBuilder.append(" , attributes->'attribute'->>'" + column[0] + "' as \"" + column[0] + "\"");
			if (column.length == 2) {
				Optional<Integer> classReadOptional = classMeta.getAttributes().stream()
						.filter(attributeMeta -> attributeMeta.getAttributeName().equalsIgnoreCase(column[0])).findAny()
						.map(attributeMeta -> attributeMeta.getReferenceClassId());
				Integer referencedClassId = classReadOptional.isPresent() ? classReadOptional.get() : null;
				referencedClassMeta = classService.read(protocol, referencedClassId);
				referencedQueryBuilder = new StringBuilder();
				referencedQueryBuilder.append("select id");
				referencedQueryBuilder
						.append(" , attributes->'attribute'->>'" + column[1] + "' as \"" + column[1] + "\"");
				referencedQueryBuilder.append(" from " + instanceTable + " where classname='"
						+ referencedClassMeta.getName() + "' and status='0'");
				referencedQueryBuilder.append(" and id in (:referencedIds)");
				referencedQuery = instanceDS.createNativeQuery(protocol, referencedQueryBuilder.toString());
				referencedQueryMap.put(column[0], referencedQuery);
				referencedIdsMap.put(column[0], new HashSet<Integer>());
				referencedDataMap.put(column[0], new HashMap<String, Object[]>());
			}
		}
		queryBuilder.append(" from " + instanceTable + " where ");

		if (filterAttributes != null && filterAttributes.size() > 0) {
			String attributesMap = "";
			for (Map.Entry<String, String> attrMap : filterAttributes.entrySet()) {
				String key = (String) attrMap.getKey();
				String value = (String) attrMap.getValue();
				attributesMap = attributesMap + " attributes->'attribute'->>'" + key + "'='" + value + "' AND ";
			}
			queryBuilder.append(attributesMap);
		}

		queryBuilder.append(" classname='" + classMeta.getName() + "' and status='0'");

		if (ownerEmployeeIds != null && !ownerEmployeeIds.isEmpty())
			queryBuilder.append(" and attributes->'attribute'->>'OwnerEmployeeId' in (:ownerEmployeeIds)");
		queryBuilder.append(" order by modifieddate desc");
		query = instanceDS.createNativeQuery(protocol, queryBuilder.toString());
		if (ownerEmployeeIds != null && !ownerEmployeeIds.isEmpty())
			query.setParameter("ownerEmployeeIds", ownerEmployeeIds);

		rows = instanceDS.executeQueryForFetchingData(protocol, query);

		for (Object[] row : rows)
			for (int i = 0; i < columns.size(); i++)
				if (columns.get(i).length == 2 && row[i + 1] != null)
					referencedIdsMap.get(columns.get(i)[0]).add(Integer.parseInt(row[i + 1].toString()));

		for (String[] column : columns) {
			if (column.length == 2) {
				referencedQuery = referencedQueryMap.get(column[0]);
				if (referencedIdsMap.get(column[0]).isEmpty())
					continue;
				referencedQuery.setParameter("referencedIds", referencedIdsMap.get(column[0]));
				referencedRows = referencedQuery.getResultList();
				for (Object[] row : referencedRows) {
					referencedDataMap.get(column[0]).put(row[0].toString(), row);
				}
			}
		}

		Iterator<Object[]> rowIterator = rows.iterator();
		while (rowIterator.hasNext()) {
			Object[] row = rowIterator.next();
			for (int i = 0; i < columns.size(); i++)
				if (columns.get(i).length == 2 && row[i + 1] != null) {
					if (referencedDataMap.get(columns.get(i)[0]).containsKey(row[i + 1]))
						row[i + 1] = referencedDataMap.get(columns.get(i)[0]).get(row[i + 1])[1];
					else
						rowIterator.remove();
				}
		}

		return rows;
	}

	@Override
	public List<Object[]> getNestedInstanceDataForColumns(Protocol protocol, String dbClassRead,
			List<String> displayedColumns, Map<String, String> filterAttributes, Integer instanceId) {

		List<String[]> columns = displayedColumns.stream().map(column -> column.split("\\.."))
				.collect(Collectors.toList());
		List<String> projection = columns.stream().map(column -> "i" + column.length + ".attributes->'attribute'->>'"
				+ column[column.length - 1] + "' as \"" + column[column.length - 1] + "\"")
				.collect(Collectors.toList());
		Integer max = columns.stream().map(column -> column.length).max(Comparator.comparing(Integer::valueOf)).get();
		Set<String> classCondition = columns.stream()
				.map(column -> getClassInstanceHistoryCondition(column, protocol, Integer.parseInt(dbClassRead)))
				.filter(condition -> condition != null && !condition.equalsIgnoreCase("")).collect(Collectors.toSet());

		Set<String> instanceCondition = columns.stream()
				.map(column -> getInstanceCondition(column, protocol, Integer.parseInt(dbClassRead)))
				.filter(condition -> condition != null && !condition.equalsIgnoreCase("")).collect(Collectors.toSet());

		String query = createQueryForNestedInstanceData(projection, max, classCondition, instanceCondition,
				filterAttributes, instanceId);
		List<Object[]> result = instanceDS.executeNativeQuery(protocol, query);
		logger.warn(query);
		return result;
	}

	@Override
	public List<Object[]> getNestedInstanceDataForColumnsForFiltering(Protocol protocol, String dbClassRead,
			List<String> columns1) {

		List<String[]> columns = columns1.stream().map(column -> column.split("\\..")).collect(Collectors.toList());
		List<String> projection = columns.stream().map(column -> "i" + column.length + ".attributes->'attribute'->>'"
				+ column[column.length - 1] + "' as \"" + column[column.length - 1] + "\"")
				.collect(Collectors.toList());
		Integer max = columns.stream().map(column -> column.length).max(Comparator.comparing(Integer::valueOf)).get();
		Set<String> classCondition = columns.stream()
				.map(column -> getClassInstanceHistoryCondition(column, protocol, Integer.parseInt(dbClassRead)))
				.filter(condition -> condition != null && !condition.equalsIgnoreCase("")).collect(Collectors.toSet());

		Set<String> instanceCondition = columns.stream()
				.map(column -> getInstanceCondition(column, protocol, Integer.parseInt(dbClassRead)))
				.filter(condition -> condition != null && !condition.equalsIgnoreCase("")).collect(Collectors.toSet());

		String query = createQueryForNestedInstanceData(projection, max, classCondition, instanceCondition, null, null);
		List<Object[]> result = instanceDS.executeNativeQuery(protocol, query);
		logger.warn(query);
		return result;
	}

	private String createQueryForNestedInstanceData(List<String> projection, Integer max, Set<String> classCondition,
			Set<String> instanceCondition, Map<String, String> filterAttributes, Integer instanceId) {
		StringBuilder query = new StringBuilder("Select i1.id as instanceid, ");
		String projectionString = projection.stream().map(Object::toString).collect(Collectors.joining(",")).toString();
		query.append(projectionString);
		query.append(" from ");
		String fromString = IntStream.range(1, max + 1).mapToObj(i -> " instancehistory i" + i).map(Object::toString)
				.collect(Collectors.joining(",")).toString();
		query.append(fromString);
		if (classCondition.size() > 0 || instanceCondition.size() > 0) {
			query.append(" where ");

			if (filterAttributes != null && filterAttributes.size() > 0) {
				String attributesMap = "";
				for (Map.Entry<String, String> attrMap : filterAttributes.entrySet()) {
					String key = (String) attrMap.getKey();
					String value = (String) attrMap.getValue();
					attributesMap = attributesMap + " attributes->'attribute'->>'" + key + "'='" + value + "' AND ";
				}
				query.append(attributesMap);
			}

			String classConditionString = classCondition.stream().map(Object::toString)
					.collect(Collectors.joining(" and ")).toString();

			String instanceConditionString = instanceCondition.stream().map(Object::toString)
					.collect(Collectors.joining(" and ")).toString();

			query.append(classConditionString);
			if (!instanceConditionString.equalsIgnoreCase("")) {
				query.append(" and ").append(instanceConditionString);
			}
		}
		if (instanceId != null) {
			query.append(" and i1.instanceid=" + instanceId);
		}
		query.append(";");
		return query.toString();
	}

	private String getInstanceCondition(String[] columns, Protocol protocol, int dbClassRead) {
		String condition = null;
		for (int i = 1; i < columns.length && columns.length > 1; i++) {
			condition = "i" + i + ".attributes->'attribute'->>'" + columns[i - 1] + "'like cast(i" + (i + 1)
					+ ".id as text)";
		}
		return condition;
	}

	private String getClassInstanceHistoryCondition(String[] columns, Protocol protocol, Integer dbClassRead) {
		String condition = null;
		Integer instanceCount = 0;
		try {
			for (String column : columns) {
				ClassMeta classMeta = classService.read(protocol, dbClassRead);
				Optional<Integer> classReadOptional = classMeta.getAttributes().stream()
						.filter(attributeMeta -> attributeMeta.getAttributeName().equalsIgnoreCase(column)).findAny()
						.map(attributeMeta -> attributeMeta.getReferenceClassId());
				dbClassRead = classReadOptional.isPresent() ? classReadOptional.get() : null;
				instanceCount++;
				condition = " i" + instanceCount + ".classname = '" + classMeta.getName() + "' ";

			}
		} catch (ClassRelatedException e) {
			logger.error("Error creating datatable join query", e);
		}
		return condition;
	}

	@Override
	public InstanceData edit(InstanceRequest request)
			throws InstanceRelatedException, ClassRelatedException, InstanceHistoryRelatedException {
		String id = validateCreateRequest(request);
		Instance instance = new Instance(new Instance.Builder().withId(request.getAppId()));

		List<Instance> savedInstanceList = instanceDS.getSavedFormDataByContext(instance, request.getProtocol());

		// actual update
		savedInstanceList.get(0).getAttributes().getAttribute().put("appName", request.getAttributes().get("appName"));
		savedInstanceList.get(0).getAttributes().getAttribute().put("appLogoUrl",
				request.getAttributes().get("appLogoUrl"));

		Instance instanceImproved = new Instance(new Instance.Builder().withId(request.getAppId())
				.withClassName(request.getClassName()).withOrganizationId(request.getProtocol().getOrgId())
				.withStatus(
						request.getStatus() == null ? KernelConstants.EnumDocStatusType.ACTIVE : request.getStatus())
				.withAttributes(savedInstanceList.get(0).getAttributes())
				.withCreatedBy(request.getProtocol().getUserName()).withCreatedDate(System.currentTimeMillis())
				.withClassData(iClassDS.getFromId(request.getProtocol(), Integer.valueOf(id)))
				.withModifiedBy(request.getProtocol().getUserName()).withModifiedDate(System.currentTimeMillis())
				.withUserId(request.getProtocol().getUserName()).withAppId(request.getAppId())
				.withFormId(request.getFormId()).withFormName(request.getFormName()));
		Instance instanceReturned = instanceDS.editApp(instanceImproved, request.getProtocol());
		return convertToInstanceData(instanceReturned);
	}

}

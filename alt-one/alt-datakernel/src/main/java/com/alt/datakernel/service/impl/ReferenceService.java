package com.alt.datakernel.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datakernel.dao.IReferenceDS;
import com.alt.datakernel.exception.ReferenceRelatedException;
import com.alt.datakernel.service.IReferenceService;

public class ReferenceService implements IReferenceService {

	@Autowired(required = true)
	IReferenceDS iReferenceDS;

	@Override
	public List<AttributeMeta> getAllAttributesByClassID(Integer metaClassID, Protocol protocol) throws ReferenceRelatedException {
		return iReferenceDS.getAllAttributesByClassID(protocol, metaClassID);
		
	}
}

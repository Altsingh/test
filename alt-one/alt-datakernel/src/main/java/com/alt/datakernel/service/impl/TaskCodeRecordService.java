/**
 * @author vaibhav.a1
 * */
package com.alt.datakernel.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alt.datacarrier.kernel.db.request.TaskCodeRecordTransport;
import com.alt.datakernel.dao.ITaskCodeRecordDS;
import com.alt.datakernel.exception.TaskCodeRecordRelatedException;
import com.alt.datakernel.model.TaskCodeRecord;
import com.alt.datakernel.service.ITaskCodeRecordService;
import com.alt.datacarrier.core.Protocol;

@Service
public class TaskCodeRecordService implements ITaskCodeRecordService {

	@Autowired(required = true)
	ITaskCodeRecordDS taskCodeRecordDS;

	@Override
	public TaskCodeRecordTransport getLastUpdateCodeDetailsByClassId(TaskCodeRecordTransport taskCodeRecordTransport)
			throws TaskCodeRecordRelatedException {
		TaskCodeRecord taskCodeRecord = new TaskCodeRecord.Builder().withAppId(taskCodeRecordTransport.getAppId())
				.withClasId(taskCodeRecordTransport.getClassId()).build();
		return taskCodeRecordDS.get(taskCodeRecord);
	}

	@Override
	public TaskCodeRecordTransport createTaskCodeRecordForClass(TaskCodeRecordTransport taskCodeRecordTransport,
			Protocol protocol) throws TaskCodeRecordRelatedException {
		TaskCodeRecord taskCodeRecord = new TaskCodeRecord.Builder().withAppId(taskCodeRecordTransport.getAppId())
				.withFormId(taskCodeRecordTransport.getFormId())
				.withFormInitials(taskCodeRecordTransport.getFormInitials())
				.withLastCodeUsed(taskCodeRecordTransport.getLastCodeUsed())
				.withFormName(taskCodeRecordTransport.getFormName())
				.withClasId(taskCodeRecordTransport.getClassId()).build();
		TaskCodeRecordTransport taskCodeRecordTransportreturned = taskCodeRecordDS.add(taskCodeRecord, protocol);
		return taskCodeRecordTransportreturned;
	}

	@Override
	public TaskCodeRecordTransport updateTaskCodeForClass(TaskCodeRecordTransport taskCodeRecordTransport)
			throws TaskCodeRecordRelatedException {
		TaskCodeRecord taskCodeRecord = new TaskCodeRecord.Builder()
				.withLastCodeUsed(taskCodeRecordTransport.getLastCodeUsed())
				.withTaskCodeRecordId(taskCodeRecordTransport.getTaskCodeID()).build();
		return taskCodeRecordDS.update(taskCodeRecord, taskCodeRecordTransport.getProtocol());
	}

}

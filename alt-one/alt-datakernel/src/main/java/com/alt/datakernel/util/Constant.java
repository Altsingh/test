package com.alt.datakernel.util;

public final class Constant {

	private Constant() {

	}

	public static final String CONSTANT_COMMA_SEPARATOR = ",";

	public static final String CONSTANT_SPACE = " ";

	public static final String CONSTANT_DOT = ".";

	public static final String CONSTANT_SINGLE_QUOTE = "'";

	public static final String CONSTANT_DOUBLE_QUOTE = "\"";

	public static final String CONSTANT_STAR = "*";

	public static final String CONSTANT_ARROW = "->";

	public static final String CONSTANT_CAST = "cast";

	public static final String CONSTANT_START_BRACKET = "(";

	public static final String CONSTANT_END_BRACKET = ")";

	public static final String CONSTANT_SELECT = "Select ";

	public static final String CONSTANT_EMPTY = "";

	public static final String CONSTANT_FROM = " from ";

	public static final String CONSTANT_ON = " on ";

	public static final String CONSTANT_EQUAL = "=";

	public static final String CONSTANT_AND = " and ";

	public static final String CONSTANT_WHERE = " where ";

	public static final String CONSTANT_ORDER_BY = " order by ";

	public static final String CONSTANT_OFFSET = " offset ";

	public static final String CONSTANT_LIMIT = " limit ";

	public static final String CONSTANT_AS = " as ";

	public static final String CONSTANT_TEXT = " text ";

	public static final String CONSTANT_COUNT_SELECT_QUERY = " Select count(*) ";

	public static final String CONSTANT_SQUARE_START_BRACKET = "[";

	public static final String CONSTANT_SQUARE_END_BRACKET = "]";
}

package com.alt.datakernel.util;

import java.util.List;

import com.alt.datacarrier.kernel.db.core.AttributesData;
import com.alt.datacarrier.kernel.db.core.DbFilters;
import com.alt.datacarrier.kernel.db.core.DynamicReadRequest;
import com.alt.datacarrier.kernel.db.core.JoinDbClass;
import com.alt.datacarrier.kernel.db.core.OrderAttribute;
import com.alt.datacarrier.kernel.db.core.ProjectionAttribute;
import com.alt.datakernel.exception.QueryRelatedException;

import static com.alt.datakernel.util.Constant.CONSTANT_COMMA_SEPARATOR;
import static com.alt.datakernel.util.Constant.CONSTANT_DOT;
import static com.alt.datakernel.util.Constant.CONSTANT_SPACE;
import static com.alt.datakernel.util.Constant.CONSTANT_STAR;
import static com.alt.datakernel.util.Constant.CONSTANT_ARROW;
import static com.alt.datakernel.util.Constant.CONSTANT_SELECT;
import static com.alt.datakernel.util.Constant.CONSTANT_EMPTY;
import static com.alt.datakernel.util.Constant.CONSTANT_FROM;
import static com.alt.datakernel.util.Constant.CONSTANT_ON;
import static com.alt.datakernel.util.Constant.CONSTANT_EQUAL;
import static com.alt.datakernel.util.Constant.CONSTANT_AND;
import static com.alt.datakernel.util.Constant.CONSTANT_WHERE;
import static com.alt.datakernel.util.Constant.CONSTANT_ORDER_BY;
import static com.alt.datakernel.util.Constant.CONSTANT_LIMIT;
import static com.alt.datakernel.util.Constant.CONSTANT_OFFSET;
import static com.alt.datakernel.util.Constant.CONSTANT_AS;
import static com.alt.datakernel.util.Constant.CONSTANT_COUNT_SELECT_QUERY;
import static com.alt.datakernel.util.Constant.CONSTANT_SINGLE_QUOTE;
import static com.alt.datakernel.util.Constant.CONSTANT_CAST;
import static com.alt.datakernel.util.Constant.CONSTANT_START_BRACKET;
import static com.alt.datakernel.util.Constant.CONSTANT_END_BRACKET;
import static com.alt.datakernel.util.Constant.CONSTANT_TEXT;
import static com.alt.datakernel.util.Constant.CONSTANT_DOUBLE_QUOTE;

public class DynamicDbQueryCreator {

	public String createCountQuery(DynamicReadRequest request) throws QueryRelatedException {
		String query = CONSTANT_EMPTY;

		if (request != null) {
			query += createCountSelectQuery();
			query += createJoins(request.getFromDbClass(), request.getJoinDbClasses());
			query += createFilters(request.getDbFilters());
		} else {
			throw new QueryRelatedException("Request cannot be null");
		}

		return query;
	}

	private String createCountSelectQuery() {
		return CONSTANT_COUNT_SELECT_QUERY;
	}

	public String create(DynamicReadRequest request) throws QueryRelatedException {
		String query = CONSTANT_EMPTY;

		if (request != null) {
			query += createSelect(request.getProjections());
			query += createJoins(request.getFromDbClass(), request.getJoinDbClasses());
			query += createFilters(request.getDbFilters());
			query += createOrderbyLimitOffset(request.getLimit(), request.getOffset(), request.getOrderBy());
		} else {
			throw new QueryRelatedException("Request cannot be null");
		}

		return query;
	}

	private String createSelect(List<ProjectionAttribute> attributes) {
		StringBuilder query = new StringBuilder(CONSTANT_SELECT);
		String result;
		if (attributes == null || attributes.isEmpty()) {
			query.append(CONSTANT_STAR);
			result = query.toString();
		} else {
			for (ProjectionAttribute attribute : attributes) {
				String dbClass = attribute.getDbClass();
				for (AttributesData data : attribute.getDbAttributes()) {
					if (data.isJsonbType() && data.getJsonbAttribute() != null) {
						query.append(CONSTANT_CAST + CONSTANT_START_BRACKET + dbClass + CONSTANT_DOT
								+ data.getAttribute() + CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE + data.getJsonName()
								+ CONSTANT_SINGLE_QUOTE + CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE
								+ data.getJsonbAttribute() + CONSTANT_SINGLE_QUOTE + CONSTANT_SPACE + CONSTANT_AS
								+ CONSTANT_TEXT + CONSTANT_END_BRACKET + CONSTANT_SPACE + CONSTANT_AS + data.getAs()
								+ CONSTANT_SPACE + CONSTANT_COMMA_SEPARATOR);
					} else {
						query.append(dbClass + CONSTANT_DOT + data.getAttribute() + CONSTANT_SPACE + CONSTANT_AS
								+ data.getAs() + CONSTANT_SPACE + CONSTANT_COMMA_SEPARATOR);
					}
				}
			}
			result = query.toString();
			result = result.substring(0, query.lastIndexOf(CONSTANT_COMMA_SEPARATOR));
		}

		return result;

	}

	private String createJoins(String fromDbClass, List<JoinDbClass> joinDbClasses) throws QueryRelatedException {
		if (fromDbClass == null) {
			throw new QueryRelatedException("From class cannot be null in query.");
		}

		StringBuilder query = new StringBuilder(CONSTANT_FROM);
		query.append(fromDbClass + CONSTANT_SPACE + fromDbClass + CONSTANT_SPACE);
		if (joinDbClasses != null) {
			for (JoinDbClass join : joinDbClasses) {
				query.append(join.getType().getName() + CONSTANT_SPACE + join.getDbClass() + CONSTANT_SPACE
						+ join.getDbClass());
				query.append(CONSTANT_ON + CONSTANT_SPACE + join.getFirstConditionFirstColumnTable() + CONSTANT_DOT
						+ join.getFirstConditionFirstColumn() + CONSTANT_EQUAL
						+ join.getFirstConditionSeconfColumnTable() + CONSTANT_DOT
						+ join.getFirstConditionSecondColumn() + CONSTANT_SPACE);
				if (join.getSecondConditionFirstColumn() != null) {
					query.append(CONSTANT_AND + CONSTANT_SPACE + join.getSecondConditionFirstColumnTable()
							+ CONSTANT_DOT + join.getSecondConditionFirstColumn() + CONSTANT_EQUAL
							+ join.getSecondConditionSecondColumnTable() + CONSTANT_DOT
							+ join.getSecondConditionSecondColumn() + CONSTANT_SPACE);
				}
			}
		}
		return query.toString();

	}

	private String createFilters(List<DbFilters> dbFilters) {
		if (dbFilters == null || dbFilters.isEmpty()) {
			return CONSTANT_EMPTY;
		}
		StringBuilder query = new StringBuilder(CONSTANT_WHERE);
		String result;
		for (DbFilters filter : dbFilters) {
			if (filter.isJsonbType() && filter.getJsonbAttribute() != null && !filter.getJsonbAttribute().isEmpty()) {
				if (filter.getCondition().equalsIgnoreCase(DbFilters.CONDITION_JSONB_COMPLEX_ARRAY_CONTAINS_SINGLE)) {
					
					query.append(filter.getClassName() + CONSTANT_DOT + filter.getAttribute() + CONSTANT_SPACE
							+ CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE + filter.getJsonName() + CONSTANT_SINGLE_QUOTE);
					for (String jsonbAttribute : filter.getJsonbAttribute()) {
						query.append(CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE + jsonbAttribute + CONSTANT_SINGLE_QUOTE);
					}
					query.append(CONSTANT_SPACE + filter.getCondition() + CONSTANT_SPACE + CONSTANT_SINGLE_QUOTE + Constant.CONSTANT_SQUARE_START_BRACKET  
							+ filter.getValue() + Constant.CONSTANT_SQUARE_END_BRACKET + CONSTANT_SINGLE_QUOTE + CONSTANT_SPACE + CONSTANT_AND);
				}
				else if(filter.getCondition().equalsIgnoreCase(DbFilters.CONDITION_JSONB_ARRAY_LENGTH)) {
					query.append(filter.getCondition() + CONSTANT_START_BRACKET);
					query.append(filter.getClassName() + CONSTANT_DOT + filter.getAttribute() + CONSTANT_SPACE
							+ CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE + filter.getJsonName() + CONSTANT_SINGLE_QUOTE);
					for (String jsonbAttribute : filter.getJsonbAttribute()) {
						query.append(CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE + jsonbAttribute + CONSTANT_SINGLE_QUOTE);
					}
					
					query.append(CONSTANT_END_BRACKET + CONSTANT_SPACE);
					query.append( CONSTANT_EQUAL + CONSTANT_SPACE + filter.getValue()+ CONSTANT_SPACE + CONSTANT_AND);
					
				}
				else if (filter.getCondition().equalsIgnoreCase(DbFilters.CONDITION_JSON_STRING_ARRAY_CONTAINS_SINGLE)) {
					
					query.append(filter.getClassName() + CONSTANT_DOT + filter.getAttribute() + CONSTANT_SPACE
							+ CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE + filter.getJsonName() + CONSTANT_SINGLE_QUOTE);
					for (String jsonbAttribute : filter.getJsonbAttribute()) {
						query.append(CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE + jsonbAttribute + CONSTANT_SINGLE_QUOTE);
					}
					query.append(CONSTANT_SPACE + filter.getCondition() + CONSTANT_SPACE + CONSTANT_SINGLE_QUOTE  
							+ filter.getValue() + CONSTANT_SINGLE_QUOTE + CONSTANT_SPACE + CONSTANT_AND);
				}
				else {
					query.append(filter.getClassName() + CONSTANT_DOT + filter.getAttribute() + CONSTANT_SPACE
							+ CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE + filter.getJsonName() + CONSTANT_SINGLE_QUOTE);
					for (String jsonbAttribute : filter.getJsonbAttribute()) {
						query.append(CONSTANT_ARROW + CONSTANT_SINGLE_QUOTE + jsonbAttribute + CONSTANT_SINGLE_QUOTE);
					}
					query.append(CONSTANT_SPACE + filter.getCondition() + CONSTANT_SINGLE_QUOTE + CONSTANT_DOUBLE_QUOTE
							+ filter.getValue() + CONSTANT_DOUBLE_QUOTE + CONSTANT_SINGLE_QUOTE + CONSTANT_SPACE
							+ CONSTANT_AND);
				}
			} else {
				query.append(filter.getClassName() + CONSTANT_DOT + filter.getAttribute() + CONSTANT_SPACE
						+ filter.getCondition() + CONSTANT_SINGLE_QUOTE + filter.getValue() + CONSTANT_SINGLE_QUOTE
						+ CONSTANT_SPACE + CONSTANT_AND);
			}
		}
		result = query.toString();
		result = result.substring(0, query.lastIndexOf(CONSTANT_AND));
		return result;

	}

	private String createOrderbyLimitOffset(Integer limit, Integer offset, List<OrderAttribute> orderBy) {
		StringBuilder query = new StringBuilder(CONSTANT_EMPTY);
		String result = CONSTANT_EMPTY;
		if (orderBy != null && !orderBy.isEmpty()) {
			query.append(CONSTANT_ORDER_BY + CONSTANT_SPACE);
			for (OrderAttribute attr : orderBy) {
				if (attr.isJsonbType() && attr.getJsonbAttribute() != null) {
					query.append(attr.getDbClass() + CONSTANT_DOT + attr.getDbAttr() + CONSTANT_SPACE + CONSTANT_ARROW
							+ CONSTANT_SINGLE_QUOTE + attr.getJsonName() + CONSTANT_SINGLE_QUOTE + CONSTANT_ARROW
							+ CONSTANT_SINGLE_QUOTE + attr.getJsonbAttribute() + CONSTANT_SINGLE_QUOTE + CONSTANT_SPACE
							+ CONSTANT_COMMA_SEPARATOR);
				} else {
					query.append(attr.getDbClass() + CONSTANT_DOT + attr.getDbAttr() + CONSTANT_SPACE
							+ attr.getSortDirection() + CONSTANT_COMMA_SEPARATOR);
				}
			}
			result = query.toString();
			result = result.substring(0, query.lastIndexOf(CONSTANT_COMMA_SEPARATOR));
		}

		if (limit != null && limit != -1) {
			result += CONSTANT_LIMIT + CONSTANT_SPACE + limit;
		}

		if (offset != null && offset != -1) {
			result += CONSTANT_OFFSET + CONSTANT_SPACE + offset;
		}

		return result;

	}

}
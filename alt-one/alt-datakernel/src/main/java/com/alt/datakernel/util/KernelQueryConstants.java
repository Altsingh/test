package com.alt.datakernel.util;

public class KernelQueryConstants {
	
	private KernelQueryConstants(){
		
	}
	
	public static final String AS = "AS";
	
	public static final String ON_KEYS = "ON KEYS";
	
	public static final String FROM = "FROM";
	
	public static final String USE_KEYS = "USE KEYS";
	
	public static final String SELECT = "SELECT";
	
	public static final String LIMIT = "LIMIT";
	
	public static final String OFFSET = "OFFSET";
	
	public static final String SUCCESS = "Success";
	
	public static final String ORDER_BY = "ORDER BY";
	
	public static final String JOIN = "JOIN";
	
	public static final String WHERE = "WHERE";
	
	public static final String AND = "AND";
	
	public static final String UNNEST = "UNNEST";
	
}

package com.alt.datakernel.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alt.datacarrier.common.enumeration.EnumDataType;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.request.CreateClassRequest;
import com.alt.datakernel.dao.IClassDS;
import com.alt.datakernel.exception.ClassRelatedException;

@Service
public class ClassValidator {

    Logger logger = Logger.getLogger(ClassValidator.class.toString());

    @Autowired(required = true)
	IClassDS classDS;

	public void validateWriteRequest(CreateClassRequest request) throws ClassRelatedException {
		if(request.getClassName() == null) {
			throw new ClassRelatedException("Class Name in CreateClassRequest cannot be null.");
		}
		if(request.getStatus() == null) {
			throw new ClassRelatedException("Status in CreateClassRequest cannot be null.");
		}
		if(request.getLocationSpecific() == null) {
			throw new ClassRelatedException("Location specific in CreateClassRequest cannot be null.");
		}
		if(request.getCustomAttributeAllowed() == null) {
			throw new ClassRelatedException("CustomAttributeAllowed in CreateClassRequest cannot be null.");
		}
//		if(request.getPackageCodes() == null || request.getPackageCodes().isEmpty()) {
//			throw new ClassRelatedException("PackageCodes in CreateClassRequest cannot be empty or null.");
//		}
		if(request.getAttributes() == null || request.getAttributes().isEmpty()) {
			throw new ClassRelatedException("Attributes in CreateClassRequest cannot be empty or null.");
		}
		if(request.getSystemType() == null) {
			throw new ClassRelatedException("SystemType in CreateClassRequest cannot be null.");
		}
		if(request.getParameterType() == null) {
			throw new ClassRelatedException("ParameterType in CreateClassRequest cannot be null.");
		}
		checkAlphanumericWithSpaces(request.getClassName());
		checkUniqueness(request.getClassName(),request.getProtocol());
		checkAttributes(request.getProtocol(), request.getAttributes());
	}

	public void validateUpdateWriteRequest(CreateClassRequest request) throws ClassRelatedException {
		if(request.getClassName() == null) {
			throw new ClassRelatedException("Class Name in CreateClassRequest cannot be null.");
		}
		if(request.getStatus() == null) {
			throw new ClassRelatedException("Status in CreateClassRequest cannot be null.");
		}
		if(request.getLocationSpecific() == null) {
			throw new ClassRelatedException("Location specific in CreateClassRequest cannot be null.");
		}
		if(request.getCustomAttributeAllowed() == null) {
			throw new ClassRelatedException("CustomAttributeAllowed in CreateClassRequest cannot be null.");
		}
//		if(request.getPackageCodes() == null || request.getPackageCodes().isEmpty()) {
//			throw new ClassRelatedException("PackageCodes in CreateClassRequest cannot be empty or null.");
//		}
		if(request.getAttributes() == null || request.getAttributes().isEmpty()) {
			throw new ClassRelatedException("Attributes in CreateClassRequest cannot be empty or null.");
		}
		if(request.getSystemType() == null) {
			throw new ClassRelatedException("SystemType in CreateClassRequest cannot be null.");
		}
		if(request.getParameterType() == null) {
			throw new ClassRelatedException("ParameterType in CreateClassRequest cannot be null.");
		}
		checkAlphanumericWithSpaces(request.getClassName());
		checkAttributes(request.getProtocol(), request.getAttributes());
	}
	
	private void checkUniqueness(String className, Protocol protocol) throws ClassRelatedException {
		ClassMeta meta = null;
		try {
			//Race condition
			meta = classDS.get(className, protocol);
		}catch(Exception e) {
			logger.log(Level.INFO, e.getMessage());
		}	
		if(meta !=null) {
			throw new ClassRelatedException("ClassName duplication in checkUniqueness for class name : " + className + " and org : " + protocol.getOrgCode());
		}
		
	}

	private void checkAttributes(Protocol protocol, List<AttributeMeta> attributes) throws ClassRelatedException {
		List<String> attributeNames = new ArrayList<>();
		for(AttributeMeta attribute: attributes) {
			if(attributeNames.contains(attribute.getAttributeName())) {
				throw new ClassRelatedException("AttributeName duplication in checkAttributes for attribute : " + attribute.getAttributeName());
			}else {
				attributeNames.add(attribute.getAttributeName());
			}
			checkAlphanumericWithSpaces(attribute.getAttributeName());
			if(attribute.getAttributeName() == null || attribute.getAttributeName().isEmpty()) {
				throw new ClassRelatedException("AttributeName in checkAttributes cannot be null or empty.");
			}
			if(attribute.getPlaceholderType() == null) {
				throw new ClassRelatedException("PlaceholderType for attribute : " + attribute.getAttributeName() + " in checkAttributes cannot be null");
			}
			if(attribute.getUnique() == null) {
				throw new ClassRelatedException("Unique for attribute : " + attribute.getAttributeName() + " in checkAttributes cannot be null");
			}
			if(attribute.getDataType()==null) {
				throw new ClassRelatedException("DataType for attribute : " + attribute.getAttributeName() + " in checkAttributes cannot be null");
			}
			if(attribute.getStatus()==null) {
				throw new ClassRelatedException("Status for attribute : " + attribute.getAttributeName() + " in checkAttributes cannot be null");
			}
			if(attribute.getIndexable()==null) {
				throw new ClassRelatedException("Indexable for attribute : " + attribute.getAttributeName() + " in checkAttributes cannot be null");
			}
			if(attribute.getSearchable()==null) {
				throw new ClassRelatedException("Searchable for attribute : " + attribute.getAttributeName() + " in checkAttributes cannot be null");
			}
			if(attribute.getDataType() == EnumDataType.REFERENCE || attribute.getDataType() == EnumDataType.REFERENCELIST) {
				checkReferenceExists(protocol, attribute.getReferenceClassId(), attribute.getAttributeName());
			}
		}
		
	}

	private void checkReferenceExists(Protocol protocol, Integer referenceClassId, String attribute) throws ClassRelatedException {
		try {
			classDS.get(referenceClassId, protocol);
		}catch(Exception e) {
			logger.log(Level.WARNING, e.getMessage(), e);
			throw new ClassRelatedException("Reference Exception inside checkReferenceExists for attribute : "+ attribute, e);
		}
	}

	private void checkAlphanumericWithSpaces(String className) throws ClassRelatedException {
		String classNameTemp = className.replaceAll(" ", "0");
		String pattern= "^[a-zA-Z0-9]*$";
		if(!classNameTemp.matches(pattern)) {
			throw new ClassRelatedException("Classname in CreateClassRequest is not alphanumeric(with spaces).");
		}
	}

}

package com.alt.datakernel.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.request.FormRequest;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datakernel.dao.IFormDS;
import com.alt.datakernel.exception.FormRelatedException;

@Service
public class FormValidator {

	Logger logger = Logger.getLogger(FormValidator.class.toString());

	@Autowired(required = true)
	IFormDS formDS;

	public void validateWriteRequest(FormRequest request) throws FormRelatedException {
		if (request.getStatus() == null) {
			throw new FormRelatedException("Status cannot be null in CreateFormRequest.");
		}
//		if(request.getComponentList() == null || request.getComponentList().isEmpty()) {
//			throw new FormRelatedException("Components cannot be null or empty in CreateFormRequest.");
//		}
		if (request.getFormType() == null) {
			throw new FormRelatedException("FormType cannot be null in CreateFormRequest.");
		}
		if (request.getFormName() == null) {
			throw new FormRelatedException("FormName cannot be null in CreateFormRequest.");
		}
		if (request.getProtocol() == null) {
			throw new FormRelatedException("Protocol cannot be null in CreateFormRequest.");
		}
		request.setFormName(request.getFormName().trim());
		checkAlphanumericWithSpaces(request.getFormName());
		checkUniqueness(request.getFormName(), request.getProtocol(), request.getVersion());
		checkComponents(request.getComponentList());
	}

	private void checkUniqueness(String formName, Protocol protocol, String version) throws FormRelatedException {
		AltForm form = null;
		try {
			// Race condition
			form = formDS.get(protocol, formName, version);
		} catch (Exception e) {
			logger.log(Level.INFO, e.getMessage());
		}
		if (form != null) {
			throw new FormRelatedException("FormName duplication in FormValidator(checkUniqueness) for form name : "
					+ formName + " and org : " + protocol.getOrgCode());
		}

	}

	private void checkComponents(List<AltAbstractComponent> components) throws FormRelatedException {
		List<String> componentIds = new ArrayList<>();

		for (AltAbstractComponent component : components) {
			if (componentIds.contains(component.getId())) {
				throw new FormRelatedException(
						"Component id duplication in FormValidator(checkComponents) for component : "
								+ component.getId());
			} else {
				componentIds.add(component.getId());
			}
			if (component.getNamespace() == null) {
				throw new FormRelatedException(
						"Namespace cannot be null in FormValidator(checkComponents) for component : "
								+ component.getId());
			}
			if (component.getName() == null) {
				throw new FormRelatedException(
						"Form name cannot be null in FormValidator(checkComponents) for component : "
								+ component.getId());
			}
			if (component.getControlType() == null) {
				throw new FormRelatedException(
						"Control type cannot be null in FormValidator(checkComponents) for component : "
								+ component.getId());
			}
		}

	}

	private void checkAlphanumericWithSpaces(String className) throws FormRelatedException {
		String classNameTemp = className.replaceAll(" ", "0");
		String pattern = "^[a-zA-Z0-9]*$";
		if (!classNameTemp.matches(pattern)) {
			throw new FormRelatedException("Classname in CreateClassRequest is not alphanumeric(with spaces).");
		}
	}

	public void validateUpdateRequest(FormRequest request) throws FormRelatedException {
		if (request.getFormId() == null) {
			throw new FormRelatedException("FormId cannot be null in CreateFormRequest.");
		}
		if (request.getProtocol() == null) {
			throw new FormRelatedException("Protocol cannot be null in CreateFormRequest.");
		}
		if (request.getComponentList() != null && !request.getComponentList().isEmpty()) {
			checkComponents(request.getComponentList());
		}
	}

	public void validateStateUpdateRequest(FormRequest request) throws FormRelatedException {
		if (request.getFormId() == null) {
			throw new FormRelatedException("FormId cannot be null in CreateFormRequest.");
		}
		if (request.getProtocol() == null) {
			throw new FormRelatedException("Protocol cannot be null in CreateFormRequest.");
		}
	}

}

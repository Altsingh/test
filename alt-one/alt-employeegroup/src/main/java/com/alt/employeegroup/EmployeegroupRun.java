package com.alt.employeegroup;

import com.alt.employeegroup.service.EmployeeGroupServiceImpl;
import com.alt.employeegroup.service.PolicyServiceImpl;
import com.alt.employeegroup.service.ReadService;
import com.alt.employeegroup.service.WriteService;
import com.alt.employeegroup.util.DbUtil;
import com.alt.employeegroup.util.Util;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = { "com.alt.employeegroup.*" })
public class EmployeegroupRun {

	public static void main(String[] args) {
		SpringApplication.run(EmployeegroupRun.class, args);

	}
	@Bean
	public Util Util() {
		return new Util();
	}
	
	@Bean
	public WriteService WriteService() {
		return new WriteService();
	}
	
	@Bean
	public ReadService ReadService() {
		return new ReadService();
	}
	
	@Bean
	public DbUtil HttpUtil() {
		return new DbUtil();
	}
	
	@Bean
	public EmployeeGroupServiceImpl EmployeeGroupService() {
		return new EmployeeGroupServiceImpl();
	}
	
	@Bean
	public PolicyServiceImpl PolicyService() {
		return new PolicyServiceImpl();
	}

}

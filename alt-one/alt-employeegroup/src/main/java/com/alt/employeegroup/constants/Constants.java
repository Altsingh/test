package com.alt.employeegroup.constants;

public class Constants {
	private Constants(){}
	
	public static final String HEADER_ORG = "organization";
	public static final String HEADER_MODULE = "module";
	public static final String EMPLOYEE_GROUP_NAME = "GroupName";
	public static final String EMPLOYEE_GROUP_PARAM = "ParameterList";
	public static final String EMPLOYEE_GROUP_VALUE = "ParameterValue";
	public static final String READ_PARAMETER_LIST = "PARAM";
	public static final String READ_PARAMETER_VALUE_LIST = "PARAMVALUE";
	public static final String RESPONSE_SUCCESS = "SUCCESS";
	public static final Integer RESPONSE_SUCCESS_CODE = 400;
	public static final String RESPONSE_FAIL = "FAIL";
	public static final Integer RESPONSE_FAIL_CODE = 200;
	public static final String RESPONSE_GROUP_CREATION_PASS = "Employee group created successfully";
	public static final String FAILURE_MSG_EMPTYGROUPNAME = "Employee group name cannot be empty";
	public static final String FAILURE_MSG_ALPGANUMERIC = "Employee group name should be alphanumeric only";
	public static final String FAILURE_MSG_DUPLICATEGROUPNAME = "Employee group with same name already exists";
	public static final String PASS_MSG_GROUPNAMEVALIDATED = "Group name validated";
	public static final String FAILURE_MSG_EMPTYGROUPPARAM = "Employee group parameters cannot be empty";
	public static final String FAILURE_MSG_DUPLICATEGROUPPARAM = "Employee group with same parameters already exists";
	public static final String PASS_MSG_GROUPPARAMVALIDATED = "Group parameters validated";
	public static final String FAILURE_MSG_BADREQUEST = "Bad request data";
	public static final String FAILURE_MSG_DBREADERROR = "Error reading data from database";
	public static final String RESPONSE_GROUP_UPDATE_PASS = "Employee group updated successfully";
	public static final String RESPONSE_UNEXPECTED_FAIL = "Unexpected error has occurred.";
	public static final Integer SESSION_TIMEOUT_CODE = 500;
	public static final String SESSION_TIMEOUT_MESSAGE = "SESSION_TIMEOUT";
	public static final String ATTRIBUTE_VALUE = "value";
	public static final String EMPTY_EMPLOYEE_LIST = "No employees associated with the given group";
	public static final String GROUPEMPLOYEE_FETCH_FAILED = "Error reading group employee list";
	public static final String EMPLOYEE_OVERLAP_ERROR = "Employees linked overlapp each other";
	public static final String PASS_SUCCESS = "Successfully completed";
}

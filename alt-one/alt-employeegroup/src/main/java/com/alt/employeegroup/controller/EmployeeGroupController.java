package com.alt.employeegroup.controller;

import com.alt.employeegroup.service.IEmployeeGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;



@RestController
@EnableAutoConfiguration
public class EmployeeGroupController {

	@Autowired(required = true)
	IEmployeeGroupService employeeGroupService;

	@RequestMapping(value = "/employeegroup/getParameters", method = RequestMethod.POST)
	@ResponseBody
	public String getParameters(@RequestBody String request) throws IOException {
		String response = null;
		response = employeeGroupService.getParameters(request);
		return response;
	}

	@RequestMapping(value = "/employeegroup/createnewgroup", method = RequestMethod.POST)
	@ResponseBody
	public String createNewGroup(@RequestBody String request) throws IOException {
		String response = null;
		response = employeeGroupService.createNewGroup(request);
		return response;
	}

	@RequestMapping(value = "/employeegroup/getGroupData", method = RequestMethod.POST)
	@ResponseBody
	public String getGroupData(@RequestBody String request) throws IOException {
		String response = null;
		response = employeeGroupService.getGroupData(request);
		return response;
	}

	@RequestMapping(value = "/employeegroup/updateDeleteGroup", method = RequestMethod.POST)
	@ResponseBody
	public String updateDeleteGroup(@RequestBody String request) throws IOException {
		String response = null;
		response = employeeGroupService.updateDeleteGroup(request);
		return response;
	}

}

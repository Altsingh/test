package com.alt.employeegroup.controller;

import com.alt.employeegroup.service.IPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;



@RestController
@EnableAutoConfiguration
public class PolicyController {

	@Autowired(required = true)
	IPolicyService policyService;

	@RequestMapping(value = "/employeegroup/crudPolicy", method = RequestMethod.POST)
	@ResponseBody
	public String crudPolicy(@RequestBody String request) throws IOException {
		String response = null;
		response = policyService.crudPolicy(request);
		return response;
	}

	@RequestMapping(value = "/employeegroup/getPolicyCreateData", method = RequestMethod.POST)
	@ResponseBody
	public String getPolicyCreateData(@RequestBody String request) throws IOException {
		String response = null;
		response = policyService.getPolicyCreateData(request);
		return response;
	}

	@RequestMapping(value = "/employeegroup/getGroupEmployees", method = RequestMethod.POST)
	@ResponseBody
	public String getGroupEmployees(@RequestBody String request) throws IOException {
		String response = null;
		response = policyService.getGroupEmployees(request);
		return response;
	}

	@RequestMapping(value = "/employeegroup/tagPolicy", method = RequestMethod.POST)
	@ResponseBody
	public String tagPolicy(@RequestBody String request) throws IOException {
		String response = null;
		response = policyService.tagPolicy(request);
		return response;
	}
}

package com.alt.employeegroup.service;

import com.alt.core.util.JwtUtil;
import com.alt.datacarrier.core.IndexingData;
import com.alt.datacarrier.employeegroup.common.*;
import com.alt.datacarrier.employeegroup.common.EmployeeGroupContants.EnumDataType;
import com.alt.datacarrier.kernel.common.*;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.alt.employeegroup.constants.Constants;
import com.alt.employeegroup.controller.EmployeeGroupController;
import com.alt.employeegroup.util.DbUtil;
import com.alt.employeegroup.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class EmployeeGroupServiceImpl implements IEmployeeGroupService {

	@Autowired(required = true)
	Util util;

	@Autowired(required = true)
	DbUtil httputil;

	@Autowired(required = true)
	ReadService readService;

	@Autowired(required = true)
	WriteService writeService;

	@Autowired(required = true)
	IEmployeeGroupService employeeGroupService;

	private static final Logger logger = Logger.getLogger(EmployeeGroupController.class);

	EmpGroupResponse finalResponse = new EmpGroupResponse();

	JwtUtil jwtUtil = new JwtUtil();

	/*
	 * fetches employee group parameter/ parameter value listing for an
	 * organization based on what request is made
	 */
	public String getParameters(@RequestBody String requestData) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String jwt = null;
		DataReadResponse queryResponse = null;
		String responseString = null;
		GetEmpParamsRequest getEmpParamsRequest = new GetEmpParamsRequest();
		try {
			getEmpParamsRequest = mapper.readValue(requestData, GetEmpParamsRequest.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseString = util.setResponse(false, Constants.FAILURE_MSG_BADREQUEST, null);
			return responseString;
		}
		logger.info("validating jwt");
		/*
		 * try { String claimsString =
		 * jwtUtil.validateJwt(getEmpParamsRequest.getProtocol().getJwt()); if
		 * (claimsString.equals(Constants.SESSION_TIMEOUT_MESSAGE)) {
		 * logger.error(Constants.SESSION_TIMEOUT_MESSAGE); response =
		 * util.setResponse(false, Constants.SESSION_TIMEOUT_MESSAGE, null);
		 * return response; } logger.info("claims: " + claimsString); // jwt =
		 * jwtUtil.generateJwt((String)claims.get("username"), //
		 * (String)claims.get("orgId")); } catch (Exception e) {
		 * logger.error(e.getMessage()); response = util.setResponse(false,
		 * "jwt verification failed", null); return response; }
		 */
		List<String> parameterGroupList = getEmpParamsRequest.getParameterGroupList();
		Map<String, Object> paramValueMap = new HashMap<>();
		List<ProjectionAttribute> projectionList = new ArrayList<>();
		ProjectionAttribute projectionAttribute = new ProjectionAttribute();
		
		String paramValueMapKey = null;
		try {
			IndexingData indexingData = getEmpParamsRequest.getIndexingData();
			logger.info("Fetching emp group parameter list");
			if (parameterGroupList == null) {
				// create query
				List<AttributesData> attrList = new ArrayList<>();
				AttributesData attributesData1 = new AttributesData();
				attributesData1.setAttribute("classCode");
				attributesData1.setReference(false);
				AttributesData attributesData2 = new AttributesData();
				attributesData2.setAttribute("name");
				attributesData2.setReference(false);
				attrList.add(attributesData1);
				attrList.add(attributesData2);

				projectionAttribute.setDbAttribute(attrList);
				projectionList.add(projectionAttribute);

				DBFilters dBFilters = new DBFilters();
				dBFilters.setAttribute("parameterType");
				dBFilters.setCondition("=");
				dBFilters.setValue("true");
				DBFilters dBFilter2 = new DBFilters();
				dBFilter2.setAttribute("type");
				dBFilter2.setCondition("=");
				dBFilter2.setValue("CLASS");
				List<DBFilters> filterList = new ArrayList<>();
				filterList.add(dBFilter2);
				filterList.add(dBFilters);
				paramValueMapKey = "parameters";
				List<String> filterJoin = new ArrayList<>();
				filterJoin.add("AND");
				queryResponse = util.getQueryData(getEmpParamsRequest.getProtocol(), projectionAttribute, filterList,
						filterJoin, indexingData, true);
				paramValueMap.put(paramValueMapKey, queryResponse.getResponse());
				logger.info("Data fetched !!");
			} else {
				logger.info("Fetching all instances of given emp group parameter");
				for (String parameterClassCode : parameterGroupList) {
					String displayUnitAttrCode = util.getDisplayUnit(getEmpParamsRequest.getProtocol(),
							parameterClassCode);
					// create query to fetch data

					List<AttributesData> attrList = new ArrayList<>();
					AttributesData attributesData1 = new AttributesData();
					attributesData1.setAttribute("id");
					attributesData1.setReference(false);
					AttributesData attributesData2 = new AttributesData();
					attributesData2.setAttribute("attributes." + displayUnitAttrCode);
					attributesData2.setReference(false);
					attrList.add(attributesData1);
					attrList.add(attributesData2);

					projectionAttribute.setDbAttribute(attrList);
					projectionList.add(projectionAttribute);

					DBFilters dBFilters = new DBFilters();
					dBFilters.setAttribute("classCode");
					dBFilters.setCondition("=");
					dBFilters.setValue(parameterClassCode);
					DBFilters dBFilter2 = new DBFilters();
					dBFilter2.setAttribute("type");
					dBFilter2.setCondition("=");
					dBFilter2.setValue("INSTANCE");
					List<DBFilters> filterList = new ArrayList<>();
					filterList.add(dBFilter2);
					filterList.add(dBFilters);
					paramValueMapKey = parameterClassCode;
					List<String> filterJoin = new ArrayList<>();
					filterJoin.add("AND");
					queryResponse = util.getQueryData(getEmpParamsRequest.getProtocol(), projectionAttribute,
							filterList, filterJoin, indexingData, true);
					paramValueMap.put(paramValueMapKey, queryResponse.getResponse());
				}
				logger.info("Data fetched !!");
			}
			responseString = new ObjectMapper().writeValueAsString(paramValueMap);
			responseString = util.setResponse(true, responseString, jwt);
		} catch (Exception e) {
			logger.error(e);
			responseString = util.setResponse(false, Constants.RESPONSE_UNEXPECTED_FAIL, null);
		}
		return responseString;
	}

	/*
	 * If all parameter validation succeeds, a new employee group is created.
	 * Validations needed: • group name should be unique. • Parameter and
	 * parameter value combination should be unique i.e. no two groups should
	 * have exact same employee listing.
	 */
	public String createNewGroup(@RequestBody String request) throws IOException {
		String response = null;
		EmpGroupResponse responseData;
		ObjectMapper mapper = new ObjectMapper();
		KernelInstanceProcessRequest requestData = new KernelInstanceProcessRequest();
		KernelInstanceProcessData processData = new KernelInstanceProcessData();
		SaveEmpGroupRequest saveEmpGroupRequest = new SaveEmpGroupRequest();
		String jwt = null;
		try {
			saveEmpGroupRequest = mapper.readValue(request, SaveEmpGroupRequest.class);
		} catch (Exception e) {
			logger.error("Error:", e);
			return util.setResponse(false, Constants.FAILURE_MSG_BADREQUEST, null);
		}
		try {
			String employeeGroupName = saveEmpGroupRequest.getGroupName();
			Map<String, String> attributes = readService.getEmpGroupAttributes(saveEmpGroupRequest.getProtocol(),
					saveEmpGroupRequest.getOrgEmployeeGroupClassCode());
			writeService.setProtocol(saveEmpGroupRequest.getProtocol());
			writeService.setAttributes(attributes);
			logger.info("Validate group name");
			response = writeService.validateGroupName(employeeGroupName, null,
					saveEmpGroupRequest.getOrgEmployeeGroupClassCode());
			if (response.equals(Constants.PASS_MSG_GROUPNAMEVALIDATED)) {
				logger.info("Group name validation completed");
			} else {
				logger.info("Group name validation failed");
				return util.setResponse(false, Constants.FAILURE_MSG_DUPLICATEGROUPNAME, null);
			}
			logger.info("Validate group parameters");
			responseData = writeService.validateGroupParameter(saveEmpGroupRequest.getParameterList(), null,
					saveEmpGroupRequest.getParamValueMap());
			response = responseData.getRespMsg();
			if (response.equals(Constants.PASS_MSG_GROUPPARAMVALIDATED)) {
				logger.info("Group parameter validation completed");
			} else if (response.equals(Constants.FAILURE_MSG_DUPLICATEGROUPPARAM)) {
				logger.info("Group parameter validation failed");
				return util.setResponse(false,
						Constants.FAILURE_MSG_DUPLICATEGROUPPARAM + " Existing group: " + responseData.getRespData(),
						null);
			} else {
				logger.info("Group parameter validation failed");
				return util.setResponse(false, Constants.RESPONSE_UNEXPECTED_FAIL, null);
			}
			logger.info("Group validated. Saving group");
			processData.setClassCode(saveEmpGroupRequest.getOrgEmployeeGroupClassCode());
			processData.setType(EnumRequestType.INSTANCE);
			processData
					.setStatus(saveEmpGroupRequest.isActive() ? EnumDocStatusType.ACTIVE : EnumDocStatusType.INACTIVE);
			Map<String, KernelSingleAttributeValueObject> attr = new HashMap<>();
			KernelSingleAttributeValueObject att1 = new KernelSingleAttributeValueObject();
			att1.setValue(saveEmpGroupRequest.getGroupName());
			attr.put(attributes.get(Constants.EMPLOYEE_GROUP_NAME), att1);
			KernelSingleAttributeValueObject att2 = new KernelSingleAttributeValueObject();
			String val2 = mapper.writeValueAsString(saveEmpGroupRequest.getParameterList());
			att2.setValue(val2);
			attr.put(attributes.get(Constants.EMPLOYEE_GROUP_PARAM), att2);
			KernelSingleAttributeValueObject att3 = new KernelSingleAttributeValueObject();
			String val3 = mapper.writeValueAsString(saveEmpGroupRequest.getParamValueMap());
			att3.setValue(val3);
			attr.put(attributes.get(Constants.EMPLOYEE_GROUP_VALUE), att3);
			processData.setAttributes(attr);
			requestData.setAction(EnumCRUDOperations.CREATE);
			requestData.setInstanceData(processData);

			List<KernelInstanceProcessRequest> requestDatalist = new ArrayList<>();
			requestDatalist.add(requestData);
			response = util.saveInstanceForGivenClass(saveEmpGroupRequest.getProtocol(), requestDatalist);
			JSONObject obj = new JSONObject(response);
			if (obj.get("responseCode").equals("Success-100")) {
				response = util.setResponse(true, Constants.RESPONSE_GROUP_CREATION_PASS, jwt);
			} else {
				response = util.setResponse(false, (String) obj.get("errMsg"), null);
			}
		} catch (Exception e) {
			logger.error("Error:", e);
			response = util.setResponse(false, Constants.RESPONSE_UNEXPECTED_FAIL, null);
		}
		return response;
	}

	/*
	 * displays group data. If group class id is given all active groups for
	 * that organization are fetched. If instance id of any group is given, data
	 * for only that group is fetched.
	 */
	public String getGroupData(@RequestBody String request) throws IOException {

		String response = null;
		String jwt = null;
		String getDataType = null;
		ObjectMapper mapper = new ObjectMapper();
		DataReadResponse dataReadResponse = new DataReadResponse();
		ReadEmployeeGroupDataRequest groupReadRequest = new ReadEmployeeGroupDataRequest();
		/* test step */
		// request =
		// "{\"protocol\":{\"userCode\":\"\",\"userName\":\"\",\"orgCode\":\"org2\",\"tenantCode\":\"\",\"sysFormCode\":\"\",\"portalType\":\"ADMIN\",\"loggedInUserIdentifier\":\"\",\"jwt\":\"eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0OTg4MDQyMTQsInN1YiI6ImxvZ2luIiwidXNlcm5hbWUiOiJsYXZpbmEuc2FuZ2h2aSIsIm9yZ0lkIjoib3JnMiIsImlzcyI6IkFsdC1Mb2dpbiIsImV4cCI6MTQ5ODgwNDMxNH0.CbUZF16FKH1PWhrb0vTzNMA_jeuNPCPRaSMglsoXWRw\",\"loggedInTimestamp\":\"\"},\"indexingData\":{\"offset\":\"0\",\"limit\":null,\"orderBy\":\"id\",\"order\":\"ASC\"}}";
		try {
			groupReadRequest = mapper.readValue(request, ReadEmployeeGroupDataRequest.class);
		} catch (Exception e) {
			logger.error("Error:", e);
			return util.setResponse(false, Constants.FAILURE_MSG_BADREQUEST, null);
		}
		try {
			List<ProjectionAttribute> projectionList = new ArrayList<>();
			ProjectionAttribute projectionAttribute = new ProjectionAttribute();
			List<DBFilters> filterList = new ArrayList<>();
			IndexingData indexingData = groupReadRequest.getIndexingData();
			String docId = util.getDocIdForClass(groupReadRequest.getProtocol(), "EmployeeGroup");
			List<AttributesData> attrList = new ArrayList<>();
			AttributesData attributesData1 = new AttributesData();
			attributesData1.setAttribute("id");
			attributesData1.setReference(false);
			AttributesData attributesData2 = new AttributesData();
			attributesData2.setAttribute("attributes");
			attributesData2.setReference(false);
			AttributesData attributesData3 = new AttributesData();
			attributesData3.setAttribute("status");
			attributesData3.setReference(false);
			attrList.add(attributesData1);
			attrList.add(attributesData2);
			attrList.add(attributesData3);

			projectionAttribute.setDbAttribute(attrList);
			projectionList.add(projectionAttribute);

			DBFilters dBFilters = new DBFilters();
			dBFilters.setAttribute("classCode");
			dBFilters.setCondition("=");
			dBFilters.setValue(docId);
			DBFilters dBFilter2 = new DBFilters();
			if (groupReadRequest.getType() == EnumDataType.EMPLOYEEGROUPMETA) {
				getDataType = "CLASS";
			} else if (groupReadRequest.getType() == EnumDataType.EMPLOYEEGROUP) {
				getDataType = "INSTANCE";
			}
			dBFilter2.setAttribute("type");
			dBFilter2.setCondition("=");
			dBFilter2.setValue(getDataType);
			filterList.add(dBFilter2);

			filterList.add(dBFilters);
			List<String> filterJoin = new ArrayList<>();
			filterJoin.add("AND");
			dataReadResponse = util.getQueryData(groupReadRequest.getProtocol(), projectionAttribute, filterList,
					filterJoin, indexingData, true);
			if (groupReadRequest.getType() == EnumDataType.EMPLOYEEGROUPMETA) {
				// meta will always be a list of size 0
				List<Map<String, Object>> responseData = dataReadResponse.getResponse();
				response = util.setResponse(true, responseData.get(0), jwt);
			} else if (groupReadRequest.getType() == EnumDataType.EMPLOYEEGROUP) {
				response = util.setResponse(true, dataReadResponse, jwt);
			}

		} catch (Exception e) {
			logger.error("Error:", e);
			response = util.setResponse(false, Constants.RESPONSE_UNEXPECTED_FAIL, null);
		}
		return response;
	}

	/*
	 * If update is requested, validate the parameters and save updated values.
	 * If delete is requested soft delete (deactivate the requested group) is
	 * performed.
	 */
	public String updateDeleteGroup(@RequestBody String request) throws IOException {
		String response = null;
		ObjectMapper mapper = new ObjectMapper();
		KernelInstanceProcessRequest requestData = new KernelInstanceProcessRequest();
		KernelInstanceProcessData processData = new KernelInstanceProcessData();
		UpdateDeleteRequest updateDeleteRequest = new UpdateDeleteRequest();
		Map<String, KernelSingleAttributeValueObject> attr = new HashMap<>();
		String jwt = null;
		try {
			updateDeleteRequest = mapper.readValue(request, UpdateDeleteRequest.class);
		} catch (Exception e) {
			logger.error("Error:", e);
			return util.setResponse(false, Constants.FAILURE_MSG_BADREQUEST, null);
		}
		try {
			if (updateDeleteRequest.getRequestType().equals(UserRequestType.DELETE)) {
				// do soft delete
				logger.info("Disabling group");
				processData.setClassCode(updateDeleteRequest.getOrgEmployeeGroupClassCode());
				processData.setType(EnumRequestType.INSTANCE);
				requestData.setAction(EnumCRUDOperations.SOFT_DELETE);
			} else if (updateDeleteRequest.getRequestType().equals(UserRequestType.UPDATE)) {
				// update group data
				Map<String, String> attributes = readService.getEmpGroupAttributes(updateDeleteRequest.getProtocol(),
						updateDeleteRequest.getOrgEmployeeGroupClassCode());
				writeService.setProtocol(updateDeleteRequest.getProtocol());
				writeService.setAttributes(attributes);
				KernelSingleAttributeValueObject att1 = new KernelSingleAttributeValueObject();
				requestData.setAction(EnumCRUDOperations.UPDATE);
				if (!(updateDeleteRequest.getGroupName() == null)) {
					logger.info("Validate group name");
					String employeeGroupName = updateDeleteRequest.getGroupName();
					String employeeGroupInstanceCode = updateDeleteRequest.getEmpGroupInstanceCode();
					response = writeService.validateGroupName(employeeGroupName, employeeGroupInstanceCode,
							updateDeleteRequest.getOrgEmployeeGroupClassCode());
					if (response.equals(Constants.PASS_MSG_GROUPNAMEVALIDATED)) {
						logger.info("Group name validation completed");
					} else {
						logger.info("Group name validation failed");
						throw new Exception(response);
					}
					response = null;
					att1.setValue(updateDeleteRequest.getGroupName());
					attr.put(attributes.get(Constants.EMPLOYEE_GROUP_NAME), att1);
				}
				if (!(updateDeleteRequest.getParameterList().isEmpty()
						|| updateDeleteRequest.getParamValueMap().isEmpty())) {
					logger.info("Validate group parameters");
					EmpGroupResponse responseData = writeService.validateGroupParameter(
							updateDeleteRequest.getParameterList(), updateDeleteRequest.getEmpGroupInstanceCode(),
							updateDeleteRequest.getParamValueMap());
					if (responseData.getRespMsg().equals(Constants.PASS_MSG_GROUPPARAMVALIDATED)) {
						logger.info("Group parameter validation completed");
					} else if (responseData.getRespMsg().equals(Constants.FAILURE_MSG_DUPLICATEGROUPPARAM)) {
						logger.error("Another group with same values already exist: " + responseData.getRespData());
						throw new Exception(
								"Another group with same values already exist: " + responseData.getRespData());
					} else {
						logger.info("Group parameter validation failed");
						throw new Exception(response);
					}
				}
				logger.info("Group validated. Saving group");
				processData.setType(EnumRequestType.INSTANCE);
				KernelSingleAttributeValueObject att2 = new KernelSingleAttributeValueObject();
				String val2 = mapper.writeValueAsString(updateDeleteRequest.getParameterList());
				att2.setValue(val2);
				attr.put(attributes.get(Constants.EMPLOYEE_GROUP_PARAM), att2);
				KernelSingleAttributeValueObject att3 = new KernelSingleAttributeValueObject();
				String val3 = mapper.writeValueAsString(updateDeleteRequest.getParamValueMap());
				att3.setValue(val3);
				attr.put(attributes.get(Constants.EMPLOYEE_GROUP_VALUE), att3);
				processData.setAttributes(attr);
			}
			processData.setCas(updateDeleteRequest.getCas());
			processData.setClassCode(updateDeleteRequest.getOrgEmployeeGroupClassCode());
			processData.setInstanceCode(updateDeleteRequest.getEmpGroupInstanceCode());
			requestData.setInstanceData(processData);
			List<KernelInstanceProcessRequest> requestDatalist = new ArrayList<>();
			requestDatalist.add(requestData);
			response = util.saveInstanceForGivenClass(updateDeleteRequest.getProtocol(), requestDatalist);
			JSONObject obj = new JSONObject(response);
			if (obj.get("responseCode").equals("Success-100")) {
				response = util.setResponse(true, Constants.RESPONSE_GROUP_UPDATE_PASS, jwt);
			} else {
				response = util.setResponse(false, (String) obj.get("errMsg"), null);
			}
		} catch (Exception e) {
			logger.error("Error:", e);
			response = util.setResponse(false, Constants.RESPONSE_UNEXPECTED_FAIL, null);
		}
		return response;
	}

	public String getPolicy(String requestData) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String jwt = null;
		DataReadResponse response = null;
		String responseString = null;
		GetPolicyListRequest policyListRequest = new GetPolicyListRequest();
		try {
			policyListRequest = mapper.readValue(requestData, GetPolicyListRequest.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseString = util.setResponse(false, Constants.FAILURE_MSG_BADREQUEST, null);
			return responseString;
		}
		try {
			logger.info("Fetching policyies");
			String docId = util.getDocIdForClass(policyListRequest.getProtocol(), "Policy");
			// create query to fetch data
			List<ProjectionAttribute> projectionList = new ArrayList<>();
			ProjectionAttribute projectionAttribute = new ProjectionAttribute();

			List<AttributesData> attrList = new ArrayList<>();
			AttributesData attributesData1 = new AttributesData();
			attributesData1.setAttribute("id");
			attributesData1.setReference(false);
			AttributesData attributesData2 = new AttributesData();
			attributesData2.setAttribute("attributes.policyName");
			attributesData2.setReference(false);
			AttributesData attributesData3 = new AttributesData();
			attributesData3.setAttribute("status");
			attributesData3.setReference(false);
			attrList.add(attributesData1);
			attrList.add(attributesData2);
			attrList.add(attributesData3);

			projectionAttribute.setDbAttribute(attrList);
			projectionList.add(projectionAttribute);

			List<DBFilters> filterList = new ArrayList<>();
			DBFilters dBFilters = new DBFilters();
			dBFilters.setAttribute("classCode");
			dBFilters.setCondition("=");
			dBFilters.setValue(docId);
			DBFilters dBFilter2 = new DBFilters();
			dBFilter2.setAttribute("type");
			dBFilter2.setCondition("=");
			dBFilter2.setValue("INSTANCE");
			filterList.add(dBFilter2);
			filterList.add(dBFilters);

			List<String> filterJoin = new ArrayList<>();
			filterJoin.add("AND");
			response = util.getQueryData(policyListRequest.getProtocol(), projectionAttribute, filterList, filterJoin,
					policyListRequest.getIndexingData(), true);
			logger.info("Data fetched !!");
			if (response.getResponse().isEmpty()) {
				throw new Exception("Data read failed");
			}
			responseString = util.setResponse(true, response.getResponse(), jwt);
		} catch (Exception e) {
			logger.error("Error:", e);
			responseString = util.setResponse(false, Constants.RESPONSE_UNEXPECTED_FAIL, null);
		}
		return responseString;
	}

}
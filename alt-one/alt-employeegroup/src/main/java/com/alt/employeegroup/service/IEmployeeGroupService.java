package com.alt.employeegroup.service;

import java.io.IOException;


import org.springframework.web.bind.annotation.RequestBody;

import com.alt.datacarrier.employeegroup.common.ReadEmployeeGroupDataRequest;

public interface IEmployeeGroupService {

	public String getParameters(@RequestBody String requestData) throws IOException;

	public String createNewGroup(@RequestBody String request) throws IOException;

	public String getGroupData(@RequestBody String groupReadRequest) throws IOException;

	public String updateDeleteGroup(@RequestBody String request) throws IOException;

}

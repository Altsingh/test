package com.alt.employeegroup.service;

import java.io.IOException;


public interface IPolicyService {
	public String crudPolicy(String request) throws IOException;

	public String getPolicyCreateData(String request) throws IOException;

	public String getGroupEmployees(String request) throws IOException;

	public String tagPolicy(String request) throws IOException;
}

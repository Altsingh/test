package com.alt.employeegroup.service;

import com.alt.core.util.JwtUtil;
import com.alt.datacarrier.business.superadmin.DataReadConstants;
import com.alt.datacarrier.business.superadmin.PolicyCreateDataRequest;
import com.alt.datacarrier.business.superadmin.PolicyTagRequest;
import com.alt.datacarrier.core.InstanceData;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.Request;
import com.alt.datacarrier.employeegroup.common.CrudPolicyRequest;
import com.alt.datacarrier.employeegroup.common.EmpGroupResponse;
import com.alt.datacarrier.employeegroup.common.GetGroupEmployeesRequest;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.alt.datacarrier.kernel.common.KernelInstanceProcessData;
import com.alt.datacarrier.kernel.common.KernelInstanceProcessRequest;
import com.alt.datacarrier.kernel.common.KernelSingleAttributeValueObject;
import com.alt.employeegroup.constants.Constants;
import com.alt.employeegroup.controller.EmployeeGroupController;
import com.alt.employeegroup.util.DbUtil;
import com.alt.employeegroup.util.Util;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.*;


public class PolicyServiceImpl implements IPolicyService {

	@Autowired(required = true)
	Util util;

	@Autowired(required = true)
	DbUtil httputil;

	@Autowired(required = true)
	ReadService readService;

	@Autowired(required = true)
	WriteService writeService;

	@Autowired(required = true)
	IEmployeeGroupService employeeGroupService;

	private static final Logger logger = Logger.getLogger(EmployeeGroupController.class);

	EmpGroupResponse finalResponse = new EmpGroupResponse();

	JwtUtil jwtUtil = new JwtUtil();

	@Override
	public String tagPolicy(String requestData) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String responseString = null;
		String response = null;
		PolicyTagRequest policyTagRequest = new PolicyTagRequest();
		Protocol protocol = new Protocol();
		Request<KernelInstanceProcessRequest> kernelRequest = new Request<>();
		List<KernelInstanceProcessRequest> requestList = new ArrayList<>();
		kernelRequest.setProtocol(policyTagRequest.getProtocol());
		KernelInstanceProcessRequest instanceProcessRequest = new KernelInstanceProcessRequest();
		KernelInstanceProcessData instanceData = new KernelInstanceProcessData();
		KernelSingleAttributeValueObject policyName = new KernelSingleAttributeValueObject();
		KernelSingleAttributeValueObject policy = new KernelSingleAttributeValueObject();
		KernelSingleAttributeValueObject empGroup = new KernelSingleAttributeValueObject();
		KernelSingleAttributeValueObject entity = new KernelSingleAttributeValueObject();
		KernelSingleAttributeValueObject packageName = new KernelSingleAttributeValueObject();
		KernelSingleAttributeValueObject org = new KernelSingleAttributeValueObject();
		KernelSingleAttributeValueObject tenant = new KernelSingleAttributeValueObject();
		Map<String, KernelSingleAttributeValueObject> attributes = new HashMap<>();
		try {
			policyTagRequest = mapper.readValue(requestData, PolicyTagRequest.class);
			protocol = policyTagRequest.getProtocol();
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseString = util.setResponse(false, Constants.FAILURE_MSG_BADREQUEST, null);
			return responseString;
		}
		try {
			String methodResponse = validateGroupParameterValues(protocol, policyTagRequest.getEmpGroupId());
			if (!methodResponse.equals("VALIDATION_SUCCESS")) {
				logger.info("parameter group validation succeded");
			} else {
				throw new Exception(methodResponse);
			}

			String docId = util.getDocIdForClass(policyTagRequest.getProtocol(), "GroupPolicy");
			instanceData.setClassCode(docId);
			instanceData.setType(EnumRequestType.INSTANCE);

			instanceProcessRequest.setAction(EnumCRUDOperations.CREATE);
			instanceData.setStatus(EnumDocStatusType.ACTIVE);
			instanceData.setCreatedBy(policyTagRequest.getProtocol().getUserCode());
			instanceData.setCreatedDate(System.currentTimeMillis());
			instanceData.setReadLock(false);
			policyName.setValue(policyTagRequest.getGroupPolicyName());
			policy.setValue(policyTagRequest.getPolicyId());
			entity.setValue(policyTagRequest.getEntityId());
			packageName.setValue(policyTagRequest.getPackageId());
			empGroup.setValue(mapper.writeValueAsString(policyTagRequest.getEmpGroupId()));
			org.setValue("org-system#class-4#instance-1");
			tenant.setValue("org-system#class-3#instance-1");
			attributes.put("groupPolicyName", policyName);
			attributes.put("policy", policy);
			attributes.put("employeeGroup", empGroup);
			attributes.put("entity", entity);
			attributes.put("package", packageName);
			attributes.put("organization", org);
			attributes.put("tenant", tenant);
			instanceData.setAttributes(attributes);
			instanceProcessRequest.setInstanceData(instanceData);
			requestList.add(instanceProcessRequest);
			response = util.saveInstanceForGivenClass(policyTagRequest.getProtocol(), requestList);
			responseString = util.setResponse(true, response, null);
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseString = util.setResponse(false, "UNEXPECTED ERROR OCCOURED", null);
		}
		return responseString;
	}

	@Override
	public String getGroupEmployees(String request) throws IOException {
		String responseString = null;
		ObjectMapper mapper = new ObjectMapper();
		List<String> employeeId = new ArrayList<>();
		try {
			GetGroupEmployeesRequest getGroupEmployeesRequest = mapper.readValue(request,
					GetGroupEmployeesRequest.class);
			employeeId = getGroupEmployeesList(getGroupEmployeesRequest.getProtocol(),
					getGroupEmployeesRequest.getEmpGroupId());
			if (employeeId.isEmpty()) {
				throw new Exception("employee list fetch failed");
			}
			responseString = util.setResponse(true, employeeId, null);
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseString = util.setResponse(false, "UNEXPECTED ERROR OCCOURED", null);
		}
		return responseString;
	}

	@Override
	public String getPolicyCreateData(String requestData) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String jwt = null;
		List<Map<String, Object>> response = null;
		String responseString = null;
		PolicyCreateDataRequest createDataRequest = new PolicyCreateDataRequest();
		try {
			createDataRequest = mapper.readValue(requestData, PolicyCreateDataRequest.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseString = util.setResponse(false, Constants.FAILURE_MSG_BADREQUEST, null);
			return responseString;
		}
		try {
			String docId = util.getDocIdForClass(createDataRequest.getProtocol(),
					createDataRequest.getClassName().getName());
			String attrReadEnum = createDataRequest.getClassName() + "_DATAREAD_COUNT";
			int numOfAttrToRead = (int) DataReadConstants.getCount(attrReadEnum);
			List<String> attrNameList = new ArrayList<>();

			for (int count = 1; count <= numOfAttrToRead; count++) {
				String attrName = (String) DataReadConstants
						.getCount(createDataRequest.getClassName() + "_ATTR" + count);
				attrNameList.add(attrName);
			}
			// create query to fetch data
			response = util.getInstanceDataForGivenAttr(createDataRequest.getProtocol(), docId,
					createDataRequest.getClassName().toString(), attrNameList);
			logger.info("Data fetched !!");
			responseString = util.setResponse(true, mapper.writeValueAsString(response), null);
		} catch (Exception e) {
			e.printStackTrace();
			responseString = util.setResponse(false, "UNEXPECTED ERROR OCCUREDs", null);
		}
		return responseString;
	}

	@Override
	public String crudPolicy(String requestData) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String responseString = null;
		String response = null;
		CrudPolicyRequest crudPolicyRequest = new CrudPolicyRequest();
		try {
			crudPolicyRequest = mapper.readValue(requestData, CrudPolicyRequest.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseString = util.setResponse(false, Constants.FAILURE_MSG_BADREQUEST, null);
			return responseString;
		}
		try {
			String policyDocId = util.getDocIdForClass(crudPolicyRequest.getProtocol(), "Policy");
			Request<KernelInstanceProcessRequest> kernelRequest = new Request<>();
			List<KernelInstanceProcessRequest> requestList = new ArrayList<>();
			kernelRequest.setProtocol(crudPolicyRequest.getProtocol());
			KernelInstanceProcessRequest instanceProcessRequest = new KernelInstanceProcessRequest();
			KernelInstanceProcessData instanceData = new KernelInstanceProcessData();
			instanceData.setClassCode(policyDocId);
			instanceData.setType(EnumRequestType.INSTANCE);
			KernelSingleAttributeValueObject policyName = new KernelSingleAttributeValueObject();
			KernelSingleAttributeValueObject module = new KernelSingleAttributeValueObject();
			KernelSingleAttributeValueObject entity = new KernelSingleAttributeValueObject();
			Map<String, KernelSingleAttributeValueObject> attributes = new HashMap<>();
			switch (crudPolicyRequest.getType()) {
			case INSERT:
				instanceProcessRequest.setAction(EnumCRUDOperations.CREATE);
				if (crudPolicyRequest.isStatus()) {
					instanceData.setStatus(EnumDocStatusType.ACTIVE);
				} else {
					instanceData.setStatus(EnumDocStatusType.INACTIVE);
				}
				instanceData.setCreatedBy(crudPolicyRequest.getProtocol().getUserCode());
				instanceData.setCreatedDate(System.currentTimeMillis());
				instanceData.setReadLock(false);
				policyName.setValue(crudPolicyRequest.getPolicyName());
				module.setValue(crudPolicyRequest.getModule());
				entity.setValue(crudPolicyRequest.getEntity());
				attributes.put("policyName", policyName);
				attributes.put("module", module);
				attributes.put("entity", entity);
				instanceData.setAttributes(attributes);
				break;
			case UPDATE:
				instanceProcessRequest.setAction(EnumCRUDOperations.UPDATE);
				if (crudPolicyRequest.isStatus()) {
					instanceData.setStatus(EnumDocStatusType.ACTIVE);
				} else {
					instanceData.setStatus(EnumDocStatusType.INACTIVE);
				}
				instanceData.setInstanceCode(crudPolicyRequest.getPolicyCode());
				instanceData.setCas(crudPolicyRequest.getCas());
				instanceData.setModifiedBy(crudPolicyRequest.getProtocol().getUserCode());
				instanceData.setModifiedDate(System.currentTimeMillis());
				policyName.setValue(crudPolicyRequest.getPolicyName());
				module.setValue(crudPolicyRequest.getModule());
				entity.setValue(crudPolicyRequest.getEntity());
				attributes.put("policyName", policyName);
				attributes.put("module", module);
				attributes.put("entity", entity);
				instanceData.setAttributes(attributes);
				break;
			case DELETE:
				instanceProcessRequest.setAction(EnumCRUDOperations.SOFT_DELETE);
				instanceData.setInstanceCode(crudPolicyRequest.getPolicyCode());
				instanceData.setCas(crudPolicyRequest.getCas());
				instanceData.setModifiedBy(crudPolicyRequest.getProtocol().getUserCode());
				instanceData.setModifiedDate(System.currentTimeMillis());
				break;
			}
			instanceProcessRequest.setInstanceData(instanceData);
			requestList.add(instanceProcessRequest);
			response = util.saveInstanceForGivenClass(crudPolicyRequest.getProtocol(), requestList);
			responseString = util.setResponse(true, response, null);
		} catch (Exception e) {
			logger.error(e.getMessage());
			responseString = util.setResponse(false, Constants.RESPONSE_UNEXPECTED_FAIL, null);
		}
		return responseString;
	}

	/**
	 * @param protocol
	 * @param empGroupId
	 * @return
	 */
	private String validateEmployeeList(Protocol protocol, List<String> empGroupIdList) {
		List<String> employeeList = new ArrayList<>();
		Set<String> set = new HashSet();
		try {
			for (String empGroupId : empGroupIdList) {
				employeeList = getGroupEmployeesList(protocol, empGroupId);
				if (employeeList == null) {
					return Constants.GROUPEMPLOYEE_FETCH_FAILED;
				}
				if (employeeList.isEmpty()) {
					logger.info("no employees present in the  given group");
				}
				for (String employee : employeeList) {
					if (!set.add(employee)) {
						logger.error("employee overlapping occoured for " + empGroupId);
						return Constants.EMPLOYEE_OVERLAP_ERROR;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return Constants.GROUPEMPLOYEE_FETCH_FAILED;
		}
		return Constants.PASS_SUCCESS;
	}

	public String validateGroupParameterValues(Protocol protocol, List<String> empGroupId) {
		ObjectMapper mapper = new ObjectMapper();
		JsonFactory f = new JsonFactory();
		TypeReference<ArrayList<String>> typeList = new TypeReference<ArrayList<String>>() {
		};
		TypeReference<HashMap<String, ArrayList<String>>> typeMap = new TypeReference<HashMap<String, ArrayList<String>>>() {
		};
		Map<String, ArrayList<String>> finalParamValues = new HashMap<>();
		Map<String, ArrayList<String>> valueMap = new HashMap<>();
		try {
			for (String groupId : empGroupId) {
				Map<String, Object> group = util.readGroupData(protocol, groupId);
				@SuppressWarnings("unchecked")
				Map<String, Map<String, String>> attributes = (Map<String, Map<String, String>>) group
						.get("attributes");
				String parameterListString = attributes.get("ParameterList").get(Constants.ATTRIBUTE_VALUE);
				JsonParser jpList = f.createJsonParser(parameterListString);
				List<String> paramList = mapper.readValue(jpList, typeList);
				String parameterMapString = attributes.get("ParameterValue").get(Constants.ATTRIBUTE_VALUE);
				JsonParser jpMap = f.createJsonParser(parameterMapString);
				valueMap = mapper.readValue(jpMap, typeMap);
				if (!finalParamValues.isEmpty()) {
					if (!compareParameterValues(finalParamValues, valueMap, paramList)) {
						return "PARAMETER_VALUES_OVERLAPP";
					}
				} else {
					finalParamValues = valueMap;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;// return empty list
		}
		return "VALIDATION_SUCCESS";
	}

	public boolean compareParameterValues(Map<String, ArrayList<String>> finalGroup,
			Map<String, ArrayList<String>> group, List<String> paramList) {
		for (String paramId : paramList) {
			// Check if groups share common parameters
			if (finalGroup.containsKey(paramId)) {
				// Check if groups share even one same parameter
				// value
				ArrayList<String> finalParamValueList = finalGroup.get(paramId);
				ArrayList<String> paramValueList = group.get(paramId);
				for (String value : paramValueList) {
					if (finalParamValueList.contains(value)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public List<String> getGroupEmployeesList(Protocol protocol, String empGroupId) {
		ObjectMapper mapper = new ObjectMapper();
		JsonFactory f = new JsonFactory();
		List<InstanceData> employees = new ArrayList<>();
		TypeReference<ArrayList<String>> typeList = new TypeReference<ArrayList<String>>() {
		};
		TypeReference<HashMap<String, ArrayList<String>>> typeMap = new TypeReference<HashMap<String, ArrayList<String>>>() {
		};
		List<String> employeeId = new ArrayList<>();
		try {
			Map<String, Object> group = util.readGroupData(protocol, empGroupId);
			Map<String, Map<String, String>> attributes = (Map<String, Map<String, String>>) group.get("attributes");
			// String groupName = attributes.get("GroupName").get("value");
			String parameterListString = attributes.get("ParameterList").get(Constants.ATTRIBUTE_VALUE);
			String parameterMapString = attributes.get("ParameterValue").get(Constants.ATTRIBUTE_VALUE);
			JsonParser jpList = f.createJsonParser(parameterListString);
			List<String> paramList = mapper.readValue(jpList, typeList);
			JsonParser jpMap = f.createJsonParser(parameterMapString);
			Map<String, ArrayList<String>> valueMap = mapper.readValue(jpMap, typeMap);
			employees = util.getEmployeeIdListForEmployeeGroup(protocol, paramList, valueMap);
			if (employees.isEmpty()) {
				logger.info("no employees tagged in the given group");
				return new ArrayList<>();// return empty list
			}
			for (InstanceData employee : employees) {
				employeeId.add(employee.getDocIdentifier());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;// return empty list
		}
		return employeeId;
	}

}

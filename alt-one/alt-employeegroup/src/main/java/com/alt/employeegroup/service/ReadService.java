package com.alt.employeegroup.service;

import com.alt.datacarrier.business.common.AltReadRequest;
import com.alt.datacarrier.business.common.AltReadResponse;
import com.alt.datacarrier.core.ClassMeta;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.Request;
import com.alt.datacarrier.core.Response;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.alt.employeegroup.constants.Constants;
import com.alt.employeegroup.controller.EmployeeGroupController;
import com.alt.employeegroup.util.DbUtil;
import com.alt.employeegroup.util.Util;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service
public class ReadService {

	private static final Logger logger = Logger.getLogger(EmployeeGroupController.class);

	@Autowired(required = true)
	Util util;

	@Value("${kernel.url}")
	String kernelReadUrl;

	@Autowired(required = true)
	DbUtil HttpUtil;

	public Map<String, String> getEmpGroupAttributes(Protocol protocol, String groupClassCode) {

		String groupNameKey = null;
		String paramGroupKey = null;
		String valueMapKey = null;
		ClassMeta response = null;

		Map<String, String> groupData = new HashMap<String, String>();
		AltReadRequest req = new AltReadRequest();
		Request<AltReadRequest> readRequest = new Request<AltReadRequest>();
		List<AltReadRequest> reqList = new ArrayList<AltReadRequest>();
		try {
			logger.info("Fetching group meta from db for");
			readRequest.setProtocol(protocol);
			req.setDocIdentifier(groupClassCode);
			req.setDepth(10);
			req.setLimit(10);
			req.setOffset(0);
			req.setType(EnumRequestType.CLASS);
			reqList.add(req);
			readRequest.setRequestData(reqList);
			Response<AltReadResponse> altResponse = HttpUtil.commonReadRequest(readRequest);
			if ("SUCCESS".equalsIgnoreCase(altResponse.getResponseStatus())) {
				response = altResponse.getResponseData().getMeta().get(groupClassCode);
			} else {
				throw new Exception("Error reading data");
			}

			logger.info("Fetching employee group attribute keys");
			groupNameKey = response.getAttributes().stream()
					.filter(attr -> attr.getAttributeName().equalsIgnoreCase(Constants.EMPLOYEE_GROUP_NAME)).findAny()
					.orElse(null).getAttributeCode();
			valueMapKey = response.getAttributes().stream()
					.filter(attr -> attr.getAttributeName().equalsIgnoreCase(Constants.EMPLOYEE_GROUP_VALUE)).findAny()
					.orElse(null).getAttributeCode();
			paramGroupKey = response.getAttributes().stream()
					.filter(attr -> attr.getAttributeName().equalsIgnoreCase(Constants.EMPLOYEE_GROUP_PARAM)).findAny()
					.orElse(null).getAttributeCode();

			groupData.put(Constants.EMPLOYEE_GROUP_NAME, groupNameKey);
			groupData.put(Constants.EMPLOYEE_GROUP_VALUE, valueMapKey);
			groupData.put(Constants.EMPLOYEE_GROUP_PARAM, paramGroupKey);
		} catch (Exception e) {
			logger.error("Error occured:", e);
		}
		return groupData;
	}
}

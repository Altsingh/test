package com.alt.employeegroup.service;

import com.alt.datacarrier.business.common.AltReadRequest;
import com.alt.datacarrier.business.common.AltReadResponse;
import com.alt.datacarrier.core.InstanceData;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.Request;
import com.alt.datacarrier.core.Response;
import com.alt.datacarrier.employeegroup.common.EmpGroupResponse;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.alt.employeegroup.constants.Constants;
import com.alt.employeegroup.controller.EmployeeGroupController;
import com.alt.employeegroup.util.DbUtil;
import com.alt.employeegroup.util.Util;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service
public class WriteService {

	Protocol protocol;

	Map<String, String> attributes;

	@Autowired(required = true)
	Util util;

	@Autowired(required = true)
	DbUtil HttpUtil;

	@Value("${kernel.url}")
	String kernelReadUrl;

	List<InstanceData> orgGroupList = null;
	JSONArray jsonArr = new JSONArray();

	private static final Logger logger = Logger.getLogger(EmployeeGroupController.class);

	// pass employeeGroupInstanceCode = null for creation of employee group, and
	// instanceCode for updation
	public String validateGroupName(String groupName, String employeeGroupInstanceCode, String groupClassCode) {
		AltReadRequest req = new AltReadRequest();
		Request<AltReadRequest> readRequest = new Request<>();
		List<AltReadRequest> reqList = new ArrayList<>();
		String resp = null;
		logger.info("Reading employee groups from db");
		try {
			if (groupName.isEmpty() || groupName.equals(" ")) {
				return Constants.FAILURE_MSG_EMPTYGROUPNAME;
			}
			if (!util.isAlphanumeric(groupName)) {
				return Constants.FAILURE_MSG_ALPGANUMERIC;
			}
			readRequest.setProtocol(protocol);
			req.setDocIdentifier(groupClassCode);
			req.setDepth(10);
			req.setLimit(10);
			req.setOffset(0);
			req.setType(EnumRequestType.INSTANCE);
			reqList.add(req);
			readRequest.setRequestData(reqList);
			Response<AltReadResponse> kernelResponse = HttpUtil.commonReadRequest(readRequest);
			orgGroupList = kernelResponse.getResponseData().getInstance().get(0);
			for (InstanceData dbGroup : orgGroupList) {
				String dbGroupName = dbGroup.getAttributes().get(Constants.EMPLOYEE_GROUP_NAME).getValue();
				if (employeeGroupInstanceCode != null) {
					// update employee group
					String dbEmployeeGroupInstanceCode = dbGroup.getDocIdentifier();
					if (groupName.equalsIgnoreCase(dbGroupName)
							&& !employeeGroupInstanceCode.equals(dbEmployeeGroupInstanceCode)) {
						return Constants.FAILURE_MSG_DUPLICATEGROUPNAME;
					}
				} else {
					// create employee group
					if (groupName.equalsIgnoreCase(dbGroupName)) {
						return Constants.FAILURE_MSG_DUPLICATEGROUPNAME;
					}
				}
			}
			logger.info("Group name validated");
			resp = Constants.PASS_MSG_GROUPNAMEVALIDATED;
		} catch (Exception e) {
			e.printStackTrace();
			resp = e.getMessage();
			logger.error(resp);
		}
		return resp;
	}

	public EmpGroupResponse validateGroupParameter(List<String> newparamList, String employeeGroupInstanceCode,
			Map<String, List<String>> newparamValueList) {
		EmpGroupResponse response = new EmpGroupResponse();
		ObjectMapper mapper = new ObjectMapper();
		String chkInactiveClass = null;
		JsonFactory f = new JsonFactory();
		try {
			if (newparamList.isEmpty() || newparamValueList.isEmpty()) {
				response.setData(null, Constants.FAILURE_MSG_EMPTYGROUPPARAM, null, null);
				return response;
			}
			chkInactiveClass = util.checkIfActive(newparamList);
			if (chkInactiveClass.equals(Constants.RESPONSE_FAIL)) {
				throw new Exception("Some error occoured. please check logs for more details");
			} else if (chkInactiveClass.equals(Constants.RESPONSE_SUCCESS)) {
				logger.info("All classes are active");
			} else {
				throw new Exception(chkInactiveClass + " in no more active");
			}
			chkInactiveClass = null;
			// check if any of the parameter value class is inactive
			for (String parameter : newparamList) {
				chkInactiveClass = util.checkIfActive(newparamValueList.get(parameter));
				if (chkInactiveClass.equals(Constants.RESPONSE_FAIL)) {
					throw new Exception("Some error occoured. please check logs for more details");
				} else if (chkInactiveClass.equals(Constants.RESPONSE_SUCCESS)) {
					logger.info("All classes are active");
				} else {
					throw new Exception(chkInactiveClass + " in no more active");
				}
				chkInactiveClass = null;
			}
			for(InstanceData group: orgGroupList){
				String groupName = group.getAttributes().get(Constants.EMPLOYEE_GROUP_NAME).getValue();
				TypeReference<ArrayList<String>> typeList = new TypeReference<ArrayList<String>>() {
				};
				String paramAsString = group.getAttributes().get(Constants.EMPLOYEE_GROUP_PARAM).getValue();
				JsonParser jpList = f.createJsonParser(paramAsString);
				List<String> paramList = mapper.readValue(jpList, typeList);
				TypeReference<HashMap<String, ArrayList<String>>> typeMap = new TypeReference<HashMap<String, ArrayList<String>>>() {
				};
				String paramValueMapString = group.getAttributes().get(Constants.EMPLOYEE_GROUP_VALUE).getValue();
				JsonParser jpMap = f.createJsonParser(paramValueMapString);
				Map<String, ArrayList<String>> valueMap = mapper.readValue(jpMap, typeMap);
				logger.info("Compare parameter list");
				if (compareLists(paramList, newparamList)) {
					logger.info("Compare parameter values");
					int sameParamValueCount = 0;
					String dbEmployeeGroupInstanceCode = group.getDocIdentifier();
					for (String param : paramList) {
						if (compareLists(valueMap.get(param), newparamValueList.get(param))) {
							sameParamValueCount++;
						}
					}
					if (sameParamValueCount == paramList.size()) {
						if ((employeeGroupInstanceCode != null)
								&& !employeeGroupInstanceCode.equals(dbEmployeeGroupInstanceCode)) {
							response.setData(null, Constants.FAILURE_MSG_DUPLICATEGROUPPARAM, groupName, null);
							return response;
						} else if (employeeGroupInstanceCode == null) {
							response.setData(null, Constants.FAILURE_MSG_DUPLICATEGROUPPARAM, groupName, null);
							return response;
						}
					}
				}
			}
			response.setData(null, Constants.PASS_MSG_GROUPPARAMVALIDATED, null, null);
		} catch (Exception e) {
			logger.error("Error:", e);
			response.setData(null, e.getMessage(), null, null);
		}
		return response;
	}

	public boolean compareLists(List<String> list1, List<String> list2) {
		if (list1.size() == list2.size()) {
			list1.retainAll(list2);
			if (list1.size() == list2.size()) {
				return true;
			}
		}
		return false;
	}

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

}

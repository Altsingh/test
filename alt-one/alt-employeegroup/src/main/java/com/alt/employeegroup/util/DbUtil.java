package com.alt.employeegroup.util;

import com.alt.core.util.CommonUtil;
import com.alt.datacarrier.business.common.AltReadRequest;
import com.alt.datacarrier.business.common.AltReadResponse;
import com.alt.datacarrier.core.Request;
import com.alt.datacarrier.core.Response;
import com.alt.datacarrier.kernel.common.KernelCRUDResponse;
import com.alt.datacarrier.kernel.common.KernelSingleReadRequest;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DbUtil {
	@Value("${kernel.url}")
	String kernelReadUrl;

	private static final Logger logger = Logger.getLogger(Util.class);
		
	public Response<AltReadResponse> commonReadRequest(Request<AltReadRequest> altRequestData) {
		String jsonInString = null;
		Request<KernelSingleReadRequest> requestList = new Request<>();
		List<KernelSingleReadRequest> requestData = new ArrayList<>();
		for (AltReadRequest altReadRequest : altRequestData.getRequestData()) {
			KernelSingleReadRequest readRequest = new KernelSingleReadRequest();
			readRequest.setDepth(altReadRequest.getDepth());
			readRequest.setDocIdentifier(altReadRequest.getDocIdentifier());
			readRequest.setLimit(altReadRequest.getLimit());
			readRequest.setOffset(altReadRequest.getOffset());
			readRequest.setType(altReadRequest.getType());
			requestData.add(readRequest);
		}
		requestList.setRequestData(requestData);
		requestList.setProtocol(altRequestData.getProtocol());
		try {
			jsonInString = new ObjectMapper().writeValueAsString(requestList);
		} catch (JsonGenerationException e) {
			logger.error("Error occured:", e);
		} catch (JsonMappingException e) {
			logger.error("Error occured:", e);
		} catch (IOException e) {
			logger.error("Error occured:", e);
		}

		String responseString = CommonUtil.getDataFromUrl(kernelReadUrl, jsonInString).toBlocking().first();
		Response<KernelCRUDResponse> responseKernel;
		try {
			responseKernel = new ObjectMapper().readValue(responseString, new TypeReference<Response<KernelCRUDResponse>>(){});
			Response<AltReadResponse> businessResponse = new Response<>();
			businessResponse.setResponseCode(responseKernel.getResponseCode());
			businessResponse.setErrMsg(responseKernel.getErrMsg());
			businessResponse.setResponseStatus(responseKernel.getResponseStatus());
			businessResponse.setResponseData(new AltReadResponse());
			businessResponse.getResponseData().setInstance(responseKernel.getResponseData().getInstance());
			businessResponse.getResponseData().setMeta(responseKernel.getResponseData().getMeta());
			return businessResponse;
		} catch (JsonParseException e) {
			logger.error("Error occured:", e);
		} catch (JsonMappingException e) {
			logger.error("Error occured:", e);
		} catch (IOException e) {
			logger.error("Error occured:", e);
		}

		return null;
	}

}

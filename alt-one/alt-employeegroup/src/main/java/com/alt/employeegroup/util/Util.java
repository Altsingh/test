package com.alt.employeegroup.util;

import com.alt.core.util.CommonUtil;
import com.alt.datacarrier.business.common.AltReadRequest;
import com.alt.datacarrier.business.common.AltReadResponse;
import com.alt.datacarrier.common.enumeration.EnumDataType;
import com.alt.datacarrier.core.*;
import com.alt.datacarrier.employeegroup.common.DataReadResponse;
import com.alt.datacarrier.employeegroup.common.EmpGroupResponse;
import com.alt.datacarrier.kernel.common.*;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.alt.employeegroup.constants.Constants;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



@Service
public class Util {

	@Autowired(required = true)
	DbUtil httpUtil;

	@Value("${kernel.writeinstance.url}")
	String kernelInstanceWriteURL;

	@Value("${kernel.dynamic.query.url}")
	String kernelQueryURL;

	private static final Logger logger = Logger.getLogger(Util.class);

	/* Converts kernel response to a more readable format */
	public Response<AltReadResponse> convertKernelResponseToAltResponse(String responseString) {
		Response<KernelCRUDResponse> responseKernel;
		try {
			responseKernel = new ObjectMapper().readValue(responseString,
					new TypeReference<Response<KernelCRUDResponse>>() {
					});
			Response<AltReadResponse> businessResponse = new Response<>();
			businessResponse.setResponseCode(responseKernel.getResponseCode());
			businessResponse.setErrMsg(responseKernel.getErrMsg());
			businessResponse.setResponseStatus(responseKernel.getResponseStatus());
			businessResponse.setResponseData(new AltReadResponse());
			businessResponse.getResponseData().setInstance(responseKernel.getResponseData().getInstance());
			businessResponse.getResponseData().setMeta(responseKernel.getResponseData().getMeta());
			return businessResponse;
		} catch (IOException e) {
			logger.error("Error occured:", e);
		}

		return null;
	}

	/*
	 * creates custom queries to read data from data kernel. works similar to
	 * sql join query
	 */
	public DataReadResponse getQueryData(Protocol protocol, ProjectionAttribute projectionAttribute,
			List<DBFilters> filterList, List<String> filterJoin, IndexingData indexingData, boolean docCountNeeded)
			throws IOException {
		Request<KernelDynamicQueryReadRequest> request = new Request<>();
		ObjectMapper mapper = new ObjectMapper();
		String response = null;
		DataReadResponse readResponse = new DataReadResponse();
		KernelDynamicQueryReadRequest kernelDynamicQueryReadRequest = new KernelDynamicQueryReadRequest();
		List<ProjectionAttribute> projectionList = new ArrayList<>();
		projectionList.add(projectionAttribute);

		kernelDynamicQueryReadRequest.setFilterJoinOperator(filterJoin);
		kernelDynamicQueryReadRequest.setProjection(projectionList);
		kernelDynamicQueryReadRequest.setDbFilters(filterList);

		request.setProtocol(protocol);
		if (indexingData.getOrderBy() != null) {
			OrderAttribute orderAttribute1 = new OrderAttribute();
			orderAttribute1.setDbAttr(indexingData.getOrderBy());
			orderAttribute1.setSortDirection(indexingData.getOrder());
			List<OrderAttribute> orderList = new ArrayList<>();
			orderList.add(orderAttribute1);
			kernelDynamicQueryReadRequest.setOrderBy(orderList);
		}
		if (indexingData.getOffset() != null) {
			kernelDynamicQueryReadRequest.setOffset(indexingData.getOffset());
		}
		if (indexingData.getLimit() != null) {
			kernelDynamicQueryReadRequest.setLimit(indexingData.getLimit());
		}
		kernelDynamicQueryReadRequest.setDocumentCountNeeded(docCountNeeded);
		List<KernelDynamicQueryReadRequest> dynamicQueryRequestList = new ArrayList<>();
		dynamicQueryRequestList.add(kernelDynamicQueryReadRequest);
		request.setRequestData(dynamicQueryRequestList);

		response = CommonUtil.getDataFromUrl(kernelQueryURL, new ObjectMapper().writeValueAsString(request))
				.toBlocking().first();
		try {
			KernelReadDynamicQueryResponse queryResponse = mapper.readValue(response,
					KernelReadDynamicQueryResponse.class);
			if (queryResponse.getQueryResponse().get(0).getCode() == 200) {
				// List<Map<String, Object>> responseData =
				// queryResponse.getQueryResponse().get(0).getResponse();

				readResponse.setResponse(queryResponse.getQueryResponse().get(0).getResponse());
				readResponse.setDoccumentCount(queryResponse.getQueryResponse().get(0).getDocumentCount());
				response = mapper.writeValueAsString(readResponse);

			} else {
				throw new Exception("error occoured");
			}
		} catch (Exception e) {
		}
		return readResponse;
	}

	/*
	 * each db class has display units i.e. default display parameters of the
	 * class. It fetches the same
	 */
	@SuppressWarnings("unchecked")
	public String getDisplayUnit(Protocol protocol, String classCode) throws IOException {
		String attributeName = null;
		ObjectMapper mapper = new ObjectMapper();
		String response = null;

		Request<KernelDynamicQueryReadRequest> request = new Request<>();

		KernelDynamicQueryReadRequest kernelDynamicQueryReadRequest = new KernelDynamicQueryReadRequest();
		List<ProjectionAttribute> projectionList = new ArrayList<>();
		ProjectionAttribute projectionAttribute = new ProjectionAttribute();

		List<AttributesData> attrList = new ArrayList<>();
		AttributesData attributesData1 = new AttributesData();
		attributesData1.setAttribute("attributes");
		attributesData1.setReference(false);
		attrList.add(attributesData1);

		projectionAttribute.setDbAttribute(attrList);
		projectionList.add(projectionAttribute);

		List<DBFilters> filterList = new ArrayList<>();
		DBFilters dBFilters = new DBFilters();
		dBFilters.setAttribute("id");
		dBFilters.setCondition("=");
		dBFilters.setValue(classCode);
		filterList.add(dBFilters);

		kernelDynamicQueryReadRequest.setProjection(projectionList);
		kernelDynamicQueryReadRequest.setDbFilters(filterList);

		request.setProtocol(protocol);
		List<KernelDynamicQueryReadRequest> dynamicQueryRequestList = new ArrayList<>();
		dynamicQueryRequestList.add(kernelDynamicQueryReadRequest);
		request.setRequestData(dynamicQueryRequestList);

		response = CommonUtil.getDataFromUrl(kernelQueryURL, new ObjectMapper().writeValueAsString(request))
				.toBlocking().first();

		try {
			KernelReadDynamicQueryResponse resp = mapper.readValue(response, KernelReadDynamicQueryResponse.class);
			if (resp.getQueryResponse().get(0).getCode() != 200) {
				throw new Exception("Error reading data for the requested class");
			}
			List<AttributeMeta> objData = (List<AttributeMeta>) resp.getQueryResponse().get(0).getResponse().get(0)
					.get("attributes");
			for (Object obj : objData) {
				HashMap<String, Object> attr = (HashMap<String, Object>) obj;
				if (attr.get("displaySequence") != null) {
					if (attr.get("displaySequence").equals(1)) {
						attributeName = (String) attr.get("attributeName");
						break;
					}
				} else {
					throw new Exception("No diaplay units are available for the requested class");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response = e.getMessage();
		}
		return attributeName;
	}

	/* write/update instance in db */
	public String saveInstanceForGivenClass(Protocol protocol, List<KernelInstanceProcessRequest> requestData) {
		Request<KernelInstanceProcessRequest> request = new Request<>();
		String response = null;
		request.setProtocol(protocol);
		request.setRequestData(requestData);
		try {
			String json = new ObjectMapper().writeValueAsString(request);
			response = CommonUtil.getDataFromUrl(kernelInstanceWriteURL, json).toBlocking().first();
		} catch (Exception e) {
			e.printStackTrace();
			response = e.getMessage();
		}
		return response;
	}

	/* sets response for all employee group requests */
	public String setResponse(boolean pass, Object msg, String jwt) throws IOException {
		EmpGroupResponse response = new EmpGroupResponse();
		ObjectMapper mapper = new ObjectMapper();
		String responseString = null;
		if (pass) {
			response.setData(Constants.RESPONSE_SUCCESS_CODE, Constants.RESPONSE_SUCCESS, msg, jwt);
		} else {
			if (msg.equals(Constants.SESSION_TIMEOUT_MESSAGE)) {
				response.setData(Constants.SESSION_TIMEOUT_CODE, Constants.SESSION_TIMEOUT_MESSAGE, msg, null);
			} else {
				response.setData(Constants.RESPONSE_FAIL_CODE, Constants.RESPONSE_FAIL, msg, null);
			}
		}
		responseString = mapper.writeValueAsString(response);
		return responseString;
	}

	/* checks a string for alphanumeric */
	public boolean isAlphanumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c))
				return false;
		}
		return true;
	}

	/* checks all docs in input list for isActive */
	public String checkIfActive(List<String> docList) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		DataReadResponse response = null;
		KernelDynamicQueryReadRequest kernelDynamicQueryReadRequest = new KernelDynamicQueryReadRequest();
		List<ProjectionAttribute> projectionList = new ArrayList<>();
		ProjectionAttribute projectionAttribute = new ProjectionAttribute();

		List<AttributesData> attrList = new ArrayList<>();
		AttributesData attributesData1 = new AttributesData();
		attributesData1.setAttribute("status");
		attributesData1.setReference(false);
		AttributesData attributesData2 = new AttributesData();
		attributesData2.setAttribute("name");
		attributesData2.setReference(false);
		attrList.add(attributesData1);
		attrList.add(attributesData2);

		projectionAttribute.setDbAttribute(attrList);
		projectionList.add(projectionAttribute);

		List<DBFilters> filterList = new ArrayList<>();
		DBFilters dBFilters = new DBFilters();
		dBFilters.setAttribute("id");
		dBFilters.setCondition("IN");
		dBFilters.setValue(mapper.writeValueAsString(docList));
		filterList.add(dBFilters);

		List<String> filterJoin = new ArrayList<>();
		filterJoin.add("AND");
		kernelDynamicQueryReadRequest.setFilterJoinOperator(filterJoin);
		kernelDynamicQueryReadRequest.setProjection(projectionList);
		kernelDynamicQueryReadRequest.setDbFilters(filterList);
		response = getQueryData(null, projectionAttribute, filterList, filterJoin, new IndexingData(), false);
		// todo
		return Constants.RESPONSE_SUCCESS;
	}

	public String getDocIdForClass(Protocol protocol, String className) throws IOException {
		String response = null;
		List<ProjectionAttribute> projectionList = new ArrayList<>();
		ProjectionAttribute projectionAttribute = new ProjectionAttribute();

		List<AttributesData> attrList = new ArrayList<>();
		AttributesData attributesData1 = new AttributesData();
		attributesData1.setAttribute("id");
		attributesData1.setReference(false);
		attrList.add(attributesData1);

		projectionAttribute.setDbAttribute(attrList);
		projectionList.add(projectionAttribute);

		List<DBFilters> filterList = new ArrayList<>();
		DBFilters dBFilters = new DBFilters();
		dBFilters.setAttribute("name");
		dBFilters.setCondition("=");
		dBFilters.setValue(className);
		DBFilters dBFilter2 = new DBFilters();
		dBFilter2.setAttribute("type");
		dBFilter2.setCondition("=");
		dBFilter2.setValue("CLASS");
		filterList.add(dBFilter2);
		filterList.add(dBFilters);

		List<String> filterJoin = new ArrayList<>();
		filterJoin.add("AND");

		DataReadResponse dataReadResponse = getQueryData(protocol, projectionAttribute, filterList, filterJoin,
				new IndexingData(), false);
		List<Map<String, Object>> res = dataReadResponse.getResponse();
		if (res.size() > 0) {
			Map<String, Object> id = res.stream().filter(
					resMap -> resMap.get("id").toString().split("-")[1].split("#")[0].equals(protocol.getOrgCode()))
					.findFirst()
					.orElse(res.stream()
							.filter(resMap -> resMap.get("id").toString().split("-")[1].split("#")[0].equals("system"))
							.findFirst().orElse(null));
			response = (String) id.get("id");
		}
		return response;
	}

	public List<Map<String, Object>> getInstanceDataForGivenAttr(Protocol protocol, String classDocId, String className,
			List<String> attrNameList) throws IOException {
		List<Map<String, Object>> response = null;
		int numOfAttrToRead = attrNameList.size();
		List<ProjectionAttribute> projectionList = new ArrayList<>();
		ProjectionAttribute projectionAttribute = new ProjectionAttribute();
		List<AttributesData> attrList = new ArrayList<>();

		try {
			AttributesData attributesData1 = new AttributesData();
			attributesData1.setAttribute("id");
			attributesData1.setReference(false);
			AttributesData attributesData2 = new AttributesData();
			attributesData2.setAttribute("status");
			attributesData2.setReference(false);
			attrList.add(attributesData1);
			attrList.add(attributesData2);
			for (int count = 0; count < numOfAttrToRead; count++) {
				String attrName = attrNameList.get(count);
				AttributesData attr = new AttributesData();
				attr.setAttribute("attributes." + attrName);
				attr.setReference(false);
				attrList.add(attr);
			}
			projectionAttribute.setDbAttribute(attrList);
			projectionList.add(projectionAttribute);

			List<DBFilters> filterList = new ArrayList<>();
			DBFilters dBFilters = new DBFilters();
			dBFilters.setAttribute("classCode");
			dBFilters.setCondition("=");
			dBFilters.setValue(classDocId);
			DBFilters dBFilter2 = new DBFilters();
			dBFilter2.setAttribute("type");
			dBFilter2.setCondition("=");
			dBFilter2.setValue("INSTANCE");
			filterList.add(dBFilter2);
			filterList.add(dBFilters);

			List<String> filterJoin = new ArrayList<>();
			filterJoin.add("AND");
			DataReadResponse dataReadResponse = getQueryData(protocol, projectionAttribute, filterList, filterJoin,
					new IndexingData(), false);
			response = dataReadResponse.getResponse();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public Map<String, Object> readGroupData(Protocol protocol, String groupId) throws IOException {
		Request<KernelDynamicQueryReadRequest> request = new Request<>();
		ObjectMapper mapper = new ObjectMapper();
		String response = null;
		KernelDynamicQueryReadRequest kernelDynamicQueryReadRequest = new KernelDynamicQueryReadRequest();
		List<ProjectionAttribute> projectionList = new ArrayList<>();
		ProjectionAttribute projectionAttribute = new ProjectionAttribute();

		List<AttributesData> attrList = new ArrayList<>();
		AttributesData attributesData1 = new AttributesData();
		attributesData1.setAttribute("id");
		attributesData1.setReference(false);
		AttributesData attributesData2 = new AttributesData();
		attributesData2.setAttribute("attributes");
		attributesData2.setReference(false);
		AttributesData attributesData3 = new AttributesData();
		attributesData3.setAttribute("status");
		attributesData3.setReference(false);
		attrList.add(attributesData1);
		attrList.add(attributesData2);
		attrList.add(attributesData3);

		projectionAttribute.setDbAttribute(attrList);
		projectionList.add(projectionAttribute);

		List<DBFilters> filterList = new ArrayList<>();
		DBFilters dBFilter1 = new DBFilters();
		dBFilter1.setAttribute("id");
		dBFilter1.setCondition("=");
		dBFilter1.setValue(groupId);
		filterList.add(dBFilter1);
		kernelDynamicQueryReadRequest.setProjection(projectionList);
		kernelDynamicQueryReadRequest.setDbFilters(filterList);

		request.setProtocol(protocol);
		List<KernelDynamicQueryReadRequest> dynamicQueryRequestList = new ArrayList<>();
		dynamicQueryRequestList.add(kernelDynamicQueryReadRequest);
		request.setRequestData(dynamicQueryRequestList);

		response = CommonUtil.getDataFromUrl(kernelQueryURL, new ObjectMapper().writeValueAsString(request))
				.toBlocking().first();
		try {
			KernelReadDynamicQueryResponse queryResponse = mapper.readValue(response,
					KernelReadDynamicQueryResponse.class);
			if (queryResponse.getQueryResponse().get(0).getCode() == 200) {
				return queryResponse.getQueryResponse().get(0).getResponse().get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	public List<InstanceData> getEmployeeIdListForEmployeeGroup(Protocol protocol, List<String> parameterList,
			Map<String, ArrayList<String>> valueMap) {

		// read employee meta
		// check which parameters need to be checked
		// make query
		// fetch employees

		Response<AltReadResponse> readResponse = new Response<>();
		AltReadRequest req = new AltReadRequest();
		Request<AltReadRequest> readRequest = new Request<>();
		List<AltReadRequest> reqList = new ArrayList<>();
		List<InstanceData> instances = new ArrayList<>();
		try {
			String docId = getDocIdForClass(protocol, "Employee");
			readRequest.setProtocol(protocol);
			req.setDocIdentifier(docId);
			req.setDepth(0);
			req.setLimit(-1);
			req.setOffset(-1);
			req.setType(EnumRequestType.BOTH);
			reqList.add(req);
			readRequest.setRequestData(reqList);
			readResponse = httpUtil.commonReadRequest(readRequest);
			logger.info("Employee group data fetched!!");
			List<AttributeMeta> attributeMetaList = readResponse.getResponseData().getMeta().get(docId).getAttributes();
			instances = readResponse.getResponseData().getInstance().get(0);
			if (!instances.isEmpty()) {
				for (AttributeMeta attrMeta : attributeMetaList) {
					if (attrMeta.getDataType().equals(EnumDataType.REFERENCE)) {
						if (parameterList.contains(attrMeta.getReferenceClassCode())) {
							String attrName = attrMeta.getAttributeName();
							instances = checkEmployeeForGroup(attrName, docId,
									valueMap.get(attrMeta.getReferenceClassCode()), protocol, instances, false);
						}
					} else if (attrMeta.getDataType().equals(EnumDataType.REFERENCELIST)) {
						if (parameterList.contains(attrMeta.getReferenceClassCode())) {
							String attrName = attrMeta.getAttributeName();
							instances = checkEmployeeForGroup(attrName, docId,
									valueMap.get(attrMeta.getReferenceClassCode()), protocol, instances, true);

						}
					}
				}
			} else {
				logger.info("No employees exist in the given group");
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return instances;
	}

	public List<InstanceData> checkEmployeeForGroup(String attrName, String empDocId, List<String> attrValues,
			Protocol protocol, List<InstanceData> instanceList, boolean isReferenceList) {
		ObjectMapper mapper = new ObjectMapper();
		JsonFactory f = new JsonFactory();
		TypeReference<ArrayList<String>> typeList = new TypeReference<ArrayList<String>>() {
		};
		List<InstanceData> groupEmployees = new ArrayList<>();
		try {
			for (int i = 0; i < instanceList.size(); i++) {
				InstanceData instance = instanceList.get(i);
				String value = instance.getAttributes().get(attrName).getValue();
				if (!isReferenceList) {
					if (attrValues.contains(value)) {
						groupEmployees.add(instance);
					}
				} else {
					JsonParser jpList = f.createParser(value);
					List<String> valueList = mapper.readValue(jpList, typeList);
					if (attrValues.containsAll(valueList)) {
						groupEmployees.add(instance);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return groupEmployees;
	}
}

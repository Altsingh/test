package com.alt.business.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("login")
public class LoginPropertyConf {
	String keycloak_url;
	String datacontext;
	String angularportcontext;
	String cipherkey;
	String httpPort;
	
	public String getKeycloak_url() {
		return keycloak_url;
	}
	public void setKeycloak_url(String keycloak_url) {
		this.keycloak_url = keycloak_url;
	}
	public String getDatacontext() {
		return datacontext;
	}
	public void setDatacontext(String datacontext) {
		this.datacontext = datacontext;
	}
	public String getAngularportcontext() {
		return angularportcontext;
	}
	public void setAngularportcontext(String angularportcontext) {
		this.angularportcontext = angularportcontext;
	}
	public String getCipherkey() {
		return cipherkey;
	}
	public void setCipherkey(String cipherkey) {
		this.cipherkey = cipherkey;
	}
	public String getHttpPort() {
		return httpPort;
	}
	public void setHttpPort(String httpPort) {
		this.httpPort = httpPort;
	}
}

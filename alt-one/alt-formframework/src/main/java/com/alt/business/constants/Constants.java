package com.alt.business.constants;

public class Constants {
	private Constants(){}
	
	public static final String HEADER_ORG = "organization";
	public static final String HEADER_MODULE = "module";
	public static final String RESPONSE_SUCCESS = "SUCCESS";
	public static final Integer RESPONSE_SUCCESS_CODE = 400;
	public static final String RESPONSE_FAIL = "FAIL";
	public static final Integer RESPONSE_FAIL_CODE = 200;
	public static final String FAILURE_MSG_BADREQUEST = "Bad request data";
	public static final String FAILURE_MSG_DBREADERROR = "Error reading data from database";
	public static final String RESPONSE_UNEXPECTED_FAIL = "Unexpected error has occurred.";
	public static final Integer SESSION_TIMEOUT_CODE = 500;
	public static final String SESSION_TIMEOUT_MESSAGE = "SESSION_TIMEOUT";
	
	public enum EnumDocType {
		PACKAGE, CLASS, INSTANCE;
	}
}

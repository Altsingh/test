package com.alt.business.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alt.business.service.AppComponent2;
import com.alt.business.util.JwtUtil;
import com.alt.datacarrier.business.superadmin.app.AppCERequest;
import com.alt.datacarrier.business.superadmin.app.AppData;
import com.alt.datacarrier.business.superadmin.app.Application;
import com.alt.datacarrier.exception.AppRelatedException;
import com.alt.datacarrier.organization.FormRolePermissionTO;
import com.alt.datacarrier.workflow.UserWorkflowInfo;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("/api/v1/apps")
//@CrossOrigin
public class AppResource {

	@Autowired(required = true)
	AppComponent2 appComponent2;

	private static final Logger logger = Logger.getLogger(AppResource.class);

	@GetMapping("/test")
	public ResponseEntity<String> getTest() {
		return new ResponseEntity<>("Hello World", HttpStatus.OK);
	}

	@GetMapping("/migrate")
	public ResponseEntity<String> migrate(@RequestHeader(value = "authToken") String authToken,
			@RequestParam(value = "appcode", required = true) String appCode)
			throws InstanceRelatedException, QueryRelatedException, IOException {
		try {
			appComponent2.migrate(JwtUtil.validateJwt(authToken), appCode, -1, -1);
		} catch (Exception e) {
			logger.log(Level.FATAL, "Error in data migration ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>("Hello World", HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Application> createApp(@RequestBody AppCERequest request,
			@RequestHeader(value = "authToken") String authToken) throws IOException {
		Application app = new Application();
		try {
			app = appComponent2.createApp(request, JwtUtil.validateJwt(authToken));
			if (app == null) {
				throw new AppRelatedException("App creation failed");
			}
		} catch (Exception e) {
			logger.log(Level.FATAL, "Error Occurred in AppResource in method createApp ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(app, HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<List<AppData>> getApps(@RequestHeader(value = "authToken") String authToken,
			@RequestParam(value = "offset", required = false) Integer offset,
			@RequestParam(value = "limit", required = false) Integer limit) {

		List<AppData> apps = null;
		try {
			apps = appComponent2.getApps(JwtUtil.validateJwt(authToken), offset, limit);
		} catch (Exception e) {
			logger.log(Level.FATAL, "Error Occurred in AppResoure in method getApps ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(apps, HttpStatus.OK);
	}

	@GetMapping("/{appId}")
	public ResponseEntity<Application> getApp(@RequestHeader(value = "authToken") String authToken,
			@PathVariable("appId") String appId) {
		Application app = null;
		try {
			app = appComponent2.getAppDetails(JwtUtil.validateJwt(authToken), appId);
		} catch (Exception e) {
			logger.log(Level.FATAL, "Error Occurred in AppResource in method getApp ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.error("Executed successfully");
		return new ResponseEntity<>(app, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<String> updateApp(@RequestHeader(value = "authToken") String authToken,
			@RequestBody AppCERequest request) throws IOException {
		ResponseEntity<String> response;
		try {
			response = appComponent2.updateApp(JwtUtil.validateJwt(authToken), request.getApp());
		} catch (Exception e) {
			logger.log(Level.FATAL, "Error Occurred in AppResoure in method updateApp ", e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}

	@PostMapping(value = ("/logo/applogoupload"), headers = ("content-type=multipart/*"), produces = "application/json")
	public ResponseEntity<ObjectNode> uploadAppLogoImage(@RequestParam("uploadingFiles") MultipartFile image) {
		return appComponent2.uploadAppLogoImage(image);
	}

	@PostMapping("/workflowInfo")
	public ResponseEntity<UserWorkflowInfo> getUserWorkflowInfo(@RequestHeader(value = "authToken") String authToken,
			@RequestBody List<Integer> contextEntityIds) {
		UserWorkflowInfo userWorkflowInfo = null;
		try {
			userWorkflowInfo = appComponent2.getUserWorkflowInfo(JwtUtil.validateJwt(authToken), contextEntityIds);
		} catch (Exception e) {
			logger.log(Level.FATAL, "Error Occurred in AppResource in method getUserWorkflowInfo ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(userWorkflowInfo, HttpStatus.OK);
	}

	@PostMapping("/changeFormPermission")
	public ResponseEntity<FormRolePermissionTO> changeFormPermission(
			@RequestHeader(value = "authToken") String authToken, @RequestBody FormRolePermissionTO request)
			throws NoSuchAlgorithmException, IOException {
		request = appComponent2.changeFormPermission(JwtUtil.validateJwt(authToken), request);
		return new ResponseEntity<>(request, HttpStatus.OK);
	}
}

package com.alt.business.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


import com.alt.datacarrier.exception.AuthorizationException;
import com.alt.datacarrier.exception.BadRequestException;
import com.alt.datacarrier.exception.InternalServerError;
import com.alt.datacarrier.exception.SuperAdminException;
import com.alt.datacarrier.core.ExceptionResponse;

@ControllerAdvice
public class BusinessLayerControllerAdvice {

	@ExceptionHandler(AuthorizationException.class)
	private ResponseEntity<ExceptionResponse> error(AuthorizationException exception) {

		return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(exception.getMessage(),HttpStatus.UNAUTHORIZED.toString()),HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(InternalServerError.class)
	private ResponseEntity<ExceptionResponse> error(InternalServerError exception) {

		return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(exception.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR.toString()),HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(BadRequestException.class)
	private ResponseEntity<ExceptionResponse> error(BadRequestException exception) {
          
		return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(exception.getMessage(),HttpStatus.BAD_REQUEST.toString()),HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(SuperAdminException.class)
	private ResponseEntity<ExceptionResponse> error(SuperAdminException exception) {

		return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(exception.getMessage(),HttpStatus.UNPROCESSABLE_ENTITY.toString()),HttpStatus.UNPROCESSABLE_ENTITY);

	}

}

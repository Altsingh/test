package com.alt.business.controller;

import com.alt.business.service.MasterDataComponent;
import com.alt.business.util.JwtUtil;
import com.alt.datacarrier.business.superadmin.ClassCEResponse;
import com.alt.datacarrier.business.superadmin.ClassMasterData;
import com.alt.datacarrier.kernel.db.request.CreateClassRequest;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/masterdata/classes")
public class ClassDataResource {


    @Autowired
    MasterDataComponent masterDataComponent;

    @GetMapping
    public ResponseEntity<List<ClassMasterData>> getMasterData(@RequestHeader(value = "authToken") String authToken,
                                                               @RequestParam(value = "offset", required = false) Integer offset,
                                                               @RequestParam(value = "limit", required = false) Integer limit) {
        //remove offset and limit when pagination is implemented on frontend side
        offset = -1;
        limit = -1;
        try {
            return masterDataComponent.getMasterData(JwtUtil.validateJwt(authToken), offset, limit);
        } catch (QueryRelatedException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @GetMapping("/{classId}")
    public ResponseEntity<ClassCEResponse> getMasterDataById(@RequestHeader(value = "authToken") String authToken,
                                                             @PathVariable Integer classId) {
        return masterDataComponent.getMasterDataById(JwtUtil.validateJwt(authToken), classId);
    }

    @GetMapping("/{classId}/attributes")
    public ResponseEntity<ClassCEResponse> getAttributesForClass(@RequestHeader(value = "authToken") String authToken,
                                                                 @PathVariable Integer classId) {
        return masterDataComponent.getMasterDataById(JwtUtil.validateJwt(authToken), classId);
    }

    @PostMapping
    public ResponseEntity<ClassCEResponse> createClassMasterData(@RequestHeader(value = "authToken") String authToken,
                                                                 @RequestBody CreateClassRequest request) {
        return masterDataComponent.createClassMasterData(JwtUtil.validateJwt(authToken), request);
    }

    @PutMapping
    public ResponseEntity<ClassCEResponse> updateClassMasterData(@RequestHeader(value = "authToken") String authToken,
                                                                 @RequestBody CreateClassRequest request) {
        return masterDataComponent.updateClassMasterData(JwtUtil.validateJwt(authToken), request);
    }

    @PostMapping("/getRefereceAttributes")
    public List<String> getReferenceAttributes(@RequestHeader(value = "authToken") String authToken,
                                               @RequestBody Map<String,String> references){
        return masterDataComponent.getReferenceAttributes(JwtUtil.validateJwt(authToken),references);
    }
    @GetMapping("/{classId}/nested-attributes")
    public ResponseEntity<List<String>> getNestedAttributesForClass(@RequestHeader(value = "authToken") String authToken,
                                                                 @PathVariable Integer classId) throws NoSuchAlgorithmException, FormRelatedException, IOException {
        return masterDataComponent.getNestedClassAttributesById(JwtUtil.validateJwt(authToken), classId);
    }
}

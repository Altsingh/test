package com.alt.business.controller;

import com.alt.business.service.InstanceDataComponent;
import com.alt.business.util.JwtUtil;
import com.alt.datacarrier.kernel.db.core.InstanceData;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/masterdata/classes/{classId}/instances")
public class InstanceDataResource {

    @Autowired
    InstanceDataComponent instanceDataComponent;

    @GetMapping
    public ResponseEntity<List<InstanceData>> getInstancesData(@RequestHeader(value = "authToken") String authToken,
                                                               @PathVariable String classId,
                                                               @RequestParam(value = "offset", required = false) Integer offset,
                                                               @RequestParam(value = "limit", required = false) Integer limit) {
        //remove offset and limit when pagination is implemented on frontend side
        offset = -1;
        limit = -1;
        try {
            return instanceDataComponent.getInstancesDataNew(JwtUtil.validateJwt(authToken), classId, offset, limit);
        } catch (QueryRelatedException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @GetMapping("/{instanceId}")
    public ResponseEntity<InstanceData> getInstanceData(@RequestHeader(value = "authToken") String authToken,
                                                        @PathVariable Integer classId,
                                                        @PathVariable Integer instanceId) {
        return instanceDataComponent.getInstanceData(JwtUtil.validateJwt(authToken), instanceId);
    }

    @PostMapping
    public ResponseEntity<InstanceData> createInstance(@RequestHeader(value = "authToken") String authToken,
                                                       @RequestBody InstanceData request) throws InstanceHistoryRelatedException {
        return instanceDataComponent.createInstance(JwtUtil.validateJwt(authToken), request);
    }

    @PutMapping
    public ResponseEntity<InstanceData> updateInstance(@RequestHeader(value = "authToken") String authToken,
                                                       @RequestBody InstanceData request) throws InstanceHistoryRelatedException {
        return instanceDataComponent.updateInstance(JwtUtil.validateJwt(authToken), request);
    }
}

package com.alt.business.controller;

import com.alt.business.service.AppComponent2;
import com.alt.business.util.Keycloak;
import com.alt.core.util.JwtUtil;
import com.alt.datacarrier.business.superadmin.app.AppData;
import com.alt.datacarrier.business.user.login.SecureLoginModel;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.employee.EmployeeResponseTO;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;


/**
 * @author vaibhav.kashyap
 */

@RestController
@RequestMapping("/loginReceiverController")
public class LoginReceiverController {

	@Autowired
	Keycloak keyclock;

	@Autowired(required = true)
	AppComponent2 appComponent;

	private static final Logger logger = Logger.getLogger(LoginReceiverController.class);

	/**
	 * {@link #secureLoginKeyCloakHandler(SecureLoginModel)} : accepts AccessToken &
	 * RefreshToken & fetches user name from</br>
	 * the keycloak server.
	 * 
	 * @return ResponseEntity<SecureLoginModel>
	 * @author vaibhav.kashyap
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<SecureLoginModel> secureLoginKeyCloakHandler(
			@RequestBody SecureLoginModel secureLoginEntity) {
		String accessToken = "";
		String realm = "";

		if (secureLoginEntity != null && secureLoginEntity.getAccesstoken() != null
				&& !secureLoginEntity.getAccesstoken().isEmpty() && secureLoginEntity.getRefreshtoken() != null
				&& !secureLoginEntity.getRefreshtoken().isEmpty()) {
			accessToken = secureLoginEntity.getAccesstoken();

			// Fetch organization ID
			realm = keyclock.getOrganizationId(accessToken);

			// Fetch User Name
			String userName = keyclock.getUsernameFromUserinfoEndpoint(accessToken,
					(realm != null && !realm.equalsIgnoreCase("")) ? realm : null);
			if (userName != null && !userName.equalsIgnoreCase("")) {
				EmployeeResponseTO userInfo = appComponent.getEmployeeInfo(Integer.parseInt(realm), userName);
				secureLoginEntity.setUserName(userName);
				Integer employeeId = userInfo == null ? null : userInfo.getEmployeeId();
				Integer userId = userInfo == null ? null : userInfo.getUserId(); // userId=28298;
				Integer tenantId = userInfo == null ? null : userInfo.getTenantId();
				String appCode = secureLoginEntity.getAppCode();
				String token = generateJwtForValidatedUser(userName, realm, employeeId, userId, tenantId, appCode);
				if (token != null) {
					secureLoginEntity.setAppCode(appCode);
					secureLoginEntity.setAuthtoken(token);
					secureLoginEntity.setReceived(true);
					secureLoginEntity.setOrgId(realm);
					secureLoginEntity.setUserId(userId!=null?userId.toString():null);
					secureLoginEntity.setPhotoPath(
							(userInfo.getPhotoPath() != null && !userInfo.getPhotoPath().equalsIgnoreCase(""))
									? userInfo.getPhotoPath()
									: "");
				}
			}

		}
		if (secureLoginEntity.getAuthtoken() != null) {
			return ResponseEntity.ok(secureLoginEntity);
		} else {
			return (ResponseEntity<SecureLoginModel>) ResponseEntity.badRequest();
		}

	}

	/**
	 * {@link #getCustomAppData(SecureLoginModel)} : returns customApp Data
	 * 
	 * @return ResponseEntity<SecureLoginModel>
	 * @author vaibhav.kashyap
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getCustomAppData", method = RequestMethod.POST)
	public ResponseEntity<SecureLoginModel> getCustomAppData(@RequestBody SecureLoginModel secureLoginEntity) {
		AppData app = null;
		try {
			app = appComponent.getCustomAppDetailsByUrl(secureLoginEntity.getAppCode(),
					Integer.parseInt(secureLoginEntity.getOrgId()));
			if (app != null) {
				secureLoginEntity.setAppData(app);
			}
		} catch (Exception e) {
			logger.log(Level.FATAL, "Error Occurred in LoginReceiverController in method getCustomAppData ", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return ResponseEntity.ok(secureLoginEntity);
	}

	/**
	 * {@link #generateJwtForValidatedUser(String, String)} : accepts userName &
	 * realm & returns JWT token</br>
	 * on the basis of parameters passed
	 * 
	 * @return String
	 * @author vaibhav.kashyap
	 * @param userInfo
	 */
	public String generateJwtForValidatedUser(String userName, String realm, Integer employeeId, Integer userId,
			Integer tenantId, String appCode) {
		Protocol protocol = new Protocol();

		Timestamp time = new Timestamp(System.currentTimeMillis());
		protocol.setLoggedInTimestamp(time.getTime());
		protocol.setOrgCode(realm);
		protocol.setOrgId(Integer.parseInt(realm));
		protocol.setUserName(userName);
		protocol.setEmployeeId(employeeId);
		protocol.setUserId(userId);
		protocol.setTenantId(tenantId);
		protocol.setAppCode(appCode);
		String jwtAuthToken = "";

		try {
			jwtAuthToken = JwtUtil.generateJwt(protocol);

		} catch (Exception ex) {
			jwtAuthToken = null;
		}

		return jwtAuthToken;
	}

}

package com.alt.business.controller;

import java.io.IOException;

import com.alt.datacarrier.exception.SuperAdminException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alt.business.service.SuperadminServiceImpl;
import com.alt.datacarrier.business.superadmin.CreateNewDocSuperadminRequest;
import com.alt.datacarrier.business.superadmin.FetchAllSuperadminRequest;


//TODO Decide

@RestController
@RequestMapping(path = "/business")
public class SuperadminController {

	@Autowired(required = true)
	SuperadminServiceImpl superadminService;

	private static final Logger logger = Logger.getLogger(SuperadminServiceImpl.class);

	@RequestMapping(value = "/fetchall", method = RequestMethod.POST)
	@ResponseBody
	public String fetchAll(@RequestBody FetchAllSuperadminRequest request) throws IOException {
		String response = null;
		try{
		response = superadminService.fetchAll(request);
		}
		catch(Exception e){
			logger.error("Failed to fetch data",e);
			throw new SuperAdminException(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/createupdatedelete", method = RequestMethod.POST)
	@ResponseBody
	public String createUpdateDelete(@RequestBody CreateNewDocSuperadminRequest request) throws IOException {
		String response = null;
		response = superadminService.createUpdateDelete(request);
		return response;
	}

}

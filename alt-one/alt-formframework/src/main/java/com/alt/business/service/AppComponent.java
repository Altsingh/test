package com.alt.business.service;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.alt.business.transport.output.CustomAppSetupResponse;
import com.alt.business.util.FileWriterUtil;
import com.alt.business.util.Utility;
import com.alt.datacarrier.business.superadmin.app.AppCERequest;
import com.alt.datacarrier.business.superadmin.app.AppData;
import com.alt.datacarrier.business.superadmin.app.Application;
import com.alt.datacarrier.business.superadmin.app.AttachedForm;
import com.alt.datacarrier.business.superadmin.app.HomeBuilderRequestTO;
import com.alt.datacarrier.business.superadmin.app.Menu;
import com.alt.datacarrier.business.superadmin.app.MenuGroups;
import com.alt.datacarrier.business.superadmin.app.Module;
import com.alt.datacarrier.common.enumeration.EnumSortDirection;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.employee.EmployeeRequestTO;
import com.alt.datacarrier.employee.EmployeeResponseTO;
import com.alt.datacarrier.exception.AppRelatedException;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.core.AttributesData;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.core.DbFilters;
import com.alt.datacarrier.kernel.db.core.DynamicReadRequest;
import com.alt.datacarrier.kernel.db.core.DynamicReadResponse;
import com.alt.datacarrier.kernel.db.core.InstanceData;
import com.alt.datacarrier.kernel.db.core.OrderAttribute;
import com.alt.datacarrier.kernel.db.core.ProjectionAttribute;
import com.alt.datacarrier.kernel.db.request.CustomAppSetupRequest;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datacarrier.organization.ChangeFormPermissionRequestResponseTO;
import com.alt.datacarrier.organization.FormPermissionsRequestTO;
import com.alt.datacarrier.organization.FormPermissionsResponseTO;
import com.alt.datacarrier.organization.FormRolePermissionTO;
import com.alt.datacarrier.organization.OrganizationRoleRequestTO;
import com.alt.datacarrier.organization.OrganizationRoleResponseTO;
import com.alt.datacarrier.organization.RoleTO;
import com.alt.datacarrier.workflow.AppliedWorkflowCardTO;
import com.alt.datacarrier.workflow.ApprovalWorkflowCardTO;
import com.alt.datacarrier.workflow.TaskGroupTO;
import com.alt.datacarrier.workflow.TaskTO;
import com.alt.datacarrier.workflow.TasksRequestTO;
import com.alt.datacarrier.workflow.TasksResponseTO;
import com.alt.datacarrier.workflow.UserWorkflowInfo;
import com.alt.datacarrier.workflow.WorkflowCardTO;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.service.IClassService;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.datakernel.service.IFormService;
import com.alt.datakernel.service.IInstanceService;
import com.alt.formengine.config.SecurityPropertiesConf;
import com.alt.formengine.service.HttpService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Component
@Deprecated
public class AppComponent {

	@Autowired(required = true)
	IInstanceService instanceService;

	@Autowired(required = true)
	IDynamicQuery dynamicQuery;

	@Autowired
	SecurityPropertiesConf securityPropertiesConf;

	@Autowired
	private FileWriterUtil fileWriterUtil;

	@Autowired
	IFormService formService;

	@Autowired
	IClassService classService;

	private static final Logger logger = Logger.getLogger(AppComponent.class);

	public Application getAppDetails(Protocol protocol, String appId) throws NumberFormatException,
			InstanceRelatedException, JsonParseException, JsonMappingException, IOException, NoSuchAlgorithmException {
		ObjectMapper mapper = new ObjectMapper();
		InstanceData data = instanceService.read(protocol, Integer.valueOf(appId));
		Map<String, Object> attributes = data.getAttributes();
		String modulesString = (String) attributes.get("modules");
		List<Module> modules = null;
		if (modulesString != null && !modulesString.isEmpty()) {
			modules = mapper.readValue(modulesString, new TypeReference<List<Module>>() {
			});
		}

		String homeCardsStr = (String) attributes.get("homeCards");
		List<HomeBuilderRequestTO> homeCards = null;
		if (homeCardsStr != null && !homeCardsStr.isEmpty()) {
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			homeCards = mapper.readValue(homeCardsStr, new TypeReference<List<HomeBuilderRequestTO>>() {
			});
		}

		List<Integer> formIds = new ArrayList<Integer>();
		List<Integer> classIds = new ArrayList<Integer>();
		this.updateRepublishedFormsInMenu(protocol, modules);
		for (Module module : modules) {
			for (Menu menu : module.getMenus()) {
				if (menu.getForm() != null) {
					formIds.add(Integer.parseInt((menu.getForm().getFormId())));
				}
			}
		}
		List<RoleTO> roles = this.getOrganizationRoles(protocol.getOrgId());
		Map<Integer, RoleTO> roleMap = new HashMap<Integer, RoleTO>();
		for (RoleTO role : roles) {
			roleMap.put(role.getRoleId(), role);
		}

		List<FormRolePermissionTO> formPermissions = this.getFormPermissions(protocol.getOrgId());
		Map<Long, List<RoleTO>> formRolePermissionMap = new HashMap<Long, List<RoleTO>>();
		for (FormRolePermissionTO permission : formPermissions) {
			if (!formRolePermissionMap.containsKey(permission.getHrFormId())) {
				formRolePermissionMap.put(permission.getHrFormId(), new ArrayList<RoleTO>());
			}
			formRolePermissionMap.get(permission.getHrFormId()).add(roleMap.get(permission.getRoleId()));
		}

		EmployeeResponseTO employeeInfo = getEmployeeInfo(protocol.getOrgId(), protocol.getUserName());

		try {
			Map<String, AltForm> formMap = new HashMap<String, AltForm>();
			if (!formIds.isEmpty()) {
				List<AltForm> forms = formService.listByIds(protocol, formIds);
				formMap = new HashMap<String, AltForm>();
				for (AltForm form : forms) {
					classIds.add(form.getMetaClassId());
					formMap.put(form.getId(), form);
				}
			}
			for (Module module : modules) {
				module.setPermitted(Boolean.FALSE);
				for (Menu menu : module.getMenus()) {
					menu.setPermitted(Boolean.FALSE);
					if (menu.getForm() != null) {
						AttachedForm form = menu.getForm();
						String formId = form.getFormId();
						if (formMap.containsKey(formId)) {
							form.setFormSecurityId(formMap.get(formId).getFormSecurityId());
							List<RoleTO> assignedRoles = new ArrayList<RoleTO>();
							if (form.getFormSecurityId() != null) {
								Long hrformId = Long.parseLong(form.getFormSecurityId());
								if (formRolePermissionMap.containsKey(hrformId)) {
									assignedRoles.addAll(formRolePermissionMap.get(hrformId));
									form.setAssignedRoles(assignedRoles);
								}

								for (RoleTO assignedRole : form.getAssignedRoles()) {
									if (employeeInfo.getAssignedRoleIds().contains(assignedRole.getRoleId())) {
										module.setPermitted(Boolean.TRUE);
										menu.setPermitted(Boolean.TRUE);
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Application.Builder().withAppCode((String) attributes.get("appCode")).withAppId(appId)
				.withAppName((String) attributes.get("appName")).withModules(modules).withHomeCards(homeCards)
				.withEmployeeInfo(employeeInfo).withClassIds(classIds).withRoles(roles)
				.withAppLogoUrl((String) attributes.get("appLogoUrl")).build();
	}

	private void updateRepublishedFormsInMenu(Protocol protocol, List<Module> modules) {
		Map<String, AttachedForm> formNameMap = new HashMap<String, AttachedForm>();
		for (Module module : modules) {
			for (Menu menu : module.getMenus()) {
				if (menu.getForm() != null) {
					formNameMap.put(menu.getForm().getFormName(), menu.getForm());
				}
			}
		}
		if (formNameMap.isEmpty())
			return;
		List<AltForm> forms = formService.listByNames(protocol, formNameMap.keySet());
		Map<String, AltForm> latestNameFormMap = new HashMap<String, AltForm>();
		for (AltForm form : forms) {
			if (form.getState().equals(EnumFormState.PUBLISH) || form.getState().equals(EnumFormState.DRAFT)) {
				latestNameFormMap.put(form.getUiClassName(), form);
			}
		}
		for (Module module : modules) {
			for (Menu menu : module.getMenus()) {
				if (menu.getForm() != null) {
					if (menu != null && menu.getForm() != null) {
						if (menu.getForm().getFormName() != null
								&& latestNameFormMap.containsKey(menu.getForm().getFormName())) {
							Integer latestFormId = Integer
									.parseInt(latestNameFormMap.get(menu.getForm().getFormName()).getId());
							Integer menuFormId = Integer.parseInt(menu.getForm().getFormId());
							if (latestFormId > menuFormId) {
								menu.getForm().setFormId(latestFormId.toString());
							}
						}

					}
				}
			}
		}
	}

	private List<FormRolePermissionTO> getFormPermissions(Integer organizationId)
			throws NoSuchAlgorithmException, IOException {
		FormPermissionsRequestTO request = new FormPermissionsRequestTO();
		request.setOrganizationId(organizationId);
		FormPermissionsResponseTO response = null;
		HttpResponse rawResponse = HttpService.callRestAPI(
				securityPropertiesConf
						.getPermissions()/* "https://s2demo.sohum.com/service/api/organization/formRolePermissions" */,
				request);
		response = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), FormPermissionsResponseTO.class);
		return response.getPermissions();
	}

	private List<RoleTO> getOrganizationRoles(Integer organizationId) throws NoSuchAlgorithmException, IOException {
		OrganizationRoleRequestTO request = new OrganizationRoleRequestTO();
		request.setOrganizationId(organizationId);
		OrganizationRoleResponseTO response = null;
		HttpResponse rawResponse = HttpService.callRestAPI(
				securityPropertiesConf.getRoles()/* "https://s2demo.sohum.com/service/api/organization/roles" */,
				request);
		response = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), OrganizationRoleResponseTO.class);
		return response.getRoles();
	}

	public EmployeeResponseTO getEmployeeInfo(Integer organizationId, String userName) {
		try {
			EmployeeRequestTO employeeRequest = new EmployeeRequestTO();
			employeeRequest.setOrganizationId(organizationId);
			employeeRequest.setUsername(userName);
			EmployeeResponseTO employeeResponse = null;
			HttpResponse rawResponse = HttpService.callRestAPI(
					securityPropertiesConf
							.getEmployeeInfo()/* "https://s2demo.sohum.com/service/api/employee/employeeInfo" */,
					employeeRequest);
			employeeResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
					EmployeeResponseTO.class);
			return employeeResponse;
		} catch (Exception e) {
			return null;
		}
	}

	public UserWorkflowInfo getUserWorkflowInfo(Protocol protocol, List<Integer> contextEntityIds)
			throws NoSuchAlgorithmException, IOException {
		UserWorkflowInfo userWorkflowInfo = new UserWorkflowInfo();
		userWorkflowInfo.setTasks(new ArrayList<TaskTO>());
		userWorkflowInfo.setCards(new ArrayList<WorkflowCardTO>());

		List<TaskTO> approvalTasks = null;
		List<TaskTO> appliedTasks = null;

		TasksRequestTO tasksRequest = new TasksRequestTO();
		tasksRequest.setOrganizationId(protocol.getOrgId());
		tasksRequest.setUsername(protocol.getUserName());
		tasksRequest.setContextEntityIds(contextEntityIds);
		TasksResponseTO tasksResponse = null;
		HttpResponse rawResponse = HttpService.callRestAPI(
				securityPropertiesConf.getWfapi4()/* "https://s2demo.sohum.com/service/api/workflow/tasksInfo" */,
				tasksRequest);
		tasksResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), TasksResponseTO.class);
		approvalTasks = tasksResponse.getTasks();

		userWorkflowInfo.getTasks().addAll(approvalTasks);

		tasksResponse = null;
		rawResponse = HttpService.callRestAPI(
				securityPropertiesConf
						.getWfapi5()/* "https://s2demo.sohum.com/service/api/workflow/appliedTasksInfo" */,
				tasksRequest);
		tasksResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), TasksResponseTO.class);
		appliedTasks = tasksResponse.getTasks();

		Set<Integer> classIds = new HashSet<Integer>();
		SimpleDateFormat sdfIn = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat sdfOut = new SimpleDateFormat("dd/MM/yyyy");
		for (TaskTO task : appliedTasks) {
			classIds.add(task.getEntityId());
			String startDateString = task.getStartDate();
			Date startDate = null;
			try {
				startDate = sdfIn.parse(startDateString);
			} catch (ParseException pe) {
				continue;
			}
			String convertedStartDateString = sdfOut.format(startDate);
			task.setStartDate(convertedStartDateString);
		}
		for (TaskTO task : approvalTasks) {
			classIds.add(task.getEntityId());
			String startDateString = task.getStartDate();
			Date startDate = null;
			try {
				startDate = sdfIn.parse(startDateString);
			} catch (ParseException pe) {
				continue;
			}
			String convertedStartDateString = sdfOut.format(startDate);
			task.setStartDate(convertedStartDateString);
		}

		if (classIds.isEmpty()) {
			return userWorkflowInfo;
		}

		List<ClassMeta> classes = classService.listAll(protocol, classIds);
		Map<String, String> classNameMap = new HashMap<String, String>();
		for (ClassMeta formClass : classes) {
			classNameMap.put(formClass.getId(), formClass.getName());
		}
		Map<String, WorkflowCardTO> cardMap = new TreeMap<String, WorkflowCardTO>();
		for (TaskTO task : appliedTasks) {
			String className = classNameMap.get(new Integer(task.getEntityId()).toString());
			if (!cardMap.containsKey(className)) {
				WorkflowCardTO wfCard = new WorkflowCardTO();

				AppliedWorkflowCardTO appliedCard = new AppliedWorkflowCardTO();
				appliedCard.setAppliedTasks(new ArrayList<TaskTO>());
				appliedCard.setAppliedTaskGroups(new ArrayList<TaskGroupTO>());
				wfCard.setAppliedWorkflowCard(appliedCard);

				ApprovalWorkflowCardTO approvalCard = new ApprovalWorkflowCardTO();
				approvalCard.setApprovalTasks(new ArrayList<TaskTO>());
				wfCard.setApprovalWorkflowCard(approvalCard);

				wfCard.setFormName(className);
				cardMap.put(className, wfCard);
			}
		}
		for (TaskTO task : approvalTasks) {
			String className = classNameMap.get(new Integer(task.getEntityId()).toString());
			if (!cardMap.containsKey(className)) {
				WorkflowCardTO wfCard = new WorkflowCardTO();

				AppliedWorkflowCardTO appliedCard = new AppliedWorkflowCardTO();
				appliedCard.setAppliedTasks(new ArrayList<TaskTO>());
				appliedCard.setAppliedTaskGroups(new ArrayList<TaskGroupTO>());
				wfCard.setAppliedWorkflowCard(appliedCard);

				ApprovalWorkflowCardTO approvalCard = new ApprovalWorkflowCardTO();
				approvalCard.setApprovalTasks(new ArrayList<TaskTO>());
				wfCard.setApprovalWorkflowCard(approvalCard);

				wfCard.setFormName(className);
				cardMap.put(className, wfCard);
			}
		}
		for (TaskTO task : appliedTasks) {
			String className = classNameMap.get(new Integer(task.getEntityId()).toString());
			cardMap.get(className).getAppliedWorkflowCard().getAppliedTasks().add(task);
		}
		for (TaskTO task : approvalTasks) {
			String className = classNameMap.get(new Integer(task.getEntityId()).toString());
			cardMap.get(className).getApprovalWorkflowCard().getApprovalTasks().add(task);
		}

		userWorkflowInfo.getCards().addAll(cardMap.values());

		for (WorkflowCardTO card : userWorkflowInfo.getCards()) {
			card.getAppliedWorkflowCard().setAppliedTaskCount(card.getAppliedWorkflowCard().getAppliedTasks().size());
			card.getApprovalWorkflowCard()
					.setApprovalTaskCount(card.getApprovalWorkflowCard().getApprovalTasks().size());

			AppliedWorkflowCardTO appliedCard = card.getAppliedWorkflowCard();
			List<TaskTO> tasks = appliedCard.getAppliedTasks();
			Map<String, Integer> statusCountMap = new TreeMap<String, Integer>();
			for (TaskTO task : tasks) {
				if (!statusCountMap.containsKey(task.getStatusMessage())) {
					statusCountMap.put(task.getStatusMessage(), 0);
				}
				statusCountMap.put(task.getStatusMessage(), statusCountMap.get(task.getStatusMessage()) + 1);
			}
			List<TaskGroupTO> taskGroups = new ArrayList<TaskGroupTO>();
			for (String status : statusCountMap.keySet()) {
				TaskGroupTO taskGroup = new TaskGroupTO();
				taskGroup.setStatus(status);
				taskGroup.setCount(statusCountMap.get(status));
				taskGroups.add(taskGroup);
			}
			appliedCard.setAppliedTaskGroups(taskGroups);
		}

		return userWorkflowInfo;
	}

	@Transactional
	public List<AppData> getApps(Protocol protocol, Integer offset, Integer limit)
			throws IOException, InstanceRelatedException, QueryRelatedException {
		if (offset == null) {
			offset = -1;
		}

		if (limit == null) {
			limit = -1;
		}

		DynamicReadRequest request = new DynamicReadRequest();
		createDynamicQueryRequest(request, protocol.getOrgCode(), offset, limit, protocol.getOrgId());

		DynamicReadResponse response = dynamicQuery.read(request);
		List<Object[]> instances = response.getResponse();

		List<AppData> apps = new ArrayList<>();
		for (Object[] instance : instances) {
			AppData app = new AppData.Builder().withAppCode((String) instance[1])
					.withAppId(((Integer) instance[6]).toString()).withAppName((String) instance[0])
					.withAppLogoUrl((String) instance[5]).withCreatedBy((String) instance[2])
					.withCreatedDate(((BigInteger) instance[3]).toString())
					.withStatus(EnumDocStatusType.values()[(Integer) instance[4]]).build();
			apps.add(app);
		}

		return apps;
	}

	/**
	 * {@link #getCustomAppDetailsByUrl(String)} : returns customApp Data
	 * 
	 * @return List<AppData>
	 * @author vaibhav.kashyap
	 */
	public List<AppData> getCustomAppDetailsByUrl(String appURL)
			throws IOException, InstanceRelatedException, QueryRelatedException {
		Integer offset = -1;
		Integer limit = 1;

		DynamicReadRequest request = new DynamicReadRequest();

		// Create request object for table being queried along with query filters
		createDynamicQueryRequestForCustomApp(request, offset, limit, appURL);
		List<AppData> apps = null;
		try {
			// read & execute query
			DynamicReadResponse response = dynamicQuery.read(request);
			List<Object[]> instances = response.getResponse();
			if (instances.size() > 0) {
				apps = new ArrayList<>();
				for (Object[] instance : instances) {
					AppData app = new AppData.Builder().withAppCode((String) instance[1])
							.withAppId(((Integer) instance[6]).toString()).withAppName((String) instance[0])
							.withAppLogoUrl((String) instance[5]).withCreatedBy((String) instance[2])
							.withCreatedDate(((BigInteger) instance[3]).toString())
							.withStatus(EnumDocStatusType.values()[(Integer) instance[4]]).build();
					apps.add(app);
				}
			}

		} catch (Exception ex) {
			apps = null;
			logger.error("Exception thrown on getCustomAppDetailsByUrl " + ex);
		}

		return apps;
	}

	private void createDynamicQueryRequest(DynamicReadRequest request, String org, Integer offset, Integer limit,
			Integer orgId) {
		List<ProjectionAttribute> projections = new ArrayList<>();
		ProjectionAttribute attribute = new ProjectionAttribute();
		attribute.setDbClass("instance");
		List<AttributesData> dbAttributes = new ArrayList<>();
		AttributesData data1 = new AttributesData();
		data1.setAttribute("attributes");
		data1.setJsonbType(true);
		data1.setJsonName("attribute");
		data1.setJsonbAttribute("appName");
		data1.setAs("AppName");
		dbAttributes.add(data1);
		AttributesData data2 = new AttributesData();
		data2.setAttribute("attributes");
		data2.setJsonbType(true);
		data2.setJsonName("attribute");
		data2.setJsonbAttribute("appCode");
		data2.setAs("AppCode");
		dbAttributes.add(data2);
		AttributesData data3 = new AttributesData();
		data3.setAttribute("createdby");
		data3.setAs("createdby");
		dbAttributes.add(data3);
		AttributesData data4 = new AttributesData();
		data4.setAttribute("createddate");
		data4.setAs("createddate");
		dbAttributes.add(data4);
		AttributesData data5 = new AttributesData();
		data5.setAttribute("status");
		data5.setAs("status");
		dbAttributes.add(data5);
		AttributesData data6 = new AttributesData();
		data6.setAttribute("attributes");
		data6.setJsonbType(true);
		data6.setJsonName("attribute");
		data6.setJsonbAttribute("appLogoUrl");
		data6.setAs("appLogoUrl");
		dbAttributes.add(data6);
		AttributesData data = new AttributesData();
		data.setAttribute("id");
		data.setAs("id");
		dbAttributes.add(data);
		attribute.setDbAttributes(dbAttributes);
		projections.add(attribute);
		request.setProjections(projections);
		request.setFromDbClass("instance");
		List<DbFilters> dbFilters = new ArrayList<>();
		/*
		 * DbFilters filter1 = new DbFilters(); filter1.setAttribute("orgid");
		 * filter1.setClassName("instance"); filter1.setCondition("=");
		 * filter1.setValue(org); dbFilters.add(filter1);
		 */
		DbFilters filter2 = new DbFilters();
		filter2.setAttribute("classname");
		filter2.setClassName("instance");
		filter2.setCondition("=");
		filter2.setValue("Application");
		dbFilters.add(filter2);
		DbFilters filter3 = new DbFilters();
		filter3.setAttribute("organizationid ");
		filter3.setClassName("instance");
		filter3.setCondition("=");
		filter3.setValue(orgId.toString());
		dbFilters.add(filter3);
		request.setDbFilters(dbFilters);
		request.setLimit(limit);
		request.setOffset(offset);
		List<OrderAttribute> orderByList = new ArrayList<>();
		OrderAttribute orderBy = new OrderAttribute();
		orderBy.setDbClass("instance");
		orderBy.setDbAttr("createddate");
		orderBy.setSortDirection(EnumSortDirection.DESC);
		orderByList.add(orderBy);
		request.setOrderBy(orderByList);

	}

	/**
	 * {@link #createDynamicQueryRequestForCustomApp(DynamicReadRequest, Integer, Integer, String)}
	 * : creates query for fetching custom app details on basis of URL passed
	 * 
	 * @author vaibhav.kashyap
	 */
	private void createDynamicQueryRequestForCustomApp(DynamicReadRequest request, Integer offset, Integer limit,
			String appCode) {
		List<ProjectionAttribute> projections = new ArrayList<>();
		ProjectionAttribute attribute = new ProjectionAttribute();
		attribute.setDbClass("instance");
		List<AttributesData> dbAttributes = new ArrayList<>();
		AttributesData data1 = new AttributesData();
		data1.setAttribute("attributes");
		data1.setJsonbType(true);
		data1.setJsonName("attribute");
		data1.setJsonbAttribute("appName");
		data1.setAs("AppName");
		dbAttributes.add(data1);
		AttributesData data2 = new AttributesData();
		data2.setAttribute("attributes");
		data2.setJsonbType(true);
		data2.setJsonName("attribute");
		data2.setJsonbAttribute("appCode");
		data2.setAs("AppCode");
		dbAttributes.add(data2);
		AttributesData data3 = new AttributesData();
		data3.setAttribute("createdby");
		data3.setAs("createdby");
		dbAttributes.add(data3);
		AttributesData data4 = new AttributesData();
		data4.setAttribute("createddate");
		data4.setAs("createddate");
		dbAttributes.add(data4);
		AttributesData data5 = new AttributesData();
		data5.setAttribute("status");
		data5.setAs("status");
		dbAttributes.add(data5);
		AttributesData data6 = new AttributesData();
		data6.setAttribute("attributes");
		data6.setJsonbType(true);
		data6.setJsonName("attribute");
		data6.setJsonbAttribute("appLogoUrl");
		data6.setAs("AppLogoUrl");
		dbAttributes.add(data6);
		AttributesData data = new AttributesData();
		data.setAttribute("id");
		data.setAs("id");
		dbAttributes.add(data);
		attribute.setDbAttributes(dbAttributes);
		projections.add(attribute);
		request.setProjections(projections);
		request.setFromDbClass("instance");
		List<DbFilters> dbFilters = new ArrayList<>();

		DbFilters filter2 = new DbFilters();
		filter2.setAttribute("classname");
		filter2.setClassName("instance");
		filter2.setCondition("=");
		filter2.setValue("Application");
		dbFilters.add(filter2);

		DbFilters filter4 = new DbFilters();
		filter4.setAttribute("attributes");
		filter4.setClassName("instance");
		filter4.setJsonbType(true);
		filter4.setJsonName("attribute");
		List<String> jsonbAttributes1 = new ArrayList<String>();
		jsonbAttributes1.add("appCode");
		filter4.setJsonbAttribute(jsonbAttributes1);
		filter4.setCondition("=");
		filter4.setValue(appCode);
		dbFilters.add(filter4);

		request.setDbFilters(dbFilters);
		request.setLimit(limit);
		request.setOffset(offset);
		List<OrderAttribute> orderByList = new ArrayList<>();
		OrderAttribute orderBy = new OrderAttribute();
		orderBy.setDbClass("instance");
		orderBy.setDbAttr("createddate");
		orderBy.setSortDirection(EnumSortDirection.DESC);
		orderByList.add(orderBy);
		request.setOrderBy(orderByList);

	}

	public Application createApp(AppCERequest request, Protocol protocol) throws InstanceRelatedException,
			ClassRelatedException, InstanceHistoryRelatedException, NoSuchAlgorithmException, IOException {
		InstanceData data = null;
		Application app = request.getApp();
		if (!Utility.validateName(app.getAppName())) {
			throw new AppRelatedException("AppName validation failed.");
		}

		if (!Utility.validateAppCode(app.getAppCode())) {
			throw new AppRelatedException("AppCode validation failed.");
		}

		InstanceRequest appRequest = null;

		if (app.getIsApplicationForEditing() != null && app.getIsApplicationForEditing() && app.getAppId() != null
				&& !app.getAppId().equalsIgnoreCase("")) {
			appRequest = new InstanceRequest.Builder().withAttributes(createAppAttributes(app)).withProtocol(protocol)
					.withClassName("Application").withAppId(Integer.parseInt(app.getAppId()))
					.withStatus(EnumDocStatusType.ACTIVE).build();
			data = instanceService.edit(appRequest);
		} else {
			appRequest = new InstanceRequest.Builder().withAttributes(createAppAttributes(app)).withProtocol(protocol)
					.withClassName("Application").withStatus(EnumDocStatusType.ACTIVE).build();
			data = instanceService.create(appRequest);
		}

		if (data != null) {
			app.setAppId(data.getInstanceId().toString());
		}

		/**
		 * If app details successfully persisted in postgres, save details in admin db
		 * as well. Call webservice for this job.
		 */
		if (app.getAppId() != null && !app.getAppId().equalsIgnoreCase("")) {
			publishAppDetailsToAdmin(protocol, app, securityPropertiesConf.getCustomappsetuprequesturl());
		}
		return app;
	}

	public boolean publishAppDetailsToAdmin(Protocol protocol, Application app, String url)
			throws InstanceRelatedException, NoSuchAlgorithmException, IOException {
		Boolean isPersistedInAdmin = false;
		CustomAppSetupResponse customAppSetupResponse = null;
		// MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		CustomAppSetupRequest requestTO = getCustomAppAdminRequestObject(protocol, app);
		HttpResponse rawResponse = HttpService
				.publishAppDetailsToAdmin(url/* "https://s2demo-admin.sohum.com/services/customappsetup" */, requestTO);

		try {
			ObjectMapper mapper = new ObjectMapper();
			customAppSetupResponse = mapper.readValue(rawResponse.getEntity().getContent(),
					CustomAppSetupResponse.class);
			if (customAppSetupResponse != null) {
				if (!customAppSetupResponse.getSuccess()) {
					logger.error("Error while saving App Details in Admin records : CustomAppSetupRestService");
					throw new RuntimeException(
							"Error while saving App Details in Admin records : CustomAppSetupRestService");
				} else {
					isPersistedInAdmin = customAppSetupResponse.getSuccess();
				}
			} else {
				logger.error("Error while saving App Details in Admin records : CustomAppSetupRestService");
				throw new RuntimeException(
						"Error while saving App Details in Admin records : CustomAppSetupRestService");
			}
		} catch (RuntimeException e) {
			logger.error("Error while reading response of form security web service.", e);
			// headers.add("response", "Error while reading response of form security web
			// servicey");
			isPersistedInAdmin = false;
		}
		logger.info("Request sent for form security. Response is {}" + isPersistedInAdmin);

		return isPersistedInAdmin;
	}

	public CustomAppSetupRequest getCustomAppAdminRequestObject(Protocol protocol, Application app) {
		CustomAppSetupRequest requestObj = new CustomAppSetupRequest();
		requestObj.setAppCode(app.getAppCode());
		requestObj.setOrganizationId(protocol.getOrgId());
		requestObj.setTenantId(Integer.parseInt(app.getTenantId()));
		requestObj.setAppName(app.getAppName());
		requestObj.setApplicationForDeletion(
				(app.getIsApplicationForDeletion() == null) ? false : app.getIsApplicationForDeletion());
		requestObj.setApplicationForEditing(
				(app.getIsApplicationForEditing() == null) ? false : app.getIsApplicationForEditing());

		if (app.getAppLogoUrl() == null || app.getAppLogoUrl().equals("")
				|| app.getAppLogoUrl().equalsIgnoreCase("undefined"))
			requestObj.setAppLogoUrl(securityPropertiesConf.getDefaultapplogo());
		else
			requestObj.setAppLogoUrl(app.getAppLogoUrl());

		return requestObj;
	}

	public ResponseEntity<String> updateApp(Protocol protocol, Application application)
			throws InstanceRelatedException, JsonProcessingException {
		Application app = application;

		if (!checkNulls(application)) {
			return new ResponseEntity<>("mandatory fields missing", HttpStatus.BAD_REQUEST);
		}
		if (!Utility.validateName(app.getAppName())) {
			return new ResponseEntity<>("app name validation failed", HttpStatus.BAD_REQUEST);
		}
		if (!validateAppModules(app.getModules())) {
			return new ResponseEntity<>("app module validation failed", HttpStatus.BAD_REQUEST);
		}
		InstanceRequest appRequest = new InstanceRequest(new InstanceRequest.Builder()
				.withAttributes(createAppAttributes(app)).withProtocol(protocol).withClassName("Application")
				.withStatus(EnumDocStatusType.ACTIVE).withInstanceId(Integer.valueOf(app.getAppId())));
		InstanceData data = instanceService.updateAttributes(appRequest);

		if (data == null) {
			return new ResponseEntity<>("App save failed", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);

	}

	private boolean checkNulls(Application application) {
		if (application.getAppName() == null || application.getAppCode() == null || application.getAppId() == null) {
			return false;
		}
		if (application.getAppName().equals("") || application.getAppCode().equals("")
				|| application.getAppId().equals("")) {
			return false;
		}
		return true;
	}

	public Map<String, Object> createAppAttributes(Application app) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String modules = null;
		String homeCards = null;

		Map<String, Object> attributes = new HashMap<>();

		if (app.getModules() != null) {
			modules = mapper.writeValueAsString(app.getModules());
		}

		if (app.getHomeCards() != null) {
			homeCards = mapper.writeValueAsString(app.getHomeCards());
		}

		attributes.put("appName", app.getAppName());
		attributes.put("appCode", app.getAppCode());
		attributes.put("modules", modules);
		attributes.put("homeCards", homeCards);
		attributes.put("annoucement", mapper.writeValueAsString(app.getAnnouncement()));
		if (app.getAppLogoUrl() == null || app.getAppLogoUrl().equals("")
				|| app.getAppLogoUrl().equalsIgnoreCase("undefined"))
			attributes.put("appLogoUrl", securityPropertiesConf.getDefaultapplogo());
		else
			attributes.put("appLogoUrl", app.getAppLogoUrl());

		return attributes;
	}

	public boolean validateAppModules(List<Module> modules) {
		if (modules == null) {
			return false;
		}
		List<String> moduleNames = modules.stream().map(Module::getName).collect(Collectors.toList());
		if (!Utility.checkUniqueStringInList(moduleNames)) {
			return false;
		}
		for (Module module : modules) {
			if (!validateAppMenuGroups(module.getMenuGroups())) {
				return false;
			}

			if (!Utility.validateName(module.getName())) {
				return false;
			}

			if (!validateAppMenus(module.getMenus())) {
				return false;
			}
		}
		return true;
	}

	private boolean validateAppMenus(List<Menu> menus) {
		if (menus == null) {
			return false;
		}
		List<String> menuNames = menus.stream().map(Menu::getName).collect(Collectors.toList());
		if (!Utility.checkUniqueStringInList(menuNames)) {
			return false;
		}
		for (Menu menu : menus) {
			if (!Utility.validateName(menu.getName())) {
				return false;
			}
		}
		return true;
	}

	private boolean validateAppMenuGroups(List<MenuGroups> menuGroups) {
		return true;
	}

	public ResponseEntity<ObjectNode> uploadAppLogoImage(MultipartFile multipartFile) {
		String imagePath = null;
		ObjectNode objectNode = null;
		try {
			String writeImageServerPath = securityPropertiesConf.getApplogowriteurl();
			writeImageServerPath += File.separator + "logo";
			imagePath = securityPropertiesConf.getApplogoreadurl() + "/logo";
			String fileName = null;
			fileName = "logo_" + ThreadLocalRandom.current().nextInt(0, 10000 + 1) + new Date().getTime() + "."
					+ FilenameUtils.getExtension(multipartFile.getOriginalFilename());
			imagePath += "/" + fileName;
			fileWriterUtil.writeFile(multipartFile, writeImageServerPath, fileName);
			objectNode = new ObjectMapper().createObjectNode();
			objectNode.put("applogourl", imagePath);
		} catch (Exception e) {
			return new ResponseEntity<ObjectNode>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(objectNode, HttpStatus.OK);
	}

	public FormRolePermissionTO changeFormPermission(Protocol protocol, FormRolePermissionTO permissionRequest)
			throws NoSuchAlgorithmException, IOException {
		ChangeFormPermissionRequestResponseTO requestResponseTO = new ChangeFormPermissionRequestResponseTO();
		requestResponseTO.setOrganizationId(protocol.getOrgId());
		requestResponseTO.setUsername(protocol.getUserName());
		requestResponseTO.setPermission(permissionRequest);
		HttpResponse rawResponse = HttpService.callRestAPI(securityPropertiesConf
				.getChangePermission()/* "https://s2demo.sohum.com/service/api/organization/changeFormPermission" */,
				requestResponseTO);
		requestResponseTO = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
				ChangeFormPermissionRequestResponseTO.class);
		return requestResponseTO.getPermission();
	}

}

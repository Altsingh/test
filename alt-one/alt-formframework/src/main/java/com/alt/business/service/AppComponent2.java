package com.alt.business.service;

import com.alt.business.transport.output.CustomAppSetupResponse;
import com.alt.business.util.FileWriterUtil;
import com.alt.business.util.Utility;
import com.alt.datacarrier.business.superadmin.app.*;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.employee.EmployeeRequestTO;
import com.alt.datacarrier.employee.EmployeeResponseTO;
import com.alt.datacarrier.exception.AppRelatedException;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.request.CustomAppSetupRequest;
import com.alt.datacarrier.kernel.uiclass.AltDataTable;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datacarrier.kernel.uiclass.EnumHTMLControl;
import com.alt.datacarrier.organization.*;
import com.alt.datacarrier.workflow.*;
import com.alt.datakernel.dao.IFormDS;
import com.alt.datakernel.dao.impl.AltAppDS;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.model.AltApp;
import com.alt.datakernel.model.AltFormData;
import com.alt.datakernel.model.HomeCards;
import com.alt.datakernel.model.Modules;
import com.alt.datakernel.service.IClassService;
import com.alt.datakernel.service.IFormService;
import com.alt.formengine.config.SecurityPropertiesConf;
import com.alt.formengine.service.HttpService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Component
public class AppComponent2 {

	@Autowired
	AppComponent appComponent;

	@Autowired(required = true)
	private AltAppDS appDS;

	@Autowired
	SecurityPropertiesConf securityPropertiesConf;

	@Autowired
	IFormService formService;

	@Autowired
	private FileWriterUtil fileWriterUtil;

	@Autowired
	IClassService classService;

	@Autowired
	IFormDS formDS;

	private static final Logger logger = Logger.getLogger(AppComponent2.class);

	public Application createApp(AppCERequest request, Protocol protocol) throws NoSuchAlgorithmException, IOException {
		Application application = request.getApp();
		if (!Utility.validateName(application.getAppName())) {
			throw new AppRelatedException("AppName validation failed.");
		}

		if (!Utility.validateAppCode(application.getAppCode())) {
			throw new AppRelatedException("AppCode validation failed.");
		}

		if (application.getIsApplicationForEditing() != null && application.getIsApplicationForEditing()
				&& application.getAppId() != null && !application.getAppId().equalsIgnoreCase("")) {

			AltApp app = AltApp.AltAppBuilder.anAltApp().withAppid(Integer.valueOf(request.getApp().getAppId()))
					.withAppCode(request.getApp().getAppCode()).withAppName(request.getApp().getAppName())
					.withCreatedBy(protocol.getUserName()).withCreatedDate(System.currentTimeMillis())
					.withHomeCards(convertToHomeCardsEntity(request.getApp().getHomeCards())).withIsDeleted(false)
					.withModifiedBy(protocol.getUserName()).withModifiedDate(System.currentTimeMillis())
					.withModules(convertToModulesEntity(request.getApp().getModules()))
					.withAppLogoUrl(application.getAppLogoUrl()).withStatus(KernelConstants.EnumDocStatusType.ACTIVE)
					.withOrganizationId(protocol.getOrgId()).build();
			appDS.edit(app);
		} else {
			AltApp app = AltApp.AltAppBuilder.anAltApp().withAppCode(request.getApp().getAppCode())
					.withAppName(request.getApp().getAppName()).withCreatedBy(protocol.getUserName())
					.withCreatedDate(System.currentTimeMillis())
					.withHomeCards(convertToHomeCardsEntity(request.getApp().getHomeCards())).withIsDeleted(false)
					.withModifiedBy(protocol.getUserName()).withModifiedDate(System.currentTimeMillis())
					.withModules(convertToModulesEntity(request.getApp().getModules()))
					.withStatus(KernelConstants.EnumDocStatusType.ACTIVE).withOrganizationId(protocol.getOrgId())
					.withAppLogoUrl(application.getAppLogoUrl()).build();
			appDS.create(app);
			/**
			 * @author harshvardan.singh
			 * setting app id after successful persist of the app instance.
			 * this appID is utilized for routing the user module creation page.
			 * */
			if(app.getAppid()!=null)
				application.setAppId(app.getAppid().toString());
		}

		/**
		 * If app details successfully persisted in postgres, save details in admin db
		 * as well. Call webservice for this job.
		 */
		publishAppDetailsToAdmin(protocol, application, securityPropertiesConf.getCustomappsetuprequesturl());

		return application;

	}

	public ResponseEntity<String> updateApp(Protocol protocol, Application application)
			throws InstanceRelatedException, JsonProcessingException {

		if (!checkNulls(application)) {
			return new ResponseEntity<>("mandatory fields missing", HttpStatus.BAD_REQUEST);
		}
		if (!Utility.validateName(application.getAppName())) {
			return new ResponseEntity<>("app name validation failed", HttpStatus.BAD_REQUEST);
		}
		if (!validateAppModules(application.getModules())) {
			return new ResponseEntity<>("app module validation failed", HttpStatus.BAD_REQUEST);
		}
		AltApp app = new AltApp();
		app = appDS.find(protocol, app, Integer.parseInt(application.getAppId()));
		app.setAppcode(application.getAppCode());
		app.setAppname(application.getAppName());
		app.setHomecards(new HomeCards(application.getHomeCards()));
		app.setModifiedby(protocol.getUserName());
		app.setModifieddate(System.currentTimeMillis());
		app.setModules(convertToModulesEntity(application.getModules()));
		app.setApplogourl(application.getAppLogoUrl());
		/*
		 * app = AltApp.AltAppBuilder.anAltApp()
		 * .withAppid(Integer.valueOf(application.getAppId()))
		 * .withAppCode(application.getAppCode()) .withAppName(application.getAppName())
		 * .withHomeCards(convertToHomeCardsEntity(application.getHomeCards()))
		 * .withIsDeleted(false) .withModifiedBy(protocol.getUserName())
		 * .withModifiedDate(System.currentTimeMillis())
		 * .withModules(convertToModulesEntity(application.getModules()))
		 * .withStatus(KernelConstants.EnumDocStatusType.ACTIVE)
		 * .withOrganizationId(protocol.getOrgId()) .build();
		 */
		appDS.edit(app);

		return new ResponseEntity<>(HttpStatus.OK);

	}

	private boolean validateAppModules(List<Module> modules) {
		if (modules == null) {
			return false;
		}
		List<String> moduleNames = modules.stream().map(Module::getName).collect(Collectors.toList());
		if (!Utility.checkUniqueStringInList(moduleNames)) {
			return false;
		}
		for (Module module : modules) {
			if (!validateAppMenuGroups(module.getMenuGroups())) {
				return false;
			}

			if (!Utility.validateName(module.getName())) {
				return false;
			}

			if (!validateAppMenus(module.getMenus())) {
				return false;
			}
		}
		return true;
	}

	private boolean validateAppMenus(List<Menu> menus) {
		if (menus == null) {
			return false;
		}
		List<String> menuNames = menus.stream().map(Menu::getName).collect(Collectors.toList());
		if (!Utility.checkUniqueStringInList(menuNames)) {
			return false;
		}
		for (Menu menu : menus) {
			if (!Utility.validateName(menu.getName())) {
				return false;
			}
		}
		return true;
	}

	private boolean validateAppMenuGroups(List<MenuGroups> menuGroups) {
		return true;
	}

	private boolean checkNulls(Application application) {
		if (application.getAppName() == null || application.getAppCode() == null || application.getAppId() == null) {
			return false;
		}
		if (application.getAppName().equals("") || application.getAppCode().equals("")
				|| application.getAppId().equals("")) {
			return false;
		}
		return true;
	}

	private Modules convertToModulesEntity(List<Module> modules) {
		return new Modules(modules);
	}

	private HomeCards convertToHomeCardsEntity(List<HomeBuilderRequestTO> homeCardTOs) {
		if (null != homeCardTOs) {
			return new HomeCards(homeCardTOs);
		} else {
			return null;
		}
	}

	public List<AppData> getApps(Protocol protocol, Integer offset, Integer limit) {
		if (offset == null) {
			offset = -1;
		}

		if (limit == null) {
			limit = -1;
		}

		List<AltApp> appEntityList = appDS.getAppsForOrganization(protocol.getOrgId());

		List<AppData> apps = appEntityList.stream().map(appEntity -> convertToApp(appEntity))
				.collect(Collectors.toList());

		return apps;
	}

	public Application getAppDetails(Protocol protocol, String appId)
			throws NumberFormatException, IOException, NoSuchAlgorithmException {

		Application application = null;
		AltApp app = appDS.find(protocol, new AltApp(), Integer.parseInt(appId));
		List<Module> modules = convertToModuleTOList(app.getModules());
		List<HomeBuilderRequestTO> homeCards = new ArrayList<>();

		if (app.getHomecards() != null) {
			homeCards = app.getHomecards().getHomeCards();
		}

		List<Integer> formIds = new ArrayList<Integer>();
		List<Integer> classIds = new ArrayList<Integer>();
		this.updateRepublishedFormsInMenu(protocol, modules);
		for (Module module : modules) {
			for (Menu menu : module.getMenus()) {
				if (menu.getForm() != null) {
					formIds.add(Integer.parseInt((menu.getForm().getFormId())));
				}
			}
		}
		List<RoleTO> roles = this.getOrganizationRoles(protocol.getOrgId());
		Map<Integer, RoleTO> roleMap = new HashMap<Integer, RoleTO>();
		for (RoleTO role : roles) {
			roleMap.put(role.getRoleId(), role);
		}

		List<FormRolePermissionTO> formPermissions = this.getFormPermissions(protocol.getOrgId());
		Map<Long, List<RoleTO>> formRolePermissionMap = new HashMap<Long, List<RoleTO>>();
		for (FormRolePermissionTO permission : formPermissions) {
			if (!formRolePermissionMap.containsKey(permission.getHrFormId())) {
				formRolePermissionMap.put(permission.getHrFormId(), new ArrayList<RoleTO>());
			}
			formRolePermissionMap.get(permission.getHrFormId()).add(roleMap.get(permission.getRoleId()));
		}

		EmployeeResponseTO employeeInfo = getEmployeeInfo(protocol.getOrgId(), protocol.getUserName());

		try {
			Map<String, AltForm> formMap = new HashMap<String, AltForm>();
			if (!formIds.isEmpty()) {
				List<AltForm> forms = formService.listByIds(protocol, formIds);
				formMap = new HashMap<String, AltForm>();
				for (AltForm form : forms) {
					classIds.add(form.getMetaClassId());
					formMap.put(form.getId(), form);
				}
			}
			for (Module module : modules) {
				module.setPermitted(Boolean.FALSE);
				for (Menu menu : module.getMenus()) {
					menu.setPermitted(Boolean.FALSE);
					if (menu.getForm() != null) {
						AttachedForm form = menu.getForm();
						String formId = form.getFormId();
						if (formMap.containsKey(formId)) {
							form.setFormSecurityId(formMap.get(formId).getFormSecurityId());
							List<RoleTO> assignedRoles = new ArrayList<RoleTO>();
							if (form.getFormSecurityId() != null) {
								Long hrformId = Long.parseLong(form.getFormSecurityId());
								if (formRolePermissionMap.containsKey(hrformId)) {
									assignedRoles.addAll(formRolePermissionMap.get(hrformId));
								}
								form.setAssignedRoles(assignedRoles);
								for (RoleTO assignedRole : form.getAssignedRoles()) {
									if (employeeInfo.getAssignedRoleIds().contains(assignedRole.getRoleId())) {
										module.setPermitted(Boolean.TRUE);
										menu.setPermitted(Boolean.TRUE);
									}
								}
							}
						}
					}
				}
			}

			application = new Application.Builder().withAppCode(app.getAppcode()).withAppId(appId).withAppName(app.getAppname())
					.withModules(modules).withHomeCards(homeCards).withEmployeeInfo(employeeInfo).withClassIds(classIds)
					.withRoles(roles).withAppLogoUrl(app.getApplogourl()).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
        logger.error("Application : "+ application);
		return application;
	}

	public ResponseEntity<ObjectNode> uploadAppLogoImage(MultipartFile multipartFile) {
		String imagePath = null;
		ObjectNode objectNode = null;
		try {
			String writeImageServerPath = securityPropertiesConf.getApplogowriteurl();
			writeImageServerPath += File.separator + "logo";
			imagePath = securityPropertiesConf.getApplogoreadurl() + "/logo";
			String fileName = null;
			fileName = "logo_" + ThreadLocalRandom.current().nextInt(0, 10000 + 1) + new Date().getTime() + "."
					+ FilenameUtils.getExtension(multipartFile.getOriginalFilename());
			imagePath += "/" + fileName;
			fileWriterUtil.writeFile(multipartFile, writeImageServerPath, fileName);
			objectNode = new ObjectMapper().createObjectNode();
			objectNode.put("applogourl", imagePath);
		} catch (Exception e) {
			return new ResponseEntity<ObjectNode>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(objectNode, HttpStatus.OK);
	}

	public UserWorkflowInfo getUserWorkflowInfo(Protocol protocol, List<Integer> contextEntityIds)
			throws NoSuchAlgorithmException, IOException {
		UserWorkflowInfo userWorkflowInfo = new UserWorkflowInfo();
		userWorkflowInfo.setTasks(new ArrayList<TaskTO>());
		userWorkflowInfo.setCards(new ArrayList<WorkflowCardTO>());

		List<TaskTO> approvalTasks = null;
		List<TaskTO> appliedTasks = null;

		TasksRequestTO tasksRequest = new TasksRequestTO();
		tasksRequest.setOrganizationId(protocol.getOrgId());
		tasksRequest.setUsername(protocol.getUserName());
		tasksRequest.setContextEntityIds(contextEntityIds);
		TasksResponseTO tasksResponse = null;
		HttpResponse rawResponse = HttpService.callRestAPI(
				securityPropertiesConf.getWfapi4()/* "https://s2demo.sohum.com/service/api/workflow/tasksInfo" */,
				tasksRequest);
		tasksResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), TasksResponseTO.class);
		approvalTasks = tasksResponse.getTasks();

		userWorkflowInfo.getTasks().addAll(approvalTasks);

		tasksResponse = null;
		rawResponse = HttpService.callRestAPI(
				securityPropertiesConf
						.getWfapi5()/* "https://s2demo.sohum.com/service/api/workflow/appliedTasksInfo" */,
				tasksRequest);
		tasksResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), TasksResponseTO.class);
		appliedTasks = tasksResponse.getTasks();

		Set<Integer> classIds = new HashSet<Integer>();
		SimpleDateFormat sdfIn = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat sdfOut = new SimpleDateFormat("dd/MM/yyyy");
		for (TaskTO task : appliedTasks) {
			classIds.add(task.getEntityId());
			String startDateString = task.getStartDate();
			Date startDate = null;
			try {
				startDate = sdfIn.parse(startDateString);
			} catch (ParseException pe) {
				continue;
			}
			String convertedStartDateString = sdfOut.format(startDate);
			task.setStartDate(convertedStartDateString);
		}
		for (TaskTO task : approvalTasks) {
			classIds.add(task.getEntityId());
			String startDateString = task.getStartDate();
			Date startDate = null;
			try {
				startDate = sdfIn.parse(startDateString);
			} catch (ParseException pe) {
				continue;
			}
			String convertedStartDateString = sdfOut.format(startDate);
			task.setStartDate(convertedStartDateString);
		}

		if (classIds.isEmpty()) {
			return userWorkflowInfo;
		}

		List<ClassMeta> classes = classService.listAll(protocol, classIds);
		Map<String, String> classNameMap = new HashMap<String, String>();
		for (ClassMeta formClass : classes) {
			classNameMap.put(formClass.getId(), formClass.getName());
		}
		Map<String, WorkflowCardTO> cardMap = new TreeMap<String, WorkflowCardTO>();
		for (TaskTO task : appliedTasks) {
			String className = classNameMap.get(new Integer(task.getEntityId()).toString());
			if (!cardMap.containsKey(className)) {
				WorkflowCardTO wfCard = new WorkflowCardTO();

				AppliedWorkflowCardTO appliedCard = new AppliedWorkflowCardTO();
				appliedCard.setAppliedTasks(new ArrayList<TaskTO>());
				appliedCard.setAppliedTaskGroups(new ArrayList<TaskGroupTO>());
				wfCard.setAppliedWorkflowCard(appliedCard);

				ApprovalWorkflowCardTO approvalCard = new ApprovalWorkflowCardTO();
				approvalCard.setApprovalTasks(new ArrayList<TaskTO>());
				wfCard.setApprovalWorkflowCard(approvalCard);

				wfCard.setFormName(className);
				cardMap.put(className, wfCard);
			}
		}
		for (TaskTO task : approvalTasks) {
			String className = classNameMap.get(new Integer(task.getEntityId()).toString());
			if (!cardMap.containsKey(className)) {
				WorkflowCardTO wfCard = new WorkflowCardTO();

				AppliedWorkflowCardTO appliedCard = new AppliedWorkflowCardTO();
				appliedCard.setAppliedTasks(new ArrayList<TaskTO>());
				appliedCard.setAppliedTaskGroups(new ArrayList<TaskGroupTO>());
				wfCard.setAppliedWorkflowCard(appliedCard);

				ApprovalWorkflowCardTO approvalCard = new ApprovalWorkflowCardTO();
				approvalCard.setApprovalTasks(new ArrayList<TaskTO>());
				wfCard.setApprovalWorkflowCard(approvalCard);

				wfCard.setFormName(className);
				cardMap.put(className, wfCard);
			}
		}
		for (TaskTO task : appliedTasks) {
			String className = classNameMap.get(new Integer(task.getEntityId()).toString());
			cardMap.get(className).getAppliedWorkflowCard().getAppliedTasks().add(task);
		}
		for (TaskTO task : approvalTasks) {
			String className = classNameMap.get(new Integer(task.getEntityId()).toString());
			cardMap.get(className).getApprovalWorkflowCard().getApprovalTasks().add(task);
		}

		userWorkflowInfo.getCards().addAll(cardMap.values());

		for (WorkflowCardTO card : userWorkflowInfo.getCards()) {
			card.getAppliedWorkflowCard().setAppliedTaskCount(card.getAppliedWorkflowCard().getAppliedTasks().size());
			card.getApprovalWorkflowCard()
					.setApprovalTaskCount(card.getApprovalWorkflowCard().getApprovalTasks().size());

			AppliedWorkflowCardTO appliedCard = card.getAppliedWorkflowCard();
			List<TaskTO> tasks = appliedCard.getAppliedTasks();
			Map<String, Integer> statusCountMap = new TreeMap<String, Integer>();
			for (TaskTO task : tasks) {
				if (!statusCountMap.containsKey(task.getStatusMessage())) {
					statusCountMap.put(task.getStatusMessage(), 0);
				}
				statusCountMap.put(task.getStatusMessage(), statusCountMap.get(task.getStatusMessage()) + 1);
			}
			List<TaskGroupTO> taskGroups = new ArrayList<TaskGroupTO>();
			for (String status : statusCountMap.keySet()) {
				TaskGroupTO taskGroup = new TaskGroupTO();
				taskGroup.setStatus(status);
				taskGroup.setCount(statusCountMap.get(status));
				taskGroups.add(taskGroup);
			}
			appliedCard.setAppliedTaskGroups(taskGroups);
		}

		return userWorkflowInfo;
	}

	private List<Module> convertToModuleTOList(Modules modules) {
		return modules.getModules();
	}

	private AppData convertToApp(AltApp appEntity) {
		return new AppData.Builder().withAppCode(appEntity.getAppcode()).withAppId(String.valueOf(appEntity.getAppid()))
				.withAppLogoUrl(appEntity.getApplogourl()).withAppName(appEntity.getAppname())
				.withCreatedBy(appEntity.getCreatedby()).withCreatedDate(String.valueOf(appEntity.getCreateddate()))
				.withStatus(appEntity.isStatus())
				.withModules(appEntity.getModules() == null ? null : appEntity.getModules().getModules())
				.withHomeCards(appEntity.getHomecards() == null ? null : appEntity.getHomecards().getHomeCards())
				.build();
	}

	private boolean publishAppDetailsToAdmin(Protocol protocol, Application app, String url)
			throws NoSuchAlgorithmException, IOException {
		Boolean isPersistedInAdmin = false;
		CustomAppSetupResponse customAppSetupResponse = null;
		// MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		CustomAppSetupRequest requestTO = getCustomAppAdminRequestObject(protocol, app);
		HttpResponse rawResponse = HttpService
				.publishAppDetailsToAdmin(url/* "https://s2demo-admin.sohum.com/services/customappsetup" */, requestTO);

		try {
			ObjectMapper mapper = new ObjectMapper();
			customAppSetupResponse = mapper.readValue(rawResponse.getEntity().getContent(),
					CustomAppSetupResponse.class);
			if (customAppSetupResponse != null) {
				if (!customAppSetupResponse.getSuccess()) {
					logger.error("Error while saving App Details in Admin records : CustomAppSetupRestService");
					throw new RuntimeException(
							"Error while saving App Details in Admin records : CustomAppSetupRestService");
				} else {
					isPersistedInAdmin = customAppSetupResponse.getSuccess();
				}
			} else {
				logger.error("Error while saving App Details in Admin records : CustomAppSetupRestService");
				throw new RuntimeException(
						"Error while saving App Details in Admin records : CustomAppSetupRestService");
			}
		} catch (RuntimeException e) {
			logger.error("Error while reading response of form security web service.", e);
			// headers.add("response", "Error while reading response of form security web
			// servicey");
			isPersistedInAdmin = false;
		}
		logger.info("Request sent for form security. Response is {}" + isPersistedInAdmin);

		return isPersistedInAdmin;
	}

	private CustomAppSetupRequest getCustomAppAdminRequestObject(Protocol protocol, Application app) {
		CustomAppSetupRequest requestObj = new CustomAppSetupRequest();
		requestObj.setAppCode(app.getAppCode());
		requestObj.setOrganizationId(protocol.getOrgId());
		requestObj.setTenantId(Integer.parseInt(app.getTenantId()));
		requestObj.setAppName(app.getAppName());
		requestObj.setApplicationForDeletion(
				(app.getIsApplicationForDeletion() == null) ? false : app.getIsApplicationForDeletion());
		requestObj.setApplicationForEditing(
				(app.getIsApplicationForEditing() == null) ? false : app.getIsApplicationForEditing());

		if (app.getAppLogoUrl() == null || app.getAppLogoUrl().equals("")
				|| app.getAppLogoUrl().equalsIgnoreCase("undefined"))
			requestObj.setAppLogoUrl(securityPropertiesConf.getDefaultapplogo());
		else
			requestObj.setAppLogoUrl(app.getAppLogoUrl());

		return requestObj;
	}

	private void updateRepublishedFormsInMenu(Protocol protocol, List<Module> modules) {
		Map<String, AttachedForm> formNameMap = new HashMap<String, AttachedForm>();
		for (Module module : modules) {
			for (Menu menu : module.getMenus()) {
				if (menu.getForm() != null) {
					formNameMap.put(menu.getForm().getFormName(), menu.getForm());
				}
			}
		}
		if (formNameMap.isEmpty())
			return;
		List<AltForm> forms = formService.listByNames(protocol, formNameMap.keySet());
		Map<String, AltForm> latestNameFormMap = new HashMap<String, AltForm>();
		for (AltForm form : forms) {
			if (form.getState().equals(EnumFormState.PUBLISH)) {
				latestNameFormMap.put(form.getUiClassName(), form);
			}
		}
		for (Module module : modules) {
			for (Menu menu : module.getMenus()) {
				if (menu.getForm() != null && latestNameFormMap.containsKey(menu.getForm().getFormName())) {
					Integer latestFormId = Integer
							.parseInt(latestNameFormMap.get(menu.getForm().getFormName()).getId());
					Integer menuFormId = Integer.parseInt(menu.getForm().getFormId());
					if (latestFormId > menuFormId) {
						menu.getForm().setFormId(latestFormId.toString());
					}
				}
			}
		}
	}

	private List<RoleTO> getOrganizationRoles(Integer organizationId) throws NoSuchAlgorithmException, IOException {
		OrganizationRoleRequestTO request = new OrganizationRoleRequestTO();
		request.setOrganizationId(organizationId);
		OrganizationRoleResponseTO response = null;
		HttpResponse rawResponse = HttpService.callRestAPI(
				securityPropertiesConf.getRoles()/* "https://s2demo.sohum.com/service/api/organization/roles" */,
				request);
		response = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), OrganizationRoleResponseTO.class);
		return response.getRoles();
	}

	private List<FormRolePermissionTO> getFormPermissions(Integer organizationId)
			throws NoSuchAlgorithmException, IOException {
		FormPermissionsRequestTO request = new FormPermissionsRequestTO();
		request.setOrganizationId(organizationId);
		FormPermissionsResponseTO response = null;
		HttpResponse rawResponse = HttpService.callRestAPI(
				securityPropertiesConf
						.getPermissions()/* "https://s2demo.sohum.com/service/api/organization/formRolePermissions" */,
				request);
		response = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), FormPermissionsResponseTO.class);
		return response.getPermissions();
	}

	public EmployeeResponseTO getEmployeeInfo(Integer organizationId, String userName) {
		try {
			EmployeeRequestTO employeeRequest = new EmployeeRequestTO();
			employeeRequest.setOrganizationId(organizationId);
			employeeRequest.setUsername(userName);
			EmployeeResponseTO employeeResponse = null;
			HttpResponse rawResponse = HttpService.callRestAPI(
					securityPropertiesConf
							.getEmployeeInfo()/* "https://s2demo.sohum.com/service/api/employee/employeeInfo" */,
					employeeRequest);
			employeeResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
					EmployeeResponseTO.class);
			return employeeResponse;
		} catch (Exception e) {
			return null;
		}
	}

	public FormRolePermissionTO changeFormPermission(Protocol protocol, FormRolePermissionTO permissionRequest)
			throws NoSuchAlgorithmException, IOException {
		ChangeFormPermissionRequestResponseTO requestResponseTO = new ChangeFormPermissionRequestResponseTO();
		requestResponseTO.setOrganizationId(protocol.getOrgId());
		requestResponseTO.setUsername(protocol.getUserName());
		requestResponseTO.setPermission(permissionRequest);
		HttpResponse rawResponse = HttpService.callRestAPI(securityPropertiesConf
				.getChangePermission()/* "https://s2demo.sohum.com/service/api/organization/changeFormPermission" */,
				requestResponseTO);
		requestResponseTO = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
				ChangeFormPermissionRequestResponseTO.class);
		return requestResponseTO.getPermission();
	}

	public AppData getCustomAppDetailsByUrl(String appCode, Integer orgId)
			throws IOException, InstanceRelatedException, QueryRelatedException {
		AltApp app = appDS.getAppByAppCode(appCode, orgId);
		return convertToApp(app);
	}

	@Transactional
    public void migrate(Protocol protocol, String appCode, Integer offset, Integer limit) throws InstanceRelatedException, QueryRelatedException, IOException {
		List<AppData> apps = appComponent.getApps(protocol,offset,limit);
		//apps.forEach(app->appDS.create(convertToAppEntity(app,protocol)));
		List<Application> appsToProcess = apps.stream()
		.filter(app->app.getAppCode().equalsIgnoreCase(appCode))
		.distinct()
		.map(appData -> {try{
							return appComponent.getAppDetails(protocol,appData.getAppId());
						}catch(Exception e) {
							e.printStackTrace();
							return null;
				}}).collect(Collectors.toList());

		appsToProcess.forEach(app -> {
						logger.info("processing app with appcode : "+app.getAppCode());
						AltApp persistedApp = appDS.persistAndFlush(convertToAppEntity(app,protocol));
						if (app!=null && app.getModules() !=null ) {
							app.getModules().stream()
									.filter(module -> module.getMenus()!=null && module.getMenus().size()>0)
									//.map(module -> module.getMenus())
									.flatMap(module-> module.getMenus().stream())
									.forEach(menu -> {
										if(menu!=null && menu.getForm()!=null && menu.getForm().getFormId()!=null) {
											logger.info("processing form with formid : " + menu.getForm().getFormId());
											AltFormData form = formDS.findForm(protocol, new AltFormData(), menu.getForm().getFormId());
											form.setAppid(persistedApp.getAppid());
											form.setModifiedby("Migration api");
											form.setModifieddate(System.currentTimeMillis());
											formDS.editForm(form);
											form.getComponents().getComponents().forEach(component-> {
                                                if (component.getControlType().equals(EnumHTMLControl.DATATABLE)) {
                                                    try {
                                                    	if(null!=((AltDataTable) component).getDbClassRead()){
															logger.info("processing form with metaclassid : " + ((AltDataTable) component).getDbClassRead());
															AltFormData tableForm = formDS.getFormEntityFromMetaClassId(protocol, Integer.valueOf(((AltDataTable) component).getDbClassRead()));
															if(tableForm!=null && persistedApp!=null) {
                                                                tableForm.setAppid(persistedApp.getAppid());
                                                                tableForm.setModifiedby("Migration api");
                                                                tableForm.setModifieddate(System.currentTimeMillis());
                                                                formDS.editForm(tableForm);
                                                            }
														}
                                                    } catch (FormRelatedException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                            );
										}
									});
						}
					});
		}

	private AltApp convertToAppEntity(Application app, Protocol protocol) {

		return  AltApp.AltAppBuilder.anAltApp()
				.withAppLogoUrl(app.getAppLogoUrl())
				.withAppCode(app.getAppCode())
				.withAppName(app.getAppName())
				.withCreatedBy("Migration api")
				.withCreatedDate(System.currentTimeMillis())
				.withIsDeleted(false)
				.withModifiedBy("Migration api")
				.withModifiedDate(System.currentTimeMillis())
				.withHomeCards(new HomeCards(app.getHomeCards()))
				.withModules(new Modules(app.getModules()))
				.withOrganizationId(protocol.getOrgId())
				.withStatus(KernelConstants.EnumDocStatusType.ACTIVE).build();
	}
}

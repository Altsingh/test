package com.alt.business.service;

import com.alt.datacarrier.business.common.AltReadRequest;
import com.alt.datacarrier.business.superadmin.DataReadResponse;
import com.alt.datacarrier.core.*;
import com.alt.datacarrier.employeegroup.common.EmpGroupResponse;
import com.alt.datacarrier.exception.InternalServerError;
import com.alt.datacarrier.exception.SuperAdminException;
import com.alt.datacarrier.kernel.common.*;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//TODO to be refectored and removed

@Service
public class DBService {

    private static final Logger logger = Logger.getLogger(DBService.class);

    /* write/update instance in db */
    public Response<KernelCRUDResponse> saveInstanceForGivenClass(Protocol protocol,
                                                                  List<KernelInstanceProcessRequest> requestData) {
        Request<KernelInstanceProcessRequest> request = new Request<>();
        Response<KernelCRUDResponse> response = null;
        request.setProtocol(protocol);
        request.setRequestData(requestData);
        try {
            //response = dataWriteService.writeInstanceKernel(request);
        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Occurred in Util in method saveInstanceForGivenClass ", e);
            throw new RuntimeException(e);
        }
        return response;
    }

    /*
     * each db class has display units i.e. default display parameters of the
     * class. It fetches the same
     */
    @SuppressWarnings("unchecked")
    public String getDisplayUnitForAttribute(Protocol protocol, String classCode) throws IOException {
        String attributeCode = null;
        ObjectMapper mapper = new ObjectMapper();
        KernelReadDynamicQueryResponse response = null;

        Request<KernelDynamicQueryReadRequest> request = new Request<>();

        KernelDynamicQueryReadRequest kernelDynamicQueryReadRequest = new KernelDynamicQueryReadRequest();
        List<ProjectionAttribute> projectionList = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();

        List<AttributesData> attrList = new ArrayList<>();
        AttributesData attributesData1 = new AttributesData();
        attributesData1.setAttribute("attributes");
        attributesData1.setReference(false);
        attrList.add(attributesData1);

        projectionAttribute.setDbAttribute(attrList);
        projectionList.add(projectionAttribute);

        List<DBFilters> filterList = new ArrayList<>();
        DBFilters dBFilters = new DBFilters();
        dBFilters.setAttribute("id");
        dBFilters.setCondition("=");
        dBFilters.setValue(classCode);
        filterList.add(dBFilters);

        kernelDynamicQueryReadRequest.setProjection(projectionList);
        kernelDynamicQueryReadRequest.setDbFilters(filterList);

        request.setProtocol(protocol);
        List<KernelDynamicQueryReadRequest> dynamicQueryRequestList = new ArrayList<>();
        dynamicQueryRequestList.add(kernelDynamicQueryReadRequest);
        request.setRequestData(dynamicQueryRequestList);

        //response = dataReadService.readDynamicUsingN1QL(request);
        try {
            if (response.getQueryResponse().get(0).getCode() != 200) {
                throw new SuperAdminException("Error reading data for the requested class");

            }
            List<AttributeMeta> objData = (List<AttributeMeta>) response.getQueryResponse().get(0).getResponse().get(0)
                    .get("attributes");
            for (Object obj : objData) {
                HashMap<String, Object> attr = (HashMap<String, Object>) obj;
                if (attr.get("displaySequence") != null) {
                    if (attr.get("displaySequence").equals(1)) {
                        attributeCode = (String) attr.get("attributeCode");
                        break;
                    }
                } else {
                    throw new SuperAdminException("No display units are available for the requested class");
                }
            }
        } catch (Exception e) {

            logger.error("Error Occurred in Util in method getDisplayUnitForAttribute : ", e);
            throw new SuperAdminException(e.getMessage());
        }
        return attributeCode;
    }

    /* write/update class in db */
    public String saveClassForGivenClass(Protocol protocol, List<KernelClassProcessRequest> requestData)
            throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        Request<KernelClassProcessRequest> request = new Request<>();
        Response<KernelCRUDResponse> response = null;
        request.setProtocol(protocol);
        request.setRequestData(requestData);
        try {
            //response = dataWriteService.writeKernel(request);
        } catch (Exception e) {
            logger.error("Error: ", e);
            //response = e.getMessage();
            throw new InternalServerError(e.getMessage());
        }
        return mapper.writeValueAsString(response);
    }

    /* sets response for all business layer group requests */
    public String setResponse(Boolean pass, String msg, String jwt) throws IOException {
        EmpGroupResponse response = new EmpGroupResponse();
        ObjectMapper mapper = new ObjectMapper();
        String responseString = null;
//        if (pass) {
//            response.setData(Constants.RESPONSE_SUCCESS_CODE, Constants.RESPONSE_SUCCESS, msg, jwt);
//        } else {
//            if (msg.equals(Constants.SESSION_TIMEOUT_MESSAGE)) {
//                response.setData(Constants.SESSION_TIMEOUT_CODE, Constants.SESSION_TIMEOUT_MESSAGE, msg, null);
//            } else {
//                response.setData(Constants.RESPONSE_FAIL_CODE, Constants.RESPONSE_FAIL, msg, null);
//            }
//        }
        responseString = mapper.writeValueAsString(response);
        return responseString;
    }

    public Map<String, String> getAttribuuteData(Protocol protocol, String classDocId) {

        KernelCRUDResponse readResponse = new KernelCRUDResponse();
        AltReadRequest req = new AltReadRequest();
        Request<AltReadRequest> readRequest = new Request<>();
        List<AltReadRequest> reqList = new ArrayList<>();
        readRequest.setProtocol(protocol);
        req.setDocIdentifier(classDocId);
        req.setDepth(0);
        req.setLimit(-1);
        req.setOffset(-1);
        req.setType(EnumRequestType.CLASS);
        reqList.add(req);
        readRequest.setRequestData(reqList);
        readResponse = commonReadRequest(readRequest);
        logger.info("Employee group data fetched!!");
        List<AttributeMeta> attributeMetaList = readResponse.getMeta().get(classDocId).getAttributes();
        Map<String, String> attrMap = new HashMap<>();
        for (AttributeMeta attrMeta : attributeMetaList) {
            attrMap.put(attrMeta.getAttributeCode(), attrMeta.getAttributeName());
        }
        return attrMap;
    }

    public Map<String, String> getAllInstanceData(Protocol protocol, String docId) {

        KernelCRUDResponse readResponse = new KernelCRUDResponse();
        AltReadRequest req = new AltReadRequest();
        Request<AltReadRequest> readRequest = new Request<>();
        List<AltReadRequest> reqList = new ArrayList<>();
        readRequest.setProtocol(protocol);
        req.setDocIdentifier(docId);
        req.setDepth(0);
        req.setLimit(-1);
        req.setOffset(-1);
        req.setType(EnumRequestType.BOTH);
        reqList.add(req);
        readRequest.setRequestData(reqList);
        readResponse = commonReadRequest(readRequest);
        logger.info("Employee group data fetched!!");
        List<InstanceData> instanceData = readResponse.getInstance().get(0);
        List<AttributeMeta> attributeMetaList = readResponse.getMeta().get(docId).getAttributes();
        String attrCode = attributeMetaList.stream()
                .filter(meta -> meta.getDisplayUnit() == true && meta.getDisplaySequence() == 1)
                .map(AttributeMeta::getAttributeCode).findAny().orElse(null);
        Map<String, String> attrMap = new HashMap<>();
        // get attr codes for required fields
        for (InstanceData instance : instanceData) {
            attrMap.put(instance.getDocIdentifier(), instance.getAttributes().get(attrCode).getValue());
        }
        return attrMap;
    }

    public String getClassCodeForGivenClassName(Protocol protocol, String docType) throws IOException {
        KernelReadDynamicQueryResponse response = null;
        String docId = null;
        ObjectMapper mapper = new ObjectMapper();
        try {

            Request<KernelDynamicQueryReadRequest> request = new Request<>();
            KernelDynamicQueryReadRequest kernelDynamicQueryReadRequest = new KernelDynamicQueryReadRequest();
            List<ProjectionAttribute> projectionList = new ArrayList<>();
            ProjectionAttribute projectionAttribute = new ProjectionAttribute();

            List<AttributesData> attrList = new ArrayList<>();
            AttributesData attributesData1 = new AttributesData();
            attributesData1.setAttribute("id");
            attributesData1.setReference(false);
            attrList.add(attributesData1);

            projectionAttribute.setDbAttribute(attrList);
            projectionList.add(projectionAttribute);

            List<DBFilters> filterList = new ArrayList<>();
            DBFilters dBFilters1 = new DBFilters();
            dBFilters1.setAttribute("name");
            dBFilters1.setCondition("=");
            dBFilters1.setValue(docType);
            filterList.add(dBFilters1);

            kernelDynamicQueryReadRequest.setProjection(projectionList);
            kernelDynamicQueryReadRequest.setDbFilters(filterList);

            request.setProtocol(protocol);
            List<KernelDynamicQueryReadRequest> dynamicQueryRequestList = new ArrayList<>();
            dynamicQueryRequestList.add(kernelDynamicQueryReadRequest);
            request.setRequestData(dynamicQueryRequestList);

           // response = dataReadService.readDynamicUsingN1QL(request);

            if (response.getQueryResponse().get(0).getCode() == 200) {
                List<Map<String, Object>> res = response.getQueryResponse().get(0).getResponse();
                if (res.size() > 0) {
                    Map<String, Object> id = res.stream().filter(
                            resMap -> resMap.get("id").toString().split("-")[1].split("#")[0].equals(protocol.getOrgCode()))
                            .findFirst()
                            .orElse(res.stream()
                                    .filter(resMap -> resMap.get("id").toString().split("-")[1].split("#")[0].equals("SYSTEMORG"))
                                    .findFirst().orElse(null));
                    docId = (String) id.get("id");
                }
            } else {
                throw new SuperAdminException("package code retrival failed");
            }
        } catch (Exception e) {
            logger.error("Error occured", e);
            throw new SuperAdminException(e.getMessage());

        }
        return docId;
    }

    public DataReadResponse getClassNameAndId(Protocol protocol, IndexingData indexingData, String pkgDocId)
            throws IOException {
        Request<KernelDynamicQueryReadRequest> request = new Request<>();
        ObjectMapper mapper = new ObjectMapper();
        KernelReadDynamicQueryResponse response = null;
        KernelDynamicQueryReadRequest kernelDynamicQueryReadRequest = new KernelDynamicQueryReadRequest();
        List<ProjectionAttribute> projectionList = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        DataReadResponse readResponse = new DataReadResponse();

        List<AttributesData> attrList = new ArrayList<>();
        AttributesData attributesData1 = new AttributesData();
        attributesData1.setAttribute("id");
        attributesData1.setReference(false);
        AttributesData attributesData2 = new AttributesData();
        attributesData2.setAttribute("name");
        attributesData2.setReference(false);
        AttributesData attributesData3 = new AttributesData();
        attributesData3.setAttribute("status");
        attributesData3.setReference(false);
        AttributesData attributesData4 = new AttributesData();
        attributesData4.setAttribute("packageCodes");
        attributesData4.setReference(false);
        AttributesData attributesData5 = new AttributesData();
        attributesData5.setAttribute("systemType");
        attributesData5.setReference(false);
        attrList.add(attributesData1);
        attrList.add(attributesData2);
        attrList.add(attributesData3);
        attrList.add(attributesData4);
        attrList.add(attributesData5);

        projectionAttribute.setDbAttribute(attrList);
        projectionList.add(projectionAttribute);

        if (!(pkgDocId == null || pkgDocId.equals(""))) {
            List<UnnestAttribute> unnestList = new ArrayList<>();
            UnnestAttribute unestAttribute = new UnnestAttribute();
            unestAttribute.setUnNestArrayName("packageCodes");
            unestAttribute.setUnNestAlias("pkg");
            unnestList.add(unestAttribute);
            kernelDynamicQueryReadRequest.setUnnest(unnestList);
        }

        List<DBFilters> filterList = new ArrayList<>();
        DBFilters dBFilters = new DBFilters();
        dBFilters.setAttribute("type");
        dBFilters.setCondition("=");
        dBFilters.setValue("CLASS");
        filterList.add(dBFilters);

        if (!(pkgDocId == null || pkgDocId.equals(""))) {
            DBFilters dBFilters2 = new DBFilters();
            dBFilters2.setAttribute("pkg");
            dBFilters2.setCondition("=");
            dBFilters2.setValue(pkgDocId);
            dBFilters2.setUnnest(true);
            filterList.add(dBFilters2);

            List<String> filterJoin = new ArrayList<>();
            filterJoin.add("AND");

            kernelDynamicQueryReadRequest.setFilterJoinOperator(filterJoin);
        }

        if (indexingData.getOrderBy() != null) {
            OrderAttribute orderAttribute1 = new OrderAttribute();
            orderAttribute1.setDbAttr(indexingData.getOrderBy());
            orderAttribute1.setSortDirection(indexingData.getOrder());
            List<OrderAttribute> orderList = new ArrayList<>();
            orderList.add(orderAttribute1);
            kernelDynamicQueryReadRequest.setOrderBy(orderList);
        }
        if (indexingData.getLimit() != null) {
            kernelDynamicQueryReadRequest.setLimit(indexingData.getLimit());
        }
        if (indexingData.getOffset() != null) {
            kernelDynamicQueryReadRequest.setOffset(indexingData.getOffset());
        }
        kernelDynamicQueryReadRequest.setDocumentCountNeeded(true);
        kernelDynamicQueryReadRequest.setProjection(projectionList);
        kernelDynamicQueryReadRequest.setDbFilters(filterList);

        request.setProtocol(protocol);
        List<KernelDynamicQueryReadRequest> dynamicQueryRequestList = new ArrayList<>();
        dynamicQueryRequestList.add(kernelDynamicQueryReadRequest);
        request.setRequestData(dynamicQueryRequestList);

       // response = dataReadService.readDynamicUsingN1QL(request);
        try {

            if (response.getQueryResponse().get(0).getCode() == 200) {
                readResponse.setResponse(response.getQueryResponse().get(0).getResponse());
                readResponse.setDoccumentCount(response.getQueryResponse().get(0).getDocumentCount());
            }
        } catch (Exception e) {
            logger.error("Error occured: ", e);
            throw new InternalServerError(e.getMessage());
        }
        return readResponse;
    }

    public DataReadResponse getInstanceData(Protocol protocol, String docId, boolean getMeta, IndexingData indexingData)
            throws IOException {

        Request<KernelDynamicQueryReadRequest> request = new Request<>();
        ObjectMapper mapper = new ObjectMapper();
        String dataType = null;
        KernelReadDynamicQueryResponse response = null;
        KernelDynamicQueryReadRequest kernelDynamicQueryReadRequest = new KernelDynamicQueryReadRequest();
        List<ProjectionAttribute> projectionList = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        DataReadResponse readResponse = new DataReadResponse();

        List<AttributesData> attrList = new ArrayList<>();
        AttributesData attributesData1 = new AttributesData();
        attributesData1.setAttribute("id");
        attributesData1.setReference(false);
        AttributesData attributesData2 = new AttributesData();
        attributesData2.setAttribute("attributes");
        attributesData2.setReference(false);
        AttributesData attributesData3 = new AttributesData();
        attributesData3.setAttribute("status");
        attributesData3.setReference(false);
        AttributesData attributesData4 = new AttributesData();
        attributesData4.setAttribute("type");
        attributesData4.setReference(false);
        AttributesData attributesData5 = new AttributesData();
        attributesData5.setAttribute("createdBy");
        attributesData5.setReference(false);
        AttributesData attributesData6 = new AttributesData();
        attributesData6.setAttribute("createdDate");
        attributesData6.setReference(false);

        attrList.add(attributesData1);
        attrList.add(attributesData2);
        attrList.add(attributesData3);
        attrList.add(attributesData4);
        attrList.add(attributesData5);
        attrList.add(attributesData6);

        projectionAttribute.setDbAttribute(attrList);
        projectionList.add(projectionAttribute);

        List<DBFilters> filterList = new ArrayList<>();
        DBFilters dBFilter1 = new DBFilters();
        dBFilter1.setAttribute("classCode");
        dBFilter1.setCondition("=");
        dBFilter1.setValue(docId);
        filterList.add(dBFilter1);
        if (getMeta) {
            dataType = "CLASS";
        } else {
            dataType = "INSTANCE";
        }
        DBFilters dBFilter2 = new DBFilters();
        dBFilter2.setAttribute("type");
        dBFilter2.setCondition("=");
        dBFilter2.setValue(dataType);
        filterList.add(dBFilter2);

        List<String> filterJoin = new ArrayList<>();
        filterJoin.add("AND");

        kernelDynamicQueryReadRequest.setFilterJoinOperator(filterJoin);
        kernelDynamicQueryReadRequest.setProjection(projectionList);
        kernelDynamicQueryReadRequest.setDbFilters(filterList);
        kernelDynamicQueryReadRequest.setCasNeeded(true);
        if (!getMeta) {
            if (indexingData.getOrderBy() != null) {
                OrderAttribute orderAttribute1 = new OrderAttribute();
                orderAttribute1.setDbAttr(indexingData.getOrderBy());
                orderAttribute1.setSortDirection(indexingData.getOrder());
                List<OrderAttribute> orderList = new ArrayList<>();
                orderList.add(orderAttribute1);
                kernelDynamicQueryReadRequest.setOrderBy(orderList);
            }
            if (indexingData.getOffset() != null) {
                kernelDynamicQueryReadRequest.setOffset(indexingData.getOffset());
            }
            if (indexingData.getLimit() != null) {
                kernelDynamicQueryReadRequest.setLimit(indexingData.getLimit());
            }
            kernelDynamicQueryReadRequest.setDocumentCountNeeded(true);
        }
        request.setProtocol(protocol);
        List<KernelDynamicQueryReadRequest> dynamicQueryRequestList = new ArrayList<>();
        dynamicQueryRequestList.add(kernelDynamicQueryReadRequest);
        request.setRequestData(dynamicQueryRequestList);

        //response = dataReadService.readDynamicUsingN1QL(request);

        readResponse.setResponse(response.getQueryResponse().get(0).getResponse());
        readResponse.setDoccumentCount(response.getQueryResponse().get(0).getDocumentCount());


        return readResponse;
    }

    public KernelCRUDResponse getInstance(Protocol protocol, String docId) {

        AltReadRequest req = new AltReadRequest();
        Request<AltReadRequest> readRequest = new Request<>();
        List<AltReadRequest> reqList = new ArrayList<>();
        KernelCRUDResponse altResponse = new KernelCRUDResponse();
        readRequest.setProtocol(protocol);
        req.setDocIdentifier(docId);
        req.setDepth(0);
        req.setLimit(0);
        req.setOffset(0);
        req.setType(EnumRequestType.INSTANCE);
        reqList.add(req);
        readRequest.setRequestData(reqList);
        altResponse = commonReadRequest(readRequest);

        return altResponse;
    }

    public KernelCRUDResponse getAllInstances(Protocol protocol, String docId, int offset, int limit) {

        AltReadRequest req = new AltReadRequest();
        Request<AltReadRequest> readRequest = new Request<>();
        List<AltReadRequest> reqList = new ArrayList<>();
        KernelCRUDResponse altResponse = new KernelCRUDResponse();
        readRequest.setProtocol(protocol);
        req.setDocIdentifier(docId);
        req.setDepth(0);
        req.setLimit(limit);
        req.setOffset(offset);
        req.setType(EnumRequestType.INSTANCE);
        reqList.add(req);
        readRequest.setRequestData(reqList);
        altResponse = commonReadRequest(readRequest);

        return altResponse;
    }

    public String getAllInstances(Protocol protocol, String classId, EnumRequestType type) {
        ObjectMapper mapper = new ObjectMapper();
        AltReadRequest req = new AltReadRequest();
        Request<AltReadRequest> readRequest = new Request<>();
        List<AltReadRequest> reqList = new ArrayList<>();
        KernelCRUDResponse altResponse = new KernelCRUDResponse();
        String response = null;
        try {
            logger.info("Fetching group meta from db for");
            readRequest.setProtocol(protocol);
            req.setDocIdentifier(classId);
            req.setDepth(0);
            req.setLimit(10);
            req.setOffset(0);
            req.setType(type);
            reqList.add(req);
            readRequest.setRequestData(reqList);
            altResponse = commonReadRequest(readRequest);
            if (type.equals(EnumRequestType.INSTANCE)) {
                response = mapper.writeValueAsString(altResponse.getInstance().get(0));
            } else if (type.equals(EnumRequestType.CLASS)) {
                response = mapper.writeValueAsString(altResponse.getMeta().get(classId));
            } else {
                response = mapper.writeValueAsString(altResponse);

            }

        } catch (Exception e) {
            logger.error("Error occured:", e);
            throw new InternalServerError(e.getMessage());
        }
        return response;
    }

    public KernelCRUDResponse commonReadRequest(Request<AltReadRequest> altRequestData) {
        Request<KernelSingleReadRequest> requestList = new Request<>();
        List<KernelSingleReadRequest> requestData = new ArrayList<>();
        for (AltReadRequest altReadRequest : altRequestData.getRequestData()) {
            KernelSingleReadRequest readRequest = new KernelSingleReadRequest();
            readRequest.setDepth(altReadRequest.getDepth());
            readRequest.setDocIdentifier(altReadRequest.getDocIdentifier());
            readRequest.setLimit(altReadRequest.getLimit());
            readRequest.setOffset(altReadRequest.getOffset());
            readRequest.setType(altReadRequest.getType());
            requestData.add(readRequest);
        }
        requestList.setRequestData(requestData);
        requestList.setProtocol(altRequestData.getProtocol());
        KernelCRUDResponse response = null; 
        //dataReadService.readKernelForAllDocsN1QLWithDepth(requestList);
        try {
            if (response.getCode() != "200") {
                throw new RuntimeException("data read failed");
            }

        } catch (RuntimeException e) {
            logger.log(Level.FATAL, "Error Occurred in Util in method commonReadRequest ", e);
            throw new RuntimeException(e);
        }
        return response;
    }
}

package com.alt.business.service;

import java.io.IOException;

import org.springframework.stereotype.Service;
@Service
public interface ISuperadminService {

	String fetchAll(String request) throws IOException;

	String createUpdateDelete(String request) throws IOException;

	String getCreateInstanceData(String request) throws IOException;

}

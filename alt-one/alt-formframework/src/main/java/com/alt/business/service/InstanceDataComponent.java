package com.alt.business.service;

import com.alt.business.constants.Constants;
import com.alt.datacarrier.business.superadmin.ClassMasterData;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.db.core.*;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.model.Instance;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.datakernel.service.IInstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Component
public class InstanceDataComponent {

    @Autowired
    IDynamicQuery dynamicQuery;

    @Autowired
    IInstanceService instanceService;

    public ResponseEntity<List<InstanceData>> getInstancesData(Protocol protocol, String classId, Integer offset, Integer limit) throws QueryRelatedException {
        if (offset == null)
            offset = -1;
        if (limit == null)
            limit = -1;
        DynamicReadRequest request = new DynamicReadRequest();
        createDynamicQueryRequest(request, protocol.getOrgCode(), classId, offset, limit, protocol.getOrgId());
        DynamicReadResponse response = dynamicQuery.read(request);
        List<Object[]> instances = response.getResponse();
        List<InstanceData> details = new ArrayList<>();
        for (Object[] instance : instances) {
            InstanceData detail = new InstanceData((Integer) instance[0], (String) instance[1], (String) instance[2], ((BigInteger) instance[3]).longValue(), (String) instance[4], KernelConstants.EnumDocStatusType.values()[(Integer) instance[5]]);
            details.add(detail);
        }
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Response", "Instance Data listing fetched successfully");
        return new ResponseEntity<>(details, headers, HttpStatus.OK);

    }

    public ResponseEntity<List<InstanceData>> getInstancesDataNew(Protocol protocol, String className, Integer offset, Integer limit) throws QueryRelatedException {
        ReadRequest readRequest = new ReadRequest();
        readRequest.setProtocol(protocol);
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        List<InstanceData> data;
        readRequest.setName(className);
        try {
            data = instanceService.readClassInstances(readRequest);
        } catch (InstanceRelatedException e) {
            e.printStackTrace();
            headers.add("Response", "Internal Server Error");
            return new ResponseEntity<>(headers, HttpStatus.OK);
        }
        headers.add("Response", "Instance Data fetched successfully");
        return new ResponseEntity<>(data, headers, HttpStatus.OK);
    }


    public ResponseEntity<InstanceData> getInstanceData(Protocol protocol, Integer instanceId) {
        InstanceData instanceData = null;
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        try {
            instanceData = instanceService.read(protocol, instanceId);
        } catch (InstanceRelatedException e) {
            e.printStackTrace();
            headers.add("Response", "Internal Server Error");
            return new ResponseEntity<>(headers, HttpStatus.OK);
        }
        headers.add("Response", "Instance Data fetched successfully");
        return new ResponseEntity<>(instanceData, headers, HttpStatus.OK);

    }

    public ResponseEntity<InstanceData> createInstance(Protocol protocol, InstanceData instanceData) throws InstanceHistoryRelatedException {
        InstanceRequest instanceRequest = new InstanceRequest.Builder().withClassName(instanceData.getClassName())
                .withAttributes(instanceData.getAttributes()).withProtocol(protocol)
                .withStatus(instanceData.getStatus()).withInstanceId(instanceData.getInstanceId())
                .withProcessHistory(true)
                .build();
        InstanceData result = null;
        try {
            result = instanceService.create(instanceRequest);
        } catch (InstanceRelatedException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (ClassRelatedException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    public ResponseEntity<InstanceData> updateInstance(Protocol protocol, InstanceData instanceData) throws InstanceHistoryRelatedException {
        InstanceRequest instanceRequest = new InstanceRequest.Builder().withClassName(instanceData.getClassName())
                .withInstanceId(instanceData.getInstanceId())
                .withAttributes(instanceData.getAttributes()).withProtocol(protocol)
                .withStatus(instanceData.getStatus()).withInstanceId(instanceData.getInstanceId())
                .withProcessHistory(true)
                .build();
        InstanceData result = null;
        try {
            result = instanceService.create(instanceRequest);
        } catch (InstanceRelatedException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (ClassRelatedException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    private void createDynamicQueryRequest(DynamicReadRequest request, String orgCode, String classId, Integer offset, Integer limit, Integer orgId) {
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute attribute = new ProjectionAttribute();
        attribute.setDbClass("instance");
        List<AttributesData> dbAttributes = new ArrayList<>();
        AttributesData data1 = new AttributesData();
        data1.setAttribute("id");
        data1.setAs("Id");
        dbAttributes.add(data1);
        AttributesData data2 = new AttributesData();
        data2.setAttribute("classname");
        data2.setAs("ClassName");
        dbAttributes.add(data2);

        AttributesData data3 = new AttributesData();
        data3.setAttribute("createdby");
        data3.setAs("CreatedBy");
        dbAttributes.add(data3);

        AttributesData data4 = new AttributesData();
        data4.setAttribute("createddate");
        data4.setAs("CreatedDate");
        dbAttributes.add(data4);

        AttributesData data5 = new AttributesData();
        data5.setAttribute("status");
        data5.setAs("Status");
        dbAttributes.add(data5);

        attribute.setDbAttributes(dbAttributes);
        projections.add(attribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("orgid");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue(orgCode);
        dbFilters.add(filter1);
        DbFilters filter2 = new DbFilters();
        filter2.setAttribute("organizationid ");
        filter2.setClassName("instance");
        filter2.setCondition("=");
        filter2.setValue(orgId.toString());
        dbFilters.add(filter2);
        if (!classId.equals("-1")) {
            filter1 = new DbFilters();
            filter1.setAttribute("classid");
            filter1.setClassName("instance");
            filter1.setCondition("=");
            filter1.setValue(classId);
            dbFilters.add(filter1);
        }
        request.setDbFilters(dbFilters);
        request.setLimit(limit);
        request.setOffset(offset);

    }

}

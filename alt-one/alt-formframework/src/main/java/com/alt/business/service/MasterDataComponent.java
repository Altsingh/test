package com.alt.business.service;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import com.alt.datacarrier.business.superadmin.ClassCEResponse;
import com.alt.datacarrier.business.superadmin.ClassMasterData;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributesData;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.core.DbFilters;
import com.alt.datacarrier.kernel.db.core.DynamicReadRequest;
import com.alt.datacarrier.kernel.db.core.DynamicReadResponse;
import com.alt.datacarrier.kernel.db.core.ProjectionAttribute;
import com.alt.datacarrier.kernel.db.request.CreateClassRequest;
import com.alt.datacarrier.workflow.ProtocolTO;
import com.alt.datacarrier.workflow.StageRequestTO;
import com.alt.datacarrier.workflow.StageResponseTO;
import com.alt.datakernel.dao.IFormDS;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.service.IClassService;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.datakernel.service.impl.ClassService;
import com.alt.formengine.config.SecurityPropertiesConf;
import com.alt.formengine.service.HttpService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class MasterDataComponent {

	@Autowired
	IDynamicQuery dynamicQuery;

	@Autowired
	IClassService classService;

	@Autowired
	SecurityPropertiesConf securityPropertiesConf;

	@Autowired
	IFormDS formDS;

	private Logger LOGGER = LoggerFactory.getLogger(ClassService.class);

	public ResponseEntity<List<ClassMasterData>> getMasterData(Protocol protocol, Integer offset, Integer limit)
			throws QueryRelatedException {

		if (offset == null)
			offset = -1;
		if (limit == null)
			limit = -1;
		DynamicReadRequest request = new DynamicReadRequest();
		createDynamicQueryRequest(request, protocol.getOrgCode(), offset, limit, protocol.getOrgId());
		DynamicReadResponse response = dynamicQuery.read(request);
		List<Object[]> instances = response.getResponse();
		List<ClassMasterData> details = new ArrayList<>();
		for (Object[] instance : instances) {
			ClassMasterData detail = new ClassMasterData(Integer.toString((Integer) instance[0]), (String) instance[1],
					(String) instance[2], ((BigInteger) instance[3]).toString(), (String) instance[4],
					Integer.toString((Integer) instance[5]));
			details.add(detail);
		}
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Response", "Class Data listing fetched successfully");
		return new ResponseEntity<>(details, headers, HttpStatus.OK);

	}

	public ResponseEntity<ClassCEResponse> getMasterDataById(Protocol protocol, Integer classId) {
		ClassMeta data = null;
		ClassCEResponse response = null;
		try {
			response = getClassAttributes(protocol, classId);

		} catch (ClassRelatedException e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	private ClassCEResponse getClassAttributes(Protocol protocol, Integer classId) throws ClassRelatedException {
		ClassMeta data;
		data = classService.read(protocol, classId);
		ClassCEResponse response = new ClassCEResponse.Builder().withAttributesDataList(data.getAttributes())
				.withClassId(data.getId()).withClassname(data.getName()).withInstanceLimiter(data.getInstanceLimiter())
				.withInstancePrefix(data.getInstancePrefix())
				.withIsCustomAttributeAllowed(data.getCustomAttributeAllowed())
				.withLocationSpecific(data.getLocationSpecific()).withOrg(protocol.getOrgCode())
				.withPackagecodes(data.getPackageCodes()).withParameterType(data.getParameterType())
				.withStatus(data.getStatus()).withSystemType(data.getSystemType()).withCreatedBy(data.getCreatedBy())
				.withCreatedDate(Long.valueOf((Long) data.getCreatedDate()).toString())
				.withModifiedBy(data.getModifiedBy())
				.withModifiedDate(Long.valueOf((Long) data.getModifiedDate()).toString()).build();
		return response;
	}

	public ResponseEntity<List<String>> getNestedClassAttributesById(Protocol protocol, Integer classId)
			throws FormRelatedException, IOException, NoSuchAlgorithmException {
		List<String> response = null;
		try {
			response = getNestedClassAttributes(protocol, classId, "");

			String formSecurityIdString = formDS.getFormEntityFromMetaClassId(protocol, classId).getFormsecurityid();

			if (formSecurityIdString != null)
				response.addAll(getWorkflowAttributes(Long.parseLong(formSecurityIdString)));

		} catch (ClassRelatedException e) {
			e.printStackTrace();
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	private List<String> getWorkflowAttributes(Long formSecurityId) throws IOException, NoSuchAlgorithmException {
		List<String> workflowAttributes = new ArrayList<>();
		StageRequestTO stageRequest = new StageRequestTO();
		stageRequest.setHrFormId(formSecurityId);
		stageRequest.setStageId(null);
		stageRequest.setCustomFlag(true);
		stageRequest.setProtocolTO(new ProtocolTO());
		StageResponseTO stageResponse = null;
		HttpResponse rawResponse = HttpService.callRestAPI(
				securityPropertiesConf
						.getWfapi1()/* "https://s2demo.sohum.com/service/api/workflow/currentStageInfo" */,
				stageRequest);
		try {
			stageResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), StageResponseTO.class);
		} catch (Exception e) {
			LOGGER.error("Error while reading response of workflow web service.", e);
		}
		if (stageResponse.getWorkflowStageId() != null && stageResponse.getWorkflowStageId() != 0) {
			workflowAttributes.add("Stage");
			workflowAttributes.add("Status");
		}
		return workflowAttributes;
	}

	private List<String> getNestedClassAttributes(Protocol protocol, Integer classId, String prefix)
			throws ClassRelatedException {
		List<String> attributes;
		ClassMeta data = classService.read(protocol, classId);
		attributes = data.getAttributes().stream().filter(attributeMeta -> attributeMeta.getReferenceClassId() != null
				&& attributeMeta.getReferenceClassId() != 0).map(attributeMeta -> {
					try {
						return getNestedClassAttributes(protocol, attributeMeta.getReferenceClassId(),
								attributeMeta.getAttributeName() + "..");
					} catch (ClassRelatedException e) {
						LOGGER.error(
								"Failed to get nested attributes for " + prefix + attributeMeta.getAttributeName());
					}
					return new ArrayList<String>();
				}).flatMap(a -> a.stream()).map(attributeName -> prefix + attributeName).collect(Collectors.toList());
		List<String> nonReferenceAttributes = data.getAttributes().stream()
				.filter(attributeMeta -> attributeMeta.getReferenceClassId() == null
						|| attributeMeta.getReferenceClassId() == 0)
				.map(attributeMeta -> prefix + attributeMeta.getAttributeName()).collect(Collectors.toList());
		attributes.addAll(nonReferenceAttributes);

		if (data.getTaskCodeRequired() != null && data.getTaskCodeRequired()) {
			attributes.add(new String("taskCode"));
		}

		return attributes;
	}

	public ResponseEntity<ClassCEResponse> createClassMasterData(Protocol protocol, CreateClassRequest request) {
		CreateClassRequest classRequest = new CreateClassRequest.Builder().withClassName(request.getClassName())
				.withAttributes(request.getAttributes()).withCustomAttributeAllowed(request.getCustomAttributeAllowed())
				.withInstanceLimiter(request.getInstanceLimiter()).withInstancePrefix(request.getInstancePrefix())
				.withLocationSpecific(request.getLocationSpecific()).withParameterType(request.getParameterType())
				.withPackageCodes(request.getPackageCodes()).withProtocol(protocol).withStatus(request.getStatus())
				.withSystemType(request.getSystemType()).build();

		request.setProtocol(protocol);
		com.alt.datacarrier.kernel.db.core.ClassMeta data = null;
		ClassCEResponse response = null;
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		try {
			data = classService.create(classRequest);
			if (data != null) {
				response = new ClassCEResponse.Builder().withAttributesDataList(data.getAttributes())
						.withClassId(data.getId()).withClassname(data.getName())
						.withInstanceLimiter(data.getInstanceLimiter()).withInstancePrefix(data.getInstancePrefix())
						.withIsCustomAttributeAllowed(data.getCustomAttributeAllowed())
						.withLocationSpecific(data.getLocationSpecific()).withOrg(protocol.getOrgCode())
						.withPackagecodes(data.getPackageCodes()).withParameterType(data.getParameterType())
						.withStatus(data.getStatus()).withSystemType(data.getSystemType())
						.withCreatedBy(data.getCreatedBy())
						.withCreatedDate(Long.valueOf((Long) data.getCreatedDate()).toString())
						.withModifiedBy(data.getModifiedBy())
						.withModifiedDate(Long.valueOf((Long) data.getModifiedDate()).toString()).build();
			}
		} catch (ClassRelatedException e) {
			e.printStackTrace();

			headers.add("Response", "Same Class Name Already Exist");
			return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
		}
		headers.add("Response", "Class Created Succesfully");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	private void createDynamicQueryRequest(DynamicReadRequest request, String orgCode, Integer offset, Integer limit,
			Integer orgId) {
		List<ProjectionAttribute> projections = new ArrayList<>();
		ProjectionAttribute attribute = new ProjectionAttribute();
		attribute.setDbClass("class");
		List<AttributesData> dbAttributes = new ArrayList<>();
		AttributesData data1 = new AttributesData();
		data1.setAttribute("classid");
		data1.setAs("Id");
		dbAttributes.add(data1);
		AttributesData data2 = new AttributesData();
		data2.setAttribute("classname");
		data2.setAs("ClassName");
		dbAttributes.add(data2);

		AttributesData data3 = new AttributesData();
		data3.setAttribute("createdby");
		data3.setAs("CreatedBy");
		dbAttributes.add(data3);

		AttributesData data4 = new AttributesData();
		data4.setAttribute("createddate");
		data4.setAs("CreatedDate");
		dbAttributes.add(data4);

		AttributesData data5 = new AttributesData();
		data5.setAttribute("packagecodes");
		data5.setAs("PackageCodes");
		dbAttributes.add(data5);

		AttributesData data6 = new AttributesData();
		data6.setAttribute("status");
		data6.setAs("Status");
		dbAttributes.add(data6);

		attribute.setDbAttributes(dbAttributes);
		projections.add(attribute);
		request.setProjections(projections);
		request.setFromDbClass("class");
		List<DbFilters> dbFilters = new ArrayList<>();
		/*
		 * DbFilters filter1 = new DbFilters(); filter1.setAttribute("org");
		 * filter1.setClassName("class"); filter1.setCondition("=");
		 * filter1.setValue(orgCode); dbFilters.add(filter1);
		 */
		DbFilters filter2 = new DbFilters();
		filter2.setAttribute("orgid");
		filter2.setClassName("class");
		filter2.setCondition("=");
		filter2.setValue(orgId.toString());
		dbFilters.add(filter2);
		request.setDbFilters(dbFilters);
		request.setLimit(limit);
		request.setOffset(offset);

	}

	public ResponseEntity<ClassCEResponse> updateClassMasterData(Protocol protocol, CreateClassRequest request) {

		request.setProtocol(protocol);
		com.alt.datacarrier.kernel.db.core.ClassMeta data = null;
		ClassCEResponse response = null;
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		try {
			// need to call update method
			data = classService.update(request);
			if (data != null) {
				response = new ClassCEResponse.Builder().withAttributesDataList(data.getAttributes())
						.withClassId(data.getId()).withClassname(data.getName())
						.withInstanceLimiter(data.getInstanceLimiter()).withInstancePrefix(data.getInstancePrefix())
						.withIsCustomAttributeAllowed(data.getCustomAttributeAllowed())
						.withLocationSpecific(data.getLocationSpecific()).withOrg(protocol.getOrgCode())
						.withPackagecodes(data.getPackageCodes()).withParameterType(data.getParameterType())
						.withStatus(data.getStatus()).withSystemType(data.getSystemType())
						.withCreatedBy(data.getCreatedBy())
						.withCreatedDate(Long.valueOf((Long) data.getCreatedDate()).toString())
						.withModifiedBy(data.getModifiedBy())
						.withModifiedDate(Long.valueOf((Long) data.getModifiedDate()).toString()).build();
			}
		} catch (ClassRelatedException e) {
			e.printStackTrace();

			headers.add("Response", "Same Class Name Already Exist");
			return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
		}
		headers.add("Response", "Class Updated Succesfully");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	public List<String> getReferenceAttributes(Protocol protocol, Map<String, String> references) {
		List<String> referenceAttributes = new ArrayList<>();
		getReferenceAttributes(protocol, references, referenceAttributes);
		return referenceAttributes;
	}

	private void getReferenceAttributes(Protocol protocol, Map<String, String> references,
			List<String> referenceAttributes) {
		references.forEach((k, v) -> {
			ClassMeta data = null;
			data = getAttributes(protocol, Integer.valueOf(v));
			getDeepReferences(protocol, referenceAttributes, k, data);
		});
	}

	private List<String> getDeepReferences(Protocol protocol, List<String> referenceAttributes, String k,
			ClassMeta data) {
		if (null != data) {
			referenceAttributes
					.addAll(data.getAttributes().stream().filter(attr -> StringUtils.isEmpty(attr.getReferenceClass()))
							.map(attr -> k + ".." + attr.getAttributeName()).collect(Collectors.toList()));
			referenceAttributes.addAll(data.getAttributes().stream()
					.filter(attr -> !StringUtils.isEmpty(attr.getReferenceClass()))
					.map(attr -> getDeepReferences(protocol, Collections.singletonList(attr.getReferenceAttr()),
							k + ".." + attr.getAttributeName(), getAttributes(protocol, attr.getReferenceClassId())))
					.flatMap(x -> x.stream()).collect(Collectors.toList()));
		}
		return referenceAttributes;
	}

	private ClassMeta getAttributes(Protocol protocol, Integer referenceClassId) {
		try {
			return classService.read(protocol, referenceClassId);
		} catch (ClassRelatedException e) {
			LOGGER.error("Unable to read attributes for classid:" + referenceClassId, e);
		}
		return null;
	}
}

package com.alt.business.service;

import com.alt.datacarrier.business.superadmin.CreateNewDocSuperadminRequest;
import com.alt.datacarrier.business.superadmin.DataReadResponse;
import com.alt.datacarrier.business.superadmin.FetchAllSuperadminRequest;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.Response;
import com.alt.datacarrier.exception.BadRequestException;
import com.alt.datacarrier.kernel.common.*;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class SuperadminServiceImpl {

	@Autowired(required = true)
	DBService dbService;

	private static final Logger logger = Logger.getLogger(SuperadminServiceImpl.class);

	public String fetchAll(FetchAllSuperadminRequest requestData) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String response = null;
		DataReadResponse readResponse = null;
		String jwt = null;
		String docId = null;


		switch (requestData.getType()) {
		case CLASS:
			readResponse = dbService.getClassNameAndId(requestData.getProtocol(), requestData.getIndexingData(),
					requestData.getDocId());
			if (readResponse == null) {
				throw new BadRequestException("Data read failed");
			}
			response = dbService.setResponse(true, mapper.writeValueAsString(readResponse), null);
			break;
		case PACKAGE:
			docId = dbService.getClassCodeForGivenClassName(requestData.getProtocol(), "Package");
			readResponse = dbService.getInstanceData(requestData.getProtocol(), docId, false,
					requestData.getIndexingData());
			if (readResponse == null) {
				throw new BadRequestException("Data read failed");
			}
			response = dbService.setResponse(true, mapper.writeValueAsString(readResponse), null);

			break;
		case INSTANCE:
			readResponse = dbService.getInstanceData(requestData.getProtocol(), requestData.getDocId(), false,
					requestData.getIndexingData());
			if (readResponse == null) {
				throw new BadRequestException("Data read failed");
			}
			response = dbService.setResponse(true, mapper.writeValueAsString(readResponse), null);
			break;
		case CLASSMETA:
			response = dbService.getAllInstances(requestData.getProtocol(), requestData.getDocId(),
					EnumRequestType.CLASS);
			if (response == null) {
				throw new BadRequestException("Data read failed");
			}
			response = dbService.setResponse(true, response, null);
			break;
		case PACKAGEMETA:
			docId = dbService.getClassCodeForGivenClassName(requestData.getProtocol(), "Package");
			response = dbService.getAllInstances(requestData.getProtocol(), docId, EnumRequestType.CLASS);
			if (response == null) {
				throw new BadRequestException("Data read failed");
			}
			response = dbService.setResponse(true, response, null);
			break;
		}

		return response;
	}

	public String createUpdateDelete(CreateNewDocSuperadminRequest requestData) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String response = null;
		String jwt = null;
		String docId;
		
		// jwt verification
		switch (requestData.getType()) {
		case CLASS:
			response = crudClass(requestData.getProtocol(), requestData.getKernelClassProcessRequest(), jwt);
			break;
		case PACKAGE:
			docId = dbService.getClassCodeForGivenClassName(requestData.getProtocol(), "Package");
			requestData.getInstanceData().getInstanceData().setClassCode(docId);
			response = crudInstance(requestData.getProtocol(), requestData.getInstanceData(), jwt);
			break;
		case INSTANCE:
			response = crudInstance(requestData.getProtocol(), requestData.getInstanceData(), jwt);
			break;
		default:
			break;
		}
		return response;
	}

	public String crudInstance(Protocol protocol, KernelInstanceProcessRequest instanceData, String jwt)
			throws IOException {
		Response<KernelCRUDResponse> response = null;
		String stringResponse = null;
		try {
			if (instanceData.getAction().equals(EnumCRUDOperations.CREATE)) {
				instanceData.getInstanceData().setCreatedDate(System.currentTimeMillis());
				instanceData.getInstanceData().setCreatedBy(protocol.getUserName());
			} else {
				instanceData.getInstanceData().setModifiedDate(System.currentTimeMillis());
				instanceData.getInstanceData().setModifiedBy(protocol.getUserName());
			}
			instanceData.getInstanceData().setType(EnumRequestType.INSTANCE);
			instanceData.getInstanceData().setStatus(EnumDocStatusType.ACTIVE);
			List<KernelInstanceProcessRequest> reqList = new ArrayList<>();
			reqList.add(instanceData);
			response = dbService.saveInstanceForGivenClass(protocol, reqList);
			if (response.getResponseCode().equals("Success-100")) {
				//stringResponse = dbService.setResponse(true, Constants.RESPONSE_SUCCESS, jwt);
			} else {
				stringResponse = dbService.setResponse(false, response.getErrMsg(), null);
			}
		} catch (Exception e) {
			logger.error("Error:", e);

			//throw new BadRequestException(Constants.RESPONSE_UNEXPECTED_FAIL);

		}
		return stringResponse;
	}

	private String crudClass(Protocol protocol, KernelClassProcessRequest classProcessRequest, String jwt)
			throws IOException {

		String response = null;
		try {
			if (classProcessRequest.getAction().equals(EnumCRUDOperations.SOFT_DELETE)) {
				classProcessRequest.getClassData().setStatus(EnumDocStatusType.INACTIVE);
			} else {
				classProcessRequest.getClassData().setStatus(EnumDocStatusType.ACTIVE);
			}
			/*****************************/
			KernelClassProcessData processData = new KernelClassProcessData();
			processData = classProcessRequest.getClassData();
			processData.setInstanceLimiter(KernelConstants.EnumInstanceLimiter.NONE);
			processData.setCustomAttributeAllowed(false);
			processData.setLocationSpecific(false);
			processData.setParameterType(false);
			processData.setReadLock(false);
			List<String> packageCode = new ArrayList<>();
            packageCode.add("abcd");
			processData.setPackageCodes(packageCode);
			classProcessRequest.setClassData(processData);


			/*****************************/
			List<KernelClassProcessRequest> reqList = new ArrayList<>();
			reqList.add(classProcessRequest);
			response = dbService.saveClassForGivenClass(protocol, reqList);
			JSONObject obj = new JSONObject(response);
			if (obj.get("responseCode").equals("Success-100")) {
				//response = dbService.setResponse(true, Constants.RESPONSE_SUCCESS, jwt);
			} else {
				response = dbService.setResponse(false, (String) obj.get("errMsg"), null);
			}
		} catch (Exception e) {
			logger.error("Error:", e);
			//throw new BadRequestException(Constants.RESPONSE_UNEXPECTED_FAIL);

		}
		return response;
	}

}

package com.alt.business.util;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author vaibhav.kashyap
 * */

public class EncryptionDecryptionUtil {

	static byte[] ivKey = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

	/**
	 * {@link #prepareDataForEncryption(HashMap)}
	 * @param HashMap
	 * 
	 * */
	public static String prepareDataForEncryption(HashMap<String, String> data) {
		if (data == null || data.isEmpty())
			return null;
		String str = "";
		for (Map.Entry<String, String> entry : data.entrySet()) {
			str = str + entry.getKey() + "=" + entry.getValue() + "$";
		}

		str = str.substring(0, str.length() - 1);
		return str;
	}

	public static String getEncryptedData(String dataString) {
		try {
			/*SecretKeySpec skeySpec = new SecretKeySpec(properties.getString("cipherkey").getBytes("UTF-8"),
					"AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			IvParameterSpec iv = new IvParameterSpec(ivKey); // ECB
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
			byte[] encrypted = cipher.doFinal(dataString.getBytes());
			System.out.println("encrypted string: " + Base64.getEncoder().encodeToString(encrypted));*/
			byte[] encrypted = dataString.getBytes();
			return Base64.getEncoder().encodeToString(encrypted);
		} catch (Exception ex) {
			ex.getStackTrace();
		}

		return null;

	}
}

package com.alt.business.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileWriterUtil {

	private static final Logger logger = Logger.getLogger(FileWriterUtil.class);

	/*public String writeFileToServer(Integer partnerId, Integer appId, String imageField, MultipartFile mutiPartFile,
			String returnedReadPath, String writePath) throws IOException {

		String partnerAppImagePath = writePath + File.separator + "partner" + File.separator + partnerId
				+ File.separator + appId;
		returnedReadPath = returnedReadPath + File.separator + "partner";
		String fileName = null;

		if (imageField.equals("screenShots")) {
			fileName = new Date().getTime() + "_" + mutiPartFile.getOriginalFilename();
			partnerAppImagePath = partnerAppImagePath + File.separator + "screenShots";
			returnedReadPath += "/" + partnerId + "/" + appId + "/" + "/screenShots/" + fileName;

		} else {
			fileName = new Date().getTime() + "_" + imageField + "."
					+ FilenameUtils.getExtension(mutiPartFile.getOriginalFilename());
			returnedReadPath += "/" + partnerId + "/" + appId + "/" + fileName;
		}

		writeFile(mutiPartFile, partnerAppImagePath, fileName);

		return returnedReadPath;
	}*/

	public void writeFile(MultipartFile multiPartFile, String appImagePath, String fileName) {

		java.nio.file.Path imagePath;
		createDirectory(appImagePath);
		logger.debug("filename : "+fileName);
		logger.debug("appImagePath : "+appImagePath);
		imagePath = Paths.get(appImagePath).resolve(fileName);
		logger.debug("imagepath : "+imagePath.toString());
		try {
			Files.copy(multiPartFile.getInputStream(), imagePath, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			logger.debug("Could not upload image", e);
		}

	}

	private void createDirectory(String appImagePath) {
		java.nio.file.Path directoryPath = Paths.get(appImagePath);

		if (!Files.isDirectory(directoryPath))
			try {
				Files.createDirectories(directoryPath);
			} catch (IOException e) {
				logger.debug("Error Occurred in FileWriterUtil in method createDirectory ", e);
			}

	}

}

package com.alt.business.util;

import java.io.IOException;

import com.alt.business.controller.AppResource;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alt.business.conf.LoginPropertyConf;


/**
 * @author vaibhav.kashyap
 */


@Component
public class Keycloak {

	@Autowired
	LoginPropertyConf loginPropertyConf;
	
	@Autowired
	TokenDecryptionUtil tokenDecryptionUtil;
	
	private static final String USERNAME = "preferred_username";

	private static final Logger logger = Logger.getLogger(Keycloak.class);

	/**
	 * {@link #getUsernameFromUserinfoEndpoint(String, String)} : fetched userName
	 * using the accessToken</br>
	 * & realm ID from keycloak server.
	 * 
	 * @param AcessToken,
	 *            Realm ID
	 * @return UserName
	 * @author vaibhav.kashyap
	 */
	public String getUsernameFromUserinfoEndpoint(String accessToken, String realm) {
		if (realm == null || realm.equalsIgnoreCase(""))
			return null;

		String keycloakUrl = loginPropertyConf.getKeycloak_url();
		String userInfoEndpoint = keycloakUrl + "auth/realms/" + realm + "/protocol/openid-connect/userinfo";
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(userInfoEndpoint);
		httpGet.addHeader("Accept", "application/json");
		httpGet.addHeader("Authorization", "bearer " + accessToken);
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			String resp = EntityUtils.toString(entity);
			logger.warn("Response from keycloak: "+resp);
			JSONObject json = new JSONObject(resp);
			EntityUtils.consume(entity);
			return json.getString(USERNAME);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					response.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * {@link #getOrganizationId(String)} : returns organization ID from
	 * accessToken.
	 * 
	 * @return OrganizationID
	 * @author vaibhav.kashyap
	 */
	public String getOrganizationId(String accessToken) {
		try {
			// calling accessToken decrptor
			TokenDecryptionUtil token = tokenDecryptionUtil.decryptAccessToken(accessToken);
			if (token != null && token.iss != null && !token.iss.equalsIgnoreCase("")) {
				String str[] = token.iss.split("/");
				return str[str.length - 1]; // reurning organization ID
			}
		} catch (Exception ex) {
			ex.getStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * {@link #getAppUrl(String)} : returns URL to called after all the processing.
	 * 
	 * @return Destination Url
	 * @author vaibhav.kashyap
	 */
	public String getAppUrl(String requestUrl) {
		if (requestUrl == null || requestUrl.equalsIgnoreCase(""))
			return null;
		String[] str = requestUrl.split(":");
		return "https:" + str[1] + loginPropertyConf.getAngularportcontext() + loginPropertyConf.getDatacontext();
	}

}

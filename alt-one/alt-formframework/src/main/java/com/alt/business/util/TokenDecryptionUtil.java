package com.alt.business.util;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author vaibhav.kashyap
 */
@Component
public class TokenDecryptionUtil {

	/**
	 * Contains the auth url with other details
	 */
	public String iss;

	/**
	 * {@link #decryptAccessToken(String)} : decrypts accessToken
	 * 
	 * @return TokenDecryptionUtil
	 * @author vaibhav.kashyap
	 */
	public TokenDecryptionUtil decryptAccessToken(String accessToken) throws Exception {
		try {
			if (accessToken != null && !accessToken.equalsIgnoreCase("")) {
				String b64payload = accessToken.split("\\.")[1];
				String jsonString = new String(Base64.decodeBase64(b64payload), "UTF-8");

				return new Gson().fromJson(jsonString, TokenDecryptionUtil.class);
			}

		} catch (Exception ex) {

		}
		return null;
	}

	/**
	 * {@link #toString()} : return string form of Gson object
	 * 
	 * @return String
	 * @author vaibhav.kashyap
	 */
	public String toString() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
}

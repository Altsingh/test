package com.alt.business.util;

import java.util.HashSet;
import java.util.List;


public abstract class Utility {

    /* checks a string for alphanumeric */
	public static boolean isAlphanumeric(String str) {
		if (str == null) {
			return false;
		} else if (str.trim().equals("")) {
			return false;
		}
		str = str.trim();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c)) {
				if (Character.isWhitespace(c))
					continue;
				return false;
			}
		}
		return true;
	}
    
    public static boolean isCharInputOnly(String str) {
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (!Character.isLetter(c))
                return false;
        }
        return true;
    }

    public static boolean isOfCorrectCharacterLength(String name, int lowerLimit, int upperLimit) {
        if (name.length() > lowerLimit && name.length() < upperLimit)
            return true;
        else
            return false;

    }

    public static boolean isNotEmpty(String name) {
        if (name == null || name.isEmpty())
            return false;
        else
            return true;

    }

    public static boolean validateName(String name) {
        if (Utility.isNotEmpty(name)) {
            return (isAlphanumeric(name) && isOfCorrectCharacterLength(name, 0, 50));
        }
        return false;
    }
    
	public static boolean validateAppCode(String code) {
		boolean flag = true;
		if (code == null) {
			return false;
		} else if (code.trim().equals("")) {
			return false;
		}
		code = code.trim();
		for (int i = 0; i < code.length(); i++) {
			char c = code.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c)) {
				return false;
			}
		}
		
		flag = isOfCorrectCharacterLength(code, 0, 15);
		return flag;

	}

    public static boolean checkUniqueStringInList(List<String> names) {
        HashSet<String> set = new HashSet<>();
        for (String name : names) {
            if (set.isEmpty()) {
                set.add(name);
            } else {
                if (set.contains(name)) {
                    return false;
                } else {
                    set.add(name);
                }
            }
        }
        return true;
    }
}

package com.alt.formengine.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("kafka")
public class KafkaConfig {
	private String groupid;
	private String topicprefix;
	private String bootstrapservers;

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	public String getTopicprefix() {
		return topicprefix;
	}

	public void setTopicprefix(String topicprefix) {
		this.topicprefix = topicprefix;
	}

	public String getBootstrapservers() {
		return bootstrapservers;
	}

	public void setBootstrapservers(String bootstrapservers) {
		this.bootstrapservers = bootstrapservers;
	}

}

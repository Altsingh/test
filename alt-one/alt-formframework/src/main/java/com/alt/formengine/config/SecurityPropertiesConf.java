package com.alt.formengine.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("form")
public class SecurityPropertiesConf {

	private String url;
	private String customappsetuprequesturl;
	private String applogowriteurl;
	private String applogoreadurl;
	private String wfapi1;
	private String wfapi2;
	private String wfapi3;
	private String wfapi4;
	private String defaultapplogo;
	private String wfapi5;
	private String employeeInfo;
	private String roles;
	private String permissions;
	private String changePermission;
	private String formsecurity;
	private String communication;
	private String reportingEmployees;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCustomappsetuprequesturl() {
		return customappsetuprequesturl;
	}

	public void setCustomappsetuprequesturl(String customappsetuprequesturl) {
		this.customappsetuprequesturl = customappsetuprequesturl;
	}

	public String getApplogowriteurl() {
		return applogowriteurl;
	}

	public void setApplogowriteurl(String applogowriteurl) {
		this.applogowriteurl = applogowriteurl;
	}

	public String getWfapi1() {
		return wfapi1;
	}

	public void setWfapi1(String wfapi1) {
		this.wfapi1 = wfapi1;
	}

	public String getWfapi2() {
		return wfapi2;
	}

	public void setWfapi2(String wfapi2) {
		this.wfapi2 = wfapi2;
	}

	public String getWfapi3() {
		return wfapi3;
	}

	public void setWfapi3(String wfapi3) {
		this.wfapi3 = wfapi3;
	}

	public String getWfapi4() {
		return wfapi4;
	}

	public void setWfapi4(String wfapi4) {
		this.wfapi4 = wfapi4;
	}

	public String getApplogoreadurl() {
		return applogoreadurl;
	}

	public void setApplogoreadurl(String applogoreadurl) {
		this.applogoreadurl = applogoreadurl;
	}

	public String getDefaultapplogo() {
		return defaultapplogo;
	}

	public void setDefaultapplogo(String defaultapplogo) {
		this.defaultapplogo = defaultapplogo;
	}

	public String getWfapi5() {
		return wfapi5;
	}

	public void setWfapi5(String wfapi5) {
		this.wfapi5 = wfapi5;
	}

	public String getEmployeeInfo() {
		return employeeInfo;
	}

	public void setEmployeeInfo(String employeeInfo) {
		this.employeeInfo = employeeInfo;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getPermissions() {
		return permissions;
	}

	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}

	public String getChangePermission() {
		return changePermission;
	}

	public void setChangePermission(String changePermission) {
		this.changePermission = changePermission;
	}

	public String getFormsecurity() {
		return formsecurity;
	}

	public void setFormsecurity(String formsecurity) {
		this.formsecurity = formsecurity;
	}

	public String getCommunication() {
		return communication;
	}

	public void setCommunication(String communication) {
		this.communication = communication;
	}

	public String getReportingEmployees() {
		return reportingEmployees;
	}

	public void setReportingEmployees(String reportingEmployees) {
		this.reportingEmployees = reportingEmployees;
	}

}

package com.alt.formengine.constant;

public final class FormConstants {
	
	private FormConstants(){}

	public static final String UI_CLASSCODE = "UiClassCode";
	public static final String COMPONENTID= "ComponentId";
	public static final String ROLE = "Role";
	public static final String STAGE = "Stage";
	public static final String EMPLOYEEGROUP = "EmployeeGroup";
	public static final String ORGANIZATION = "Organization";
	public static final String USERID = "UserId";
	public static final String SECURITY_TYPE= "securityType";
	public static final String SECURITY_LEVEL_TYPE = "securityLevelType";
	
	public static final String LANGUAGE="Language";
	public static final String LANGUAGE_CODE="LanguageCode";
	public static final String LOCALE="Locale";
	public static final String LOCALE_CODE="LocaleCode";
	
	public static final String BUNDLE="Bundle";
	public static final String RESOURCE_MAP="Resource";
	
}

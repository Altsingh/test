package com.alt.formengine.constant;


public class MenuConstants{

	public static final String HEADER_ORG = "organization";
	public static final String HEADER_MODULE = "module";
	public static final String RESPONSE_SUCCESS = "Success";
	public static final Integer RESPONSE_SUCCESS_CODE = 400;
	public static final String RESPONSE_FAIL = "FAIL";
	public static final Integer RESPONSE_FAIL_CODE = 200;
	public static final String FAILURE_MSG_BADREQUEST = "Bad request data";
	public static final String FAILURE_MSG_DBREADERROR = "Error reading data from database";
	public static final String FAILURE_MSG_MENUCREATE = "Error creating new menu";
	public static final String RESPONSE_UNEXPECTED_FAIL = "Unexpected error has occurred.";
	public static final Integer SESSION_TIMEOUT_CODE = 500;
	public static final String SESSION_TIMEOUT_MESSAGE = "SESSION_TIMEOUT";
	
	public static final String MENU_CLASS_NAME = "Menu";
	public static final String MENURESOURCE_CLASS_NAME = "MenuResource";
	public static final String WEB_SOURCE_NAME = "web";
	public static final String IOS_SOURCE_NAME = "ios";
	public static final String ANDROID_SOURCE_NAME = "android";
	public static final String ANDROID_ICON = "androidIcon";
	
	public static final String RESOURCE_LABEL = "resource";
	public static final String PACKAGE_LABEL = "package";
	public static final String NAME_LABEL = "name";
	public static final String LABEL_LABEL = "label";
	public static final String PARENTMENU_LABEL = "parentMenu";
	public static final String UICLASS_LABEL = "uiClass";
	public static final String MENUTYPE_LABEL = "menuType";
	public static final String MOBILE_SEQUENCE_LABEL = "mobileSequence";
	public static final String IOS_ICON_LABEL = "iosIcon";
	public static final String WEB_ICON_LABEL = "webIcon";
	public static final String WEB_SEQUENCE_LABEL = "webSequence";
	public static final String PARENTMENU_SEQUENCE = "1";
	public static final String UICLASSCODE_LABEL = "uiClassCode";
	public static final String BUNDLE_LABEL = "bundle";
	public static final String ATTRIBUTES_LABEL = "attributes";
	public static final String VALUE_LABEL = "value";
	public static final String DOCID_LABEL = "id";
	
}

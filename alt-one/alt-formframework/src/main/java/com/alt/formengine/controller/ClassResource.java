package com.alt.formengine.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.formframework.response.ClassAttributesResponse;
import com.alt.datacarrier.formframework.response.ClassIdAndNameResponse;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.service.IFormService;
import com.alt.formengine.service.ClassComponent;
import com.alt.formengine.util.JwtUtil;

@RestController
@RequestMapping("api/v1/classes")
public class ClassResource {

	@Autowired
	ClassComponent classComponent;

	@Autowired
	IFormService formService;

	@GetMapping
	public ResponseEntity<List<ClassIdAndNameResponse>> getClasses(
			@RequestHeader(value = "authToken", required = false) String authToken,
			@RequestParam(value = "id", required = false) boolean id,
			@RequestParam(value = "name", required = false) boolean name,
			@RequestParam(value = "attributes", required = false) boolean attributes,
			@RequestParam(value = "limit", required = false, defaultValue = "500") int limit,
			@RequestParam(value = "offset", required = false, defaultValue = "-1") int offset,
			@RequestParam(value = "appId", required = false) Integer appId) throws QueryRelatedException {

		Protocol protocol = JwtUtil.validateJwt(authToken);

		ResponseEntity<List<ClassIdAndNameResponse>> response = classComponent
				.getClasses(JwtUtil.validateJwt(authToken), id, name, attributes, limit, offset);

		try {
			List<AltForm> appForms = formService
					.readAll(new ReadRequest(protocol, null, -1, -1, -1, null, null, null, appId));
			Set<Integer> appClassIds = new HashSet<Integer>();
			appForms.forEach(appForm -> appClassIds.add(appForm.getMetaClassId()));

			Iterator<ClassIdAndNameResponse> iterator = response.getBody().iterator();
			while (iterator.hasNext()) {
				if (!appClassIds.contains(Integer.parseInt(iterator.next().getId())))
					iterator.remove();
			}
		} catch (Exception e) {

		}

		return response;
	}

	@GetMapping("/{classId}")
	public ResponseEntity<ClassAttributesResponse> getAltClass(@RequestHeader(value = "authToken") String authToken,
			@PathVariable("classId") String classId,
			@RequestParam(value = "attributes", required = false) boolean attributes)
			throws NumberFormatException, ClassRelatedException {
		return classComponent.getAltClass(JwtUtil.validateJwt(authToken), attributes, classId);
	}
}

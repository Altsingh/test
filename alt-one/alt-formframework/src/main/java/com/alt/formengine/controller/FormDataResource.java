package com.alt.formengine.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alt.datacarrier.formframework.request.ExecutorAjaxInstanceTransporter;
import com.alt.datacarrier.formframework.request.FormDataSaveRequest;
import com.alt.datacarrier.formframework.response.FormCreateResponse;
import com.alt.datacarrier.kernel.db.core.InstanceData;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datakernel.dao.IInstanceDS;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.exception.RuleRelatedException;
import com.alt.datakernel.exception.TaskCodeRecordRelatedException;
import com.alt.datakernel.service.IInstanceService;
import com.alt.formengine.exception.FormDataRelatedException;
import com.alt.formengine.service.FormDataComponent;
import com.alt.formengine.util.JwtUtil;

@RestController
@RequestMapping("/api/v1/forms1")
public class FormDataResource {

	@Autowired
	FormDataComponent formDataComponent;

	@Autowired(required = true)
	IInstanceDS instanceDS;
	
	@Autowired(required = true)
	IInstanceService instanceService;

	@PostMapping
	//@CrossOrigin
	public ResponseEntity<FormCreateResponse> saveFormData(@RequestHeader(value = "authToken") String authToken,
			@RequestBody FormDataSaveRequest formDataSaveRequest)
			throws FormDataRelatedException, InstanceRelatedException, ClassRelatedException, QueryRelatedException,
			FormRelatedException, ClientProtocolException, RuleRelatedException, IOException, ClassCastException,
			ParseException, TaskCodeRecordRelatedException {

		return formDataComponent.saveFormData(formDataSaveRequest, JwtUtil.validateJwt(authToken));
	}
	
	@PostMapping("/getInstancesByClassId")
	public ResponseEntity<List<InstanceData>> getInstancesByClassId(
			@RequestBody ExecutorAjaxInstanceTransporter transporter) {

		ReadRequest request = new ReadRequest();
		request.setProtocol(JwtUtil.validateJwt(transporter.getAuthToken()));

		// In the following case, id is classId
		if (transporter.getId() != null)
			request.setId(transporter.getId());

		if (transporter.getClassName() != null)
			request.setClassName(transporter.getClassName());

		if (transporter.getAttributes() != null && transporter.getAttributes().size() > 0) {
			request.setAttributes(transporter.getAttributes());
		}

		List<InstanceData> data = null;
		try {
			data = instanceService.readClassInstancesById(request);
		} catch (InstanceRelatedException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<List<InstanceData>>(data, HttpStatus.OK);
	}
}

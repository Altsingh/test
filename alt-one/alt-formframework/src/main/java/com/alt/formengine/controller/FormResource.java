package com.alt.formengine.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alt.datacarrier.formframework.request.CommEnvlp;
import com.alt.datacarrier.formframework.request.DataTableOnDemand;
import com.alt.datacarrier.formframework.request.DownloadDataTableRequestResponse;
import com.alt.datacarrier.formframework.request.ExecutorAjaxInstanceTransporter;
import com.alt.datacarrier.formframework.request.FormCRUDData;
import com.alt.datacarrier.formframework.request.FormCreateRequest;
import com.alt.datacarrier.formframework.request.FormDataRetrieveRequest;
import com.alt.datacarrier.formframework.request.FormInstanceFetchRequest;
import com.alt.datacarrier.formframework.request.FormInstanceUpdateRequest;
import com.alt.datacarrier.formframework.request.FormUpdateRequest;
import com.alt.datacarrier.formframework.request.GlobalTextfieldReloadRequestResponse;
import com.alt.datacarrier.formframework.request.ReloadLocalDataTableRequestResponse;
import com.alt.datacarrier.formframework.request.TestAjaxResponse;
import com.alt.datacarrier.formframework.request.UploadDataTableRequestResponse;
import com.alt.datacarrier.formframework.response.FormCreateResponse;
import com.alt.datacarrier.formframework.response.FormDetails;
import com.alt.datacarrier.formframework.response.FormInstanceResponse;
import com.alt.datacarrier.kernel.uiclass.AltDataTable;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.MailRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.exception.ReferenceRelatedException;
import com.alt.formengine.exception.FormDataRelatedException;
import com.alt.formengine.service.FormComponent;
import com.alt.formengine.util.JwtUtil;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("/api/v1/forms")
//@CrossOrigin
public class FormResource {

	String newFormVersion = "V1";

	@Autowired
	FormComponent formComponent;

	private static final Logger logger = org.apache.log4j.Logger.getLogger(FormResource.class);

	@GetMapping("/test")
	public ResponseEntity<String> getTest() {
		return new ResponseEntity<>("Hello World", HttpStatus.OK);
	}

	@GetMapping
	//@CrossOrigin
	public ResponseEntity<List<FormDetails>> getForms(@RequestHeader(value = "authToken") String authToken,
			@RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
			@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
			@RequestParam(value = "published", required = false, defaultValue = "0") boolean published,
			@RequestParam(value = "appId", required = false, defaultValue = "0") int appId)
			throws QueryRelatedException {
		return formComponent.getForms(JwtUtil.validateJwt(authToken), limit, offset, published, appId);
	}

	@GetMapping("/{formId}")
	// @CrossOrigin
	public ResponseEntity<FormCRUDData> getForm(@RequestHeader(value = "authToken") String authToken, @PathVariable("formId") String docId,
												@RequestParam(value = "productId",required = false)String productId,
												@RequestParam(value = "tenantId", required = false) String tenantId)
			throws FormRelatedException, QueryRelatedException, ClassRelatedException, NoSuchAlgorithmException,
			IOException, InstanceRelatedException {
		return formComponent.getForm(new FormDataRetrieveRequest(tenantId, productId, docId, null, null),
				JwtUtil.validateJwt(authToken));

	}

	@PostMapping("/getForm")
	// @CrossOrigin
	public ResponseEntity<FormCRUDData> getForm(@RequestHeader(value = "authToken") String authToken,
			@RequestBody FormDataRetrieveRequest formDataRetrieveRequest)
			throws FormRelatedException, QueryRelatedException, ClassRelatedException, NoSuchAlgorithmException,
			IOException, InstanceRelatedException {
		return formComponent.getForm(formDataRetrieveRequest, JwtUtil.validateJwt(authToken));
	}

	@GetMapping("/deleteinstance/{instanceId}")
	 //@CrossOrigin
	public ResponseEntity<FormCreateResponse> deleteInstance(@RequestHeader(value = "authToken") String authToken,
			@PathVariable("instanceId") String instanceId)
			throws QueryRelatedException, InstanceRelatedException, InstanceHistoryRelatedException {
		return formComponent.deleteInstance(instanceId, JwtUtil.validateJwt(authToken));

	}

	@GetMapping("/admin/{securityId}")
	 //@CrossOrigin
	public ResponseEntity<FormCRUDData> getFormFromSecurityId(
			@RequestHeader(value = "tenantId", required = false) String tenantId,
			@RequestHeader(value = "authToken") String authToken, @PathVariable("securityId") String docId)
			throws FormRelatedException, QueryRelatedException, ClassRelatedException, InstanceRelatedException {
		logger.log(Level.FATAL, "get request for form for single form");

		return formComponent.getFormFromSecurityId(tenantId, docId, JwtUtil.validateJwt(authToken));
	}

	@PostMapping
	 //@CrossOrigin
	public ResponseEntity<FormCreateResponse> createForm(@RequestHeader(value = "authToken") String authToken,
			@RequestBody FormCreateRequest formCreateRequest) throws FormRelatedException, InstanceRelatedException,
			ClassRelatedException, InstanceHistoryRelatedException {
		return formComponent.createFormDraft(formCreateRequest, JwtUtil.validateJwt(authToken), newFormVersion);

	}

	@PutMapping
	 //@CrossOrigin
	public ResponseEntity<FormCreateResponse> updateFormDraft(@RequestHeader(value = "authToken") String authToken,
			@RequestBody FormUpdateRequest formUpdateRequest)
			throws FormRelatedException, IOException, NoSuchAlgorithmException, QueryRelatedException,
			InstanceRelatedException, ClassRelatedException, InstanceHistoryRelatedException {
		return formComponent.updateFormDraft(formUpdateRequest, JwtUtil.validateJwt(authToken));
	}

	@PostMapping("/publish")
	 //@CrossOrigin
	public ResponseEntity<FormCreateResponse> publishForm(@RequestHeader(value = "authToken") String authToken,
			@RequestBody FormUpdateRequest formUpdateRequest) throws FormRelatedException, IOException,
			NoSuchAlgorithmException, QueryRelatedException, InstanceRelatedException, ClassRelatedException {
		return formComponent.publishForm(formUpdateRequest.getUiClassCode(), formUpdateRequest.getUiClassName(),
				formUpdateRequest.getTaskCodeEnabled(), JwtUtil.validateJwt(authToken));
	}

	@PostMapping("/submitFormMeta")
	 //@CrossOrigin
	public ResponseEntity<MultiValueMap> submitFormMeta(@RequestHeader(value = "authToken") String authToken,
			@RequestBody FormUpdateRequest formUpdateRequest)
			throws FormRelatedException, IOException, NoSuchAlgorithmException, QueryRelatedException {
		return formComponent.sendFormMetaForSecurity(formUpdateRequest, JwtUtil.validateJwt(authToken));
	}

	@PostMapping("/submitForm")
	 //@CrossOrigin
	public ResponseEntity<FormInstanceResponse> submitFormInstance(
			@RequestBody FormInstanceUpdateRequest formInstanceUpdateRequest) throws FormRelatedException,
			InstanceRelatedException, ClassRelatedException, InstanceHistoryRelatedException {

		String authToken = "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTA2NTEwNDUsInN1YiI6ImxvZ2luIiwidXNlckNvZGUiOiJ1c2VyMDAxIiwibG9nZ2VkSW5UaW1lc3RhbXAiOjE1MTA2NTEwNDUwNzgsInVzZXJOYW1lIjoidXNlciIsIm9yZ0NvZGUiOiJvcmcyIiwidGVuYW50Q29kZSI6IiAiLCJzeXNGb3JtQ29kZSI6IiAiLCJwb3J0YWxUeXBlIjoiU1VQRVJBRE1JTiIsImxvZ2dlZEluVXNlcklkZW50aWZpZXIiOiIgIiwiaXNzIjoiQWx0LUxvZ2luIiwiZXhwIjoxNTQyMTg3MDQ1fQ.sBLgWygjlciZv7YOgHRc2RAeoQzPZn-P8PY6z6V8oX8";
		return formComponent.submitFormInstance(formInstanceUpdateRequest, JwtUtil.validateJwt(authToken));
	}

	@PostMapping("/fetchForm")
	public ResponseEntity<FormInstanceResponse> fetchFormInstance(
			@RequestBody FormInstanceFetchRequest formInstanceFetchRequest)
			throws FormRelatedException, QueryRelatedException, IOException {

		String authToken = "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTA2NTEwNDUsInN1YiI6ImxvZ2luIiwidXNlckNvZGUiOiJ1c2VyMDAxIiwibG9nZ2VkSW5UaW1lc3RhbXAiOjE1MTA2NTEwNDUwNzgsInVzZXJOYW1lIjoidXNlciIsIm9yZ0NvZGUiOiJvcmcyIiwidGVuYW50Q29kZSI6IiAiLCJzeXNGb3JtQ29kZSI6IiAiLCJwb3J0YWxUeXBlIjoiU1VQRVJBRE1JTiIsImxvZ2dlZEluVXNlcklkZW50aWZpZXIiOiIgIiwiaXNzIjoiQWx0LUxvZ2luIiwiZXhwIjoxNTQyMTg3MDQ1fQ.sBLgWygjlciZv7YOgHRc2RAeoQzPZn-P8PY6z6V8oX8";
		return formComponent.fetchFormInstance(formInstanceFetchRequest, JwtUtil.validateJwt(authToken));
	}

	@PostMapping("/mailOperations")
	 //@CrossOrigin
	public ResponseEntity<CommEnvlp> communicationOperations(@RequestHeader(value = "authToken") String authToken,
			@RequestBody CommEnvlp commEnvlp) throws MailRelatedException, NoSuchAlgorithmException, IOException {
		return formComponent.communicationOperations(commEnvlp, JwtUtil.validateJwt(authToken));

	}

	@PostMapping("/reloadGlobalTextfields")
	public ResponseEntity<GlobalTextfieldReloadRequestResponse> reloadGlobalTextfields(
			@RequestHeader(value = "authToken") String authToken,
			@RequestBody GlobalTextfieldReloadRequestResponse request)
			throws MailRelatedException, NoSuchAlgorithmException, IOException {
		return formComponent.reloadGlobalTextfields(JwtUtil.validateJwt(authToken), request);
	}

	@PostMapping("/reloadLocalDataTable")
	public ResponseEntity<ReloadLocalDataTableRequestResponse> reloadLocalDatatable(
			@RequestHeader(value = "authToken") String authToken,
			@RequestBody ReloadLocalDataTableRequestResponse request)
			throws MailRelatedException, NoSuchAlgorithmException, IOException {
		return formComponent.reloadLocalDataTable(JwtUtil.validateJwt(authToken), request);
	}
	
	@PostMapping("/testAjax")
	public ResponseEntity<TestAjaxResponse> testAjaxRequest(@RequestBody TestAjaxResponse request) {
		TestAjaxResponse ajax = new TestAjaxResponse();
		return new ResponseEntity<TestAjaxResponse>(ajax, HttpStatus.OK);
	}

	@PostMapping("/downloadDataTable")
	public ResponseEntity<DownloadDataTableRequestResponse> downloadDataTable(
			@RequestHeader(value = "authToken") String authToken, @RequestBody DownloadDataTableRequestResponse request)
			throws IOException, FormRelatedException, FormDataRelatedException, InstanceRelatedException,
			ClassRelatedException, QueryRelatedException, ReferenceRelatedException {

		DownloadDataTableRequestResponse response = formComponent.downloadDataTable(JwtUtil.validateJwt(authToken),
				request);

		return new ResponseEntity<DownloadDataTableRequestResponse>(response, HttpStatus.OK);
	}

	@PostMapping("/uploadDataTable")
	public ResponseEntity<UploadDataTableRequestResponse> uploadDataTable(
			@RequestHeader(value = "authToken") String authToken, @RequestBody UploadDataTableRequestResponse request)
			throws IOException, FormDataRelatedException, QueryRelatedException, FormRelatedException,
			ClassRelatedException, InstanceRelatedException, InstanceHistoryRelatedException,
			ReferenceRelatedException {

		UploadDataTableRequestResponse response = formComponent.uploadDataTable(JwtUtil.validateJwt(authToken),
				request);

		return new ResponseEntity<UploadDataTableRequestResponse>(response, HttpStatus.OK);
	}
	
	@PostMapping("/getDataForTableByExternalAPI")
	/**
	 * @author vaibhav.kashyap
	 * */
	public ResponseEntity<DataTableOnDemand> getDataForTableByExternalAPI(
			@RequestBody ExecutorAjaxInstanceTransporter transporter) {
		AltDataTable table = formComponent.getDataForTableByExternalAPI(transporter.getComponentName(),
				transporter.getFormName(), JwtUtil.validateJwt(transporter.getAuthToken()),transporter.getAttributes(), transporter.getFormRoles());
		return new ResponseEntity<DataTableOnDemand>(new DataTableOnDemand(table), HttpStatus.OK);
	}
	
	@PostMapping(value = ("/uploadFile"), headers = ("content-type=multipart/*"), produces = "application/json")
	public ResponseEntity<ObjectNode> uploadFile(@RequestParam("uploadingFiles") MultipartFile file) {
		return formComponent.uploadFile(file);
	}

	@GetMapping(value = ("/downloadFile"), produces = "application/octet-stream")
	public ResponseEntity<Resource> downloadFile(@RequestParam("filePath") String filePath)
			throws FileNotFoundException, MalformedURLException {
		return formComponent.downloadFile(filePath);
	}
}
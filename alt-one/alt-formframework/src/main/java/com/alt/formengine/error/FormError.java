package com.alt.formengine.error;

public enum FormError {

	SUCCESS(200,"Success"),
	FAILED(800,"Failed to create form"),
	FAILED_TO_CREATE_SECURITY(801,"Failed to save security for form.");
	
	private final Integer code;
	private final String description;

	private FormError(int code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public Integer getCode() {
		return code;
	}

	@Override
	public String toString() {
		return code + ": " + description;
	}

}

package com.alt.formengine.exception;

public class FormDataRelatedException extends Exception {

    public FormDataRelatedException(String message) {
        super(message);
    }

    public FormDataRelatedException(String message, Throwable e) {
        super(message, e);
    }

}

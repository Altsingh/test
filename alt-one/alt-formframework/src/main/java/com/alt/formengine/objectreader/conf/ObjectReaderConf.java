package com.alt.formengine.objectreader.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("objectreader")
public class ObjectReaderConf {

	private String deployedreader_url;

	public String getDeployedreader_url() {
		return deployedreader_url;
	}

	public void setDeployedreader_url(String deployedreader_url) {
		this.deployedreader_url = deployedreader_url;
	}

}

package com.alt.formengine.objectreader.transport;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class HBaseObjectResponseTO  extends ObjectResponseTO implements Serializable{
	
	private List<Map<String, String>> data;

	public List<Map<String, String>> getData() {
		return data;
	}

	public void setData(List<Map<String, String>> data) {
		this.data = data;
	}

}


package com.alt.formengine.objectreader.transport;

import java.io.Serializable;
import java.util.List;

public class ObjectAttributeTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2543232049448316374L;

	private String objectCode;

	private String objectAttrCode;

	private String value;

	private List<String> values;

	private String operator;

	private String attrColumnLabel;

	private String operatorPosition;

	public String getOperatorPosition() {
		return operatorPosition;
	}

	public void setOperatorPosition(String operatorPosition) {
		this.operatorPosition = operatorPosition;
	}

	public String getObjectCode() {
		return objectCode;
	}

	public void setObjectCode(String objectCode) {
		this.objectCode = objectCode;
	}

	public String getObjectAttrCode() {
		return objectAttrCode;
	}

	public void setObjectAttrCode(String objectAttrCode) {
		this.objectAttrCode = objectAttrCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getAttrColumnLabel() {
		return attrColumnLabel;
	}

	public void setAttrColumnLabel(String attrColumnLabel) {
		this.attrColumnLabel = attrColumnLabel;
	}

}

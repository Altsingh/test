package com.alt.formengine.objectreader.transport;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ObjectReaderRequestTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5134136617591844011L;
	
	@JsonProperty("requestedAttrs")
	private List<ObjectAttributeTO> requestedAttrs;
	
	@JsonProperty("filterAttrs")
	private List<ObjectAttributeTO> filterAttrs;

	public List<ObjectAttributeTO> getRequestedAttrs() {
		return requestedAttrs;
	}

	public void setRequestedAttrs(List<ObjectAttributeTO> requestedAttrs) {
		this.requestedAttrs = requestedAttrs;
	}

	public List<ObjectAttributeTO> getFilterAttrs() {
		return filterAttrs;
	}

	public void setFilterAttrs(List<ObjectAttributeTO> filterAttrs) {
		this.filterAttrs = filterAttrs;
	}
	
	
	
}

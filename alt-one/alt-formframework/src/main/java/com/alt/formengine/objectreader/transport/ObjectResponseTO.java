package com.alt.formengine.objectreader.transport;

import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ObjectResponseTO<T> implements Serializable {
	
	private ArrayList<T> data;
	
	private String responeMessage;
	
	private String responseCode;

	public String getResponeMessage() {
		return responeMessage;
	}

	public void setResponeMessage(String responeMessage) {
		this.responeMessage = responeMessage;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
	
}

package com.alt.formengine.rule.util;

public abstract class RuleOperations {
	public static String checkInstanceTypeEquals(Object... ruleInput) {
		boolean allDataTypeisNotSame = false;
		String dataType = null;
		for (int i = 0; i < ruleInput.length; i++) {
			if (!(ruleInput[0].getClass().getSimpleName().equalsIgnoreCase(ruleInput[i].getClass().getSimpleName()))) {
				allDataTypeisNotSame = true;
				break;
			}
		}

		if (allDataTypeisNotSame)
			dataType = null;
		else
			dataType = ruleInput[0].getClass().getSimpleName();

		return dataType;
	}
}

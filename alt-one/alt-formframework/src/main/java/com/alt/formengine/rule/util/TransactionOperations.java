package com.alt.formengine.rule.util;

import java.util.Calendar;
import java.util.Map;

import com.alt.rule.model.RuleInput;

public class TransactionOperations extends RuleOperations {

	public static String concatOperands(Map<String, Object> componentFilledByUserByValues, String operandDataType,
			RuleInput... ruleInputUserValues) {
		String resultant = null;
		try {
			switch (operandDataType) {
			case "Integer":
				// operation not supported
				break;
			case "Double":
				// operation not supported
			case "Long":
				// operation not supported
			case "Boolean":
				// Operation not supported
				break;
			case "Short":
				break;
			case "String":
				resultant = "";
				for (int i = 0; i < ruleInputUserValues.length; i++) {
					resultant = resultant
							+ ((String) componentFilledByUserByValues.get(ruleInputUserValues[i].getDbClassAttr()))
									.toString();
				}
				return resultant;
			case "Char":
				break;
			case "Date":
				break;
			default:
				resultant = null;
				break;
			}
		} catch (Exception e) {
			resultant = null;
		}

		return resultant;
	}

	public static <T extends Number> T substractOperands(Map<String, Object> componentFilledByUserByValues,
			String operandDataType, RuleInput... ruleInputUserValues) throws NumberFormatException {
		try {
			switch (operandDataType) {
			case "Integer":

				Integer sum = new Integer(0);
				for (int i = 0; i < ruleInputUserValues.length; i++) {
					if (i == 0) {
						sum = ((Integer) componentFilledByUserByValues.get(ruleInputUserValues[i].getDbClassAttr()))
								.intValue();
						continue;
					}
					sum = sum - ((Integer) componentFilledByUserByValues.get(ruleInputUserValues[i].getDbClassAttr()))
							.intValue();
				}
				return (T) Integer.valueOf(sum);

			case "Double":

				Double sumDbl = new Double(0);
				for (int i = 0; i < ruleInputUserValues.length; i++) {
					sumDbl = sumDbl
							- ((Double) componentFilledByUserByValues.get(ruleInputUserValues[i].getDbClassAttr()))
									.doubleValue();
				}
				return (T) Double.valueOf(sumDbl);

			case "Long":

				Long sumLng = new Long(0);
				for (int i = 0; i < ruleInputUserValues.length; i++) {
					sumLng = sumLng
							- ((Long) componentFilledByUserByValues.get(ruleInputUserValues[i].getDbClassAttr()))
									.longValue();
				}
				return (T) Long.valueOf(sumLng);

			case "Boolean":
				// Operation not supported
				break;
			case "Short":
				break;
			case "String":
				// operation on supported
				break;
			case "Char":
				break;
			case "Date":
				break;
			default:
				return null;
			}
		} catch (Exception e) {
			return null;
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public static <T extends Number> T addOperands(Map<String, Object> componentFilledByUserByValues,
			String operandDataType, RuleInput... ruleInputUserValues) throws NumberFormatException {
		try {
			switch (operandDataType) {
			case "Integer":

				Integer sum = new Integer(0);
				for (int i = 0; i < ruleInputUserValues.length; i++) {
					sum = sum + ((Integer) componentFilledByUserByValues.get(ruleInputUserValues[i].getDbClassAttr()))
							.intValue();
				}
				return (T) Integer.valueOf(sum);

			case "Double":

				Double sumDbl = new Double(0);
				for (int i = 0; i < ruleInputUserValues.length; i++) {
					sumDbl = sumDbl
							+ ((Double) componentFilledByUserByValues.get(ruleInputUserValues[i].getDbClassAttr()))
									.doubleValue();
				}
				return (T) Double.valueOf(sumDbl);

			case "Long":

				Long sumLng = new Long(0);
				for (int i = 0; i < ruleInputUserValues.length; i++) {
					sumLng = sumLng
							+ ((Long) componentFilledByUserByValues.get(ruleInputUserValues[i].getDbClassAttr()))
									.longValue();
				}
				return (T) Long.valueOf(sumLng);

			case "Boolean":
				// Operation not supported
				break;
			case "Short":
				break;
			case "String":
				// operation on supported
				break;
			case "Char":
				break;
			case "Date":
				break;
			default:
				return null;
			}

		} catch (Exception e) {
			return null;
		}
		return null;
	}

}

package com.alt.formengine.rule.util;

import java.util.Map;

import com.alt.rule.model.RuleInput;

public class ValidationOperations extends RuleOperations {

	public static boolean ifEquals(RuleInput firstRuleInput, RuleInput secondRuleInput,
			Map<String, Object> componentFilledByUserByValues) {
		if (componentFilledByUserByValues.get(firstRuleInput.getDbClassAttr())
				.equals(componentFilledByUserByValues.get(secondRuleInput.getDbClassAttr()))) {
			return true;
		}
		return false;
	}

	public static boolean ifGreaterThan(RuleInput firstRuleInput, RuleInput secondRuleInput,
			Map<String, Object> componentFilledByUserByValues, String operandDataType) {
		boolean isGreater = false;
		switch (operandDataType) {
		case "Integer":
			Integer operandIntOne = (Integer) componentFilledByUserByValues.get(firstRuleInput.getDbClassAttr());
			Integer operandIntTwo = (Integer) componentFilledByUserByValues.get(secondRuleInput.getDbClassAttr());
			if (operandIntTwo.intValue() < operandIntOne.intValue()) {
				isGreater = true;
			}
			break;
		case "Double":
			Double operandDbleOne = (Double) componentFilledByUserByValues.get(firstRuleInput.getDbClassAttr());
			Double operandDblTwo = (Double) componentFilledByUserByValues.get(secondRuleInput.getDbClassAttr());
			if (operandDblTwo.doubleValue() < operandDbleOne.doubleValue()) {
				isGreater = true;
			}
			break;
		case "Long":
			Long operandLngeOne = (Long) componentFilledByUserByValues.get(firstRuleInput.getDbClassAttr());
			Long operandLngTwo = (Long) componentFilledByUserByValues.get(secondRuleInput.getDbClassAttr());
			if (operandLngTwo.longValue() < operandLngeOne.longValue()) {
				isGreater = true;
			}
			break;
		case "Boolean":
			// Operation not supported
			break;
		case "Short":
			break;
		case "String":
			String operandStrOne = (String) componentFilledByUserByValues.get(firstRuleInput.getDbClassAttr());
			String operandStrTwo = (String) componentFilledByUserByValues.get(secondRuleInput.getDbClassAttr());
			int resultant = operandStrOne.compareToIgnoreCase(operandStrTwo);
			if (resultant > 0) {
				isGreater = true;
			}
			break;
		case "Char":
			break;
		case "Date":
			break;
		default:
			// isEquals = false;
			break;
		}

		return isGreater;
	}

	public static boolean ifLesserThan(RuleInput firstRuleInput, RuleInput secondRuleInput,
			Map<String, Object> componentFilledByUserByValues, String operandDataType) {
		boolean isLesser = false;
		switch (operandDataType) {
		case "Integer":
			Integer operandIntOne = (Integer) componentFilledByUserByValues.get(firstRuleInput.getDbClassAttr());
			Integer operandIntTwo = (Integer) componentFilledByUserByValues.get(secondRuleInput.getDbClassAttr());
			if (operandIntOne.intValue() < operandIntTwo.intValue()) {
				isLesser = true;
			}
			break;
		case "Double":
			Double operandDbleOne = (Double) componentFilledByUserByValues.get(firstRuleInput.getDbClassAttr());
			Double operandDblTwo = (Double) componentFilledByUserByValues.get(secondRuleInput.getDbClassAttr());
			if (operandDbleOne.doubleValue() < operandDblTwo.doubleValue()) {
				isLesser = true;
			}
			break;
		case "Long":
			Long operandLngeOne = (Long) componentFilledByUserByValues.get(firstRuleInput.getDbClassAttr());
			Long operandLngTwo = (Long) componentFilledByUserByValues.get(secondRuleInput.getDbClassAttr());
			if (operandLngeOne.longValue() < operandLngTwo.longValue()) {
				isLesser = true;
			}
			break;
		case "Boolean":
			// Operation not supported
			break;
		case "Short":
			break;
		case "String":
			String operandStrOne = (String) componentFilledByUserByValues.get(firstRuleInput.getDbClassAttr());
			String operandStrTwo = (String) componentFilledByUserByValues.get(secondRuleInput.getDbClassAttr());
			int resultant = operandStrOne.compareToIgnoreCase(operandStrTwo);
			if (resultant < 0) {
				isLesser = true;
			}
			break;
		case "Character":
			break;
		case "Date":
			break;
		default:
			// isEquals = false;
			break;
		}

		return isLesser;
	}

}

package com.alt.formengine.run;

import com.alt.business.conf.LoginPropertyConf;
import com.alt.business.service.AppComponent;
import com.alt.business.service.DBService;
import com.alt.business.service.SuperadminServiceImpl;
import com.alt.datacarrier.business.superadmin.app.Module;
import com.alt.datakernel.DataKernelConfig;
import com.alt.datakernel.dao.*;
import com.alt.datakernel.dao.impl.*;
import com.alt.datakernel.service.IAltAttributeDataService;
import com.alt.datakernel.service.IClassService;
import com.alt.datakernel.service.IInstanceHistoryService;
import com.alt.datakernel.service.IReferenceService;
import com.alt.datakernel.service.impl.*;
import com.alt.datakernel.validation.ClassValidator;
import com.alt.formengine.config.KafkaConfig;
import com.alt.formengine.controller.FormResource;
import com.alt.formengine.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@ComponentScan(basePackages = {"com.alt.formengine.*","com.alt.business.*","com.alt.datakernel.service",
        "com.alt.datakernel.service.impl",
        "com.alt.datakernel.validation",
        "com.alt.datakernel.model"})
@Import(value = {DataKernelConfig.class})
@EntityScan(
        basePackageClasses = {com.alt.datakernel.model.AltFormData.class,
                com.alt.datakernel.model.AltClassData.class,
                com.alt.datakernel.model.AltAttributeData.class,
                com.alt.datakernel.model.AltReferenceData.class,
                com.alt.datakernel.model.Components.class,
                com.alt.datakernel.model.Attributes.class,
                com.alt.datakernel.model.Instance.class}
)
public class FormEngineRun extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(FormEngineRun.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(FormEngineRun.class, args);
    }

    @Bean
    public FormResource formResource() {
        return new FormResource();
    }


    @Bean
    public FormCommonService formCommonService() {
        return new FormCommonService();
    }
    
    @Bean
    public RuleExecutionService ruleExecutionService() {
    	return new RuleExecutionService();
    }

    @Bean
    public HttpService httpService() {
        return new HttpService();
    }

    @Bean
    public DbService dbService() {
        return new DbService();
    }

    @Bean
    public ClassComponent classComponent() {
        return new ClassComponent();
    }


    @Bean
    public FormComponent formComponent() {
        return new FormComponent();
    }


    @Bean
    public FormDataComponent formDataComponent() {
        return new FormDataComponent();
    }

    @Bean
    public DBService DBService() {
        return new DBService();
    }

    @Bean
    public Module Module() {
        return new Module();
    }

    @Bean
    public SuperadminServiceImpl SuperadminServiceImpl() {
        return new SuperadminServiceImpl();
    }

    @Bean
    public AppComponent appComponent() {
        return new AppComponent();
    }

    @Bean
    public InstanceService instanceService() {
        return new InstanceService();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public IInstanceDS iInstanceDS() {
        return new InstanceDS();
    }

    @Bean
    public IInstanceHistoryDS iInstanceHistoryDS() {
    	return new InstanceHistoryDS();
    }
    
    @Bean
    public IInstanceHistoryService iInstanceHistoryService() {
    	return new InstanceHistoryService();
    }
    
    @Bean
    public IClassDS iClassDS() {
        return new AltClassDS();
    }
    
    /*@Bean
    public IDynamicQuery iDynamicQuery() {
    	return new DynamicQueryService();
    }*/

    @Bean
    public IQueryDS iQueryDS() {
        return new QueryDS();
    }

    @Bean
    public IFormDS iFormDS() {
        return new AltFormDS();
    }

    @Bean
    public LoginPropertyConf SecurityPropertiesConf() {
        return new LoginPropertyConf();
    }

    @Bean
    public KafkaConfig kafkaConfig() {
        return new KafkaConfig();
    }

    @Bean
    public IClassService iClassService() {
        return new ClassService();
    }

    @Bean
    public ClassValidator classValidator() {
        return new ClassValidator();
    }
    
    @Bean
    public IReferenceDS iReferenceDS() {
    	return new AltReferenceDS();
    }
    
    @Bean IReferenceService iReferenceService() {
    	return new ReferenceService();
    }
    
    @Bean  IAltAttributeDataService iAltAttributeDataService() {
    	return new AltAttributeDataService();
    }
    
    @Bean IAltAttributeDataDS iAltAttributeDataDS() {
    	return new AltAttributeDataDS();
    }

    @Bean
    public AltAppDS altAppDS() {
        return new AltAppDS();
    }
    
    @Bean
    public ITaskCodeRecordDS iTaskCodeRecordDS() {
    	return new TaskCodeRecordDS();
    }

    @Bean
    @Profile("local")
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");

            }
        };
    }
}

package com.alt.formengine.service;

import com.alt.datacarrier.common.enumeration.EnumSortDirection;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.formframework.response.ClassAttributesResponse;
import com.alt.datacarrier.formframework.response.ClassIdAndNameResponse;
import com.alt.datacarrier.kernel.db.core.*;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.service.IClassService;
import com.alt.datakernel.service.IDynamicQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;

public class ClassComponent {
	
    @Autowired
    IDynamicQuery dynamicQuery;

    @Autowired
    IClassService classService;
    
    public ResponseEntity<List<ClassIdAndNameResponse>> getClasses(Protocol protocol, boolean isIdNeeded, boolean isNameNeeded, 
    		boolean isAttributeNeeded, int limit, int offset) throws QueryRelatedException {
        DynamicReadRequest request = new DynamicReadRequest();
        createDynamicQueryRequest(request, protocol.getOrgCode(), offset, limit, protocol.getOrgId());
		
        DynamicReadResponse response = dynamicQuery.read(request);
        List<Object[]> instances = response.getResponse();
        List<ClassIdAndNameResponse> details = new ArrayList<>();
        for(Object[] instance : instances) {
        	ClassIdAndNameResponse detail = new ClassIdAndNameResponse(((Integer)instance[0]).toString(), (String)instance[1]);
        	details.add(detail);
        }
        
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Response", "Class id and name fetched successfully");
		return new ResponseEntity<>(details, headers, HttpStatus.OK);
	
    }

	private void createDynamicQueryRequest(DynamicReadRequest request, String orgCode, int offset, int limit, Integer orgId ) {
		List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute attribute = new ProjectionAttribute();
        attribute.setDbClass("class");
        List<AttributesData> dbAttributes = new ArrayList<>();
        AttributesData data1 = new AttributesData();
        data1.setAttribute("classid");
        data1.setAs("Id");
        dbAttributes.add(data1);
        AttributesData data2 = new AttributesData();
        data2.setAttribute("classname");
        data2.setAs("ClassName");
        dbAttributes.add(data2);
        attribute.setDbAttributes(dbAttributes);
        projections.add(attribute);
		request.setProjections(projections );
		request.setFromDbClass("class");
		List<DbFilters> dbFilters = new ArrayList<>();
		DbFilters filter1 = new DbFilters();
		filter1.setAttribute("status");
		filter1.setClassName("class");
		filter1.setCondition("=");
		filter1.setValue("0");
		dbFilters.add(filter1);
		DbFilters filter2 = new DbFilters();
		filter2.setAttribute("orgid");
		filter2.setClassName("class");
		filter2.setCondition("=");
		filter2.setValue(orgId.toString());
		dbFilters.add(filter2);
		request.setDbFilters(dbFilters);
		request.setLimit(limit);
		request.setOffset(offset);
		List<OrderAttribute> orderByList = new ArrayList<>();
		OrderAttribute orderBy = new OrderAttribute();
		orderBy.setDbClass("class");
		orderBy.setDbAttr("createddate");
		orderBy.setSortDirection(EnumSortDirection.DESC);
		orderByList.add(orderBy);
		request.setOrderBy(orderByList);
	}
	
    public ResponseEntity<ClassAttributesResponse> getAltClass(Protocol protocol, boolean isAttributeNeeded, String classId) throws NumberFormatException, ClassRelatedException{
    	ClassMeta meta = classService.read(protocol, Integer.valueOf(classId));
    	ClassAttributesResponse classAttributes = new ClassAttributesResponse();
    	List<String> attributes = new ArrayList<>();
    	for(AttributeMeta attribute : meta.getAttributes()) {
    		attributes.add(attribute.getAttributeName());
    	}
		classAttributes.setAttributes(attributes);
        
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Response", "Class attributes fetched successfully");
		return new ResponseEntity<>(classAttributes, headers, HttpStatus.OK);
    }

}

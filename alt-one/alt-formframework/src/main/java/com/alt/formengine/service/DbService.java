package com.alt.formengine.service;

import com.alt.datacarrier.common.enumeration.EnumSortDirection;
import com.alt.datacarrier.kernel.db.core.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DbService {

    protected static DynamicReadRequest createDynamicQueryRequest(DynamicReadRequest request, String formId) {

        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute attribute = new ProjectionAttribute();
        attribute.setDbClass("instance");
        List<AttributesData> dbAttributes = new ArrayList<>();
        AttributesData data1 = new AttributesData();
        data1.setAttribute("attributes");
        data1.setJsonbType(true);
        data1.setJsonName("attribute");
        data1.setJsonbAttribute("customFieldGroupId");
        data1.setAs("CustomFieldGroupId");
        dbAttributes.add(data1);
        AttributesData data2 = new AttributesData();
        data2.setAttribute("attributes");
        data2.setJsonbType(true);
        data2.setJsonName("attribute");
        data2.setJsonbAttribute("customFormType");
        data2.setAs("CustomFormType");
        dbAttributes.add(data2);
        AttributesData data3 = new AttributesData();
        data3.setAttribute("attributes");
        data3.setJsonbType(true);
        data3.setJsonName("attribute");
        data3.setJsonbAttribute("organization");
        data3.setAs("Organization");
        dbAttributes.add(data3);
        AttributesData data4 = new AttributesData();
        data4.setAttribute("attributes");
        data4.setJsonbType(true);
        data4.setJsonName("attribute");
        data4.setJsonbAttribute("orgId");
        data4.setAs("OrgId");
        dbAttributes.add(data4);
        AttributesData data5 = new AttributesData();
        data5.setAttribute("attributes");
        data5.setJsonbType(true);
        data5.setJsonName("attribute");
        data5.setJsonbAttribute("tenantId");
        data5.setAs("TenantId");
        dbAttributes.add(data5);
        AttributesData data6 = new AttributesData();
        data6.setAttribute("attributes");
        data6.setJsonbType(true);
        data6.setJsonName("attribute");
        data6.setJsonbAttribute("customFormId");
        data6.setAs("CustomFormId");
        dbAttributes.add(data6);
        AttributesData data7 = new AttributesData();
        data7.setAttribute("attributes");
        data7.setJsonbType(true);
        data7.setJsonName("attribute");
        data7.setJsonbAttribute("customFieldGroupName");
        data7.setAs("customFieldGroupName");
        dbAttributes.add(data7);

        attribute.setDbAttributes(dbAttributes);
        projections.add(attribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("FormMapping");
        dbFilters.add(filter1);
        DbFilters filter2 = new DbFilters();
        filter2.setAttribute("attributes");
        filter2.setClassName("instance");
        filter2.setJsonbType(true);
        List<String> jsonbAttributes = new ArrayList<String>();
        jsonbAttributes.add("formId");
        filter2.setJsonbAttribute(jsonbAttributes);
        filter2.setJsonName("attribute");
        filter2.setCondition("=");
        filter2.setValue(formId);
        dbFilters.add(filter2);
        request.setDbFilters(dbFilters);
        return request;
    }

    protected DynamicReadRequest createDynamicQueryRequest(DynamicReadRequest request, int offset, int limit, Integer orgId,boolean published, int appId) {
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute attribute = new ProjectionAttribute();
        attribute.setDbClass("form");
        List<AttributesData> dbAttributes = new ArrayList<>();
        AttributesData data1 = new AttributesData();
        data1.setAttribute("formname");
        data1.setAs("FormName");
        dbAttributes.add(data1);
        AttributesData data2 = new AttributesData();
        data2.setAttribute("createdby");
        data2.setAs("CreatedBy");
        dbAttributes.add(data2);
        AttributesData data3 = new AttributesData();
        data3.setAttribute("state");
        data3.setAs("State");
        dbAttributes.add(data3);
        AttributesData data4 = new AttributesData();
        data4.setAttribute("formid");
        data4.setAs("Id");
        dbAttributes.add(data4);
        attribute.setDbAttributes(dbAttributes);
        projections.add(attribute);
        request.setProjections(projections);
        request.setFromDbClass("form");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter2 = new DbFilters();
        filter2.setAttribute("orgid");
        filter2.setClassName("form");
        filter2.setCondition("=");
        filter2.setValue(orgId.toString());
        dbFilters.add(filter2);
        if(published){
            DbFilters filter3 = new DbFilters();
            filter3.setAttribute("state");
            filter3.setClassName("form");
            filter3.setCondition("=");
            filter3.setValue("1");
            dbFilters.add(filter3);
        }if(appId>0) {
            DbFilters filter4 = new DbFilters();
            filter4.setAttribute("appId");
            filter4.setClassName("form");
            filter4.setCondition("=");
            filter4.setValue(((Integer)appId).toString());
            dbFilters.add(filter4);
        }
        request.setDbFilters(dbFilters);
        request.setLimit(limit);
        request.setOffset(offset);
        List<OrderAttribute> orderByList = new ArrayList<>();
        OrderAttribute orderBy = new OrderAttribute();
        orderBy.setDbClass("form");
        orderBy.setDbAttr("createddate");
        orderBy.setSortDirection(EnumSortDirection.DESC);
        orderByList.add(orderBy);
        request.setOrderBy(orderByList);
        return request;
    }

    protected static DynamicReadRequest findPublishedFormForGivenFormName(String formName, Integer
            orgId, Integer state) {
        DynamicReadRequest request = new DynamicReadRequest();
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute attribute = new ProjectionAttribute();
        attribute.setDbClass("form");
        List<AttributesData> dbAttributes = new ArrayList<>();
        AttributesData data1 = new AttributesData();
        data1.setAttribute("formid");
        data1.setAs("Id");
        AttributesData data2 = new AttributesData();
        data2.setAttribute("version");
        data2.setAs("version");
        dbAttributes.add(data1);
        dbAttributes.add(data2);
        attribute.setDbAttributes(dbAttributes);
        projections.add(attribute);
        request.setProjections(projections);
        request.setFromDbClass("form");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("orgid");
        filter1.setClassName("form");
        filter1.setCondition("=");
        filter1.setValue(orgId.toString());
        DbFilters filter2 = new DbFilters();
        filter2.setAttribute("formname");
        filter2.setClassName("form");
        filter2.setCondition("=");
        filter2.setValue(formName);
        DbFilters filter3 = new DbFilters();
        filter3.setAttribute("state");
        filter3.setClassName("form");
        filter3.setCondition("=");
        filter3.setValue(state.toString());
        dbFilters.add(filter1);
        dbFilters.add(filter2);
        dbFilters.add(filter3);
        request.setDbFilters(dbFilters);
        List<OrderAttribute> orderByList = new ArrayList<>();
        OrderAttribute orderBy = new OrderAttribute();
        orderBy.setDbClass("form");
        orderBy.setDbAttr("createddate");
        orderBy.setSortDirection(EnumSortDirection.DESC);
        orderByList.add(orderBy);
        request.setOrderBy(orderByList);
        return request;
    }


}

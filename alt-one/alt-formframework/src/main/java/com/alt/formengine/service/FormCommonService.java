package com.alt.formengine.service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.formframework.request.FormCRUDData;
import com.alt.datacarrier.formframework.request.FormUpdateRequest;
import com.alt.datacarrier.formsecurity.core.AltOneCustomFormSecurityRequest;
import com.alt.datacarrier.formsecurity.core.Field;
import com.alt.datacarrier.formsecurity.core.FieldGroup;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.request.FormRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datacarrier.kernel.uiclass.AltAbstractBaseComponent;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.AltAttachmentsComponent;
import com.alt.datacarrier.kernel.uiclass.AltButtonComponent;
import com.alt.datacarrier.kernel.uiclass.AltCheckboxComponent;
import com.alt.datacarrier.kernel.uiclass.AltDataTable;
import com.alt.datacarrier.kernel.uiclass.AltDropdownComponent;
import com.alt.datacarrier.kernel.uiclass.AltExcelComponent;
import com.alt.datacarrier.kernel.uiclass.AltGroupComponent;
import com.alt.datacarrier.kernel.uiclass.AltMultiSelectComponent;
import com.alt.datacarrier.kernel.uiclass.AltOption;
import com.alt.datacarrier.kernel.uiclass.AltPanelComponent;
import com.alt.datacarrier.kernel.uiclass.AltTabbedPanelComponent;
import com.alt.datacarrier.kernel.uiclass.EnumComponentParentEnvironment;
import com.alt.datacarrier.kernel.uiclass.EnumHTMLControl;
import com.alt.datacarrier.kernel.uiclass.FileDetail;
import com.alt.datacarrier.organization.RoleTO;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.exception.ReferenceRelatedException;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.datakernel.service.IFormService;
import com.alt.datakernel.service.IInstanceService;
import com.alt.datakernel.service.IReferenceService;
import com.alt.datakernel.service.impl.ClassService;
import com.alt.formengine.config.SecurityPropertiesConf;
import com.alt.formengine.objectreader.conf.ObjectReaderConf;
import com.alt.formengine.objectreader.transport.HBaseObjectResponseTO;
import com.alt.formengine.objectreader.transport.ObjectAttributeTO;
import com.alt.formengine.objectreader.transport.ObjectReaderRequestTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.peoplestrong.hris.to.ReportingEmployeesRequestResponseTO;

@Service
public class FormCommonService {

	@Autowired
	DbService dbService;

	@Autowired(required = true)
	IDynamicQuery dynamicQuery;

	@Autowired
	IFormService formService;

	@Autowired
	IReferenceService iReferenceService;

	@Autowired
	IInstanceService instanceService;

	@Autowired
	ObjectReaderConf objectReaderConf;

	@Autowired
	private ClassService classService;

	@Autowired
	SecurityPropertiesConf securityPropertiesConf;

	@Autowired
	FormDataComponent formDataComponent;

	private Logger LOGGER = LoggerFactory.getLogger(FormCommonService.class);

	protected ResponseEntity<FormCRUDData> convertToResponse(Protocol protocol, AltForm altForm,
			HashMap<String, List<AltOption>> attributeValuebyAttributeName,
			HashMap<String, List<String>> presavedUserValulesByComponent, Collection<RoleTO> formRoles,
			String instanceId) throws QueryRelatedException, ClassRelatedException, InstanceRelatedException {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		if (altForm == null) {
			headers.add("ERROR", "Form fetched is null");
			return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		Long hrFormId = (altForm.getFormSecurityId() == null) ? null : Long.parseLong(altForm.getFormSecurityId());
		FormCRUDData form = new FormCRUDData.Builder()
				.componentList(resolveDbAttributesBinding(protocol, altForm.getComponentList(),
						attributeValuebyAttributeName, presavedUserValulesByComponent, formRoles, instanceId))
				.customComponentAllowed(altForm.isCustomComponentAllowed()).description(altForm.getDescription())
				.moduleCode(altForm.getModuleCode()).orgCode(altForm.getOrgCode()).status(altForm.getStatus())
				.type(altForm.getFormType()).uiClassCode(altForm.getId()).uiClassName(altForm.getUiClassName())
				.versionId(altForm.getVersion()).metaClassId(altForm.getMetaClassId())
				.attributeValueByAttributeName(attributeValuebyAttributeName).formState(altForm.getState())
				.withHrFormId(hrFormId).withAppid(altForm.getAppid()).withParentFormId(altForm.getParentFormId())
				.taskCodeRequired(altForm.getTaskCodeRequired()).build();
		headers.add("Response", "form listing fetched successfully");
		return new ResponseEntity<>(form, headers, HttpStatus.OK);
	}

	private List<AltAbstractComponent> resolveDbAttributesBinding(Protocol protocol,
			List<AltAbstractComponent> componentList, HashMap<String, List<AltOption>> attributeValuebyAttributeName,
			HashMap<String, List<String>> presavedUserValulesByComponent, Collection<RoleTO> formRoles,
			String instanceId) throws QueryRelatedException, ClassRelatedException, InstanceRelatedException {
		for (AltAbstractComponent component : componentList) {
			EnumHTMLControl controlType = component.getControlType();
			AltAbstractBaseComponent baseComponent = null;
			if (controlType == EnumHTMLControl.TEXTFIELD || controlType == EnumHTMLControl.DROPDOWN
					|| controlType == EnumHTMLControl.MULTI_SELECT || controlType == EnumHTMLControl.DATE
					|| controlType == EnumHTMLControl.IMAGE || controlType == EnumHTMLControl.TEXTAREA) {
				baseComponent = (AltAbstractBaseComponent) component;
			} else if (controlType == EnumHTMLControl.BUTTON) {
				continue;
			} else if (controlType == EnumHTMLControl.PANEL) {
				this.resolveDbAttributesBinding(protocol, ((AltPanelComponent) component).getComponentList(),
						attributeValuebyAttributeName, presavedUserValulesByComponent, formRoles, instanceId);
				continue;
			} else if (controlType == EnumHTMLControl.TAB_PANEL) {
				for (AltAbstractComponent childComponent : ((AltTabbedPanelComponent) component).getPanels()) {
					this.resolveDbAttributesBinding(protocol, ((AltPanelComponent) childComponent).getComponentList(),
							attributeValuebyAttributeName, presavedUserValulesByComponent, formRoles, instanceId);
				}
				continue;
			} else if (controlType == EnumHTMLControl.DATATABLE) {
				AltDataTable table = (AltDataTable) component;
				try {
					if (null != table.getDbClassRead()) {
						table.setAllColumns(fetchAttributesByClassId(protocol, table.getDbClassRead(), null));
						Integer instanceIdInteger = null;
						if (Boolean.TRUE.equals(component.getInstanceSpecificHistory())) {
							try {
								instanceIdInteger = Integer.parseInt(instanceId);
							} catch (NumberFormatException nfe) {
							}
						}
						if (component.getHistoryEnabled() != null && component.getHistoryEnabled()) {
							table.setData(getDataForInstanceTable(protocol, table.getDbClassRead(),
									table.getDisplayedColumns(), null, instanceIdInteger));
							if (!CollectionUtils.isEmpty(table.getAllColumns())) {
								table.setFilteringData(getFilteringDataForInstanceTable(protocol,
										table.getDbClassRead(), table.getAllColumns()));
							}
						} else {
							Collection<String> ownerEmployeeIds = null;
							if (table.getMatchOwnerId() == null || table.getMatchOwnerId().equals("current_employee")) {
								ownerEmployeeIds = new HashSet<String>();
								ownerEmployeeIds.add(protocol.getEmployeeId().toString());
							} else if (table.getMatchOwnerId() != null
									&& table.getMatchOwnerId().equals("reporting_employee")) {
								ownerEmployeeIds = new HashSet<String>();
								ownerEmployeeIds.add("0");
								for (Integer employeeId : this.getReportingEmployeeIds(protocol, formRoles)) {
									ownerEmployeeIds.add(employeeId.toString());
								}
							} else if (table.getMatchOwnerId() != null
									&& table.getMatchOwnerId().equals("all_employee")) {
								ownerEmployeeIds = new HashSet<String>();
							} else {
								// do not load unless reporting employee selected
							}

							table.setData(getDataForTable(protocol, table.getDbClassRead(), table.getDisplayedColumns(),
									ownerEmployeeIds, null));
							if (!CollectionUtils.isEmpty(table.getAllColumns())) {
								table.setFilteringData(getDataForTable(protocol, table.getDbClassRead(),
										table.getAllColumns(), ownerEmployeeIds, null));
							}
						}

						if (table.getFormPopup() != null) {
							List<AltButtonComponent> navigationButtons = new ArrayList<AltButtonComponent>();
							ReadRequest request = new ReadRequest(protocol, null, -1, -1, -1, table.getFormPopup(),
									null, null);
							AltForm form = formService.readFromId(request);
							List<String> formNames = new ArrayList<String>();
							formNames.add(form.getUiClassName());
							List<AltForm> forms = formService.listByNames(protocol, formNames);
							form = forms.get(forms.size() - 1);
							for (AltAbstractComponent abstractComponent : formDataComponent
									.unNestComponentList(form.getComponentList())) {
								if (abstractComponent.getControlType() == EnumHTMLControl.BUTTON) {
									AltButtonComponent buttonComponent = (AltButtonComponent) abstractComponent;
									if (buttonComponent.getNavigation() != null) {
										navigationButtons.add(buttonComponent);
									}
								}
							}
							table.setNavigationButtons(navigationButtons);
						}
					}
				} catch (InstanceRelatedException e) {
					LOGGER.error("Error retreiving data for table", e);
				} catch (FormRelatedException e) {
					LOGGER.error("Error retreiving data for table", e);
				}
				continue;
			}

			if (controlType == EnumHTMLControl.TEXTFIELD) {
				if (presavedUserValulesByComponent != null && !presavedUserValulesByComponent.isEmpty()) {
					List<String> values = presavedUserValulesByComponent.get(baseComponent.getName());
					if (values != null && !values.isEmpty()) {
						baseComponent.setValue(values.get(0));
					}
				}
			} else if (controlType == EnumHTMLControl.DATE) {
				if (presavedUserValulesByComponent != null && !presavedUserValulesByComponent.isEmpty()) {
					List<String> values = presavedUserValulesByComponent.get(baseComponent.getName());
					if (values != null && !values.isEmpty()) {
						baseComponent.setValue(values.get(0));
					}
				}
			} else if (controlType == EnumHTMLControl.IMAGE) {
				if (presavedUserValulesByComponent != null && !presavedUserValulesByComponent.isEmpty()) {
					List<String> values = presavedUserValulesByComponent.get(baseComponent.getName());
					if (values != null && !values.isEmpty()) {
						baseComponent.setValue(values.get(0));
					}
				}
			} else if (controlType == EnumHTMLControl.LABEL) {
				if (presavedUserValulesByComponent != null && !presavedUserValulesByComponent.isEmpty()) {
					List<String> values = presavedUserValulesByComponent.get(baseComponent.getName());
					if (values != null && !values.isEmpty()) {
						baseComponent.setValue(values.get(0));
					}
				}
			} else if (controlType == EnumHTMLControl.DROPDOWN) {
				AltDropdownComponent<String> dropDownComponent = (AltDropdownComponent<String>) baseComponent;
				if (dropDownComponent.getOptions() == null || dropDownComponent.getOptions().size() == 0) {
					List<AltOption> options = null;
					// populate master data if any
					if (attributeValuebyAttributeName != null && !attributeValuebyAttributeName.isEmpty()
							&& attributeValuebyAttributeName
									.containsKey(((AltGroupComponent) component).getDbClassRead() + "_"
											+ ((AltGroupComponent) component).getDbAttrRead())) {
						options = attributeValuebyAttributeName.get(((AltGroupComponent) component).getDbClassRead()
								+ "_" + ((AltGroupComponent) component).getDbAttrRead());
					}
					if (options != null)
						Collections.sort(options);
					dropDownComponent.setOptions(options);
				}

				// populate values if form pre-saved by user
				if (presavedUserValulesByComponent != null && !presavedUserValulesByComponent.isEmpty()) {
					List<String> values = presavedUserValulesByComponent.get(dropDownComponent.getName());
					if (values != null && !values.isEmpty()) {
						if (values.size() > 1) {
							dropDownComponent.setValue(values.toString());
						} else {
							dropDownComponent.setValue(values.get(0));
						}
					}
				}
				if (dropDownComponent.getDbClassRead() != null && !dropDownComponent.getDbClassType()
						.equalsIgnoreCase(EnumComponentParentEnvironment.GLOBAL.toString())) {
					dropDownComponent.setAllColumns(
							fetchAttributesByClassId(protocol, dropDownComponent.getDbClassRead(), dropDownComponent));

					// dropDownComponent.setAttributeName(attributeName);
					if (!CollectionUtils.isEmpty(dropDownComponent.getAllColumns())) {
						dropDownComponent.setFilteringData(getDataForTable(protocol, dropDownComponent.getDbClassRead(),
								dropDownComponent.getAllColumns(), new HashSet<>(), null));
					}
				}

			} else if (controlType == EnumHTMLControl.MULTI_SELECT) {
				AltMultiSelectComponent<String> multiSelectComponent = (AltMultiSelectComponent<String>) baseComponent;

				// populate master data if any
				List<AltOption> options = null;
				if (attributeValuebyAttributeName != null && !attributeValuebyAttributeName.isEmpty()
						&& attributeValuebyAttributeName.containsKey(((AltGroupComponent) component).getDbClassRead()
								+ "_" + ((AltGroupComponent) component).getDbAttrRead())) {
					options = attributeValuebyAttributeName.get(((AltGroupComponent) component).getDbClassRead() + "_"
							+ ((AltGroupComponent) component).getDbAttrRead());
				}
				if (options != null)
					Collections.sort(options);
				multiSelectComponent.setOptions(options);

				// populate values if form pre-saved by user
				if (presavedUserValulesByComponent != null && !presavedUserValulesByComponent.isEmpty()) {
					List<String> values = presavedUserValulesByComponent.get(multiSelectComponent.getName());
					if (values != null && !values.isEmpty()) {
						multiSelectComponent.setValues(values);

						if (options != null && !options.isEmpty()) {
							List<AltOption> group1Options = new ArrayList<AltOption>();
							List<AltOption> group2Options = new ArrayList<AltOption>();
							for (AltOption option : options) {
								if (option.getValue() != null && values.contains((String) option.getValue()))
									group1Options.add(option);
								else
									group2Options.add(option);
							}
							options.clear();
							options.addAll(group1Options);
							options.addAll(group2Options);
						}
					}
				}
			} else if (controlType == EnumHTMLControl.EXCEL) {
				if (presavedUserValulesByComponent != null && !presavedUserValulesByComponent.isEmpty()) {
					List<String> values = presavedUserValulesByComponent.get(component.getName());
					AltExcelComponent excelComponent = (AltExcelComponent) component;
					if (values != null && !values.isEmpty()) {
						String filePath = values.get(0);
						String fileName = filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length());
						excelComponent.setDataFileName(fileName);
						excelComponent.setDataFilePath(filePath);
					}
				}
			} else if (controlType == EnumHTMLControl.ATTACHMENTS) {
				if (presavedUserValulesByComponent != null && !presavedUserValulesByComponent.isEmpty()) {
					List<String> values = presavedUserValulesByComponent.get(component.getName());
					AltAttachmentsComponent attachmentsComponent = (AltAttachmentsComponent) component;
					if (values != null && !values.isEmpty()) {
						String json = values.get(0);
						List<FileDetail> files = null;
						try {
							files = new ObjectMapper().readValue(json, new TypeReference<List<FileDetail>>() {
							});
						} catch (Exception e) {
						}
						attachmentsComponent.setFiles(files);
					}
				}
			} else if (controlType == EnumHTMLControl.CHECKBOX) {
				if (presavedUserValulesByComponent != null && !presavedUserValulesByComponent.isEmpty()) {
					List<String> values = presavedUserValulesByComponent.get(component.getName());
					AltCheckboxComponent checkboxComponent = (AltCheckboxComponent) component;
					if (values != null && !values.isEmpty()) {
						String value = values.get(0);
						checkboxComponent.setValue(Boolean.parseBoolean(value));
					}
				}
			}
		}
		return componentList;

	}

	/**
	 * @author vaibhav.kashyap
	 */
	public AltForm getFormDetailsByFormName(String formName, Protocol protocol) throws FormRelatedException {
		ReadRequest request = new ReadRequest();
		request.setFormName(formName);
		request.setProtocol(protocol);
		return formService.read(request);
	}

	/**
	 * @author vaibhav.kashyap
	 */
	public AltAbstractComponent getComponentFromComponentTreeOfForm(String componentName,
			List<AltAbstractComponent> componentList) {
		for (AltAbstractComponent component : componentList) {
			if (component.getControlType().name().equalsIgnoreCase("PANEL")) {
				AltPanelComponent panel = (AltPanelComponent) component;
				AltAbstractComponent comp = getComponentFromComponentTreeOfForm(componentName,
						panel.getComponentList());
				if (comp != null)
					return comp;
			}
			if (component.getName().equalsIgnoreCase(componentName)) {
				return component;
			}

		}
		return null;
	}

	/**
	 * @author vaibhav.kashyap
	 */
	public AltDataTable getDataForTableByExternalAPI(String componentName, String formName, Protocol protocol,
			Map<String, String> filterAttributes, RoleTO formRoles) {

		if (formName == null || formName.equalsIgnoreCase(""))
			return null;
		AltDataTable table = null;
		try {
			/**
			 * get form details with component using the form name.
			 */
			AltForm formRetrieved = getFormDetailsByFormName(formName, protocol);
			if (formRetrieved == null)
				return null;

			table = (AltDataTable) getComponentFromComponentTreeOfForm(componentName, formRetrieved.getComponentList());

			if (table != null && null != table.getDbClassRead()) {
				table.setAllColumns(fetchAttributesByClassId(protocol, table.getDbClassRead(), null));

				if (table.getHistoryEnabled() != null && table.getHistoryEnabled()) {
					table.setData(getDataForInstanceTable(protocol, table.getDbClassRead(), table.getDisplayedColumns(),
							filterAttributes, null));
					if (!CollectionUtils.isEmpty(table.getAllColumns())) {
						table.setFilteringData(getFilteringDataForInstanceTable(protocol, table.getDbClassRead(),
								table.getAllColumns()));
					}
				} else {
					Collection<String> ownerEmployeeIds = null;
					if (table.getMatchOwnerId() == null || table.getMatchOwnerId().equals("current_employee")) {
						ownerEmployeeIds = new HashSet<String>();
						ownerEmployeeIds.add(protocol.getEmployeeId().toString());
					} else if (table.getMatchOwnerId() != null
							&& table.getMatchOwnerId().equals("reporting_employee")) {
						ownerEmployeeIds = new HashSet<String>();
						Collection<RoleTO> formRole = null;
						if (formRoles != null) {
							List<RoleTO> role = new ArrayList<>();
							role.add(formRoles);
							formRole = role;
						}
						for (Integer employeeId : this.getReportingEmployeeIds(protocol,
								(formRole != null) ? formRole : null)) {
							ownerEmployeeIds.add(employeeId.toString());
						}
					} else if (table.getMatchOwnerId() != null && table.getMatchOwnerId().equals("all_employee")) {
						ownerEmployeeIds = new HashSet<String>();
					} else {
						// do not load unless reporting employee selected
					}

					table.setData(getDataForTable(protocol, table.getDbClassRead(), table.getDisplayedColumns(),
							ownerEmployeeIds, filterAttributes));
					if (!CollectionUtils.isEmpty(table.getAllColumns())) {
						table.setFilteringData(getDataForTable(protocol, table.getDbClassRead(), table.getAllColumns(),
								ownerEmployeeIds, filterAttributes));
					}
				}

			} else {
				table = null;
			}
		} catch (InstanceRelatedException | FormRelatedException | ClassRelatedException e) {
			LOGGER.error("Error retreiving data for table", e);
		} catch (Exception e) {
			LOGGER.error("Error retreiving data for table", e);
		}

		return table;

	}

	private List<String> fetchAttributesByClassId(Protocol protocol, String dbClassRead, AltDropdownComponent comp)
			throws ClassRelatedException {
		ReadRequest readRequest = new ReadRequest();
		readRequest.setName(dbClassRead);
		readRequest.setProtocol(protocol);
		ClassMeta classMeta = classService.read(protocol, Integer.valueOf(dbClassRead));
		List<String> ret = new ArrayList<String>();
		if (classMeta != null && !CollectionUtils.isEmpty(classMeta.getAttributes())) {
			for (AttributeMeta attributeMeta : classMeta.getAttributes()) {
				if (comp != null && comp.getDbAttrRead().equals(attributeMeta.getAttributeId())) {
					comp.setAttributeName(attributeMeta.getAttributeName());
				}
				ret.add(attributeMeta.getAttributeName());
			}
			/*
			 * if (classMeta.getTaskCodeRequired()) { ret.add(new String("taskCode")); }
			 */
		}
		return ret;
	}

	@SuppressWarnings("unused")
	public Map<Integer, Object> getDataForTable(Protocol protocol, String dbClassRead, List<String> displayedColumns,
			Collection<String> ownerEmployeeIds, Map<String, String> filterAttributes)
			throws InstanceRelatedException, ClassRelatedException {
		Map<Integer, Object> tableFinal = new HashMap<>();
		if (ownerEmployeeIds == null)
			return tableFinal;
		List<Object[]> tableData = instanceService.getNestedDataForColumns(protocol, dbClassRead, displayedColumns,
				ownerEmployeeIds, filterAttributes);
		Map<Integer, Object> table = tableData.stream().collect(Collectors.toMap(a -> (int) a[0],
				a -> Arrays.copyOfRange(a, 1, displayedColumns.size() + 1), (a1, a2) -> {
					return a1;
				}));
		one: for (Integer key : table.keySet()) {
			Object obj[] = (Object[]) table.get(key);
			two: for (int i = 0; i < obj.length; i++) {
				boolean put = false;
				if (obj[i] != null) {
					put = true;
				}

				if (put) {
					tableFinal.put(key, obj);
					break two;
				}
			}
		}
		/*
		 * if(tableFinal.size()<=0) { tableFinal = table; }
		 */
		return tableFinal;
	}

	private Map<Integer, Object> getDataForInstanceTable(Protocol protocol, String dbClassRead,
			List<String> displayedColumns, Map<String, String> filterAttributes, Integer instanceId)
			throws InstanceRelatedException, ClassRelatedException {
		List<Object[]> tableData = instanceService.getNestedInstanceDataForColumns(protocol, dbClassRead,
				displayedColumns, filterAttributes, instanceId);
		Map<Integer, Object> table = tableData.stream()
				.collect(Collectors.toMap(a -> (int) a[0], a -> Arrays.copyOfRange(a, 1, displayedColumns.size() + 1)));
		Map<Integer, Object> tableFinal = new HashMap<>();
		one: for (Integer key : table.keySet()) {
			Object obj[] = (Object[]) table.get(key);
			two: for (int i = 0; i < obj.length; i++) {
				boolean put = false;
				if (obj[i] != null) {
					put = true;
				}

				if (put) {
					tableFinal.put(key, obj);
					break two;
				}
			}
		}
		/*
		 * if(tableFinal.size()<=0) { tableFinal = table; }
		 */
		return tableFinal;
	}

	private Map<Integer, Object> getFilteringDataForInstanceTable(Protocol protocol, String dbClassRead,
			List<String> columns) throws InstanceRelatedException, ClassRelatedException {
		List<Object[]> tableData = instanceService.getNestedInstanceDataForColumnsForFiltering(protocol, dbClassRead,
				columns);
		Map<Integer, Object> table = tableData.stream()
				.collect(Collectors.toMap(a -> (int) a[0], a -> Arrays.copyOfRange(a, 1, columns.size() + 1)));
		Map<Integer, Object> tableFinal = new HashMap<>();
		one: for (Integer key : table.keySet()) {
			Object obj[] = (Object[]) table.get(key);
			two: for (int i = 0; i < obj.length; i++) {
				boolean put = false;
				if (obj[i] != null) {
					put = true;
				}

				if (put) {
					tableFinal.put(key, obj);
					break two;
				}
			}
		}
		/*
		 * if(tableFinal.size()<=0) { tableFinal = table; }
		 */
		return tableFinal;
	}

	protected boolean updateFormWithSecurityDetails(FormUpdateRequest formUpdateRequest, Protocol protocol,
			AltOneCustomFormSecurityRequest formSecurityRequest) throws FormRelatedException {

		Map<String, String> nameClientIdMap = new HashMap<String, String>();
		Map<String, Long> nameHrFormFieldIdMap = new HashMap<String, Long>();
		for (FieldGroup fieldGroup : formSecurityRequest.getFieldGroups()) {
			for (Field field : fieldGroup.getFields()) {
				String clientId = field.getClientId();
				String compName = clientId.split(":")[(clientId.split(":").length) - 1];
				nameClientIdMap.put(compName, clientId);

				nameHrFormFieldIdMap.put(compName, field.getHrFormFieldId());
			}
		}

		for (AltAbstractComponent component : formUpdateRequest.getComponentList()) {

			if (component.getControlType().equals(EnumHTMLControl.PANEL)) {
				((AltPanelComponent) component).getComponentList().forEach(c -> {
					c.setClientId(nameClientIdMap.get(c.getName()));
					c.setHrFormFieldId(nameHrFormFieldIdMap.get(c.getName()));
				});
			} else {
				component.setClientId(nameClientIdMap.get(component.getName()));
				component.setHrFormFieldId(nameHrFormFieldIdMap.get(component.getName()));
			}
		}

		FormRequest request = FormRequest.builder().withComponentList(formUpdateRequest.getComponentList())
				.withVersion(formUpdateRequest.getVersionId()).withProtocol(protocol)
				.withFormId(Integer.valueOf(formUpdateRequest.getUiClassCode()))
				.withState(formUpdateRequest.getFormState()).build();
		AltForm formAfterUpdation = formService.updateFormSecurity(request,
				formSecurityRequest.getHrFormId().toString());

		return !(formAfterUpdation == null);

	}

	public List<AttributeMeta> getAllAttributesByClassID(Integer metaclassid, Protocol protocol)
			throws ReferenceRelatedException {
		return iReferenceService.getAllAttributesByClassID(metaclassid, protocol);

	}

	public HashMap<String, List<AltOption>> getAllAttributeValuesByAttributeName(List<AttributeMeta> fetchedAttributes,
			Protocol protocol) throws InstanceRelatedException {
		return instanceService.getAllAttributeValuesByAttributeName(fetchedAttributes, protocol);
	}

	public ObjectReaderRequestTO getObjectReaderRequestTO(String objectCode, String objectAttrCode, Protocol protocol,
			String tenantId) {
		ObjectReaderRequestTO requestObj = new ObjectReaderRequestTO();

		List<ObjectAttributeTO> requestObjAttrTOList = new ArrayList<>();

		ObjectAttributeTO requestAttrTO = new ObjectAttributeTO();
		requestAttrTO.setObjectCode(objectCode);
		requestAttrTO.setObjectAttrCode(objectAttrCode);
		requestObjAttrTOList.add(requestAttrTO);

		if (objectCode.equalsIgnoreCase("EMPNEW")) {
			requestAttrTO = new ObjectAttributeTO();
			requestAttrTO.setObjectCode(objectCode);
			requestAttrTO.setObjectAttrCode("EMPLOYEEID");
			requestObjAttrTOList.add(requestAttrTO);
		}

		requestObj.setRequestedAttrs(requestObjAttrTOList);

		List<ObjectAttributeTO> filterObjAttrTOList = new ArrayList<>();
		ObjectAttributeTO filterAttrTO = new ObjectAttributeTO();
		filterAttrTO.setObjectCode(objectCode);
		filterAttrTO.setObjectAttrCode("CARDKEY");
		filterAttrTO.setValue(tenantId + "\\_");
		// filterAttrTO.setValue(tenantId);
		filterAttrTO.setOperator("like");
		filterAttrTO.setOperatorPosition("startsWith");
		filterObjAttrTOList.add(filterAttrTO);
		ObjectAttributeTO filterAttrTO2 = new ObjectAttributeTO();
		filterAttrTO2.setObjectCode(objectCode);
		filterAttrTO2.setObjectAttrCode("TENANTID");
		filterAttrTO2.setValue(tenantId);
		filterAttrTO2.setValues(null);
		// filterAttrTO.setValue(tenantId);
		filterAttrTO2.setOperator("=");
		filterObjAttrTOList.add(filterAttrTO2);
		ObjectAttributeTO filterAttrTO3 = new ObjectAttributeTO();
		filterAttrTO3.setObjectCode(objectCode);
		filterAttrTO3.setObjectAttrCode("ISACTIVE");
		filterAttrTO3.setValue("1");
		filterAttrTO3.setOperator("=");
		filterAttrTO3.setValues(null);
		filterObjAttrTOList.add(filterAttrTO3);
		requestObj.setFilterAttrs(filterObjAttrTOList);

		return requestObj;
	}

	/**
	 * {@link #getGlobalAllAttributeValuesByAttributeName(AltAbstractComponent, Protocol)}
	 * : fetches list of values for each component marked as global.
	 * 
	 * @throws NoSuchAlgorithmException, IOException,RuntimeException
	 * 
	 * @return HashMap<String, List<String>>
	 * @author vaibhav.kashyap
	 * @param formRoles
	 */
	@SuppressWarnings("rawtypes")
	public HashMap<String, List<AltOption>> getGlobalAllAttributeValuesByAttributeName(
			AltAbstractComponent eachComponent, Protocol protocol, String tenantId, Collection<RoleTO> formRoles) {
		HBaseObjectResponseTO response;
		HttpResponse rawResponse = null;
		HashMap<String, List<AltOption>> valuesByAttribute = null;

		String objectReaderUrl = objectReaderConf.getDeployedreader_url();
		String getGlobalAttributeValuesEndpoint = objectReaderUrl + "api/v1/empgroup/getObjectData";
		try {
			if (eachComponent != null && ((AltGroupComponent) eachComponent).getDbClassRead() != null
					&& ((AltGroupComponent) eachComponent).getDbClassRead() != null) {
				String objectCode = ((AltGroupComponent) eachComponent).getDbClassRead();
				String objectAttrCode = ((AltGroupComponent) eachComponent).getDbAttrRead();
				rawResponse = HttpService.getObjectReaderData(getGlobalAttributeValuesEndpoint,
						getObjectReaderRequestTO(objectCode, objectAttrCode, protocol, tenantId));
				if (rawResponse != null) {
					valuesByAttribute = new HashMap<>();
					LOGGER.warn("Response from object reader ", rawResponse.getEntity().getContent());
					response = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
							HBaseObjectResponseTO.class);
					if (response.getData() != null && response.getData().size() > 0) {

						Set<Integer> reportingEmployeeIds = new HashSet<Integer>();
						if (objectCode.equalsIgnoreCase("EMPNEW")) {
							reportingEmployeeIds = this.getReportingEmployeeIds(protocol, formRoles);
						}

						List<Map<String, String>> temp = response.getData();
						List<AltOption> values = new ArrayList<>();
						for (Map<String, String> childmap : temp) {
							for (String childkey : childmap.keySet()) {
								if (!objectCode.equalsIgnoreCase("EMPNEW")) {
									values.add(new AltOption(childmap.get(childkey), childmap.get(childkey)));
								} else if (!childkey.equalsIgnoreCase("EMPLOYEEID")) {
									if (childmap.get("EMPLOYEEID") == null || childmap.get("EMPLOYEEID").isEmpty())
										break;
									Integer employeeId = Integer.parseInt(childmap.get("EMPLOYEEID"));
									if (reportingEmployeeIds.contains(employeeId))
										values.add(new AltOption(childmap.get(childkey), childmap.get("EMPLOYEEID")));
								}
							}
						}
						valuesByAttribute.put(objectCode + "_" + objectAttrCode, values);
					}
				}
			}

		} catch (NoSuchAlgorithmException | IOException | RuntimeException e) {
			LOGGER.error("Error while reading response from Object Reader", e);
		} finally {

		}

		return valuesByAttribute;
	}

	public String getGlobalSingleAttributeValueByAttributeName(AltAbstractBaseComponent component, Protocol protocol,
			String tenantId) {
		HBaseObjectResponseTO response;
		HttpResponse rawResponse = null;
		String objectReaderUrl = objectReaderConf.getDeployedreader_url();
		String globalAttributeValuesEndpoint = objectReaderUrl + "api/v1/empgroup/getObjectData";
		String value = null;

		try {
			if (component != null && component.getDbClassRead() != null && component.getDbClassRead() != null) {
				rawResponse = HttpService.getObjectReaderData(globalAttributeValuesEndpoint,
						getObjectReaderRequestTOByFilter(component.getDbClassRead(), component.getDbAttrRead(),
								protocol, tenantId));
				if (rawResponse != null) {
					LOGGER.warn("Response from object reader ", rawResponse.getEntity().getContent());
					response = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
							HBaseObjectResponseTO.class);
					if (response.getData() != null && response.getData().size() > 0) {
						List<Map<String, String>> temp = response.getData();
						for (Map<String, String> childmap : temp) {
							for (String childkey : childmap.keySet()) {
								value = childmap.get(childkey);
							}
						}
					}
				}
			}

		} catch (NoSuchAlgorithmException | IOException | RuntimeException e) {
			LOGGER.error("Error while reading response from Object Reader", e);
		} finally {

		}

		return value;
	}

	private ObjectReaderRequestTO getObjectReaderRequestTOByFilter(String objectCode, String objectAttrCode,
			Protocol protocol, String tenantId) {
		ObjectReaderRequestTO requestObj = new ObjectReaderRequestTO();

		List<ObjectAttributeTO> requestObjAttrTOList = new ArrayList<>();
		ObjectAttributeTO requestAttrTO = new ObjectAttributeTO();
		requestAttrTO.setObjectCode(objectCode);
		requestAttrTO.setObjectAttrCode(objectAttrCode);
		requestObjAttrTOList.add(requestAttrTO);
		requestObj.setRequestedAttrs(requestObjAttrTOList);

		List<ObjectAttributeTO> filterObjAttrTOList = new ArrayList<>();
		ObjectAttributeTO filterAttrTO = null;
		filterAttrTO = new ObjectAttributeTO();
		filterAttrTO.setObjectCode(objectCode);
		filterAttrTO.setObjectAttrCode("EMPLOYEEID");
		filterAttrTO.setValue(protocol.getEmployeeId().toString());
		filterAttrTO.setOperator("=");
		ObjectAttributeTO filterAttrTO2 = new ObjectAttributeTO();
		filterAttrTO2.setObjectCode(objectCode);
		filterAttrTO2.setObjectAttrCode("TENANTID");
		filterAttrTO2.setValue(tenantId);
		filterAttrTO2.setValues(null);
		// filterAttrTO.setValue(tenantId);
		filterAttrTO2.setOperator("=");
		ObjectAttributeTO filterAttrTO3 = new ObjectAttributeTO();
		filterAttrTO3.setObjectCode(objectCode);
		filterAttrTO3.setObjectAttrCode("ISACTIVE");
		filterAttrTO3.setValue("1");
		filterAttrTO3.setOperator("=");
		filterAttrTO3.setValues(null);
		filterObjAttrTOList.add(filterAttrTO3);
		filterObjAttrTOList.add(filterAttrTO2);
		filterObjAttrTOList.add(filterAttrTO);
		requestObj.setFilterAttrs(filterObjAttrTOList);

		return requestObj;
	}

	public ObjectReaderRequestTO getObjectReaderRequestTOByFilter(String objectCode, List<String> objectAttrCodes,
			Protocol protocol, Integer employeeId) {
		ObjectReaderRequestTO requestObj = new ObjectReaderRequestTO();

		List<ObjectAttributeTO> requestObjAttrTOList = new ArrayList<>();
		objectAttrCodes.forEach(objectAttrCode -> {
			ObjectAttributeTO requestAttrTO = new ObjectAttributeTO();
			requestAttrTO.setObjectCode(objectCode);
			requestAttrTO.setObjectAttrCode(objectAttrCode);
			requestObjAttrTOList.add(requestAttrTO);
		});

		requestObj.setRequestedAttrs(requestObjAttrTOList);

		List<ObjectAttributeTO> filterObjAttrTOList = new ArrayList<>();
		ObjectAttributeTO filterAttrTO = null;
		filterAttrTO = new ObjectAttributeTO();
		filterAttrTO.setObjectCode(objectCode);
		filterAttrTO.setObjectAttrCode("CARDKEY");
		filterAttrTO.setValue(protocol.getTenantId().toString() + "_" + employeeId.toString());
		filterAttrTO.setOperator("=");
		ObjectAttributeTO filterAttrTO2 = new ObjectAttributeTO();
		filterAttrTO2.setObjectCode(objectCode);
		filterAttrTO2.setObjectAttrCode("TENANTID");
		filterAttrTO2.setValue(protocol.getTenantId().toString());
		filterAttrTO2.setValues(null);
		// filterAttrTO.setValue(tenantId);
		filterAttrTO2.setOperator("=");
		ObjectAttributeTO filterAttrTO3 = new ObjectAttributeTO();
		filterAttrTO3.setObjectCode(objectCode);
		filterAttrTO3.setObjectAttrCode("ISACTIVE");
		filterAttrTO3.setValue("1");
		filterAttrTO3.setOperator("=");
		filterAttrTO3.setValues(null);
		filterObjAttrTOList.add(filterAttrTO3);
		filterObjAttrTOList.add(filterAttrTO2);
		filterObjAttrTOList.add(filterAttrTO);
		requestObj.setFilterAttrs(filterObjAttrTOList);

		return requestObj;
	}

	public Map<String, String> getGlobalSingleAttributeList(List<String> objectAttrList, Protocol protocol,
			Integer employeeId) {
		HBaseObjectResponseTO response;
		HttpResponse rawResponse = null;
		String objectReaderUrl = objectReaderConf.getDeployedreader_url();
		String globalAttributeValuesEndpoint = objectReaderUrl + "api/v1/empgroup/getObjectData";

		try {
			if (objectAttrList != null && objectAttrList.size() > 0) {
				rawResponse = HttpService.getObjectReaderData(globalAttributeValuesEndpoint,
						getObjectReaderRequestTOByFilter("EMPNEW", objectAttrList, protocol, employeeId));
				if (rawResponse != null) {
					LOGGER.warn("Response from object reader ", rawResponse.getEntity().getContent());
					response = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
							HBaseObjectResponseTO.class);
					if (response.getData() != null && response.getData().size() > 0) {
						List<Map<String, String>> temp = response.getData();
						return temp.get(0);
					}
				}
			}

		} catch (NoSuchAlgorithmException | IOException | RuntimeException e) {
			LOGGER.error("Error while reading response from Object Reader", e);
		} finally {

		}

		return null;
	}

	public Set<Integer> getReportingEmployeeIds(Protocol protocol, Collection<RoleTO> formRoles) {
		Set<Integer> reportingEmployeeIds = new HashSet<Integer>();

		if (formRoles == null || formRoles.isEmpty())
			return reportingEmployeeIds;

		ReportingEmployeesRequestResponseTO requestResponseTO = new ReportingEmployeesRequestResponseTO();
		requestResponseTO.setOrganizationId(protocol.getOrgId());
		requestResponseTO.setUserId(protocol.getUserId());
		requestResponseTO.setRoleIds(new HashSet<Integer>());
		for (RoleTO role : formRoles) {
			requestResponseTO.getRoleIds().add(role.getRoleId());
		}
		try {
			HttpResponse rawResponse = HttpService.callRestAPI(securityPropertiesConf.getReportingEmployees()
			/*
			 * "https://s2demo.sohum.com/service/api/hris/reportingEmployees"
			 */, requestResponseTO);
			requestResponseTO = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
					ReportingEmployeesRequestResponseTO.class);
			reportingEmployeeIds = requestResponseTO.getEmployeeIds();
		} catch (Exception e) {
		}
		return reportingEmployeeIds;
	}
}

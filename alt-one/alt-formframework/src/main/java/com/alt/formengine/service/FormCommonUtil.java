package com.alt.formengine.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alt.datacarrier.formframework.request.EnumFormMappingType;
import com.alt.datacarrier.formframework.request.FormCreateRequest;
import com.alt.datacarrier.formframework.request.FormMapping;
import com.alt.datacarrier.formframework.request.FormUpdateRequest;
import com.alt.datacarrier.formsecurity.core.AltOneCustomFormSecurityRequest;
import com.alt.datacarrier.formsecurity.core.CRUDRequestType;
import com.alt.datacarrier.formsecurity.core.Field;
import com.alt.datacarrier.formsecurity.core.FieldGroup;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.AltCustomFormDataContextFilter;
import com.alt.datacarrier.kernel.uiclass.AltCustomFormDataContextRequest;
import com.alt.datacarrier.kernel.uiclass.AltPanelComponent;
import com.alt.datacarrier.kernel.uiclass.AltTabbedPanelComponent;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datacarrier.kernel.uiclass.EnumHTMLControl;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.formengine.util.CommonUtil;

public class FormCommonUtil {

	protected static Map<String, Object> createFormMappingAttributes(FormMapping formMapping) {
		Map<String, Object> attributes = new HashMap<>();
		attributes.put("customFieldGroupId", formMapping.getCustomFieldGroupId());
		attributes.put("customFieldGroupName", formMapping.getCustomFieldGroupName());
		attributes.put("customFormId", formMapping.getCustomFormId());
		attributes.put("customFormType", formMapping.getCustomFormType());
		attributes.put("formId", formMapping.getFormId());
		attributes.put("organization", formMapping.getOrganization());
		attributes.put("orgId", formMapping.getOrgId());
		attributes.put("tenantId", formMapping.getTenantId());
		return attributes;
	}

	protected static AltOneCustomFormSecurityRequest createFormSecurityRequest(List<AltAbstractComponent> componentList,
			Object[] formMappingInstance, String name) {
		AltOneCustomFormSecurityRequest formSecurityRequest = new AltOneCustomFormSecurityRequest();
		formSecurityRequest.setFormName(name);
		formSecurityRequest.setOrganizationID(CommonUtil.getDbAttrValueInLong(formMappingInstance[3]));
		formSecurityRequest.setTenantID(CommonUtil.getDbAttrValueInLong(formMappingInstance[4]));
		EnumFormMappingType type = EnumFormMappingType
				.valueOf(CommonUtil.getDbAttrValueInString(formMappingInstance[1]));
		if (type == EnumFormMappingType.CUSTOMFORM) {
			formSecurityRequest.setFormRequestType(CRUDRequestType.CREATE);
			formSecurityRequest.setCustomComponentAllowed(true);
			formSecurityRequest.setFieldGroups(convertToFieldGroups(componentList, name));
		} else if (type == EnumFormMappingType.CUSTOMFORMFIELD) {
			formSecurityRequest.setHrFormId(CommonUtil.getDbAttrValueInLong(formMappingInstance[5]));
			formSecurityRequest.setFormRequestType(CRUDRequestType.CHILD_UPDATE);
			formSecurityRequest.setFieldGroups(
					convertToFieldGroups(componentList, CommonUtil.getDbAttrValueInString(formMappingInstance[0]),
							CommonUtil.getDbAttrValueInString(formMappingInstance[6]), name, false));
		} else if (type == EnumFormMappingType.CUSTOMFORMFIELDNEW) {
			formSecurityRequest.setHrFormId(CommonUtil.getDbAttrValueInLong(formMappingInstance[5]));
			formSecurityRequest.setFormRequestType(CRUDRequestType.CHILD_UPDATE);
			formSecurityRequest.setFieldGroups(
					convertToFieldGroups(componentList, CommonUtil.getDbAttrValueInString(formMappingInstance[0]),
							CommonUtil.getDbAttrValueInString(formMappingInstance[6]), name, true));
		}
		return formSecurityRequest;
	}

	protected static List<FieldGroup> convertToFieldGroups(List<AltAbstractComponent> componentList,
			String fieldGroupIdString, String filedGroupName, String formName, boolean newCustomFormField) {
		String clientId = null;
		Long fieldGroupId = Long.parseLong(fieldGroupIdString);
		List<FieldGroup> fieldGroups = new ArrayList<>();
		FieldGroup fieldGroup = new FieldGroup();
		fieldGroup.setFieldGroupRequestType(CRUDRequestType.CHILD_UPDATE);
		if (newCustomFormField) {
			fieldGroup.setHrFieldGroupName(formName + "." + filedGroupName);
			clientId = formName + ":" + fieldGroupId;
		} else {
			fieldGroup.setHrFormFieldGroupId(fieldGroupId);
			clientId = formName + ":" + filedGroupName;
		}
		List<Field> fields = generateFields(componentList, clientId, false);
		fieldGroup.setFields(fields);
		fieldGroups.add(fieldGroup);
		return fieldGroups;

	}

	private static List<Field> generateFields(List<AltAbstractComponent> componentList, String clientId,
			boolean isNewCustomForm) {
		List<Field> childTabPanelFields = new ArrayList<>();
		List<Field> childPanelFields = new ArrayList<>();
		List<Field> fields = new ArrayList<>();
		for (AltAbstractComponent component : componentList) {
			if (component.getControlType().equals(EnumHTMLControl.TAB_PANEL)) {
				AltTabbedPanelComponent tabpanel = (AltTabbedPanelComponent) component;
				childTabPanelFields = generateFields(tabpanel, clientId);
			} else if (component.getControlType().equals(EnumHTMLControl.PANEL)) {
				AltPanelComponent panel = (AltPanelComponent) component;
				childPanelFields.addAll(generateFields(panel, clientId));
			} else {
				Field field = new Field();
				String fieldClientId = null;
				if (isNewCustomForm) {
					fieldClientId = clientId + "DefaultPanel:" + component.getName();
				} else {
					fieldClientId = clientId + ":" + component.getName();
				}
				fieldClientId = fieldClientId.replace(":.", ":");
				field.setClientId(fieldClientId);
				field.setFieldRequestType(CRUDRequestType.CREATE);
				field.setActionFlag(component.getControlType().equals(EnumHTMLControl.BUTTON));
				field.setHrFormFieldId(component.getHrFormFieldId());
				fields.add(field);
			}
		}
		fields.addAll(childTabPanelFields);
		fields.addAll(childPanelFields);
		return fields;
	}

	private static List<Field> generateFields(AltTabbedPanelComponent tabpanel, String clientId) {
		clientId += "." + tabpanel.getName();
		List<Field> childPannelFields = new ArrayList<>();
		for (AltPanelComponent panel : tabpanel.getPanels()) {
			childPannelFields = generateFields(panel, clientId);
		}
		return childPannelFields;
	}

	private static List<Field> generateFields(AltPanelComponent panel, String clientId) {
		clientId += "." + panel.getName();
		List<Field> childPannelFields = new ArrayList<>();
		List<Field> childTabPannelFields = new ArrayList<>();
		List<Field> fields = new ArrayList<>();
		for (AltAbstractComponent component : panel.getComponentList()) {
			if (component.getControlType().equals(EnumHTMLControl.PANEL)) {
				AltPanelComponent childPanel = (AltPanelComponent) component;
				childPannelFields = generateFields(childPanel, clientId);
			} else if (component.getControlType().equals(EnumHTMLControl.TAB_PANEL)) {
				AltTabbedPanelComponent childTabPanel = (AltTabbedPanelComponent) component;
				childTabPannelFields = generateFields(childTabPanel, clientId);
			} else {
				Field field = new Field();
				String fieldClientId = clientId + ":" + component.getName();
				fieldClientId = fieldClientId.replace(":.", ":");
				field.setClientId(fieldClientId);
				field.setFieldRequestType(CRUDRequestType.CREATE);
				field.setActionFlag(component.getControlType().equals(EnumHTMLControl.BUTTON));
				field.setHrFormFieldId(component.getHrFormFieldId());
				fields.add(field);
			}
		}
		fields.addAll(childPannelFields);
		fields.addAll(childTabPannelFields);
		return fields;
	}

	private static List<FieldGroup> convertToFieldGroups(List<AltAbstractComponent> componentList, String formName) {
		List<FieldGroup> fieldGroups = new ArrayList<>();
		FieldGroup fieldGroup = new FieldGroup();
		fieldGroup.setFieldGroupRequestType(CRUDRequestType.CREATE);
		fieldGroup.setHrFieldGroupName(formName + "." + "DefaultPanel");
		String clientId = formName + ":";

		List<Field> fields = generateFields(componentList, clientId, true);

		fieldGroup.setFields(fields);
		fieldGroups.add(fieldGroup);
		return fieldGroups;
	}

	protected static boolean validateForm(FormCreateRequest formCreateRequest) {
		return formCreateRequest.getUiClassName().length() > 0;
	}

	protected static void validate(FormUpdateRequest formUpdateRequest, Integer orgId, String userName)
			throws FormRelatedException {
		if (formUpdateRequest.getFormState() == null) {
			throw new FormRelatedException("Form state cannot be null in update form request.");
		} else if (formUpdateRequest.getFormState() == EnumFormState.OBSOLETE) {
			throw new FormRelatedException("Obselete form cannot be updated");
		}
		if (orgId == null || orgId <= 0) {
			throw new FormRelatedException("orgId cannot be null/empty inside protocol in  update form request.");
		}
		if (userName == null || userName.isEmpty()) {
			throw new FormRelatedException("userName cannot be null/empty inside protocol in update form request.");
		}
	}

	protected static void validate(List<AltCustomFormDataContextRequest> formContexts) throws FormRelatedException {
		for (AltCustomFormDataContextRequest formContext : formContexts) {
			if (formContext.getContext().getContextType() == null
					|| formContext.getContext().getContextType().trim().length() == 0)
				throw new FormRelatedException("Context is missing in the contextRequest.");
			if (!isCharInputOnly(formContext.getContext().getContextType()))
				throw new FormRelatedException("Context data not valid.");
			if (formContext.getContext().getContextType().length() > 100)
				throw new FormRelatedException("Context length should not more than 100.");
			for (AltCustomFormDataContextFilter filter : formContext.getContextFilter()) {
				if (filter.getContextLabel() == null || filter.getContextLabel().trim().length() == 0)
					throw new FormRelatedException("contextLabel is missing in the contextFilter.");
				if (filter.getContextValue() == null || filter.getContextValue().trim().length() == 0)
					throw new FormRelatedException("contextValue is missing in the contextFilter.");
				if (filter.getContextLabel().length() > 100)
					throw new FormRelatedException("contextLabel length should not more than 100.");
				if (filter.getContextValue().length() > 100)
					throw new FormRelatedException("contextValue length should not more than 100.");
				if (!isCharInputOnly(filter.getContextLabel()))
					throw new FormRelatedException(
							"ContextLabel data not valid for label : " + filter.getContextLabel());
				if (!isAlphanumeric(filter.getContextValue()))
					throw new FormRelatedException(
							"ContextValue data not valid for value : " + filter.getContextValue());
			}
		}
	}

	protected static FormMapping convertObjectToFormMapping(Object[] instance) {
		EnumFormMappingType customFormType = EnumFormMappingType
				.valueOf(CommonUtil.getDbAttrValueInString(instance[1]));
		String organization = CommonUtil.getDbAttrValueInString(instance[2]);
		String orgId = CommonUtil.getDbAttrValueInString(instance[3]);
		String tenantId = CommonUtil.getDbAttrValueInString(instance[4]);
		String customFormId = CommonUtil.getDbAttrValueInString(instance[5]);
		String customFieldGroupId = CommonUtil.getDbAttrValueInString(instance[0]);
		String customFieldGroupName = CommonUtil.getDbAttrValueInString(instance[6]);

		FormMapping mapping = new FormMapping.Builder().withCustomFormType(customFormType)
				.withCustomFormId(customFormId).withCustomFieldGroupId(customFieldGroupId)
				.withCustomFieldGroupName(customFieldGroupName).withOrganization(organization)
				.withOrgId(Integer.parseInt(orgId)).withTenantId(tenantId).build();
		CommonUtil.getDbAttrValueInString(instance[0]);
		return mapping;
	}

	public static Map<String, Object> createFormDataMappingAttributes(AltForm formData,
			AltCustomFormDataContextRequest contextReq) {
		Map<String, Object> attributes = new HashMap<>();
		attributes.put("context", contextReq.getContext());
		attributes.put("contextFilter", contextReq.getContextFilter());
		attributes.put("formData", formData);
		return attributes;
	}

	public static boolean isAlphanumeric(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c))
				return false;
		}
		return true;
	}

	public static boolean isCharInputOnly(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (!Character.isLetter(c))
				return false;
		}
		return true;
	}

	public static String getJsonFromContextFilter(AltCustomFormDataContextFilter contextFilter) {
		return "{ " + "\"contextLabel\" : \"" + contextFilter.getContextLabel() + "\" , \"contextValue\" : \""
				+ contextFilter.getContextValue() + "\" }";
	}

}

package com.alt.formengine.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import com.alt.business.util.FileWriterUtil;
import com.alt.datacarrier.common.enumeration.EnumDataType;
import com.alt.datacarrier.common.enumeration.EnumSortDirection;
import com.alt.datacarrier.communication.CommunicationRequestResponseTO;
import com.alt.datacarrier.communication.MailConfigTO;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.formframework.request.CommEnvlp;
import com.alt.datacarrier.formframework.request.DownloadDataTableRequestResponse;
import com.alt.datacarrier.formframework.request.EnumFormMappingType;
import com.alt.datacarrier.formframework.request.FormCRUDData;
import com.alt.datacarrier.formframework.request.FormCreateRequest;
import com.alt.datacarrier.formframework.request.FormDataRetrieveRequest;
import com.alt.datacarrier.formframework.request.FormDataSaveRequest;
import com.alt.datacarrier.formframework.request.FormInstanceFetchRequest;
import com.alt.datacarrier.formframework.request.FormInstanceUpdateRequest;
import com.alt.datacarrier.formframework.request.FormMapping;
import com.alt.datacarrier.formframework.request.FormUpdateRequest;
import com.alt.datacarrier.formframework.request.GlobalTextfieldReloadRequestResponse;
import com.alt.datacarrier.formframework.request.ReloadLocalDataTableRequestResponse;
import com.alt.datacarrier.formframework.request.UploadDataTableRequestResponse;
import com.alt.datacarrier.formframework.response.FormCreateResponse;
import com.alt.datacarrier.formframework.response.FormDetails;
import com.alt.datacarrier.formframework.response.FormInstanceResponse;
import com.alt.datacarrier.formsecurity.AltComponentSecurity;
import com.alt.datacarrier.formsecurity.SysCredentialTO;
import com.alt.datacarrier.formsecurity.UiFormSecurityTO;
import com.alt.datacarrier.formsecurity.core.AltOneCustomFormSecurityRequest;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.core.AttributeMeta;
import com.alt.datacarrier.kernel.db.core.AttributesData;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.core.DbFilters;
import com.alt.datacarrier.kernel.db.core.DynamicReadRequest;
import com.alt.datacarrier.kernel.db.core.DynamicReadResponse;
import com.alt.datacarrier.kernel.db.core.InstanceData;
import com.alt.datacarrier.kernel.db.core.OrderAttribute;
import com.alt.datacarrier.kernel.db.core.ProjectionAttribute;
import com.alt.datacarrier.kernel.db.request.CreateClassRequest;
import com.alt.datacarrier.kernel.db.request.FormRequest;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datacarrier.kernel.db.request.TaskCodeRecordTransport;
import com.alt.datacarrier.kernel.uiclass.AltAbstractBaseComponent;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.AltButtonComponent;
import com.alt.datacarrier.kernel.uiclass.AltCustomFormDataContextFilter;
import com.alt.datacarrier.kernel.uiclass.AltCustomFormDataContextRequest;
import com.alt.datacarrier.kernel.uiclass.AltDataTable;
import com.alt.datacarrier.kernel.uiclass.AltGroupComponent;
import com.alt.datacarrier.kernel.uiclass.AltOption;
import com.alt.datacarrier.kernel.uiclass.AltPanelComponent;
import com.alt.datacarrier.kernel.uiclass.AltTabbedPanelComponent;
import com.alt.datacarrier.kernel.uiclass.AltTextboxComponent;
import com.alt.datacarrier.kernel.uiclass.EnumComponentParentEnvironment;
import com.alt.datacarrier.kernel.uiclass.EnumFormState;
import com.alt.datacarrier.kernel.uiclass.EnumHTMLControl;
import com.alt.datacarrier.kernel.uiclass.FormNavigation;
import com.alt.datacarrier.organization.RoleTO;
import com.alt.datacarrier.workflow.ActionResponseTO;
import com.alt.datacarrier.workflow.ProtocolTO;
import com.alt.datacarrier.workflow.StageRequestTO;
import com.alt.datacarrier.workflow.StageResponseTO;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.MailRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.exception.ReferenceRelatedException;
import com.alt.datakernel.exception.TaskCodeRecordRelatedException;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.datakernel.service.IFormService;
import com.alt.datakernel.service.IInstanceService;
import com.alt.datakernel.service.ITaskCodeRecordService;
import com.alt.datakernel.service.impl.ClassService;
import com.alt.formengine.config.SecurityPropertiesConf;
import com.alt.formengine.exception.FormDataRelatedException;
import com.alt.formengine.util.CommonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class FormComponent {

	@Autowired(required = true)
	IDynamicQuery dynamicQuery;

	@Autowired
	IFormService formService;

	@Autowired
	DbService dbService;

	@Autowired
	IInstanceService instanceService;

	@Autowired
	FormCommonService formCommonService;

	@Autowired
	SecurityPropertiesConf securityPropertiesConf;

	@Autowired
	private ClassService classService;

	@Autowired
	ITaskCodeRecordService taskCodeRecordService;

	@Autowired
	FormDataComponent formDataComponent;

	@Autowired
	FileWriterUtil fileWriterUtil;

	private Logger LOGGER = LoggerFactory.getLogger(FormComponent.class);

	public ResponseEntity<List<FormDetails>> getForms(Protocol protocol, int limit, int offset, boolean published,
			int appId) throws QueryRelatedException {
		DynamicReadRequest request = new DynamicReadRequest();
		request = dbService.createDynamicQueryRequest(request, offset, limit, protocol.getOrgId(), published, appId);

		DynamicReadResponse response = dynamicQuery.read(request);
		List<Object[]> instances = response.getResponse();
		List<FormDetails> details = new ArrayList<>();
		for (Object[] instance : instances) {
			FormDetails detail = new FormDetails((String) instance[0], (String) instance[1],
					EnumFormState.values()[(Integer) instance[2]].name(), ((Integer) instance[3]).toString());
			details.add(detail);
		}
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Response", "form listing fetched successfully");
		return new ResponseEntity<>(details, headers, HttpStatus.OK);
	}

	public ResponseEntity<FormCreateResponse> deleteInstance(String instanceId, Protocol protocol)
			throws InstanceRelatedException, InstanceHistoryRelatedException {
		FormCreateResponse resp = new FormCreateResponse();
		if (instanceId == null || instanceId.equalsIgnoreCase("")) {
			resp.setResponseMessage("No Row Selected");
			resp.setSuccess(false);
		}
		InstanceData instanceData = null;

		try {
			InstanceRequest instanceRequest = new InstanceRequest.Builder().withInstanceId(Integer.parseInt(instanceId))
					.withProtocol(protocol).build();
			instanceData = instanceService.deleteInstance(instanceRequest);
			if (instanceData != null && instanceData.getInstanceId() != null) {
				if (instanceData.getStatus().equals(KernelConstants.EnumDocStatusType.INACTIVE)) {
					resp.setResponseMessage("Deleted Successfully!!!");
					resp.setSuccess(true);
				} else {
					resp.setResponseMessage("Deletion Failed!!!");
					resp.setSuccess(false);
				}
			} else {
				resp.setResponseMessage("Deletion Failed!!!");
				resp.setSuccess(false);
			}
		} catch (Exception ex) {
			resp.setResponseMessage("Deletion Failed!!!");
			resp.setSuccess(false);
		}

		return new ResponseEntity<>(resp, HttpStatus.OK);

	}

	public ResponseEntity<FormCRUDData> getForm(FormDataRetrieveRequest formDataRetrieveRequest, Protocol protocol)
			throws FormRelatedException, QueryRelatedException, ClassRelatedException, NoSuchAlgorithmException,
			IOException, InstanceRelatedException {
		AltForm form = null;
		if (formDataRetrieveRequest.getFormId() != null && !formDataRetrieveRequest.getFormId().trim().isEmpty()) {
			ReadRequest request = new ReadRequest(protocol, null, -1, -1, -1,
					Integer.valueOf(formDataRetrieveRequest.getFormId()), null, null);
			form = formService.readFromId(request);
		} else if (formDataRetrieveRequest.getInstanceId() != null
				&& !formDataRetrieveRequest.getInstanceId().trim().isEmpty()) {
			InstanceData instance = instanceService.read(protocol,
					Integer.parseInt(formDataRetrieveRequest.getInstanceId()));
			ReadRequest request = new ReadRequest(protocol, null, -1, -1, -1, instance.getFormId(), null, null);
			form = formService.readFromId(request);
		}

		if (form.getMetaClassId() != null && form.getMetaClassId() > 0) {
			ClassMeta classMeta = classService.read(protocol, form.getMetaClassId());
			if (classMeta != null && classMeta.getTaskCodeRequired() != null) {
				form.setTaskCodeRequired(classMeta.getTaskCodeRequired());
			}
		}

		/**
		 * Prepare a map of all the attributes along with the list of values for each.
		 */
		HashMap<String, List<AltOption>> attributeValuebyAttributeName = null;
		HashMap<String, List<String>> presavedUserValulesByComponent = null;
		try {
			/**
			 * if app id is null it is being called while publishing the form hence no need
			 * to fetch form attribute details in this flow
			 */
			if (form != null && formDataRetrieveRequest.getAppId() != null
					&& !formDataRetrieveRequest.getAppId().equalsIgnoreCase(""))
				attributeValuebyAttributeName = getAllAttributeValuesListOfForm(formDataRetrieveRequest.getTenantId(),
						form.getId(), protocol, form, formDataRetrieveRequest.getFormRoles());

			/**
			 * Load pre-saved values filled-in by user only if instance id is available &
			 * instance id will be available when it is being called from data table in edit
			 * mode
			 */
			if (formDataRetrieveRequest.getInstanceId() != null
					&& !formDataRetrieveRequest.getInstanceId().equalsIgnoreCase("")
					&& !formDataRetrieveRequest.getInstanceId().equalsIgnoreCase("undefined")) {
				presavedUserValulesByComponent = getUserSavedDataForFormByContext(formDataRetrieveRequest, protocol);
			}
			FormNavigation navigation = formDataRetrieveRequest.getNavigation();
			if (navigation != null) {
				if (presavedUserValulesByComponent == null) {
					presavedUserValulesByComponent = new HashMap<String, List<String>>();
				}
				List<String> values = new ArrayList<String>();
				values.add(navigation.getSourceComponentValue());
				presavedUserValulesByComponent.put(navigation.getTargetComponentName(), values);
			}

		} catch (FormDataRelatedException | InstanceRelatedException | ClassRelatedException
				| ReferenceRelatedException e) {
			LOGGER.error("Exception while fetching attributeValuebyAttributeName");
		}

		this.determineFormSecurity(form, formDataRetrieveRequest.getAssignedRoleIds(), protocol.getOrgId(), "EN",
				formDataRetrieveRequest.getStageId());

		ResponseEntity<FormCRUDData> response = formCommonService.convertToResponse(protocol, form,
				attributeValuebyAttributeName, presavedUserValulesByComponent, formDataRetrieveRequest.getFormRoles(),
				formDataRetrieveRequest.getInstanceId());

		return response;
	}

	/**
	 * {@link #getUserSavedDataForFormByContext(String, String, Protocol)} : Load
	 * user filled-in saved values if any, on form load
	 * 
	 * @throws InstanceRelatedException
	 * 
	 * @return HashMap<String, List<String>>
	 * @author vaibhav.kashyap
	 */
	public HashMap<String, List<String>> getUserSavedDataForFormByContext(
			FormDataRetrieveRequest formDataRetrieveRequest, Protocol protocol) {
		HashMap<String, List<String>> presavedUserValulesByComponent = null;
		if (formDataRetrieveRequest.getInstanceId() == null && formDataRetrieveRequest.getFormName() == null
				&& formDataRetrieveRequest.getFormName().equalsIgnoreCase("")
				&& formDataRetrieveRequest.getAppId() == null
				&& formDataRetrieveRequest.getAppId().equalsIgnoreCase(""))
			return null;
		InstanceRequest instanceRequest = null;

		if (formDataRetrieveRequest.getInstanceId() == null) {
			if ((formDataRetrieveRequest.getFormName() == null
					&& formDataRetrieveRequest.getFormName().equalsIgnoreCase(""))
					|| (formDataRetrieveRequest.getAppId() == null
							&& formDataRetrieveRequest.getAppId().equalsIgnoreCase(""))) {
				return null;
			} else {

				if (formDataRetrieveRequest.getFormId() != null
						&& !formDataRetrieveRequest.getFormId().equalsIgnoreCase("")
						&& !formDataRetrieveRequest.getFormId().equalsIgnoreCase("undefined")) {
					instanceRequest = new InstanceRequest.Builder().withProtocol(protocol)
							.withAppId(Integer.parseInt(formDataRetrieveRequest.getAppId()))
							.withFormName(formDataRetrieveRequest.getFormName())
							.withFormId(Integer.parseInt(formDataRetrieveRequest.getFormId())).build();
				} else {
					instanceRequest = new InstanceRequest.Builder().withProtocol(protocol)
							.withAppId(Integer.parseInt(formDataRetrieveRequest.getAppId()))
							.withFormName(formDataRetrieveRequest.getFormName()).build();
				}
			}
		} else {
			instanceRequest = new InstanceRequest.Builder()
					.withInstanceId(Integer.parseInt(formDataRetrieveRequest.getInstanceId())).build();
		}

		try {
			presavedUserValulesByComponent = instanceService.getSavedFormDataByContext(instanceRequest, protocol);
		} catch (Exception ex) {
			LOGGER.error("Exception while fetching presaved instance data");
			presavedUserValulesByComponent = null;
		}

		return presavedUserValulesByComponent;
	}

	/**
	 * {@link #getAllAttributeValuesListOfForm(String, Protocol, AltForm)} : Prepare
	 * a map of all the attributes along with the list of values for each.
	 * 
	 * @throws FormDataRelatedException,InstanceRelatedException, ClassRelatedException,
	 *                                                            QueryRelatedException,
	 *                                                            FormRelatedException,
	 *                                                            ReferenceRelatedException
	 * 
	 * @return HashMap<String, List<String>>
	 * @author vaibhav.kashyap
	 * @param formRoles
	 */
	public HashMap<String, List<AltOption>> getAllAttributeValuesListOfForm(String tenantId, String formId,
			Protocol protocol, AltForm form, Collection<RoleTO> formRoles)
			throws FormDataRelatedException, InstanceRelatedException, ClassRelatedException, QueryRelatedException,
			FormRelatedException, ReferenceRelatedException {
		HashMap<String, List<AltOption>> attributeValuebyAttributeName = null;
		try {
			attributeValuebyAttributeName = getFormComponentsAndAttributeValuesByFormID(tenantId, formId, protocol,
					form, formRoles);
			if (attributeValuebyAttributeName == null || attributeValuebyAttributeName.size() <= 0)
				attributeValuebyAttributeName = null;
		} catch (FormDataRelatedException | InstanceRelatedException | ClassRelatedException
				| ReferenceRelatedException e) {
			LOGGER.error("Exception while fetching attributeValuebyAttributeName");
			attributeValuebyAttributeName = null;
			e.printStackTrace();
		}

		return attributeValuebyAttributeName;
	}

	/**
	 * {@link #getFormComponentsAndAttributeValuesByFormID(String, Protocol, AltForm)}
	 * : Fetch all the components if required else get all values w.r.t. each
	 * attribute.
	 * 
	 * @throws FormDataRelatedException,InstanceRelatedException, ClassRelatedException,
	 *                                                            QueryRelatedException,
	 *                                                            FormRelatedException,
	 *                                                            ReferenceRelatedException
	 * 
	 * @return HashMap<String, List<String>>
	 * @author vaibhav.kashyap
	 * @param formRoles
	 */
	public HashMap<String, List<AltOption>> getFormComponentsAndAttributeValuesByFormID(String tenantId, String formId,
			Protocol protocol, AltForm form, Collection<RoleTO> formRoles)
			throws FormDataRelatedException, InstanceRelatedException, ClassRelatedException, QueryRelatedException,
			FormRelatedException, ReferenceRelatedException {
		AltForm formFetched = null;
		/**
		 * We need to have form instance which has list of all the components in it.
		 * Hence fetch if it isn't available. *
		 */
		if (form == null || form.getId() == null || form.getId().equalsIgnoreCase("")) {
			if (formId == null || formId.equalsIgnoreCase(""))
				return null;
			/**
			 * Fetch form instance
			 */
			ReadRequest request = new ReadRequest(protocol, null, -1, -1, -1, Integer.valueOf(formId), null, null);
			formFetched = formService.readFromId(request);
			if (formFetched == null || formFetched.getComponentList() == null
					|| formFetched.getComponentList().size() <= 0)
				return null;
		} else {
			formFetched = form;
		}
		/**
		 * After the components, go for their values.
		 */
		return getFormComponentvalues(tenantId, formId, formFetched.getComponentList(), formFetched.getMetaClassId(),
				protocol, formRoles);
	}

	/**
	 * {@link #getFormComponentvalues(String, List, Integer, Protocol)} : Check if
	 * the list has local or global attribute accordingly, call to fetch attribute
	 * values.
	 * 
	 * @throws ReferenceRelatedException, InstanceRelatedException
	 * 
	 * @return HashMap<String, List<String>>
	 * @author vaibhav.kashyap
	 * @param formRoles
	 */
	public HashMap<String, List<AltOption>> getFormComponentvalues(String tenantId, String formId,
			List<AltAbstractComponent> componentList, Integer metaClassId, Protocol protocol,
			Collection<RoleTO> formRoles) throws ReferenceRelatedException, InstanceRelatedException {
		HashMap<String, List<AltOption>> valuesByAttributeParent = new HashMap<>();
		Map<String, String> globalAttributeValues = formCommonService.getGlobalSingleAttributeList(
				getGlobalAttributesListForLoggedInEmployee(componentList), protocol, protocol.getEmployeeId());

		for (AltAbstractComponent component : componentList) {
			if (component.getControlType().equals(EnumHTMLControl.DROPDOWN)
					|| component.getControlType().equals(EnumHTMLControl.MULTI_SELECT)) {
				if (EnumComponentParentEnvironment.LOCAL.toString()
						.equalsIgnoreCase(((AltGroupComponent) component).getDbClassType())) {

					HashMap<String, List<AltOption>> valuesByAttribute = getLocalAllAttributeValuesByAttributeName(
							metaClassId, protocol);
					if (valuesByAttribute != null && !valuesByAttribute.isEmpty()) {
						for (String key : valuesByAttribute.keySet()) {
							if (!valuesByAttributeParent.containsKey(key)) {
								valuesByAttributeParent.put(key, valuesByAttribute.get(key));
							}
						}
					}
				} else if (EnumComponentParentEnvironment.GLOBAL.toString()
						.equalsIgnoreCase(((AltGroupComponent) component).getDbClassType())) {
					HashMap<String, List<AltOption>> valuesByAttribute = formCommonService
							.getGlobalAllAttributeValuesByAttributeName(((AltGroupComponent) component), protocol,
									tenantId, formRoles);
					if (valuesByAttribute != null && !valuesByAttribute.isEmpty()) {
						for (String key : valuesByAttribute.keySet()) {
							if (!valuesByAttributeParent.containsKey(key)) {
								valuesByAttributeParent.put(key, valuesByAttribute.get(key));
							}
						}
					}
				}
			} else if (component.getControlType().equals(EnumHTMLControl.TEXTFIELD)) {
				if (EnumComponentParentEnvironment.GLOBAL.toString().equalsIgnoreCase(
						((AltTextboxComponent) component).getDbClassType()) && null != globalAttributeValues) {
					String value = globalAttributeValues.get(((AltTextboxComponent) component).getDbAttrRead());
					((AltAbstractBaseComponent) component).setValue(value);
				}
			} else if (component.getControlType().equals(EnumHTMLControl.PANEL)) {
				valuesByAttributeParent.putAll(getFormComponentvalues(tenantId, formId,
						((AltPanelComponent) component).getComponentList(), metaClassId, protocol, formRoles));
			}
		}
		return valuesByAttributeParent;
	}

	private List<String> getGlobalAttributesListForLoggedInEmployee(List<AltAbstractComponent> componentList) {
		List<String> globalTextBoxAttributes = componentList.stream()
				.filter(component -> component.getControlType() == EnumHTMLControl.TEXTFIELD
						&& EnumComponentParentEnvironment.GLOBAL.toString()
								.equalsIgnoreCase(((AltTextboxComponent) component).getDbClassType())
						&& (((AltTextboxComponent) component).getDbAttrRead()) != null)
				.map(component -> (((AltTextboxComponent) component).getDbAttrRead())).collect(Collectors.toList());
		List<String> globalTextBoxAttributesPanel = componentList.stream()
				.filter(component -> component.getControlType() == EnumHTMLControl.PANEL)
				.flatMap(component -> ((AltPanelComponent) component).getComponentList().stream())
				.filter(component -> component.getControlType() == EnumHTMLControl.TEXTFIELD
						&& EnumComponentParentEnvironment.GLOBAL.toString()
								.equalsIgnoreCase(((AltTextboxComponent) component).getDbClassType())
						&& (((AltTextboxComponent) component).getDbAttrRead()) != null)
				.map(component -> (((AltTextboxComponent) component).getDbAttrRead())).collect(Collectors.toList());
		globalTextBoxAttributes.addAll(globalTextBoxAttributesPanel);
		return globalTextBoxAttributes;
	}

	public HashMap<String, List<AltOption>> getLocalAllAttributeValuesByAttributeName(Integer metaclassid,
			Protocol protocol) throws ReferenceRelatedException, InstanceRelatedException {
		// get all the attributes w.r.t metaclassid
		List<AttributeMeta> fetchedAttributes = formCommonService.getAllAttributesByClassID(metaclassid, protocol);
		HashMap<String, List<AltOption>> valuesByAttribute = null;
		if (fetchedAttributes != null && fetchedAttributes.size() > 0) {
			// now fetch all the values w.r.t each attribute
			valuesByAttribute = formCommonService.getAllAttributeValuesByAttributeName(fetchedAttributes, protocol);
		}
		return valuesByAttribute;
	}

	public ResponseEntity<FormCRUDData> getFormFromSecurityId(String tenantId, String formSecurityId, Protocol protocol)
			throws FormRelatedException, QueryRelatedException, ClassRelatedException, InstanceRelatedException {
		ReadRequest request = new ReadRequest(protocol, null, -1, -1, -1, null, null, formSecurityId);
		AltForm form = formService.readFormFromSecurityId(request);
		HashMap<String, List<AltOption>> attributeValuebyAttributeName = null;
		try {
			attributeValuebyAttributeName = getAllAttributeValuesListOfForm(tenantId, form.getId(), protocol, form,
					new ArrayList<RoleTO>());
		} catch (FormDataRelatedException | InstanceRelatedException | ClassRelatedException
				| ReferenceRelatedException e) {
			LOGGER.error("Exception while fetching attributeValuebyAttributeName");
		}
		return formCommonService.convertToResponse(protocol, form, attributeValuebyAttributeName, null, null, null);
	}

	public ResponseEntity<FormCreateResponse> createFormDraft(FormCreateRequest formCreateRequest, Protocol protocol,
			String version) throws FormRelatedException, InstanceRelatedException, ClassRelatedException,
			InstanceHistoryRelatedException {
		if (!FormCommonUtil.validateForm(formCreateRequest))
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		List<AltAbstractComponent> componentList = null;
		ClassMeta classMeta = null;

		if (formCreateRequest.getParentFormId() == null || formCreateRequest.getFormState() != null)
			componentList = formCreateRequest.getComponentList();
		else {
			componentList = this.getInheritedComponents(protocol, formCreateRequest.getAppId(),
					formCreateRequest.getParentFormId());
			ReadRequest request = new ReadRequest(protocol, null, -1, -1, -1, formCreateRequest.getParentFormId(), null,
					null);
			AltForm parentForm = formService.readFromId(request);
			classMeta = classService.read(protocol, parentForm.getMetaClassId());
		}

		if (formCreateRequest.getMetaclassid() != null) {
			classMeta = classService.read(protocol, formCreateRequest.getMetaclassid());
		}

		FormRequest request = FormRequest.builder().withComponentList(componentList)
				.withCustomComponentAllowed(formCreateRequest.isCustomComponentAllowed())
				.withDescription(formCreateRequest.getDescription()).withFormName(formCreateRequest.getUiClassName())
				.withFormType(formCreateRequest.getType()).withOrgId(protocol.getOrgId()).withProtocol(protocol)
				.withStatus(formCreateRequest.getStatus()).withState(EnumFormState.DRAFT).withVersion(version)
				.withHrFormId(formCreateRequest.getHrFormId()).withAppId(formCreateRequest.getAppId())
				.withClassMeta(classMeta).withParentFormId(formCreateRequest.getParentFormId()).build();
		AltForm form = null;
		try {
			form = formService.create(request);
			if ((formCreateRequest.getFormMapping().getCustomFormType().equals(EnumFormMappingType.CUSTOMFORM)
					|| formCreateRequest.getFormMapping().getCustomFormType()
							.equals(EnumFormMappingType.CUSTOMFORMFIELD))
					&& form.getId() != null) {
				createFormMapping(protocol, formCreateRequest.getFormMapping(), form.getId(),
						formCreateRequest.getAppId());
			}
		} catch (Exception e) {
		}

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		if (form == null) {
			headers.add("error", "form creation failed");
			return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			FormCreateResponse formCreateResponse = new FormCreateResponse();
			formCreateResponse.setFormId(form.getId());
			formCreateResponse.setSuccess(true);
			formCreateResponse.setResponseMessage("Created SuccessFully");
			headers.add("response", "created successfully");
			return new ResponseEntity<>(formCreateResponse, headers, HttpStatus.OK);
		}
	}

	private List<AltAbstractComponent> getInheritedComponents(Protocol protocol, Integer appId, Integer parentFormId) {
		List<AltAbstractComponent> componentList = new ArrayList<AltAbstractComponent>();
		List<AltForm> allForms = null;
		try {
			allForms = formService.readAll(new ReadRequest(protocol, null, -1, -1, -1, null, null, null, appId));
		} catch (FormRelatedException fre) {
		}
		Map<Integer, AltForm> allFormsMap = new HashMap<Integer, AltForm>();
		allForms.forEach(form -> allFormsMap.put(Integer.parseInt(form.getId()), form));

		getParentComponents(allFormsMap, componentList, parentFormId);
		Set<String> componentNames = new HashSet<String>();
		Iterator<AltAbstractComponent> componentIterator = componentList.iterator();
		while (componentIterator.hasNext()) {
			AltAbstractComponent component = componentIterator.next();
			component.setHrFormFieldId(null);
			component.setSecurity(null);
			if (component.getControlType().equals(EnumHTMLControl.PANEL)) {
				Iterator<AltAbstractComponent> panelComponentIterator = ((AltPanelComponent) component)
						.getComponentList().iterator();

				while (panelComponentIterator.hasNext()) {
					AltAbstractComponent panelChildComponent = panelComponentIterator.next();
					panelChildComponent.setHrFormFieldId(null);
					panelChildComponent.setSecurity(null);
					if (!componentNames.contains(panelChildComponent.getName()))
						componentNames.add(panelChildComponent.getName());
					else
						panelComponentIterator.remove();
				}

			}

			if (!componentNames.contains(component.getName()))
				componentNames.add(component.getName());
			else
				componentIterator.remove();
		}

		return componentList;
	}

	private void getParentComponents(Map<Integer, AltForm> allFormsMap, List<AltAbstractComponent> components,
			Integer parentFormId) {
		if (parentFormId == null || !allFormsMap.containsKey(parentFormId))
			return;

		AltForm parentForm = allFormsMap.get(parentFormId);
		List<AltAbstractComponent> parentComponents = parentForm.getComponentList();
		if (parentComponents != null)
			components.addAll(0, parentComponents);
		getParentComponents(allFormsMap, components, parentForm.getParentFormId());
	}

	private void createFormMapping(Protocol protocol, FormMapping formMapping, String formId, Integer appId)
			throws InstanceRelatedException, ClassRelatedException, InstanceHistoryRelatedException {
		formMapping.setFormId(formId);
		InstanceRequest instanceRequest = new InstanceRequest.Builder()
				.withAttributes(FormCommonUtil.createFormMappingAttributes(formMapping)).withProtocol(protocol)
				.withClassName("FormMapping").withStatus(KernelConstants.EnumDocStatusType.ACTIVE).withAppId(appId)
				.withFormId(Integer.parseInt(formId)).build();
		instanceService.create(instanceRequest);
	}

	public ResponseEntity<FormCreateResponse> updateFormDraft(FormUpdateRequest formUpdateRequest, Protocol protocol)
			throws FormRelatedException, IOException, NoSuchAlgorithmException, QueryRelatedException,
			InstanceRelatedException, ClassRelatedException, InstanceHistoryRelatedException {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		if (protocol == null) {
			throw new FormRelatedException("Protocol cannot be null in updateFormDraft.");
		}
		FormCommonUtil.validate(formUpdateRequest, protocol.getOrgId(), protocol.getUserName());
		FormRequest request = null;
		switch (formUpdateRequest.getFormState()) {
		case DRAFT:
			request = FormRequest.builder().withComponentList(formUpdateRequest.getComponentList())
					.withVersion(formUpdateRequest.getVersionId()).withProtocol(protocol)
					.withFormId(Integer.valueOf(formUpdateRequest.getUiClassCode())).withState(EnumFormState.DRAFT)
					.build();
			break;
		case PUBLISH:
			int formId = getFormIdFromFormNameAndState(formUpdateRequest.getUiClassName(), protocol,
					EnumFormState.DRAFT);
			if (formId != 0) {
				FormCreateResponse formCreateResponse = new FormCreateResponse();
				formCreateResponse.setFormId(String.valueOf(formId));
				formCreateResponse.setSuccess(true);
				formCreateResponse.setResponseMessage("New form opened in draft state");
				headers.add("response", "created successfully");
				headers.add("formId", String.valueOf(formId));
				return new ResponseEntity<>(formCreateResponse, headers, HttpStatus.OK);
			} else {
				FormCreateRequest formCreateRequest = new FormCreateRequest.Builder().withBundleId(null)
						.withFormState(EnumFormState.DRAFT).withComponentList(formUpdateRequest.getComponentList())
						.withCustomComponentAllowed(formUpdateRequest.isCustomComponentAllowed())
						.withDescription(formUpdateRequest.getDescription())
						.withModuleCode(formUpdateRequest.getModuleCode()).withOrgId(protocol.getOrgId())
						.withStatus(formUpdateRequest.getStatus()).withType(formUpdateRequest.getType())
						.withUiClassName(formUpdateRequest.getUiClassName())
						.withFormMapping(fetchFormMappingForGivenForm(protocol, formUpdateRequest.getUiClassCode()))
						.withComponentCounter(formUpdateRequest.getComponentCounter())
						.withHrFormId(formUpdateRequest.getHrFormId()).withAppId(formUpdateRequest.getAppid())
						.withMetaclassid(formUpdateRequest.getMetaclassid())
						.withParentFormId(formUpdateRequest.getParentFormId()).build();
				String version = CommonUtil.generateVersion(formUpdateRequest);
				return createFormDraft(formCreateRequest, protocol, version);
			}
		}
		AltForm formAfterUpdation = formService.updateComponentsVersionState(request);
		if (formAfterUpdation == null) {
			headers.add("error", "form updation failed");
			return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			FormCreateResponse formCreateResponse = null;
			formCreateResponse = new FormCreateResponse(true, "Updated SuccessFully", null, formAfterUpdation.getId());
			headers.add("response", "updated successfully");
			return new ResponseEntity<>(formCreateResponse, headers, HttpStatus.OK);
		}

	}

	public ResponseEntity<MultiValueMap> sendFormMetaForSecurity(FormUpdateRequest formUpdateRequest, Protocol protocol)
			throws QueryRelatedException, IOException, NoSuchAlgorithmException, FormRelatedException {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		if (!formUpdateRequest.getFormState().equals(EnumFormState.PUBLISH)) {
			LOGGER.warn("Only published form meta can be submitted");
			headers.add("response", "Error: Only published form meta can be submitted");
			return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
		}
		DynamicReadRequest request = new DynamicReadRequest();
		request.setProtocol(protocol);
		request = dbService.createDynamicQueryRequest(request, formUpdateRequest.getUiClassCode());
		DynamicReadResponse response1 = dynamicQuery.read(request);
		List<Object[]> instances = response1.getResponse();
		Object[] formMappingInstance = null;
		boolean generalForm = false;
		if (instances != null && !instances.isEmpty()) {
			formMappingInstance = instances.get(0);
		} else {
			generalForm = true;
		}
		if (!generalForm) {
			AltOneCustomFormSecurityRequest formSecurityRequest = FormCommonUtil.createFormSecurityRequest(
					formUpdateRequest.getComponentList(), formMappingInstance, formUpdateRequest.getUiClassName());
			formSecurityRequest.setHrFormId(formUpdateRequest.getHrFormId());
			HttpResponse rawResponse = HttpService.getData(
					securityPropertiesConf.getUrl()/* "https://s2demo-admin.sohum.com/services/altone-custom-form" */,
					formSecurityRequest);
			try {
				formSecurityRequest = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
						AltOneCustomFormSecurityRequest.class);
			} catch (RuntimeException e) {
				LOGGER.error("Error while reading response of form security web service.", e);
				headers.add("response", "Error while reading response of form security web servicey");
				return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			if (formSecurityRequest.getMessage().getCode().equals("200")) {
				LOGGER.info("Request sent for form security. Response is {}", formSecurityRequest.toString());
			} else {
				throw new FormRelatedException("Form security web service request failed.");
			}
			if (!formCommonService.updateFormWithSecurityDetails(formUpdateRequest, protocol, formSecurityRequest)) {
				LOGGER.error("Error while saving client id to db ");
				headers.add("response", "Error while saving client id to db ");
				return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private FormMapping fetchFormMappingForGivenForm(Protocol protocol, String uiClassCode)
			throws QueryRelatedException {
		DynamicReadRequest readRequest = new DynamicReadRequest();
		readRequest.setProtocol(protocol);
		readRequest = DbService.createDynamicQueryRequest(readRequest, uiClassCode);
		DynamicReadResponse response1 = dynamicQuery.read(readRequest);
		List<Object[]> instances = response1.getResponse();
		Object[] instance = null;
		boolean generalForm = false;
		if (instances != null && !instances.isEmpty()) {
			instance = instances.get(0);
			return FormCommonUtil.convertObjectToFormMapping(instance);
		} else {
			FormMapping formMapping = new FormMapping.Builder().withCustomFormType(EnumFormMappingType.GENERAL)
					.withFormId(uiClassCode).withOrgId(protocol.getOrgId()).withTenantId(protocol.getTenantCode())
					.build();
			return formMapping;
		}
	}

	public ResponseEntity<FormCreateResponse> publishForm(String formIdToBePublished, String formName,
			Boolean taskCodeRequired, Protocol protocol) throws FormRelatedException, QueryRelatedException,
			InstanceRelatedException, ClassRelatedException, NoSuchAlgorithmException, IOException {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		int formId = getFormIdFromFormNameAndState(formName, protocol, EnumFormState.PUBLISH);
		AltForm formAfterUpdation = null;
		if (formId != 0) {
			LOGGER.info("Updating old published form to OBSELETE");
			formAfterUpdation = updateFormStateForGivenFormId(protocol, formId, EnumFormState.OBSOLETE, null);

			// removed
			// classService.markInactiveFormClass(protocol, formName);

			if (formAfterUpdation == null) {
				headers.add("error", "form updation PUBLISHED to OBSELETE failed");
				return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			LOGGER.info("Form update PUBLISH to OBSELETE done!");
		} else {
			LOGGER.info("Updating new form to PUBLISH");
		}

		ResponseEntity<FormCRUDData> formResponse = getForm(
				new FormDataRetrieveRequest(null, null, formIdToBePublished, formName, null), protocol);

		ClassMeta classMeta = null;
		if (formResponse.getBody().getMetaclassid() != null) {
			classMeta = updateClassForForm(formResponse.getBody(), taskCodeRequired, protocol);
		} else {
			classMeta = createClassForForm(formResponse.getBody(), taskCodeRequired, protocol);
		}

		formAfterUpdation = updateFormStateForGivenFormId(protocol, Integer.parseInt(formIdToBePublished),
				EnumFormState.PUBLISH, classMeta);
		if (formAfterUpdation == null) {
			headers.add("error", "form updation DRAFT to PUBLISHED failed");
			return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			try {
				if (classMeta.getTaskCodeRequired() != null && classMeta.getTaskCodeRequired()
						&& formResponse.getBody().getMetaclassid() == null) {
					createInitialTaskCodeForGeneratedClass(formResponse.getBody(), classMeta, formName, protocol);
				}
			} catch (TaskCodeRecordRelatedException e) {
				headers.add("error", "Task Code creation failed for the form");
				return new ResponseEntity<>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			FormCreateResponse formCreateResponse = null;
			formCreateResponse = new FormCreateResponse(true, "Updated SuccessFully", null, formAfterUpdation.getId());
			headers.add("response", "updated successfully");
			return new ResponseEntity<>(formCreateResponse, headers, HttpStatus.OK);
		}
	}

	private int getFormIdFromFormNameAndState(String formName, Protocol protocol, EnumFormState state)
			throws QueryRelatedException {
		DynamicReadRequest request = DbService.findPublishedFormForGivenFormName(formName, protocol.getOrgId(),
				state.ordinal());
		DynamicReadResponse response = dynamicQuery.read(request);
		List<Object[]> instances = response.getResponse();
		if (instances.size() > 0) {
			Object[] objArray = instances.get(0);
			Integer id = (Integer) objArray[0];
			String version = (String) objArray[1];
			return id;
		} else {
			return 0;
		}
	}

	private AltForm updateFormStateForGivenFormId(Protocol protocol, int formId, EnumFormState state,
			ClassMeta classMeta) throws FormRelatedException {
		FormRequest formRequest = new FormRequest.Builder().withProtocol(protocol).withFormId(formId).withState(state)
				.withClassMeta(classMeta).build();
		return formService.updateState(formRequest);
	}

	public ResponseEntity<FormInstanceResponse> submitFormInstance(FormInstanceUpdateRequest formInstanceUpdateRequest,
			Protocol validateJwt) throws FormRelatedException, InstanceRelatedException, ClassRelatedException,
			InstanceHistoryRelatedException {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

		if (formInstanceUpdateRequest.getFormData().getId() == null
				|| formInstanceUpdateRequest.getFormData().getId().length() == 0)
			throw new FormRelatedException("Form id is missing");
		ReadRequest formReadRequest = new ReadRequest(validateJwt, null, -1, -1, -1,
				Integer.valueOf(formInstanceUpdateRequest.getFormData().getId()), null, null);
		FormCommonUtil.validate(formInstanceUpdateRequest.getContextRequests());
		AltForm formReadData = formService.readFromId(formReadRequest);
		boolean validClientIDs = formReadData.validateFormFieldClientIDs(formInstanceUpdateRequest.getFormData());
		if (!validClientIDs)
			throw new FormRelatedException("Client IDs does not match");
		validClientIDs = formInstanceUpdateRequest.getFormData().validateDuplicateClientIDs();
		if (!validClientIDs)
			throw new FormRelatedException("Duplicate Client IDs");

		formReadData.setValuesInComponents(formInstanceUpdateRequest.getFormData());
		formInstanceUpdateRequest.setFormData(formReadData);
		for (AltCustomFormDataContextRequest contextReq : formInstanceUpdateRequest.getContextRequests()) {
			InstanceRequest instanceRequest = new InstanceRequest.Builder()
					.withAttributes(FormCommonUtil
							.createFormDataMappingAttributes(formInstanceUpdateRequest.getFormData(), contextReq))
					.withProtocol(validateJwt).withClassName("FormData")
					.withStatus(KernelConstants.EnumDocStatusType.ACTIVE).build();
			instanceService.create(instanceRequest);
		}

		FormInstanceResponse response = new FormInstanceResponse(true, "Instances saved successfully");
		headers.add("response", "Saved successfully");
		return new ResponseEntity<>(response, headers, HttpStatus.OK);
	}

	public ResponseEntity<FormInstanceResponse> fetchFormInstance(FormInstanceFetchRequest formInstanceFetchRequest,
			Protocol validateJwt) throws FormRelatedException, QueryRelatedException, IOException {
		if (formInstanceFetchRequest.getFormSecurityId() == null
				|| formInstanceFetchRequest.getFormSecurityId().longValue() == 0L) {
			FormInstanceResponse response = new FormInstanceResponse(false, "FormSecurityid is missing", null);
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add("response", "FormSecurityid is missing");
			return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
		}
		ReadRequest request = new ReadRequest(validateJwt, null, -1, -1, -1, null, null,
				formInstanceFetchRequest.getFormSecurityId().toString());
		AltForm form1 = null;
		try {
			form1 = formService.readFormFromSecurityId(request);
		} catch (Exception e) {
			FormInstanceResponse response = new FormInstanceResponse(false,
					"Formsecurityid does not match with existing formsecurityid", form1);
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add("response", "Formsecurityid does not match with existing formsecurityid");
			return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
		}
		AltForm form2 = null;
		ObjectMapper mapper = new ObjectMapper();
		if (formInstanceFetchRequest.getContextRequests() == null) {
			FormInstanceResponse response = new FormInstanceResponse(true,
					"Case when context is null(call before context creation)", form1);
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add("response", "Case when context is null(call before context creation)");
			return new ResponseEntity<>(response, headers, HttpStatus.OK);
		}
		for (AltCustomFormDataContextRequest context : formInstanceFetchRequest.getContextRequests()) {
			if (context.getContext() == null) {
				FormInstanceResponse response = new FormInstanceResponse(false,
						"Context is missing in the contextRequest", form1);
				MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
				headers.add("response", "Context is missing in the contextRequest");
				return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
			} else if (context.getContext().getContextType().length() > 100) {
				FormInstanceResponse response = new FormInstanceResponse(false,
						"Context length should not more than 100", form1);
				MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
				headers.add("response", "Context length should not more than 100");
				return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
			} else if (!FormCommonUtil.isCharInputOnly(context.getContext().getContextType())) {
				FormInstanceResponse response = new FormInstanceResponse(false, "Context data not valid", form1);
				MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
				headers.add("response", "Context data not valid");
				return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
			}
			for (AltCustomFormDataContextFilter contextFilterObj : context.getContextFilter()) {

				if (contextFilterObj.getContextLabel() == null) {
					FormInstanceResponse response = new FormInstanceResponse(false,
							"ContextLabel is missing in the contextFilter", form1);
					MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
					headers.add("response", "ContextLabel is missing in the contextFilter");
					return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
				} else if (contextFilterObj.getContextLabel().length() > 100) {
					FormInstanceResponse response = new FormInstanceResponse(false,
							"ContextLabel length should not more than 100", form1);
					MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
					headers.add("response", "ContextLabel length should not more than 100");
					return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
				} else if (!FormCommonUtil.isAlphanumeric(contextFilterObj.getContextLabel())) {
					FormInstanceResponse response = new FormInstanceResponse(false,
							"ContextLabel should only be alpha numeric", form1);
					MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
					headers.add("response", "ContextLabel should only be alpha numeric");
					return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
				}

				if (contextFilterObj.getContextValue() == null) {
					FormInstanceResponse response = new FormInstanceResponse(false,
							"ContextValue is missing in the contextFilter", form1);
					MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
					headers.add("response", "ContextValue is missing in the contextFilter");
					return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
				} else if (contextFilterObj.getContextValue().length() > 100) {
					FormInstanceResponse response = new FormInstanceResponse(false,
							"ContextValue length should not more than 100", form1);
					MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
					headers.add("response", "ContextValue length should not more than 100");
					return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
				} else if (!FormCommonUtil.isAlphanumeric(contextFilterObj.getContextValue())) {
					FormInstanceResponse response = new FormInstanceResponse(false,
							"ContextValue should only be alphanumeric", form1);
					MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
					headers.add("response", "ContextValue should only be alphanumeric");
					return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
				}
			}
			DynamicReadResponse response = new DynamicReadResponse();
			DynamicReadRequest readRequest = new DynamicReadRequest();
			createCategoriesReadRequest(readRequest, form1.getId(), context);
			response = dynamicQuery.read(readRequest);
			if (response.getResponse() == null || response.getResponse().isEmpty()) {
				continue;
			}
			Object formArray = response.getResponse().get(0);
			if (formArray != null) {
				form2 = mapper.readValue((String) formArray, AltForm.class);
				break;
			}
		}

		if (form2 == null) {
			FormInstanceResponse response = new FormInstanceResponse(false, "No form corresponsding to given context",
					form1);
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add("response", "No form corresponsding to given context");
			return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
		}
		FormInstanceResponse response = new FormInstanceResponse(true, "AltForm fetched successfully", form2);
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("response", "Altform fetched successfully");

		return new ResponseEntity<>(response, headers, HttpStatus.OK);
	}

	private void createCategoriesReadRequest(DynamicReadRequest request, String formId,
			AltCustomFormDataContextRequest context) {
		List<ProjectionAttribute> projections = new ArrayList<>();
		ProjectionAttribute projectionAttribute = new ProjectionAttribute();
		projectionAttribute.setDbClass("instance");
		projectionAttribute.setDbAttributes(getDBAttributesForApp());
		projections.add(projectionAttribute);
		request.setProjections(projections);
		request.setFromDbClass("instance");
		List<DbFilters> dbFilters = new ArrayList<>();
		DbFilters filter1 = new DbFilters();
		filter1.setAttribute("attributes");
		filter1.setClassName("instance");
		filter1.setJsonbType(true);
		filter1.setJsonName("attribute");
		List<String> jsonbAttributes1 = new ArrayList<String>();
		jsonbAttributes1.add("formData");
		jsonbAttributes1.add("id");
		filter1.setJsonbAttribute(jsonbAttributes1);
		filter1.setCondition("=");
		filter1.setValue(formId);
		dbFilters.add(filter1);
		DbFilters filter2 = new DbFilters();
		filter2.setAttribute("attributes");
		filter2.setClassName("instance");
		filter2.setJsonbType(true);
		filter2.setJsonName("attribute");
		List<String> jsonbAttributes2 = new ArrayList<String>();
		jsonbAttributes2.add("context");
		jsonbAttributes2.add("contextType");
		filter2.setJsonbAttribute(jsonbAttributes2);
		filter2.setCondition("=");
		filter2.setValue(context.getContext().getContextType());
		dbFilters.add(filter2);

		List<String> jsonbAttributes4;
		DbFilters filter3;

		for (AltCustomFormDataContextFilter contextFilterObj : context.getContextFilter()) {

			filter3 = new DbFilters();
			filter3.setAttribute("attributes");
			filter3.setClassName("instance");
			filter3.setJsonbType(true);
			filter3.setJsonName("attribute");
			jsonbAttributes4 = new ArrayList<String>();
			jsonbAttributes4.add("contextFilter");
			filter3.setJsonbAttribute(jsonbAttributes4);
			filter3.setCondition(DbFilters.CONDITION_JSONB_COMPLEX_ARRAY_CONTAINS_SINGLE);
			filter3.setValue(FormCommonUtil.getJsonFromContextFilter(contextFilterObj));
			dbFilters.add(filter3);
		}

		filter3 = new DbFilters();
		filter3.setAttribute("attributes");
		filter3.setClassName("instance");
		filter3.setJsonbType(true);
		filter3.setJsonName("attribute");
		jsonbAttributes4 = new ArrayList<String>();
		jsonbAttributes4.add("contextFilter");
		filter3.setJsonbAttribute(jsonbAttributes4);
		filter3.setCondition(DbFilters.CONDITION_JSONB_ARRAY_LENGTH);
		filter3.setValue(context.getContextFilter().size() + "");
		dbFilters.add(filter3);

		request.setDbFilters(dbFilters);
		List<OrderAttribute> orderByList = new ArrayList<>();
		OrderAttribute orderBy = new OrderAttribute();
		orderBy.setDbClass("instance");
		orderBy.setDbAttr("modifieddate");
		orderBy.setJsonbType(false);
		orderBy.setSortDirection(EnumSortDirection.DESC);
		orderByList.add(orderBy);
		request.setOrderBy(orderByList);
		request.setLimit(1);
	}

	private List<AttributesData> getDBAttributesForApp() {
		List<AttributesData> dbAttributes = new ArrayList<>();
		dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true)
				.withJsonName("attribute").withJsonbAttribute("formData").withAs("formData").build());
		return dbAttributes;
	}

	private String generateTaskCodeInitialsForClass(String formName, String formId) {
		if (formName == null || formName.equalsIgnoreCase("")) {
			return null;
		}
		String initials = "";
		if (formName.contains(" ")) {
			String[] wrd = formName.split(" ");
			for (int i = 0; i < wrd.length; i++) {
				initials = initials + wrd[i].charAt(0);
			}
		} else {
			initials = initials + formName.charAt(0) + formName.charAt(1);
		}

		initials = initials + formId;

		return initials;
	}

	private TaskCodeRecordTransport createInitialTaskCodeForGeneratedClass(FormCRUDData altForm, ClassMeta classMeta,
			String formName, Protocol protocol) throws TaskCodeRecordRelatedException {
		TaskCodeRecordTransport taskCodeRecordTransport = new TaskCodeRecordTransport.Builder()
				.withAppId(altForm.getAppid()).withClassId(classMeta.getId()).withFormId(altForm.getUiClassCode())
				.withFormInitials(generateTaskCodeInitialsForClass(altForm.getUiClassName(), altForm.getUiClassCode()))
				.withFormName(formName).withLastCodeUsed("00000").withProtocol(protocol).build();
		try {
			taskCodeRecordTransport = taskCodeRecordService.createTaskCodeRecordForClass(taskCodeRecordTransport,
					protocol);
		} catch (TaskCodeRecordRelatedException tcre) {
			LOGGER.error("Failed to create initial entry for taskcode in class table while publishing",tcre);
			throw new TaskCodeRecordRelatedException("Failed to create class for form", tcre);
		}
		return taskCodeRecordTransport;
	}

	private ClassMeta createClassForForm(FormCRUDData altForm, Boolean taskCodeRequired, Protocol protocol)
			throws ClassRelatedException {
		ClassMeta classMeta;
		List<AttributeMeta> attributeList = new ArrayList<>();
		getAttributeList(altForm.getComponentList(), attributeList);

		/**
		 * Marking TaskCodeRequired flag as true i.e. it is required. ***It needs to be
		 * changed & should be based on configuration provided by user***
		 */

		CreateClassRequest classRequest = new CreateClassRequest.Builder().withClassName(altForm.getUiClassName())
				.withAttributes(attributeList).withCustomAttributeAllowed(true)
				.withInstanceLimiter(KernelConstants.EnumInstanceLimiter.NONE)
				.withInstancePrefix(altForm.getUiClassName()).withLocationSpecific(false).withParameterType(false)
				.withPackageCodes(null).withProtocol(protocol).withStatus(KernelConstants.EnumDocStatusType.ACTIVE)
				.withTaskCodeRequired((taskCodeRequired != null) ? taskCodeRequired : false).withSystemType(false)
				.build();

		try {
			classMeta = classService.create(classRequest);
		} catch (ClassRelatedException e) {
			LOGGER.error("Failed to create class for form");
			throw new ClassRelatedException("Failed to create class for form", e);
		}
		return classMeta;
	}

	private ClassMeta updateClassForForm(FormCRUDData altForm, Boolean taskCodeRequired, Protocol protocol)
			throws ClassRelatedException {
		ClassMeta classMeta = classService.read(protocol, altForm.getMetaclassid());

		List<AttributeMeta> attributeList = new ArrayList<>();
		getAttributeList(altForm.getComponentList(), attributeList);

		Set<String> existingAttributes = new HashSet<String>();
		classMeta.getAttributes().forEach(attribute -> existingAttributes.add(attribute.getAttributeName()));
		for (AttributeMeta attribute : attributeList) {
			if (!existingAttributes.contains(attribute.getAttributeName())) {
				classMeta.getAttributes().add(attribute);
			}
		}

		CreateClassRequest classRequest = new CreateClassRequest.Builder().withClassName(classMeta.getName())
				.withAttributes(classMeta.getAttributes()).withCustomAttributeAllowed(true)
				.withInstanceLimiter(KernelConstants.EnumInstanceLimiter.NONE)
				.withInstancePrefix(altForm.getUiClassName()).withLocationSpecific(false).withParameterType(false)
				.withPackageCodes(null).withProtocol(protocol).withStatus(KernelConstants.EnumDocStatusType.ACTIVE)
				.withSystemType(false).withClassId(altForm.getMetaclassid().toString())
				.withTaskCodeRequired(classMeta.getTaskCodeRequired()).build();
		try {
			classMeta = classService.update(classRequest);
		} catch (ClassRelatedException e) {
			LOGGER.error("Failed to create class for form");
			throw new ClassRelatedException("Failed to create class for form", e);
		}
		return classMeta;
	}

	private void getAttributeList(List<AltAbstractComponent> componentList, List<AttributeMeta> attributeList) {
		for (AltAbstractComponent component : componentList) {
			if (component.getControlType().equals(EnumHTMLControl.PANEL)) {
				getAttributeList(((AltPanelComponent) component).getComponentList(), attributeList);
			} else if (component.getControlType().equals(EnumHTMLControl.TAB_PANEL)) {
				((AltTabbedPanelComponent) component).getPanels().forEach(panel -> {
					getAttributeList(((AltPanelComponent) panel).getComponentList(), attributeList);
				});
			} else if (!component.getControlType().equals(EnumHTMLControl.BUTTON)
					&& !component.getControlType().equals(EnumHTMLControl.BLANK)) {
				AttributeMeta attr = new AttributeMeta();
				attr.setAttributeName(component.getName());
				attr.setDisplayUnit(false);
				attr.setIndexable(false);
				attr.setMandatory(false);
				attr.setPlaceholderType(false);
				// attr.setReferenceClassId(attribute.getReferenceClassId());
				attr.setSearchable(true);
				attr.setStatus(KernelConstants.EnumDocStatusType.ACTIVE);
				attr.setUnique(false);

				if (isOptionsReferenceAttribute(component)) {
					attr.setDataType(EnumDataType.REFERENCE);
					attr.setReferenceClass(((AltGroupComponent) component).getDbClassRead());
					attr.setReferenceClassId(Integer.parseInt(((AltGroupComponent) component).getDbClassRead()));
					attr.setReferenceAttr(((AltGroupComponent) component).getDbAttrRead());
				} else if (isGlobalReferenceAttribute(component)) {
					attr.setDataType(EnumDataType.GLOBALREFERENCE);
					attr.setReferenceClass(((AltGroupComponent) component).getDbClassRead());
					// attr.setReferenceClassId(Integer.parseInt(((AltGroupComponent)component).getDbClassRead()));
					attr.setReferenceAttr(((AltGroupComponent) component).getDbAttrRead());
				} else {
					attr.setDataType(((AltAbstractComponent) component).getDataType());
				}
				attributeList.add(attr);
			}
		}
	}

	private boolean isGlobalReferenceAttribute(AltAbstractComponent component) {
		return (component.getControlType().equals(EnumHTMLControl.DROPDOWN)
				|| component.getControlType().equals(EnumHTMLControl.MULTI_SELECT))
				&& ((AltGroupComponent) component).getDbClassRead() != null
				&& ((AltGroupComponent) component).getDbAttrRead() != null
				&& ((AltGroupComponent) component).getDbClassType().equalsIgnoreCase("Global");
	}

	private boolean isOptionsReferenceAttribute(AltAbstractComponent component) {
		return (component.getControlType().equals(EnumHTMLControl.DROPDOWN)
				|| component.getControlType().equals(EnumHTMLControl.MULTI_SELECT))
				&& ((AltGroupComponent) component).getDbClassRead() != null
				&& ((AltGroupComponent) component).getDbAttrRead() != null
				&& ((AltGroupComponent) component).getDbClassType().equalsIgnoreCase("Local");
	}

	private void determineFormSecurity(AltForm form, Collection<Integer> userRoleList, Integer organizationId,
			String bundle, Integer stageId) throws NoSuchAlgorithmException, IOException {

		if (form.getFormSecurityId() == null)
			return;

		StageRequestTO stageRequest = new StageRequestTO();
		stageRequest.setHrFormId(Long.parseLong(form.getFormSecurityId()));
		stageRequest.setStageId(stageId);
		stageRequest.setCustomFlag(true);
		stageRequest.setProtocolTO(new ProtocolTO());
		StageResponseTO stageResponse = null;
		HttpResponse rawResponse = HttpService.callRestAPI(
				securityPropertiesConf
						.getWfapi1()/* "https://s2demo.sohum.com/service/api/workflow/currentStageInfo" */,
				stageRequest);
		try {
			stageResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(), StageResponseTO.class);
		} catch (Exception e) {
			LOGGER.error("Error while reading response of workflow web service.", e);
			return;
		}

		SysCredentialTO formSecurityRequest = new SysCredentialTO();
		formSecurityRequest.setFormName(form.getUiClassName());
		formSecurityRequest.setRoleList(userRoleList == null ? new HashSet<Integer>() : new HashSet<>(userRoleList));
		formSecurityRequest.setOrganizationID(organizationId);
		formSecurityRequest.setBundle(bundle);
		formSecurityRequest.setStageID(stageResponse.getWorkflowStageId());
		Map<String, UiFormSecurityTO> securityMap = null;
		rawResponse = HttpService.callRestAPI(
				securityPropertiesConf
						.getFormsecurity()/* "https://s2demo.sohum.com/service/api/organization/formsecurity" */,
				formSecurityRequest);
		try {
			securityMap = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
					new TypeReference<HashMap<String, UiFormSecurityTO>>() {
					});
		} catch (Exception e) {
			LOGGER.error("Error while reading response of form security web service.", e);
			return;
		}

		Map<Long, UiFormSecurityTO> fieldIdSecurityMap = new HashMap<Long, UiFormSecurityTO>();
		for (UiFormSecurityTO security : securityMap.values()) {
			fieldIdSecurityMap.put(security.getHrFormFieldID(), security);
		}
		for (AltAbstractComponent component : form.getComponentList()) {
			if (fieldIdSecurityMap.containsKey(component.getHrFormFieldId())) {
				component.setSecurity(fieldIdSecurityMap.get(component.getHrFormFieldId()).getAltComponentSecurity());
			} else if (component.getControlType().equals(EnumHTMLControl.PANEL)) {
				AltComponentSecurity security = new AltComponentSecurity();
				security.setViewable(false);
				component.setSecurity(security);
				((AltPanelComponent) component).getComponentList().forEach(c -> {

					if (fieldIdSecurityMap.containsKey(c.getHrFormFieldId())) {
						c.setSecurity(fieldIdSecurityMap.get(c.getHrFormFieldId()).getAltComponentSecurity());
						if (fieldIdSecurityMap.get(c.getHrFormFieldId()).getAltComponentSecurity().getViewable()) {
							security.setViewable(true);
						}

					}
				});

			}
		}

		if (stageResponse.getWorkflowStageId() == null)
			return;
		List<ActionResponseTO> actions = stageResponse.getActions();
		Map<String, ActionResponseTO> actionMap = new HashMap<String, ActionResponseTO>();
		for (ActionResponseTO action : actions) {
			actionMap.put(action.getFormFieldName(), action);
		}
		for (AltAbstractComponent component : form.getFlatComponentList()) {
			if (component instanceof AltButtonComponent) {
				if (actionMap.containsKey(component.getName())) {
					((AltButtonComponent) component).setActionId(actionMap.get(component.getName()).getActionId());
				} else if (component.getSecurity() != null) {
					component.getSecurity().setViewable(Boolean.FALSE);
				}
			}
		}
	}

	public ResponseEntity<CommEnvlp> communicationOperations(CommEnvlp commEnvlp, Protocol protocol)
			throws NoSuchAlgorithmException, IOException {

		CommunicationRequestResponseTO temp = new CommunicationRequestResponseTO();

		MailConfigTO config = new MailConfigTO();
		config.setBcc(commEnvlp.getBcc());
		config.setBody(commEnvlp.getBody());
		config.setCc(commEnvlp.getCc());
		config.setDescription(commEnvlp.getTemplateName());
		config.setTo(commEnvlp.getTo());
		config.setFrom(commEnvlp.getFrom());
		config.setSubject(commEnvlp.getSubject());
		config.setName(commEnvlp.getTemplateName());
		config.setCommId(commEnvlp.getCommId());

		temp.setAction(commEnvlp.getAction());
		if (commEnvlp.getSelectMailIds() != null && commEnvlp.getSelectMailIds().size() > 0) {
			temp.setSelectMailIds(commEnvlp.getSelectMailIds());
		}
		temp.setMailConfig(config);
		temp.setTenantId(protocol.getTenantId());
		temp.setOrganizationId(protocol.getOrgId());

		try {

			HttpResponse rawResponse = HttpService.callRestAPI(
					securityPropertiesConf
							.getCommunication()/* "https://s2demo.sohum.com/service/api/workflow/currentStageInfo" */,
					temp);

			CommunicationRequestResponseTO commSetupResponse = new ObjectMapper()
					.readValue(rawResponse.getEntity().getContent(), CommunicationRequestResponseTO.class);

			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			headers.add("Response", "form listing fetched successfully");
			return new ResponseEntity<>(commResponseConveter(commSetupResponse), headers, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Error while reading response of workflow web service.", e);
			return null;
		}

	}

	private CommEnvlp commResponseConveter(CommunicationRequestResponseTO commSetupResponse)
			throws MailRelatedException {
		CommEnvlp envelope = null;
		if (commSetupResponse != null
				&& (commSetupResponse.getMailConfig() != null || commSetupResponse.getMailConfigs() != null)) {
			envelope = new CommEnvlp();
			if (commSetupResponse.getMailConfig() != null) {
				if (commSetupResponse.getAction() != null)
					envelope.setAction(commSetupResponse.getAction());
				if (commSetupResponse.getMailConfig().getTo() != null)
					envelope.setTo(commSetupResponse.getMailConfig().getTo());
				if (commSetupResponse.getMailConfig().getName() != null)
					envelope.setTemplateName(commSetupResponse.getMailConfig().getName());
				if (commSetupResponse.getMailConfig().getFrom() != null)
					envelope.setFrom(commSetupResponse.getMailConfig().getFrom());
				if (commSetupResponse.getMailConfig().getBcc() != null)
					envelope.setBcc(commSetupResponse.getMailConfig().getBcc());
				if (commSetupResponse.getMailConfig().getBody() != null)
					envelope.setBody(commSetupResponse.getMailConfig().getBody());
				if (commSetupResponse.getMailConfig().getCc() != null)
					envelope.setCc(commSetupResponse.getMailConfig().getCc());
				if (commSetupResponse.getMailConfig().getCommId() != null)
					envelope.setCommId(commSetupResponse.getMailConfig().getCommId());
				if (commSetupResponse.getMailConfig().getSubject() != null)
					envelope.setSubject(commSetupResponse.getMailConfig().getSubject());
			}
			CommEnvlp envelopeItr = null;
			if (commSetupResponse.getMailConfigs() != null && commSetupResponse.getMailConfigs().size() > 0) {
				List<CommEnvlp> envlpList = new ArrayList<CommEnvlp>();
				List<MailConfigTO> list = commSetupResponse.getMailConfigs();
				for (MailConfigTO configTO : list) {
					envelopeItr = new CommEnvlp();
					if (configTO.getTo() != null)
						envelopeItr.setTo(configTO.getTo());
					if (configTO.getName() != null)
						envelopeItr.setTemplateName(configTO.getName());
					if (configTO.getFrom() != null)
						envelopeItr.setFrom(configTO.getFrom());
					if (configTO.getBcc() != null)
						envelopeItr.setBcc(configTO.getBcc());
					if (configTO.getBody() != null)
						envelopeItr.setBody(configTO.getBody());
					if (configTO.getCc() != null)
						envelopeItr.setCc(configTO.getCc());
					if (configTO.getCommId() != null)
						envelopeItr.setCommId(configTO.getCommId());
					if (configTO.getSubject() != null)
						envelopeItr.setSubject(configTO.getSubject());

					envlpList.add(envelopeItr);
				}
				envelope.setCommEnvlpList(envlpList);
			}

		}

		return envelope;
	}

	public ResponseEntity<GlobalTextfieldReloadRequestResponse> reloadGlobalTextfields(Protocol protocol,
			GlobalTextfieldReloadRequestResponse request) {
		Map<String, String> globalAttributeValues = formCommonService
				.getGlobalSingleAttributeList(request.getObjectAttrList(), protocol, request.getEmployeeId());
		List<String> attributeValues = new ArrayList<String>();
		for (String name : request.getObjectAttrList()) {
			attributeValues.add(globalAttributeValues.get(name));
		}
		request.setObjectAttrValues(attributeValues);
		return new ResponseEntity<GlobalTextfieldReloadRequestResponse>(request, HttpStatus.OK);
	}

	public ResponseEntity<ReloadLocalDataTableRequestResponse> reloadLocalDataTable(Protocol protocol,
			ReloadLocalDataTableRequestResponse request) {
		for (AltDataTable table : request.getTables()) {
			try {
				Collection<String> ownerEmployeeIds = new HashSet<String>();
				ownerEmployeeIds.add(request.getOwnerEmployeeId().toString());
				table.setData(formCommonService.getDataForTable(protocol, table.getDbClassRead(),
						table.getDisplayedColumns(), ownerEmployeeIds, null));
			} catch (Exception e) {

			}
		}
		return new ResponseEntity<ReloadLocalDataTableRequestResponse>(request, HttpStatus.OK);
	}

	public DownloadDataTableRequestResponse downloadDataTable(Protocol protocol,
			DownloadDataTableRequestResponse request)
			throws IOException, FormRelatedException, FormDataRelatedException, InstanceRelatedException,
			ClassRelatedException, QueryRelatedException, ReferenceRelatedException {

		AltDataTable table = request.getTable();

		ReadRequest formRequest = new ReadRequest(protocol, null, -1, -1, -1, table.getFormPopup(), null, null);
		AltForm form = formService.readFromId(formRequest);

		Map<String, List<AltOption>> optionMap = getAllAttributeValuesListOfForm(request.getTenantId(),
				table.getFormPopup().toString(), protocol, form, request.getFormRoles());

		List<String[]> dropDownOptions = new ArrayList<String[]>();
		for (int i = 0; i < table.getDisplayedColumns().size(); i++) {
			String column = table.getDisplayedColumns().get(i);
			String fieldName = null;
			String[] options = null;
			if (column.indexOf("..") != -1) {
				fieldName = column.split("\\.\\.")[1];
				for (String key : optionMap.keySet()) {
					String keyFieldName = key.split("_")[1];
					if (fieldName.equalsIgnoreCase(keyFieldName)) {
						List<AltOption> altOptions = optionMap.get(key);
						options = new String[altOptions.size()];
						for (int j = 0; j < altOptions.size(); j++) {
							options[j] = altOptions.get(j).getLabel();
						}
					}
				}
			}
			dropDownOptions.add(options);
		}

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("data");
		XSSFRow row = sheet.createRow(0);

		XSSFCellStyle headerStyle = workbook.createCellStyle();
		XSSFFont headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 11);
		headerFont.setBold(true);
		headerStyle.setFont(headerFont);

		for (int i = 0; i < table.getHeaders().size(); i++) {
			XSSFCell cell = row.createCell(i);
			cell.setCellValue(table.getHeaders().get(i));
			cell.setCellStyle(headerStyle);

			String[] options = dropDownOptions.get(i);
			if (options != null) {
				XSSFDataValidationHelper validationHelper = new XSSFDataValidationHelper(sheet);
				XSSFDataValidationConstraint validationConstraint = (XSSFDataValidationConstraint) validationHelper
						.createExplicitListConstraint(options);
				CellRangeAddressList addressList = new CellRangeAddressList(1,
						workbook.getSpreadsheetVersion().getLastRowIndex(), i, i);
				XSSFDataValidation validation = (XSSFDataValidation) validationHelper
						.createValidation(validationConstraint, addressList);
				validation.setShowErrorBox(true);
				validation.setSuppressDropDownArrow(true);
				sheet.addValidationData(validation);
			}
		}

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		workbook.write(byteArrayOutputStream);
		workbook.close();

		request.setFileName("template.xlsx");
		String fileData = Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
		request.setFileData(fileData);

		return request;
	}

	public UploadDataTableRequestResponse uploadDataTable(Protocol protocol, UploadDataTableRequestResponse request)
			throws IOException, FormDataRelatedException, QueryRelatedException, FormRelatedException,
			ClassRelatedException, InstanceRelatedException, InstanceHistoryRelatedException,
			ReferenceRelatedException {

		AltDataTable table = request.getTable();

		String fileDataBase64 = request.getFileDataBase64();
		byte[] fileBytes = Base64.getDecoder().decode(fileDataBase64);
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(fileBytes);
		XSSFWorkbook workbook = new XSSFWorkbook(byteArrayInputStream);
		XSSFSheet sheet = workbook.getSheet("data");

		ReadRequest formRequest = new ReadRequest(protocol, null, -1, -1, -1, table.getFormPopup(), null, null);
		AltForm form = formService.readFromId(formRequest);
		Map<String, List<AltOption>> optionMap = getAllAttributeValuesListOfForm(request.getTenantId(),
				table.getFormPopup().toString(), protocol, form, request.getFormRoles());

		List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
		Iterator<Row> rowIterator = sheet.rowIterator();
		if (rowIterator.hasNext())
			rowIterator.next();
		while (rowIterator.hasNext()) {
			Map<String, Object> rowData = new HashMap<String, Object>();
			int i = 0;
			Row row = rowIterator.next();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				String column = table.getDisplayedColumns().get(i);
				String value = cellIterator.next().getStringCellValue();
				String fieldName = null;
				String attributeName = null;
				if (column.indexOf("..") != -1) {
					fieldName = column.split("\\.\\.")[0];
					attributeName = column.split("\\.\\.")[1];

					for (String key : optionMap.keySet()) {
						String keyFieldName = key.split("_")[1];
						if (attributeName.equalsIgnoreCase(keyFieldName)) {
							List<AltOption> altOptions = optionMap.get(key);
							for (AltOption option : altOptions) {
								if (value.equals(option.getLabel()))
									value = option.getValue().toString();
							}
						}
					}

				} else
					fieldName = column;
				rowData.put(fieldName, value);
				i++;
			}
			rows.add(rowData);
		}

		workbook.close();

		FormDataSaveRequest formDataSaveRequest = new FormDataSaveRequest();
		formDataSaveRequest.setFormName(form.getUiClassName());
		formDataSaveRequest.setFormId(form.getId());
		formDataSaveRequest.setAppId(request.getAppId());
		for (Map<String, Object> rowData : rows) {
			rowData.put("Status", null);
			rowData.put("Stage", null);
			rowData.put("OwnerEmployeeId", protocol.getEmployeeId().toString());
			formDataComponent.saveInstance(protocol, rowData, form.getMetaClassId().toString(), formDataSaveRequest);
		}

		request.setFileDataBase64(null);
		return request;
	}

	/**
	 * @author vaibhav.kashyap
	 */
	public AltDataTable getDataForTableByExternalAPI(String componentName, String formName, Protocol protocol,
			Map<String, String> filterAttributes, RoleTO formRoles) {
		return formCommonService.getDataForTableByExternalAPI(componentName, formName, protocol, filterAttributes,
				formRoles);
	}

	public ResponseEntity<ObjectNode> uploadFile(MultipartFile file) {
		String filePath = null;
		ObjectNode objectNode = null;
		try {
			String writeFileServerPath = securityPropertiesConf.getApplogowriteurl();
			writeFileServerPath += File.separator + "files";
			filePath = securityPropertiesConf.getApplogoreadurl() + "/files";
			String fileName = null;
			fileName = "file_" + ThreadLocalRandom.current().nextInt(0, 10000 + 1) + new Date().getTime() + "."
					+ FilenameUtils.getExtension(file.getOriginalFilename());
			filePath += "/" + fileName;
			fileWriterUtil.writeFile(file, writeFileServerPath, fileName);
			objectNode = new ObjectMapper().createObjectNode();
			objectNode.put("dataFileName", file.getOriginalFilename());
			objectNode.put("dataFilePath", filePath);
		} catch (Exception e) {
			return new ResponseEntity<ObjectNode>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(objectNode, HttpStatus.OK);
	}

	public ResponseEntity<Resource> downloadFile(String filePath) throws MalformedURLException {
		UrlResource resource = new UrlResource(filePath);
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

}

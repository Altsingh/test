package com.alt.formengine.service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.formframework.request.FormCRUDData;
import com.alt.datacarrier.formframework.request.FormDataSaveRequest;
import com.alt.datacarrier.formframework.response.FormCreateResponse;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.db.core.AltForm;
import com.alt.datacarrier.kernel.db.core.ClassMeta;
import com.alt.datacarrier.kernel.db.core.InstanceData;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.kernel.db.request.ReadRequest;
import com.alt.datacarrier.kernel.db.request.TaskCodeRecordTransport;
import com.alt.datacarrier.kernel.uiclass.AltAbstractBaseComponent;
import com.alt.datacarrier.kernel.uiclass.AltAbstractComponent;
import com.alt.datacarrier.kernel.uiclass.AltAttachmentsComponent;
import com.alt.datacarrier.kernel.uiclass.AltButtonComponent;
import com.alt.datacarrier.kernel.uiclass.AltCheckboxComponent;
import com.alt.datacarrier.kernel.uiclass.AltExcelComponent;
import com.alt.datacarrier.kernel.uiclass.AltLinkComponent;
import com.alt.datacarrier.kernel.uiclass.AltMultiSelectComponent;
import com.alt.datacarrier.kernel.uiclass.AltPanelComponent;
import com.alt.datacarrier.kernel.uiclass.AltTabbedPanelComponent;
import com.alt.datacarrier.kernel.uiclass.EnumHTMLControl;
import com.alt.datacarrier.workflow.ProtocolTO;
import com.alt.datacarrier.workflow.RoleActorTO;
import com.alt.datacarrier.workflow.TransitionRequestTO;
import com.alt.datacarrier.workflow.TransitionResponseTO;
import com.alt.datacarrier.workflow.WorkflowHistoryRequestTO;
import com.alt.datacarrier.workflow.WorkflowHistoryResponseTO;
import com.alt.datakernel.dao.IInstanceDS;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.FormRelatedException;
import com.alt.datakernel.exception.InstanceHistoryRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.MailRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.exception.RuleRelatedException;
import com.alt.datakernel.exception.TaskCodeRecordRelatedException;
import com.alt.datakernel.model.Instance;
import com.alt.datakernel.service.IAltAttributeDataService;
import com.alt.datakernel.service.IClassService;
import com.alt.datakernel.service.IFormService;
import com.alt.datakernel.service.IInstanceService;
import com.alt.datakernel.service.impl.TaskCodeRecordService;
import com.alt.formengine.config.KafkaConfig;
import com.alt.formengine.config.SecurityPropertiesConf;
import com.alt.formengine.exception.FormDataRelatedException;
import com.alt.rule.model.Rule;
import com.alt.rule.transport.request.RuleCreationRequest;
import com.alt.rule.transport.response.RuleCreationResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.peoplestrong.framework.messageproducer.kafka.KafkaMessageProducer;
import com.peoplestrong.framework.messageproducer.kafka.KafkaProducerConfig;
import com.peoplestrong.framework.messageproducer.kafka.KafkaTopic;
import com.peoplestrong.framework.messageproducer.messages.paas.PaasCommunicationMessage;

public class FormDataComponent {

	@Autowired
	IInstanceService instanceService;

	@Autowired
	IClassService classService;

	@Autowired
	IFormService formService;

	@Autowired
	IAltAttributeDataService iAltAttributeDataService;

	@Autowired
	FormCommonService formCommonService;

	@Autowired
	RuleExecutionService ruleExecutionService;

	@Autowired
	SecurityPropertiesConf securityPropertiesConf;

	@Autowired
	TaskCodeRecordService taskCodeRecordService;

	@Autowired
	IInstanceDS instanceDS;

	@Autowired
	KafkaConfig kafkaConfig;

	private static final Logger logger = Logger.getLogger(FormDataComponent.class);

	public List<AltAbstractComponent> unNestComponentList(List<AltAbstractComponent> components) {
		List<AltAbstractComponent> flatComponentList = new ArrayList<>();
		for (AltAbstractComponent component : components) {
			if (component.getControlType().equals(EnumHTMLControl.PANEL)) {
				flatComponentList.addAll(this.unNestComponentList(((AltPanelComponent) component).getComponentList()));
			} else if (component.getControlType().equals(EnumHTMLControl.TAB_PANEL)) {
				AltTabbedPanelComponent tabbedPanelComponent = (AltTabbedPanelComponent) component;
				for (AltPanelComponent panel : tabbedPanelComponent.getPanels()) {
					flatComponentList.addAll(this.unNestComponentList(panel.getComponentList()));
				}
			} else {
				flatComponentList.add(component);
			}
		}
		return flatComponentList;
	}

	/**
	 * {@link #getFormDetailsByID(String, Protocol)} : fetches form details on basis
	 * of formID passed
	 * 
	 * @return ResponseEntity<FormCRUDData>
	 * @author vaibhav.kashyap
	 * @throws InstanceRelatedException
	 */
	@Transactional
	public ResponseEntity<FormCRUDData> getFormDetailsByID(String formID, Protocol protocol)
			throws FormDataRelatedException, QueryRelatedException, FormRelatedException, ClassRelatedException,
			InstanceRelatedException {
		ReadRequest request = new ReadRequest(protocol, null, -1, -1, -1, Integer.valueOf(formID), null, null);
		AltForm form;
		form = formService.readFromId(request);
		return formCommonService.convertToResponse(protocol, form, null, null, null, null);
	}

	public ResponseEntity<FormCreateResponse> saveFormData(FormDataSaveRequest request, Protocol protocol)
			throws FormDataRelatedException, InstanceRelatedException, ClassRelatedException, QueryRelatedException,
			FormRelatedException, RuleRelatedException, ClientProtocolException, IOException, ClassCastException,
			ParseException, TaskCodeRecordRelatedException {
		// list of components
		// segregate components on the basis of class
		// save one by one
		ResponseEntity<FormCRUDData> fetchedFormDetails = null;
		FormCreateResponse resp = new FormCreateResponse();
		List<Integer> commIdList = new ArrayList<Integer>();
		try {
			/**
			 * Fetching form details which is required to get associated meta class id
			 */
			fetchedFormDetails = getFormDetailsByID(request.getFormId(), protocol);
		} catch (Exception ex) {
			logger.error("Form details not found for which submition is performed", ex);
			fetchedFormDetails = null;
		}
		Map<String, Object> componentFilledByUserByValues = null;

		Map<String, Map<String, Object>> classAttributeValueMap = new HashMap<>();
		List<AltAbstractComponent> flatComponentList = this.unNestComponentList(request.getComponents());

		if (flatComponentList.size() > 0)
			componentFilledByUserByValues = new HashMap<String, Object>();

		RuleCreationResponse rulesListAppliedOnForm = null;
		Integer actionId = null;
		for (AltAbstractComponent component : flatComponentList) {
			AltAbstractBaseComponent comp = null;
			if (component.getControlType().equals(EnumHTMLControl.BUTTON)) {
				AltButtonComponent btn = (AltButtonComponent) component;
				if (Boolean.TRUE.equals(btn.getClicked())) {
					actionId = btn.getActionId();
				}
				if (btn.getCommIdList() != null && btn.getCommIdList().size() > 0) {
					if (btn.getCommIdList().size() > 1)
						commIdList.addAll(btn.getCommIdList());
					else if (btn.getCommIdList().size() == 1)
						commIdList.add(btn.getCommIdList().get(0));
				}
				if (btn.getRuleObjects() != null && btn.getRuleObjects().size() > 0) {
					rulesListAppliedOnForm = getRuleById(btn.getRuleObjects());
					if (rulesListAppliedOnForm != null && (rulesListAppliedOnForm.getRules() == null
							|| rulesListAppliedOnForm.getRules().size() <= 0))
						rulesListAppliedOnForm = null;
				}
				continue;
			} else if (component.getControlType().equals(EnumHTMLControl.EXCEL)) {
				prepareComponentByUserFilledInValues((AltExcelComponent) component, componentFilledByUserByValues);
			} else if (component.getControlType().equals(EnumHTMLControl.ATTACHMENTS)) {
				prepareComponentByUserFilledInValues((AltAttachmentsComponent) component,
						componentFilledByUserByValues);
			} else if (component.getControlType().equals(EnumHTMLControl.CHECKBOX)) {
				prepareComponentByUserFilledInValues((AltCheckboxComponent) component, componentFilledByUserByValues);
			} else if (component.getControlType().equals(EnumHTMLControl.LINK)) {
				prepareComponentByUserFilledInValues((AltLinkComponent) component, componentFilledByUserByValues);
			} else {

				comp = (AltAbstractBaseComponent) component;

				/**
				 * if the input entered by user for a given component is not null then put the
				 * value in attribute map
				 */
				if (comp.getValue() == null || comp.getValue().toString().equals("") || comp.getDataType() == null)
					continue;
				/**
				 * depending on the data type of the user filled in data
				 */
				try {
					prepareComponentByUserFilledInValues(comp, componentFilledByUserByValues);
				} catch (Exception e) {
					resp.setResponseMessage("Incorrect values entered by user!!!");
					resp.setSuccess(false);
					return new ResponseEntity<>(resp, HttpStatus.OK);
				}

			}

			Map<String, Object> attrValueMap = new HashMap<>();
			if (comp != null && comp.getDbClassWrite() != null && (!comp.getDbClassWrite().equals(""))
					&& comp.getDbAttrWrite() != null && (!comp.getDbAttrWrite().equals(""))) {
				attrValueMap.put(comp.getDbAttrWrite(), comp.getValue());
				classAttributeValueMap.put(comp.getDbClassWrite(), attrValueMap);
			}
		}

		String taskCode = "";
		if (request.getInstanceId() == null || request.getInstanceId().equalsIgnoreCase("")) {
			/**
			 * In this case new instance is being created hence, new taskcode is required to
			 * be generated.
			 */
			taskCode = generateTaskCode(request.getAppId(), fetchedFormDetails.getBody().getMetaclassid().toString(),
					protocol);
		} else {
			/**
			 * request received is for instance update hence no new taskcode is required.
			 * Fetch taskCode associated with instance id for update.
			 */
			taskCode = getTaskCodeForDataUpdate(request.getInstanceId(), protocol);
		}

		if (taskCode != null && !taskCode.equalsIgnoreCase("")) {
			componentFilledByUserByValues.put("taskCode", taskCode);
		}
		if (!componentFilledByUserByValues.containsKey("OwnerEmployeeId")) {
			componentFilledByUserByValues.put("OwnerEmployeeId", protocol.getEmployeeId().toString());
		}
		if (request.getInstanceId() != null && !request.getInstanceId().isEmpty()) {
			Instance updatedInstance = instanceDS.find(protocol, Integer.parseInt(request.getInstanceId()));
			componentFilledByUserByValues.put("OwnerEmployeeId",
					updatedInstance.getAttributes().getAttribute().get("OwnerEmployeeId"));
		}

		Set<String> classIds = classAttributeValueMap.keySet();
		for (String classId : classIds) {
			Map<String, Object> map = new HashMap<>();
			Map<String, Object> attributes = classAttributeValueMap.get(classId);
			boolean arrayList = false;
			for (Map.Entry<String, Object> entry : attributes.entrySet()) {
				if (entry.getValue() instanceof ArrayList) {
					arrayList = true;
					List<String> values = (ArrayList<String>) entry.getValue();
					for (String value : values) {
						map.put(entry.getKey(), value);
						// saveInstance(protocol, map, classId, request);
						map.clear();
					}
				} else {
					arrayList = false;
					map.put(entry.getKey(), entry.getValue());

				}
			}
			if (arrayList == false) {
				// saveInstance(protocol, map, classId, request);
			}
		}

		if (componentFilledByUserByValues == null || componentFilledByUserByValues.size() == 1
				|| fetchedFormDetails == null) {
			resp.setResponseMessage("Warning : Cannot submit empty form!!!");
			resp.setSuccess(false);
		} else if (fetchedFormDetails != null && (fetchedFormDetails.getBody().getMetaclassid() == null
				|| fetchedFormDetails.getBody().getMetaclassid() <= 0)) {
			resp.setResponseMessage("Warning : You're trying to access unpublished form!!!");
			resp.setSuccess(false);
		} else {
			try {
				boolean executeRuleCompletion = false;

				if (rulesListAppliedOnForm != null && rulesListAppliedOnForm.getRules() != null
						&& rulesListAppliedOnForm.getRules().size() > 0) {
					executeRuleCompletion = executeRule(resp, rulesListAppliedOnForm, componentFilledByUserByValues);
				}

				if (executeRuleCompletion || (rulesListAppliedOnForm == null)) {
					request.setProcessHistory(true);
					InstanceData instanceData = saveInstance(protocol, componentFilledByUserByValues,
							fetchedFormDetails.getBody().getMetaclassid().toString(), request);

					if (commIdList.size() > 0) {
						updateCommunicationRecord(componentFilledByUserByValues, commIdList, protocol, request);
					}

					WorkflowHistoryResponseTO workflowHistoryResponseTO = processWorkflow(protocol.getOrgId(),
							protocol.getUserName(), actionId, fetchedFormDetails.getBody().getMetaclassid(),
							(long) instanceData.getInstanceId(), request.getSysUserWorkflowHistoryId(),
							request.getEmployeeId(), protocol.getTenantId(), protocol.getUserId(), taskCode);
					componentFilledByUserByValues.put("Status",
							workflowHistoryResponseTO == null ? null : workflowHistoryResponseTO.getStatus());
					componentFilledByUserByValues.put("Stage",
							workflowHistoryResponseTO == null ? null : workflowHistoryResponseTO.getStage());
					request.setInstanceId(String.valueOf(instanceData.getInstanceId()));
					request.setProcessHistory(false);
					saveInstance(protocol, componentFilledByUserByValues,
							fetchedFormDetails.getBody().getMetaclassid().toString(), request);

					resp.setSuccess(true);
					resp.setResponseMessage(
							workflowHistoryResponseTO == null || workflowHistoryResponseTO.getAlertMessage() == null
									? "Instances Created."
									: workflowHistoryResponseTO.getAlertMessage());
				}
			} catch (Exception ex) {
				logger.error("Instance creation failed for request" + request, ex);
				resp.setResponseMessage("Instances Creation Failed.");
				resp.setSuccess(false);
			}

		}

		return new ResponseEntity<>(resp, HttpStatus.OK);

	}

	/**
	 * {@link #getTaskCodeForDataUpdate(String, Protocol)}}
	 * 
	 * @author vaibhav.kashyap
	 */
	private String getTaskCodeForDataUpdate(String instanceId, Protocol protocol) {
		InstanceRequest instanceRequest = new InstanceRequest.Builder().withInstanceId(Integer.parseInt(instanceId))
				.build();
		HashMap<String, List<String>> presavedUserValulesByComponent = null;
		String taskCode = null;
		try {
			presavedUserValulesByComponent = instanceService.getSavedFormDataByContext(instanceRequest, protocol);
			if (presavedUserValulesByComponent != null && presavedUserValulesByComponent.size() > 0) {
				if (presavedUserValulesByComponent.containsKey("taskCode")) {
					taskCode = (String) presavedUserValulesByComponent.get("taskCode").get(0);
				}
			}

		} catch (Exception ex) {
			logger.error("Exception thrown while fetching taskcode for data update" + ex);
			taskCode = null;
		}

		return taskCode;
	}

	/**
	 * {@link #generateTaskCode(String, String, String, Protocol)}
	 * 
	 * @author vaibhav.kashyap
	 */
	private String generateTaskCode(String appId, String classId, Protocol protocol)
			throws TaskCodeRecordRelatedException {
		if (appId == null || appId.equalsIgnoreCase("") || classId == null || classId.equalsIgnoreCase(""))
			return null;

		TaskCodeRecordTransport transport = new TaskCodeRecordTransport();
		transport.setAppId(Integer.parseInt(appId));
		// transport.setFormId(formId);
		transport.setClassId(classId);
		String strCode = null;
		try {
			synchronized (this) {
				transport = taskCodeRecordService.getLastUpdateCodeDetailsByClassId(transport);

				if (transport != null && transport.getTaskCodeID() != null && transport.getTaskCodeID() > 0) {
					int code = Integer.parseInt(transport.getLastCodeUsed()) + 1;
					strCode = transport.getFormInitials() + code;

					transport.setLastCodeUsed(String.valueOf(code));
					transport.setProtocol(protocol);
					updateLastUsedTaskCode(transport);
				}

			}

		} catch (Exception e) {
			logger.error("Exception thrown while generating taskcode" + e);
			strCode = null;
		}

		return strCode;
	}

	private TaskCodeRecordTransport updateLastUsedTaskCode(TaskCodeRecordTransport transport)
			throws TaskCodeRecordRelatedException {
		TaskCodeRecordTransport transportTO = null;
		try {
			transportTO = taskCodeRecordService.updateTaskCodeForClass(transport);
		} catch (Exception e) {
			logger.error("Exception thrown while updating taskCode for id : " + transport.getTaskCodeID(), e);
			transportTO = null;
		}
		return (transportTO != null) ? transportTO : null;
	}

	private boolean updateCommunicationRecord(Map<String, Object> componentFilledByUserByValues,
			List<Integer> commIdList, Protocol protocol, FormDataSaveRequest request) throws MailRelatedException {
		if (componentFilledByUserByValues == null || componentFilledByUserByValues.size() <= 0)
			return false;

		boolean sent = true;
		Map<String, String> placeholdermap = new HashMap<String, String>();
		for (String key : componentFilledByUserByValues.keySet()) {
			String value = null;
			value = componentFilledByUserByValues.get(key) == null ? ""
					: componentFilledByUserByValues.get(key).toString();
			placeholdermap.put("@@" + key.replaceAll("\\s", "") + "@@", value);
		}

		KafkaProducerConfig kpconfig = new KafkaProducerConfig();
		kpconfig.setGroupId(kafkaConfig.getGroupid());
		kpconfig.setKafkaTopicPrefix(kafkaConfig.getTopicprefix());
		kpconfig.setBootstrapServers(kafkaConfig.getBootstrapservers());
		KafkaMessageProducer messageProducer = new KafkaMessageProducer(kpconfig);

		PaasCommunicationMessage passCommObj = null;
		try {
			for (int commId : commIdList) {
				passCommObj = new PaasCommunicationMessage();
				passCommObj
						.setEmployeeID(Integer.parseInt((String) componentFilledByUserByValues.get("OwnerEmployeeId")));
				passCommObj.setOrganizationId(protocol.getOrgId());
				passCommObj.setTenantId(protocol.getTenantId());
				passCommObj.setContextEntityId(0);
				passCommObj.setContextInstanceId(0);
				passCommObj.setAttachmentFolderPath(null);
				passCommObj.setMimeType("text/html");
				passCommObj.setMailFrom(null);
				passCommObj.setMailSendDate(null);
				passCommObj.setHasMailUser(false);
				passCommObj.setStatusID(98);
				passCommObj.setContextSenderUserID(protocol.getUserId());
				passCommObj.setCommID(new Integer(commId));
				passCommObj.setPlaceholderValueMap(placeholdermap);
				passCommObj.setFormName(request.getFormName());
				passCommObj.setAppName(request.getAppName());
				ObjectMapper mapper = new ObjectMapper();
				logger.info("Sending Mail, writing into Kafka, producing message: "
						+ mapper.writeValueAsString(passCommObj));
				messageProducer.produceMessage(passCommObj, KafkaTopic.COMMUNICATION_PAAS_MAIL);
			}

		} catch (Exception ex) {
			sent = false;
		}

		return sent;
	}

	private WorkflowHistoryResponseTO processWorkflow(Integer organizationId, String userName, Integer stageActionId,
			Integer contextEntityId, Long contextInstanceId, Long workflowHistoryId, Integer employeeId,
			Integer tenantId, Integer userId, String taskCode) throws NoSuchAlgorithmException, IOException {
		TransitionRequestTO transitionRequest = new TransitionRequestTO();
		transitionRequest.setStageActionId(stageActionId);
		transitionRequest.setRuleOutcome("goToDefaultTransition");
		ProtocolTO protocol = new ProtocolTO();
		protocol.setTenantID(tenantId);
		transitionRequest.setProtocolTO(protocol);
		transitionRequest.setEmployeeId(employeeId);
		transitionRequest.setUserID(userId);
		TransitionResponseTO transitionResponse = null;
		HttpResponse rawResponse = HttpService.callRestAPI(
				securityPropertiesConf.getWfapi2()/* "https://s2demo.sohum.com/service/api/workflow/nextStageInfo" */,
				transitionRequest);
		transitionResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
				TransitionResponseTO.class);

		if (transitionResponse.getRoleActorsMap() == null)
			return null;

		WorkflowHistoryRequestTO workflowRequest = new WorkflowHistoryRequestTO();
		workflowRequest.setContextEntityId(contextEntityId);
		workflowRequest.setContextInstanceId(contextInstanceId);
		workflowRequest.setTransitionId(transitionResponse.getTransitionId());
		List<RoleActorTO> actors = new ArrayList<RoleActorTO>();
		for (List<RoleActorTO> responseActors : transitionResponse.getRoleActorsMap().values()) {
			actors.addAll(responseActors);
		}
		workflowRequest.setRoleActors(actors);
		workflowRequest.setUsername(userName);
		workflowRequest.setOrganizationId(organizationId);
		workflowRequest.setPreviousWorkflowHistoryId(workflowHistoryId);
		workflowRequest.setTaskStatus(transitionResponse.getToMessage());
		workflowRequest.setTaskCode(taskCode);
		WorkflowHistoryResponseTO workflowResponse = null;
		rawResponse = HttpService.callRestAPI(
				securityPropertiesConf
						.getWfapi3()/* "https://s2demo.sohum.com/service/api/workflow/createWorkflowHistory" */,
				workflowRequest);
		workflowResponse = new ObjectMapper().readValue(rawResponse.getEntity().getContent(),
				WorkflowHistoryResponseTO.class);
		return workflowResponse;
	}

	public void prepareComponentByUserFilledInValues(AltAbstractBaseComponent component,
			Map<String, Object> componentFilledByUserByValues)
			throws ClassCastException, ParseException, NumberFormatException {
		String dataType = component.getDataType().getName().toLowerCase();
		switch (dataType) {
		case "integer":
			componentFilledByUserByValues.put(component.getName(),
					Integer.parseInt(component.getValue().toString().trim()));
			break;
		case "string":
			if (component.getControlType().equals(EnumHTMLControl.MULTI_SELECT)) {
				List<String> selectedValues = ((AltMultiSelectComponent) component).getValues();
				if (selectedValues != null && !selectedValues.isEmpty()) {
					componentFilledByUserByValues.put(component.getName(), selectedValues);
				}
			} else if (component.getControlType().equals(EnumHTMLControl.DROPDOWN)) {
				componentFilledByUserByValues.put(component.getName(), component.getValue().toString().trim());
				if (Boolean.TRUE.equals(component.getInstanceOwnerFlag())) {
					componentFilledByUserByValues.put("OwnerEmployeeId", component.getValue().toString().trim());
				}
			} else {
				componentFilledByUserByValues.put(component.getName(), component.getValue().toString().trim());
			}
			break;
		case "double":
			componentFilledByUserByValues.put(component.getName(),
					Double.parseDouble((component.getValue().toString())));
			break;
		case "float":
			componentFilledByUserByValues.put(component.getName(),
					Float.parseFloat((component.getValue().toString().trim())));
			break;
		case "long":
			componentFilledByUserByValues.put(component.getName(),
					Long.parseLong((component.getValue().toString().trim())));
			break;
		case "date":
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(component.getValue().toString()));
			componentFilledByUserByValues.put(component.getName(), component.getValue().toString());
			break;
		case "image":
			if (component.getValue() != null && !component.getValue().toString().equalsIgnoreCase(""))
				componentFilledByUserByValues.put(component.getName(), component.getValue().toString());
			break;
		case "textarea":
			componentFilledByUserByValues.put(component.getName(), component.getValue().toString().trim());
			break;
		default:
			break;
		}

	}

	private void prepareComponentByUserFilledInValues(AltExcelComponent component,
			Map<String, Object> componentFilledByUserByValues) {
		componentFilledByUserByValues.put(component.getName(), component.getDataFilePath());
	}

	private void prepareComponentByUserFilledInValues(AltAttachmentsComponent component,
			Map<String, Object> componentFilledByUserByValues) throws JsonProcessingException {
		componentFilledByUserByValues.put(component.getName(),
				new ObjectMapper().writeValueAsString(component.getFiles()));
	}

	private void prepareComponentByUserFilledInValues(AltCheckboxComponent component,
			Map<String, Object> componentFilledByUserByValues) throws JsonProcessingException {
		componentFilledByUserByValues.put(component.getName(), Boolean.TRUE.equals(component.getValue()));
	}

	private void prepareComponentByUserFilledInValues(AltLinkComponent component,
			Map<String, Object> componentFilledByUserByValues) throws JsonProcessingException {
		ObjectNode link = new ObjectMapper().createObjectNode();
		link.put("linkName", component.getLinkName());
		link.put("linkUrl", component.getLinkUrl());
		componentFilledByUserByValues.put(component.getName(), new ObjectMapper().writeValueAsString(link));
	}

	private boolean executeRule(FormCreateResponse response,
			RuleCreationResponse ruleCreationResponseWithRuleListForForm,
			Map<String, Object> componentFilledByUserByValues) {
		Boolean allExecution = true;
		String ruleExecutedSuccessfully[] = null;
		ruleExecutedSuccessfully = ruleExecutionService.executeRule(ruleCreationResponseWithRuleListForForm,
				componentFilledByUserByValues);
		if (ruleExecutedSuccessfully != null) {
			if (ruleExecutedSuccessfully[0] != null) {
				response.setResponseMessage(ruleExecutedSuccessfully[0]);
				response.setSuccess(false);
				allExecution = false;
			}
			if (ruleExecutedSuccessfully[1] != null) {
				response.setResponseMessage(ruleExecutedSuccessfully[1]);
				response.setSuccess(false);
				allExecution = false;
			}

		}
		return allExecution;
	}

	public InstanceData saveInstance(Protocol protocol, Map<String, Object> componentFilledByUserByValues,
			String classId, FormDataSaveRequest formDataSaveRequest)
			throws InstanceRelatedException, ClassRelatedException, InstanceHistoryRelatedException {
		ClassMeta data = classService.read(protocol, Integer.valueOf(classId));
		InstanceRequest request = null;
		if (formDataSaveRequest.getInstanceId() != null && !formDataSaveRequest.getInstanceId().trim().isEmpty()) {
			request = new InstanceRequest(new InstanceRequest.Builder().withAttributes(componentFilledByUserByValues)
					.withProtocol(protocol).withClassName(data.getName())
					.withFormName(formDataSaveRequest.getFormName()).withStatus(EnumDocStatusType.ACTIVE)
					.withInstanceId(Integer.parseInt(formDataSaveRequest.getInstanceId()))
					.withFormId(Integer.parseInt(formDataSaveRequest.getFormId()))
					.withAppId(Integer.parseInt(formDataSaveRequest.getAppId()))
					.withProcessHistory(formDataSaveRequest.isProcessHistory()));
		} else {
			request = new InstanceRequest(new InstanceRequest.Builder().withAttributes(componentFilledByUserByValues)
					.withProtocol(protocol).withClassName(data.getName())
					.withFormName(formDataSaveRequest.getFormName()).withStatus(EnumDocStatusType.ACTIVE)
					.withFormId(Integer.parseInt(formDataSaveRequest.getFormId()))
					.withAppId(Integer.parseInt(formDataSaveRequest.getAppId()))
					.withProcessHistory(formDataSaveRequest.isProcessHistory()));
		}
		return instanceService.create(request);
	}

	private RuleCreationResponse getRuleById(List<Rule> rules)
			throws RuleRelatedException, ClientProtocolException, IOException {

		List<Long> ruleIdList = new ArrayList<>();
		for (Rule rl : rules) {
			ruleIdList.add(rl.getRuleId());
		}

		RuleCreationRequest ruleRequest = new RuleCreationRequest();
		ruleRequest.setRuleIdList(ruleIdList);

		return ruleExecutionService.getRuleById(ruleRequest);
	}

}

package com.alt.formengine.service;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.alt.datacarrier.formsecurity.core.AltOneCustomFormSecurityRequest;
import com.alt.datacarrier.kernel.db.request.CustomAppSetupRequest;
import com.alt.formengine.objectreader.transport.ObjectReaderRequestTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class HttpService {

	private static final Logger logger = Logger.getLogger(HttpService.class);

	protected static HttpResponse getData(String url, AltOneCustomFormSecurityRequest request)
			throws IOException, NoSuchAlgorithmException {
		ObjectMapper mapper = new ObjectMapper();
		StringEntity requestEntity = new StringEntity(mapper.writeValueAsString(request), ContentType.APPLICATION_JSON);
		logger.warn("Requesting to url" + url);
		logger.warn("Request sent: " + mapper.writeValueAsString(request));

		HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
		DefaultHttpClient client = new DefaultHttpClient();
		SchemeRegistry registry = new SchemeRegistry();
		SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
		socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
		registry.register(new Scheme("https", socketFactory, 443));
		SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
		DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
		HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

		HttpPost postMethod = new HttpPost(url);
		postMethod.setHeader("Content-Type", "application/json");
		postMethod.setEntity(requestEntity);
		HttpResponse rawResponse = httpClient.execute(postMethod);
		logger.warn("Response : " + rawResponse.getEntity().getContent());
		return rawResponse;
	}

	public static HttpResponse getObjectReaderData(String url, ObjectReaderRequestTO requestTO)
			throws IOException, NoSuchAlgorithmException {
		ObjectMapper mapper = new ObjectMapper();
		StringEntity requestEntity = new StringEntity(mapper.writeValueAsString(requestTO),
				ContentType.APPLICATION_JSON);
		logger.warn("Requesting to url" + url);
		logger.warn("Request sent: " + mapper.writeValueAsString(requestTO));

		HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
		DefaultHttpClient client = new DefaultHttpClient();
		SchemeRegistry registry = new SchemeRegistry();
		SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
		socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
		registry.register(new Scheme("https", socketFactory, 443));
		SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
		DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
		HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

		HttpPost postMethod = new HttpPost(url);
		postMethod.setHeader("Content-Type", "application/json");
		postMethod.setEntity(requestEntity);
		HttpResponse rawResponse = httpClient.execute(postMethod);
		logger.warn("Response : " + rawResponse.getEntity().getContent());

		return rawResponse;
	}

	public static HttpResponse publishAppDetailsToAdmin(String url, CustomAppSetupRequest requestTO)
			throws IOException, NoSuchAlgorithmException {
		ObjectMapper mapper = new ObjectMapper();
		StringEntity requestEntity = new StringEntity(mapper.writeValueAsString(requestTO),
				ContentType.APPLICATION_JSON);
		logger.warn("Requesting to url" + url);
		logger.warn("Request sent: " + mapper.writeValueAsString(requestTO));

		HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
		DefaultHttpClient client = new DefaultHttpClient();
		SchemeRegistry registry = new SchemeRegistry();
		SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
		socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
		registry.register(new Scheme("https", socketFactory, 443));
		SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
		DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
		HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

		HttpPost postMethod = new HttpPost(url);
		postMethod.setHeader("Content-Type", "application/json");
		postMethod.setEntity(requestEntity);
		HttpResponse rawResponse = httpClient.execute(postMethod);
		logger.warn("Response : " + rawResponse.getEntity().getContent());
		return rawResponse;
	}

	public static HttpResponse callRestAPI(String url, Object request) throws IOException, NoSuchAlgorithmException {
		ObjectMapper mapper = new ObjectMapper();
		StringEntity requestEntity = new StringEntity(mapper.writeValueAsString(request), ContentType.APPLICATION_JSON);
		logger.warn("Requesting to url" + url);
		logger.warn("Request sent: " + mapper.writeValueAsString(request));

		HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
		DefaultHttpClient client = new DefaultHttpClient();
		SchemeRegistry registry = new SchemeRegistry();
		SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
		socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
		registry.register(new Scheme("https", socketFactory, 443));
		SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
		DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());
		HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

		HttpPost postMethod = new HttpPost(url);
		postMethod.setHeader("Content-Type", "application/json");
		postMethod.setEntity(requestEntity);
		HttpResponse rawResponse = httpClient.execute(postMethod);
		logger.warn("Response : " + rawResponse.getEntity().getContent());
		return rawResponse;
	}
}

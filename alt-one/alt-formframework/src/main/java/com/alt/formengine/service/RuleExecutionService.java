package com.alt.formengine.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

import com.alt.datakernel.exception.RuleRelatedException;
import com.alt.formengine.rule.conf.RuleConf;
import com.alt.formengine.rule.util.RuleOperations;
import com.alt.formengine.rule.util.TransactionOperations;
import com.alt.formengine.rule.util.ValidationOperations;
import com.alt.rule.model.Rule;
import com.alt.rule.model.TransactionCase;
import com.alt.rule.model.TransactionRule;
import com.alt.rule.model.ValidationCase;
import com.alt.rule.model.ValidationCondition;
import com.alt.rule.model.ValidationRule;
import com.alt.rule.transport.request.RuleCreationRequest;
import com.alt.rule.transport.response.RuleCreationResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RuleExecutionService {

	@Autowired
	RuleConf ruleConf;
	
	private static final String OPERAND_MISMATCH = "Operands Data Type Mismatch!!!";

	public RuleCreationResponse getRuleById(RuleCreationRequest ruleCreationRequest)
			throws RuleRelatedException, ClientProtocolException, IOException {

		RuleCreationResponse ruleCreationResponse = null;

		ObjectMapper mapper = new ObjectMapper();
		StringEntity requestEntity = new StringEntity(mapper.writeValueAsString(ruleCreationRequest),
				ContentType.APPLICATION_JSON);

		String endpoint = ruleConf.getUrl() + "/getRuleById";
		HttpPost httppost = new HttpPost(endpoint);
		httppost.setEntity(requestEntity);
		httppost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

		// Execute and get the response.
		HttpClient httpclient = HttpClients.createDefault();
		HttpResponse response = httpclient.execute(httppost);
		if (response != null) {
			ruleCreationResponse = new ObjectMapper().readValue(response.getEntity().getContent(),
					RuleCreationResponse.class);
		}
		return ruleCreationResponse;
	}

	public String[] executeRule(RuleCreationResponse rulesListAppliedOnForm,
			Map<String, Object> componentFilledByUserByValues) {
		String failureMessages[] = null;
		if (rulesListAppliedOnForm.getRules() != null && !rulesListAppliedOnForm.getRules().isEmpty()) {
			int i = 0;
			failureMessages = new String[2];
			failureMessages[0] = null;
			failureMessages[1] = null;

			for (; i < rulesListAppliedOnForm.getRules().size();) {
				if (rulesListAppliedOnForm.getRules().get(i) instanceof ValidationRule) {
					failureMessages[0] = executeValidationRule((List<ValidationRule>) rulesListAppliedOnForm.getRules(),
							componentFilledByUserByValues);
				} else if (rulesListAppliedOnForm.getRules().get(i) instanceof TransactionRule) {
					failureMessages[1] = executeTransactionRule(
							(List<TransactionRule>) rulesListAppliedOnForm.getRules(), componentFilledByUserByValues);
				}
				i++;
			}
		}

		return failureMessages;
	}

	public String executeValidationRule(List<ValidationRule> ruleList,
			Map<String, Object> componentFilledByUserByValues) {
		boolean validationSuccess = false;

		String failureMessage = null;

		for (ValidationRule rule : ruleList) {
			List<ValidationCase> casesList = rule.getCases();
			for (ValidationCase caseObj : casesList) {

				if (!componentFilledByUserByValues.containsKey(caseObj.getFirstInput().getDbClassAttr())
						&& componentFilledByUserByValues.containsKey(caseObj.getSecondInput().getDbClassAttr())) {
					// Error : input left in-complete by user
					return "Data validation rule applied on form failed";
				}

				String operandDataType = ValidationOperations.checkInstanceTypeEquals(
						componentFilledByUserByValues.get(caseObj.getFirstInput().getDbClassAttr()),
						componentFilledByUserByValues.get(caseObj.getSecondInput().getDbClassAttr()));

				if (operandDataType == null) {
					// Error : data type does not match of the inputs
					return OPERAND_MISMATCH;
				}

				String validationType = caseObj.getValidationType().name();
				switch (validationType) {
				case "EQUALS":
					validationSuccess = ValidationOperations.ifEquals(caseObj.getFirstInput(), caseObj.getSecondInput(),
							componentFilledByUserByValues);
					break;
				case "GREATER_THAN":
					validationSuccess = ValidationOperations.ifGreaterThan(caseObj.getFirstInput(),
							caseObj.getSecondInput(), componentFilledByUserByValues, operandDataType);
					break;
				case "LESS_THAN":
					validationSuccess = ValidationOperations.ifLesserThan(caseObj.getFirstInput(),
							caseObj.getSecondInput(), componentFilledByUserByValues, operandDataType);
					break;
				default:
					validationSuccess = false;
					System.out.println("invalid validation rule");
					break;
				}

				if (!validationSuccess) {
					if (caseObj.getFailureMessage() != null && !caseObj.getFailureMessage().equalsIgnoreCase("")) {
						failureMessage = caseObj.getFailureMessage();
					} else {
						failureMessage = "Data validation rule applied on form failed";
					}
				}
			}

		}

		return failureMessage;
	}

	public String executeTransactionRule(List<TransactionRule> ruleList,
			Map<String, Object> componentFilledByUserByValues) {
		String defaultTransactionFailureMessage = "Data transactionn rule applied on form failed";
		String failureMessage = null;
		Number transactionSuccess;
		String resultantString;
		try {
			for (TransactionRule rule : ruleList) {
				List<TransactionCase> casesList = rule.getCases();
				for (TransactionCase caseObj : casesList) {

					if (!componentFilledByUserByValues.containsKey(caseObj.getFirstInput().getDbClassAttr())
							&& componentFilledByUserByValues.containsKey(caseObj.getSecondInput().getDbClassAttr())) {
						// Error : input left in-complete by user
						return defaultTransactionFailureMessage;
					}

					String operandDataType = TransactionOperations.checkInstanceTypeEquals(
							componentFilledByUserByValues.get(caseObj.getFirstInput().getDbClassAttr()),
							componentFilledByUserByValues.get(caseObj.getSecondInput().getDbClassAttr()));

					if (operandDataType == null) {
						// Error : data type does not match of the inputs
						return OPERAND_MISMATCH;
					}

					String validationType = caseObj.getValidationType().name();
					switch (validationType) {
					case "ADD":
						transactionSuccess = TransactionOperations.addOperands(componentFilledByUserByValues,
								operandDataType, caseObj.getFirstInput(), caseObj.getSecondInput());
						if (transactionSuccess != null) {
							componentFilledByUserByValues.put(caseObj.getAssignToAttribute().getDbClassAttr(),
									transactionSuccess);
						} else {
							if (caseObj.getFailureMessage() != null
									&& !caseObj.getFailureMessage().equalsIgnoreCase("")) {
								failureMessage = caseObj.getFailureMessage();
							} else {
								failureMessage = defaultTransactionFailureMessage;
							}
						}
						break;
					case "SUBTRACT":
						transactionSuccess = TransactionOperations.substractOperands(componentFilledByUserByValues,
								operandDataType, caseObj.getFirstInput(), caseObj.getSecondInput());
						if (transactionSuccess != null) {
							componentFilledByUserByValues.put(caseObj.getAssignToAttribute().getDbClassAttr(),
									transactionSuccess);
						} else {
							if (caseObj.getFailureMessage() != null
									&& !caseObj.getFailureMessage().equalsIgnoreCase("")) {
								failureMessage = caseObj.getFailureMessage();
							} else {
								failureMessage = defaultTransactionFailureMessage;
							}
						}

						break;
					case "CONCAT":
						resultantString = TransactionOperations.concatOperands(componentFilledByUserByValues,
								operandDataType, caseObj.getFirstInput(), caseObj.getSecondInput());
						if (resultantString != null) {
							componentFilledByUserByValues.put(caseObj.getAssignToAttribute().getDbClassAttr(),
									resultantString);
						} else {
							if (caseObj.getFailureMessage() != null
									&& !caseObj.getFailureMessage().equalsIgnoreCase("")) {
								failureMessage = caseObj.getFailureMessage();
							} else {
								failureMessage = defaultTransactionFailureMessage;
							}
						}

						break;
					default:
						failureMessage = defaultTransactionFailureMessage;
						System.out.println("invalid transaction rule");
						break;
					}
				}

			}
		} catch (Exception ex) {
			failureMessage = defaultTransactionFailureMessage;
		}

		return failureMessage;
	}

}

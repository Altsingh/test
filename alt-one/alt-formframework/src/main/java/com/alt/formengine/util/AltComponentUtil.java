package com.alt.formengine.util;

import java.util.List;
import java.util.stream.Collectors;

import com.alt.datacarrier.kernel.uiclass.DatatableRecord;

public class AltComponentUtil {
	
	public List<DatatableRecord> getActiveRecords(List<DatatableRecord> inputRecordList){
		List<DatatableRecord> recordList = inputRecordList
											.stream()
											.filter(record -> record.isActive())
											.collect(Collectors.toList());
		return recordList;
	}

}

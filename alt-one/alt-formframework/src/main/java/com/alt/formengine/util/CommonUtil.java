package com.alt.formengine.util;

import com.alt.datacarrier.formframework.request.FormUpdateRequest;

import java.util.regex.PatternSyntaxException;

public class CommonUtil {

    public static String getDbAttrValueInString(Object o) {
        return ((String) o).replaceAll("\"", "");
    }

    public static Long getDbAttrValueInLong(Object o) {
        return Long.valueOf(getDbAttrValueInString(o));
    }

    public static String generateVersion(FormUpdateRequest formUpdateRequest) {
        String version;
        version = formUpdateRequest.getVersionId().split("_")[0];
        try {
            Integer varsion_num = Integer.parseInt(formUpdateRequest.getVersionId().split("_")[1]);
            version = version + Integer.toString(varsion_num++);
        }
        catch (RuntimeException e){
            version = formUpdateRequest.getVersionId() + ".1";
        }
        return version;
    }
}

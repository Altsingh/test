package com.alt.formengine.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.xml.bind.DatatypeConverter;

import com.alt.datacarrier.common.enumeration.EnumPortalTypes;
import com.alt.datacarrier.core.Protocol;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

public class JwtUtil {
	static String secretKey = "alt_secret_key";

	public static Protocol validateJwt(String jwt) {

		Claims claims = null;
		String res1 = Base64.getEncoder().encodeToString(secretKey.getBytes(StandardCharsets.UTF_8));
		try {
			claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(res1)).parseClaimsJws(jwt)
					.getBody();
		} catch (ExpiredJwtException e) {
			throw new RuntimeException("Auth Token Expired");
		}
		return getProtocolFromClaims(claims);
	}

	private static Protocol getProtocolFromClaims(Claims claims) {

		Protocol protocol = new Protocol();
		protocol.setUserName(claims.get("userName", String.class));
		protocol.setUserCode(claims.get("userCode", String.class));
		protocol.setLoggedInTimestamp(claims.get("loggedInTimestamp", Long.class));
		protocol.setLoggedInUserIdentifier(claims.get("loggedInUserIdentifier", String.class));
		protocol.setOrgCode(claims.get("orgCode", String.class));
		protocol.setOrgId(claims.get("orgId", Integer.class));
		protocol.setTenantCode(claims.get("tenantCode", String.class));
		protocol.setSysFormCode(claims.get("sysFormCode", String.class));
		protocol.setPortalType(EnumPortalTypes.valueOf(claims.get("portalType", String.class)));
		protocol.setEmployeeId(claims.get("employeeId", Integer.class));
		protocol.setUserId(claims.get("userId", Integer.class));
		protocol.setTenantId(claims.get("tenantId", Integer.class));
		protocol.setAppCode(claims.get("appCode",String.class));
		return protocol;

	}

}

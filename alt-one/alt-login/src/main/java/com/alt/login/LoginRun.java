package com.alt.login;

import com.alt.login.config.ServerPropertiesConf;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = {"com.alt.login.*"})
public class LoginRun extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(LoginRun.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(LoginRun.class, args);
    }


	@Bean 
	public ServerPropertiesConf serverProperties(){
		return new ServerPropertiesConf();
	}
	
}

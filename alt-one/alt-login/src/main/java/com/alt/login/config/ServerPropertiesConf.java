package com.alt.login.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("server")
public class ServerPropertiesConf {
	
	private String loginAuthService;

	public String getLoginAuthService() {
		return loginAuthService;
	}

	public void setLoginAuthService(String loginAuthService) {
		this.loginAuthService = loginAuthService;
	}

	

}

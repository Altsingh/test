package com.alt.login.constants;


public class Constants {

    private Constants() {
        throw new IllegalStateException("Constants class");
    }

    public static final String RESPONSE_SUCCESS = "SUCCESS";
    public static final Integer RESPONSE_SUCCESS_CODE = 400;
    public static final String RESPONSE_FAIL = "FAIL";
    public static final Integer RESPONSE_FAIL_CODE = 200;
    public static final String ORGANIZATION_CLASS = "Organization";
    public static final String ORGANIZATION_URL = "OrganizationUrl";
    public static final String ORGANIZATION_CODE = "OrganizationCode";

}

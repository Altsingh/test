package com.alt.login.controller;

import com.alt.login.pojo.LoginAuthRequest;
import com.alt.login.pojo.LoginResponseDataTO;
import com.alt.login.service.LoginComponent;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/login")
public class LoginController {

    @Autowired(required = true)
    LoginComponent loginService;

    private static final Logger logger = Logger.getLogger(LoginController.class);

    @GetMapping("/test")
    public ResponseEntity<String> getTest() {
        return new ResponseEntity<>("Login Works", HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<LoginResponseDataTO> authorizeUser(@RequestBody LoginAuthRequest loginAuthRequest) throws IOException, EncoderException {
        loginAuthRequest.setUserNameHash(new String((new Base64()).encode(loginAuthRequest.getUserName().getBytes())));
        loginAuthRequest.setPasswordHash(new String((new Base64()).encode(loginAuthRequest.getPassword().getBytes())));
        return loginService.authorizeUser(loginAuthRequest);
    }


}

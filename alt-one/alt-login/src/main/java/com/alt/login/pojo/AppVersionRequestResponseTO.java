package com.alt.login.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

class AppVersionRequestResponseTO {
    @JsonProperty("forceVersion")
    public String forceVersion;

    @JsonProperty("operatingSytem")
    public String operatingSytem;

    @JsonProperty("unforceVersion")
    public String unforceVersion;

    public AppVersionRequestResponseTO(String forceVersion, String operatingSytem, String unforceVersion) {
        this.forceVersion = forceVersion;
        this.operatingSytem = operatingSytem;
        this.unforceVersion = unforceVersion;
    }

    public AppVersionRequestResponseTO() {
    }
}

package com.alt.login.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginAuthRequest {

    @JsonProperty("userName")
    private String userName;

    @JsonProperty("password")
    private String password;

    @JsonProperty("companyURL")
    private String companyURL;

    @JsonProperty("portalType")
    private String portalType;

    @JsonProperty("deviceName")
    public String deviceName;

    @JsonProperty("osName")
    public String osName;

    @JsonProperty("version")
    public String version;

    @JsonProperty("appVersion")
    public String appVersion;

    @JsonProperty("userNameHash")
    private String userNameHash;

    @JsonProperty("passwordHash")
    private String passwordHash;

    public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getCompanyURL() {
		return companyURL;
	}

	public String getPortalType() {
		return portalType;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public String getOsName() {
		return osName;
	}

	public String getVersion() {
		return version;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public String getUserNameHash() {
		return userNameHash;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setUserNameHash(String userNameHash) {
        this.userNameHash = userNameHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public LoginAuthRequest() {
    }

    private LoginAuthRequest(Builder builder) {
        userName = builder.userName;
        password = builder.password;
        companyURL = builder.companyURL;
        portalType = builder.portalType;
        deviceName = builder.deviceName;
        osName = builder.osName;
        version = builder.version;
        appVersion = builder.appVersion;
        userNameHash = builder.userNameHash;
        passwordHash = builder.passwordHash;
    }


    public static final class Builder {
        private String userName;
        private String password;
        private String companyURL;
        private String portalType;
        private String deviceName;
        private String osName;
        private String version;
        private String appVersion;
        private String userNameHash;
        private String passwordHash;

        public Builder() {
        }

        public Builder withUserName(String val) {
            userName = val;
            return this;
        }

        public Builder withPassword(String val) {
            password = val;
            return this;
        }

        public Builder withCompanyURL(String val) {
            companyURL = val;
            return this;
        }

        public Builder withPortalType(String val) {
            portalType = val;
            return this;
        }

        public Builder withDeviceName(String val) {
            deviceName = val;
            return this;
        }

        public Builder withOsName(String val) {
            osName = val;
            return this;
        }

        public Builder withVersion(String val) {
            version = val;
            return this;
        }

        public Builder withAppVersion(String val) {
            appVersion = val;
            return this;
        }

        public Builder withUserNameHash(String val) {
            userNameHash = val;
            return this;
        }

        public Builder withPasswordHash(String val) {
            passwordHash = val;
            return this;
        }

        public LoginAuthRequest build() {
            return new LoginAuthRequest(this);
        }
    }
}

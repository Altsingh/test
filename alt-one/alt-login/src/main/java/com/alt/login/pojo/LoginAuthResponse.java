package com.alt.login.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class LoginAuthResponse {

    @JsonProperty("messageCode")
    private MessageCodeTO messageCode;

    @JsonProperty("responseData")
    private List<LoginResponseDataTO> responseData;

    @JsonProperty("response")
    private String response;

    @JsonProperty("responseMap")
    private String responseMap;

    public LoginAuthResponse() {
    }

    public LoginAuthResponse(MessageCodeTO messageCode, List<LoginResponseDataTO> responseData) {
        this.messageCode = messageCode;
        this.responseData = responseData;
    }

    public LoginAuthResponse(MessageCodeTO messageCode, List<LoginResponseDataTO> responseData, String response, String responseMap) {
        this.messageCode = messageCode;
        this.responseData = responseData;
        this.response = response;
        this.responseMap = responseMap;
    }

    public MessageCodeTO getMessageCode() {
        return messageCode;
    }

    public List<LoginResponseDataTO> getResponseData() {
        return responseData;
    }
}



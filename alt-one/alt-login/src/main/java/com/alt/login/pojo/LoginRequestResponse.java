package com.alt.login.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginRequestResponse {

    @JsonProperty("input")
    private LoginAuthRequest input;

    @JsonProperty("output")
    private LoginAuthResponse output;

    public LoginRequestResponse(LoginAuthRequest input, LoginAuthResponse output) {
        this.input = input;
        this.output = output;
    }
}

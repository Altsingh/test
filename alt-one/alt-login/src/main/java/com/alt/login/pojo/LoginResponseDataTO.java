package com.alt.login.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class LoginResponseDataTO {

    @JsonProperty("userId")
    private Long userId;

    @JsonProperty("userName")
    private String userName;

    @JsonProperty("email")
    private String email;

    @JsonProperty("profileImage")
    private String profileImage;

    @JsonProperty("profileName")
    private String profileName;

    @JsonProperty("grade")
    private String grade;

    @JsonProperty("joiningDate")
    private String joiningDate;

    @JsonProperty("organizationid")
    private String organizationId;

    @JsonProperty("employeeCode")
    private String employeeCode;

    @JsonProperty("roleList")
    private List<Integer> roleList;

    @JsonProperty("authToken")
    private String authToken;

    @JsonProperty("isFirstLogin")
    private String isFirstLogin;

    @JsonProperty("appVersionInfo")
    AppVersionRequestResponseTO appVersionRequestResponseTO;

    @JsonProperty("passwordPolicy")
    public PasswordPolicyInfoTO passwordPolicyTO;

    @JsonProperty("messengerEnabled")
    private Boolean messengerEnabled;

    @JsonProperty("mobileSessionTimeout")
    private Integer mobileSessionTimeout;

    @JsonProperty("jwt")
    private String jwt;

    @JsonProperty("isAdmin")
    private Boolean isAdmin;

    public LoginResponseDataTO() {
    }

    private LoginResponseDataTO(Builder builder) {
        userId = builder.userId;
        userName = builder.userName;
        email = builder.email;
        profileImage = builder.profileImage;
        profileName = builder.profileName;
        grade = builder.grade;
        joiningDate = builder.joiningDate;
        organizationId = builder.organizationId;
        employeeCode = builder.employeeCode;
        roleList = builder.roleList;
        authToken = builder.authToken;
        isFirstLogin = builder.isFirstLogin;
        appVersionRequestResponseTO = builder.appVersionRequestResponseTO;
        passwordPolicyTO = builder.passwordPolicyTO;
        messengerEnabled = builder.messengerEnabled;
        mobileSessionTimeout = builder.mobileSessionTimeout;
        jwt = builder.jwt;
        isAdmin = builder.isAdmin;
    }

    public Long getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getProfileName() {
        return profileName;
    }

    public String getGrade() {
        return grade;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public List<Integer> getRoleList() {
        return roleList;
    }

    public String getAuthToken() {
        return authToken;
    }

    public String getIsFirstLogin() {
        return isFirstLogin;
    }

    public AppVersionRequestResponseTO getAppVersionRequestResponseTO() {
        return appVersionRequestResponseTO;
    }

    public PasswordPolicyInfoTO getPasswordPolicyTO() {
        return passwordPolicyTO;
    }

    public Boolean getMessengerEnabled() {
        return messengerEnabled;
    }

    public Integer getMobileSessionTimeout() {
        return mobileSessionTimeout;
    }

    public String getJwt() {
        return jwt;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }


    public static final class Builder {
        private Long userId;
        private String userName;
        private String email;
        private String profileImage;
        private String profileName;
        private String grade;
        private String joiningDate;
        private String organizationId;
        private String employeeCode;
        private List<Integer> roleList;
        private String authToken;
        private String isFirstLogin;
        private AppVersionRequestResponseTO appVersionRequestResponseTO;
        private PasswordPolicyInfoTO passwordPolicyTO;
        private Boolean messengerEnabled;
        private Integer mobileSessionTimeout;
        private String jwt;
        private Boolean isAdmin;

        public Builder() {
        }

        public Builder withUserId(Long val) {
            userId = val;
            return this;
        }

        public Builder withUserName(String val) {
            userName = val;
            return this;
        }

        public Builder withEmail(String val) {
            email = val;
            return this;
        }

        public Builder withProfileImage(String val) {
            profileImage = val;
            return this;
        }

        public Builder withProfileName(String val) {
            profileName = val;
            return this;
        }

        public Builder withGrade(String val) {
            grade = val;
            return this;
        }

        public Builder withJoiningDate(String val) {
            joiningDate = val;
            return this;
        }

        public Builder withOrganizationId(String val) {
            organizationId = val;
            return this;
        }

        public Builder withEmployeeCode(String val) {
            employeeCode = val;
            return this;
        }

        public Builder withRoleList(List<Integer> val) {
            roleList = val;
            return this;
        }

        public Builder withAuthToken(String val) {
            authToken = val;
            return this;
        }

        public Builder withIsFirstLogin(String val) {
            isFirstLogin = val;
            return this;
        }

        public Builder withAppVersionRequestResponseTO(AppVersionRequestResponseTO val) {
            appVersionRequestResponseTO = val;
            return this;
        }

        public Builder withPasswordPolicyTO(PasswordPolicyInfoTO val) {
            passwordPolicyTO = val;
            return this;
        }

        public Builder withMessengerEnabled(Boolean val) {
            messengerEnabled = val;
            return this;
        }

        public Builder withMobileSessionTimeout(Integer val) {
            mobileSessionTimeout = val;
            return this;
        }

        public Builder withJwt(String val) {
            jwt = val;
            return this;
        }

        public Builder withIsAdmin(Boolean val) {
            isAdmin = val;
            return this;
        }

        public LoginResponseDataTO build() {
            return new LoginResponseDataTO(this);
        }
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }
}

package com.alt.login.pojo;

public class MessageCodeTO {
    private String code;

    private String message;

    private String description;

    private String validationKeys;

    public MessageCodeTO() {
    }

    private MessageCodeTO(Builder builder) {
        code = builder.code;
        message = builder.message;
        description = builder.description;
        validationKeys = builder.validationKeys;
    }

    public static final class Builder {
        private String code;
        private String message;
        private String description;
        private String validationKeys;

        public Builder() {
        }

        public Builder withCode(String val) {
            code = val;
            return this;
        }

        public Builder withMessage(String val) {
            message = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withValidationKeys(String val) {
            validationKeys = val;
            return this;
        }

        public MessageCodeTO build() {
            return new MessageCodeTO(this);
        }
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDescription() {
        return description;
    }

    public String getValidationKeys() {
        return validationKeys;
    }
}

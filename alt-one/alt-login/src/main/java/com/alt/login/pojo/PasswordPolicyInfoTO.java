package com.alt.login.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

class PasswordPolicyInfoTO {

    private static final long serialVersionUID = 81230666112321616L;

    @JsonProperty("minimumCharacters")
    private String minimumCharacters;
    @JsonProperty("resetPasswordOnFirstLogin")
    private boolean resetPasswordOnFirstLogin;
    @JsonProperty("forcePasswordChangeFrequency")
    private String forcePasswordChangeFrequency;
    @JsonProperty("noOfAttempts")
    private String noOfAttempts;
    @JsonProperty("showCapchaAfter")
    private String showCapchaAfter;
    @JsonProperty("allowRepeatOldPasswordAfter")
    private String allowRepeatOldPasswordAfter;
    @JsonProperty("passwordPolicyList")
    private List<String> passwordPolicyList;
    @JsonProperty("blockingDuration")
    private String blockingDuration;

    public PasswordPolicyInfoTO() {
    }

    private PasswordPolicyInfoTO(Builder builder) {
        minimumCharacters = builder.minimumCharacters;
        resetPasswordOnFirstLogin = builder.resetPasswordOnFirstLogin;
        forcePasswordChangeFrequency = builder.forcePasswordChangeFrequency;
        noOfAttempts = builder.noOfAttempts;
        showCapchaAfter = builder.showCapchaAfter;
        allowRepeatOldPasswordAfter = builder.allowRepeatOldPasswordAfter;
        passwordPolicyList = builder.passwordPolicyList;
        blockingDuration = builder.blockingDuration;
    }

    public static final class Builder {
        private String minimumCharacters;
        private boolean resetPasswordOnFirstLogin;
        private String forcePasswordChangeFrequency;
        private String noOfAttempts;
        private String showCapchaAfter;
        private String allowRepeatOldPasswordAfter;
        private List<String> passwordPolicyList;
        private String blockingDuration;

        public Builder() {
        }

        public Builder withMinimumCharacters(String val) {
            minimumCharacters = val;
            return this;
        }

        public Builder withResetPasswordOnFirstLogin(boolean val) {
            resetPasswordOnFirstLogin = val;
            return this;
        }

        public Builder withForcePasswordChangeFrequency(String val) {
            forcePasswordChangeFrequency = val;
            return this;
        }

        public Builder withNoOfAttempts(String val) {
            noOfAttempts = val;
            return this;
        }

        public Builder withShowCapchaAfter(String val) {
            showCapchaAfter = val;
            return this;
        }

        public Builder withAllowRepeatOldPasswordAfter(String val) {
            allowRepeatOldPasswordAfter = val;
            return this;
        }

        public Builder withPasswordPolicyList(List<String> val) {
            passwordPolicyList = val;
            return this;
        }

        public Builder withBlockingDuration(String val) {
            blockingDuration = val;
            return this;
        }

        public PasswordPolicyInfoTO build() {
            return new PasswordPolicyInfoTO(this);
        }
    }
}

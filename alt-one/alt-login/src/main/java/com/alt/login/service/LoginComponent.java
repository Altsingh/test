package com.alt.login.service;

import com.alt.core.util.JwtUtil;
import com.alt.datacarrier.core.Protocol;
import com.alt.login.config.ServerPropertiesConf;
import com.alt.login.pojo.LoginAuthRequest;
import com.alt.login.pojo.LoginAuthResponse;
import com.alt.login.pojo.LoginRequestResponse;
import com.alt.login.pojo.LoginResponseDataTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.EncoderException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;

@Service
public class LoginComponent {

	@Autowired
    ServerPropertiesConf serverProperties;

    JwtUtil jwtUtil = new JwtUtil();

    public ResponseEntity<LoginResponseDataTO> authorizeUser(LoginAuthRequest loginAuthRequest) throws EncoderException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        LoginRequestResponse loginRequestResponse = new LoginRequestResponse(loginAuthRequest, null);
        HttpResponse rawResponse = getData(serverProperties.getLoginAuthService(), loginRequestResponse);
        LoginAuthResponse dataResponse = new LoginAuthResponse();
        try {
            dataResponse = mapper.readValue(rawResponse.getEntity().getContent(), LoginAuthResponse.class);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (dataResponse.getMessageCode().getCode().equals("EC200")) { //TODO: pick codes from a property file
            LoginResponseDataTO loginResponseDataTO = dataResponse.getResponseData().get(0);
            Protocol protocol = new Protocol();
            Timestamp time = new Timestamp(System.currentTimeMillis());
            protocol.setLoggedInTimestamp(time.getTime());
            protocol.setOrgCode(loginResponseDataTO.getOrganizationId());
            protocol.setUserName(loginResponseDataTO.getUserName());
            //protocol.setUserCode(String.valueOf(loginResponseDataTO.getUserId()));
            
            String jwt = jwtUtil.generateJwt(protocol);
            loginResponseDataTO.setJwt(jwt);
            if(loginAuthRequest.getUserName().equals("lavina.sanghvi@peoplestrong.com")){ //TODO: remove hard coded user name  
                loginResponseDataTO.setAdmin(true);
            }else{
                loginResponseDataTO.setAdmin(false);
            }
            return new ResponseEntity<LoginResponseDataTO>(loginResponseDataTO, HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    public HttpResponse getData(String url, LoginRequestResponse loginAuthRequest) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        HttpClient client = HttpClientBuilder.create().build();
        StringEntity requestEntity = new StringEntity(mapper.writeValueAsString(loginAuthRequest), 
        		ContentType.APPLICATION_JSON);
        HttpPost postMethod = new HttpPost(url);
        postMethod.setHeader("Content-Type", "application/json");
        postMethod.setEntity(requestEntity);
        HttpResponse rawResponse = client.execute(postMethod);
        return rawResponse;
    }
    
}

package com.alt.marketplace.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("server")
public class ServerPropertiesConf {
	
	private String fromEmailId;
	
	private String emailPassword;
	
	private String toEmailIds;
	
	private String ccEmailIds;
	
	private String bccEmailIds;
	
	private String writeImageServerPath;
	
	private String readImageServerPath;
	
	public String getFromEmailId() {
		return fromEmailId;
	}

	public void setFromEmailId(String fromEmailId) {
		this.fromEmailId = fromEmailId;
	}

	public String getEmailPassword() {
		return emailPassword;
	}

	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}

	public String getWriteImageServerPath() {
		return writeImageServerPath;
	}

	public void setWriteImageServerPath(String writeImageServerPath) {
		this.writeImageServerPath = writeImageServerPath;
	}

	public String getReadImageServerPath() {
		return readImageServerPath;
	}

	public void setReadImageServerPath(String readImageServerPath) {
		this.readImageServerPath = readImageServerPath;
	}

	public String getToEmailIds() {
		return toEmailIds;
	}

	public void setToEmailIds(String toEmailIds) {
		this.toEmailIds = toEmailIds;
	}

	public String getCcEmailIds() {
		return ccEmailIds;
	}

	public void setCcEmailIds(String ccEmailIds) {
		this.ccEmailIds = ccEmailIds;
	}

	public String getBccEmailIds() {
		return bccEmailIds;
	}

	public void setBccEmailIds(String bccEmailIds) {
		this.bccEmailIds = bccEmailIds;
	}

	
}

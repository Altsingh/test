package com.alt.marketplace.controller;

import com.alt.datacarrier.marketplace.core.BannerEntity;
import com.alt.marketplace.service.BannerComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/v1/marketplace/banners")
public class BannerResource {

    @Autowired
    BannerComponent bannerComponent;

    @GetMapping
    public ResponseEntity<List<BannerEntity>> getBanners() {
        return bannerComponent.getBanners();
    }

    @PostMapping
    public ResponseEntity<BannerEntity> createBanner(@RequestBody BannerEntity request) {
        return bannerComponent.createBanner(request);
    }

    @PostMapping("/uploadImage")
    public ResponseEntity<String> uploadImages(
            @RequestParam("uploadingFiles") MultipartFile multipartFile) {
        return bannerComponent.uploadImageForBanner(multipartFile);
    }

    
    @PutMapping("/updateSequence")
    public ResponseEntity<List<BannerEntity>> updateBannerSequence(@RequestBody List<BannerEntity> request){
    	    	 
    	return bannerComponent.updateBannerSequence(request);
    }

    @PutMapping
    public ResponseEntity<BannerEntity> updateBanner(@RequestBody BannerEntity request){
        return bannerComponent.updateBanner(request);
    }

}

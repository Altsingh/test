package com.alt.marketplace.controller;

import com.alt.datacarrier.marketplace.core.Category;
import com.alt.datacarrier.marketplace.core.SubCategory;
import com.alt.marketplace.service.CategoryComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/marketplace/categories")
public class CategoryResource {

    @Autowired
    CategoryComponent categoryComponent;

    @GetMapping
    public ResponseEntity<List<Category>> getCategories(){
        return categoryComponent.getCategories();
    }

    @PostMapping
    public ResponseEntity<Category> createCategory(@RequestBody Category category){
        return categoryComponent.createCategory(category);
    }

    @GetMapping("/{categoryId}/subcategories")
    public  ResponseEntity<List<SubCategory>> getSubCategories(@PathVariable("categoryId") String categoryId){
        return new ResponseEntity<>(categoryComponent.getSubcategories(categoryId),HttpStatus.OK);
    }

    @PutMapping("/sequence")
    public ResponseEntity<String> updateCategorySequence(){
        return new ResponseEntity<>("sequence updated successfully", HttpStatus.OK);
    }

    @PutMapping("/{categoryId}/subcategories/sequence")
    public ResponseEntity<String> updateSubCategorySequence(@PathVariable("categoryId") String categoryId,
                                                            @RequestBody List<SubCategory> subCategories){
        return new ResponseEntity<>(categoryComponent.updateSubCategorySequence(subCategories), HttpStatus.OK);
    }
}

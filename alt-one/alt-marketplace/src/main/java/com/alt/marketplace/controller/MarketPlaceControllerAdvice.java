package com.alt.marketplace.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.alt.datacarrier.core.ExceptionResponse;
import com.alt.datacarrier.exception.AuthorizationException;
import com.alt.datacarrier.exception.MarketPlaceException;

@ControllerAdvice
public class MarketPlaceControllerAdvice {
	
		@ExceptionHandler(AuthorizationException.class)
		private ResponseEntity<ExceptionResponse> error(AuthorizationException exception) {

			return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(exception.getMessage(),HttpStatus.UNAUTHORIZED.toString()),HttpStatus.UNAUTHORIZED);
		}
		
		@ExceptionHandler(MarketPlaceException.class)
		private ResponseEntity<ExceptionResponse> error(MarketPlaceException exception) {

			return new ResponseEntity<ExceptionResponse>(new ExceptionResponse(exception.getMessage(),HttpStatus.UNPROCESSABLE_ENTITY.toString()),HttpStatus.UNPROCESSABLE_ENTITY);
		}
}

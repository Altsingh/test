package com.alt.marketplace.controller;

import com.alt.datacarrier.marketplace.core.CategoryAppDetail;
import com.alt.datacarrier.marketplace.core.MarketplaceAppDetail;
import com.alt.datacarrier.marketplace.core.SubCategoryAppDetail;
import com.alt.datacarrier.marketplace.request.ContactUsDetails;
import com.alt.datacarrier.marketplace.request.UserEnquiryDetails;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.marketplace.service.MarketPlaceEnquiryComponent;
import com.alt.marketplace.service.MarketplaceAppComponent;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

@RestController
@RequestMapping("/api/v1/marketplaceapps")
public class MarketplaceAppResource {

    private static final Logger logger = Logger.getLogger(MarketplaceAppResource.class);

    @Autowired
    MarketplaceAppComponent appComponent;

    @Autowired
    MarketPlaceEnquiryComponent userEnquiryComponent;

    @GetMapping("/test")
    public ResponseEntity<String> getTest() {
        return new ResponseEntity<>("Hello World", HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<CategoryAppDetail>> getApps() {

        List<CategoryAppDetail> apps;
        try {
            apps = appComponent.getApps();
        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Occurred in MarketplaceAppResource in method getApps ", e);
            return new ResponseEntity<List<CategoryAppDetail>>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<List<CategoryAppDetail>>(apps, HttpStatus.OK);
    }

    @PostMapping("/enquiry")
    public ResponseEntity<String> userEnquiry(@RequestBody UserEnquiryDetails userEnquiryDetails) {

        return userEnquiryComponent.saveUserEnquiry(userEnquiryDetails);

    }

    @PostMapping("/contactUs")
    public ResponseEntity<String> contactUs(@RequestBody ContactUsDetails contactUsDetails) {

        return userEnquiryComponent.saveContactUs(contactUsDetails);

    }

    @GetMapping("/{categoryId}")
    public ResponseEntity<CategoryAppDetail> getAppsForCategory(@PathVariable("categoryId") String categoryId) {

        CategoryAppDetail apps = null;
        try {
            apps = appComponent.getAppsForCategory(categoryId);
        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Occurred in MarketplaceAppResource in method getAppsForCategory ", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(apps, HttpStatus.OK);
    }

    @GetMapping("/subcategory/{categoryId}/{subCategoryId}")
    public ResponseEntity<SubCategoryAppDetail> getAppsForSubCategory(@PathVariable("categoryId") String categoryId,
                                                                      @PathVariable("subCategoryId") String subCategoryId) {

        SubCategoryAppDetail apps = null;
        try {
            apps = appComponent.getAppsForSubCategory(categoryId, subCategoryId);
        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Occurred in MarketplaceAppResource in method getAppsForSubCategory ", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(apps, HttpStatus.OK);
    }


    @GetMapping("/app/{appId}")
    public ResponseEntity<MarketplaceAppDetail> getAppDetailsFromId(@PathVariable("appId") Integer appId) {

        MarketplaceAppDetail app = null;
        try {
            app = appComponent.getAppDetailsFromId(appId);
        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Occurred in MarketplaceAppResource in method getAppForId ", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(app, HttpStatus.OK);
    }
    
    @PostMapping
    public ResponseEntity<MarketplaceAppDetail> createApp(@RequestBody MarketplaceAppDetail request) throws QueryRelatedException{
    	
    	return appComponent.createApp(request);     		
    	
    }
    
    @PutMapping("/sequence")
    public ResponseEntity<String> updateBannerSequence(@RequestBody List<MarketplaceAppDetail> request){
    	    	 
    	return appComponent.updateAppSequence(request);
    }
}

package com.alt.marketplace.controller;

import com.alt.datacarrier.marketplace.core.MarketplaceAppDetail;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.marketplace.service.MarketplaceAppComponent;
import com.alt.marketplace.service.PartnerAppComponent;
import com.alt.marketplace.util.JwtUtil;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/v1/marketplace/partner/apps")
public class MarketplacePartnerAppResource {

    private static final Logger logger = Logger.getLogger(MarketplaceAppResource.class);

    @Autowired(required = true)
    PartnerAppComponent partnerAppComponent;
    
    @Autowired(required = true)
    MarketplaceAppComponent appComponent;
    
    @PostMapping
    public ResponseEntity<MarketplaceAppDetail> createApp(@RequestBody MarketplaceAppDetail request) throws QueryRelatedException{
    	
    	return appComponent.createApp(request);
    }

    @GetMapping
    public ResponseEntity<List<MarketplaceAppDetail>> getApps(@RequestHeader(value = "authToken") String authToken) {

        List<MarketplaceAppDetail> apps = null;
        try {
            apps = partnerAppComponent.getApps(JwtUtil.validateJwt(authToken));
        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Occurred in MarketplacePartnerAppResource in method getApps ", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<List<MarketplaceAppDetail>>(apps, HttpStatus.OK);
    } 
    
    @PutMapping()
    public ResponseEntity<String> editAppDetails(@RequestBody MarketplaceAppDetail request){
    	    	 
    	return appComponent.editAppDetails(request);
    }
    
    @PostMapping("/uploadImage/{appId}/{imageField}")
    public ResponseEntity<String> uploadImages(@PathVariable("appId")Integer appId,
    		@PathVariable("imageField")String imageField,@RequestParam("uploadingFiles") MultipartFile multipartFile){
        return appComponent.uploadImageForApp(appId,imageField,multipartFile);
    }
    
}

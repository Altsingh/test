package com.alt.marketplace.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/mocks")
public class MockResource {

    @GetMapping("/userapps")
    public String getUserApps(){
        return  "[\n" +
                "    {\n" +
                "    \"category\":{\n" +
                "          \"categoryName\":\"Web Apps\",\n" +
                "          \"categoryId\":\"1\"\n" +
                "        },\n" +
                "    \"apps\":[\n" +
                "        {\n" +
                "        \"url\":\"https://worklife.peoplestrong.com/altone/altonelogin.jsf?token=3DD48DEC4526476C9FC53003CD4308FA658507D7B575582FFBA1DD21732CDB92F64ECEF2F46B95410ACCECE3ACF7BD56\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/partner/1521226/155/1516255926216_appLogo.png\",\n" +
                "        \"name\":\"Worklife\",\n" +
                "        \"appId\":\"1\"\n" +
                "        },{\n" +
                "        \"url\":\"https://recruitment.peoplestrong.com/beta/home?url=https://recruitment.peoplestrong.com&session=eyJhbGciOiJBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0.NR4HurNcjCoewVXBpOZcTE4qODjhGM0D4G1xiAHBjOxJ_wRt-Mrocw.pKrRxt2vME2No4v7BCbLWw.uLNzbMjk1_I6wX0MvQpnkL5EGj6rA9Ea8Q2Q8i_pjoLk9jlC8lqWcg919-c8Cd17tAU-IA5vBvDYZvZWF-rn4E4C0E6up_Zapx6sAXJSi2f0wMjG5vxu9hhgqPwCotILyEeWXIYoTDFx5mdNDSuCc7xR_42e-BxUI29BlokJje9zvt7jKBdK53cn2dx1F1B1TTDFcGjkPZtnFsa3KFrHNw.TcaXfWH1w63I2ZVA0Vkz7g\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/partner/1521226/129/1516099946350_appLogo.png\",\n" +
                "        \"name\":\"Recruit\",\n" +
                "        \"appId\":\"2\"\n" +
                "        },{\n" +
                "        \"url\":\"https://altmessenger.peoplestrong.com/messenger/\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/partner/1521226/156/1516257649734_appLogo.png\",\n" +
                "        \"name\":\"Messenger\",\n" +
                "        \"appId\":\"3\"\n" +
                "        },{\n" +
                "        \"url\":\"https://peoplestrongsandbox.capabiliti.co/auth/peoplestrong/E9B7F4A7275284838AF49A56FFEBE028306EC4B26BCD751E99F78568760B6C896FFF3B6CDAC9DB92BFE2E230C99CD5B6\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/partner/1521226/137/1516169216814_appLogo.png\",\n" +
                "        \"name\":\"Learning\",\n" +
                "        \"appId\":\"4\"\n" +
                "        }\n" +
                "        ]\n" +
                "    },\n" +
                "    {\n" +
                "    \"category\":{\n" +
                "          \"categoryName\":\"Partner Apps\",\n" +
                "          \"categoryId\":\"2\"\n" +
                "        },\n" +
                "    \"apps\":[\n" +
                "        {\n" +
                "        \"url\":\"http://payreview.work?user_email=testing%2Bdata@payreview.work&user_token=SHWyEzt_tXsvKFecV4PR\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/images/partnericon/payreview.png\",\n" +
                "        \"name\":\"Pay Review\",\n" +
                "        \"appId\":\"5\"\n" +
                "        },{\n" +
                "        \"url\":\"https://peoplestrong-demo.synergita.com/Account/DemologON?userName=priyanka@psdemo.com&password=company1\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/images/partnericon/synergita.png\",\n" +
                "        \"name\":\"Synergita\",\n" +
                "        \"appId\":\"6\"\n" +            
           
                "        },{\n" +
                "        \"url\":\"https://secure.yatra.com/social/custom/crp/login.htm?returnUrl=https%3A%2F%2Fwww.yatra.com%2Ffresco%2Fcorporate%2Fhome?unique=0082063115&channel=crp\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/partner/1521226/136/1516097724913_appLogo.png\",\n" +
                "        \"name\":\"Yatra\",\n" +
                "        \"appId\":\"7\"\n" +
                "        },{\n" +
                "        \"url\":\"https://www.reckrut.com/saas-autologin/q0OyjNOQnaegrJ6DuMdpd6xyf3hjfGWepK9ys6egqnmghqAVINASHPIYUSHT29PW4AVINASHPIYUSHDj4MiR15OSgK4%3D\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/images/partnericon/Reckrut.png\",\n" +
                "        \"name\":\"Reckrut\",\n" +
                "        \"appId\":\"8\"\n" +
                
                "        },{\n" +
                "        \"url\":\"https://www.talview.com/video-interviewing/\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/images/partnericon/talview.png\",\n" +
                "        \"name\":\"Talview\",\n" +
                "        \"appId\":\"9\"\n" +
                "        },{\n" +
                "        \"url\":\"https://enterprise.xoxoday.com/home/auth/login?EmailAddress=dml2ZWsuc2V0aGlAcGVvcGxlc3Ryb25nLmNvbQ==&CompanyID=lfzp44blwebmocuzvlpakeg5lz5tjstx&Hash=13e56b12a28dce8faee41ceeadbe9917\",\n" +
                "        \"appLogo\":\"https://image.peoplestrong.com/marketplace/images/partnericon/XOXO.png\",\n" +
                "        \"name\":\"XOXODay\",\n" +
                "        \"appId\":\"10\"\n" +
                "        }\n" +
                "        ]\n" +
                "    }\n" +
                "        ]";
    }
}

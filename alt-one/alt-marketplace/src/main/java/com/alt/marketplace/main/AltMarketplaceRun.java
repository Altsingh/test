package com.alt.marketplace.main;

import com.alt.datakernel.DataKernelConfig;
import com.alt.datakernel.dao.IClassDS;
import com.alt.datakernel.dao.IFormDS;
import com.alt.datakernel.dao.IInstanceDS;
import com.alt.datakernel.dao.IQueryDS;
import com.alt.datakernel.dao.impl.AltClassDS;
import com.alt.datakernel.dao.impl.AltFormDS;
import com.alt.datakernel.dao.impl.InstanceDS;
import com.alt.datakernel.dao.impl.QueryDS;
import com.alt.datakernel.service.impl.InstanceService;
import com.alt.marketplace.config.ServerPropertiesConf;
import com.alt.marketplace.controller.MarketplaceAppResource;
import com.alt.marketplace.service.BannerComponent;
import com.alt.marketplace.service.CategoryComponent;
import com.alt.marketplace.service.MarketPlaceEnquiryComponent;
import com.alt.marketplace.service.MarketplaceAppComponent;
import com.alt.marketplace.service.common.CreateCategoryRequests;
import com.alt.marketplace.service.common.CreateRequestBanner;
import com.alt.marketplace.util.FileWriterUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages = { "com.alt.marketplace.*" })
@Import(value = { DataKernelConfig.class })
@EntityScan(
        basePackageClasses = {com.alt.datakernel.model.AltFormData.class,
        		com.alt.datakernel.model.AltClassData.class,
        		com.alt.datakernel.model.AltAttributeData.class,
        		com.alt.datakernel.model.AltReferenceData.class,
        		com.alt.datakernel.model.Components.class,
        		com.alt.datakernel.model.Attributes.class,
        		com.alt.datakernel.model.Instance.class}
)
public class AltMarketplaceRun extends SpringBootServletInitializer{
	
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AltMarketplaceRun.class);
    }

	public static void main(String[] args) throws Exception {
		SpringApplication.run(AltMarketplaceRun.class, args);
	}

	@Bean
	public CreateRequestBanner createReadRequestBanner(){ return  new CreateRequestBanner();}

	@Bean
	public BannerComponent bannerComponent(){ return  new BannerComponent();}

	@Bean
	public CategoryComponent categoryComponent(){
    	return  new CategoryComponent();
	}

	@Bean
	public CreateCategoryRequests createReadRequest(){
		return  new CreateCategoryRequests();
	}

	@Bean
	public MarketplaceAppComponent marketplaceAppComponent() {
		return new MarketplaceAppComponent();
	}

	@Bean
	public MarketplaceAppResource marketplaceAppResource() {
		return new MarketplaceAppResource();
	}
	
	@Bean
	public InstanceService instanceService() {
		return new InstanceService();
	}

	@Bean
	public IInstanceDS iInstanceDS() {
		return new InstanceDS();
	}

	@Bean
	public IClassDS iClassDS() {
		return new AltClassDS();
	}
	
	@Bean
	public IQueryDS iQueryDS() {
		return new QueryDS();
	}
	
	@Bean
	public IFormDS iFormDS() {
		return new AltFormDS();
	}
	
	@Bean
	public MarketPlaceEnquiryComponent userEnquiryComponent() {
		return new MarketPlaceEnquiryComponent();
	}
	
	@Bean 
	public FileWriterUtil fileWriterUtil(){
		return new FileWriterUtil();
	}
	
	@Bean 
	public ServerPropertiesConf serverProperties(){
		return new ServerPropertiesConf();
	}
	
}

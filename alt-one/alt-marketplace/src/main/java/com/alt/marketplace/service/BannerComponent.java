package com.alt.marketplace.service;

import com.alt.datacarrier.core.Response;
import com.alt.datacarrier.exception.MarketPlaceException;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.db.core.DynamicReadRequest;
import com.alt.datacarrier.kernel.db.core.DynamicReadResponse;
import com.alt.datacarrier.kernel.db.core.InstanceData;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.marketplace.core.BannerEntity;
import com.alt.datacarrier.marketplace.enums.EnumBannerStatus;
import com.alt.datacarrier.marketplace.enums.EnumBannerType;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.datakernel.service.IInstanceService;
import com.alt.marketplace.config.ServerPropertiesConf;
import com.alt.marketplace.service.common.CreateRequestBanner;
import com.alt.marketplace.util.FileWriterUtil;
import com.alt.marketplace.util.MarketPlaceUtil;
import com.alt.marketplace.util.Util;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class BannerComponent {

    @Autowired(required = true)
    IInstanceService instanceService;
    @Autowired
    CreateRequestBanner createRequestBanner;

    @Autowired
    IDynamicQuery dynamicQuery;

    @Autowired
    FileWriterUtil fileWriterUtil;

    @Autowired
    ServerPropertiesConf serverProperties;

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BannerComponent.class);

    public ResponseEntity<List<BannerEntity>> getBanners() {

        List<BannerEntity> bannerEntityList = new ArrayList<>();
        try {
            DynamicReadResponse response = new DynamicReadResponse();
            DynamicReadRequest readRequest = new DynamicReadRequest();
            createRequestBanner.createReadRequest(readRequest);
            response = dynamicQuery.read(readRequest);
            if (response.getCode() == 200) {
                List<Object[]> objectList = new ArrayList<>();
                objectList = response.getResponse();
                reduntantWork(objectList);
                for (Object[] objects : objectList) {
                    BannerEntity bannerEntity = new BannerEntity.Builder().withBannerId((Integer) objects[6])
                            .withImageBanner((String) objects[0]).withImageThumbnail((String) objects[1])
                            .withType(EnumBannerType.valueOf((String) objects[2]))
                            .withStatus(EnumBannerStatus.valueOf((String) objects[3])).withLink((String) objects[4])
                            .withSequence(Integer.parseInt((String) objects[5])).build();
                    bannerEntityList.add(bannerEntity);
                }
            }
        } catch (QueryRelatedException e) {
            logger.log(Level.FATAL, "Error Occurred in BannerComponent in method getBanners ", e);
        }
        return new ResponseEntity<>(bannerEntityList, HttpStatus.OK);
    }

    public BannerEntity getBannerFromId(Integer bannerId) throws InstanceRelatedException {
        InstanceData data = instanceService.read(MarketPlaceUtil.getMarketPlaceProtocol(), Integer.valueOf(bannerId));
        Map<String, Object> attributes = data.getAttributes();
        BannerEntity bannerEntity = new BannerEntity.Builder().withImageBanner((String) attributes.get("imageBanner"))
                .withImageThumbnail((String) attributes.get("imageThumbnail")).withLink((String) attributes.get("link"))
                .withType(EnumBannerType.valueOf((String) attributes.get("type")))
                .withSequence((int) attributes.get("sequence"))
                .withStatus(EnumBannerStatus.valueOf((String) attributes.get("status"))).build();
        bannerEntity.setBannerId(data.getInstanceId());

        return bannerEntity;
    }

    public void reduntantWork(List<Object[]> response) {
        for (Object[] categoryObject : response) {
            for (int i = 0; i < categoryObject.length - 1; i++) {
                String temp = ((String) categoryObject[i]).replaceAll("\"", "");
                categoryObject[i] = temp;
            }
        }
    }

    public ResponseEntity<BannerEntity> createBanner(BannerEntity bannerEntity) {
        try {
            InstanceData data = createInstanceDataForBanner(bannerEntity);
            bannerEntity.setBannerId(data.getInstanceId());
        } catch (InstanceRelatedException | QueryRelatedException | ClassRelatedException e) {
            logger.log(Level.FATAL, "Error Occurred in BannerCreation ", e);
            throw new MarketPlaceException("Banner creation failed");
        }
        return new ResponseEntity<BannerEntity>(bannerEntity, HttpStatus.OK);
    }

    private InstanceData createInstanceDataForBanner(BannerEntity bannerEntity)
            throws InstanceRelatedException, ClassRelatedException, QueryRelatedException {

        InstanceRequest bannerRequest = null;
        int sequence = getBannerMaxSequence();
        sequence += 1;
        bannerEntity.setSequence(sequence);
        bannerRequest = new InstanceRequest.Builder()
                .withAttributes(createRequestBanner.createWriteRequestBanner(bannerEntity))
                .withProtocol(MarketPlaceUtil.getMarketPlaceProtocol()).withClassName("Banner")
                .withStatus(KernelConstants.EnumDocStatusType.ACTIVE).build();
        return instanceService.create(bannerRequest);
    }

    private int getBannerMaxSequence() throws QueryRelatedException {
        DynamicReadRequest request = new DynamicReadRequest();
        createRequestBanner.createRequestForMaxSequence(request);
        DynamicReadResponse response = dynamicQuery.read(request);
        List<Object[]> instances = response.getResponse();
        if (instances == null || instances.isEmpty()) {
            return 0;
        }
        Object obj = instances.get(0);
        if (obj != null) {
            return Integer.valueOf(Util.replaceDoubleQuote((String) obj));
        }
        return -1;
    }

    public ResponseEntity<String> uploadImageForBanner(MultipartFile multipartFile) {
        String imagePath = null;
        try {
            String writeImageServerPath = serverProperties.getWriteImageServerPath();
            writeImageServerPath += File.separator + "banner";
            imagePath = serverProperties.getReadImageServerPath() + "/" + "banner";
            String fileName = null;
            fileName = "banner_" + new Date().getTime() + "."
                    + FilenameUtils.getExtension(multipartFile.getOriginalFilename());
            imagePath += "/" + fileName;
            fileWriterUtil.writeFile(multipartFile, writeImageServerPath, fileName);

        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Occurred in  in method uploadImageForBanner ", e);
            return new ResponseEntity<>("Could not upload image", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(imagePath, HttpStatus.OK);
    }


    public ResponseEntity<BannerEntity> updateBanner(BannerEntity bannerEntity){

        try {
            InstanceData data = updateBannerInstanceData(bannerEntity);
        } catch (Exception e) {
            logger.log(Level.FATAL, "Error Occurred in update banner", e);
            throw new MarketPlaceException("Banner updation failed");
        }
        return new ResponseEntity<BannerEntity>(bannerEntity,HttpStatus.OK);
    }

    private InstanceData updateBannerInstanceData(BannerEntity bannerEntity) {
        InstanceData data = null;
        try {
            InstanceRequest bannerRequest = new InstanceRequest.Builder()
                    .withAttributes(createRequestBanner.createWriteRequestBanner(bannerEntity))
                    .withProtocol(MarketPlaceUtil.getMarketPlaceProtocol()).withClassName("Banner")
                    .withStatus(KernelConstants.EnumDocStatusType.ACTIVE)
                    .withInstanceId(Integer.valueOf(bannerEntity.getBannerId())).build();
            data = instanceService.updateAttributes(bannerRequest);
        } catch (InstanceRelatedException e) {
            logger.log(Level.FATAL, "Error Occurred in updating imagepath in  method updateAppInstanceData ", e);
            throw new MarketPlaceException("ImagePath update Failed");
        }
        return data;
    }

    public ResponseEntity<List<BannerEntity>> updateBannerSequence(List<BannerEntity> bannerEntities) {

        List<BannerEntity> response = new ArrayList<>();

        for (BannerEntity banner : bannerEntities) {
            InstanceData instanceData = updateBannerInstanceData(banner);
            Map<String, Object> attributes = instanceData.getAttributes();
            BannerEntity bannerEntity = new BannerEntity.Builder()
                    .withImageBanner((String) attributes.get("imageBanner"))
                    .withImageThumbnail((String) attributes.get("imageThumbnail"))
                    .withLink((String) attributes.get("link"))
                    .withType((EnumBannerType) attributes.get("type"))
                    .withSequence((int) attributes.get("sequence"))
                    .withStatus((EnumBannerStatus) attributes.get("status")).build();
            bannerEntity.setBannerId(instanceData.getInstanceId());
            response.add(bannerEntity);

        }
        return new ResponseEntity<List<BannerEntity>>(response, HttpStatus.OK);
    }

}

package com.alt.marketplace.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alt.datacarrier.exception.MarketPlaceException;
import com.alt.datacarrier.kernel.common.KernelConstants;
import com.alt.datacarrier.kernel.db.core.InstanceData;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.marketplace.core.SubCategory;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.service.IInstanceService;
import com.alt.marketplace.util.MarketPlaceUtil;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.alt.datacarrier.kernel.db.core.DynamicReadRequest;
import com.alt.datacarrier.kernel.db.core.DynamicReadResponse;
import com.alt.datacarrier.marketplace.core.Category;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.marketplace.service.common.CreateCategoryRequests;

public class CategoryComponent {

    @Autowired
    IDynamicQuery dynamicQuery;

    @Autowired(required = true)
    IInstanceService instanceService;

    @Autowired
    CreateCategoryRequests createCategoryRequests;

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(CategoryComponent.class);

    public ResponseEntity<List<Category>> getCategories() {
        List<Category> categoryList = new ArrayList<>();
        try {
            DynamicReadResponse response = new DynamicReadResponse();
            DynamicReadRequest readRequest = new DynamicReadRequest();
            createCategoryRequests.createCategoriesReadRequest(readRequest);
            response = dynamicQuery.read(readRequest);
            if (response.getCode() == 200) {
                List<Object[]> objectList = new ArrayList<>();
                objectList = response.getResponse();
                redundantWork(objectList);
                for (Object[] objects : objectList) {
                    Category category = new Category.Builder().withCategoryName((String) objects[0]).
                            withCategoryId(Integer.toString((Integer) objects[1])).build();
                    categoryList.add(category);
                }
            }
        } catch (QueryRelatedException e) {
            logger.log(Level.FATAL, "Error Occurred in CategoryComponent in method getCategories ", e);
        }
        return new ResponseEntity<List<Category>>(categoryList, HttpStatus.OK);
    }

    //to do remove this redundant function
    public void redundantWork(List<Object[]> response) {
        for (Object[] categoryObject : response) {
            String categoryName = ((String) categoryObject[0]).replaceAll("\"", "");
            categoryObject[0] = categoryName;
        }
    }

    //to do remove this redundant function
    public void redundantWorkForSubCategory(List<Object[]> response) {
        for (Object[] categoryObject : response) {
            String subCategoryName = ((String) categoryObject[0]).replaceAll("\"", "");
            categoryObject[0] = subCategoryName;
            String sequence = ((String) categoryObject[1]).replaceAll("\"", "");
            categoryObject[1] = sequence;
        }
    }

    public ResponseEntity<Category> createCategory(Category category) {
        try {
            InstanceData data = createInstanceDataForCategory(category);
            category.setCategoryId(Integer.toString(data.getInstanceId()));
        } catch (InstanceRelatedException | QueryRelatedException | ClassRelatedException e) {
            logger.log(Level.FATAL, "Error Occurred in Category Creation ", e);
            throw new MarketPlaceException("Category Creation Failed");
        }
        return new ResponseEntity<Category>(category, HttpStatus.OK);
    }

    private InstanceData createInstanceDataForCategory(Category category)
            throws InstanceRelatedException, ClassRelatedException, QueryRelatedException {

        InstanceRequest categoryRequest = null;
        int sequence = getCategoryMaxSequence();
        sequence += 1;
        category.setSequence(Integer.toString(sequence));
        categoryRequest = new InstanceRequest.Builder()
                .withAttributes(createCategoryRequests.createWriteRequestCategory(category))
                .withProtocol(MarketPlaceUtil.getMarketPlaceProtocol()).withClassName("Category")
                .withStatus(KernelConstants.EnumDocStatusType.ACTIVE).build();
        return instanceService.create(categoryRequest);
    }

    private int getCategoryMaxSequence() {
        return 0;
    }

    public List<SubCategory> getSubcategories(String categoryId) {
        List<SubCategory> subCategories = new ArrayList<>();
        try {
            DynamicReadResponse response = new DynamicReadResponse();
            DynamicReadRequest readRequest = new DynamicReadRequest();
            createCategoryRequests.createSubCategoriesReadRequest(readRequest, categoryId);
            response = dynamicQuery.read(readRequest);
            if (response.getCode() == 200) {
                List<Object[]> objectList = response.getResponse();
                redundantWorkForSubCategory(objectList);
                for (Object[] objects : objectList) {
                    SubCategory subCategory = new SubCategory.Builder().withSubCategoryName((String) objects[0]).
                            withSequence((String)objects[1]).
                            withSubCategoryId(Integer.toString((Integer) objects[2])).build();
                    subCategories.add(subCategory);
                }
            }
        } catch (QueryRelatedException e) {
            logger.log(Level.FATAL, "Error Occurred in CategoryComponent in method getCategories ", e);
        }
        return subCategories;

    }

    public String updateSubCategorySequence(List<SubCategory> subCategories) {
        for (SubCategory subCategory : subCategories) {
            updateSubCategoryInstanceData(subCategory);
        }
        return "sequence updated successfully";
    }

    private InstanceData updateSubCategoryInstanceData(SubCategory subCategory) {
        InstanceData instanceData = null;
        try {
            InstanceRequest instanceRequest = new InstanceRequest.Builder().withAttributes(
                    createCategoryRequests.createWriteRequestSubCategory(subCategory))
                    .withProtocol(MarketPlaceUtil.getMarketPlaceProtocol()).withClassName("SubCategory")
                    .withStatus(KernelConstants.EnumDocStatusType.ACTIVE)
                    .withInstanceId(Integer.parseInt(subCategory.getSubCategoryId())).build();
            instanceData = instanceService.updateAttributes(instanceRequest);
        } catch (InstanceRelatedException e) {
            logger.log(Level.FATAL, "Error occurred while creating instance", e);
        }
        return instanceData;
    }

}

package com.alt.marketplace.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.alt.datacarrier.exception.MarketPlaceException;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.marketplace.request.ContactUsDetails;
import com.alt.datacarrier.marketplace.request.UserEnquiryDetails;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.service.IInstanceService;
import com.alt.marketplace.config.ServerPropertiesConf;
import com.alt.marketplace.util.MailAuthenticator;
import com.alt.marketplace.util.MarketPlaceUtil;

public class MarketPlaceEnquiryComponent {

	@Autowired
	private ServerPropertiesConf serverProperties;	

	@Autowired(required = true)
	IInstanceService instanceService;

	private static final Logger logger = Logger.getLogger(MarketPlaceEnquiryComponent.class);

	public ResponseEntity<String> saveUserEnquiry(UserEnquiryDetails userEnquiryDetails) {

		if (validator(userEnquiryDetails)) {

			saveUserEnquiryDetails(userEnquiryDetails);
			sendUserDetailsViaMail(userEnquiryDetails);

		} else
			throw new MarketPlaceException("Could not send enquiry.");

		return new ResponseEntity<String>("User Enquiry Details mailed and saved successfully", HttpStatus.OK);

	}

	public ResponseEntity<String> saveContactUs(ContactUsDetails contactUsDetails) {

		saveContactDetails(contactUsDetails);
		sendContactDetailsViaMail(contactUsDetails);

		return new ResponseEntity<String>("User Enquiry Details mailed and saved successfully", HttpStatus.OK);
	}

	private boolean validator(UserEnquiryDetails userEnquiryDetails) {
		if (userEnquiryDetails.getOrganizationName() == null || userEnquiryDetails.getOrganizationName().length() == 0)
			return false;
		if (userEnquiryDetails.getFirstName() == null || userEnquiryDetails.getFirstName().length() == 0)
			return false;
		if (userEnquiryDetails.getMobileNumber() == null || userEnquiryDetails.getMobileNumber().length() == 0)
			return false;
		if (userEnquiryDetails.getEmail() == null || userEnquiryDetails.getEmail().length() == 0)
			return false;
		return true;
	}

	public boolean sendUserDetailsViaMail(UserEnquiryDetails userEnquiryDetails) {
		Properties props = getMailProperties();
		Authenticator auth = new MailAuthenticator(serverProperties.getFromEmailId(),
				serverProperties.getEmailPassword());
		Session session = Session.getInstance(props, auth);
		MimeMessage mimeMessage = getMimeMessage(userEnquiryDetails, session);
		if (mimeMessage != null)
			try {
				Transport.send(mimeMessage);
			} catch (MessagingException e) {
				logger.log(Level.FATAL, "Error Occurred in UserEnquiryComponent in method sendUserDetailsViaMail ", e);
				throw new MarketPlaceException("Could not send mail for User Enquiry details");

			}

		return true;
	}
	
	public boolean sendContactDetailsViaMail(ContactUsDetails contactDetails) {
		Properties props = getMailProperties();
		Authenticator auth = new MailAuthenticator(serverProperties.getFromEmailId(),
				serverProperties.getEmailPassword());
		Session session = Session.getInstance(props, auth);
		MimeMessage mimeMessage = getMimeMessageContactUs(contactDetails, session);
		if (mimeMessage != null)
			try {
				Transport.send(mimeMessage);
			} catch (MessagingException e) {
				logger.log(Level.FATAL, "Error Occurred in UserEnquiryComponent in method sendUserDetailsViaMail ", e);
				throw new MarketPlaceException("Could not send mail for User Enquiry details");

			}

		return true;
	}
	
	public MimeMessage getMimeMessage(UserEnquiryDetails userEnquiryDetails, Session session) {
		MimeMessage mimeMsg = new MimeMessage(session);
		try {
			String subject = "MarketPlace User Enquiry for App: "+ userEnquiryDetails.getAppName(); // add app Id or //
															// Name
			InternetAddress fromAddress = new InternetAddress(serverProperties.getFromEmailId());		
			mimeMsg.setFrom(fromAddress);
			mimeMsg.setRecipients(RecipientType.TO, serverProperties.getToEmailIds());
			mimeMsg.setRecipients(RecipientType.BCC, serverProperties.getBccEmailIds());
			mimeMsg.setRecipients(RecipientType.CC, serverProperties.getCcEmailIds());
			mimeMsg.setSubject(subject);
			session.setDebug(true);
			Multipart msgMultipart = new MimeMultipart();
			BodyPart msgHtmlPart = new MimeBodyPart();
			msgHtmlPart.setContent(getMessageHTMLContent(userEnquiryDetails), "text/html");
			msgMultipart.addBodyPart(msgHtmlPart);
			mimeMsg.setContent(msgMultipart);
			return mimeMsg;
		} catch (Exception ex) {
			logger.log(Level.FATAL, "Error Occurred in UserEnquiryComponent in method getMimeMessage ", ex);
			return null;
		}

	}

	public MimeMessage getMimeMessageContactUs(ContactUsDetails contactDetails, Session session) {
		MimeMessage mimeMsg = new MimeMessage(session);
		try {
			String subject = "MarketPlace Contact Us Enquiry"; 
			InternetAddress fromAddress = new InternetAddress(serverProperties.getFromEmailId());		
			mimeMsg.setFrom(fromAddress);
			mimeMsg.setRecipients(RecipientType.TO, serverProperties.getToEmailIds());
			mimeMsg.setRecipients(RecipientType.BCC, serverProperties.getBccEmailIds());
			mimeMsg.setRecipients(RecipientType.CC, serverProperties.getCcEmailIds());
			mimeMsg.setSubject(subject);
			session.setDebug(true);
			Multipart msgMultipart = new MimeMultipart();
			BodyPart msgHtmlPart = new MimeBodyPart();
			msgHtmlPart.setContent(getMessageHTMLContentContactUs(contactDetails), "text/html");
			msgMultipart.addBodyPart(msgHtmlPart);
			mimeMsg.setContent(msgMultipart);
			return mimeMsg;
		} catch (Exception ex) {
			logger.log(Level.FATAL, "Error Occurred in UserEnquiryComponent in method getMimeMessage ", ex);
			return null;
		}

	}

	private Object getMessageHTMLContentContactUs(ContactUsDetails contactDetails) {
		return "<html><br>Hi Admin,<br><br>Name: " + contactDetails.getName()
		+ "<br>Company Name: " + contactDetails.getCompanyName() + "<br>Company Website: "
		+ contactDetails.getCompanyWebsite() + "<br>Email: " + contactDetails.getEmail()
		+ "<br>Company Address " + contactDetails.getCompanyAddress()
		+ "<br>MobileNumber: " + contactDetails.getMobileNumber() + "<br>Best Time To Call: "
		+ contactDetails.getBestTimeToCall()+ "<br>Comment: " + contactDetails.getComment()
		+ "<br><br> Alt MarketPlace</html>";
	}

	private String getMessageHTMLContent(UserEnquiryDetails userEnquiryDetails) {

		return "<html><br>Hi Admin,<br><br>Organisation Name: " + userEnquiryDetails.getOrganizationName()
				+ "<br>First Name: " + userEnquiryDetails.getFirstName() + "<br>Last Name: "
				+ userEnquiryDetails.getLastName() + "<br>Email: " + userEnquiryDetails.getEmail()
				+ "<br>How did you hear about Alt Marketplace: " + userEnquiryDetails.getHowDidYouHearText()
				+ "<br>MobileNumber: " + userEnquiryDetails.getMobileNumber() + "<br>Referred By: "
				+ userEnquiryDetails.getReferredBy() + "<br>Employee Count: " + userEnquiryDetails.getEmployeeCount()
				+ "<br><br> Alt MarketPlace</html>";

	}

	private Properties getMailProperties() {
		Properties mailProperties = new Properties();

		mailProperties.put("mail.smtp.host", "email-1.peoplestrong.com");
        mailProperties.put("mail.transport.protocol", "smtp");
        mailProperties.put("mail.login.username", "altone.peoplestrong.com");
        mailProperties.put("mail.login.password", "AltData@456");
        mailProperties.put("mail.debug", "true");
        mailProperties.put("mail.store.protocol", "pop3");
        mailProperties.put("mail.smtp.port", "587");
        
		return mailProperties;
	}

	public boolean saveUserEnquiryDetails(UserEnquiryDetails userEnquiryDetails) {
		try {
			InstanceRequest appRequest = new InstanceRequest.Builder()
					.withAttributes(createEnquiryDetailsAttributes(userEnquiryDetails))
					.withProtocol(MarketPlaceUtil.getMarketPlaceProtocol()).withClassName("UserEnquiryDetails")
					.withStatus(EnumDocStatusType.ACTIVE).build();
			instanceService.create(appRequest);
		} catch (InstanceRelatedException | ClassRelatedException ex) {
			logger.log(Level.FATAL, "Error Occurred in UserEnquiryComponent in method saveUserEnquiryDetails ", ex);
			throw new MarketPlaceException("Could not save market place enquiry details in DB");
		}
		return true;
	}

	public boolean saveContactDetails(ContactUsDetails contactUsDetails) {
		try {
			InstanceRequest appRequest = new InstanceRequest.Builder()
					.withAttributes(createContactUsAttributes(contactUsDetails))
					.withProtocol(MarketPlaceUtil.getMarketPlaceProtocol()).withClassName("ContactUsDetails")
					.withStatus(EnumDocStatusType.ACTIVE).build();
			instanceService.create(appRequest);
		} catch (InstanceRelatedException | ClassRelatedException ex) {
			logger.log(Level.FATAL, "Error Occurred in UserEnquiryComponent in method saveUserEnquiryDetails ", ex);
			throw new MarketPlaceException("Could not save market place enquiry details in DB");
		}
		return true;
	}

	private Map<String, Object> createEnquiryDetailsAttributes(UserEnquiryDetails userEnquiryDetails) {
		Map<String, Object> attributes = new HashMap<String, Object>();
		attributes.put("organisationName", userEnquiryDetails.getOrganizationName());
		attributes.put("firstName", userEnquiryDetails.getFirstName());
		attributes.put("lastName", userEnquiryDetails.getLastName());
		attributes.put("email", userEnquiryDetails.getEmail());
		attributes.put("howDidYouHearText", userEnquiryDetails.getHowDidYouHearText());
		attributes.put("mobileNumber", userEnquiryDetails.getMobileNumber());
		attributes.put("referredBy", userEnquiryDetails.getReferredBy());
		attributes.put("employeeCount", userEnquiryDetails.getEmployeeCount());
		return attributes;
	}

	private Map<String, Object> createContactUsAttributes(ContactUsDetails contactUsDetails) {
		Map<String, Object> attributes = new HashMap<String, Object>();
		attributes.put("name", contactUsDetails.getName());
		attributes.put("companyName", contactUsDetails.getCompanyName());
		attributes.put("companyAddress", contactUsDetails.getCompanyAddress());
		attributes.put("companyWebsite", contactUsDetails.getCompanyWebsite());
		attributes.put("email", contactUsDetails.getEmail());
		attributes.put("mobileNumber", contactUsDetails.getMobileNumber());
		attributes.put("bestTimeToCall", contactUsDetails.getBestTimeToCall());
		attributes.put("comment", contactUsDetails.getComment());
		return attributes;
	}

	
}
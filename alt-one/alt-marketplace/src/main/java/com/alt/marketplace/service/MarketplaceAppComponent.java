package com.alt.marketplace.service;

import com.alt.datacarrier.common.enumeration.EnumSortDirection;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.exception.MarketPlaceException;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumDocStatusType;
import com.alt.datacarrier.kernel.db.core.*;
import com.alt.datacarrier.kernel.db.request.InstanceRequest;
import com.alt.datacarrier.marketplace.core.*;
import com.alt.datakernel.exception.ClassRelatedException;
import com.alt.datakernel.exception.InstanceRelatedException;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.datakernel.service.IInstanceService;
import com.alt.marketplace.config.ServerPropertiesConf;
import com.alt.marketplace.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class MarketplaceAppComponent {

    @Autowired(required = true)
    IInstanceService instanceService;

    @Autowired(required = true)
    IDynamicQuery dynamicQuery;

    @Autowired
    FileWriterUtil fileWriterUtil;

    @Autowired
    ServerPropertiesConf serverProperties;

    @Autowired
    CategoryComponent categoryComponent;

    private static final Logger logger = Logger.getLogger(MarketplaceAppComponent.class);

    public List<CategoryAppDetail> getApps() throws QueryRelatedException, NumberFormatException, InstanceRelatedException {
        DynamicReadRequest request = new DynamicReadRequest();
        createDynamicQueryRequest(request);

        DynamicReadResponse response = dynamicQuery.read(request);
        List<Object[]> instances = response.getResponse();

        Map<String, List<Object[]>> categoryWiseAppDetails = new HashMap<>();
        for (Object[] instance : instances) {
            String category = Util.replaceDoubleQuote(((String) instance[0]));

            if (categoryWiseAppDetails.containsKey(category)) {
                categoryWiseAppDetails.get(category).add(instance);
            } else {
                List<Object[]> objects = new ArrayList<>();
                objects.add(instance);
                categoryWiseAppDetails.put(category, objects);
            }
        }

        return convertToCategoryAppDetails(request.getProtocol(), categoryWiseAppDetails);
    }

    private List<CategoryAppDetail> convertToCategoryAppDetails(Protocol protocol,
                                                                Map<String, List<Object[]>> categoryWiseAppDetails) throws NumberFormatException, InstanceRelatedException {
        List<CategoryAppDetail> details = new ArrayList<>();
        for (Map.Entry<String, List<Object[]>> entry1 : categoryWiseAppDetails.entrySet()) {
            String category = entry1.getKey();
            Map<String, List<Object[]>> subCategoryWiseAppDetails = new HashMap<>();
            for (Object[] instance : entry1.getValue()) {
                String subCategory = Util.replaceDoubleQuote((String) instance[11]);
                if (subCategoryWiseAppDetails.containsKey(subCategory)) {
                    subCategoryWiseAppDetails.get(subCategory).add(instance);
                } else {
                    List<Object[]> objects = new ArrayList<>();
                    objects.add(instance);
                    subCategoryWiseAppDetails.put(subCategory, objects);
                }
            }
            List<SubCategoryAppDetail> subCategoryAppDetails = new ArrayList<>();

            for (Map.Entry<String, List<Object[]>> entry2 : subCategoryWiseAppDetails.entrySet()) {
                String subCategory = entry2.getKey();
                List<MarketplaceAppDetail> appList = new ArrayList<>();
                for (Object[] list : entry2.getValue()) {
                    String screenshots = Util.replaceDoubleQuote((String) list[9]);

                    String[] screenshotArray = null;
                    List<String> screenshotsList = new ArrayList<>();
                    if (screenshots != null) {
                        screenshotArray = screenshots.split(",");
                    }
                    for (String s : screenshotArray) {
                        s = s.replaceAll("[\\[]", "");
                        s = s.replaceAll("[\\]]", "");
                        screenshotsList.add(s);
                    }

                    Integer id = ((Integer) list[10]);
                    String appName = Util.replaceDoubleQuote((String) list[1]);
                    String company = Util.replaceDoubleQuote((String) list[2]);
                    String rating = Util.replaceDoubleQuote((String) list[3]);
                    String launchDate = Util.replaceDoubleQuote((String) list[4]);
                    String previewBanner = Util.replaceDoubleQuote((String) list[5]);
                    String description = Util.replaceDoubleQuote((String) list[6]);
                    String appLogo = Util.replaceDoubleQuote((String) list[7]);
                    String headerCoverImage = Util.replaceDoubleQuote((String) list[8]);
                    String sequence = Util.replaceDoubleQuote((String) list[12]);
                    String shortDesc = Util.replaceDoubleQuote((String) list[13]);

                    MarketplaceAppDetail app = MarketplaceAppDetail.builder().withAppName(appName).withCompany(company)
                            .withRating(rating).withLaunchDate(launchDate).withPreviewBanner(previewBanner)
                            .withDescription(description).withAppLogo(appLogo).withHeaderCoverImage(headerCoverImage)
                            .withScreenShots(screenshotsList).withCategoryId(category).withAppId(id).withSubCategory(subCategory)
                            .withSequence(sequence).withShortDescription(shortDesc)
                            .build();
                    appList.add(app);

                }
                System.out.println(appList);
                appList.sort(new AppComparator());
                System.out.println(appList);

                // sort appList
                SubCategory subCat = null;
                if (subCategory != null) {
                    InstanceData subCategoryData = instanceService.read(protocol, Integer.valueOf(subCategory));
                    subCat = SubCategory.builder().withSubCategoryId(subCategoryData.getInstanceId().toString())
                            .withSubCategoryName((String) subCategoryData.getAttributes().get("subcategoryname"))
                            .withSequence((String) subCategoryData.getAttributes().get("sequence")).build();
                }
                SubCategoryAppDetail subCategoryAppDetail = SubCategoryAppDetail.builder().withSubCategory(subCat)
                        .withApp(appList).build();
                subCategoryAppDetails.add(subCategoryAppDetail);

            }

            // sort subCategoryAppDetails
            subCategoryAppDetails.sort(new SubCategoryComparator());
            //subCategoryAppDetails = sortSubCategory(subCategoryAppDetails);

            InstanceData categoryData = instanceService.read(protocol, Integer.valueOf(category));
            Category cat = Category.builder().withCategoryId(categoryData.getInstanceId().toString())
                    .withCategoryName((String) categoryData.getAttributes().get("categoryname"))
                    .withSequence((String) categoryData.getAttributes().get("sequence")).build();
            CategoryAppDetail categoryAppDetail = CategoryAppDetail.builder().withCategory(cat)
                    .withSubCategories(subCategoryAppDetails)
                    .build();
            details.add(categoryAppDetail);
        }

        //sort categoryAppDetail
        details.sort(new CategoryComparator());
        return details;

    }

    private void createDynamicQueryRequest(DynamicReadRequest request) {
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        projectionAttribute.setDbClass("instance");
        projectionAttribute.setDbAttributes(getDBAttributesForApp());
        projections.add(projectionAttribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");

        List<OrderAttribute> orderByList = new ArrayList<>();
        OrderAttribute orderBy = new OrderAttribute();
        orderBy.setDbClass("instance");
        orderBy.setDbAttr("attributes");
        orderBy.setJsonbType(true);
        orderBy.setJsonName("attribute");
        orderBy.setJsonbAttribute("sequence");
        orderBy.setSortDirection(EnumSortDirection.DESC);
        orderByList.add(orderBy);
        request.setOrderBy(orderByList);

        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("App");

        dbFilters.add(filter1);
        request.setDbFilters(dbFilters);
        Protocol protocol = MarketPlaceUtil.getMarketPlaceProtocol();
        request.setProtocol(protocol);

    }

    public CategoryAppDetail getAppsForCategory(String categoryId)
            throws QueryRelatedException, NumberFormatException, InstanceRelatedException {
        DynamicReadRequest request = new DynamicReadRequest();
        createDynamicQueryRequest(request, categoryId);

        DynamicReadResponse response = dynamicQuery.read(request);
        List<Object[]> instances = response.getResponse();

        Map<String, List<Object[]>> subCategoryWiseAppDetails = new HashMap<>();
        for (Object[] instance : instances) {
            String subCategory = Util.replaceDoubleQuote((String) instance[11]);
            if (subCategoryWiseAppDetails.containsKey(subCategory)) {
                subCategoryWiseAppDetails.get(subCategory).add(instance);
            } else {
                List<Object[]> objects = new ArrayList<>();
                objects.add(instance);
                subCategoryWiseAppDetails.put(subCategory, objects);
            }
        }

        List<SubCategoryAppDetail> subCategoryAppDetails = new ArrayList<>();

        for (Map.Entry<String, List<Object[]>> entry2 : subCategoryWiseAppDetails.entrySet()) {
            String subCategory = entry2.getKey();
            List<MarketplaceAppDetail> appList = new ArrayList<>();
            for (Object[] list : entry2.getValue()) {
                String screenshots = Util.replaceDoubleQuote((String) list[9]);
                String[] screenshotArray = null;
                List<String> screenshotsList = new ArrayList<>();
                if (screenshots != null) {
                    screenshotArray = screenshots.split(",");
                }
                for (String s : screenshotArray) {
                    s = s.replaceAll("[\\[]", "");
                    s = s.replaceAll("[\\]]", "");
                    screenshotsList.add(s);
                }

                String appName = Util.replaceDoubleQuote((String) list[1]);
                String company = Util.replaceDoubleQuote((String) list[2]);
                String rating = Util.replaceDoubleQuote((String) list[3]);
                String launchDate = Util.replaceDoubleQuote((String) list[4]);
                String previewBanner = Util.replaceDoubleQuote((String) list[5]);
                String description = Util.replaceDoubleQuote((String) list[6]);
                String appLogo = Util.replaceDoubleQuote((String) list[7]);
                String headerCoverImage = Util.replaceDoubleQuote((String) list[8]);
                Integer appId = ((Integer) list[10]);
                String shortDesc = Util.replaceDoubleQuote((String) list[13]);
                String sequence =  Util.replaceDoubleQuote((String) list[12]);

                MarketplaceAppDetail app = MarketplaceAppDetail.builder().withAppName(appName).withCompany(company)
                        .withRating(rating).withLaunchDate(launchDate).withPreviewBanner(previewBanner).withDescription(description)
                        .withAppLogo(appLogo).withHeaderCoverImage(headerCoverImage).withScreenShots(screenshotsList)
                        .withAppId(appId).withSubCategory(subCategory).withCategoryId(categoryId)
                        .withShortDescription(shortDesc).withSequence(sequence)
                        .build();
                appList.add(app);
            }
            appList.sort(new AppComparator());
            InstanceData subCategoryData = instanceService.read(MarketPlaceUtil.getMarketPlaceProtocol(),
                    Integer.valueOf(subCategory));
            SubCategory subCat = SubCategory.builder().withSubCategoryId(subCategoryData.getInstanceId().toString())
                    .withSubCategoryName((String) subCategoryData.getAttributes().get("subcategoryname"))
                    .withSequence((String) subCategoryData.getAttributes().get("sequence")).build();

            SubCategoryAppDetail subCategoryAppDetail = SubCategoryAppDetail.builder().withSubCategory(subCat)
                    .withApp(appList).build();
            subCategoryAppDetails.add(subCategoryAppDetail);
        }
        subCategoryAppDetails.sort(new SubCategoryComparator());
        InstanceData data = instanceService.read(MarketPlaceUtil.getMarketPlaceProtocol(), Integer.valueOf(categoryId));
        Map<String, Object> attributes = data.getAttributes();
        String categoryName = Util.replaceDoubleQuote((String) attributes.get("categoryname"));
        return CategoryAppDetail.builder().withSubCategories(subCategoryAppDetails)
                .withCategory(Category.builder().withCategoryId(categoryId).withCategoryName(categoryName)
                        .withSequence(Util.replaceDoubleQuote((String) attributes.get("sequence"))).build())
                .build();
    }

    private void createDynamicQueryRequest(DynamicReadRequest request, String categoryId) {
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        projectionAttribute.setDbClass("instance");
        projectionAttribute.setDbAttributes(getDBAttributesForApp());
        projections.add(projectionAttribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("App");
        dbFilters.add(filter1);
        DbFilters filter2 = new DbFilters();
        filter2.setAttribute("attributes");
        filter2.setClassName("instance");
        filter2.setJsonbType(true);
        List<String> jsonbAttributes = new ArrayList<String>();
        jsonbAttributes.add("category");
        filter2.setJsonbAttribute(jsonbAttributes);
        filter2.setJsonName("attribute");
        filter2.setCondition("=");
        filter2.setValue(categoryId);
        dbFilters.add(filter2);
        request.setDbFilters(dbFilters);
        request.setProtocol(MarketPlaceUtil.getMarketPlaceProtocol());

    }

    protected List<AttributesData> getDBAttributesForApp() {
        List<AttributesData> dbAttributes = new ArrayList<>();
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("category").withAs("category").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("appName").withAs("appName").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("company").withAs("company").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("rating").withAs("rating").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("launchDate").withAs("launchDate").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("previewBanner").withAs("previewBanner").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("description").withAs("description").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("appLogo").withAs("appLogo").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("headerCoverImage").withAs("headerCoverImage").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("screenShots").withAs("screenShots").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("id").withJsonbType(false).withAs("id").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("subCategory").withAs("subCategory").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("sequence").withAs("sequence").build());
        dbAttributes.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute").withJsonbAttribute("shortDescription").withAs("shortDescription").build());
        return dbAttributes;

    }

    public MarketplaceAppDetail getAppDetailsFromId(Integer appId)
            throws NumberFormatException, InstanceRelatedException {
        InstanceData data = instanceService.read(MarketPlaceUtil.getMarketPlaceProtocol(), Integer.valueOf(appId));
        Map<String, Object> attributes = data.getAttributes();
        MarketplaceAppDetail details = MarketplaceAppDetail.builder()
                .withHeaderCoverImage(Util.replaceDoubleQuote((String) attributes.get("headerCoverImage"))).withAppId(appId)
                .withPreviewBanner(Util.replaceDoubleQuote((String) attributes.get("previewBanner")))
                .withAppLogo(Util.replaceDoubleQuote((String) attributes.get("appLogo")))
                .withAppName(Util.replaceDoubleQuote((String) attributes.get("appName")))
                .withCategoryId(Util.replaceDoubleQuote((String) attributes.get("category")))
                .withCompany(Util.replaceDoubleQuote((String) attributes.get("company")))
                .withDescription(Util.replaceDoubleQuote((String) attributes.get("description")))
                .withLaunchDate(Util.replaceDoubleQuote((String) attributes.get("launchDate")))
                .withRating(Util.replaceDoubleQuote((String) attributes.get("rating")))
                .withScreenShots((List<String>) attributes.get("screenShots"))
                .withSubCategory(Util.replaceDoubleQuote((String) attributes.get("subCategory")))
                .withWebsiteUrl(Util.replaceDoubleQuote((String) attributes.get("websiteUrl")))
                .withVideoUrl(Util.replaceDoubleQuote((String) attributes.get("videoUrl")))
                .withAppUrl(Util.replaceDoubleQuote((String) attributes.get("appUrl")))
                .withShortDescription(Util.replaceDoubleQuote((String) attributes.get("shortDescription")))
                .withHeaderCoverImage(Util.replaceDoubleQuote((String) attributes.get("headerCoverImage")))
                .withPreviewBanner(Util.replaceDoubleQuote((String) attributes.get("previewBanner")))
                .withPartnerId(Util.replaceDoubleQuote((String) attributes.get("partnerId")))
                .withSequence(Util.replaceDoubleQuote((String) attributes.get("sequence")))
                .build();

        return details;
    }

    public ResponseEntity<MarketplaceAppDetail> createApp(MarketplaceAppDetail app) throws QueryRelatedException {
        try {
            if (MarketPlaceUtil.isValidateApp(app)) {
                InstanceData data = createInstanceDataForApp(app);
                app.setAppId(data.getInstanceId());
            } else
                throw new MarketPlaceException("Invalid app data");
        } catch (InstanceRelatedException | ClassRelatedException e) {
            logger.log(Level.FATAL, "Error Occurred in createApp ", e);
            throw new MarketPlaceException("App creation failed");
        }
        return new ResponseEntity<MarketplaceAppDetail>(app, HttpStatus.OK);
    }

    public ResponseEntity<String> editAppDetails(MarketplaceAppDetail app) {
        updateAppInstanceData(app);
        return new ResponseEntity<String>("Updated successfully", HttpStatus.OK);

    }

    public ResponseEntity<String> uploadImageForApp(Integer appId, String imageField, MultipartFile multipartFile) {

        MarketplaceAppDetail app = null;
        try {
            app = getAppDetailsFromId(appId);

        } catch (NumberFormatException | InstanceRelatedException e1) {
            logger.log(Level.FATAL,
                    "Error Occurred in UserEnquiryComponent in method uploadImageForApp " + e1.getMessage());
            return new ResponseEntity<>("Could not upload image", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (app == null)
            throw new MarketPlaceException("App doesn't exist");
        if (app.getAppId() == null)
            throw new MarketPlaceException("App Id is null");

        String partnerId = app.getPartnerId();

        if (partnerId == null) {
            throw new MarketPlaceException("Partner Id is null");
        }

        String imagePath = null;
        try {
            imagePath = fileWriterUtil.writeFileToServer(partnerId, app.getAppId(), imageField, multipartFile,
                    serverProperties.getReadImageServerPath(), serverProperties.getWriteImageServerPath());
            switch (imageField) {

                case "screenShots":
                    List<String> screenshots = app.getScreenShots();
                    screenshots.add(imagePath);
                    app.setScreenshots(screenshots);
                    break;
                case "appLogo":
                    app.setAppLogo(imagePath);
                    break;
                case "previewBanner":
                    app.setPreviewBanner(imagePath);
                    break;
                case "headerCover":
                    app.setHeaderCoverImage(imagePath);
                    break;
            }

        } catch (IOException e) {
            logger.log(Level.FATAL, "Error Occurred in UserEnquiryComponent in method uploadImageForApp ", e);
            return new ResponseEntity<>("Could not upload image", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        updateAppInstanceData(app);
        return new ResponseEntity<>(imagePath, HttpStatus.OK);
    }

    private InstanceData createInstanceDataForApp(MarketplaceAppDetail app)
            throws InstanceRelatedException, ClassRelatedException, QueryRelatedException {
        InstanceRequest appRequest = null;
        try {
            Integer sequence = getAppMaxSequence(app.getCategoryId(), app.getSubCategory());
            sequence += 1;
            appRequest = new InstanceRequest.Builder().withAttributes(createAppAttributes(app, sequence.toString()))
                    .withProtocol(MarketPlaceUtil.getMarketPlaceProtocol()).withClassName("App")
                    .withStatus(EnumDocStatusType.ACTIVE).build();
        } catch (JsonProcessingException e) {
            logger.log(Level.FATAL, "Error Occurred in method createInstanceDataForApp ", e);
        }
        return instanceService.create(appRequest);

    }

    private Integer getAppMaxSequence(String categoryId, String subCategory) throws QueryRelatedException {
        String defaultSubCategoryId = "";
        DynamicReadRequest request = new DynamicReadRequest();

        if (subCategory == null || subCategory.isEmpty()) {
            defaultSubCategoryId = getDefaultSubCategoryId(categoryId);
            createDynamicQueryRequest1(request, categoryId, defaultSubCategoryId);
        } else {
            createDynamicQueryRequest1(request, categoryId, subCategory);
        }

        DynamicReadResponse response = dynamicQuery.read(request);
        List<Object[]> instances = response.getResponse();

        if (instances == null || instances.isEmpty()) {
            return 0;
        }

        Object obj = instances.get(0);
        if (obj != null) {
            return Integer.valueOf(Util.replaceDoubleQuote((String) obj));
        }
        return -1;

    }

    private void createDynamicQueryRequest1(DynamicReadRequest request, String categoryId, String subCategory) {
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        projectionAttribute.setDbClass("instance");
        List<AttributesData> dbAttributes = new ArrayList<>();
        AttributesData data1 = new AttributesData();
        data1.setAttribute("attributes");
        data1.setJsonbType(true);
        data1.setJsonName("attribute");
        data1.setJsonbAttribute("sequence");
        data1.setAs("Sequence");
        dbAttributes.add(data1);
        projectionAttribute.setDbAttributes(dbAttributes);
        projections.add(projectionAttribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("App");
        dbFilters.add(filter1);
        DbFilters filter2 = new DbFilters();
        filter2.setAttribute("attributes");
        filter2.setClassName("instance");
        filter2.setJsonbType(true);
        List<String> jsonbAttributes1 = new ArrayList<String>();
        jsonbAttributes1.add("category");
        filter2.setJsonbAttribute(jsonbAttributes1);
        filter2.setJsonName("attribute");
        filter2.setCondition("=");
        filter2.setValue(categoryId);
        dbFilters.add(filter2);
        DbFilters filter3 = new DbFilters();
        filter3.setAttribute("attributes");
        filter3.setClassName("instance");
        filter3.setJsonbType(true);
        List<String> jsonbAttributes2 = new ArrayList<String>();
        jsonbAttributes2.add("subCategory");
        filter3.setJsonbAttribute(jsonbAttributes2);
        filter3.setJsonName("attribute");
        filter3.setCondition("=");
        filter3.setValue(subCategory);
        dbFilters.add(filter3);
        request.setDbFilters(dbFilters);
        List<OrderAttribute> orderByList = new ArrayList<>();
        OrderAttribute orderBy = new OrderAttribute();
        orderBy.setDbClass("instance");
        orderBy.setDbAttr("attributes");
        orderBy.setJsonbType(true);
        orderBy.setJsonName("attribute");
        orderBy.setJsonbAttribute("sequence");
        orderBy.setSortDirection(EnumSortDirection.DESC);
        orderByList.add(orderBy);
        request.setOrderBy(orderByList);
        request.setLimit(1);
        request.setProtocol(MarketPlaceUtil.getMarketPlaceProtocol());
    }

    private InstanceData updateAppInstanceData(MarketplaceAppDetail app) {
        InstanceData data = null;
        try {
            InstanceRequest appRequest = new InstanceRequest(
                    new InstanceRequest.Builder().withAttributes(createAppAttributes(app, app.getSequence()))
                            .withProtocol(MarketPlaceUtil.getMarketPlaceProtocol()).withClassName("Application")
                            .withStatus(EnumDocStatusType.ACTIVE).withInstanceId(Integer.valueOf(app.getAppId())));
            data = instanceService.updateAttributes(appRequest);

        } catch (NumberFormatException | JsonProcessingException | InstanceRelatedException e) {
            logger.log(Level.FATAL, "Error Occurred in UserEnquiryComponent in method updateAppInstanceData ", e);
            throw new MarketPlaceException("ImagePath update  Failed");
        }
        return data;

    }

    public Map<String, Object> createAppAttributes(MarketplaceAppDetail app, String sequence) throws JsonProcessingException {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("partnerId", app.getPartnerId());
        attributes.put("appName", app.getAppName());
        attributes.put("company", app.getCompany());
        attributes.put("rating", app.getRating());
        attributes.put("launchDate", app.getLaunchDate());
        attributes.put("category", app.getCategoryId());
        attributes.put("description", app.getDescription());
        attributes.put("appLogo", app.getAppLogo());
        attributes.put("screenShots", app.getScreenShots());
        if (app.getSubCategory() != null && !app.getSubCategory().isEmpty()) {
            attributes.put("subCategory", app.getSubCategory());
        } else {
            String defaultSubCategoryId = getDefaultSubCategoryId(app.getCategoryId());
            attributes.put("subCategory", defaultSubCategoryId);
        }
        attributes.put("websiteUrl", app.getWebsiteUrl());
        attributes.put("videoUrl", app.getVideoUrl());
        attributes.put("appUrl", app.getAppUrl());
        attributes.put("shortDescription", app.getShortDescription());
        attributes.put("headerCoverImage", app.getHeaderCoverImage());
        attributes.put("previewBanner", app.getPreviewBanner());
        attributes.put("sequence", sequence);
        return attributes;
    }

    private String getDefaultSubCategoryId(String categoryId) {
        List<SubCategory> subCategories = categoryComponent.getSubcategories(categoryId);
        String subCategoryId = null;
        for (SubCategory subCategory : subCategories) {
            if (subCategory.getSubCategoryName().equals("Default")) {
                subCategoryId = subCategory.getSubCategoryId();
            }
        }
        return subCategoryId;

    }

    public SubCategoryAppDetail getAppsForSubCategory(String categoryId, String subCategory) throws NumberFormatException,
            InstanceRelatedException, QueryRelatedException {
        DynamicReadRequest request = new DynamicReadRequest();
        createDynamicQueryRequest(request, categoryId, subCategory);

        DynamicReadResponse response = dynamicQuery.read(request);
        List<Object[]> instances = response.getResponse();

        List<MarketplaceAppDetail> appList = new ArrayList<>();
        for (Object[] list : instances) {
            String screenshots = Util.replaceDoubleQuote((String) list[9]);
            String[] screenshotArray = null;
            List<String> screenshotsList = new ArrayList<>();
            if (screenshots != null) {
                screenshotArray = screenshots.split(",");
            }
            for (String s : screenshotArray) {
                s = s.replaceAll("[\\[]", "");
                s = s.replaceAll("[\\]]", "");
                screenshotsList.add(s);
            }

            String appName = Util.replaceDoubleQuote((String) list[1]);
            String company = Util.replaceDoubleQuote((String) list[2]);
            String rating = Util.replaceDoubleQuote((String) list[3]);
            String launchDate = Util.replaceDoubleQuote((String) list[4]);
            String previewBanner = Util.replaceDoubleQuote((String) list[5]);
            String description = Util.replaceDoubleQuote((String) list[6]);
            String appLogo = Util.replaceDoubleQuote((String) list[7]);
            String headerCoverImage = Util.replaceDoubleQuote((String) list[8]);
            Integer appId = ((Integer) list[10]);
            String sequence = Util.replaceDoubleQuote((String) list[12]);
            MarketplaceAppDetail app = MarketplaceAppDetail.builder().withAppName(appName).withCompany(company)
                    .withRating(rating).withLaunchDate(launchDate).withPreviewBanner(previewBanner)
                    .withDescription(description).withAppLogo(appLogo).withHeaderCoverImage(headerCoverImage)
                    .withScreenShots(screenshotsList).withAppId(appId).withSubCategory(subCategory)
                    .withCategoryId(categoryId).withSequence(sequence).build();
            appList.add(app);
        }
        appList.sort(new AppComparator());
        InstanceData subCategoryData = instanceService.read(MarketPlaceUtil.getMarketPlaceProtocol(),
                Integer.valueOf(subCategory));
        SubCategory subCat = SubCategory.builder().withSubCategoryId(subCategoryData.getInstanceId().toString())
                .withSubCategoryName((String) subCategoryData.getAttributes().get("subcategoryname"))
                .withSequence((String) subCategoryData.getAttributes().get("sequence")).build();

        return SubCategoryAppDetail.builder().withSubCategory(subCat).withApp(appList).build();

    }

    private void createDynamicQueryRequest(DynamicReadRequest request, String categoryId, String subCategoryId) {
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        projectionAttribute.setDbClass("instance");
        projectionAttribute.setDbAttributes(getDBAttributesForApp());
        projections.add(projectionAttribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("App");
        dbFilters.add(filter1);
        DbFilters filter2 = new DbFilters();
        filter2.setAttribute("attributes");
        filter2.setClassName("instance");
        filter2.setJsonbType(true);
        List<String> jsonbAttributes1 = new ArrayList<String>();
        jsonbAttributes1.add("category");
        filter2.setJsonbAttribute(jsonbAttributes1);
        filter2.setJsonName("attribute");
        filter2.setCondition("=");
        filter2.setValue(categoryId);
        dbFilters.add(filter2);
        DbFilters filter3 = new DbFilters();
        filter3.setAttribute("attributes");
        filter3.setClassName("instance");
        filter3.setJsonbType(true);
        List<String> jsonbAttributes2 = new ArrayList<String>();
        jsonbAttributes2.add("subCategory");
        filter3.setJsonbAttribute(jsonbAttributes2);
        filter3.setJsonName("attribute");
        filter3.setCondition("=");
        filter3.setValue(subCategoryId);
        dbFilters.add(filter3);
        request.setDbFilters(dbFilters);
        request.setProtocol(MarketPlaceUtil.getMarketPlaceProtocol());
    }

	public ResponseEntity<String> updateAppSequence(List<MarketplaceAppDetail> request) {
		for (MarketplaceAppDetail appDetail: request){
			updateAppInstanceData(appDetail);
		}
		return new ResponseEntity<String>("sequence updated successfully", HttpStatus.OK);
	}

}

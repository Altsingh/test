package com.alt.marketplace.service;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.*;
import com.alt.datacarrier.marketplace.core.MarketplaceAppDetail;
import com.alt.datakernel.exception.QueryRelatedException;
import com.alt.datakernel.service.IDynamicQuery;
import com.alt.marketplace.util.MarketPlaceUtil;
import com.alt.marketplace.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PartnerAppComponent {

    @Autowired
    MarketplaceAppComponent appComponent;

    @Autowired
    IDynamicQuery dynamicQuery;

    public List<MarketplaceAppDetail> getApps(Protocol protocol) throws QueryRelatedException {
        DynamicReadRequest request = new DynamicReadRequest();
        request = createDynamicQueryRequest(request, protocol.getUserCode());

        DynamicReadResponse response = dynamicQuery.read(request);
        List<Object[]> instances = response.getResponse();

        List<MarketplaceAppDetail> appList = new ArrayList<>();
        for (Object[] list : instances) {
            Integer appId = ((Integer) list[10]);
            String screenshots = Util.replaceDoubleQuote((String) list[9]);

            String[] screenshotArray = null;
            List<String> screenshotsList = new ArrayList<>();
            if (screenshots != null) {
                screenshotArray = screenshots.split(",");
            }
            for (String s : screenshotArray) {
                s = s.replaceAll("[\\[]", "");
                s = s.replaceAll("[\\]]", "");
                screenshotsList.add(s);
            }
            String appName = Util.replaceDoubleQuote((String) list[1]);
            String company = Util.replaceDoubleQuote((String) list[2]);
            String rating = Util.replaceDoubleQuote((String) list[3]);
            String launchDate = Util.replaceDoubleQuote((String) list[4]);
            String appImage = Util.replaceDoubleQuote((String) list[5]);
            String description = Util.replaceDoubleQuote((String) list[6]);
            String appLogo = Util.replaceDoubleQuote((String) list[7]);
            String appCoverImage = Util.replaceDoubleQuote((String) list[8]);
            String shortDesc = Util.replaceDoubleQuote((String) list[13]);

            MarketplaceAppDetail app = MarketplaceAppDetail.builder().withAppId(appId).withAppName(appName).withCompany(company)
                    .withRating(rating).withLaunchDate(launchDate).withPreviewBanner(appImage).withDescription(description)
                    .withAppLogo(appLogo).withHeaderCoverImage(appCoverImage).withScreenShots(screenshotsList)
                    .withShortDescription(shortDesc)
                    .build();
            appList.add(app);
        }
        return appList;

    }

    private DynamicReadRequest createDynamicQueryRequest(DynamicReadRequest request, String userId) {
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        projectionAttribute.setDbClass("instance");
        projectionAttribute.setDbAttributes(appComponent.getDBAttributesForApp());
        projections.add(projectionAttribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("App");
        dbFilters.add(filter1);
        DbFilters filter2 = new DbFilters();
        filter2.setAttribute("attributes");
        filter2.setClassName("instance");
        filter2.setJsonbType(true);
        List<String> jsonbAttributes = new ArrayList<String>();
        jsonbAttributes.add("partnerId");
        filter2.setJsonbAttribute(jsonbAttributes);
        filter2.setJsonName("attribute");
        filter2.setCondition("=");
        filter2.setValue(userId);
        dbFilters.add(filter2);
        request.setDbFilters(dbFilters);
        request.setProtocol(MarketPlaceUtil.getMarketPlaceProtocol());
        return request;
    }
}

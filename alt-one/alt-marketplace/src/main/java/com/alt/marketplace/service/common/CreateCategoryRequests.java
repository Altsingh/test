package com.alt.marketplace.service.common;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.*;
import com.alt.datacarrier.marketplace.core.Category;
import com.alt.datacarrier.marketplace.core.SubCategory;
import com.alt.marketplace.util.MarketPlaceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateCategoryRequests {

    public DynamicReadRequest createCategoriesReadRequest(DynamicReadRequest request) {
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        projectionAttribute.setDbClass("instance");
        projectionAttribute.setDbAttributes(getDBAttributesForCategory());
        projections.add(projectionAttribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("Category");
        dbFilters.add(filter1);
        request.setDbFilters(dbFilters);
        Protocol protocol = MarketPlaceUtil.getMarketPlaceProtocol();
        request.setProtocol(protocol);
        return request;
    }

    public List<AttributesData> getDBAttributesForCategory() {
        List<AttributesData> attributesData = new ArrayList<>();
        attributesData.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute")
                .withJsonbAttribute("categoryname").withAs("categoryName").build());
        attributesData.add(new AttributesData.Builder().withAttribute("id").withAs("categoryId").build());
        return attributesData;
    }

    public Map<String, Object> createWriteRequestCategory(Category category) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("categoryname", category.getCategoryName());
        attributes.put("sequence", category.getSequence());
        return attributes;
    }

    public Map<String, Object> createWriteRequestSubCategory(SubCategory subCategory) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("subcategoryname", subCategory.getSubCategoryName());
        attributes.put("sequence", subCategory.getSequence());
        attributes.put("category", "1");
        return attributes;
    }

    public DynamicReadRequest createSubCategoriesReadRequest(DynamicReadRequest request, String categoryId) {
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        projectionAttribute.setDbClass("instance");
        projectionAttribute.setDbAttributes(getDBAttributesForSubCategory());
        projections.add(projectionAttribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("SubCategory");
        dbFilters.add(filter1);
        DbFilters filter2 = new DbFilters();
        filter2.setAttribute("attributes");
        filter2.setJsonbType(true);
        filter2.setJsonName("attribute");
        List<String> jsonbAttributes1 = new ArrayList<String>();
        jsonbAttributes1.add("category");
        filter2.setJsonbAttribute(jsonbAttributes1);
        filter2.setClassName("instance");
        filter2.setCondition("=");
        filter2.setValue(categoryId);
        dbFilters.add(filter2);
        request.setDbFilters(dbFilters);
        Protocol protocol = MarketPlaceUtil.getMarketPlaceProtocol();
        request.setProtocol(protocol);
        return request;
    }

    public List<AttributesData> getDBAttributesForSubCategory() {
        List<AttributesData> attributesDatas = new ArrayList<>();
        attributesDatas.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute")
                .withJsonbAttribute("subcategoryname").withAs("SubCategoryName").build());
        attributesDatas.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute")
                .withJsonbAttribute("sequence").withAs("Sequence").build());

        attributesDatas.add(new AttributesData.Builder().withAttribute("id").withAs("SubCategoryId").build());
        return attributesDatas;
    }

}

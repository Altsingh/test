package com.alt.marketplace.service.common;

import com.alt.datacarrier.common.enumeration.EnumSortDirection;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.kernel.db.core.AttributesData;
import com.alt.datacarrier.kernel.db.core.DbFilters;
import com.alt.datacarrier.kernel.db.core.DynamicReadRequest;
import com.alt.datacarrier.kernel.db.core.OrderAttribute;
import com.alt.datacarrier.kernel.db.core.ProjectionAttribute;
import com.alt.datacarrier.marketplace.core.BannerEntity;
import com.alt.marketplace.util.MarketPlaceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateRequestBanner {

    public  DynamicReadRequest createReadRequest(DynamicReadRequest request){
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        projectionAttribute.setDbClass("instance");
        projectionAttribute.setDbAttributes(getDBAttributesForBanner());
        projections.add(projectionAttribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("Banner");
        dbFilters.add(filter1);
        List<OrderAttribute> orderByList = new ArrayList<>();
        OrderAttribute orderBy = new OrderAttribute();
        orderBy.setDbClass("instance");
        orderBy.setDbAttr("attributes");
        orderBy.setJsonbType(true);
        orderBy.setJsonName("attribute");
        orderBy.setJsonbAttribute("sequence");
        orderBy.setSortDirection(EnumSortDirection.ASC);
        orderByList.add(orderBy);
        request.setOrderBy(orderByList);
        request.setDbFilters(dbFilters);
        Protocol protocol = MarketPlaceUtil.getMarketPlaceProtocol();
        request.setProtocol(protocol);
        return  request;

    }


    public List<AttributesData> getDBAttributesForBanner(){
        List<AttributesData> attributesData=new ArrayList<>();
        attributesData.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute")
                .withJsonbAttribute("imageBanner").withAs("imageBanner").build());
        attributesData.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute")
                .withJsonbAttribute("imageThumbnail").withAs("imageThumbnail").build());
        attributesData.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute")
                .withJsonbAttribute("type").withAs("type").build());
        attributesData.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute")
                .withJsonbAttribute("status").withAs("status").build());
        attributesData.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute")
                .withJsonbAttribute("link").withAs("link").build());
        attributesData.add(new AttributesData.Builder().withAttribute("attributes").withJsonbType(true).withJsonName("attribute")
                .withJsonbAttribute("sequence").withAs("sequence").build());

        attributesData.add(new AttributesData.Builder().withAttribute("id").withAs("bannerId").build());

        return  attributesData;
    }

    public void createRequestForMaxSequence(DynamicReadRequest request){
        List<ProjectionAttribute> projections = new ArrayList<>();
        ProjectionAttribute projectionAttribute = new ProjectionAttribute();
        projectionAttribute.setDbClass("instance");
        List<AttributesData> dbAttributes = new ArrayList<>();
        AttributesData data1 = new AttributesData();
        data1.setAttribute("attributes");
        data1.setJsonbType(true);
        data1.setJsonName("attribute");
        data1.setJsonbAttribute("sequence");
        data1.setAs("Sequence");
        dbAttributes.add(data1);
        projectionAttribute.setDbAttributes(dbAttributes);
        projections.add(projectionAttribute);
        request.setProjections(projections);
        request.setFromDbClass("instance");
        List<DbFilters> dbFilters = new ArrayList<>();
        DbFilters filter1 = new DbFilters();
        filter1.setAttribute("classname");
        filter1.setClassName("instance");
        filter1.setCondition("=");
        filter1.setValue("Banner");
        dbFilters.add(filter1);
        request.setDbFilters(dbFilters);
        List<OrderAttribute> orderByList = new ArrayList<>();
        OrderAttribute orderBy = new OrderAttribute();
        orderBy.setDbClass("instance");
        orderBy.setDbAttr("attributes");
        orderBy.setJsonbType(true);
        orderBy.setJsonName("attribute");
        orderBy.setJsonbAttribute("sequence");
        orderBy.setSortDirection(EnumSortDirection.DESC);
        orderByList.add(orderBy);
        request.setOrderBy(orderByList);
        request.setLimit(1);
        request.setProtocol(MarketPlaceUtil.getMarketPlaceProtocol());
    }

    public Map<String, Object> createWriteRequestBanner(BannerEntity bannerEntity){
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("status", bannerEntity.getStatus());
        attributes.put("type", bannerEntity.getType());
        attributes.put("imageBanner", bannerEntity.getImageBanner());
        attributes.put("imageThumbnail", bannerEntity.getImageThumbnail());
        attributes.put("link", bannerEntity.getLink());
        attributes.put("sequence", bannerEntity.getSequence());
        return attributes;
    }
}

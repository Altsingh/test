package com.alt.marketplace.util;

import com.alt.datacarrier.marketplace.core.MarketplaceAppDetail;

import java.util.Comparator;

public class AppComparator implements Comparator<MarketplaceAppDetail> {


    @Override
    public int compare(MarketplaceAppDetail o1, MarketplaceAppDetail o2) {
    	if(o1.getSequence() != null && o2.getSequence()!= null) {
    		Integer sc1 = Integer.parseInt(o1.getSequence());
    		Integer sc2 = Integer.parseInt(o2.getSequence());
    		return Integer.compare(sc1, sc2);
    	}
		return 0;
    }

}

package com.alt.marketplace.util;

import com.alt.datacarrier.marketplace.core.CategoryAppDetail;

import java.util.Comparator;

public class CategoryComparator implements Comparator<CategoryAppDetail> {

	@Override
    public int compare(CategoryAppDetail o1, CategoryAppDetail o2) {
		if(o1.getCategory().getSequence() != null && o2.getCategory().getSequence()!= null) {
			Integer sc1 = Integer.parseInt(o1.getCategory().getSequence());
			Integer sc2 = Integer.parseInt(o2.getCategory().getSequence());
			return Integer.compare(sc1, sc2);
		}
		return 0;
    }

}

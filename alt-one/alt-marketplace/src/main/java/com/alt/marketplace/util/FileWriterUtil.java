package com.alt.marketplace.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;

import com.alt.datacarrier.exception.MarketPlaceException;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

public class FileWriterUtil {
    
	private static final Logger logger = Logger.getLogger(FileWriterUtil.class);
	
    public String writeFileToServer(String partnerId, Integer appId, String imageField, MultipartFile mutiPartFile,
    		String returnedReadPath, String writePath) throws IOException {

        String partnerAppImagePath = writePath +File.separator+ "partner" + File.separator + partnerId + File.separator + appId;
        returnedReadPath =returnedReadPath+File.separator+"partner";
        String fileName = null;

        if (imageField.equals("screenShots")) {
            fileName = new Date().getTime() + "_" + mutiPartFile.getOriginalFilename();
            partnerAppImagePath = partnerAppImagePath + File.separator + "screenShots";
            returnedReadPath += "/" + partnerId + "/" + appId + "/" + "/screenShots/" + fileName;

        } else {
            fileName = new Date().getTime() + "_" + imageField + "." + FilenameUtils.getExtension(mutiPartFile.getOriginalFilename());
            returnedReadPath += "/" + partnerId + "/" + appId + "/" + fileName;
        }

         writeFile(mutiPartFile,  partnerAppImagePath, fileName);

        return returnedReadPath;
    }


    public void writeFile(MultipartFile multiPartFile , String partnerAppImagePath, String fileName) {

        java.nio.file.Path imagePath;
        createDirectory(partnerAppImagePath);

        imagePath = Paths.get(partnerAppImagePath).resolve(fileName);

        try {
            Files.copy(multiPartFile.getInputStream(), imagePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new MarketPlaceException("Could not upload image");
        }

    }

    private void createDirectory(String partnerAppImagePath) {
        java.nio.file.Path directoryPath = Paths.get(partnerAppImagePath);

        if (!Files.isDirectory(directoryPath))
            try {
                Files.createDirectories(directoryPath);
            } catch (IOException e) {
            	logger.log(Level.FATAL, "Error Occurred in FileWriterUtil in method createDirectory ", e);
            }

    }

}

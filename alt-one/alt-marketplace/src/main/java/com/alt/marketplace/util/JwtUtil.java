package com.alt.marketplace.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.xml.bind.DatatypeConverter;

import com.alt.datacarrier.common.enumeration.EnumPortalTypes;
import com.alt.datacarrier.core.Protocol;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;

public class JwtUtil {
    static String secretKey = "alt_secret_key";

    public static Protocol validateJwt(String jwt)  {

        Claims claims = null;
        String res1 = Base64.getEncoder().encodeToString(secretKey.getBytes(StandardCharsets.UTF_8));
        try{
        	claims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(res1)).parseClaimsJws(jwt)
                    .getBody();
        }catch(ExpiredJwtException e){
            throw new RuntimeException("Auth Token Expired", e);
        }catch(MalformedJwtException e) {
        	throw new RuntimeException("JWT taken malformed", e);
        }
        return getProtocolFromClaims(claims);
    }
    
    private static Protocol getProtocolFromClaims(Claims claims) {

        Protocol protocol = new Protocol();
        protocol.setUserName(claims.get("userName", String.class));
        protocol.setUserCode(claims.get("userCode", String.class));
        protocol.setLoggedInTimestamp(claims.get("loggedInTimestamp", Long.class));
        protocol.setLoggedInUserIdentifier(claims.get("loggedInUserIdentifier", String.class));
        protocol.setOrgCode(claims.get("orgCode", String.class));
        protocol.setTenantCode(claims.get("tenantCode", String.class));
        protocol.setSysFormCode(claims.get("sysFormCode", String.class));
        protocol.setPortalType(EnumPortalTypes.valueOf(claims.get("portalType", String.class)));
        return protocol;

    }
}

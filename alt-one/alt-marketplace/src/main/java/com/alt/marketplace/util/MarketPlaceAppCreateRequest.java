package com.alt.marketplace.util;

import org.springframework.web.multipart.MultipartFile;

import com.alt.datacarrier.marketplace.core.MarketplaceAppDetail;

public class MarketPlaceAppCreateRequest {

	private MultipartFile file;
	
	private MarketplaceAppDetail app;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public MarketplaceAppDetail getApp() {
		return app;
	}

	public void setApp(MarketplaceAppDetail app) {
		this.app = app;
	}


	
}

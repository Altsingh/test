package com.alt.marketplace.util;

import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.marketplace.core.MarketplaceAppDetail;

public abstract class MarketPlaceUtil {

	public static Protocol getMarketPlaceProtocol() {
		Protocol protocol = new Protocol();
		protocol.setDomain("marketplace");
		protocol.setOrgCode("SYSTEMORG");
		protocol.setUserCode("user002");
		return protocol;
	}

	public static boolean isValidateApp(MarketplaceAppDetail app) {
		if(app.getAppName()==null || app.getAppName().length()==0)
			return false;
		
		return true;
	}
}

package com.alt.marketplace.util;

import com.alt.datacarrier.marketplace.core.SubCategoryAppDetail;

import java.util.Comparator;

public class SubCategoryComparator implements Comparator<SubCategoryAppDetail> {

    @Override
    public int compare(SubCategoryAppDetail o1, SubCategoryAppDetail o2) {
    	if(o1.getSubCategory().getSequence() != null && o2.getSubCategory().getSequence()!= null) {
    		Integer sc1 = Integer.parseInt(o1.getSubCategory().getSequence());
    		Integer sc2 = Integer.parseInt(o2.getSubCategory().getSequence());
    		return Integer.compare(sc1, sc2);
    	}
		return 0;
    }

}

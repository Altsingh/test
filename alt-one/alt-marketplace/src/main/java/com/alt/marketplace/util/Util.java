package com.alt.marketplace.util;

public class Util {

    public static String replaceDoubleQuote(String fieldName) {
        if (fieldName != null) {
            fieldName = fieldName.replaceAll("\"", "");
        }
        return fieldName;
    }
}

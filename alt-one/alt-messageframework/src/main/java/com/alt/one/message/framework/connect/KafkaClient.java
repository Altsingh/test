package com.alt.one.message.framework.connect;

import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.alt.one.message.framework.consumer.ConsumerThread;

public class KafkaClient {

	private static final Logger logger = Logger.getLogger(KafkaClient.class.toString());

	public static void produce(String topicName, String input){
		Producer producer = KafkaManager.getProducer();
		ProducerRecord<String, String> rec = new ProducerRecord<String, String>(topicName, null, input);
		producer.send(rec, new Callback() {
            public void onCompletion(RecordMetadata metadata, Exception exception) {
                logger.info("Message sent to topic ->" + metadata.topic() +" stored at offset->" + metadata.offset());
            }
        });
	}
	
	public static void consume(String topicName, int startingOffset, Class<?> runnableClass) throws InterruptedException{
		KafkaConsumer<String,String> kafkaConsumer = KafkaManager.getConsumer();
		ExecutorService executorService = KafkaManager.geExecutorService();
		ConsumerThread consumerThread = new ConsumerThread(topicName, startingOffset, kafkaConsumer, runnableClass, executorService);
        consumerThread.start();
        consumerThread.join();
	}

}

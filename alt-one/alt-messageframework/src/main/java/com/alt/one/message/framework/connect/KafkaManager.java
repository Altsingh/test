package com.alt.one.message.framework.connect;

import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;

import org.apache.kafka.clients.producer.Producer;

import static com.alt.datacarrier.core.Constant.CONSTANT_COLON;

/**
 * The class <code>KafkaManager.java</code>
 *
 * @author sumeet.mahajan
 *
 * @createdOn June 13, 2017
 */
public class KafkaManager {
	
	private static String ip = "192.168.0.130";
    
	private static String port = "9092";
	
	private static String groupId = "1";
	
	private static Producer producer;
		
	private static KafkaConsumer<String,String> kafkaConsumer;
	
	private static ExecutorService executorService;

	public static ExecutorService geExecutorService(){
		if(executorService == null){
			executorService = Executors.newFixedThreadPool(50);
		}
		return executorService;
	}
	
	public static Producer getProducer(){
		if(producer == null){
			Properties configProperties = new Properties();
	        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,ip + CONSTANT_COLON + port);
	        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.ByteArraySerializer");
	        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer");

	        producer = new KafkaProducer(configProperties);			
		}
		return producer;
	}
	
	public static KafkaConsumer<String,String> getConsumer(){
		if(kafkaConsumer == null){
			Properties configProperties = new Properties();
	        configProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,ip + CONSTANT_COLON + port);
	        configProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.ByteArrayDeserializer");
	        configProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");
	        configProperties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
	        configProperties.put(ConsumerConfig.CLIENT_ID_CONFIG, "offset123");
	        configProperties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,false);
	        configProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");

	        kafkaConsumer = new KafkaConsumer<String, String>(configProperties);
		}
		return kafkaConsumer;
	}
	
}

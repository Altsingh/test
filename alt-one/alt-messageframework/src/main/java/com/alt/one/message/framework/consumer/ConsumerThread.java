package com.alt.one.message.framework.consumer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.WakeupException;

/**
 * The class <code>Producer.java</code>
 *
 * @author sumeet.mahajan
 *
 * @createdOn June 13, 2017
 */
public class ConsumerThread extends Thread{
	
	private Logger logger = Logger.getLogger(ConsumerThread.class.toString());

    private String topicName;
    
    private long startingOffset;
    
    private KafkaConsumer<String,String> kafkaConsumer;

    private Class<?> runnableClass;
    
    private ExecutorService executorService;
    
    public ConsumerThread(String topicName, long startingOffset, KafkaConsumer<String,String> kafkaConsumer, 
    		Class<?> runnableClass, ExecutorService executorService){
        this.topicName = topicName;
        this.startingOffset = startingOffset;
        this.kafkaConsumer = kafkaConsumer;
        this.runnableClass = runnableClass;
        this.executorService = executorService;
    }
    
    public void run() {
        kafkaConsumer.subscribe(Arrays.asList(topicName), new ConsumerRebalanceListener() {

        	public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        		logger.log(Level.INFO, Arrays.toString(partitions.toArray()) + " topic-partitions are revoked from this consumer\n");
            }

        	public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
        		logger.log(Level.INFO, Arrays.toString(partitions.toArray()) + " topic-partitions are assigned to this consumer\n");
                Iterator<TopicPartition> topicPartitionIterator = partitions.iterator();
                while(topicPartitionIterator.hasNext()){
                    TopicPartition topicPartition = topicPartitionIterator.next();
                    logger.log(Level.INFO,"Current offset is " + kafkaConsumer.position(topicPartition) + " committed offset is ->" + 
                    		kafkaConsumer.committed(topicPartition));
                    if(startingOffset == 0){
                        //Setting offset to beginning
                        kafkaConsumer.seekToBeginning(topicPartition);
                    }else if(startingOffset == -1){
                        //Setting it to the end
                        kafkaConsumer.seekToEnd(topicPartition);
                    }else if(startingOffset != -2){
                        //Resetting offset to startingOffset
                        kafkaConsumer.seek(topicPartition, startingOffset);
                    }
                }
            }
        });
        
        try {
            while (true) {
                ConsumerRecords<String, String> records = kafkaConsumer.poll(100);
                for (ConsumerRecord<String, String> record : records) {
                	try {
                		Runnable instance = (Runnable) runnableClass.getConstructor(String.class).newInstance(record.value());
						executorService.execute(instance);
					} catch (Exception e){
						logger.log(Level.SEVERE,"Exception executing runnable passed from client : " + runnableClass + "\n Exception : " + e); 
					}
                	kafkaConsumer.commitSync();
                }
            }
        }catch(WakeupException ex){
        	logger.log(Level.INFO,"Exception caught " + ex);
        }finally{
            logger.log(Level.INFO,"After closing KafkaConsumer");
        }
    }

	public KafkaConsumer<String,String> getKafkaConsumer(){
        return this.kafkaConsumer;
    }
    
}

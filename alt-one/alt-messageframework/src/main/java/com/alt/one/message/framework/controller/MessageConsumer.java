package com.alt.one.message.framework.controller;

import java.util.concurrent.ExecutionException;

import com.alt.one.message.framework.connect.KafkaClient;

public class MessageConsumer {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		KafkaClient.consume("testing", -2,MyRunnable.class);
	}
}

package com.alt.one.message.framework.controller;

import static com.alt.datacarrier.core.Constant.CONSTANT_COLON;
import static com.alt.datacarrier.core.ErrorConstants.CONSTANT_MESSAGE_WRITE_COMPLETE_MSG;
import static com.alt.datacarrier.core.ErrorConstants.CONSTANT_MESSAGE_WRITE_FAILURE_MSG;
import static com.alt.datacarrier.core.ErrorConstants.CONSTANT_RESPONSE_CODE_MESSAGE_WRITE_COMPLETE;
import static com.alt.datacarrier.core.ErrorConstants.CONSTANT_RESPONSE_CODE_MESSAGE_WRITE_FAILED;
import static com.alt.datacarrier.core.ErrorConstants.CONSTANT_STATUS_FAILED;
import static com.alt.datacarrier.core.ErrorConstants.CONSTANT_STATUS_SUCCESS;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alt.datacarrier.core.Response;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.alt.datacarrier.message.framework.common.KafkaMessage;
import com.alt.datacarrier.message.framework.common.MessageFrameworkResponse;
import com.alt.one.message.framework.connect.KafkaClient;

public class MessageProducer {

	private Logger logger = LoggerFactory.getLogger(MessageProducer.class);

	public static void main(String[] args){
		MessageProducer producer = new MessageProducer();
		List<KafkaMessage> messageList = new ArrayList<KafkaMessage>();
		KafkaMessage message = new KafkaMessage();
		message.setDocumentCode("abcd");
		message.setTopic("testing");
		message.setType(EnumRequestType.INSTANCE);
		messageList.add(message);
		producer.produce(messageList);
	}
	
	public Response<MessageFrameworkResponse> produce(List<KafkaMessage> messageList) {
		Response<MessageFrameworkResponse> response = new Response<MessageFrameworkResponse>();
		try{
			for(KafkaMessage message : messageList){
				KafkaClient.produce(message.getTopic(), message.getDocumentCode() + CONSTANT_COLON + message.getType().name()
						+ CONSTANT_COLON + message.getOperation().name());
			}
			response.setResponseCode(CONSTANT_RESPONSE_CODE_MESSAGE_WRITE_COMPLETE);
			response.setErrMsg(CONSTANT_MESSAGE_WRITE_COMPLETE_MSG);
			response.setResponseStatus(CONSTANT_STATUS_SUCCESS);
		}catch(Exception e){
			logger.error("Exception while producing message for kafka: " + e);
			response.setResponseCode(CONSTANT_RESPONSE_CODE_MESSAGE_WRITE_FAILED);
			response.setErrMsg(CONSTANT_MESSAGE_WRITE_FAILURE_MSG);
			response.setResponseStatus(CONSTANT_STATUS_FAILED);
		}
		return response;
	}
	
}

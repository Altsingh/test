package com.alt.one.message.framework.controller;

public class MyRunnable implements Runnable {
	 
	private String data;
	  
	public MyRunnable(String _data) {
		this.data = _data;
	}

	@Override
	public void run() {
		System.out.println(data);
	}
}

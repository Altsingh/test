package com.alt.search.common.web;

import com.alt.search.elastic.ElasticSearchIndexer;
import com.alt.search.service.index.ElasticIndexService;
import com.alt.search.service.index.IndexService;
import com.alt.search.service.search.SearchService;
import com.alt.search.solr.SolrManager;
import com.alt.search.utils.HttpUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = { "com.alt.search.*" })
public class AltSearchRun {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(AltSearchRun.class, args);
	}

	@Bean
	public IndexService IndexService() {
		return new IndexService();
	}

	@Bean
	public SearchService SearchService() {
		return new SearchService();
	}
	
	@Bean
	public SolrManager SolrManager() {
		return new SolrManager();
	}

	@Bean
	public ElasticSearchIndexer ElasticSearchIndexer() {
		return new ElasticSearchIndexer();
	}
	
	@Bean
	public ElasticIndexService ElasticIndexService() {
		return new ElasticIndexService();
	}
	
	
	@Bean
	public HttpUtil HttpUtil() {

		return new HttpUtil();
	}

}

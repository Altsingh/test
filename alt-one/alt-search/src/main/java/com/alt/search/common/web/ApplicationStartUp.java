package com.alt.search.common.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.alt.one.message.framework.connect.KafkaClient;
import com.alt.search.elastic.ElasticSearchIndexer;
import com.alt.search.service.index.ElasticIndexService;
import com.alt.search.service.index.IndexService;
import com.alt.search.solr.SolrManager;
import com.alt.search.solr.SolrRunnable;
import com.alt.search.utils.HttpUtil;

import ch.qos.logback.classic.Level;

@Component
public class ApplicationStartUp implements ApplicationListener<ApplicationReadyEvent> {
	
	private static final Logger logger = Logger.getLogger(ApplicationStartUp.class);

	@Value("${kernel.url}")
	String kernelReadUrl;
	
	@Value("${solr.url}")
	String solrUrl;
	
	@Autowired(required=true)
	ElasticIndexService elasticIndexService;
	
	@Autowired(required=true)
	IndexService indexService;
	
	@Autowired(required=true)
	HttpUtil httpUtil;	
	
	@Autowired(required=true)
	SolrManager solrManager;
	
	@Autowired(required=true)
	ElasticSearchIndexer elasticSearchIndexer;
	
	

	/**
	 * This event is executed as late as conceivably possible to indicate that
	 * the application is ready to service requests.
	 */
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		try {
			
    		ElasticSearchIndexer.initiateElasticClient(solrUrl);
    		
    		SolrRunnable.setHttpUtil(httpUtil);
			SolrRunnable.setSolrManager(solrManager);
			SolrRunnable.setKernelUrl(kernelReadUrl);
			SolrRunnable.setIndexService(indexService);
			SolrRunnable.setElasticSearchIndexer(elasticSearchIndexer);		
			SolrRunnable.setElasticIndexService(elasticIndexService);		

			KafkaClient.consume("testing", -2, SolrRunnable.class);
						
		} catch (Exception e) {
			logger.log(null, Level.ERROR,e);
		}

	}

}
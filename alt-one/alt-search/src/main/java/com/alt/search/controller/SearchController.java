package com.alt.search.controller;

import java.io.IOException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alt.datacarrier.search.SearchRequest;
import com.alt.datacarrier.search.SearchResponse;
import com.alt.search.service.index.IndexService;
import com.alt.search.service.search.SearchService;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * The class <code>SearchController.java</code>
 *
 * @author Navjot
 *
 * @createdOn May 30, 2017
 */

@Controller
public class SearchController {

	@Autowired(required = true)
	IndexService indexService;
	
	@Autowired(required = true)
	SearchService searchService;
	
//	@ResponseBody
//	@RequestMapping(value = "/alt-search/fetchDocfromKernel", method = RequestMethod.POST, consumes = "application/json")
//	public String fetchDocumentFromKernel(@RequestBody SearchDataInputRequest request)
//			throws JsonGenerationException, JsonMappingException, IOException {	
//     //   String response = indexService.indexDocumentInSolr(request);
//		return null;	
//	}	
    	
	@ResponseBody
	@RequestMapping(value = "/alt-search/searchData", method = RequestMethod.POST, consumes = "application/json")
	public String searchData(@RequestBody SearchRequest request)
			throws JsonGenerationException, JsonMappingException, IOException {	
		 
		SearchResponse searchResponse = searchService.searchDocumentInSolr(request);
		String response= new ObjectMapper().writeValueAsString(searchResponse);
		return response;	
	}	
	

}

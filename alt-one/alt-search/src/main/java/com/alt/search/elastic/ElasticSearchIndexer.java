package com.alt.search.elastic;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

public class ElasticSearchIndexer {
	private static final Logger logger = Logger.getLogger(ElasticSearchIndexer.class);

	private static TransportClient client = null;

	
	public static void initiateElasticClient(String elasticURL) {
		Settings settings = Settings.builder().put("cluster.name", "altonesearch").put("client.transport.sniff", true)
				.build();
		try {
			client = new PreBuiltTransportClient(settings)
					.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("192.168.2.198"), 9300));
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void indexDocument(Map<String,Object> documentMap, String index, String type) {
        
		IndexResponse resp = client.prepareIndex(index, type).setSource(documentMap).execute().actionGet();

	}
	
	

	public void addMapping(XContentBuilder builder, String index, String type) {
		PutMappingResponse response=client.admin().
                indices().
                preparePutMapping(index).
                setType(type).
                setSource(builder).
                execute().
                actionGet();

	}
}

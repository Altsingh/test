package com.alt.search.service.index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.solr.common.SolrInputDocument;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.springframework.stereotype.Service;

import com.alt.datacarrier.business.common.AltReadRequest;
import com.alt.datacarrier.business.common.AltReadResponse;
import com.alt.datacarrier.common.enumeration.EnumPortalTypes;
import com.alt.datacarrier.core.AttributeMeta;
import com.alt.datacarrier.core.ClassMeta;
import com.alt.datacarrier.core.Constant;
import com.alt.datacarrier.core.InstanceData;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.Request;
import com.alt.datacarrier.core.Response;
import com.alt.datacarrier.core.SingleAttributeValue;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.alt.datacarrier.kernel.common.KernelReadQueryResponse;
import com.alt.datacarrier.kernel.common.KernelStringQueryReadRequest;
import com.alt.search.elastic.ElasticSearchIndexer;
import com.alt.search.utils.HttpUtil;
import com.alt.search.utils.SearchConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ElasticIndexService {

	private static final Logger logger = Logger.getLogger(ElasticIndexService.class);

	private static HttpUtil httpUtil = null;

	private static ElasticSearchIndexer elasticSearchIndexer = null;

	private static String kernelUrl = "";

	/**
	 * @param request
	 * 
	 *            Fetches Document from Data Kernel and inserts in Solr
	 * 
	 */
	public String indexDocument(String request, HttpUtil _httpUtil, ElasticSearchIndexer _elasticSearchIndexer,
			String _kernelUrl) {
		Response<AltReadResponse> altResponse = null;
		String response = null;
		try {
			httpUtil = _httpUtil;
			elasticSearchIndexer = _elasticSearchIndexer;
			kernelUrl = _kernelUrl;
			String[] requestFields = request.split(":");
			String docId = requestFields[0];
			String docType = requestFields[1];
			String docOperation = requestFields[2];
			logger.info("Doc Type" + docType);
			if (docOperation.equals(EnumCRUDOperations.REMOVE.toString())) {

				deleteFromSolr(requestFields[0]);

			} else {
				altResponse = getDocumentFromKernel(docId, EnumRequestType.valueOf(docType));
				if (altResponse != null) {
					logger.info(" altResponse :    search service" + altResponse.getResponseCode());
					response = new ObjectMapper().writeValueAsString(altResponse);

					logger.info(" response :    search service" + response);
					if (docType.equals(EnumRequestType.CLASS.toString())) {
						insertClassMeta(altResponse, docId);
					} else if (docType.equals(EnumRequestType.INSTANCE.toString())) {

						insertInstanceData(altResponse);

					} else {
						insertClassMeta(altResponse, docId);
						insertInstanceData(altResponse);
					}
					if (docOperation.equals(EnumCRUDOperations.UPDATE.toString())) {
						List<String> refDocIdList = getReferencedDocIds(
								altResponse.getResponseData().getInstance().get(0).get(0).getClassCode());
						refDocIdList.forEach(refDocId -> {
							insertInstanceData(getDocumentFromKernel(refDocId, EnumRequestType.INSTANCE));

						});
					}
				}
			}
			response = "Success";

		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return response;
	}

	/**
	 * 
	 * @param docId
	 * @return
	 */
	private List<String> getReferencedDocIds(String docId) {

		List<String> idList = new ArrayList<>();
		String query = "SELECT a.id FROM `Alt` a unnest attributes c where c.`referenceClassCode`= \"";

		KernelReadQueryResponse responseKernel = getQueryResponse(query, docId);

		logger.info("responseKernel :: " + responseKernel);

		List<Map<String, Object>> mapList = responseKernel.getQueryResponse().get(0).getResponse();

		query = "SELECT instanceCode FROM `Alt` where type=\"INSTANCE\"  AND classCode= \"";
		for (Map<String, Object> map : mapList) {

			responseKernel = getQueryResponse(query, map.get("id").toString());
			List<Map<String, Object>> idMapList = responseKernel.getQueryResponse().get(0).getResponse();

			for (Map<String, Object> mapId : idMapList) {

				idList.add(mapId.get("instanceCode").toString());
			}

		}

		return idList;
	}

	/**
	 * @param docId,
	 *            type
	 * 
	 *            Fetches Document from Data Kernel
	 * 
	 */
	private Response<AltReadResponse> getDocumentFromKernel(String docId, EnumRequestType type) {
		AltReadRequest req = new AltReadRequest();
		Request<AltReadRequest> readRequest = new Request<AltReadRequest>();
		List<AltReadRequest> reqList = new ArrayList<AltReadRequest>();
		try {
			logger.info("Fetching document from NoSQL DB");
			readRequest.setProtocol(searchProtocol());
			req.setDocIdentifier(docId);
			req.setDepth(5);
			req.setLimit(10);
			req.setOffset(0);
			req.setType(type);
			reqList.add(req);
			readRequest.setRequestData(reqList);
			return httpUtil.docReadRequestToKernel(readRequest, kernelUrl);
		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return null;
	}

	public KernelReadQueryResponse getQueryResponse(String query, String docId) {

		Request<KernelStringQueryReadRequest> requestData = new Request<KernelStringQueryReadRequest>();
		List<KernelStringQueryReadRequest> requestList = new ArrayList<KernelStringQueryReadRequest>();
		KernelStringQueryReadRequest request = new KernelStringQueryReadRequest();
		String reqStr = query + docId + "\"";
		request.setRequest(reqStr);
		requestList.add(request);
		requestData.setRequestData(requestList);

		String jsonInString = "";
		try {
			jsonInString = new ObjectMapper().writeValueAsString(requestData);

		} catch (IOException e) {
			logger.error("Error occured:", e);
		}
		String url = "http://192.168.0.130:8080/alt-kernel/processN1QLQuery";
		String responseString = httpUtil.getDataFromUrl(url, jsonInString).toBlocking().first();

		KernelReadQueryResponse responseKernel = null;

		try {
			responseKernel = new ObjectMapper().readValue(responseString, new TypeReference<KernelReadQueryResponse>() {
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info(e);
		}
		return responseKernel;

	}

	/**
	 * 
	 * Returns Protocol Object for Search Request
	 * 
	 */

	public Protocol searchProtocol() {
		Protocol protocol = new Protocol();
		protocol.setLoggedInTimestamp(122344l);
		protocol.setLoggedInUserIdentifier("SearchUser");
		protocol.setOrgCode("123123");
		protocol.setPortalType(EnumPortalTypes.ADMIN);
		protocol.setUserCode("SearchUser");
		return protocol;
	}

	/**
	 * @param altResponse
	 * 
	 *            Inserts document in Solr
	 * 
	 * 
	 */
	public void insertInstanceData(Response<AltReadResponse> altResponse) {

		List<InstanceData> instanceDataList = altResponse.getResponseData().getInstance().get(0);

		for (InstanceData instanceData : instanceDataList) {
			Map<String, Object> doc = getDocMapForInstance(instanceData);
			logger.info("************* elastic Doc " + doc.toString());
			String type = instanceData.getClassCode().replaceAll("#", "-");
			elasticSearchIndexer.indexDocument(doc, "orgid", type);

		}

	}

	/**
	 * @param altResponse
	 * 
	 *            Inserts document in Solr
	 * 
	 * 
	 */
	public void insertClassMeta(Response<AltReadResponse> altResponse, String docId) {

		Map<String, ClassMeta> classMetaMap = altResponse.getResponseData().getMeta();
		ClassMeta classMeta = classMetaMap.get(docId);
		Map<String, Object> elasticDocMap = new HashMap<String, Object>();
		String orgId = "orgid"; // REPLACE it with orgId
		elasticDocMap.put(SearchConstants.ID, classMeta.getId());
		elasticDocMap.put(SearchConstants.CAS, classMeta.getCas());
		elasticDocMap.put(SearchConstants.CLASSCODE, classMeta.getClassCode());
		elasticDocMap.put(SearchConstants.NAME, classMeta.getName());
		elasticDocMap.put(SearchConstants.CREATEDBY, classMeta.getCreatedBy());
		elasticDocMap.put(SearchConstants.CREATEDDATE, classMeta.getCreatedDate());
		elasticDocMap.put(SearchConstants.MODIFIEDBY, classMeta.getModifiedBy());
		elasticDocMap.put(SearchConstants.MODIFIEDDATE, classMeta.getModifiedDate());
		elasticDocMap.put(SearchConstants.STATUS, classMeta.getStatus().toString());
		elasticDocMap.put(SearchConstants.TYPE, classMeta.getType().toString());
		elasticDocMap.put(SearchConstants.CUSTOM_ATTRIBUTE_ALLOWED, classMeta.getCustomAttributeAllowed());
		elasticDocMap.put(SearchConstants.INSTANCE_LIMITER, classMeta.getInstanceLimiter().toString());
		elasticDocMap.put(SearchConstants.INSTANCE_PREFIX, classMeta.getInstancePrefix());
		elasticDocMap.put(SearchConstants.LOCATION_SPECIFIC, classMeta.getLocationSpecific());
		elasticDocMap.put(SearchConstants.PACKAGE_CODES, classMeta.getPackageCodes());
		elasticDocMap.put(SearchConstants.PARAMETER_TYPE, classMeta.getParameterType());
		elasticDocMap.put(SearchConstants.REFERRED_BY, classMeta.getReferredBy());
		elasticDocMap.put(SearchConstants.SYSTEM_TYPE, classMeta.getSystemType());
		elasticDocMap.put(SearchConstants.UPDATION_UUID_LIST, classMeta.getUpdationUuidList());

		List<AttributeMeta> attributesMetaList = classMeta.getAttributes();
		Map<String, Object> elasticAttributeDocMap = new HashMap();
		List<Map<String, Object>> attributesDocList = new ArrayList();
		XContentBuilder builder = null;
		String type = classMeta.getId();
		type = type.replaceAll("#", "-");
		try {
			builder = XContentFactory.jsonBuilder().startObject().startObject(type).startObject("properties")				
					.startObject("classCode").field("type", "string").endObject()
					.startObject("createdDate").field("type", "long").endObject()
					.startObject("cas").field("type", "string").endObject()
					.startObject("createdBy").field("type", "string").endObject()
					.startObject("modifiedDate").field("type", "long").endObject()
					.startObject("modifiedBy").field("type", "string").endObject()
					.startObject("id").field("type", "string").endObject()
					.startObject("type").field("type", "string").endObject()
					.startObject("status").field("type", "string").endObject()
		            .startObject("attributes").field("type", "nested").startObject("properties");
			for (AttributeMeta attributeMeta : attributesMetaList) {
				elasticAttributeDocMap = new HashMap<String, Object>();
				elasticAttributeDocMap.put(SearchConstants.ID,
						classMeta.getId() + Constant.CONSTANT_UNDERSCORE + attributeMeta.getAttributeCode());
				elasticAttributeDocMap.put(SearchConstants.ATTRIBUTE_CODE, attributeMeta.getAttributeCode());
				elasticAttributeDocMap.put(SearchConstants.ATTRIBUTE_NAME, attributeMeta.getAttributeName());
				elasticAttributeDocMap.put(SearchConstants.DATA_TYPE, attributeMeta.getDataType().toString());
				elasticAttributeDocMap.put(SearchConstants.DEFAULT_VALUE, attributeMeta.getDefaultValue());
				elasticAttributeDocMap.put(SearchConstants.DISPLAY_SEQUENCE, attributeMeta.getDisplaySequence());
				elasticAttributeDocMap.put(SearchConstants.DISPLAY_UNIT, attributeMeta.getDisplayUnit());
				elasticAttributeDocMap.put(SearchConstants.INDEXABLE, attributeMeta.getIndexable());
				elasticAttributeDocMap.put(SearchConstants.MANDATORY, attributeMeta.getMandatory());
				elasticAttributeDocMap.put(SearchConstants.PLACEHOLDER_TYPE, attributeMeta.getPlaceholderType());
				elasticAttributeDocMap.put(SearchConstants.REFERENCE_CLASS_CODE, attributeMeta.getReferenceClassCode());
				elasticAttributeDocMap.put(SearchConstants.SEARCHABLE, attributeMeta.getSearchable());
				elasticAttributeDocMap.put(SearchConstants.STATUS, attributeMeta.getStatus().toString());
				elasticAttributeDocMap.put(SearchConstants.UNIQUE, attributeMeta.getUnique());
				String store = "yes";
				String index = "not_analyzed";
				String datatype = attributeMeta.getDataType().toString().toLowerCase();
				if (attributeMeta.getSearchable()) {
					index = "analyzed";
					store = "no";
				}
				if (datatype.equals("reference")) {
					datatype = "string";
					builder.startObject(attributeMeta.getAttributeName()).field("type", datatype).endObject();
				} else {
					builder.startObject(attributeMeta.getAttributeName()).field("type", datatype).field("store", store)
							.field("index", index).endObject();
				}
				// elasticAttributeDocMap.put(attributeMeta.,
				// attributeMeta.getValidator()); //AttributeValidator insert as
				// a
				// ChildDoc

				attributesDocList.add(elasticAttributeDocMap);
			}
			builder.endObject().endObject().endObject().endObject().endObject();
			logger.info("STRINGGGGGGG  " + builder.string());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		elasticDocMap.put("attributes", attributesDocList);
		elasticSearchIndexer.addMapping(builder, orgId, type);
		elasticSearchIndexer.indexDocument(elasticDocMap, orgId, "classmeta");

	}

	/**
	 * @param
	 * 
	 * 			Returns
	 *            SolrInputDocument
	 */
	private Map<String, Object> getDocMapForInstance(InstanceData instanceData) {
		Map<String, Object> elasticDocMap = new HashMap<String, Object>();

		elasticDocMap.put(SearchConstants.ID, instanceData.getDocIdentifier());
		elasticDocMap.put(SearchConstants.CAS, instanceData.getCas());
		elasticDocMap.put(SearchConstants.CLASSCODE, instanceData.getClassCode());
		elasticDocMap.put(SearchConstants.CREATEDBY, instanceData.getCreatedBy());
		elasticDocMap.put(SearchConstants.CREATEDDATE, instanceData.getCreatedDate());
		elasticDocMap.put(SearchConstants.MODIFIEDBY, instanceData.getModifiedBy());
		elasticDocMap.put(SearchConstants.MODIFIEDDATE, instanceData.getModifiedDate());
		elasticDocMap.put(SearchConstants.STATUS, instanceData.getStatus());
		elasticDocMap.put(SearchConstants.TYPE, instanceData.getType());

		try {
			elasticDocMap.put("attributes", getChildDocForAttributes(instanceData, "", ""));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return elasticDocMap;

	}

	// private List<SolrInputDocument> getChildDocForAttributes(InstanceData
	// instanceData, String solrDocId,
	// String parentId) {
	//
	// HashMap<String, Boolean> attributeSearchableMap =
	// getAttributesSearchableFlag(instanceData.getClassCode());
	//
	// List<SolrInputDocument> attributesChildDoc = new
	// ArrayList<SolrInputDocument>();
	//
	// SolrInputDocument attributesDoc = null;
	//
	// Map<String, SingleAttributeValue> attributesMap =
	// instanceData.getAttrValues();
	//
	// String thisSolrDocId = solrDocId;
	// String thisParentId = parentId;
	//
	// if (solrDocId.equals("")) {
	// solrDocId = instanceData.getClassCode();
	// parentId = instanceData.getDocIdentifier();
	// } else {
	// solrDocId = solrDocId + Constant.CONSTANT_UNDERSCORE +
	// instanceData.getClassCode();
	// parentId = parentId + Constant.CONSTANT_PERIOD +
	// instanceData.getDocIdentifier();
	// }
	//
	// for (Map.Entry<String, SingleAttributeValue> attribute :
	// attributesMap.entrySet()) {
	// logger.info("key " + attribute.getKey() + "value: " +
	// attribute.getValue());
	//
	// attributesDoc = new SolrInputDocument();
	// if (thisSolrDocId.equals("")) {
	// attributesDoc.addField("id", instanceData.getClassCode() +
	// Constant.CONSTANT_UNDERSCORE
	// + attribute.getKey() + Constant.CONSTANT_UNDERSCORE +
	// instanceData.getDocIdentifier());
	// attributesDoc.addField("parentId", parentId);
	// } else {
	// attributesDoc.addField("id",
	// thisSolrDocId + Constant.CONSTANT_UNDERSCORE +
	// instanceData.getClassCode()
	// + Constant.CONSTANT_UNDERSCORE + attribute.getKey() +
	// Constant.CONSTANT_UNDERSCORE
	// + instanceData.getDocIdentifier());
	// attributesDoc.addField("parentId",
	// thisParentId + Constant.CONSTANT_PERIOD +
	// instanceData.getDocIdentifier());
	// }
	//
	// if (attribute.getValue().isReference()) {
	//
	// List<InstanceData> refInstanceDataList =
	// attribute.getValue().getReferenceValue();
	// for (InstanceData refInstanceData : refInstanceDataList) {
	// List<SolrInputDocument> referenceDoc =
	// getChildDocForAttributes(refInstanceData, solrDocId,
	// parentId);
	// attributesDoc.addChildDocuments(referenceDoc);
	//
	// }
	//
	// } else {
	// if (attributeSearchableMap.get(attribute.getKey()) == null
	// || !attributeSearchableMap.get(attribute.getKey())) {
	// attributesDoc.addField(attribute.getKey() +
	// SearchConstants.NOTSEARCHABLE,
	// attribute.getValue().getValue());
	//
	// } else {
	// attributesDoc.addField(attribute.getKey() + SearchConstants.SEARCHABLE_S,
	// attribute.getValue().getValue());
	//
	// }
	// }
	// attributesChildDoc.add(attributesDoc);
	//
	// }
	// return attributesChildDoc;
	// }

	private Map<String,Object> getChildDocForAttributes(InstanceData instanceData, String solrDocId, String parentId) throws IOException {

		Map<String, Object> attributesDoc = new HashMap<String, Object>();
		Map<String, SingleAttributeValue> attributesMap = instanceData.getAttributes();

		for (Map.Entry<String, SingleAttributeValue> attribute : attributesMap.entrySet()) {
			logger.info("key " + attribute.getKey() + "value:  " + attribute.getValue());

			
			// attributesDoc.put("id",
			// instanceData.getClassCode()
			// + Constant.CONSTANT_UNDERSCORE + attribute.getKey() +
			// Constant.CONSTANT_UNDERSCORE
			// + instanceData.getDocIdentifier());
			// check if id is needed for Elastic nested document

			if (attribute.getValue().isReference()) {

				List<InstanceData> refInstanceDataList = attribute.getValue().getReferenceValue();
				List<String> idList = new ArrayList<String>();
				for (InstanceData refInstanceData : refInstanceDataList) {

					idList.add(refInstanceData.getDocIdentifier());
					
				}

				attributesDoc.put(attribute.getKey(), idList.toString());
				attributesDoc.put(attribute.getKey()+"_refBool", true);
			} else {
				attributesDoc.put(attribute.getKey(), attribute.getValue().getValue());

			}
			

		}
		return attributesDoc;
	}

	/**
	 * @param classCode
	 * 
	 *            Returns Map of attributeCode and searchable flag
	 * 
	 */
	private HashMap<String, Boolean> getAttributesSearchableFlag(String classCode) {
		Response<AltReadResponse> altResponse = null;
		HashMap<String, Boolean> searchableMap = new HashMap<String, Boolean>();
		try {
			altResponse = getDocumentFromKernel(classCode, EnumRequestType.CLASS);
			if (altResponse != null) {
				List<AttributeMeta> attributeMetaList = altResponse.getResponseData().getMeta().get(classCode)
						.getAttributes();
				for (AttributeMeta attributeMeta : attributeMetaList) {
					searchableMap.put(attributeMeta.getAttributeCode(), attributeMeta.getSearchable());
				}
			}
		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return searchableMap;
	}

	private void deleteFromSolr(String docId) {
		// solrManager.deleteFromSolr(docId);
	}
}
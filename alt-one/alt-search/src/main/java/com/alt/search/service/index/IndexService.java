package com.alt.search.service.index;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.stereotype.Service;

import com.alt.datacarrier.business.common.AltReadRequest;
import com.alt.datacarrier.business.common.AltReadResponse;
import com.alt.datacarrier.common.enumeration.EnumPortalTypes;
import com.alt.datacarrier.core.AttributeMeta;
import com.alt.datacarrier.core.ClassMeta;
import com.alt.datacarrier.core.Constant;
import com.alt.datacarrier.core.InstanceData;
import com.alt.datacarrier.core.Protocol;
import com.alt.datacarrier.core.Request;
import com.alt.datacarrier.core.Response;
import com.alt.datacarrier.core.SingleAttributeValue;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumCRUDOperations;
import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;
import com.alt.datacarrier.kernel.common.KernelReadQueryResponse;
import com.alt.datacarrier.kernel.common.KernelStringQueryReadRequest;
import com.alt.search.solr.SolrManager;
import com.alt.search.utils.HttpUtil;
import com.alt.search.utils.SearchConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class IndexService {

	private static final Logger logger = Logger.getLogger(IndexService.class);

	private static HttpUtil httpUtil = null;

	private static SolrManager solrManager = null;

	private static String kernelUrl = "";

	/**
	 * @param request
	 * 
	 *            Fetches Document from Data Kernel and inserts in Solr
	 * 
	 */
	public String indexDocumentInSolr(String request, HttpUtil _httpUtil, SolrManager _solrManager, String _kernelUrl) {
		Response<AltReadResponse> altResponse = null;
		String response = null;
		try {
			httpUtil = _httpUtil;
			solrManager = _solrManager;
			kernelUrl = _kernelUrl;
			String[] requestFields = request.split(":");
			String docId = requestFields[0];
			String docType = requestFields[1];
			String docOperation = requestFields[2];
			logger.info("Doc Type" + docType);
			if (docOperation.equals(EnumCRUDOperations.REMOVE.toString())) {

				deleteFromSolr(requestFields[0]);

			} else {
				altResponse = getDocumentFromKernel(docId, EnumRequestType.valueOf(docType));
				if (altResponse != null) {
					logger.info(" altResponse :    search service" + altResponse.getResponseCode());
					response = new ObjectMapper().writeValueAsString(altResponse);

					logger.info(" response :    search service" + response);
					if (docType.equals(EnumRequestType.CLASS.toString())) {
						insertClassMetaInSolr(altResponse, docId);
					} else if (docType.equals(EnumRequestType.INSTANCE.toString())) {

						insertInstanceDataInSolr(altResponse);

					} else {
						insertClassMetaInSolr(altResponse, docId);
						insertInstanceDataInSolr(altResponse);
					}
					if (docOperation.equals(EnumCRUDOperations.UPDATE.toString())) {
						List<String> refDocIdList = getReferencedDocIds(
								altResponse.getResponseData().getInstance().get(0).get(0).getClassCode());
						refDocIdList.forEach(refDocId -> {
							insertInstanceDataInSolr(getDocumentFromKernel(refDocId, EnumRequestType.INSTANCE));

						});
					}
				}
			}
			response = "Success";

		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return response;
	}

	/**
	 * 
	 * @param docId
	 * @return
	 */
	private List<String> getReferencedDocIds(String docId) {

		List<String> idList = new ArrayList<>();
		String query = "SELECT a.id FROM `Alt` a unnest attributes c where c.`referenceClassCode`= \"";

		KernelReadQueryResponse responseKernel = getQueryResponse(query, docId);

		logger.info("responseKernel :: " + responseKernel);

		List<Map<String, Object>> mapList = responseKernel.getQueryResponse().get(0).getResponse();

		query = "SELECT instanceCode FROM `Alt` where type=\"INSTANCE\"  AND classCode= \"";
		for (Map<String, Object> map : mapList) {

			responseKernel = getQueryResponse(query, map.get("id").toString());
			List<Map<String, Object>> idMapList = responseKernel.getQueryResponse().get(0).getResponse();

			for (Map<String, Object> mapId : idMapList) {

				idList.add(mapId.get("instanceCode").toString());
			}

		}

		return idList;
	}
	
	/**
	 * @param docId,
	 *            type
	 * 
	 *            Fetches Document from Data Kernel
	 * 
	 */
	private Response<AltReadResponse> getDocumentFromKernel(String docId, EnumRequestType type) {
		AltReadRequest req = new AltReadRequest();
		Request<AltReadRequest> readRequest = new Request<AltReadRequest>();
		List<AltReadRequest> reqList = new ArrayList<AltReadRequest>();
		try {
			logger.info("Fetching document from NoSQL DB");
			readRequest.setProtocol(searchProtocol());
			req.setDocIdentifier(docId);
			req.setDepth(5);
			req.setLimit(10);
			req.setOffset(0);
			req.setType(type);
			reqList.add(req);
			readRequest.setRequestData(reqList);
			return httpUtil.docReadRequestToKernel(readRequest, kernelUrl);
		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return null;
	}
	
	public KernelReadQueryResponse getQueryResponse(String query, String docId) {

		Request<KernelStringQueryReadRequest> requestData = new Request<KernelStringQueryReadRequest>();
		List<KernelStringQueryReadRequest> requestList = new ArrayList<KernelStringQueryReadRequest>();
		KernelStringQueryReadRequest request = new KernelStringQueryReadRequest();
		String reqStr = query + docId + "\"";
		request.setRequest(reqStr);
		requestList.add(request);
		requestData.setRequestData(requestList);

		String jsonInString = "";
		try {
			jsonInString = new ObjectMapper().writeValueAsString(requestData);

		} catch (IOException e) {
			logger.error("Error occured:", e);
		}
		String url = "http://192.168.0.130:8080/alt-kernel/processN1QLQuery";
		String responseString = httpUtil.getDataFromUrl(url, jsonInString).toBlocking().first();

		KernelReadQueryResponse responseKernel = null;

		try {
			responseKernel = new ObjectMapper().readValue(responseString, new TypeReference<KernelReadQueryResponse>() {
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info(e);
		}
		return responseKernel;

	}

	/**
	 * 
	 * Returns Protocol Object for Search Request
	 * 
	 */

	public Protocol searchProtocol() {
		Protocol protocol = new Protocol();
		protocol.setLoggedInTimestamp(122344l);
		protocol.setLoggedInUserIdentifier("SearchUser");
		protocol.setOrgCode("123123");
		protocol.setPortalType(EnumPortalTypes.ADMIN);
		protocol.setUserCode("SearchUser");
		return protocol;
	}

	/**
	 * @param altResponse
	 * 
	 *            Inserts document in Solr
	 * 
	 * 
	 */
	public void insertInstanceDataInSolr(Response<AltReadResponse> altResponse) {

		List<InstanceData> instanceDataList = altResponse.getResponseData().getInstance().get(0);

		for (InstanceData instanceData : instanceDataList) {
			SolrInputDocument solrDoc = getSolrDocument(instanceData);
			logger.info("************* solrDoc " + solrDoc);
			solrManager.saveDocumentInSolr(solrDoc);

		}

	}

	/**
	 * @param altResponse
	 * 
	 *            Inserts document in Solr
	 * 
	 * 
	 */
	public void insertClassMetaInSolr(Response<AltReadResponse> altResponse, String docId) {

		Map<String, ClassMeta> classMetaMap = altResponse.getResponseData().getMeta();
		ClassMeta classMeta = classMetaMap.get(docId);
		SolrInputDocument solrDoc = new SolrInputDocument();
		solrDoc.addField(SearchConstants.ID, classMeta.getId());
		solrDoc.addField(SearchConstants.CAS, classMeta.getCas());
		solrDoc.addField(SearchConstants.CLASSCODE, classMeta.getClassCode());
		solrDoc.addField(SearchConstants.NAME, classMeta.getName());
		solrDoc.addField(SearchConstants.CREATEDBY, classMeta.getCreatedBy());
		solrDoc.addField(SearchConstants.CREATEDDATE, classMeta.getCreatedDate());
		solrDoc.addField(SearchConstants.MODIFIEDBY, classMeta.getModifiedBy());
		solrDoc.addField(SearchConstants.MODIFIEDDATE, classMeta.getModifiedDate());
		solrDoc.addField(SearchConstants.STATUS, classMeta.getStatus().toString());
		solrDoc.addField(SearchConstants.TYPE, classMeta.getType().toString());
		solrDoc.addField(SearchConstants.CUSTOM_ATTRIBUTE_ALLOWED, classMeta.getCustomAttributeAllowed());
		solrDoc.addField(SearchConstants.INSTANCE_LIMITER, classMeta.getInstanceLimiter().toString());
		solrDoc.addField(SearchConstants.INSTANCE_PREFIX, classMeta.getInstancePrefix());
		solrDoc.addField(SearchConstants.LOCATION_SPECIFIC, classMeta.getLocationSpecific());
		solrDoc.addField(SearchConstants.PACKAGE_CODES, classMeta.getPackageCodes());
		solrDoc.addField(SearchConstants.PARAMETER_TYPE, classMeta.getParameterType());
		solrDoc.addField(SearchConstants.REFERRED_BY, classMeta.getReferredBy());
		solrDoc.addField(SearchConstants.SYSTEM_TYPE, classMeta.getSystemType());
		solrDoc.addField(SearchConstants.UPDATION_UUID_LIST, classMeta.getUpdationUuidList());

		List<AttributeMeta> attributesMetaList = classMeta.getAttributes();
		List<SolrInputDocument> attributesChildDoc = new ArrayList<SolrInputDocument>();
		SolrInputDocument solrAttributeDoc = null;

		for (AttributeMeta attributeMeta : attributesMetaList) {

			solrAttributeDoc = new SolrInputDocument();
			solrAttributeDoc.addField(SearchConstants.ID,
					classMeta.getId() + Constant.CONSTANT_UNDERSCORE + attributeMeta.getAttributeCode());
			solrAttributeDoc.addField(SearchConstants.ATTRIBUTE_CODE, attributeMeta.getAttributeCode());
			solrAttributeDoc.addField(SearchConstants.ATTRIBUTE_NAME, attributeMeta.getAttributeName());
			solrAttributeDoc.addField(SearchConstants.DATA_TYPE, attributeMeta.getDataType().toString());
			solrAttributeDoc.addField(SearchConstants.DEFAULT_VALUE, attributeMeta.getDefaultValue());
			solrAttributeDoc.addField(SearchConstants.DISPLAY_SEQUENCE, attributeMeta.getDisplaySequence());
			solrAttributeDoc.addField(SearchConstants.DISPLAY_UNIT, attributeMeta.getDisplayUnit());
			solrAttributeDoc.addField(SearchConstants.INDEXABLE, attributeMeta.getIndexable());
			solrAttributeDoc.addField(SearchConstants.MANDATORY, attributeMeta.getMandatory());
			solrAttributeDoc.addField(SearchConstants.PLACEHOLDER_TYPE, attributeMeta.getPlaceholderType());
			solrAttributeDoc.addField(SearchConstants.REFERENCE_CLASS_CODE, attributeMeta.getReferenceClassCode());
			solrAttributeDoc.addField(SearchConstants.SEARCHABLE, attributeMeta.getSearchable());
			solrAttributeDoc.addField(SearchConstants.STATUS, attributeMeta.getStatus().toString());
			solrAttributeDoc.addField(SearchConstants.UNIQUE, attributeMeta.getUnique());
			// solrAttributeDoc.addField(attributeMeta.,
			// attributeMeta.getValidator()); //AttributeValidator insert as a
			// ChildDoc
			attributesChildDoc.add(solrAttributeDoc);
		}
		solrDoc.addChildDocuments(attributesChildDoc);
		solrManager.saveDocumentInSolr(solrDoc);

	}

	

	/**
	 * @param
	 * 
	 * 			Returns
	 *            SolrInputDocument
	 */
	private SolrInputDocument getSolrDocument(InstanceData instanceData) {
		SolrInputDocument solrDoc = new SolrInputDocument();

		solrDoc.addField(SearchConstants.ID, instanceData.getDocIdentifier());
		solrDoc.addField(SearchConstants.CAS, instanceData.getCas());
		solrDoc.addField(SearchConstants.CLASSCODE, instanceData.getClassCode());
		solrDoc.addField(SearchConstants.CREATEDBY, instanceData.getCreatedBy());
		solrDoc.addField(SearchConstants.CREATEDDATE, instanceData.getCreatedDate());
		solrDoc.addField(SearchConstants.MODIFIEDBY, instanceData.getModifiedBy());
		solrDoc.addField(SearchConstants.MODIFIEDDATE, instanceData.getModifiedDate());
		solrDoc.addField(SearchConstants.STATUS, instanceData.getStatus());
		solrDoc.addField(SearchConstants.TYPE, instanceData.getType());

		solrDoc.addChildDocuments(getSolrChildDocForAttributes(instanceData,"",""));

		return solrDoc;

	}

	private List<SolrInputDocument> getSolrChildDocForAttributes(InstanceData instanceData,String solrDocId,  String parentId) {

		HashMap<String, Boolean> attributeSearchableMap = getAttributesSearchableFlag(instanceData.getClassCode());
		
		List<SolrInputDocument> attributesChildDoc = new ArrayList<SolrInputDocument>();

		SolrInputDocument attributesDoc = null;

		Map<String, SingleAttributeValue> attributesMap = instanceData.getAttributes();
		
		String thisSolrDocId = solrDocId;
		String thisParentId = parentId;
		
		if(solrDocId.equals("")){
			solrDocId = instanceData.getClassCode();
		    parentId =  instanceData.getDocIdentifier();
		}
		else{			
			solrDocId = solrDocId + Constant.CONSTANT_UNDERSCORE +instanceData.getClassCode();	
			parentId = parentId+ Constant.CONSTANT_PERIOD +  instanceData.getDocIdentifier();			
		}	

		for (Map.Entry<String, SingleAttributeValue> attribute : attributesMap.entrySet()) {
			logger.info("key " + attribute.getKey() + "value:  " + attribute.getValue());
			
			attributesDoc = new SolrInputDocument();				
			if(thisSolrDocId.equals("")){
			    attributesDoc.addField("id", instanceData.getClassCode() + Constant.CONSTANT_UNDERSCORE + attribute.getKey()+ Constant.CONSTANT_UNDERSCORE + instanceData.getDocIdentifier());
				attributesDoc.addField("parentId", parentId);	
			}
			else{
				attributesDoc.addField("id",
						thisSolrDocId+ Constant.CONSTANT_UNDERSCORE +instanceData.getClassCode() + Constant.CONSTANT_UNDERSCORE + attribute.getKey()+ Constant.CONSTANT_UNDERSCORE + instanceData.getDocIdentifier());
				attributesDoc.addField("parentId", thisParentId+ Constant.CONSTANT_PERIOD + instanceData.getDocIdentifier());			
			}	
		
						
			if (attribute.getValue().isReference()) {

				List<InstanceData> refInstanceDataList = attribute.getValue().getReferenceValue();
				for (InstanceData refInstanceData : refInstanceDataList) {
					List<SolrInputDocument> referenceDoc = getSolrChildDocForAttributes(refInstanceData, solrDocId, parentId);
					attributesDoc.addChildDocuments(referenceDoc);

				}

			} else {
				if (attributeSearchableMap.get(attribute.getKey()) == null
						|| !attributeSearchableMap.get(attribute.getKey())) {
					attributesDoc.addField(attribute.getKey() + SearchConstants.NOTSEARCHABLE,
							attribute.getValue().getValue());

				} else {
					attributesDoc.addField(attribute.getKey() + SearchConstants.SEARCHABLE_S,
							attribute.getValue().getValue());

				}
			}
			attributesChildDoc.add(attributesDoc);

		}
		return attributesChildDoc;
	}
	/**
	 * @param classCode
	 * 
	 *            Returns Map of attributeCode and searchable flag
	 * 
	 */
	private HashMap<String, Boolean> getAttributesSearchableFlag(String classCode) {
		Response<AltReadResponse> altResponse = null;
		HashMap<String, Boolean> searchableMap = new HashMap<String, Boolean>();
		try {
			altResponse = getDocumentFromKernel(classCode, EnumRequestType.CLASS);
			if (altResponse != null) {
				List<AttributeMeta> attributeMetaList = altResponse.getResponseData().getMeta().get(classCode)
						.getAttributes();
				for (AttributeMeta attributeMeta : attributeMetaList) {
					searchableMap.put(attributeMeta.getAttributeCode(), attributeMeta.getSearchable());
				}
			}
		} catch (Exception ex) {
			logger.info(ex.getMessage());
		}
		return searchableMap;
	}

	private void deleteFromSolr(String docId) {
		solrManager.deleteFromSolr(docId);
	}
}
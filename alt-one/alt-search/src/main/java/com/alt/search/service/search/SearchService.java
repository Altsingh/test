package com.alt.search.service.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.alt.datacarrier.core.Constant;
import com.alt.datacarrier.search.SearchRequest;
import com.alt.datacarrier.search.SearchResponse;
import com.alt.search.solr.SolrManager;
import com.alt.search.utils.SearchConstants;

@Service
public class SearchService {

	@Autowired(required = true)
	SolrManager solrManager;

	public SearchResponse searchDocumentInSolr(SearchRequest request) {

		String queryFields = StringUtils.collectionToDelimitedString(request.getQueryFields(),
				SearchConstants.BLANK_SPACE, "", SearchConstants.DYNAMIC_SEARCHABLE_ATTRIBUTE);
		
		List<String> filterQueryList = new ArrayList<String>();
		List<Map<String, String>> filterQueryMapList = request.getFilterQuery();
		for (Map<String, String> filerQueryMap : filterQueryMapList) {

			for (Map.Entry<String, String> entry : filerQueryMap.entrySet()) {
				filterQueryList.add(entry.getKey() + Constant.CONSTANT_COLON + entry.getValue());
			}

		}
		return solrManager.searchDocument(request.getSearchString(), queryFields, filterQueryList,request);

	}

}

package com.alt.search.solr;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.json.JSONArray;
import org.json.JSONObject;
import com.alt.datacarrier.core.Constant;
import com.alt.datacarrier.search.SearchConstants.EnumSearchRequestType;
import com.alt.datacarrier.search.SearchRequest;
import com.alt.datacarrier.search.SearchResponse;
import com.alt.search.utils.SearchConstants;

@SuppressWarnings("deprecation")
public class SolrManager {

	private static SolrClient solrClient = null;

	private static final Logger logger = Logger.getLogger(SolrManager.class);

	public static void initiateSolrClient(String solrURL) {
		solrClient = new HttpSolrClient(solrURL);
	}

	/**
	 * Index Document in Solr
	 * 
	 * @param doc
	 */
	public void saveDocumentInSolr(SolrInputDocument doc) {

		try {
			solrClient.add(doc);
			solrClient.commit();
		} catch (SolrServerException | IOException e) {
			logger.info("Error while indexing document in Solr: "+e);
		}
	}

	/**
	 * Search Document in Solr on basis of search string, filter data, index
	 * 
	 * @param searchString
	 * @param queryFields
	 * @param filterQueryList
	 * @param start
	 * @param rows
	 * @return
	 */
	public SearchResponse searchDocument(String searchString, String queryFields, List<String> filterQueryList,
			SearchRequest request) {
		SolrQuery query = new SolrQuery();
		SearchResponse searchResponse = new SearchResponse();
		query.setQuery(searchString);
		query.set(SearchConstants.DEF_TYPE, SearchConstants.DISMAX);
		query.set(SearchConstants.ALTERNATE_QUERY, SearchConstants.ALTERNATE_QUERY_VALUE);
		
	    if(EnumSearchRequestType.GLOBAL.equals(request.getSearchRequestType())){
			query.set(SearchConstants.QUERY_FIELD, SearchConstants.GLOBAL_QUERY_FIELD);
		}	
	    else{
	    	query.set(SearchConstants.QUERY_FIELD, queryFields);
		}
		
		for (String filterQuery : filterQueryList) {
			query.addFilterQuery(filterQuery);
		}
		query.setStart(request.getStart());
		query.setRows(request.getRows());
	
		QueryResponse response = null;
		SolrDocumentList docs = null;
		JSONObject resultJSON = new JSONObject();
		JSONObject jsonDoc = new JSONObject();
		JSONArray jsonDocArray = new JSONArray();
		try {
			response = solrClient.query(query);
			docs = response.getResults();
			resultJSON.put(SearchConstants.NUMBER_OF_DOCS_FOUND, docs.getNumFound());
			resultJSON.put(SearchConstants.START, docs.getStart());
			for (SolrDocument solrDoc : docs) {
				Collection<String> solrDocFields = solrDoc.getFieldNames();
				jsonDoc = new JSONObject();
				for (String field : solrDocFields) {
					String value = solrDoc.get(field).toString();
					if (field.contains(SearchConstants.SEARCHABLE_S)) {
						
						jsonDoc.put(field.substring(0, field.indexOf(SearchConstants.SEARCHABLE_S)), value);
					}
					else if(field.contains(SearchConstants.NOTSEARCHABLE)){
						jsonDoc.put(field.substring(0, field.indexOf(SearchConstants.NOTSEARCHABLE)),value);
					}
					else{
						jsonDoc.put(field,value);
					}
				}
				jsonDocArray.put(jsonDoc);
				resultJSON.put("docs", jsonDocArray);
			}
			searchResponse.setResponseCode(200);
			searchResponse.setResponseMessage(Constant.CONSTANT_SUCCESS);
			searchResponse.setResult(resultJSON.toString());

		} catch (SolrServerException | IOException e) {
			logger.info("Error occured while fetching document from Solr: " + e);
			searchResponse.setResponseCode(300);
			searchResponse.setResponseMessage("Failed");
		}
		return searchResponse;
	}

	/**
	 * Delete document from Solr by passing Document Id
	 * 
	 * @param docId
	 */
	public void deleteFromSolr(String docId) {
		try {
			solrClient.deleteById(docId);
		} catch (SolrServerException | IOException e) {
			logger.info("Error occured while deleting document in Solr: " + e);
		}

	}

	/**
	 * Closes Solr Connection
	 */
	public void destroy() {
		try {
			solrClient.close();
		} catch (IOException e) {
			logger.info(e);
		}
	}
}

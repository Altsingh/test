package com.alt.search.solr;

import org.apache.log4j.Logger;

import com.alt.search.elastic.ElasticSearchIndexer;
import com.alt.search.service.index.ElasticIndexService;
import com.alt.search.service.index.IndexService;
import com.alt.search.utils.HttpUtil;


public class SolrRunnable implements Runnable{
	
	private String data;
	private static IndexService indexService;
	private static HttpUtil httpUtil;
	private static SolrManager solrManager;
	private static ElasticSearchIndexer elasticSearchIndexer;
	private static ElasticIndexService elasticIndexService;
	private static String kernelUrl;	
	private static final Logger logger = Logger.getLogger(SolrRunnable.class);
	
	public SolrRunnable(String _data) {		
		this.data = _data;
	}
	
	public static IndexService getIndexService() {
		return indexService;
	}

	public static void setIndexService(IndexService indexService) {
		SolrRunnable.indexService = indexService;
	}
	
	public static HttpUtil getHttpUtil() {
		return httpUtil;
	}
	
	public static void setHttpUtil(HttpUtil httpUtil) {
		SolrRunnable.httpUtil = httpUtil;
	}
	
	public static SolrManager getSolrManager() {
		return solrManager;
	}
	
	public static void setSolrManager(SolrManager solrManager) {
		SolrRunnable.solrManager = solrManager;
	}

	public static String getKernelUrl() {
		return kernelUrl;
	}

	public static void setKernelUrl(String kernelUrl) {
		SolrRunnable.kernelUrl = kernelUrl;
	}	
	
	public static ElasticSearchIndexer getElasticSearchIndexer() {
		return elasticSearchIndexer;
	}

	public static void setElasticSearchIndexer(ElasticSearchIndexer elasticSearchIndexer) {
		SolrRunnable.elasticSearchIndexer = elasticSearchIndexer;
	}
    
	public static ElasticIndexService getElasticIndexService() {
		return elasticIndexService;
	}

	public static void setElasticIndexService(ElasticIndexService elasticIndexService) {
		SolrRunnable.elasticIndexService = elasticIndexService;
	}

	@Override
	public void run() {		
		logger.info("Search KAFKa  "+ data);		
		elasticIndexService.indexDocument(data, httpUtil, elasticSearchIndexer, kernelUrl);
	
	}
}

package com.alt.search.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.client.methods.HttpAsyncMethods;
import org.apache.log4j.Logger;

import com.alt.datacarrier.business.common.AltReadRequest;
import com.alt.datacarrier.business.common.AltReadResponse;
import com.alt.datacarrier.core.Request;
import com.alt.datacarrier.core.Response;
import com.alt.datacarrier.kernel.common.KernelCRUDResponse;
import com.alt.datacarrier.kernel.common.KernelSingleReadRequest;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import rx.Observable;
import rx.apache.http.ObservableHttp;

public class HttpUtil {
	
	private static final Logger logger = Logger.getLogger(HttpUtil.class);
	
	public Response<AltReadResponse> docReadRequestToKernel(Request<AltReadRequest> altRequestData, String kernelUrl) {
		String jsonInString = null;
		Request<KernelSingleReadRequest> requestList = new Request<KernelSingleReadRequest>();
		List<KernelSingleReadRequest> requestData = new ArrayList<KernelSingleReadRequest>();
		for (AltReadRequest altReadRequest : altRequestData.getRequestData()) {
			KernelSingleReadRequest readRequest = new KernelSingleReadRequest();
			readRequest.setDepth(altReadRequest.getDepth());
			readRequest.setDocIdentifier(altReadRequest.getDocIdentifier());
			readRequest.setLimit(altReadRequest.getLimit());
			readRequest.setOffset(altReadRequest.getOffset());
			readRequest.setType(altReadRequest.getType());
			requestData.add(readRequest);
		}
		requestList.setRequestData(requestData);
		requestList.setProtocol(altRequestData.getProtocol());
		try {
			jsonInString = new ObjectMapper().writeValueAsString(requestList);
		} catch (JsonGenerationException e) {
			logger.error("Error occured:", e);
		} catch (JsonMappingException e) {
			logger.error("Error occured:", e);
		} catch (IOException e) {
			logger.error("Error occured:", e);
		}

		String responseString = getDataFromUrl(kernelUrl, jsonInString).toBlocking().first();
		
		Response<KernelCRUDResponse> responseKernel;
		logger.info(responseString +" ******** HTTP UTIL RESPONSE STRING");
		try {
			responseKernel = new ObjectMapper().readValue(responseString, new TypeReference<Response<KernelCRUDResponse>>(){});
			logger.info(responseKernel+  "**************** responseKernel");
			Response<AltReadResponse> businessResponse = new Response<AltReadResponse>();
			businessResponse.setResponseCode(responseKernel.getResponseCode());
			businessResponse.setErrMsg(responseKernel.getErrMsg());
			businessResponse.setResponseStatus(responseKernel.getResponseStatus());
			businessResponse.setResponseData(new AltReadResponse());
			businessResponse.getResponseData().setInstance(responseKernel.getResponseData().getInstance());
			businessResponse.getResponseData().setMeta(responseKernel.getResponseData().getMeta());
			logger.info(businessResponse.toString() +" ******** HTTP UTIL businessResponse STRING");
			return businessResponse;
		} catch (JsonParseException e) {
			logger.error("Error occured:", e);
		} catch (JsonMappingException e) {
			logger.error("Error occured:", e);
		} catch (IOException e) {
			logger.error("Error occured:", e);
		}

		return null;
	}


	public rx.Observable<String> getDataFromUrl(String url, String jsonRequest) {
		Observable<String> rawResponse = null;
		try {
			final RequestConfig requestConfig = RequestConfig.custom()
			        .setSocketTimeout(30000)
			        .setConnectTimeout(500).build();
			final CloseableHttpAsyncClient httpClient = HttpAsyncClients.custom()
			        .setDefaultRequestConfig(requestConfig)
			        .setMaxConnPerRoute(20)
			        .setMaxConnTotal(50)
			        .build();
			httpClient.start();
			rawResponse = (ObservableHttp.createRequest(
					HttpAsyncMethods.createPost(url, jsonRequest, ContentType.APPLICATION_JSON), httpClient))
							.toObservable().flatMap(resp -> resp.getContent().map(String::new));
		} catch (Exception e) {
			logger.error("Error occured:", e);
		}
		return rawResponse;
	}

	public Map<String, Observable<String>> getDataFromUrlList(Map<String, String> requestMap) {
		Map<String, Observable<String>> response = new HashMap<String, Observable<String>>();
		try {
			final RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30000).setConnectTimeout(5000)
					.build();
			final CloseableHttpAsyncClient httpClient = HttpAsyncClients.custom().setDefaultRequestConfig(requestConfig)
					.setMaxConnPerRoute(20).setMaxConnTotal(50).build();
			httpClient.start();
			requestMap.entrySet().parallelStream().forEach(entry -> {
				try {
					response.put(entry.getKey(),
							(ObservableHttp.createRequest(HttpAsyncMethods.createPost(entry.getKey(), entry.getValue(),
									ContentType.APPLICATION_JSON), httpClient)).toObservable()
											.flatMap(resp -> resp.getContent().map(String::new)));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			logger.error("Error occured:", e);
		}
		return response;
	}

}

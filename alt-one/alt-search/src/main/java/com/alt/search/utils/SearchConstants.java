package com.alt.search.utils;

public class SearchConstants {
	
	private SearchConstants(){
		
	}
	
	public static final String ID = "id";

	public static final String CAS = "cas";

	public static final String CLASSCODE = "classCode";

	public static final String CREATEDBY = "createdBy";

	public static final String CREATEDDATE = "createdDate";

	public static final String TYPE = "type";

	public static final String STATUS = "status";

	public static final String MODIFIEDDATE = "modifiedDate";

	public static final String MODIFIEDBY = "modifiedBy";

	public static final String SEARCHABLE_S = "_S";

	public static final String NOTSEARCHABLE = "_";

	public static final String DYNAMIC_SEARCHABLE_ATTRIBUTE = "_A";

	public static final String BLANK_SPACE = " ";

	public static final String NAME = "name";
	
	public static final String CUSTOM_ATTRIBUTE_ALLOWED = "customAttributeAllowed";

	public static final String INSTANCE_LIMITER = "instanceLimiter";

	public static final String INSTANCE_PREFIX = "instancePrefix";

	public static final String LOCATION_SPECIFIC = "locationSpecific";

	public static final String PACKAGE_CODES = "packageCodes";

	public static final String SYSTEM_TYPE = "systemType";

	public static final String UPDATION_UUID_LIST = "updationUuidList";

	public static final String PARAMETER_TYPE = "parameterType";

	public static final String REFERRED_BY = "referredBy";
	
	public static final String ATTRIBUTE_CODE = "attributeCode";

	public static final String ATTRIBUTE_NAME = "attributeName";

	public static final String DATA_TYPE = "dataType";

	public static final String DEFAULT_VALUE = "defaultValue";

	public static final String DISPLAY_SEQUENCE = "displaySequence";
	
	public static final String DISPLAY_UNIT = "displayUnit";

	public static final String INDEXABLE = "indexable";

	public static final String MANDATORY = "mandatory";

	public static final String PLACEHOLDER_TYPE = "placeholderType";

	public static final String REFERENCE_CLASS_CODE = "referenceClassCode";
	
	public static final String SEARCHABLE = "searchable";
	
	public static final String UNIQUE = "unique";

	public static final String START = "start";
	
	public static final String NUMBER_OF_DOCS_FOUND = "numberOfDocsFound";

	public static final String DEF_TYPE = "defType";
	
	public static final String DISMAX = "dismax";
	
	public static final String ALTERNATE_QUERY = "q.alt";
	
	public static final String ALTERNATE_QUERY_VALUE = "*:*";

	public static final String QUERY_FIELD = "qf";

	public static final String GLOBAL_QUERY_FIELD = "auto_attrib";

}

package com.alt.search.utils;

import com.alt.datacarrier.kernel.common.KernelConstants.EnumRequestType;

public class SearchDataInputRequest {
	
	String docId;
	
	EnumRequestType type;

	public String getDocId() {
		return docId;
	}

	public void setDocId(String docId) {
		this.docId = docId;
	}
	
	public EnumRequestType getType() {
		return type;
	}

	public void setType(EnumRequestType type) {
		this.type = type;
	}


	
}
